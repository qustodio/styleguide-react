module.exports = {
  commitUrlFormat: '{{host}}/{{owner}}/{{repository}}/commits/{{hash}}',
  message: 'Bumped version to %s',
  writerOpts: {
    types: [
      { type: 'feat', section: 'Features', regex: /^ROCK-\d+ feat/ },
      { type: 'feature', section: 'Features', regex: /^ROCK-\d+ feature/ },
      { type: 'fix', section: 'Bug Fixes', regex: /^ROCK-\d+ fix/ },
      {
        type: 'perf',
        section: 'Performance Improvements',
        regex: /^ROCK-\d+ perf/,
      },
      { type: 'revert', section: 'Reverts', regex: /^ROCK-\d+ revert/ },
      {
        type: 'docs',
        section: 'Documentation',
        hidden: true,
        regex: /^ROCK-\d+ docs/,
      },
      {
        type: 'style',
        section: 'Styles',
        hidden: true,
        regex: /^ROCK-\d+ style/,
      },
      {
        type: 'chore',
        section: 'Miscellaneous Chores',
        hidden: true,
        regex: /^ROCK-\d+ chore/,
      },
      {
        type: 'refactor',
        section: 'Code Refactoring',
        hidden: true,
        regex: /^ROCK-\d+ refactor/,
      },
      { type: 'test', section: 'Tests', hidden: true, regex: /^ROCK-\d+ test/ },
      {
        type: 'build',
        section: 'Build System',
        hidden: true,
        regex: /^ROCK-\d+ build/,
      },
      {
        type: 'ci',
        section: 'Continuous Integration',
        hidden: true,
        regex: /^ROCK-\d+ ci/,
      },
    ],
  },
  scripts: {
    prerelease: 'yarn build && git add build',
    postbump: 'node ./scripts/indexing-release.js && git add public',
  },
};
