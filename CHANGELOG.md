# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [11.1.0](https://bitbucket.org/qustodio/styleguide-react/compare/11.0.0...11.1.0) (2025-02-25)


### Features

* ROCK-307 updates Tooltip component for a11y compliance ([caeb70e](https://bitbucket.org/qustodio/styleguide-react/commits/caeb70e3d0dc7d5d3d9677fd65c3d7f8e658692b))
* ROCK-311 adds timeline, circle-o and grid-2 icons ([0d524f4](https://bitbucket.org/qustodio/styleguide-react/commits/0d524f43f6d59d517639abb0789e35e7f1aa86c6))


### Bug Fixes

* update Avatar component to use `Text` instead of `Typography` ([3b1849f](https://bitbucket.org/qustodio/styleguide-react/commits/3b1849f73ae51b69d926ee0dab15d24c8f8b9caf))

## [11.0.0](https://bitbucket.org/qustodio/styleguide-react/compare/9.6.0...11.0.0) (2025-02-17)

### ⚠ BREAKING CHANGES

* PAR-6003 typography component ([c2d0dcb](https://bitbucket.org/qustodio/styleguide-react/commits/c2d0dcb18ed13ec0b99214386fe1a016b327470f))
  * feat!: remove deprecated fields
  * refactor!: rename `Typography` by `Text`
  * feat: add deprecated to the font-size variables
  * docs: update ADR
  * feat: remove old heading styles
  * chore: rebuild test snapshots

#### Deprecation notes

##### SASS variables and classes

Some SASS variables has been removed:

```scss
  --font-size-h1: var(--font-size-xxxxl);
  --font-size-h2: var(--font-size-xxxl);
  --font-size-h3: var(--font-size-xxl);
  --font-size-h4: var(--font-size-xl);
  --font-size-h5: var(--font-size-l);
```

It is necessary to change the use of these variables with the proper mixins defined in the typography foundations, for example [in Public site](src/tokens/fonts/alias/public-site.scss).

Something similar happens with `.rck-h1`, `.rck-h2`, `.rck-h3`, `.rck-h4` and `.rck-h5` classes. The places where these classes are used should be replaced by the proper mixins. Sometimes these styles has to be replaced by the correct Text component with this specific variant. The equivalence is:

* `h1` -> `headline-1`
* `h2` -> `headline-2`
* `h3` -> `title-1`
* `h4` -> `title-2`
* `h5` -> `title-3`

It’s important to check the page after apply the corresponding style

##### React changes

The use of the `Typography` component has been deprecated. Since now, `Text` component will be used for styled text. There is more information in the [created ADR](docs/adr/0005-new-text-component.md).

### Features

* **fundation-color:** ROCK-279 update colors foundation ([1145cab](https://bitbucket.org/qustodio/styleguide-react/commits/1145cab1c313a886f2367f67c11a8edb3e884fd6))
* **fundation-typography:** ROCK-280 update typography foundation ([#484](https://bitbucket.org/qustodio/styleguide-react/issues/484)) ([d95c57a](https://bitbucket.org/qustodio/styleguide-react/commits/d95c57a1673faa0a347bcc0d779d38c0be234fb9))
* **fundation-color:** ROCK-302 colour foundations update ([ab477d2](https://bitbucket.org/qustodio/styleguide-react/commits/ab477d2b067734c30a7e6477c9059ebe8927e6c6))



## [10.0.0](https://bitbucket.org/qustodio/styleguide-react/compare/9.6.0...10.0.0) (2025-02-21)


### ⚠ BREAKING CHANGES

* * `AvatarSize.large` size is changed to `AvatarSize.extraLarge`

* New size variant of 80 x 80 is created and is mapped to `AvatarSize.large`

* Removes avatar inner container size reductions for text and image avatars.

* Removes avatar the 3px margin

* Removes `AvatarStatus` enum

* Removes `discarded` prop

* Removes `debug` prop

* Deprecates the `emptyBackground` prop`

* Adds `isFreeForm` to remove overflow from the avatar

### Features

* ROCK-299 updates the Avatar component ([82fe72f](https://bitbucket.org/qustodio/styleguide-react/commits/82fe72f2acc3ab89140e458bfe451ebb842513ea))

## [9.6.0](https://bitbucket.org/qustodio/styleguide-react/compare/9.5.0...9.6.0) (2024-12-12)


### Features

* **IconWithBadge:** add icon with badge component to use in the notification center to inform of new elements ([fe85792](https://bitbucket.org/qustodio/styleguide-react/commits/fe85792c9ec4c79b018e380e0a2fb94370cbcc69))


### Bug Fixes

* **ExpandCollapseItem:** add opacity to icons when the row is disabled ([1d3e1b8](https://bitbucket.org/qustodio/styleguide-react/commits/1d3e1b8ce9be18ecff1c7dfb7f842fa97bc18d01))

## [9.5.0](https://bitbucket.org/qustodio/styleguide-react/compare/9.4.0...9.5.0) (2024-09-17)


### Features

* adds `Conversation` component (ROCK-270) ([3e0e37b](https://bitbucket.org/qustodio/styleguide-react/commits/3e0e37b3d3a36bcbdf7abb021988c38adee772b1))

## [9.4.0](https://bitbucket.org/qustodio/styleguide-react/compare/9.3.0...9.4.0) (2024-08-30)


### Features

* **CompactStyleListItem:** add clickable space to the compact list ([6c0de3f](https://bitbucket.org/qustodio/styleguide-react/commits/6c0de3f9f528bef23af57899a56101169f48f62a))
* **CustomStyleListItem:** add optional clickable area in the right content ([346b7cf](https://bitbucket.org/qustodio/styleguide-react/commits/346b7cf5b62bce9b7079776287ca70d9be989084))
* **ExpandText:** create expand text component ([b83859b](https://bitbucket.org/qustodio/styleguide-react/commits/b83859bb0d4ce3624b8f05a8017bc9f567f6d341))
* **SelectableListItem:** add clickable area to icon title and subtitle ([e3ccead](https://bitbucket.org/qustodio/styleguide-react/commits/e3cceadd087806b6356dda97bbcbce144955547d))


### Bug Fixes

* **ExpandCollapseRowItem:** prevent from bubbling the on info click event ([9a88424](https://bitbucket.org/qustodio/styleguide-react/commits/9a88424d13a33ed1517718b9a81f246c35178a0b))
* remove pointer from content inside expandableBase component ([e186d2f](https://bitbucket.org/qustodio/styleguide-react/commits/e186d2ffdca93ca290a598aecb3700fec5a1f9fd))

## [9.3.0](https://bitbucket.org/qustodio/styleguide-react/compare/9.2.2...9.3.0) (2024-07-12)


### Features

* Improve `Typography` component to have punctual bold text inside ([e74ce89](https://bitbucket.org/qustodio/styleguide-react/commits/e74ce89d48ca34fef8d3351404b5d5291db1fcc3))

### Bug Fixes

* prevent the user from selection the active option with the keyboard when the dropdown is not active ([dc07f84](https://bitbucket.org/qustodio/styleguide-react/commits/dc07f8451bd512b345e6a1ad21292803027f5cc4))

## [9.2.2](https://bitbucket.org/qustodio/styleguide-react/compare/9.2.1...9.2.2) (2024-06-20)

## [9.2.1](https://bitbucket.org/qustodio/styleguide-react/compare/9.2.0...9.2.1) (2024-06-14)


### Bug Fixes

* add missing export for expandCollapseRowItem ([04fbb06](https://bitbucket.org/qustodio/styleguide-react/commits/04fbb0611149f5c4d29f062a33171c010cb08245))

## [9.2.0](https://bitbucket.org/qustodio/styleguide-react/compare/9.1.2...9.2.0) (2024-06-13)

## [9.1.2](https://bitbucket.org/qustodio/styleguide-react/compare/9.1.1...9.1.2) (2024-05-24)

 This release contains no changes, it's only being released because the build system version has been updated.

## 9.1.1 - 2024-03-28

Fixed:

- `Accordion` Component:
  - Fix layout shift in when opening an Accordion Item.
  - Set optional accordion props as optional in the Accordion interface too.
  - Fixes box-sizing value in AccordionItem component

- `Button` Component:
  - Handle `wellbeing` yellow button as a `color` instead of as a `type`.

- `BoxSelector` Component:
  - Fix extra margin on right side
  - Add listener to handle input changes
  - Fix box-sizing value

## 9.1.0 - 2024-03-25

Added:

 - Icons: adds icons `heartCircleCheck`, `messageSmile`, `commentsQuestionCheck`, `heartbeat` and `faceSmile`

Fixed:

 - `MarketingBanner` Component: Fixes text alignment when the property value is center

## 9.0.0 - 2024-03-18

Added:

- Added Rocket Color Token types: `RckColorPalette`, `RckBaseColor`, `RckSemanticColor`, `RckBrandColor`, `RckColor`.
- Added Rocket Color Gradient Token types
- Added `textWrapper` helper.
- Added `ShadowedCard` component.
- Added `BoxSelector` and `BoxSelectorItem` components.
- Added `MarketingBanner` component

Updated:

- `Accordion` Component:
  - Now accepts `color` prop, which accepts any value from `RckBaseColor`.
  - Now accepts `highlightOnOpen`, that adds a background in a lighter shade of `color` to the open `AccordionItem`.
  - Now accepts `maxColumns` prop, which sets the number of columns to distribute the `AccordionItem` components in, as long as the container element has enough space (if not, fall backs to the previous value e.g: 3 > 2, and 2 > 1).

- `AccordionItem` Component:
  - `content` prop can now contain any type of JSX element, not just a string.

Breaking changes:

- `Accordion` Component:
  - No longer accepts `type` prop.

- `AccordionItem` Component:
  - Removed `isHighlightedWhenOpen` prop. This property is now set at the `Accordion` level.
  - `icon` no longer accepts background color or other `Icon` properties. Only a string with the icon name is accepted.
  - `content` can now contain any type of JSX element, not just a string.

## 8.1.0 - 2024-02-21

Added:

- adds custom badge to `SideNavigationMenu`.
- adds `handHoldingHeard` icon.

Fixes:

- fix badge position on `SideNavigationMenu`

## 8.0.0 - 2024-02-13

Added:

- Adds new `color`, `spacing`, `radius`, `shadow`, `fonts`, `typography` and `layout` design tokens.
- Adds `ModalInfo` component.
- Adds `gap` prop to `FlexLayout` component.
- Exports `Toast` component.

Update:

- Updates `Modal`, `Typography` and `Toast` (previously known as `Notification`) component to follow the new design specs.

Breaking changes:

- `Toast` Component
  - Renames `Notification` component to `Toast`.
  - Renames `NotificationManager` class to `ToastManager`.
  - Renames `.rck-notification` css class to `.rck-toast`.
- `Typography` Component
  - Deprecates the following props:
    - `component` prop in favor of `renderAs`.
    - from `type` prop: `h1`, `h2`, `h3`, `h4`, `h5` and `caption2-quote`.
  - Removed the following props:
    - from `weight` prop: `inherit` and `medium`.
    - from `align` prop: `inherit`, `justify`, and `right`.
- `Modal` Component
  - Deprecates the following `Modal` props:
    - `width`, `height`, `actionLabel`, `actionLoading`, `isFullScreen`, `buttonsAlignment` and `closeIconType`.

## 7.21.1 - 2024-01-22

Fixes:

- Adds the correct test id prop in `TimepickerInput` to propagate it to `TextFieldBase`.

## 7.21.0 - 2023-12-20

Added:

- Adds new fontawesome icons.

## 7.20.0 - 2023-12-07

Update:

- Update grey colors using `rck-grey-500`, `rck-grey-400`, `rck-grey-300`, `rck-grey-200` and `rck-grey-100` tokens.

## 7.19.0 - 2023-12-05

Added:

- Adds yellow color palette, rename orange color to peach, fixes planboxes ribbon colors

Update:

- Adds `isFullScreen` and `showBackButton` props to the `Modal` component

## 7.18.0 - 2023-11-27

Update:

- updates `AlertBox` component to adhere to the new rocket design system
- updates `booster-lighter` color to the new value

## 7.17.0 - 2023-11-24

Update:

- update `Tag` component warning variant (text: lighter, bg-color: default)
- update `AlertBox` component warning variant (text: lighter, bg-color: default)

## 7.16.0 - 2023-11-20

Update:

- Update warning colors using `warning-400`, `warning-500`, `warning-600` and `warning-700` tokens.

## 7.15.0 - 2023-10-24

Added:

- Adds new internal `TextFieldBase` component.
- Adds `TimePickerInput` component.

Update:

- Updates `DurationInput` using `TextFieldBase` component.
- Removes some scroll to show half of the previous option from active in `Dropdown`.

## 7.14.0 - 2023-10-09

Added:

- Adds new fontawesome icons.

Update:

- Autoselects input text when focusing in `DurationInput`

## 7.13.0 - 2023-09-13

Update:

- `Dropdown` scrolls to the `selected` option when it is opened.

## 7.12.0 - 2023-09-07

Added:

- Adds new fontawesome icons.

## 7.11.0 - 2023-09-05

Added:

- Adds new fontawesome icons.

Update:

- Updates `Drawer` component to allow passing `className` prop.

## 7.10.0 - 2023-08-24

Added:

- Adds Table part components and ComparaisonTable one.
- Adds `PlanBoxExtended` component.
- Adds new fontawesome icons.
- Adds `checkbox` and `radio` variant to `SelectableListItem` component.

## 7.9.0 - 2023-07-25

Added:

- Adds new fontawesome icons.
- Adds `ToggleContainer` component.

## 7.8.0 - 2023-07-03

Added:

- Adds `usePopover` hook.
- Adds `Tooltip` component.

Update:

- Updates `AutoPortal` implementation.

## 7.7.0 - 2023-07-03

Added:

- Adds rocket-launch icon

## 7.6.0 - 2023-06-27

Update:

- Updates icon type to add circle plus icon
- Updates icon sizes to allow usage of x4

## 7.5.0 - 2023-06-20

Added:

- Adds a rollup pre-build script to extract rocket colors to a JS file.
- Exports `theme` object with the global and routines color palette.

Update:

- Updates `ColorPicker` component so it accepts a colors palette object/array as a prop.

## 7.4.1 - 2023-06-13

Changes:

- Fix `ColorPicker` routines color order.

## 7.4.0 - 2023-06-09

Changes:

- Adds `TextField` and `GroupedInput` ref property.

## 7.3.1 - 2023-04-26

Changes:

- Fix `Card` overflow which was causing a bug with the scroll when the content of the card is higher than the screen height.

## 7.3.0 - 2023-04-19

Changes:

- Fix `PageTitle` overflowing container and pushing content to the right out of the screen when title is too long.
- Adds `error` and `white` color to `plain` button type.

## 7.2.1 - 2023-04-14

Reverts:

- Fix `PageTitle` overflowing container and pushing content to the right out of the screen when title is too long.

Changes:

- Fix: Fixes `Drawer` component to hide the invisible container when it's closed.

## 7.2.0 - 2023-04-13

Changes:

- Fix: Adds the option toggle ellipsis for page subtitle to not have hidden overflow
- Fix `MultiStep` active step state delegation.
- Adds: `AlertBox` text by default aligned start.
- Adds: add no referrer policy to icon adapter image

## 7.1.2 - 2023-04-05

Changes:

- Fix `PageTitle` overflowing container and pushing content to the right out of the screen when title is too long.

## 7.1.1 - 2023-04-05

Changes:

- Fix `TextArea` box-sizing.

## 7.1.0 - 2023-04-05

Changes:

- Adds `TextArea` component.
- Adds `textCentered` variant in `TextField` component.

## 7.0.1 - 2023-03-15

Changes:

- Allows `className` prop override the styles of `TransparentButton` default styles.
- Fixes `iconType` prop in `Tag` component.
- Makes `text`, `variant`, `type` props optional in `TagProps`
- Makes `color`, `weight` and `align` as `'inherit'` for default value in `Typography`.

## 7.0.0 - 2023-03-09

Added:

- Adds new `Tag` component.
- Marks `Badge` component as deprecated in favor of `Tag` component.
- Updates `GlobalType` to include all the color palette options.
- Creates `$rck-global-color-palette` variable with the design system global color palette.
- Creates `$rck-routine-color-palette` variable with the routines color palette.
- Adds `rck-color-palette-map()` util function to manipulate color palettes.
- Adds `Typography` component.
- Marks `.rck-h[1-5]` global classes as deprecated in favor of `Typography` component.
- Updates `CardHeader` component to allow custom title and a wrapper property.
- Adds `CardActionTitle` component.
- Adds new colors scales to decoration and routines color palette.

Breaking changes:

- Rocket requires atleast sass-loader=^8.0.2 and sass=^1.2.2
- Removes `$global-types`, `$global-types-lighter`, `$global-types-keys` variables and replaces them with `$rck-global-color-palette`.

## 6.0.0 - 2023-03-02

Added:

- Adds routines color palette
- Adds color picker component
- Add new icons for routines: backpack, basketball, bookOpen, books, buildingColumns, bullseye, filter, gamepad, gamepadModern, headphones, joystick, moon, music, newspaper, palette, shareNodes, slidersHorizontal, smoking, sportsball, utensils, xmarksLines
- Add selectable list items
- Add school color to palette

ADR:

- Adds Code documentation ADR

Breaking changes:

- Removed ActivityListStyleItem

## 5.10.0 - 2023-02-21

Added:

- Adds Filter and FilterGroup component

## 5.9.0 - 2023-02-08

Added:

- Adds PageTitle font weight as semibold

## 5.8.0 - 2023-02-07

Added:

- Adds Icons arrowUpRightFromSquare, fileCircleInfo, graduationCap, wifi

## 5.7.0 - 2023-02-01

Added:

- Adds Autoportal component.
- Adds Drawer component.
- Adds MultiStep component.
- Adds PageTitle medium size.
- Adds Icons balanceScale, bell, headSet, hourglassEnd, hourglassHalf, icons, laptop, pause, pauseCircle, shieldCheck, stopWatch and userShield.

Update:

- Font Awesome update to v6.2.1.

## 5.6.0 - 2023-01-23

Added:

- Adds PhonePlus, HourglassHalf and Icons icon.

Update:

- Standardize TextField component.

## 5.5.0 - 2022-12-21

Added:

- ExpandableCard: Added component with horizontal and vertical mode.
- Adds laptop icon.

## 5.4.0 - 2022-12-09

Added:

- ActionInput: Added new compact variant.
- Adds color to active tab label.

## 5.3.1 - 2022-10-26

Issues:

- Card: Allows setting CardHeader `type` independently from Card `type`.

Fixes:

- Card state with error type.

## 5.3.0 - 2022-10-24

Added:

- Card: Added plain red background variant to the Card component.
- Icon: Added `ShieldCheck`, `WifiSlash` and `HourGlassEnd` icons.

Issues:

- DurationInput: Removed local state from the component.

## 5.2.2 - 2022-10-05

Issues:

- Added `$images-path` overridable global variable to set the base path for images.

## 5.2.1 - 2022-10-03

Issues:

- Export DurationInput

## 5.2.0 - 2022-10-03

Added:

- Button: Added `white` variant to `ButtonColor` prop for button with buttonType={ButtonType.secondary}.
- Icons: Added `Pause` icon and it's circle variant.
- Colors: Added `secondary` and `orange` color gradients to the pallette (`$rck-color-{type}-gradient-[from|to]`).
- Cards: Added new prop 'backgroundGradient'.
- Cards: Added new prop 'backgroundShapes'.
- Added `DurationInput` component.

Changes:

- Build: Add index.html to list the different versions that we have.

## 5.1.0 - 2022-09-19

Added:

- Export ButtonColor
- Export ButtonType

## 5.0.0 - 2022-09-14

Changes:

- Colors: New color \$rck-color-error-darker: #a03941.
- Colors: Changed colors $rck-color-primary-lighter and $rck-color-success-lighter: #e9fff8.
- Stories: Buttons/More Button story moved to Misc/Buttons/More Button.
- Stories: Buttons/Transparent Button story moved to Misc/Buttons/Transparent Button.
- Stories: Buttons/ActionableContent story moved to Misc/Buttons/ActionableContent.
- Stories: Deleted Design/Cards/Button/Links story and related files.
- Button: Remove dark background from loading button.
- Button: new prop 'buttonType'.
- Button: new Prop 'color'.
- Button: On centered block buttons, push icons to the sides.

Breaking changes:

- Button: Block button now has a max width of 384px.
- Button: Removed prop 'hollow'. Use buttonType={ButtonType.secondary} instead.
- Button: Removed prop 'highlighted'. Use buttonType={ButtonType.secondary} and color={ButtonColor.error} instead.
- Button: Removed prop 'plain'. Use buttonType={ButtonType.plain} instead.
- Button: Removed prop 'inverted'. Use color={ButtonType.white} instead.
- Button: Removed prop 'bigRounded'.
- Button: Removed prop 'shadow'.
- ButtonSize: Removed value 'extra-small'.

## 4.7.0 - 2022-09-15

Changes:

- DropdownOption: Pass onClick handler Synthetic event as optional second parameter

```js
const logAndStopPropagation = (name: string, event?: SyntheticEvent): void => {
  console.log(name);
  event.stopPropagation();
};

<DropdownOption onClick={logAndStopPropagation} />;
```

## 4.6.0 - 2022-09-12

Changes:

- Icon: Add custom icons support `RocketCustomIcon`
- Icon: Decoupling Font Awesome from the `Icon` component
- Build: Clean storybook builder and unnecessary type definitions

## 4.5.0 - 2022-09-07

Changes:

- InstructionCard: feat Adds clickable icon as Link using prop iconAslink

## 4.4.1 - 2022-07-22

Changes:

- Avatar: fix Image and Text Avatar size

## 4.4.0 - 2022-07-14

Changes:

- ProfileInfoHeader: add optional action element
- Badge: change the styles of the element

## 4.3.2 - 2022-07-01

Issues:

- Export AvatarVariant enum

## 4.3.1 - 2022-07-01

Changes:

- Avatar: add option to use a custom image
- Avatar: add option to use a couple of letters

## 4.3.0 - 2022-06-21

Changes:

- Add font weight generic css classes

## 4.0.35 - 2021-08-16

Issue:

- Card: fix height overflow and height percentage calculations inside body

## 4.0.36 - 21-08-17

Issue:

- Textbox: disabled state consistency (border colors and active border colors fixed). Also cursor not-allowed added
- AlertBox: inside Card should keep its styles over right icon

## 4.0.37 - 21-08-19

Update:

- List: adding flex-wrap prop for horizontal lists

## 4.0.38 - 21-08-19

Added:
-Layout: new `centerX`property to center content without flex, also Layout now returns the DOM `ref`

Issues:

- SideNavigationMenu: fix for iOS 13 and SE

## 4.0.41 - 21-08-23

Added:

- CopyBox: Widget which displays the link to be copied along a copy icon. When clicking the icon the passed url prop gets written in the clipboard

## 4.0.42 - 21-08-23

Update:

- PlanBox: remove optionalText property and at it place use the priceAdditionalInformation
- Banner: new position at corner left

## 4.0.43 - 21-08-23

Added:
-Notifications and NotificationManager

## 4.0.44 - 21-08-27

Issues:

- Change remove-scroll to dependency
- Add remove-scroll in external library in rollup config

## 4.0.45 - 21-08-27

Added:

- Icon and Svg: grayLight color added

Issues:

- NotificationManager: fix onClose manually

## 4.0.46 - 21-08-27

Issues:

- Sidebar menu: some padding and font size fixes

## 4.0.47 - 21-08-30

Added:

- Banner: now accept `rounded` prop to allow or not rounded borders
- New ListSecondaryText component for lists
- PageTitle: added new `subtitle` prop

Issues:

- ListThumbnail: now play icon is smaller
- Menu: fix paddings
- Tabs: added paddings to tabs
- Tabs: added paddings to tabs

## 4.0.48 - 21-08-30

Added:

- AlertBox: added info type
- Layout: added a couple long special margins
- FreeText new component

Issues:

- AlertBox: fix colors following V3 styles
- Banner: fix typo issues with `rounded` className
- Menu: outline removed

## 4.0.49 - 21-09-13

Added:

- EmptyPlaceHolder: Add new prop `smallText`

Issues:

- Cards: fix scroll

## 4.0.50 - 21-09-14

Added:

- ListColeredText: added new `wordBreak` prop

Issues:

- Icons: fixes square radius to 4px
- List: Regular Style items properly centered

## 4.0.51 - 21-09-20

Issues:

- List: changed listColoredText from `primary` to `secondary`value
- Sidebar: item padding and text centering fixed

## 4.1.0 - 21-09-17

Breaking change:

- PlanBox UI changed
  - Button size changed to big
  - Below the title of the box, now we display the yearly price
  - Number of devices is displayed above the button
  - When the PlanBox is highlighted and has a discount, the priceAdditionalInformation is colored in orange
  - The spacing between the different elements changed
- PlanBox props api changed
  - all the props related to the button are grouped in button prop
  - all the props related to the ribbon are grouped in ribbon prop
  - subtitle has been renamed to deviceInformation
  - useDefaultWidthValues and type props have been deleted
- Ribbon changed
  - center type has been deleted
  - top and right positioning have been updated

## 4.1.1 - 6-10-20

Issues:

- SideNavigationMenu: not apply hover for mobile resolutions

## 4.1.2 - 26-10-20

Breaking change:

- Button property removed

## 4.1.3 - 8-11-20

Breaking change:

- PlanBox UI changed

## 4.1.4 - 8-12-14

Issues:

- Do not remove root if it's using single hook

## 4.1.5 - 8-12-17

Issues:

- Update types for Planbox component

## 4.1.6 - 8-12-20

Issues:

- Fix alignment for checkbox container icon at Planbox component

## 4.1.7 - 4-1-21

Issues:

- Make bigger icons at page title component

## 4.1.8 - 5-1-21

Added:

- New red button color for a highlighted state

## 4.1.9 - 5-1-26

Added:

- Update font sizes for headers

## 4.1.10 - 5-1-26

Issues:

- Remove border bottom from List component

## 4.1.11 - 22-2-8

Added:

- New disabled state for action input component
- New disabled color for icons

## 4.1.12 - 22-2-9

Issues:

- Do not apply secondary color to border to action input component on active

## 4.2.0 - 22-02-22

Changes:

- Modal: add `hideCloseButton` property
- PortalModal: add `hideCloseButton` property

## 4.2.1 - 22-03-09

Changes:

- Icon: add `fa-user-headset` type

## 4.0.34 - 2021-08-16

Update:

- Fix button height and convert text to two lines when text exceeds size

## 4.0.33 - 2021-08-13

Changes:

- Cards: useless backgrondColor header prop removed

## 4.0.32 - 2021-08-13

Update

- Modify display flex to block in profile header name

## 4.0.31 - 2021-08-12

Added:

- StepBar: Now labels could be hidden by new `hideLabels`prop

Fixes:

- Layout: fix issue with flex wrap property. Also added to justify-content the space-evenly option
- TransparentButton: allow different html type buttons
- PageTitle: fixed issue when right side is empty. Also fix issues with paddings when is not small.

## 4.0.30 - 2021-08-11

Add

- Link icon type

## 4.0.29 - 2021-08-11

Add:

- Layout: more bigger margins added until 112px

## 4.0.28 - 2021-08-11

Issues:

- StepBar: fix issue with last step

## 4.0.27 - 2021-08-09

Update:

- Removed large prop and respective styles from EmptyPlaceholder
- Updated font props according to mock-ups for EmptyPlaceholder and h4 & .rck-h4
- Centered title element in EmptyPlaceholder

## 4.0.26 - 2021-08-09

Added:

- Ellipsis to Label

## 4.0.25 - 2021-08-09

Issues:

- Fix and use variable side bar expanded in profile name info

## 4.0.24 - 2021-08-09

Issue:

- List: Delete overflow hidden at root level
- ActionButton: display flex added

## 4.0.23 - 2021-08-07

Added:

- Avatar: added new prop `discarded`
- Layout: added new spacing `auto`
  Issues:
- Avatar: fix disabled status

## 4.0.22 - 2021-08-06

Issues:

- Fix condition to affiliate image at LicenseInfoContainer

## 4.0.21 - 2021-08-06

Added:

- New SafeArea component
- MoreActionButton: fix color
- PageTitle: could change icon colors now
- Icon: new type black

## 4.0.18 - 2021-08-02

Issues:

- Accordion: fix margins by media

## 4.0.19 - 2021-08-2

Added:

- New component for Brands Header

## 4.0.20 -2021-08-05

Added:

- Card: body is now flex column styled
- PageTitle: paddings in actions removed and component height fixed

## 4.0.17 - 2021-07-30

Issues:

- Card: fix accidentally removed banner feature from Card

## 4.0.16 - 2021-07-30

Update:

- New styles for StepBar component

## 4.0.15 - 2021-07-28

Update:

- Remove background color and add type in Card

## 4.0.14 - 2021-07-27

Added:

- AlertBox: new optional prop `rounded`

## 4.0.13 - 2021-07-27

Issues:

- Apply new styles for LicenseInfoContainer

## 4.0.12 - 2021-07-26

Issues:

- List: fix native column list

## 4.0.11 - 2021-07-26

Breaking changes:

- Card: now fillHeight and fillWidth are `fullWidth`and `fullHeight`

Added:

- AlertBox: `fullWidth` new prop
- New component Banner
- Card header could accept Banner or AlertBox components as header
- Dropdown: `enableRemoveScroll`new props to remove scroll when scrolling area is not the window
- Icon: Added new icon `fa-phone-slash`
- Layout: Added new content spacing sizes: 24, 32, 40, 48, 56
- List: new prop `useNativeColumns`, set to true by default because backward compatibility

Issues:

- Icon: square dimensions fixed for regular and 10x
- LicenseInfoContainer: added displayName property
- List: Support for non-native columns property
- SideNavigationMenu: text now does not has max-height to support large texts

## 4.0.10 - 2021-07-23

Added:

- PlanBox now will have new styles and accepts two new properties to higlight it and price additional information

## 4.0.9 - 2021-07-21

Added:

- Card: new `minHeight` prop for body
- FlexLayout return the container div DOM ref
- Layouts: new `textAlignement` prop

Issues:

- ProfileInfoHeader: line-height increased to about overlap of lines
- Button: `block`mode that expands all the width available was text broken

## 4.0.8 - 2021-07-20

Issues:

- Replace badge for a button at license info container

## 4.0.7 - 2021-07-19

Issues:

- Update size of empty background at Avatar component

## 4.0.6 - 2021-07-19

Issues:

- Use Link at licenseInfoContainer component

## 4.0.5 - 2021-07-19

Added:

- At Action Input:
  - Prop to handle icon color
  - Padding from 16px to 8px
- At Text field
  - Update border color to grey light
  - Update font size for label
- At Avatar
  - New prop to empty background type of avatar
- At Badge
  - New type for secondary color background
- At Side navigation menu
  - New styles for license
  - New prop licenseInfoContainer to display license and affiliate information
  - Update padding, font sizes, background colours
- At New component for LicenseInfo
  - Change to 14px font size
- At Side bar
  - Icons to 24px and font size to 16px, hover are bigger (square) when expanded

## 4.0.4 - 2021-07-16

Added:

- Layouts now accept colors (new `backgroundColor` prop)
- Menu has a new prop `scrollable`and now `profileInfoHeader`prop is optional
- PageTitle accepts new action name `settings`, new prop `centered`
- GroupHeader now has white background color
- Card now has `fillWidth`and `fillHeight`props and `minHeight`
- Layout has now `800px` as a valid layout spacing size
- FlexLayout basis now accept percentages

Issues:

- Menu: Some issues related to styles fixed (heights and scrollable content)
- PageTitle: `menu` should been removed from right icons and added to left icons like `back`
- Layout: fix some style issues
- List multicolumn fix breaking elements

## 4.0.3 - 2021-07-14

Added:

- Accordion released

## 4.0.2 - 2021-07-13

Added:

- Accordion component

## 4.0.1 - 2021-07-13

Breaking changes:

- ProfileInfoHeader multiple fixes in styles. Also API changed adding `avatar` props and other removed

Changes:

- Cards cleanup, now 'removeMargins' prop has been deleted
- Menu cleanup. New props `profileInfoHeader` and `expanded` added. New scrollable area added
- New component PageTitle

Added:

- Icons now accepts prop `circle` and new color `green-decoration-contrast`. Also added new icons `bars`, `edit` and `pencil`
- FlexLayout and Layout accepts new DOM position configurations, also scrollY capability and height and width. New units has been added like `vh`
  and `vw`

## 3.2.2 - 2021-06-29

Issues:

- Add missing build files

## 4.0.0 - 2021-07-6

Breaking Changes:

- AccountInfoHeader: props `fullname` and `email` removed. `logoImage` prop added
- Button: props `ui` and `type`removed. `bigRounded`prop added
- Card:
  - header props: `headerAction`changed to `actionElement`, `onClickHeaderAction` changed to `onClickAction`. New props added like `border`and `backgroundColor`
  - footer props: `footerLeftAction`and `footerRightAction`removed. New props `onClickAction` and `actionElement`added
- StyledHeaderType change from enum to type
- Icon: default value for Icon type is `regular`. Also Icon adds new prop `square` and `contrastBlue`. New `fa-plus`icon added.
- MenuItem: props removed like `isFirstItem`, `isLastItem`, `isCollapsedItem`, `icon` and `isOpen`. Also new prop `isHeader`added
- Menu: styles about draggable support removed and support for section headers
- Color pallette changed
- Font family change to 'poppins'
- GlobalType (scss) now old primary is `secondary` also added new `primary` and old secondary is `gray`
- Variables (scss) `$side-navigation-menu-width` and `$side-navigation-menu-width-expanded changed`

Changes:

- Badge: new props `size`added and new type added as `contrast`
- SideNavigationMenu: new props `accountInfo`, `affiliateBadge` and `licenseBadge`
- GroupHeader now uses H4 header as default and colors changed
- CompactStyleListItem new prop `extraRightSpace`
- Quote spacing reduced
- Remove unused Avatar props

Added:

- New ProfileInfoHeader component
- New AffiliateBadge component
- New ActionableContent component
- New public Link component with support for icon left/right
- Added new font-size with 10px
- New data-testid added on dropdown container to suppor looking for it when is closed
- Switch now accept `className` prop

Issues

- Link disabled fixed

## 3.2.1 - 2021-06-29

Changed:

- PlanBox new prop to handle when to use default width values

## 3.2.0 - 2021-06-18

Added:

- Basic components now are memoized to improve performance
- ActionInput accept new 'error' prop

## 3.1.32 - 2021-06-17

Added:

- Added new icon `cog`
- MoreActionsButton now implement TransparentButton inside
- Dropdown:
  - mixins renamed with prefix "rck"
  - Dropwdown now accept active prop to be able to open dropdown from props
  - New Dropdown events available like onOpen and onClose
  - Support for single portal hook for usePortal and Dropdown
  - Now mewu horizontal horientation accepts "center" value

Issues:

- ActionInput should have button as button type
- Fix Dropdown position update when component is opened

## 3.1.31 - 2021-06-16

Issues:

- Padding fixed at Switch component

## 3.1.30 - 2021-06-16

Issues:

- Switch position fixed for RegularStyledListItem

## 3.1.29 - 2021-06-10

Changed:

- ContentSeparator border color changed to gray-lighter
- Added wideLeftSubtitle prop to LocationStyleListItem component for better spacing when hour is in AM/PM format
- increased InstructionCard icon size for both compact and regular

## 3.1.28 - 2021-06-09

Changed:

- Text property to content in the tab panel component

## 3.1.27 - 2021-06-09

Changed:

- added new 'large' prop to EmptyPlaceholder component

Issues:

- Fixed broken snapshot tests

## 3.1.26 - 2021-06-07

Added:

- InstructionCard component (used in Family Locator -> Kid's timeline)

Changed:

- added new props & styling to LocationStyleListItem: actions & showActions
- added new icon types: fa-map-marked & fa-map-marked-alt

## 3.1.25 - 2021-06-07

Added:

- list thumbnail compoment now has imageMinHeight and imageMinWidth props
- New component CloudList (final version)
- Display prop to FlexLayout component to allow to change de default display css prop

Issues:

- Fixed problem with Quotes (they didn't making the ellipsis)
- Force font size in Quote component

## 3.1.24 - 2021-06-04

Breaking changes:

- SvgSize component now is Svg component and it allow dev to change height/width and color

Added:

- New component CloudList
- New props in EmptyPlaceHolder component: `fillAvailabelSpace`, `centered`, `coloredBackground` and `icon`
- The removeMargins new prop for Card component
- New `display` prop for Layout component

Changed:

- DomainTrustSeal now has a smaller font size
- Quote maxLines property now has a type instead of enum

Issues:

- Fixed ActivityStyleListItem quote section

## 3.1.23 - 2021-06-03

Issues:

- Fix list thumbnail util to fill space given by layout

## 3.1.19 - 2021-05-31

Breaking Change:

- ListThumbnail util from List do not accept onClick and url anymore. If dev wants to add some behavior it could be done in its wrapper
- Last item from List do not have margin bottom/top when is set from listItemPadding/Margin props. Layout should be responsive about margins ourside the component

Added:

- Layout now has new prop noWrap
- Layout spacing now supports percentages: 25%, 50% and 75%
- Layout has a new prop lineHeight
- List could be rendered in horizontal orientation with new prop "horizontalOrientation"
- TransparentButton has new property "fillAvailableSpace"
- New component DomainTrustSeal
- New SvgSize compoenent to give size to children svg

Issues:

- Apply style spacings just to children li in List component
- Remove border bottom from List when item is the last one
- Switch is not properly disabled

## 3.1.20 - 2021-06-1

Added:

- Property at Dropdown component to align item

## 3.1.21 - 2021-06-2

Issues:

- List horizontal mode was breaking list `columns` property/feature. Now fixed

## 3.1.22 - 2021-06-2

Issues:

- Button component fix: hover state is sticky on touch devices.

## 3.1.18 - 2021-05-27

Added:

- Slider input component

## 3.1.17 - 2021-05-27

Added:

- New property for Dropdown compontent to fix menu position

## 3.1.16 - 2021-05-26

Changes:

- Makes bigger area of RegularStyleListItem actionElement

## 3.1.15 - 2021-05-22

Change:

- Fixed icon size and positioning for ListBckImageIcon and CustomStyledListItem

## 3.1.14 - 2021-05-21

Breaking Changes:

- To import Layout scss now you sould add 'Layout.scss" in your styles instead of 'FlexLayout.scss'

Added:

- Icons for Android and Windows added
- New Layout props 'minWidth', 'maxWidth', 'minHeight' and 'maxHeight'
- ClickableListItem now could render Link or TransparentButton based on the props recieved
- DeactivableContent to allow disable custom content
- ListItemBckImageIcon to allow background image icons inside Lists
- ListTitle and ListSubtitle now could render its tag as 'P' or 'DIV'
- List "allowCustomStyles" changed to "allowCustomListItemStyle" so now you have to specify witch type is valid
- Added new input type available for TextField as "search"
- Layout could be rendered now as span or div

Issues:

- Added margin and padding 0 to TransparentButton
- Added missing displayName properties in Compact style list components
- Internal scss utils prefixed with "rck"
- Flex align-items "start" option missing from scss
- Fixing icon size in TextField - height was calculated to 12.5px making it too small
- Removed TextField inner shadow in Safari mobile

## 3.1.13 - 2021-05-20

Changes:

- Fixing horizontal scroll when dropdown is open
- Adding new prop at TextField to handle min height

## 3.1.12 - 2021-05-19

Changes:

- PortalId mandatory at Dropdown component

## 3.1.11 - 2021-05-18

Changes:

- Adding data test id to label at ActionInput component

## 3.1.10 - 2021-05-15

Changes:

- Adding min height for TextField element to accomodate error messages
- InfoFieldLabel font-size change to 14px

## 3.1.9 - 2021-05-13

Changes:

- Fixing typo in RegularListItem classname element, adding explicit display: flex to avoid positioning artefacts when selecting an action element.

## 3.1.8 - 2021-05-12

Added:

- StyledInstruction component to apply color at InstructionBlock
- StyledHeader component

## 3.1.7 - 2021-05-12

Changes:

- Adding null value checks when iterating React children. When a null child is passed explicitly we return it directly.

## 3.1.6 - 2021-05-11

Changes:

- TransparentButton disabled style fixed
- default portal id added to usePortal following defaul from PortalModal component
- Dropdown portalId prop is now optional

## 3.1.5 - 2021-05-07

Changes:

- ActionInput has white background color on focus

## 3.1.4 - 2021-05-06

Breaking changes:

- FlexLayoutItem component removed

Added:

- Layout component added to apply spacing without flex
- Button now has 'form' property
- New InfoFieldLabel component added. TextField now uses this label to generate error/info messages

Changes:

- RadioButton margins removed, layout should give it the spacing. Also tabIndex prop was removed
- TextField adds new property "id"
- Switch now has onChange prop

Issues:

- RadioButton now works properly with redux-form. It works with value instead of id
- TextField scss helpers now have the rck prefix added to avoid naming collisions
- TextField sccs compatible with PAR
- TextField "Lines" type renamed to avoid naming collision to "InfoFieldLabelLines"

## 3.1.3 - 2021-05-05

Added:

- New Layout SelectionBar component

## 3.1.2 - 2021-05-04

- TBD

## 3.1.1 - 2021-05-03

Breaking change:

- FlexLayoutItem props now are optional, spacing and box props removed

Changes:

- medium amd big Button min-width changed

- Added ScrollableContent displayName
- Added GroupHeader displayName
- Added InstructionBlock displayName
- Added Spinner displayName
- Added FlexLayout displayName
- Added Label displayName
- Added AccountInfoHeader displayName
- Added ContentSeparator displayName
- Added flex item props to FlexLayouts
- Added first letter capitalization in ui button
- New property in FlexLayout: matchParentHeight

Issues:

- selfAlign not working in FlexLayoutItem
- Margins removed from Tabs
- Disabled button should not change color on hover

## 3.1.0 - 2021-04-29

Breaking change:

- spacing is not longer following --spacing-[none, s, m, l...] now the spacing will be: --spacing-[0, 8, 16 ...]

Issues:

- flex layout must follow css nomenclature: "..-Alingmnet"· now is ·...-alignment"

## 3.0.18 - 2021-04-27

Added:

- New Layout components like FlexLayout and FlexItemLayout
- New ContentSeparator component
- New TransparentButton component
- New GroupHeader component
- New Label component

Issues:

- Switch should use div instead of label if there is no label
- Add rck prefix to our sass helpers in List component
- Switch not properly trigger onClick event fixed
- Missing styles in TransparentButton fixed

## 3.0.17 - 2021-04-28

Issues:

- Change avatar size at account info header

## 3.0.16 - 2021-04-20

Issues:

- Close icon at portal modal should have data test id

## 3.0.15 - 2021-04-13

Added:

- New lockAlt and arrowCircleRight icons to IconType

## 3.0.12 - 2021-04-08

Added:

- New envelope icon to IconType

## 3.0.13 - 2021-04-8

Added:

- New syncAlt, download, mapMarkerSlask, user and calendarAlt icons to IconType

## 3.0.14 - 2021-04-9

Added:

- new property for compact list items "bottomPadding" to enable extra bottom spacing
- New utils for List: CompactTitle, CompactSubtitle, CompactRightSubtitle, CompactRightTitle and ColoredText

Issues:

- Portal modal classes should be rck prefixed
- Modal children div container should take 100% of the available height
- Add Spinner as exportable

## 3.0.10 - 2021-04-01

Minor changes:

- Makes bigger font size at email AccountInfoHeader

## 3.0.11 - 2021-04-6

Added:

- New componnt Spinner
- New spinner available in Button with loadingUi property
- New makeClickableListItem now available from library
- New ScrollableContent component
- New property in RegularStyleListItem to center middle content "centerMiddleContent"
- New danger status added to Avatar

Removed:

- makeClickableListItem util removed

Issues:

- dom animation hook should have deactivable
- list item spacing properties renamed with listItem prefix to be more self explanatory
- makeClickableListItem removed from list items
- avatar x-small now renders border color and height properly

## 3.0.9 - 2021-04-01

Minor changes:

- AccountInfoHeader should have smaller avatar and bigger height
- Updates font size at Ribbon component
- Aligns buttons at PlanBox component

## 3.0.8 - 2021-03-31

Added:

- new AccountInfoHeader component
- isolate header at SideNavigationMenu

## 3.0.7 - 2021-03-24

Added:

- none or 0 as valid spacing for list items
- Link abstraction to support anchor or redux like Link
- clicable list item support
- configurable number of columns for List component
- New property animateOnChange on Tabs

Issues:

- modal title should have more bottom spacing (from s to m)
- fix missing rck prefix in Avatar, Ribbon and Stepbar component
- fix animation on Tabs
- fix interactive aria properties for Avatar
- normalize bold to semi-bold font size

## 3.0.6 - 2021-03-21

Minor changes:

- Makes bigger clickable area for each item at SideNavigationMenu component

## 3.0.5 - 2021-03-19

- TestId generation for child components
- Added new Icon "fa-rocket"
  Issues:
- Fix button text centered in Safari
- Fix modal line-height text

## 3.0.4 - 2021-03-18

Minor changes:

- Aligns text at SideNavigationMenu component

## 3.0.3 - 2021-03-16

Minor changes:

- Checkbox onClick event adds the value as paramter

## 3.0.2 - 2021-03-15

Added/finished new components:

- InstructionBlock
- Checkbox

## 3.0.1 - 2021-03-12

Minor changes at the following components:

- PortalModal (minHeight property added)
- Modal (minHeight property added and extra margin top for buttons)

## 3.0.0 - 2021-03-02

We added the following components to the library:

- ActionInput
- Badge
- Checkbox (WIP)
- MoreActionsButton
- Dropdown
- Icon
- Menu
- Quote
- RadioButton
- SideNavigationMenu
- Switch
- Tabs

### Breaking Changes:

1. We changed the following components:

   - TextField (API changes)
   - Button
   - Modals (API changes)
   - AlertBox
   - List (API changes)
   - Card (API changes)

2. We changed old css naming convention staff in order to follow `rck-` convention.

3. Also we removed `iconClass` properties from components in order to pass the new `icon` property to support new Icon component

4. Now we have a common interface for all components:

```
    children?: React.ReactNode | string;
    className?: string;
    testId?: string;
```

5. And other for components that should support disabling capabilities:

```
    disabled?: boolean;
```

### Other Changes:

1. Also we created redux adapter for inputs (TextfieldAdapter)

2. We added testId property to all of our components in order to be able to render data-testid attributes

## 2.0.0 - 2020-11-03

### Breaking Changes

We have unified the colors palette. You have to import the new colors variables `src/assets/scss/_rocket-colors.scss` in your global scss file before importing `src/assets/scss/_variables.scss`.

The rest of colors variables will be deprecated in the next version
