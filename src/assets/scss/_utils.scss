@mixin breakpoint($point) {
  /*
  Usage example:

  .c-something {
    padding: 10px;

    @include breakpoint(sm) {
      padding: 15px;
    }
  }
  */

  @if $point == lg {
    @media (min-width: $screen-lg) {
      @content;
    }
  } @else if $point == md {
    @media (min-width: $screen-md) {
      @content;
    }
  } @else if $point == sm {
    @media (min-width: $screen-sm) {
      @content;
    }
  } @else if $point == xs-only {
    @media (max-width: $screen-sm-1) {
      @content;
    }
  }
}

@function rem($pixels, $context: 16px) {
  /*
  Usage example:

  .something {
    font-size: rem(24px);
  }
  */

  @if (unitless($pixels)) {
    $pixels: $pixels * 1px;
  }

  @if (unitless($context)) {
    $context: $context * 1px;
  }

  @return $pixels / $context * 1rem;
}

%clearfix {
  /*
  Usage example:

  .something {
    @extend %clearfix;
  }
  */
  &::after {
    content: '';
    display: table;
    clear: both;
  }
}

@mixin rck-ellipsis($lines: 1) {
  overflow: hidden;
  @if $lines == 1 {
    text-overflow: ellipsis;
    white-space: nowrap;
  } @else {
    -webkit-line-clamp: ($lines);
    display: -webkit-box;
    -webkit-box-orient: vertical;
    white-space: initial;
  }
}

@mixin rck-text-align() {
  &--align-left {
    text-align: left;
  }
  &--align-right {
    text-align: right;
  }
  &--align-center {
    text-align: center;
  }
}

@mixin rck-overlap-element() {
  width: 100%;
  height: 100%;
  position: absolute;
  z-index: 2;
}

@mixin rck-overlaped-element() {
  position: relative;
  z-index: 1;
  opacity: 0.5;
}

@mixin rck-resolution-mobile() {
  @media (max-width: 768px) {
    @content;
  }
}

@mixin rck-word-break() {
  &--word-break-break-all {
    word-break: break-all;
  }
  &--word-break-keep-all {
    word-break: keep-all;
  }
  &--word-break-normal {
    word-break: normal;
  }
}

/// This function provides a flexible way to modify a color palette map
/// by picking or omitting specific color keys or by mapping existing color keys
/// to new values or create new color keys.
///
/// @example:
/// $my-palette: rck-color-palette-map($pick: ('primary', 'secondary'), $mapping: ('primary': ('base': 'light', 'light': #999)));
/// Outputs palette with only primary and secondary color and for primary color
/// changes 'base' value to 'light' and 'light' value to '#999'.
///
/// In mappings, you can create new color scale keys but the value have to be a
/// valid color or another existing scale within the palette color.
/// The nomenclature is the following:
/// $mapping: (([color/s]): ([to], [from], ...), ...)
///
/// All the following are valid:
/// $mapping: ('primary': ('base': 'light'))
/// $mapping: ('primary': ('base': #000))
/// $mapping: ('primary': ('random': 'dark'))
/// $mapping: (('primary', 'secondary', 'error'): ('base': 'light'))
///
/// The following will result in error:
/// $mapping: ('primary': ('base': 'random')) // 'random' is not a color nor a valid color scale.
@function rck-color-palette-map(
  $source-palette: $rck-global-color-palette,
  $pick: (),
  $omit: (),
  $mapping: ()
) {
  $palette: $source-palette;

  // Do not allow simultanious exclusion and inclusion
  @if length($omit) > 0 and length($pick) > 0 {
    @error 'You can only use one filter at a time';
  }

  // Omit keys
  @if length($omit) > 0 {
    $err: map-contains-keys($source-palette, $omit);
    @if ($err != null) {
      @error $err;
    }

    $palette: $source-palette;
    @each $key in $omit {
      $palette: map-remove($palette, $key);
    }
  }

  // Picks keys
  @if length($pick) > 0 {
    $err: map-contains-keys($source-palette, $pick);
    @if ($err != null) {
      @error $err;
    }

    $palette: ();
    @each $key in $pick {
      $value: map-get($source-palette, $key);
      $palette: map-merge(
        $palette,
        (
          $key: $value,
        )
      );
    }
  }

  // Key mappings
  @if length($mapping) > 0 {
    $palette-copy: $palette;
    @each $keys, $value in $mapping {
      // $color-key = 'primary'
      // $value = ('base': 'dark', 'dark': #000)

      // this allows -> ( [key] | ([key1, key2, ...]): value )
      @each $color-key in $keys {
        $err: map-contains-keys($source-palette, $color-key);
        @if ($err != null) {
          @error $err;
        }

        @if type-of($value) != map {
          @error 'Mappings should be a List or a Map';
        }

        @each $swap-pair in $value {
          // $swap-pair  = 'base': 'dark'
          // $swap-pair  = 'dark': #000

          $new-value: nth($swap-pair, 2);
          @if type-of($new-value) != color {
            $new-value: map-deep-get($palette, $color-key, nth($swap-pair, 2));
            @if $new-value == null {
              @error '#{nth($swap-pair , 2)} is not a valid #{$color-key} color scale.';
            }
          }

          $palette-copy: map-merge(
            $palette,
            (
              $color-key:
                map-merge(
                  map-get($palette-copy, $color-key),
                  (
                    // We override the color scale key value if it exists.
                      // If not we append a new one, it accepts any arbitrary key name.
                      nth($swap-pair, 1):
                      $new-value
                  )
                ),
            )
          );
        }
        // swap the overridden palette
        $palette: $palette-copy;
      }
    }
  }

  @return $palette;
}

/// Takes a list of colors as input and checks whether each color exists
/// in a global color palette.
/// The function returns an error message if any color in the input list is
/// not found in the global color palette, and otherwise returns null.
///
/// We cannot use the built-in map-has-key($map, $keys...) because keys param
/// cannot be a tuple of keys.
/// Private: rck-color-palette-map()
@function map-contains-keys($palette, $colors) {
  @each $color in $colors {
    @if (index(map-keys($palette), $color) == null) {
      @return '#{$color} is not a valid color palette key.';
    }
  }
  @return null;
}

/// Utility function that retrieves a nested value from a map/list
/// using a variable number of keys.
///
/// @example
/// $color: ("primary": ("base": #000));
///
/// @debug map-deep-get($color, "primary", "base");  // #000
/// @debug map-deep-get($color, "primary");          // ("base": #000)
@function map-deep-get($map, $keys...) {
  $scope: $map;
  $i: 1;
  @while (type-of($scope) == map) and ($i <= length($keys)) {
    $scope: map-get($scope, nth($keys, $i));
    $i: $i + 1;
  }
  @return $scope;
}
