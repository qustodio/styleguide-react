# Rocket custom icons

Set of custom icons for Rocket.
The icons have been generated with [Icomoon](https://icomoon.io/app/).

## Edit

To edit the icons, load the project in Icomoon with the [`rocket-icons.json`](./rocket-icons.json) file.

You can import it back to the IcoMoon app via `Main Menu` → `Manage Projects` to retrieve your icon selection.

After do some changes, press Download and replace the content of the [`rocket-icons.json`](./rocket-icons.json) file by the `selection.json` content included in the downloaded zip file (the file will be automatically prettied when pushed).

To have the new icons available, add the icon name into the [types](/src/components/Icons/CustomIcons/RocketCustomIcon.types.ts) file.
