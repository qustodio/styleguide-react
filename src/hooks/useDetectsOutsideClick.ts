import { useState, useEffect, MutableRefObject } from 'react';

const useDetectOutsideClick = <T extends HTMLElement>(
  elements: MutableRefObject<T>[],
  initialState: boolean
): readonly [boolean, (active: boolean) => void, boolean] => {
  const [isActive, setInnerIsActive] = useState(initialState);
  const [prevIsActive, setPrevIsActive] = useState(initialState);

  const setIsActive = (active: boolean): void => {
    if (active !== isActive) {
      setPrevIsActive(isActive);
    }
    setInnerIsActive(active);
  };

  useEffect(() => {
    const pageClickEvent = (e: Event) => {
      if (
        !elements.some(
          el => el.current && el.current.contains(e.target as Node)
        )
      ) {
        setIsActive(false);
      }
    };

    if (isActive) {
      window.addEventListener('click', pageClickEvent);
    }

    return () => {
      window.removeEventListener('click', pageClickEvent);
    };
  }, [isActive, elements]);

  return [isActive, setIsActive, prevIsActive] as const;
};

export default useDetectOutsideClick;
