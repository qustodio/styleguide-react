import { MutableRefObject, Ref, useMemo } from 'react';

type RefType<T> =
  | MutableRefObject<T | null>
  | ((instance: T | null) => void)
  | null
  | undefined;

export const assignRefs = <T>(...refs: RefType<T>[]) => (
  node: T | null
): void => {
  refs.forEach(currRef => {
    if (typeof currRef === 'function') {
      currRef(node);
    } else if (currRef) {
      // eslint-disable-next-line no-param-reassign
      currRef.current = node;
    }
  });
};

const useAssignRef = <T>(
  ...refs: Array<Ref<T> | undefined>
): React.RefCallback<T> | null =>
  /**
   * This will create a new function if the refs passed to this hook change and are all defined.
   * This means react will call the old ref copy with `null` and the new ref copy
   * with the ref. Cleanup naturally emerges from this behavior.
   */
  useMemo(() => {
    if (refs.every(ref => ref == null)) {
      return null;
    }

    return instance => {
      assignRefs(...refs)(instance);
    };
  }, refs);

export default useAssignRef;
