import { EventListenersModifier } from '@popperjs/core/lib/modifiers/eventListeners';

export const ARROW_SIZE_CSS_VAR = '--popper-arrow-size';
export const ARROW_OFFSET_CSS_VAR = '--popper-arrow-offset';
export const ARROW_SHADOW_CSS_VAR = '--popper-arrow-shadow-color';
export const ARROW_BG_CSS_VAR = '--popper-arrow-bg';

export const DATA_POPPER_ARROW_ATTR = 'data-popper-arrow';
export const DATA_POPPER_HIDDEN_ATTR = 'data-popper-reference-hidden';

const popperEventListeners: EventListenersModifier['options'] = {
  scroll: true,
  resize: true,
};

export const getEventListenersModifier = (
  value?: boolean | EventListenersModifier['options']
): Partial<EventListenersModifier> => {
  if (typeof value === 'object') {
    return {
      enabled: true,
      options: { ...popperEventListeners, ...value },
    };
  }
  return {
    enabled: value,
    options: popperEventListeners,
  };
};

export const getArrowStyle = (props: {
  size: number;
  shadowColor: string;
  bg: string;
  style: Record<string, unknown>;
}): Record<string, unknown> => {
  const { size = '12px', shadowColor, bg, style } = props;
  const computedStyle: Record<string, unknown> = {
    ...style,
    position: 'absolute',
  };
  if (size) {
    computedStyle[ARROW_SIZE_CSS_VAR] = size;
  }
  if (shadowColor) {
    computedStyle[ARROW_SHADOW_CSS_VAR] = shadowColor;
  }
  if (bg) {
    computedStyle[ARROW_BG_CSS_VAR] = bg;
  }
  return computedStyle;
};

export const toVar = (cssVar: string, defaultValue?: string): string =>
  !defaultValue ? `var(${cssVar})` : `var(${cssVar}, ${defaultValue})`;
