import React from 'react';
import { Instance, VirtualElement, createPopper } from '@popperjs/core';
import useAssignRef from '../useAssignRef';
import {
  DATA_POPPER_ARROW_ATTR,
  getArrowStyle,
  getEventListenersModifier,
} from './helpers';
import { hidePopper, matchToTriggerWidth, positionArrow } from './modifiers';
import { PopoverProps } from './types/PopoverProps.types';

const usePopover = (props: PopoverProps = {}) => {
  const {
    enabled = true,
    modifiers,
    placement = 'top',
    strategy = 'absolute',
    arrow = true,
    arrowPadding = 8,
    eventListeners = true,
    offset,
    gutter = 8,
    flip = true,
    boundary = 'clippingParents',
    preventOverflow = true,
    clipOverflow = true,
    matchWidth,
  } = props;

  const targetRef = React.useRef<Element | VirtualElement | null>(null);
  const popperRef = React.useRef<HTMLElement | null>(null);
  const popperInstance = React.useRef<Instance | null>(null);

  const cleanup = React.useRef(() => {});

  const initializePopper = React.useCallback(() => {
    if (!enabled || !targetRef.current || !popperRef.current) return;

    cleanup.current?.(); // clear previous popper instance

    popperInstance.current = createPopper(
      targetRef.current,
      popperRef.current,
      {
        placement,
        strategy,
        modifiers: [
          positionArrow,
          hidePopper,
          {
            ...matchToTriggerWidth,
            enabled: Boolean(matchWidth),
          },
          {
            name: 'eventListeners',
            ...getEventListenersModifier(eventListeners),
          },
          {
            name: 'arrow',
            enabled: Boolean(arrow),
            options: { padding: arrowPadding },
          },
          {
            name: 'offset',
            options: {
              offset: offset ?? [0, gutter],
            },
          },
          {
            name: 'flip',
            enabled: Boolean(flip),
            options: { padding: 8 },
          },
          {
            name: 'preventOverflow',
            enabled: Boolean(preventOverflow),
            options: {
              boundary,
              altBoundary: true,
              mainAxis: true,
              altAxis: true,
            },
          },
          {
            name: 'hide',
            enabled: Boolean(clipOverflow),
          },
          ...(modifiers ?? []),
        ],
      }
    );

    // force update one-time to fix any positioning issues
    popperInstance.current.forceUpdate();

    cleanup.current = popperInstance.current.destroy;
  }, [
    placement,
    enabled,
    modifiers,
    matchWidth,
    eventListeners,
    arrow,
    arrowPadding,
    offset,
    gutter,
    flip,
    preventOverflow,
    boundary,
    strategy,
    clipOverflow,
  ]);

  // onUnmount
  React.useEffect(
    () => () => {
      if (!targetRef.current && !popperRef.current) {
        popperInstance.current?.destroy();
        popperInstance.current = null;
      }
    },
    []
  );

  const targetRefSetter = React.useCallback(
    <T extends Element | VirtualElement>(node: T | null) => {
      targetRef.current = node;
      initializePopper();
    },
    [initializePopper]
  );

  const popperRefSetter = React.useCallback(
    <T extends HTMLElement>(node: T | null) => {
      popperRef.current = node;
      initializePopper();
    },
    [initializePopper]
  );

  const getReferenceProps = React.useCallback(
    (refProps = {}, ref = null) => ({
      ...refProps,
      ref: useAssignRef(targetRef, ref),
    }),
    [targetRef]
  );

  const getPopperProps = React.useCallback(
    (popperProps = {}, ref = null) => ({
      ...popperProps,
      ref: useAssignRef(popperRef, ref),
    }),
    [popperRef]
  );

  const getArrowProps = React.useCallback((arrowProps = {}, ref = null) => {
    const { size, shadowColor, bg, style, ...rest } = arrowProps;
    return {
      ...rest,
      ref,
      [DATA_POPPER_ARROW_ATTR]: '',
      style: getArrowStyle(arrowProps),
    };
  }, []);

  return {
    update() {
      popperInstance.current?.update();
    },
    forceUpdate() {
      popperInstance.current?.forceUpdate();
    },
    instance: popperInstance.current,
    targetRef: targetRefSetter,
    popperRef: popperRefSetter,
    getReferenceProps,
    getPopperProps,
    getArrowProps,
  };
};

export default usePopover;
