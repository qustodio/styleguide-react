/**
 * Extracts ARIA attributes from a props object.
 *
 * @example
 * const ariaProps = useAriaProps({
 *   'aria-label': 'test',
 *   'aria-describedby': 'description',
 *   className: 'text-red' // Will be filtered out
 * });
 * // Returns { 'aria-label': 'test', 'aria-describedby': 'description' }
 */
const useAriaProps = <T extends Record<string, unknown>>(
  props: T
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Record<keyof T, any> =>
  Object.entries(props).reduce((acc, [key, value]) => {
    if (key.startsWith('aria-') && value != null) {
      acc[key as keyof T] = value as T[keyof T];
    }
    return acc;
  }, {} as T);

export default useAriaProps;
