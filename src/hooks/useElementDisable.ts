import { useEffect, MutableRefObject } from 'react';

const useElementDisable = <T extends HTMLElement>(
  el: MutableRefObject<T>,
  disable: boolean,
  disableClass: string,
  overlapClass: string
): void => {
  const createOverlap = () => {
    const div = document.createElement('div');
    div.className = overlapClass;
    return div;
  };

  useEffect(() => {
    if (el && el.current) {
      if (disable && !el.current.classList.contains(disableClass)) {
        el.current.classList.add(disableClass);
        if (el.current.prepend) {
          el.current.prepend(createOverlap());
        }
      }
      if (!disable && el.current.classList.contains(disableClass)) {
        el.current.classList.remove(disableClass);
        const overlap = el.current.querySelector(`.${overlapClass}`);
        if (overlap) {
          el.current.removeChild(overlap);
        }
      }
    }
  }, [disable]);
};

export default useElementDisable;
