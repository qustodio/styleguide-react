import { useState, useEffect, MutableRefObject } from 'react';

export enum FloatingMenuPlacement {
  top = 'top',
  bottom = 'bottom',
}

const useFloatingPlacement = <T extends HTMLElement>(
  el: MutableRefObject<T>,
  initialPlacement: FloatingMenuPlacement,
  isActive: boolean,
  fixed?: boolean
): readonly [FloatingMenuPlacement, () => void] => {
  const [placement, setPlacement] = useState(initialPlacement);

  const updatePlacement = (newPlacement?: FloatingMenuPlacement) => {
    if (el.current && isActive) {
      if (fixed && initialPlacement) {
        setPlacement(initialPlacement);
        return;
      }

      const dimensions = el.current.getBoundingClientRect();
      const totalHeight = window.innerHeight;

      let hasAvailableTopSpace = false;
      let hasAvailableBottomSpace = false;

      if (placement === FloatingMenuPlacement.bottom) {
        hasAvailableTopSpace = dimensions.top - dimensions.height > 0;
        hasAvailableBottomSpace =
          dimensions.top + dimensions.height < totalHeight;
      } else {
        hasAvailableTopSpace = dimensions.bottom - dimensions.height > 0;
        hasAvailableBottomSpace =
          dimensions.bottom + dimensions.height < totalHeight;
      }

      if (newPlacement && hasAvailableBottomSpace && hasAvailableTopSpace) {
        setPlacement(newPlacement);
      } else if (!hasAvailableBottomSpace && !hasAvailableTopSpace) {
        let availableSpaceTop = 0;
        let availableSpaceBottom = 0;

        if (placement === FloatingMenuPlacement.bottom) {
          availableSpaceTop = dimensions.top;
          availableSpaceBottom = totalHeight - dimensions.top;
        } else {
          availableSpaceTop = dimensions.bottom;
          availableSpaceBottom = totalHeight - dimensions.bottom;
        }

        if (
          availableSpaceBottom > availableSpaceTop &&
          placement !== FloatingMenuPlacement.bottom
        ) {
          setPlacement(FloatingMenuPlacement.bottom);
        } else if (
          availableSpaceTop > availableSpaceBottom &&
          placement !== FloatingMenuPlacement.top
        ) {
          setPlacement(FloatingMenuPlacement.top);
        }
      } else if (
        placement === FloatingMenuPlacement.bottom &&
        !hasAvailableBottomSpace
      ) {
        setPlacement(FloatingMenuPlacement.top);
      } else if (
        placement === FloatingMenuPlacement.top &&
        !hasAvailableTopSpace
      ) {
        setPlacement(FloatingMenuPlacement.bottom);
      }
    }
  };

  const updateAfterScroll = () => {
    updatePlacement();
  };

  useEffect(() => {
    document.addEventListener('scroll', updateAfterScroll);
    updatePlacement();
    return () => {
      document.removeEventListener('scroll', updateAfterScroll);
    };
  });

  useEffect(() => {
    updatePlacement(initialPlacement);
  }, [initialPlacement]);

  useEffect(() => {
    updatePlacement();
  }, [isActive]);

  return [placement, updatePlacement] as const;
};

export default useFloatingPlacement;
