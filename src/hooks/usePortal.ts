import { useEffect, useRef } from 'react';
import { isTestEnv } from '../helpers';

const usePortal = (id: string, useSingleHook?: boolean): HTMLElement => {
  const rootElemRef = useRef<HTMLElement | null>(null);
  const uniqueId = 'portal-container';

  const getRootElem = () => {
    if (!rootElemRef.current) {
      rootElemRef.current = document.createElement('div');

      if (useSingleHook) {
        rootElemRef.current.setAttribute('name', uniqueId);
      }
    }
    return rootElemRef.current;
  };

  useEffect(() => {
    const parentElem = document.querySelector(`#${id}`);

    if (!parentElem && !isTestEnv)
      throw new Error('usePortal: Element to append for portal is invalid');

    const container = parentElem?.querySelector(`[name="${uniqueId}"]`);

    if (!useSingleHook || (useSingleHook && !container)) {
      parentElem?.appendChild(getRootElem());
    }

    if (useSingleHook && !rootElemRef.current?.parentElement) {
      rootElemRef.current = container as HTMLElement;
    }

    return () => {
      if (!useSingleHook) {
        getRootElem().remove();
      }
    };
  }, [id]);

  return getRootElem();
};

export default usePortal;
