import { useState, MutableRefObject, useLayoutEffect } from 'react';
import { DropdownElementOrientation } from '../components/Dropdown/types';
import { FloatingMenuPlacement } from './useFloatingPlacement';

const useAttachedDomPosition = <T extends HTMLElement>(
  el: MutableRefObject<T>,
  elToAttach: MutableRefObject<HTMLElement>,
  placement: FloatingMenuPlacement,
  actionElementPosition: DropdownElementOrientation,
  marginFromAttached?: number,
  initialTop?: number,
  initialLeft?: number
): readonly [React.CSSProperties, () => void] => {
  const defaultPosition = { top: 0, left: 0 };
  // top already has margin: 8px;
  const defaultMarginTop = -8;

  const [actionElementDOMPosition, setActionElementDOMPos] = useState<
    React.CSSProperties
  >({
    top: initialTop || defaultPosition.top,
    left: initialLeft || defaultPosition.left,
  });

  const getLeft = (
    left: number,
    right: number,
    widthActionEl: number
  ): number => {
    if (actionElementPosition === DropdownElementOrientation.center) {
      const {
        width: widthAttachedEl,
      } = elToAttach.current.getBoundingClientRect();
      const offset = Math.abs((widthAttachedEl - widthActionEl) / 2);
      return left - offset + window.scrollX;
    }
    return (
      (actionElementPosition === DropdownElementOrientation.right
        ? left
        : right) + window.scrollX
    );
  };

  const updateActionElementDOMPosition = (): void => {
    if (el && el.current && elToAttach && elToAttach.current) {
      const {
        bottom,
        right,
        top,
        left,
        width: widthActionEl,
      } = el.current.getBoundingClientRect();

      const updatedDOMPos = {
        top:
          (placement === FloatingMenuPlacement.bottom
            ? bottom - (marginFromAttached || defaultMarginTop)
            : top + (marginFromAttached || defaultMarginTop)) + window.scrollY,
        left: getLeft(left, right, widthActionEl),
      };

      setActionElementDOMPos({
        top: updatedDOMPos.top,
        left: updatedDOMPos.left,
      });
    }
  };

  useLayoutEffect(() => {
    window.addEventListener('resize', updateActionElementDOMPosition);
    window.addEventListener('scroll', updateActionElementDOMPosition, true);
    updateActionElementDOMPosition();
    return () => {
      window.removeEventListener('resize', updateActionElementDOMPosition);
      window.removeEventListener('scroll', updateActionElementDOMPosition);
    };
  }, [placement, actionElementPosition, elToAttach.current]);

  return [actionElementDOMPosition, updateActionElementDOMPosition] as const;
};

export default useAttachedDomPosition;
