import * as React from 'react';


const useToggle = (initial: boolean): [
  boolean, 
  { toggle: () => void, open: () => void, close: () => void }
] => {
  const [isOpen, setIsOpen] = React.useState<boolean>(initial);

  return [
    isOpen,
    {
      toggle: () => setIsOpen(s => !s),
      open: () => setIsOpen(true),
      close: () => setIsOpen(false),
    }
  ];
};

export default useToggle;
