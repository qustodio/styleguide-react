import { useEffect, MutableRefObject } from 'react';

const useDomAnimation = <T extends HTMLElement>(
  el: MutableRefObject<T>,
  animationClass: string,
  milliseconds = 200,
  trigger: unknown[] | undefined = undefined,
  disabled = false
): void => {
  useEffect(() => {
    if (el && el.current) {
      if (!disabled && !el.current.classList.contains(animationClass)) {
        el.current.classList.add(animationClass);
        setTimeout(() => {
          if (el && el.current) {
            el.current.classList.remove(animationClass);
          }
        }, milliseconds);
      }
    }
  }, trigger);
};

export default useDomAnimation;
