import * as React from 'react';
import TextField, {
  InputIconConfiguration,
  SupportTextConfiguration,
  InputType,
} from '../../../components/TextField/TextField';
import { Input, Meta } from './types';

const TextFieldAdapter: React.FunctionComponent<{
  label?: string;
  className?: string;
  disabled?: boolean;
  block?: boolean;
  type: InputType;
  error?: boolean;
  supportTextConfiguration?: SupportTextConfiguration;
  placeholder?: string;
  iconConfiguration?: InputIconConfiguration;
  autoComplete?: string;
  autoCorrect?: string;
  autoCapitalize?: string;
  spellCheck?: boolean;
  maxlength?: number;
  minlength?: number;
  required?: boolean;
  min?: number;
  max?: number;
  pattern?: string;
  defaultValue?: string;
  canBeCleared?: boolean;
  showAlertOnError?: boolean;
  iconLeft?: JSX.Element;
  meta: Meta;
  input: Input;
}> = ({
  label,
  className,
  disabled,
  block,
  type,
  error,
  supportTextConfiguration,
  placeholder,
  autoComplete,
  autoCorrect,
  autoCapitalize,
  spellCheck,
  maxlength,
  minlength,
  required,
  min,
  max,
  pattern,
  defaultValue,
  canBeCleared,
  showAlertOnError,
  iconLeft,
  meta,
  input,
}) => (
  <TextField
    label={label}
    className={className}
    disabled={disabled}
    block={block}
    type={type}
    error={(!!meta?.error && meta?.touched) || error}
    supportTextConfiguration={
      meta?.error
        ? { text: meta?.error, ...supportTextConfiguration }
        : supportTextConfiguration
    }
    placeholder={placeholder}
    autoComplete={autoComplete}
    autoCorrect={autoCorrect}
    autoCapitalize={autoCapitalize}
    spellCheck={spellCheck}
    maxlength={maxlength}
    minlength={minlength}
    required={required}
    min={min}
    max={max}
    pattern={pattern}
    defaultValue={defaultValue}
    canBeCleared={canBeCleared}
    showAlertOnError={showAlertOnError}
    iconLeft={iconLeft}
    {...input}
  />
);

TextFieldAdapter.displayName = 'TextFieldAdapter';

export default TextFieldAdapter;
