import { SyntheticEvent } from 'react';

export interface AnchorPropTypes {
  to?: string;
  className?: string;
  onClick?: (ev: SyntheticEvent) => void;
}
