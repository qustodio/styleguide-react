import { TextWordBreak } from './types';

// eslint-disable-next-line import/prefer-default-export
export const getWordBreakClass = (
  prefix: string,
  type?: TextWordBreak
): string => (type ? `${prefix}--word-break-${type}` : '');
