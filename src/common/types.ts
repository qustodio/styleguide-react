import { AriaRole, TabIndex } from './aria.types';

export enum GlobalType {
  default = 'default',
  primary = 'primary',
  secondary = 'secondary',
  gray = 'gray',
  success = 'success',
  warning = 'warning',
  error = 'error',
  white = 'white',
  booster = 'booster',
  school = 'school',
  marketing = 'marketing',
  decorationGreen = 'decoration-green',
  decorationBlue = 'decoration-blue',
  decorationOrange = 'decoration-orange',
}

export enum RoutineColor {
  yellow = 'yellow',
  red = 'red',
  purple = 'purple',
  pink = 'pink',
  blue = 'blue',
  green = 'green',
  marine = 'marine',
}

export enum TextAlign {
  notSet = 'not-set',
  left = 'left',
  right = 'right',
  center = 'center',
}

export interface TestableNestedCompomentProps {
  parentTestId?: string;
}

export interface TestableComponentProps {
  testId?: string;
}

export interface BaseComponentProps extends TestableComponentProps {
  id?: string;
  role?: AriaRole;
  tabIndex?: TabIndex;
  children?: React.ReactNode | string;
  className?: string;
}

export interface DisabledProps {
  disabled?: boolean;
}

export type TextWordBreak = 'break-all' | 'keep-all' | 'normal';

export type ValueOf<T> = T[keyof T];

export interface ImageLikeComponentProps {
  src?: string;
  alt?: string;
  srcSet?: string;
  imgSizes?: string;
  imgLoading?: 'eager' | 'lazy';
  imgReferrerPolicy?:
    | 'no-referrer'
    | 'no-referrer-when-downgrade'
    | 'origin'
    | 'origin-when-cross-origin'
    | 'same-origin'
    | 'strict-origin'
    | 'strict-origin-when-cross-origin'
    | 'unsafe-url';
}
