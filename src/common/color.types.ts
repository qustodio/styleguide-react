export type RckColorPalette = 'base' | 'brand' | 'semantic';

export type RckBaseColor =
  | 'white'
  | 'black'
  | 'grey'
  | 'violet'
  | 'mint'
  | 'cherry'
  | 'magenta'
  | 'peach'
  | 'yolk'
  | 'yellow'
  | 'red'
  | 'purple'
  | 'pink'
  | 'blue'
  | 'green'
  | 'marine';

export type RckSemanticColor = 'success' | 'error' | 'booster' | 'warning';

export type RckBrandColor = 'neutral' | 'primary';

export type RckColor = RckBaseColor | RckSemanticColor | RckBrandColor;
