export type RckBaseGrandient =
  | 'yolk'
  | 'yellow'
  | 'orange'
  | 'red'
  | 'pink'
  | 'purple'
  | 'brand'
  | 'marine'
  | 'blue'
  | 'green';
