import React from 'react';
import Button, {
  ButtonType,
  ButtonSize,
  ButtonColor,
  ButtonIconPosition,
} from '../../components/Button/Button';
import Prop, { PropSchema } from './Props';

const supportedComponents = {
  Button: {
    size: PropSchema('array', ButtonSize),
    iconPosition: PropSchema('array', ButtonIconPosition),
    loading: PropSchema('boolean'),
    buttonType: PropSchema('array', ButtonType),
    color: PropSchema('array', ButtonColor),
    disabled: PropSchema('boolean'),
    block: PropSchema('boolean'),
    centered: PropSchema('boolean'),
  },
};

const Component = name => {
  const getName = () => name;

  const isSupported = () => name in supportedComponents;

  const hasProp = prop =>
    isSupported() ? prop in supportedComponents[name] : false;

  const getPropSchema = prop =>
    hasProp(prop) ? supportedComponents[name][prop] : false;

  const getProp = (prop, valuesProvided = [], optionsProvided = {}) => {
    if (!hasProp(prop)) {
      throw new Error(`Property ${prop} not supported in Component ${name}.`);
    }
    const propSchema = getPropSchema(prop);
    return Prop(
      prop,
      propSchema.getType(),
      propSchema.getSupportedValues(),
      valuesProvided,
      optionsProvided
    );
  };

  const render = (props, content) => {
    if (isSupported()) {
      switch (name) {
        case 'Button':
          return React.createElement(Button, props, content);
      }
    } else {
      throw new Error(`Component ${name} not supported.`);
    }
  };

  return {
    getName,
    isSupported,
    getProp,
    render,
  };
};

export default Component;
