const Combination = props => {
  const propData = [];

  const createNew = () => {
    props.forEach(prop =>
      propData.push({
        name: prop.getName(),
        value: prop.getValues()[0],
      })
    );
    return propData;
  };

  /**
   * Get index of the next property to change.
   *
   * The next property to change is the last property that doesn't have the
   * last possible value. If we don't find any property that is not in its
   * last possible value, it means we have completely iterated over all
   * properties and values, and already have all possible combinations. It
   * will return false and the loop will stop.
   *
   * @return int|false Index of the next property to change. False if there
   * 				   are no properties left to change.
   */
  const getActiveProp = () => {
    let activePropIndex;

    const activePropFound = [...props]
      .slice()
      .reverse()
      .some((prop, reverseIndex) => {
        const trueIndex = props.length - 1 - reverseIndex;
        const propValues = prop.getValues();
        const lastValue = propValues[propValues.length - 1];
        const currentValue = propData[trueIndex].value;
        if (lastValue !== currentValue) {
          // A prop with values left to iterate has been found.
          activePropIndex = trueIndex;
          return true; // Stop searching.
        }
        return false; // Keep searching.
      }); // False only if all props have no values left to iterate.

    return activePropFound ? activePropIndex : false;
  };

  /**
   * Advance a property to its next value.
   *
   * @param int activePropIndex Index of the property to change.
   */
  const setNextValue = activePropIndex => {
    const currentValue = propData[activePropIndex].value;
    const propValues = props[activePropIndex].getValues();
    const currentValueIndex = propValues.indexOf(currentValue);
    propData[activePropIndex].value = propValues[currentValueIndex + 1];
  };

  /**
   * Reset all properties that come after the one we changed.
   *
   * @param int activePropIndex Index of the last property we changed.
   */
  const resetPropsAfter = activePropIndex => {
    let indexPropAfter = activePropIndex + 1;
    for (indexPropAfter; indexPropAfter <= props.length - 1; indexPropAfter++) {
      const firstValue = props[indexPropAfter].getValues()[0];
      propData[indexPropAfter].value = firstValue;
    }
  };

  /**
   * Clone an existing combination, and advance it to the next position.
   *
   * @param array combination Combination to use as a base.
   * @return array|false	Next combination to the one provided, or false if
   * 						there are no combinations left after the one
   * 						provided.
   */
  const createFrom = combination => {
    combination.forEach(values =>
      propData.push({
        name: values.name,
        value: values.value,
      })
    );
    const activePropIndex = getActiveProp();
    if (activePropIndex !== false) {
      setNextValue(activePropIndex);
      resetPropsAfter(activePropIndex);
      return propData;
    }
    return false;
  };

  return {
    createNew,
    createFrom,
  };
};

const CombinationList = () => {
  const combinations = [];

  const isEmpty = () => combinations.length === 0;

  const addCombination = combination => combinations.push(combination);

  const getLastCombination = () =>
    !isEmpty() ? combinations[combinations.length - 1] : false;

  const getCombinations = () => combinations;

  return {
    isEmpty,
    addCombination,
    getLastCombination,
    getCombinations,
  };
};

const generateCombinations = props => {
  const combinationList = CombinationList();
  let combinationsLeft = true;

  while (combinationsLeft) {
    const combination = Combination(props);

    if (combinationList.isEmpty()) {
      const combinationData = combination.createNew();
      combinationList.addCombination(combinationData);
    } else {
      const lastCombination = combinationList.getLastCombination();
      const combinationData = combination.createFrom(lastCombination);
      if (combinationData === false) {
        combinationsLeft = false;
        break;
      } else {
        combinationList.addCombination(combinationData);
      }
    }
  }
  return combinationList.getCombinations();
};

const getCombinationOrder = props => {
  // Combine props separately if they have the dontCombine option.
  const order = {
    first: [],
    middle: [],
    last: [],
  };

  props.forEach(prop => {
    if (prop.getOption('dontCombine')) {
      if (order.middle.length === 0) {
        order.first.push(prop);
      } else {
        order.last.push(prop);
      }
    } else {
      order.middle.push(prop);
    }
  });
  return order;
};

const flattenCombinations = combinations => {
  const flatCombinations = combinations.map(combination => {
    const flatValues = {};
    combination.forEach(values => {
      flatValues[values.name] = values.value;
    });
    return flatValues;
  });
  return flatCombinations;
};

const CombineProps = props => {
  const combinations = [];

  const combinationOrder = getCombinationOrder(props);

  combinations.push(...generateCombinations(combinationOrder.middle));

  combinationOrder.first.forEach(prop => {
    combinations.shift(...generateCombinations([prop]));
  });

  combinationOrder.last.forEach(prop => {
    combinations.push(...generateCombinations([prop]));
  });

  return flattenCombinations(combinations);
};

export default CombineProps;
