import { toArray, sanitizeArray } from './DataHelpers';
import { OptionGroup } from './Options';

const propTypes = ['boolean', 'array'];

const PropSchema = (type, values = null) => {
  if (!propTypes.includes(type)) {
    throw new Error(`Property type ${type} not supported.`);
  }

  const getType = () => type;

  const getSupportedValues = () => {
    switch (type) {
      case 'array':
        const arrayValues = toArray(values);
        return arrayValues.length > 0 ? arrayValues : false;
      case 'boolean':
        return [false, true];
    }
    return false;
  };

  return {
    getType,
    getSupportedValues,
  };
};

const Prop = (
  name,
  type,
  valuesSupported = [],
  valuesProvided = [],
  optionsProvided = {}
) => {
  const options = OptionGroup(optionsProvided, type);

  const applyOptions = values => {
    switch (type) {
      case 'boolean':
        if (options.getOption('trueOnly') === true) {
          return [true];
        } else if (options.getOption('falseOnly') === true) {
          return [false];
        } else if (options.getOption('trueFirst') === true) {
          return [true, false];
        }
        break;

      case 'array':
        if (options.hasOption('excludeValues')) {
          const toExclude = toArray(options.getOption('excludeValues'));
          values = values.filter(value => !toExclude.includes(value));
        }
        if (options.hasOption('addValueBefore')) {
          values.unshift(options.getOption('addValueBefore'));
        }
        if (options.hasOption('addValueAfter')) {
          values.push(options.getOption('addValueAfter'));
        }
        break;
    }
    return values;
  };

  const sanitizeValues = values =>
    sanitizeArray(values, toArray(valuesSupported), true);

  const values = applyOptions(sanitizeValues(valuesProvided));

  if (values.length === 0) {
    throw new Error(`No value assigned to Property ${name}.`);
  }

  const getName = () => name;
  const getValues = () => values;

  return {
    hasOption: options.hasOption,
    getOption: options.getOption,
    getName,
    getValues,
  };
};

export { propTypes, PropSchema, Prop as default };
