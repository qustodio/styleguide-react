type PropValue = string | number | boolean | JSX.Element;

interface PropConfigBase {
  name: string;
}

interface PropConfigSingleValue {
  value: PropValue;
}

interface PropConfigMultipleValues {
  values: PropValue[];
}

export type PropConfig = PropConfigBase &
  (PropConfigSingleValue | PropConfigMultipleValues);
