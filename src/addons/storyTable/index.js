/**
 * @todo Redo in TypeScript.
 * @todo Config option add headers to the table's rows/columns.
 * @todo Get components, properties and property types automatically.
 * @todo Test behavior accepting arguments as strings, array or objects.
 * @todo Test behavior without setting props or cols or rows.
 * @todo Add styles to table/row/col at option instead of on filter.
 * @todo Add title and description as table options.
 */
import React from 'react';
import {
  objetifyArguments,
  simplifyValue,
  arrayContainsObjects,
} from './DataHelpers';
import { OptionGroup } from './Options';
import Component from './Components';
import CombineProps from './PropCombination';
import './styles.css';

const StoryTable = (componentName, optionsProvided = {}) => {
  const component = Component(componentName);
  const options = OptionGroup(optionsProvided);
  const props = {
    all: [],
    rows: [],
    cols: [],
  };

  const addProp = (
    positionInTable,
    propName,
    valuesProvided = null,
    optionsProvided = {}
  ) => {
    const prop = component.getProp(propName, valuesProvided, optionsProvided);
    props[positionInTable].push(prop);
  };

  const parseProps = (
    positionInTable = 'all',
    propsProvided = {},
    depth = 0
  ) => {
    // This extra check allows adding the same prop to different
    // rows/columns using this nomenclature.
    // [{key1: value1},{key2: value2}]
    if (depth === 0 && arrayContainsObjects(propsProvided)) {
      depth++;
      propsProvided.forEach(propProvided => {
        parseProps(positionInTable, propProvided, depth);
      });
    } else {
      const args = objetifyArguments(propsProvided);
      for (let propName in args) {
        if (positionInTable === 'all') {
          // Cant specify more than 1 value in fixed property.
          const propValues = simplifyValue(args[propName]);
          addProp(positionInTable, propName, propValues, {});
        } else {
          const propOptions = args[propName];
          addProp(positionInTable, propName, null, propOptions);
        }
      }
    }
  };

  const addProps = (propsProvided = {}) => {
    parseProps('all', propsProvided);
  };

  const addCols = (propsProvided = {}) => {
    parseProps('cols', propsProvided);
  };

  const addRows = (propsProvided = {}) => {
    parseProps('rows', propsProvided);
  };

  const getKeyPrefix = () => {
    return options.hasOption('key') ? `${options.getOption('key')}-row-` : '';
  };

  const renderRow = (rowIndex, elementProps, content) => {
    let rowProps = {
      className: 'rck-storytable__row',
    };
    if (options.hasOption('key')) {
      rowProps.key = `${getKeyPrefix()}${rowIndex}`;
    }
    if (options.hasOption('filterRowProps')) {
      rowProps = options.getOption('filterRowProps')(rowProps, elementProps);
    }
    return React.createElement('tr', rowProps, content);
  };

  const renderCol = (rowIndex, colIndex, elementProps, content) => {
    let colProps = {
      className: 'rck-storytable__col',
    };
    if (options.hasOption('key')) {
      colProps.key = `${getKeyPrefix()}${rowIndex}-col-${colIndex}-wrapper`;
    }
    if (options.hasOption('filterColProps')) {
      colProps = options.getOption('filterColProps')(colProps, elementProps);
    }
    return React.createElement('td', colProps, content);
  };

  const getElementContent = elementProps => {
    let content = options.hasOption('content')
      ? options.getOption('content')
      : (content = '');

    if (options.hasOption('filterContent')) {
      content = options.getOption('filterContent')(content, elementProps);
    }
    return content;
  };

  const renderComponent = (rowIndex, colIndex, elementProps) => {
    if (options.hasOption('key')) {
      elementProps.key = `${getKeyPrefix()}${rowIndex}-col-${colIndex}`;
    }
    if (options.hasOption('filterProps')) {
      elementProps = options.getOption('filterProps')(elementProps);
    }
    const content = getElementContent(elementProps);
    return component.render(elementProps, content);
  };

  const render = () => {
    const table = [];
    const tableData = {
      all: CombineProps(props.all),
      rows: CombineProps(props.rows),
      cols: CombineProps(props.cols),
    };

    tableData.rows.forEach((rowProps, rowIndex) => {
      const rowContent = [];
      tableData.cols.forEach((colProps, colIndex) => {
        let elementProps = {
          ...tableData.all[0],
          ...rowProps,
          ...colProps,
        };

        rowContent.push(
          renderCol(rowIndex, colIndex, colProps, [
            renderComponent(rowIndex, colIndex, elementProps),
          ])
        );
      });
      table.push(renderRow(rowIndex, rowProps, rowContent));
    });

    let tableProps = {
      className: 'rck-storytable',
    };
    if (options.hasOption('filterTableProps')) {
      tableProps = options.getOption('filterTableProps')(tableProps);
    }
    return React.createElement('table', tableProps, <tbody>{table}</tbody>);
  };

  return {
    addProps,
    addCols,
    addRows,
    render,
  };
};

export default StoryTable;
