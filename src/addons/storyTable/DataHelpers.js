export const isString = value => typeof value === 'string';

export const isNumber = value => typeof value === 'number';

export const isFunction = value => typeof value === 'function';

export const isObject = value =>
  typeof value === 'object' && !Array.isArray(value) && value !== null;

export const toBoolean = value => (value ? true : false);

/**
 * Normalizes a value's type to be handled as an array.
 *
 * [value1, value2] = [value1, value2]
 * {key1: value1, key2: value2} = [value1, value2]
 * string/int/true/function = [string/int/true]
 * undefined/null/false/[]/{} = []
 * function = [function]
 */
export const toArray = value => {
  if (Array.isArray(value)) {
    return value;
  }
  if (isObject(value)) {
    return Object.values(value);
  }
  if (!value) {
    return [];
  }
  return [value];
};

/**
 * Normalizes a value's type to be handled as an object.
 *
 * {key1: value1, key2: value2} = {key1: value1, key2: value2}
 * [value1, value2] = {value1: value1, value2: value2}
 * true = { value: true }
 * string/int = {string/int: string/int}
 * function = {functionName : function}
 * undefined/null/false/[]/{} = {}
 */
export const toObject = value => {
  if (isObject(value)) {
    return value;
  }
  if (Array.isArray(value)) {
    const newObject = {};
    value.forEach(arrayValue => (newObject[arrayValue] = arrayValue));
    return newObject;
  }
  if (isString(value) || isNumber(value)) {
    return { [value]: value };
  }
  if (isFunction(value)) {
    const functionName = value.name;
    return { [functionName]: value };
  }
  return {};
};

export const sanitizeArray = (
  array,
  allowedValues,
  returnAllowedOnEmpty = false
) => {
  const sanitizedArray = toArray(array);
  sanitizedArray.filter(value => allowedValues.includes(value));
  return returnAllowedOnEmpty === true && sanitizedArray.length === 0
    ? allowedValues
    : sanitizedArray;
};

export const getValueType = value => {
  if (isFunction(value)) {
    return 'function';
  } else if (isString(value)) {
    return 'string';
  } else if (isNumber(value)) {
    return 'number';
  } else if (Array.isArray(value)) {
    return 'array';
  } else {
    // If value doesn't match any of these cases, treat it as a boolean
    // by default.
    return 'boolean';
  }
};

export const normalizeValueType = value => {
  switch (getValueType(value)) {
    case 'array':
      return toArray(value);
    case 'boolean':
      return toBoolean(value);
  }
  return value;
};

/**
 * Takes a list of arguments in different formats and turns it to an object.
 *
 * null/undefined/boolean = {}
 * value = {value: value}
 * value1,value2 = {value1: value1, value2: value2}
 * [value1,value2] = {value1: value1, value2: value2}
 * [value1, {key2: value2}] = {value1: value1, key2: value2}
 * {key1: value1, key2: value2} = {key1: value1, key2: value2}
 * [{key1: value1},{key2: value2}] =
 */
export const objetifyArguments = (...args) => {
  const newObject = {};
  args.forEach(arg => {
    let objectifiedArg;
    if (Array.isArray(arg)) {
      objectifiedArg = objetifyArguments(...arg);
    } else if (isObject(arg)) {
      objectifiedArg = arg;
    } else {
      objectifiedArg = toObject(arg);
    }
    Object.assign(newObject, objectifiedArg);
  });
  return newObject;
};

/**
 * Takes an array or object and turns it to a single value.
 */
export const simplifyValue = value => {
  if (isObject(value)) {
    value = toArray(value);
  }
  if (Array.isArray(value)) {
    return value.length > 0 ? value[0] : false;
  }
  if (typeof value === null || typeof value === undefined) {
    return false;
  }
  return value;
};

export const arrayContainsObjects = array => {
  if (Array.isArray(array)) {
    return array.some(arrayItem => isObject(arrayItem));
  }
  return false;
};
