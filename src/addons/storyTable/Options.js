import {
  toObject,
  sanitizeArray,
  getValueType,
  normalizeValueType,
} from './DataHelpers';
import { propTypes } from './Props';

const optionTypes = ['boolean', 'string', 'function', 'array'];

const OptionSchema = (optiontypesToSupport = [], propTypesToSupport = []) => {
  const optiontypesSupported = sanitizeArray(
    optiontypesToSupport,
    optionTypes,
    true
  );

  const propTypesSupported = sanitizeArray(propTypesToSupport, propTypes, true);

  const supportsValueType = value => {
    return optiontypesSupported.includes(getValueType(value));
  };

  const supportsPropType = propType => propTypesSupported.includes(propType);

  return {
    supportsPropType,
    supportsValueType,
  };
};

const tableConfigOptions = {
  // Avoids React warning when rendering multiple elements:
  // "Warning: Each Child in a List Should Have a Unique 'key' Prop".
  key: OptionSchema('string'),
  // Content of the component by default
  content: OptionSchema('string'),
  // Alter the props of a component before render.
  filterProps: OptionSchema('function'),
  // Alter the content of a component before render.
  filterContent: OptionSchema('function'),
  // Alter the props of the table before render.
  filterTableProps: OptionSchema('function'),
  // Alter the props of a row before render.
  filterRowProps: OptionSchema('function'),
  // Alter the props of a column before render.
  filterColProps: OptionSchema('function'),
};

const propConfigOptions = {
  trueOnly: OptionSchema('boolean', 'boolean'),
  falseOnly: OptionSchema('boolean', 'boolean'),
  trueFirst: OptionSchema('boolean', 'boolean'),
  addValueBefore: OptionSchema('string', 'array'),
  addValueAfter: OptionSchema('string', 'array'),
  excludeValues: OptionSchema(['array', 'string', 'boolean'], 'array'),
  // When generating prop combinations, generate all possible variations of
  // this prop alone, but not all possible combinations with others.
  dontCombine: OptionSchema('boolean'),
};

const Option = (name, value, propType = null) => {
  const isPropOption = () => propType && propTypes.includes(propType);

  const getAllOptions = () =>
    isPropOption() ? propConfigOptions : tableConfigOptions;

  const optionExists = () => name in getAllOptions();

  const getOptionSchema = () => getAllOptions()[name];

  const isValid = () =>
    optionExists() &&
    getOptionSchema().supportsValueType(value) &&
    (!isPropOption() || getOptionSchema().supportsPropType(propType));

  const getValue = () => normalizeValueType(value);

  return {
    isValid,
    getValue,
  };
};

const OptionGroup = (optionsProvided = {}, propType = null) => {
  const optionsInGroup = {};

  const parseOptions = optionsProvided => {
    const optionsObject = toObject(optionsProvided);
    for (let name in optionsObject) {
      const optionValue = optionsObject[name];
      const option = Option(name, optionValue, propType);
      if (option.isValid()) {
        optionsInGroup[name] = option;
      }
    }
  };

  parseOptions(optionsProvided);

  const hasOption = option => option in optionsInGroup;

  const getOption = option =>
    hasOption(option) ? optionsInGroup[option].getValue() : false;

  return {
    hasOption,
    getOption,
  };
};

export { Option, OptionGroup };
