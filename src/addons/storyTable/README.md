# StoryTable Addon for Storybook

Given a React component, a list of properties, and a list of possible values per property, generates every possible combination of properties and values for the component. This is used to easily render many variants of a React component.

The component variants are displayed in a table. The list of possible values per property can be iterated either across the x (columns) or y (rows) axis of the table.

## Usage

Import the StoryTable component in the story where you want to display a table with your component variants.

```js
import StoryTable from '../src/addons/storyTable/components/StoryTable';
```

Create a function that returns a JSX Element that uses your component.

```js
const yourComponentFactory = (props, content) => (
 <yourComponent {...props}>
  {content}
 </yourComponent>
);
```

If your component doesn't allow children, you can omit the `content` argument.

```js
const yourComponentFactory = (props) => (
 <yourComponent {...props} />
);
```

Create a list of properties that you want to display in every variant.

```js
const sharedProps = [
 { name: 'align', value: 'center' }
 { name: 'color', value: 'blue' }
];
```

Create a list of properties that you want to display across the y axis of the table (columns).

```js
const colProps = [
 { name: 'size', values: ['small', 'medium', 'big'] }
 { name: 'loading', values: [true, false] }
];
```

Create a list of properties that you want to display across the x axis of the table (rows).

```js
const rowProps = [
 { name: 'type', values: ['solid', 'hollow'] }
];
```

Pass the factory function to the StoryTable component.

```js
<StoryTable
 component={yourComponentFactory}
 content="Click me!"
 all={sharedProps}
 cols={colProps}
 rows={rowProps}
/>
```
