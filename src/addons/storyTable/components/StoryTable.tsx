import React from 'react';
import { StoryTableProps } from '../types/StoryTableProps';

const StoryTable = (props: StoryTableProps) => {
  const {
    component: componentFactory,
    content: componentContent,
    all: sharedProps = [],
    rows: rowProps = [],
    cols: colProps = [],
  } = props;

  return (
    <table className="rck-storytable__table">
      <tbody>
        <tr>
          {/* @todo Generate and render component variants. */}
          <td>{componentFactory({}, componentContent)}</td>
        </tr>
      </tbody>
    </table>
  );
};

export default React.memo(StoryTable);
