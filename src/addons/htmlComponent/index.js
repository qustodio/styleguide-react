import addons, { makeDecorator } from '@storybook/addons';
import pretty from 'pretty';
import ReactDOMServer from 'react-dom/server';

export const withHtmlComponent = makeDecorator({
  name: 'htmlComponent',
  wrapper: (getStory, context) => {
    const story = getStory(context);

    const html = !shouldUpdateHtmlComponentViewFor(context.kind)
      ? 'This component is not supported by this addon'
      : pretty(ReactDOMServer.renderToStaticMarkup(story));

    const channel = addons.getChannel();
    channel.emit('qustodio/html-component/show-html', html);

    return story;
  },
});

const shouldUpdateHtmlComponentViewFor = storyType => {
  const storyTypesThatUseReactPortals = [
    'Modals',
    'Dropdown',
    'Lists',
    'Menu',
    'Drawer',
    'Tooltip',
    'TimePickerInput',
    'ModalsInfo',
    'Accordion',
  ];

  if (storyTypesThatUseReactPortals.includes(storyType)) {
    return false;
  }

  return true;
};
