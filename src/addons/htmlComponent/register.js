import React from 'react';
import addons from '@storybook/addons';
import { AddonPanel, SyntaxHighlighter } from '@storybook/components';
import { ThemeProvider, convert } from '@storybook/theming';
import './styles.css';

class HtmlComponent extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = {
      markup: '',
    };
    this.onShowStaticMarkup = this.onShowStaticMarkup.bind(this);
  }

  onShowStaticMarkup(markup) {
    this.setState({ markup });
  }

  componentDidMount() {
    const { channel } = this.props;
    channel.on('qustodio/html-component/show-html', this.onShowStaticMarkup);
  }

  render() {
    const { markup } = this.state;
    return (
      <div className="html-component__wrapper">
        <ThemeProvider theme={convert()}>
          <SyntaxHighlighter bordered copyable format={false} language="html">
            {markup}
          </SyntaxHighlighter>
        </ThemeProvider>
      </div>
    );
  }
}

addons.register('qustodio/html-component', () => {
  addons.addPanel('qustodio/html-component/panel', {
    title: 'HTML Component',
    render: ({ active, key }) => (
      <AddonPanel key={key} active={active}>
        <HtmlComponent channel={addons.getChannel()} />
      </AddonPanel>
    ),
  });
});
