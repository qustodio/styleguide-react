import themeArtifact from '../artefacts/themeArtifact';

const createRckTheme = <T>(...overrides: T[]): typeof themeArtifact & T =>
  Object.assign({}, ...[themeArtifact, ...overrides]);

export default createRckTheme;
