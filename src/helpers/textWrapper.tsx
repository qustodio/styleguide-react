/**
 * Wraps lines of text (such as those obtained from a Knobs text field) in
 * paragraphs or any other HTML element.
 *
 * This can be used to generate repetitive elements in stories for a more
 * realistic preview of Component usage.
 *
 * Generates unique keys for each line in order to avoid React warnings for
 * identical elements.
 *
 * Usage:
 *
 * - Wrap in paragraphs:
 *   textWrapper('keyPrefix').wrap(myText);
 *
 * - Wrap in other HTML element:
 *   textWrapper('keyPrefix', 'li').wrap(myText);
 *
 * - Wrap in JSX Element:
 *   const myFactoryFunc = (key, line) => (
 *      <MyComponent key={key} otherProp="prop">{line}</MyComponent>
 *   );
 *   textWrapper('keyPrefix', myFactoryFunc).wrap(myText);
 */
import React, { createElement } from 'react';

type FactoryFunction = (key: string, line: string) => JSX.Element;

const textWrapper = (
  keyPrefix: string,
  wrappingElement: keyof HTMLElementTagNameMap | FactoryFunction = 'p'
) => {
  let keyIndex = 0;

  const reset = () => {
    keyIndex = 0;
  };

  const incrementKeyIndex = () => {
    keyIndex += 1;
  };

  const getCurrentKey = () => `${keyPrefix}${keyIndex}`;

  const wrapLine = (line: string): JSX.Element => {
    incrementKeyIndex();
    return typeof wrappingElement === 'string'
      ? createElement(wrappingElement, { key: getCurrentKey() }, line)
      : wrappingElement(getCurrentKey(), line);
  };

  const wrap = (text: string): JSX.Element => {
    const lines = text.split('\n');
    const wrappedLines = lines.map(line => wrapLine(line));
    return <>{wrappedLines}</>;
  };

  return { wrap, reset };
};

export default textWrapper;
