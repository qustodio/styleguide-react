export const classNames = (...classes: unknown[]): string =>
  classes.filter(className => className !== '').join(' ');

const testEnvironments = ['test', 'testing'];
export const isTestEnv = (): boolean =>
  testEnvironments.includes(process.env.NODE_ENV ?? '');

export const getTestId = (
  prefix: string,
  id?: string,
  suffix?: string,
  suffix2?: string
): string | undefined =>
  id
    ? `${prefix}__${id}${suffix ? `--${suffix}` : ''}${
        suffix2 ? `--${suffix2}` : ''
      }`
    : undefined;

export const getTestIdWithParentTestId = (
  parentTestId?: string,
  testId?: string
): string | undefined => {
  if (!parentTestId && !testId) return undefined;
  if (!parentTestId) return testId;

  return `${parentTestId}${testId ? `__${testId}` : ''}`;
};

export const handleKeyboardSelection = <T>(
  handlerFn: (e: React.KeyboardEvent<T>) => void
) => (e: React.KeyboardEvent<T>): void => {
  const spaceCode = 32;
  if (e.keyCode === spaceCode) handlerFn(e);
};

/** Checks if a given value is a member of the specified enum. */
export const isEnumMember = <T>(
  value: unknown,
  enumArg: Record<string | number | symbol, T>
): value is T => (Object.values(enumArg) as unknown[]).includes(value);
