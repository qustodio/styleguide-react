type EventHandler<U> = (data: U, ...rest: unknown[]) => void;
export type TouchInteractionHandler<U = never> = (
  state?: 'in' | 'out'
) => EventHandler<U>;
/**
 * Returns an object with events handlers to manage enter an leave an Element in desktop and mobile.
 *
 * @param handler - The handler that will manage the fired events.
 * @returns - An object containing the onMouseEnter and onMouseLeave if the platform is Desktop.
 *              If the platform is Mobile, the object will have onTouchStart and onTouchEnd.
 */
const getTouchPointHandlers = <U>(
  handler: TouchInteractionHandler<U>,
  isMobile = true
) => {
  function onWhenIn<V extends U>(e: V): void;
  function onWhenIn<R, V extends U>(a: V, ...b: R[]): void;
  function onWhenIn<R, V extends U>(a: V, ...b: R[]): void {
    handler('in')(a, ...b);
  }

  function onWhenOut<V extends U>(e: V): void;
  function onWhenOut<R, V extends U>(a: V, ...b: R[]): void;
  function onWhenOut<R, V extends U>(a: V, ...b: R[]): void {
    handler('out')(a, ...b);
  }

  const handlers = () => {
    let events: Record<string, typeof onWhenIn | typeof onWhenOut> = {
      onMouseEnter: onWhenIn,
      onMouseLeave: onWhenOut,
    };
    if (isMobile) {
      events = {
        onTouchStart: onWhenIn,
        onTouchEnd: onWhenOut,
      };
    }
    const focusEvents: Record<string, typeof onWhenIn | typeof onWhenOut> = {
      onFocus: onWhenIn,
      onBlur: onWhenOut,
    };

    return {
      touchEvents: events,
      focusEvents,
      isMobile,
    };
  };

  return handlers();
};

export default getTouchPointHandlers;
