import { createMuiTheme, Theme } from '@material-ui/core/styles';

const theme: Theme = createMuiTheme({
  palette: {
    primary: {
      light: '#E1F7FD',
      main: '#2AABCB',
      dark: '#394459',
    },
    secondary: {
      main: '#1DBC54',
    },
    success: {
      main: '#70C041',
    },
    warning: {
      main: '#F57C00',
    },
    error: {
      main: '#CC0000',
    },
    info: {
      main: '#888F9B',
    },
    contrastThreshold: 3,
    tonalOffset: 0.7,
  },
  spacing: 8,
});

export default theme;
