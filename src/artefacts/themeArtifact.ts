/**
 * WARNING: AUTO-GENERATED FILE
 *
 * This file was generated automatically by a script. Any modifications made to this file may be overwritten
 * the next time the script runs. It is strongly recommended NOT to edit this file directly.
 * Instead, modify the appropriate source file(s) and regenerate this file using the script.
 *
 * @see scripts/rollup-plugin-extract-sass-variables.js
 */

/** Rocket theme object */
const themeArtifact = {
  palette: {
    peach400: '#ffe8dc',
    peach500: '#ff965c',
    peach600: '#fd752c',
    peach700: '#d94d02',
    yellow400: '#ffedcf',
    yellow500: '#ffa710',
    yellow600: '#b37100',
    rckGrey500: '#36383b',
    rckGrey400: '#55585d',
    rckGrey300: '#989ba0',
    rckGrey200: '#dddedf',
    rckGrey100: '#fafafb',
    primary: '#3b9e8a',
    primaryDark: '#2f7768',
    primaryLight: '#9dcfc4',
    primaryLighter: '#e9fff8',
    secondary: '#6161ff',
    secondaryDark: '#4949b9',
    secondaryDarker: '#1f6178',
    secondaryLight: '#d7d7ff',
    secondaryLighter: '#f1f1ff',
    grayDark: '#36383b',
    gray: '#55585d',
    graySemi: '#989ba0',
    grayLight: '#dddedf',
    grayLighter: '#fafafb',
    black: '#36383b',
    white: '#ffffff',
    white70: 'rgba(255, 255, 255, 0.7)',
    white50: 'rgba(255, 255, 255, 0.5)',
    success: '#9dcfc4',
    successDark: '#2f7768',
    successLight: '#9dcfc4',
    successLighter: '#e9fff8',
    error: '#f3656f',
    errorDarker: '#a03941',
    errorDark: '#c84851',
    errorLight: '#fe9296',
    errorLighter: '#ffdde1',
    warningDark: '#d94d02',
    warning: '#fd752c',
    warningLight: '#ff965c',
    warningLighter: '#ffe8dc',
    decorationOrange: '#ff965c',
    decorationOrangeDark: '#c85618',
    decorationOrangeLight: '#ffdbc7',
    decorationBlue: '#60d0ec',
    decorationBlueDark: '#117892',
    decorationBlueLight: '#dff6fb',
    decorationGreen: '#49c592',
    decorationGreenDark: '#236c4e',
    decorationGreenLight: '#dbf3e9',
    booster: '#d70469',
    boosterDark: '#890343',
    boosterLight: '#f26faf',
    boosterLighter: '#fee1ef',
    school: '#0a95c2',
    schoolDark: '#054b61',
    schoolLight: '#6dd6f8',
    schoolLighter: '#cef1fd',
    marketingRed: '#e54141',
    marketingOrange: '#fd752c',
    marketingOrangeLight: '#ffe6d8',
    secondaryGradientFrom: '#6161ff',
    secondaryGradientTo: '#5252cc',
    orangeGradientFrom: '#ff965c',
    orangeGradientTo: '#c85618',
  },
  routinesPalette: {
    redDark: '#9d1515',
    red: '#e54141',
    redLight: '#fad9d9',
    purpleDark: '#7e0ca7',
    purple: '#b210ea',
    purpleLight: '#f0cffb',
    blueDark: '#008099',
    blue: '#00a9ca',
    blueLight: '#cceef4',
    yellowDark: '#b27100',
    yellow: '#ffa710',
    yellowLight: '#ffedcf',
    pinkDark: '#cc004a',
    pink: '#ff357e',
    pinkLight: '#ffd7e5',
    greenDark: '#319b6f',
    green: '#49c592',
    greenLight: '#dbf3e9',
    marineDark: '#25255b',
    marine: '#2f2f75',
    marineLight: '#d5d5e3',
  },
};
export default themeArtifact;
