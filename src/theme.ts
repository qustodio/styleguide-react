import createRckTheme from './helpers/createRckTheme';

const defaultThemeProperties = {};

const theme = createRckTheme(defaultThemeProperties);

export default theme;
