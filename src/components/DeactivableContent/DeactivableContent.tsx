import React, { useRef } from 'react';
import useElementDisable from '../../hooks/useElementDisable';
import { getTestId, classNames } from '../../helpers/index';
import { BaseComponentProps, DisabledProps } from '../../common/types';

export enum DeactivableContentTest {
  prefix = 'DeactivableListItem',
}

export interface DeactivableContentProps
  extends DisabledProps,
    BaseComponentProps {}

const DeactivableContent: React.FunctionComponent<DeactivableContentProps> = ({
  disabled = false,
  testId,
  children,
  className = '',
}) => {
  const listItemRef = useRef(document.createElement('div'));
  useElementDisable(
    listItemRef,
    disabled,
    'rck-deactivable-content--disabled',
    'rck-deactivable-content__overlap'
  );
  return (
    <div
      data-testid={getTestId(DeactivableContentTest.prefix, testId)}
      ref={listItemRef}
      className={classNames('rck-deactivable-content', className)}
      aria-disabled={disabled}
    >
      {children}
    </div>
  );
};

DeactivableContent.displayName = 'DeactivableContent';

export default DeactivableContent;
