/* eslint-disable import/prefer-default-export */
export enum CardTest {
  prefix = 'Card',
  footerActionLeft = 'CardFooter__action__left',
  footerActionRight = 'CardFooter__action__right',
  headerTitle = 'CardHeader__title',
  headerAction = 'CardHeader__action',
  body = 'CardBody',
}
