import { BaseComponentProps, GlobalType } from '../../../common/types';
import { CardFooterProps } from './CardFooterProps.types';
import { CardHeaderProps } from './CardHeaderProps.types';

export type MinHeightOpt = '520';

export interface CardProps extends BaseComponentProps {
  header?: CardHeaderProps;
  footer?: CardFooterProps;
  minHeight?: MinHeightOpt;
  width?: string | number;
  height?: string | number;
  type?: GlobalType;
  displayAll?: boolean;
  fullWidth?: boolean;
  fullHeight?: boolean;
  backgroundGradient?: boolean;
  backgroundShapes?: boolean;
}
