/* eslint-disable jsx-a11y/anchor-is-valid */
// TODO: it should be button instead of anchors
import * as React from 'react';
import { getTestId } from '../../helpers';
import Link from '../Link/Link';
import { CardFooterProps } from './types/CardFooterProps.types';
import { CardTest } from './types/CardTest.types';

const CardFooter: React.FunctionComponent<CardFooterProps> = ({
  actionElement: footerActionElement,
  testId,
  onClickAction,
}: CardFooterProps) => (
  <div className="rck-card__footer">
    {footerActionElement && (
      <span className="rck-card__action rck-card__action--center">
        <Link
          onClick={onClickAction}
          data-testid={getTestId(
            CardTest.prefix,
            testId,
            CardTest.footerActionRight
          )}
        >
          {footerActionElement}
        </Link>
      </span>
    )}
  </div>
);

CardFooter.displayName = 'CardFooter';

export default CardFooter;

// TODO: In PAR we're using some of the types previously exported from this file.
// The ideal would be to remove these and use the ones from the index.
// This will require a task in both PAR and ROCK to handle breaking changes.
export { CardFooterProps } from './types/CardFooterProps.types';
export { CardHeaderProps } from './types/CardHeaderProps.types';
