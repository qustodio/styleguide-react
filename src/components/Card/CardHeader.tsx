/* eslint-disable jsx-a11y/anchor-is-valid */
// TODO: it should be button instead of anchors
import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import Link from '../Link/Link';
import { CardHeaderProps } from './types/CardHeaderProps.types';
import { CardTest } from './types/CardTest.types';

const getTitle = (
  title: CardHeaderProps['title'],
  testId: CardHeaderProps['testId']
) => {
  if (!title) return null;
  if (typeof title !== 'string') {
    return React.cloneElement(title, {
      className:
        'rck-card__header__title rck-card__header__title--custom-component',
      'data-testid': getTestId(CardTest.prefix, testId, CardTest.headerTitle),
    });
  }
  return (
    <h4
      className="rck-card__header__title rck-card__header__title--only-text"
      data-testid={getTestId(CardTest.prefix, testId, CardTest.headerTitle)}
    >
      {title}
    </h4>
  );
};

const getActionElement = (
  actionElement: CardHeaderProps['actionElement'],
  onClickAction: CardHeaderProps['onClickAction'],
  testId: CardHeaderProps['testId']
) => {
  if (!actionElement) return null;
  if (onClickAction || typeof actionElement === 'string') {
    return (
      <span className="rck-card__action rck-card__action--right">
        <Link
          onClick={onClickAction}
          testId={getTestId(CardTest.prefix, testId, CardTest.headerAction)}
        >
          {actionElement}
        </Link>
      </span>
    );
  }

  return (
    <span className="rck-card__action rck-card__action--right">
      {React.cloneElement(actionElement, {
        'data-testid': getTestId(
          CardTest.prefix,
          testId,
          CardTest.headerAction
        ),
      })}
    </span>
  );
};

const CardHeader: React.FunctionComponent<CardHeaderProps> = ({
  title,
  type,
  border,
  testId,
  banner,
  actionElement,
  wrapper,
  onClickAction,
}: CardHeaderProps) => {
  const classes = classNames(
    'rck-card__header',
    border ? 'rck-card__header--with-border' : '',
    banner ? 'rck-card__header--with-banner' : '',
    `rck-card__header--header-colors-${type}`
  );
  const renderedBody = !banner ? (
    <>
      {getTitle(title, testId)}
      {getActionElement(actionElement, onClickAction, testId)}
    </>
  ) : (
    banner
  );

  return wrapper ? (
    wrapper({ children: renderedBody, className: classes })
  ) : (
    <div className={classes}>{renderedBody}</div>
  );
};

CardHeader.displayName = 'CardHeader';

export default CardHeader;
