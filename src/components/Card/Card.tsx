import * as React from 'react';
import { GlobalType } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import CardFooter from './CardFooter';
import CardHeader from './CardHeader';
import { CardProps } from './types/CardProps.types';
import { CardTest } from './types/CardTest.types';

const Card: React.FunctionComponent<CardProps> = ({
  header,
  footer,
  displayAll,
  fullHeight,
  fullWidth,
  minHeight,
  className = '',
  width,
  height,
  children,
  type = GlobalType.default,
  backgroundGradient,
  backgroundShapes,
  testId,
}) => {
  const backgroundColorClass = !backgroundGradient
    ? `rck-card--bck-color-${type}`
    : `rck-card--bck-color-${type}-gradient`;

  const noBodyAndFooter = footer === undefined && children === undefined;

  return (
    <div
      className={classNames(
        'rck-card',
        displayAll ? 'rck-card--display-all' : '',
        fullHeight ? 'rck-card--full-height' : '',
        fullWidth ? 'rck-card--full-width' : '',
        type
          ? `rck-card--${type} ${backgroundColorClass}`
          : 'rck-card--bck-color-default',
        backgroundShapes ? 'rck-card--with-shapes' : '',
        noBodyAndFooter ? 'rck-card--only-header' : '',
        className
      )}
      style={{ width, height }}
      data-testid={getTestId(CardTest.prefix, testId)}
    >
      {header && <CardHeader type={type} {...header} />}

      {children ? (
        <div
          className={classNames(
            'rck-card__body',
            minHeight ? `rck-card__body--min-height-${minHeight}` : ''
          )}
          data-testid={getTestId(CardTest.prefix, testId, CardTest.body)}
        >
          {children}
        </div>
      ) : null}
      {footer && <CardFooter {...footer} />}
    </div>
  );
};

Card.displayName = 'Card';

export default Card;
