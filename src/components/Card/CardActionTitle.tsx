import * as React from 'react';
import { BaseComponentProps, GlobalType } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconType } from '../Icons';

export type CardActionTitleColor =
  | 'inherit'
  | GlobalType.default
  | GlobalType.white
  | GlobalType.gray
  | GlobalType.primary
  | GlobalType.secondary
  | GlobalType.error;

export interface CardActionTitleProps extends BaseComponentProps {
  icon?: IconType | React.ReactNode;
  color?: CardActionTitleColor;
}

export enum CardActionTitleTest {
  prefix = 'CardActionTitle',
}

const getIcon = (icon: CardActionTitleProps['icon']) => {
  if (!icon) return null;
  if (React.isValidElement(icon)) return icon;
  if (Object.values(IconType).includes(icon as IconType)) {
    return (
      <Icon type={icon as IconType} className="rck-card-action-title__icon" />
    );
  }
  return null;
};

const CardActionTitle: React.FunctionComponent<CardActionTitleProps> = ({
  icon,
  color = GlobalType.default,
  className = '',
  testId,
  children,
}) => (
  <div
    className={classNames(
      'rck-card-action-title',
      `rck-card-action-title--color-${color}`,
      className
    )}
    data-testid={getTestId(CardActionTitleTest.prefix, testId)}
  >
    {getIcon(icon)}
    {children}
  </div>
);

export default React.memo(CardActionTitle);
