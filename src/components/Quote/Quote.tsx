import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export type QuoteMaxLines = 'none' | 'one' | 'two' | 'three' | 'four' | 'five';

export interface QuoteProps extends BaseComponentProps {
  maxLines?: QuoteMaxLines;
}

export enum QuoteTest {
  prefix = 'Quote',
}

const Quote: React.FunctionComponent<QuoteProps> = ({
  maxLines = 'one',
  className = '',
  testId,
  children,
}) => (
  <div
    className={classNames('rck-quote', `rck-quote--${maxLines}`, className)}
    data-testid={getTestId(QuoteTest.prefix, testId)}
  >
    <i>{children}</i>
  </div>
);

Quote.displayName = 'Quote';

export default React.memo(Quote);
