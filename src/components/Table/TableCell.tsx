import * as React from 'react';
import { classNames } from '../../helpers';
import { BaseComponentProps } from '../../common/types';

interface TableCellProps extends BaseComponentProps {
  head?: boolean;
  alignement?: 'left' | 'right' | 'center';
}

const TableCell = ({ head, alignement, children }: TableCellProps) =>
  head ? (
    <th
      className={classNames(
        'rck-table-cell',
        'rck-table-cell--head',
        alignement ? `rck-table-cell--${alignement}` : ''
      )}
    >
      {children}
    </th>
  ) : (
    <td
      className={classNames(
        'rck-table-cell',
        alignement ? `rck-table-cell--${alignement}` : ''
      )}
    >
      {children}
    </td>
  );

export default TableCell;
