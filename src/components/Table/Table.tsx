import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';

interface TableProps extends BaseComponentProps {
  border: 'default' | 'none';
}

const Table = ({ border, children, className, testId }: TableProps) => (
  <table
    className={classNames(
      'rck-table',
      border !== 'default' ? `rck-table--border-${border}` : '',
      className ?? ''
    )}
    data-testid={getTestId('rck-table', testId)}
  >
    {children}
  </table>
);

export default Table;
