import * as React from 'react';
import { BaseComponentProps } from '../../common/types';

const TableHead = ({ children }: BaseComponentProps) => (
  <thead className="rck-table-head">{children}</thead>
);

export default TableHead;
