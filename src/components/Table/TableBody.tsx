import * as React from 'react';
import { BaseComponentProps } from '../../common/types';

const TableBody = ({ children }: BaseComponentProps) => (
  <tbody>{children}</tbody>
);

export default TableBody;
