import * as React from 'react';
import { BaseComponentProps } from '../../common/types';

const TableRow = ({ children }: BaseComponentProps) => (
  <tr className="rck-table-row">{children}</tr>
);

export default TableRow;
