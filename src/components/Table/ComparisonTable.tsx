import * as React from 'react';

import Table from './Table';
import TableBody from './TableBody';
import TableCell from './TableCell';
import TableHead from './TableHead';
import TableRow from './TableRow';
import Icon, { IconColor, IconFamily, IconSize, IconType } from '../Icons';

export interface ComparisonTableData {
  head: Array<{
    icon?: typeof IconType[keyof typeof IconType];
    text: string;
  }>;
  rows: Array<string | boolean | null>[];
}

export interface ComparisonTableProps {
  data: ComparisonTableData;
  key: string;
  border?: 'default' | 'none';
}

const renderCell = (
  cellContent: string | boolean | null,
  index: number,
  tableKey: string
) => {
  if (typeof cellContent === 'string') {
    // if it is the first column align the element to the left, otherwise center it
    const alignement = index === 0 ? 'left' : 'center';
    return (
      <TableCell
        alignement={alignement}
        key={`table-head-cell-${tableKey}-${index}`}
      >
        {cellContent}
      </TableCell>
    );
  }
  if (typeof cellContent === 'boolean') {
    return (
      <TableCell
        alignement="center"
        key={`table-head-cell-${tableKey}-${index}`}
      >
        {cellContent ? (
          <Icon
            type={IconType.check}
            color={IconColor.greenDecorationContrast}
            size={IconSize.lg}
            family={IconFamily.solid}
          />
        ) : (
          <Icon
            type={IconType.xmark}
            color={IconColor.error}
            size={IconSize.lg}
            family={IconFamily.solid}
          />
        )}
      </TableCell>
    );
  }
  if (cellContent === null) {
    return (
      <TableCell
        alignement="center"
        key={`table-head-cell-${tableKey}-${index}`}
      >
        -
      </TableCell>
    );
  }
  return '';
};

const ComparisonTable = ({
  data,
  key,
  border = 'default',
}: ComparisonTableProps) => (
  <Table border={border} className="rck-comparison-table">
    <TableHead>
      <TableRow>
        {data.head.map((cell, index) => {
          const cellHeadKey = `table-head-cell-${key}-${index}`;
          if (index === 0) {
            return (
              <TableCell head alignement="left" key={cellHeadKey}>
                {cell.icon && (
                  <Icon
                    type={cell.icon as IconType}
                    className="rck-comparison-table__head-icon"
                  />
                )}
                {cell.text}
              </TableCell>
            );
          }
          return (
            <TableCell alignement="center" key={cellHeadKey}>
              {cell.text}
            </TableCell>
          );
        })}
      </TableRow>
    </TableHead>
    <TableBody>
      {data.rows.map((row, index) => {
        const rowBodyKey = `table-row-${key}-${index}`;
        return (
          <TableRow key={rowBodyKey}>
            {row.map((cell, indexCell) => renderCell(cell, indexCell, key))}
          </TableRow>
        );
      })}
    </TableBody>
  </Table>
);

ComparisonTable.displayName = 'ComparisonTable';

export default ComparisonTable;
