import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';

export interface AffiliateBadgeProps extends BaseComponentProps {
  iconSrc: string;
  description?: string;
}

export enum AffiliateBadgeTest {
  prefix = 'AffiliateBadge',
}

const AffiliateBadge: React.FunctionComponent<AffiliateBadgeProps> = ({
  iconSrc,
  description,
  testId,
  className = '',
}) => (
  <div
    className={classNames('rck-affiliate-badge', className)}
    data-testid={getTestId(AffiliateBadgeTest.prefix, testId)}
  >
    <img src={iconSrc} alt={description} />
  </div>
);

AffiliateBadge.displayName = 'AffiliateBadge';

export default React.memo(AffiliateBadge);
