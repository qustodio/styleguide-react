import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';

export enum BadgeType {
  error = 'error',
  warning = 'warning',
  neutral = 'neutral',
  brand = 'brand',
  secondary = 'secondary',
  success = 'success',
  dark = 'dark',
  contrast = 'contrast',
  school = 'school',
}

export type BadgeSize = 'regular' | 'small';
export interface BadgeProps extends BaseComponentProps {
  text: string;
  type: BadgeType;
  size?: BadgeSize;
}

export enum BadgeTest {
  prefix = 'Badge',
}

/**
 * @deprecated Please use `Tag` component instead.
 * ```jsx
 * <Tag type="squared" {...} />
 * ```
 */
const Badge: React.FunctionComponent<BadgeProps> = ({
  text,
  type,
  size = 'regular',
  className = '',
  testId,
}) => (
  <div
    className={classNames(
      'rck-badge',
      `rck-badge--${type}`,
      `rck-badge--${size}`,
      className
    )}
    data-testid={getTestId(BadgeTest.prefix, testId)}
  >
    {text}
  </div>
);

Badge.displayName = 'Badge';

export default React.memo(Badge);
