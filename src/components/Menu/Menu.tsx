import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import MenuItem, { MenuItemTypeProps } from './MenuItem';
import { BaseComponentProps } from '../../common/types';
import { ProfileInfoHeaderProps } from '../AccountInfoHeader/ProfileInfoHeader';
import ScrollableContent from '../ScrollableContent/ScrollableContent';

export interface MenuProps extends BaseComponentProps {
  items: MenuItemTypeProps[];
  selectedRule: string | undefined;
  showBadge?: boolean;
  isDataReady?: boolean;
  scrollable?: boolean;
  onClick: (ruleKey: string) => void;
  onClickAccordionMenu?: (newAccordionMenuVisibility: boolean) => void;
  accordionMenuIsVisible?: boolean;
  expanded?: boolean;
  profileInfoHeader?: React.ReactElement<ProfileInfoHeaderProps>;
}

export enum MenuTest {
  prefix = 'Menu',
}

const Menu: React.FunctionComponent<MenuProps> = ({
  className = '',
  testId,
  items,
  selectedRule,
  showBadge = true,
  isDataReady = true,
  scrollable,
  onClick,
  onClickAccordionMenu = () => {},
  accordionMenuIsVisible = false,
  profileInfoHeader,
  expanded,
}) => {
  const renderChildren = (children: MenuItemTypeProps[], isLoading?: boolean) =>
    children.map(item => {
      const isChildrenActive = selectedRule === item.key;
      return (
        <MenuItem
          key={item.key}
          menuItem={item}
          onClick={() => onClick(item.key)}
          showBadge={showBadge}
          isLoading={isLoading}
          isActive={isChildrenActive}
          expanded={expanded}
          testId={testId}
        />
      );
    });

  const renderMenu = () =>
    items.map(rule => {
      const isLoading = !isDataReady && rule.needsData;
      const isActive = selectedRule === rule.key;
      if (rule.children) {
        const { children } = rule;

        return (
          <React.Fragment key={rule.key}>
            <MenuItem
              menuItem={rule}
              onClick={() => onClickAccordionMenu(!accordionMenuIsVisible)}
              showBadge={showBadge}
              isLoading={isLoading}
              isActive={isActive}
              isHeader
              expanded={expanded}
              testId={testId}
            />
            <ul className={classNames('rck-menu__section', className)}>
              {renderChildren(children, isLoading)}
            </ul>
          </React.Fragment>
        );
      }

      return (
        <MenuItem
          key={rule.key}
          menuItem={rule}
          onClick={() => onClick(rule.key)}
          showBadge={showBadge}
          isLoading={isLoading}
          isActive={isActive}
          expanded={expanded}
          testId={testId}
        />
      );
    });
  return (
    <div
      className={classNames(
        'rck-menu',
        !expanded ? 'rck-menu--no-expanded' : '',
        className
      )}
      datatest-id={getTestId(MenuTest.prefix, testId)}
    >
      {profileInfoHeader && (
        <div className="rck-menu__profile-info-header">{profileInfoHeader}</div>
      )}
      <ul className="rck-menu__list">
        {scrollable && (
          <ScrollableContent hideScrollbar>{renderMenu()}</ScrollableContent>
        )}
        {!scrollable && renderMenu()}
      </ul>
    </div>
  );
};

Menu.displayName = 'Menu';

export default Menu;
