// TODO: We have to review this because the main role of the menu should be "navigation" but we are using
// our anchors like buttons

import * as React from 'react';
import Badge, { BadgeType } from '../Badge/Badge';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';

export interface MenuItemTypeProps {
  key: string;
  translation?: string;
  content?: JSX.Element;
  isPremium: boolean;
  needsData?: boolean;
  children?: MenuItemTypeProps[] | undefined;
  showContent?: boolean;
}

export interface MenuItemProps extends BaseComponentProps {
  children?: JSX.Element;
  menuItem: MenuItemTypeProps;
  isHeader?: boolean;
  showBadge?: boolean;
  isLoading?: boolean;
  isActive?: boolean;
  expanded?: boolean;
  onClick?: () => void;
}

export enum MenuItemTest {
  prefix = 'MenuItem',
  actionElement = 'Action-link',
}

export const MenuItemContent: React.FunctionComponent<MenuItemProps> = ({
  menuItem,
  showBadge,
  isLoading,
  isActive,
  isHeader,
  expanded,
  onClick,
  testId,
}) => (
  <li
    key={menuItem.key}
    className="rck-menu-item__item-wrapper"
    data-testid={getTestId(MenuItemTest.prefix, testId, menuItem.key)}
  >
    <div
      className={classNames(
        'rck-menu-item__item',
        `rck-menu-item__${menuItem.key}`,
        isLoading ? 'rck-menu-item__item--blank' : '',
        isHeader ? 'rck-menu-item__item--header' : '',
        isActive ? 'rck-menu-item__item--active' : '',
        !expanded ? 'rck-menu-item__item--no-expanded' : ''
      )}
      key={menuItem.key}
      onClick={isLoading ? undefined : onClick}
      onKeyDown={isLoading ? undefined : onClick}
      data-testid={getTestId(
        MenuItemTest.prefix,
        testId,
        menuItem.key,
        MenuItemTest.actionElement
      )}
      tabIndex={isHeader ? undefined : 0}
      role={isHeader ? undefined : 'button'}
    >
      <span
        className={classNames(
          'rck-menu-item__text',
          isActive ? 'rck-menu-item__text--active' : ''
        )}
      >
        {isHeader && (
          <strong>
            {menuItem.content ? menuItem.content : menuItem.translation}
          </strong>
        )}
        {!isHeader &&
          (menuItem.content ? menuItem.content : menuItem.translation)}
      </span>
      {showBadge && menuItem.isPremium && (
        <Badge
          className="rck-menu-item__badge"
          type={menuItem.isPremium ? BadgeType.brand : BadgeType.success}
          text={menuItem.isPremium ? 'Premium' : ''}
        />
      )}
    </div>
  </li>
);

const MenuItem: React.FunctionComponent<MenuItemProps> = props => {
  const { children, menuItem } = props;
  return (
    <>
      {children}
      {menuItem.showContent && <MenuItemContent {...props} />}
    </>
  );
};

MenuItem.displayName = 'MenuItem';

export default React.memo(MenuItem);
