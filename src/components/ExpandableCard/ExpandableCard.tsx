import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';

import Icon, { IconColor, IconType } from '../Icons';

export enum ExpandableCardTest {
  prefix = 'ExpandableCard',
}

interface ExpandableCardVerticalProps extends BaseComponentProps {
  opener: boolean;
  height: string;
}

const ExpandableCardVertical: React.FC<ExpandableCardVerticalProps> = ({
  opener = true,
  children,
  height,
  testId,
}: ExpandableCardVerticalProps) => {
  const [closed, setClosed] = React.useState(true);
  const toggle = () => setClosed((isClosed) => !isClosed);

  return (
    <div
      className={classNames(
        'rck-expandable-card-vertical__container',
        closed
          ? 'rck-expandable-card-vertical__container--closed'
          : 'rck-expandable-card-vertical__container--opened'
      )}
      style={{ height: closed ? height : 'auto' }}
    >
      <div
        className="rck-expandable-card-vertical__items"
        data-testid={getTestId(ExpandableCardTest.prefix, testId)}
      >
        {children}
      </div>

      {opener ? (
        <div className="rck-expandable-card-vertical__icon">
          <Icon
            testId={getTestId(ExpandableCardTest.prefix, testId)}
            type={closed ? IconType.angleDown : IconType.angleUp}
            color={IconColor.regular}
            onClick={toggle}
          />
        </div>
      ) : null}
    </div>
  );
};

const ExpandableCardHorizontal: React.FC<BaseComponentProps> = ({
  children,
  testId,
}: BaseComponentProps) => (
  <div
    className="rck-expandable-card-horizontal__container"
    data-testid={getTestId(ExpandableCardTest.prefix, testId)}
  >
    {children}
  </div>
);

interface ExpandableCardProps extends ExpandableCardVerticalProps {
  mode: 'horizontal' | 'vertical';
}

const ExpandableCard: React.FC<ExpandableCardProps> = ({
  mode,
  children,
  height,
  opener,
  testId,
}: ExpandableCardProps) =>
  mode === 'vertical' ? (
    <ExpandableCardVertical height={height} opener={opener} testId={testId}>
      {children}
    </ExpandableCardVertical>
  ) : (
    <ExpandableCardHorizontal testId={testId}>
      {children}
    </ExpandableCardHorizontal>
  );

export default ExpandableCard;
