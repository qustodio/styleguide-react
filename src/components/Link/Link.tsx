import React, { SyntheticEvent } from 'react';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
import { AriaLabelingProps } from '../../common/aria.types';
import {
  BaseComponentProps,
  DisabledProps,
  TestableComponentProps,
} from '../../common/types';
import { classNames } from '../../helpers/index';
import useAriaProps from '../../hooks/useAriaProps';

export type IconPosition = 'right' | 'left';

export interface LinkProps
  extends BaseComponentProps,
    TestableComponentProps,
    DisabledProps,
    AriaLabelingProps {
  href?: string;
  hrefLang?: string;
  rel?: string;
  target?: string;
  referrerPolicy?: string;
  useAsLink?: React.FunctionComponent<AnchorPropTypes>;
  ariaLabel?: string;
  iconPosition?: IconPosition;
  onClick?: (ev: SyntheticEvent) => void;
  icon?: JSX.Element;
}

const Link: React.FunctionComponent<LinkProps> = ({
  useAsLink: Anchor,
  ariaLabel,
  testId,
  id,
  role,
  tabIndex,
  href,
  hrefLang,
  rel,
  target,
  referrerPolicy,
  icon,
  iconPosition = 'right',
  disabled,
  className = '',
  onClick,
  children,
  ...rest
}) => {
  const ariaProps = useAriaProps(rest);
  return (
    <span
      className={classNames(
        'rck-link',
        disabled ? 'rck-link--disabled' : '',
        className
      )}
    >
      {Anchor && (
        <Anchor
          to={href}
          className=""
          aria-label={ariaLabel}
          data-testid={testId}
          onClick={disabled ? undefined : onClick}
        >
          {icon && iconPosition === 'left' && (
            <span className="rck-link__icon--left">{icon}</span>
          )}
          {children}
          {icon && iconPosition === 'right' && (
            <span className="rck-link__icon--right">{icon}</span>
          )}
        </Anchor>
      )}
      {!Anchor && (
        <a
          {...ariaProps}
          id={id}
          href={href}
          aria-label={ariaLabel}
          data-testid={testId}
          onClick={disabled ? undefined : onClick}
          role={role}
          tabIndex={tabIndex}
          hrefLang={hrefLang}
          rel={rel}
          target={target}
          referrerPolicy={referrerPolicy}
        >
          {icon && iconPosition === 'left' && (
            <span className="rck-link__icon--left">{icon}</span>
          )}
          {children}
          {icon && iconPosition === 'right' && (
            <span className="rck-link__icon--right">{icon}</span>
          )}
        </a>
      )}
    </span>
  );
};

Link.displayName = 'Link';

export default React.memo(Link);
