import React, { useState } from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconColor, IconType } from '../Icons';
import Button, {
  ButtonIconPosition,
  ButtonSize,
  ButtonType,
} from '../Button/Button';

const ToggleContainerTest = {
  prefix: 'ToggleContainer',
};

export interface ToggleContainerProps extends BaseComponentProps {
  buttonOpenText: string;
  buttonCloseText: string;
  initiallyOpen?: boolean;
}

const ToggleContainer = ({
  buttonOpenText,
  buttonCloseText,
  initiallyOpen = false,
  children,
  className,
  testId,
}: ToggleContainerProps) => {
  const [isOpen, setIsOpen] = useState<boolean>(initiallyOpen);

  return (
    <div
      className={classNames('rck-toggle-container__wrapper', className || '')}
      data-testid={getTestId(ToggleContainerTest.prefix, testId)}
    >
      <div
        className={classNames(
          'rck-toggle-container__container',
          isOpen
            ? 'rck-toggle-container__container--open'
            : 'rck-toggle-container__container--close'
        )}
      >
        {children}
      </div>
      <div>
        <Button
          buttonType={ButtonType.plain}
          onClick={() => setIsOpen(currentValue => !currentValue)}
          size={ButtonSize.medium}
          icon={
            <Icon
              type={isOpen ? IconType.angleUp : IconType.angleDown}
              color={IconColor.secondary}
            />
          }
          iconPosition={ButtonIconPosition.right}
        >
          {isOpen ? buttonCloseText : buttonOpenText}
        </Button>
      </div>
    </div>
  );
};

ToggleContainer.displayName = 'ToggleContainer';

export default ToggleContainer;
