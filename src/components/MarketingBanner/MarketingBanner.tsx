import React, { ReactElement } from 'react';
import { RckColor } from '../../common/color.types';
import { RckBaseGrandient } from '../../common/gradient.types';
import { TagProps } from '../Tag';
import { ButtonProps } from '../Button/Button';
import { classNames } from '../../helpers';
import Icon, { IconType } from '../Icons';
import Text from '../Text';

export type MarketingBannerProps = {
  backgroundType?: 'plain' | 'gradient';
  backgroundColor: RckColor | RckBaseGrandient;
  backgroundShapes?: boolean;
  imageSrc?: string;
  tag?: ReactElement<TagProps>;
  title: string;
  description?: string;
  button: ReactElement<ButtonProps>;
  showCloseIcon?: boolean;
  closeIconColor?: RckColor;
  alignment?: 'center' | 'left';
  onClose?: () => void;
};

const MarketingBanner = ({
  backgroundType = 'plain',
  backgroundColor,
  backgroundShapes = false,
  imageSrc,
  tag,
  title,
  description,
  button,
  showCloseIcon = false,
  closeIconColor,
  alignment = 'center',
  onClose,
}: MarketingBannerProps) => (
  <div
    className={classNames(
      'rck-marketing-banner',
      `rck-marketing-banner--alignment-${alignment}`,
      backgroundType === 'gradient'
        ? `rck-marketing-banner--background-gradient-${backgroundColor}`
        : `rck-marketing-banner--background-${backgroundColor}`,
      backgroundShapes ? `rck-marketing-banner--background-with-shapes` : ''
    )}
  >
    <div className="rck-marketing-banner__header">
      {showCloseIcon && (
        <div
          className={classNames(
            'rck-marketing-banner__close-icon',
            `rck-marketing-banner__close-icon--${closeIconColor}`
          )}
        >
          <Icon type={IconType.times} onClick={onClose} />
        </div>
      )}
    </div>
    <div className="rck-marketing-banner__body">
      {imageSrc && (
        <img
          src={imageSrc}
          className="rck-marketing-banner__image"
          alt="marketing banner"
        />
      )}
      <div className="rck-marketing-banner__right-content">
        {tag}
        <Text variant="title-2-semibold-responsive">{title}</Text>
        {description && <Text variant="body-2-regular">{description}</Text>}
        <div className="rck-marketing-banner__button">{button}</div>
      </div>
    </div>
  </div>
);

MarketingBanner.displayName = 'MarketingBanner';

export default MarketingBanner;
