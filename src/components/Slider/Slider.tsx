import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps, DisabledProps } from '../../common/types';

export interface SliderProps extends BaseComponentProps, DisabledProps {
  id: string;
  name?: string;
  block?: boolean;
  minValue: number;
  maxValue: number;
  value: number;
  formatLabel: string;
  onInput?: (ev: React.SyntheticEvent, value: string) => void;
  onChange?: (ev: React.SyntheticEvent, value: string) => void;
}

export enum SliderTest {
  prefix = 'Slider',
}

const Slider: React.FunctionComponent<SliderProps> = ({
  className = '',
  name,
  id,
  disabled,
  block,
  minValue,
  maxValue,
  value,
  testId,
  formatLabel,
  onInput,
  onChange,
}) => {
  const inputRef = React.useRef(document.createElement('input'));

  const handleOnInput = (ev: React.SyntheticEvent) => {
    if (!disabled) {
      if (onInput) {
        onInput(ev, inputRef.current.value);
      }
    }
  };

  const handleOnChange = (ev: React.SyntheticEvent) => {
    if (!disabled) {
      if (onChange) {
        onChange(ev, inputRef.current.value);
      }
    }
  };

  return (
    <div
      className={classNames(
        className,
        'rck-slider',
        block ? 'rck-slider--block' : '',
        disabled ? 'rck-slider--disabled' : ''
      )}
    >
      <label htmlFor={id} data-testid={getTestId(SliderTest.prefix, testId)}>
        {value} {formatLabel}
      </label>

      <input
        id={id}
        className="rck-slider"
        name={name}
        ref={inputRef}
        type="range"
        min={minValue}
        max={maxValue}
        value={value}
        onInput={handleOnInput}
        onChange={handleOnChange}
      />
    </div>
  );
};

Slider.displayName = 'Slider';

export default Slider;
