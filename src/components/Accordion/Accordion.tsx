import React, { useRef, useLayoutEffect, useEffect, useState } from 'react';
import { RckBaseColor } from '../../common/color.types';
import { classNames, getTestId } from '../../helpers';
import AccordionItem, { AccordionItemProps } from './AccordionItem';
import { BaseComponentProps } from '../../common/types';

export interface AccordionProps extends BaseComponentProps {
  color?: RckBaseColor;
  highlightOnOpen?: boolean;
  maxColumns?: 1 | 2 | 3;
  items: Array<AccordionItemProps>;
}

export enum AccordionTest {
  prefix = 'Accordion',
}

const Accordion = ({
  testId,
  color,
  highlightOnOpen = false,
  maxColumns = 1,
  items = [],
}: AccordionProps) => {
  // Start with an empty `<ul></ul>` to avoid null checks.
  const elementRef = useRef<HTMLUListElement>(document.createElement('ul'));

  /**
   * When first loading the accordion, check if any item is forced open.
   */
  const getInitialOpenItemKey = (): string | null => {
    const openItem = items.find(item => item.isOpen === true);
    return openItem ? openItem.key : null;
  };

  const [openItemKey, setOpenItemKey] = useState<string | null>(
    getInitialOpenItemKey()
  );

  /*
   * To create a responsive, masonry-like multi-column layout for the Accordion,
   * we use the hack described here:
   * @link https://tobiasahlin.com/blog/masonry-with-css/
   *
   * This hack requires the height of the Accordion to be set explicitely,
   * and to be at least as tall as the tallest column inside it. So we start by
   * setting the height to a very large number, and then we calculate the actual
   * height.
   *
   * Because the height is calculated on `useLayoutEffect()`, the calculations
   * are made before the component is rendered, so there are no layout shifts.
   */
  const resizeCalcHeight = 99999;
  /*
   * Sizes are stored as integers using `Math.ceil` to avoid re-rendering the
   * accordion when the difference in size is negligible. We round up to avoid
   * accordion items not fitting due to differences caused by decimal values.
   */
  const [accordionHeight, setAccordionHeight] = useState<number>(
    resizeCalcHeight
  );
  const [accordionWidth, setAccordionWidth] = useState<number>(0);
  const [isResizing, setIsResizing] = useState<boolean>(false);

  /**
   * Get the actual height of the accordion based on the bottom-most item offset.
   *
   * We need this workaround because for the masonry-like layout to work, the
   * accordion's height must be set explicitly, and it must be at least as tall
   * as the tallest column.
   *
   * For this reason, we set the accordion height to an absurdly large number,
   * and then calculate the actual height based on its contents.
   */
  const calcAccordionHeight = (accordionEl: HTMLUListElement): number => {
    const parentOffset = accordionEl.getBoundingClientRect().top;
    const bottomRowItems = Array.from(accordionEl.children).splice(
      maxColumns * -1
    );
    const bottomRowItemsOffset = bottomRowItems.map(
      (item): number => item.getBoundingClientRect().bottom - parentOffset
    );
    return Math.ceil(Math.max(...bottomRowItemsOffset));
  };

  /*
   * Create observer that triggers height re-calculation when the Accordion
   * width changes.
   */
  const previousWidth = useRef<number>(accordionWidth);

  useEffect(() => {
    const observerCallback = (entries: ResizeObserverEntry[]) => {
      const accordionEl = entries[0];
      const currentWidth = Math.ceil(accordionEl.borderBoxSize[0].inlineSize);
      if (Math.ceil(previousWidth.current) !== Math.ceil(currentWidth)) {
        previousWidth.current = currentWidth;
        setAccordionWidth(currentWidth);
      }
    };

    const observer = new ResizeObserver(observerCallback);

    observer.observe(elementRef.current, { box: 'border-box' });

    return () => observer.disconnect();
  }, []);

  /*
   * Trigger height re-calculation when a property that can affect height changes.
   */
  useLayoutEffect(() => {
    // On the fist render, we just calculate and set the Accordions width. After
    // changing the width, this effect will run again, so we calculate the height
    // then.
    const isFirstRender = accordionWidth === 0;
    if (isFirstRender) {
      const elementWidth = Math.ceil(
        elementRef.current.getBoundingClientRect().width
      );
      previousWidth.current = elementWidth;
      setAccordionWidth(elementWidth);
    } else {
      setIsResizing(true);
      setAccordionHeight(resizeCalcHeight);
    }
  }, [openItemKey, items, maxColumns, accordionWidth]);

  /*
   * Calculates and updates the actual accordion height based on its contents,
   * only after ensuring the accordion height has been set to a large number to
   * allow calculation.
   */
  useLayoutEffect(() => {
    if (isResizing) {
      setIsResizing(false);
      setAccordionHeight(calcAccordionHeight(elementRef.current));
    }
  }, [accordionHeight, isResizing]);

  const renderItem = (
    itemProps: AccordionItemProps
  ): React.ReactElement<AccordionItemProps> => (
    <AccordionItem
      {...itemProps}
      isOpen={itemProps.key === openItemKey}
      onClick={() =>
        setOpenItemKey(itemProps.key === openItemKey ? null : itemProps.key)
      }
    />
  );

  return (
    /* The wrapper is needed to enable @container CSS queries. */
    <div className="rck-accordion__wrapper">
      <ul
        className={classNames(
          'rck-accordion',
          color ? `rck-accordion--color-${color}` : '',
          maxColumns > 1 ? `rck-accordion--columns-${maxColumns}` : '',
          highlightOnOpen ? 'rck-accordion--highlight-on-open' : ''
        )}
        data-testid={getTestId(AccordionTest.prefix, testId)}
        role="menu"
        ref={elementRef}
        style={{ height: `${accordionHeight}px` }}
      >
        {items.map(renderItem)}
      </ul>
    </div>
  );
};

Accordion.displayName = 'Accordion';

export default Accordion;
