import React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';
import Icon, { IconType, IconFamily } from '../Icons';

export interface AccordionItemProps extends BaseComponentProps {
  key: string;
  title: string;
  content: string | JSX.Element;
  isOpen?: boolean;
  icon: string;
  onClick?: () => void;
}

export enum AccordionItemTest {
  prefix = 'AccordionItem',
}

const AccordionItem = ({
  title,
  content,
  isOpen = false,
  icon,
  onClick,
  testId,
}: AccordionItemProps) => (
  <li
    className={classNames(
      'rck-accordion__item',
      'rck-accordion-item',
      isOpen ? 'rck-accordion-item--open' : ''
    )}
    data-testid={getTestId(AccordionItemTest.prefix, testId)}
    role="menuitem"
    tabIndex={0}
    aria-expanded={isOpen}
    onClick={onClick}
    onKeyDown={onClick}
  >
    <Icon
      type={icon as IconType}
      family={isOpen ? IconFamily.solid : IconFamily.regular}
      className="rck-accordion-item__icon"
    />

    <div className="rck-accordion-item__title">{title}</div>

    <button className="rck-accordion-item__button" type="button">
      <Icon type={isOpen ? IconType.minus : IconType.plus} />
    </button>

    <div className="rck-accordion-item__content">{content}</div>
  </li>
);

AccordionItem.displayName = 'AccordionItem';

export default AccordionItem;
