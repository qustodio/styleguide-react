import * as React from 'react';
import IconAdapter from '../List/IconAdapter';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconColor, IconFamily, IconType } from '../Icons';

export enum IconWithBadgeTest {
  prefix = 'IconWithBadge',
}

export interface IconWithBadgeProps {
  icon: string | JSX.Element;
  showBadge: boolean;
  badgeColor?: IconColor;
  badgePosition?: 'right' | 'left';
  containerClassName?: string;
  badgeClassname?: string;
  iconClassname?: string;
  testId?: string;
}

const IconWithBadge = ({
  icon,
  showBadge,
  badgeColor = IconColor.secondary,
  badgePosition = 'left',
  containerClassName,
  badgeClassname,
  iconClassname,
  testId,
}: IconWithBadgeProps) => (
  <div
    className={classNames('rck-icon-with-badge__container', containerClassName)}
    data-testid={getTestId(IconWithBadgeTest.prefix, testId)}
  >
    <IconAdapter
      icon={icon}
      className={classNames('rck-icon-with-badge__icon', iconClassname)}
    />
    <div
      className={classNames(
        'rck-icon-with-badge__badge',
        badgePosition === 'right'
          ? 'rck-icon-with-badge__badge--right'
          : 'rck-icon-with-badge__badge--left',
        badgeClassname
      )}
    >
      {showBadge && (
        <Icon
          type={IconType.circle}
          family={IconFamily.solid}
          color={badgeColor}
          className={classNames('rck-icon-with-badge__badge-icon')}
          testId="new-item-list"
          circle
        />
      )}
    </div>
  </div>
);

IconWithBadge.displayName = 'IconWithBadge';

export default IconWithBadge;
