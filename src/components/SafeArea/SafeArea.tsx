import * as React from 'react';

const SafeArea: React.FC = () => <div className="rck-safe-area" />;

SafeArea.displayName = 'SafeArea';

export default SafeArea;
