import * as React from 'react';
import { TooltipProps } from './types/Tooltip.types';

export const getTooltipContent = (
  label: React.ReactNode,
  title: string | undefined
) => {
  const content = [];
  if (title) {
    content.push(
      <div key="1" className="rck-tooltip__title">
        {title}
      </div>
    );
  }
  if (typeof label === 'string') {
    content.push(
      <div key="2" className="rck-tooltip__label">
        {label}
      </div>
    );
  } else {
    content.push(<React.Fragment key="3">{label}</React.Fragment>);
  }
  return <>{...content}</>;
};

export const getTooltipTransitionClass = (
  transition: TooltipProps['smoothTransition']
): string => {
  if (!transition) return '';
  if (typeof transition === 'boolean') {
    return 'rck-tooltip--smooth-transition-500';
  }
  if (typeof transition === 'number') {
    return `rck-tooltip--smooth-transition-${transition}`;
  }
  return '';
};

export const generateGetBoundingClientRect = (x = 0, y = 0) => () =>
  ({
    width: 0,
    height: 0,
    top: y,
    right: x,
    bottom: y,
    left: x,
  } as ClientRect | DOMRect);
