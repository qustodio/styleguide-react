import * as React from 'react';
import useAssignRef from '../../hooks/useAssignRef';
import { TooltipProps } from './types/Tooltip.types';
import AutoPortal from '../AutoPortal/AutoPortal';
import getTouchPointHandlers, {
  TouchInteractionHandler,
} from '../../helpers/getTouchPointHandlers';
import useToggle from '../../hooks/useToggle';
import {
  generateGetBoundingClientRect,
  getTooltipContent,
  getTooltipTransitionClass,
} from './helpers';
import usePopover from '../../hooks/usePopover/usePopover';
import { classNames, getTestId } from '../../helpers';

const TooltipTest = {
  prefix: 'Tooltip',
};

/**
 * Tooltip
 *
 * @example
 *```tsx
 * import { Tooltip } from 'styleguide-react';
 *
 * <Tooltip title="Title" label="label" placement="top">
 *      <span>Open Tooltip</span>
 * </Tooltip>
 *```
 */
const Tooltip = React.forwardRef<Element, TooltipProps>(
  (
    {
      id,
      className,
      triggerClassName,
      testId,
      children,
      label,
      title,
      hasArrow = true,
      isOpen: isAlwaysOpen = false,
      disableForHoverTouch = false,
      disableForFocusBlur = false,
      smoothTransition = false,
      isDisabled = false,
      isMobile = false,
      coords,
      keepMounted = false,
      'aria-live': ariaLive = 'off',
      ...popperParams
    },
    forwardedRef
  ) => {
    const virtualElement = React.useRef({
      getBoundingClientRect: generateGetBoundingClientRect(),
    });
    const triggerWrapperRef = React.useRef<HTMLDivElement | null>(null);

    const [isOpen, { open, close }] = useToggle(false);
    const [shadowOpenValue, setShadowOpenValue] = React.useState(false);

    const interactionHandler: TouchInteractionHandler<Event> = action => () => {
      if (action === 'in') {
        open();
        setShadowOpenValue(true);
      }
      if (action === 'out') {
        if (!keepMounted) close();
        setShadowOpenValue(false);
      }
    };

    // Obtain the event handlers props
    const { touchEvents, focusEvents } = getTouchPointHandlers(
      interactionHandler,
      isMobile
    );

    // Create a unique id for each new tooltip (unless an `id` is provided)
    const tooltipId = getTestId(
      TooltipTest.prefix,
      id ?? (+Date.now()).toString()
    );

    const {
      popperRef,
      getReferenceProps,
      getArrowProps,
      getPopperProps,
      update,
      targetRef,
    } = usePopover(popperParams);

    const ownPopperRefHandler = useAssignRef(popperRef, forwardedRef);

    // Tooltip should have only one child node is it exists
    const child = (children
      ? React.Children.only(children)
      : null) as React.ReactElement & {
      ref?: React.Ref<unknown>;
    };

    const touchEventsProps =
      !isAlwaysOpen && !disableForHoverTouch ? touchEvents : {};
    const focusEventsProps =
      !isAlwaysOpen && !disableForFocusBlur ? focusEvents : {};

    const handlersProps = { ...touchEventsProps, ...focusEventsProps };
    const ariaProps = {
      ...((isAlwaysOpen || shadowOpenValue) && {
        // Silence the tooltip for screen readers when aria-live is "off".
        // This prevents duplicate announcements when the child trigger element
        // already has an aria-label.
        'aria-describedby': ariaLive === 'off' ? undefined : tooltipId,
      }),
    };
    const triggerWrapperProps = getReferenceProps(
      { ...handlersProps, ...ariaProps },
      triggerWrapperRef
    );

    React.useEffect(() => {
      if (coords) targetRef(virtualElement.current);
    }, []);

    // Close tooltip on ESC key press
    React.useEffect(() => {
      const handleKeyDown = (e: KeyboardEvent) => {
        if (e.key === 'Escape') {
          interactionHandler('out')(e);
        }
      };

      document.addEventListener('keydown', handleKeyDown);
      return () => document.removeEventListener('keydown', handleKeyDown);
    }, []);

    React.useEffect(() => {
      if (coords?.x && coords.y) {
        virtualElement.current.getBoundingClientRect = generateGetBoundingClientRect(
          coords.x,
          coords.y
        );
        update();
      }
    }, [coords?.x, coords?.y]);

    const isMountedButHidden = !isAlwaysOpen && keepMounted && !shadowOpenValue;
    const popperProps = getPopperProps(
      {
        className: classNames(
          'rck-tooltip',
          isMountedButHidden ? 'rck-tooltip--hidden' : '',
          className || '',
          getTooltipTransitionClass(smoothTransition)
        ),
      },
      ownPopperRefHandler
    );

    const arrowProps = getArrowProps();

    if ((!label && !title) || isDisabled) {
      return <>{children}</>;
    }

    const shouldShowTooltip = isAlwaysOpen || isOpen;

    return (
      <>
        {/**
         * Most of our React components don't support arbitrary props, preventing us
         * from passing event handlers and refs directly to the trigger/child element.
         *
         * To work around this, we wrap the trigger in a <div> and attach the ref and
         * event handlers to it instead.
         *
         * Assigning the `ref` to the trigger is crucial, as it determines the correct
         * placement of the Tooltip.
         */}
        <div
          tabIndex={-1}
          className={classNames('rck-tooltip__trigger', triggerClassName)}
          {...triggerWrapperProps}
        >
          {child}
        </div>
        {shouldShowTooltip && (
          <AutoPortal id={id || 'tooltip-portal'} className="">
            <div
              role="tooltip"
              id={tooltipId}
              data-testid={getTestId(TooltipTest.prefix, testId)}
              tabIndex={-1}
              aria-live={ariaLive}
              {...popperProps}
            >
              {getTooltipContent(label, title)}
              {hasArrow && (
                <div className="rck-tooltip__arrow" {...arrowProps} />
              )}
            </div>
          </AutoPortal>
        )}
      </>
    );
  }
);

Tooltip.displayName = 'Tooltip';

export default Tooltip;
