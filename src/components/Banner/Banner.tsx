import React, { SyntheticEvent } from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconSize, IconType } from '../Icons';

export type BannerType = 'warning' | 'primary' | 'error' | 'white';

export interface BannerProps extends BaseComponentProps {
  type: BannerType;
  icon?: JSX.Element;
  showCloseIcon?: JSX.Element;
  centered?: boolean;
  fullWidth?: boolean;
  rounded?: boolean;
  onClose?: (ev: SyntheticEvent) => void;
}

export enum BannerTest {
  prefix = 'Banner',
  actionElement = 'Action__close',
}

const Banner: React.FunctionComponent<BannerProps> = ({
  type,
  icon,
  showCloseIcon,
  centered,
  fullWidth,
  rounded = true,
  testId,
  className = '',
  children,
  onClose,
}) => (
  <div
    className={classNames(
      'rck-banner',
      `rck-banner--${type}`,
      fullWidth ? 'rck-banner--full-width' : '',
      !rounded ? 'rck-banner--no-rounded' : '',
      className
    )}
    data-testid={getTestId(BannerTest.prefix, testId)}
  >
    {icon && <div className="rck-banner__icon">{icon}</div>}
    <div
      className={classNames(
        'rck-banner__text',
        centered ? `rck-banner__text--centered` : ''
      )}
    >
      {children}
    </div>
    {showCloseIcon && (
      <div
        className="rck-banner__icon-right"
        onClick={onClose}
        role="button"
        onKeyDown={onClose}
        tabIndex={0}
        data-testid={getTestId(
          BannerTest.prefix,
          testId,
          BannerTest.actionElement
        )}
      >
        <Icon type={IconType.times} size={IconSize.lg} />
      </div>
    )}
  </div>
);

Banner.displayName = 'Banner';

export default React.memo(Banner);
