import * as React from 'react';
import { getTestId } from '../../helpers';
import { FlexLayoutProps } from './types';
import flexLayoutClassNames from './helpers/flexLayoutClassNames';

export enum FlexLayoutTest {
  prefix = 'FlexLayout',
}

const FlexLayout: React.FunctionComponent<FlexLayoutProps> = React.forwardRef<
  HTMLDivElement,
  FlexLayoutProps
>((props, ref) => {
  const {
    mainaxis,
    wrap,
    mainaxisAlignment,
    crossaxisAlignment,
    crossaxisLineSpacing,
    marginLeft,
    marginRight,
    marginTop,
    marginBottom,
    paddingLeft,
    paddingRight,
    paddingTop,
    paddingBottom,
    margin,
    padding,
    minHeight,
    minWidth,
    maxHeight,
    maxWidth,
    width,
    height,
    noWrap,
    lineHeight,
    hasBox,
    order,
    flexGrow,
    flexShrink,
    flexBasis,
    alignSelf,
    gap,
    matchParentHeight = false,
    display = 'flex',
    position,
    left,
    right,
    bottom,
    top,
    scrollY,
    backgroundColor,
    textAlignement,
    className = '',
    children,
    testId,
  } = props;

  return (
    <div
      ref={ref}
      style={{
        left,
        right,
        top,
        bottom,
      }}
      className={flexLayoutClassNames(
        mainaxis,
        { lineHeight, noWrap },
        {
          marginBottom,
          marginLeft,
          marginRight,
          marginTop,
          paddingBottom,
          paddingLeft,
          paddingRight,
          paddingTop,
          padding,
          margin,
          minHeight,
          minWidth,
          maxHeight,
          maxWidth,
          width,
          height,
        },
        {
          order,
          flexGrow,
          flexShrink,
          flexBasis,
          alignSelf,
          gap,
        },
        wrap,
        mainaxisAlignment,
        crossaxisAlignment,
        crossaxisLineSpacing,
        matchParentHeight,
        display,
        position,
        hasBox,
        scrollY,
        backgroundColor,
        textAlignement,
        className
      )}
      data-testid={getTestId(FlexLayoutTest.prefix, testId)}
    >
      {children}
    </div>
  );
});

FlexLayout.displayName = 'FlexLayout';

export default FlexLayout;
