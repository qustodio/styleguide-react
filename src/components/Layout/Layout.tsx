import * as React from 'react';
import { getTestId } from '../../helpers';
import { LayoutProps } from './types';
import layoutClassNames from './helpers/layoutClassNames';

export enum LayoutTest {
  prefix = 'Layout',
}

const Layout: React.FunctionComponent<LayoutProps> = React.forwardRef<
  HTMLDivElement,
  LayoutProps
>((props, ref) => {
  const {
    display,
    position,
    left,
    right,
    top,
    bottom,
    marginLeft,
    marginRight,
    marginTop,
    marginBottom,
    paddingLeft,
    paddingRight,
    paddingTop,
    paddingBottom,
    margin,
    padding,
    minHeight,
    minWidth,
    maxHeight,
    maxWidth,
    width,
    height,
    noWrap,
    lineHeight,
    scrollY,
    centerX,
    backgroundColor,
    renderAs: Tag = 'div',
    textAlignement,
    className = '',
    children,
    testId,
  } = props;

  return (
    <Tag
      ref={ref}
      style={{
        left,
        right,
        top,
        bottom,
      }}
      className={layoutClassNames(
        { lineHeight, noWrap },
        {
          marginBottom,
          marginLeft,
          marginRight,
          marginTop,
          paddingBottom,
          paddingLeft,
          paddingRight,
          paddingTop,
          padding,
          margin,
          minHeight,
          minWidth,
          maxHeight,
          maxWidth,
          width,
          height,
        },
        display,
        position,
        scrollY,
        backgroundColor,
        textAlignement,
        centerX,
        className
      )}
      data-testid={getTestId(LayoutTest.prefix, testId)}
    >
      {children}
    </Tag>
  );
});

Layout.displayName = 'Layout';

export default Layout;
