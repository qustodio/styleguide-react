import memoize from 'memoizee';
import { classNames } from '../../../helpers';
import { FlexItemProps } from '../types';

const flexItemClassNames = ({
  order,
  flexBasis,
  flexGrow,
  flexShrink,
  alignSelf,
  gap,
}: FlexItemProps): string => {
  const hasProps =
    order ?? flexBasis ?? flexGrow ?? flexShrink ?? alignSelf ?? gap;

  if (!hasProps) return '';

  return classNames(
    'rck-layout-item',
    'rck-flex-layout-item',
    order ? `rck-flex-layout-item--order-${order}` : '',
    flexBasis
      ? `rck-flex-layout-item--flex-basis-${flexBasis.replace('%', 'pc')}`
      : '',
    flexGrow
      ? `rck-flex-layout-item--flex-grow-${flexGrow.replace('%', 'pc')}`
      : '',
    flexShrink
      ? `rck-flex-layout-item--flex-shrink-${flexShrink.replace('%', 'pc')}`
      : '',
    alignSelf ? `rck-flex-layout-item--flex-alignself-${alignSelf}` : '',
    gap ? `rck-flex-layout-item--gap-${gap}` : ''
  );
};

export default memoize(flexItemClassNames);
