import memoize from 'memoizee';
import { classNames } from '../../../helpers';
import { LayoutTextProps } from '../types';

const layoutTextClassNames = ({
  lineHeight,
  noWrap,
}: LayoutTextProps): string =>
  classNames(
    lineHeight ? `rck-layout--line-height-${lineHeight.replace('.', '-')}` : '',
    noWrap ? 'rck-layout--nowrap' : ''
  );

export default memoize(layoutTextClassNames);
