import memoize from 'memoizee';
import { classNames } from '../../../helpers';
import { LayoutSpacingProps } from '../types';

const spacingClassNames = ({
  marginLeft,
  marginBottom,
  marginTop,
  marginRight,
  paddingLeft,
  paddingRight,
  paddingTop,
  paddingBottom,
  padding,
  margin,
  minHeight,
  maxHeight,
  minWidth,
  maxWidth,
  height,
  width,
}: LayoutSpacingProps): string =>
  classNames(
    padding ? `rck-layout--padding-spacing-${padding}` : '',
    margin ? `rck-layout--margin-spacing-${margin}` : '',
    marginLeft ? `rck-layout--margin-left-spacing-${marginLeft}` : '',
    marginRight ? `rck-layout--margin-right-spacing-${marginRight}` : '',
    marginTop ? `rck-layout--margin-top-spacing-${marginTop}` : '',
    marginBottom ? `rck-layout--margin-bottom-spacing-${marginBottom}` : '',
    paddingLeft ? `rck-layout--padding-left-spacing-${paddingLeft}` : '',
    paddingRight ? `rck-layout--padding-right-spacing-${paddingRight}` : '',
    paddingTop ? `rck-layout--padding-top-spacing-${paddingTop}` : '',
    paddingBottom ? `rck-layout--padding-bottom-spacing-${paddingBottom}` : '',
    minHeight ? `rck-layout--min-height-${minHeight.replace('%', 'pc')}` : '',
    maxHeight ? `rck-layout--max-height-${maxHeight.replace('%', 'pc')}` : '',
    minWidth ? `rck-layout--min-width-${minWidth.replace('%', 'pc')}` : ``,
    maxWidth ? `rck-layout--max-width-${maxWidth.replace('%', 'pc')}` : '',
    height ? `rck-layout--height-${height.replace('%', 'pc')}` : '',
    width ? `rck-layout--width-${width.replace('%', 'pc')}` : ''
  );

export default memoize(spacingClassNames);
