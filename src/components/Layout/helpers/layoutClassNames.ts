import memoize from 'memoizee';
import { classNames } from '../../../helpers';
import {
  LayoutColors,
  LayoutDisplay,
  LayoutPosition,
  LayoutSpacingProps,
  LayoutTextAlignement,
  LayoutTextProps,
} from '../types';

import layoutTextClassNames from './layoutTextClassNames';
import spacingClassNames from './spacingClassNames';

const getCentered = (centerX?: boolean, position?: LayoutPosition) => {
  if (!centerX) return '';
  return centerX && (position === 'absolute' || position === 'fixed')
    ? 'rck-layout--center-x-absolute'
    : 'rck-layout--center-x';
};

const layoutClassNames = (
  layoutTextProps: LayoutTextProps,
  layoutSpacingProps: LayoutSpacingProps,
  display?: LayoutDisplay,
  position = 'inherit' as LayoutPosition,
  scrollY?: boolean,
  backgroundColor?: LayoutColors,
  textAlignement?: LayoutTextAlignement,
  centerX?: boolean,
  className = ''
) =>
  classNames(
    'rck-layout',
    spacingClassNames(layoutSpacingProps),
    layoutTextClassNames(layoutTextProps),
    display ? `rck-layout--display-${display}` : '',
    position !== 'inherit' ? `rck-layout--position-${position}` : '',
    scrollY ? 'rck-layout--scroll-y' : '',
    backgroundColor ? `rck-layout--bck-color-${backgroundColor}` : '',
    textAlignement ? `rck-layout--text-alignement-${textAlignement}` : '',
    getCentered(centerX, position),
    className
  );

export default memoize(layoutClassNames);
