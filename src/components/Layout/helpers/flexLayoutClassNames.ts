import memoize from 'memoizee';
import { classNames } from '../../../helpers';
import {
  FlexItemProps,
  FlexLayoutCrossaxisAlignment,
  FlexLayoutCrossaxisLineSpacing,
  FlexLayoutDisplay,
  FlexLayoutMainaxis,
  FlexLayoutMainaxisAlignment,
  FlexLayoutWrap,
  LayoutColors,
  LayoutSpacingProps,
  LayoutTextAlignement,
  LayoutTextProps,
} from '../types';
import flexItemClassNames from './flexItemClassNames';
import layoutTextClassNames from './layoutTextClassNames';
import spacingClassNames from './spacingClassNames';

const flexLayoutClassNames = (
  mainaxis: FlexLayoutMainaxis,
  layoutTextProps: LayoutTextProps,
  layoutSpacingProps: LayoutSpacingProps,
  layoutflexItemProps: FlexItemProps,
  wrap?: FlexLayoutWrap,
  mainaxisAlignment?: FlexLayoutMainaxisAlignment,
  crossaxisAlignment?: FlexLayoutCrossaxisAlignment,
  crossaxisLineSpacing?: FlexLayoutCrossaxisLineSpacing,
  matchParentHeight?: boolean,
  display?: FlexLayoutDisplay,
  position = 'inherit',
  hasBox?: boolean,
  scrollY?: boolean,
  backgroundColor?: LayoutColors,
  textAlignement?: LayoutTextAlignement,
  className = ''
): string =>
  classNames(
    'rck-layout',
    'rck-flex-layout',
    mainaxis ? `rck-flex-layout--main-axis-${mainaxis}` : '',
    wrap ? `rck-flex-layout--wrap-${wrap}` : '',
    mainaxisAlignment
      ? `rck-flex-layout--main-axis-alignment-${mainaxisAlignment}`
      : '',
    crossaxisAlignment
      ? `rck-flex-layout--cross-axis-alignment-${crossaxisAlignment}`
      : '',
    crossaxisLineSpacing
      ? `rck-flex-layout--cross-axis-line-spacing${crossaxisLineSpacing}`
      : '',
    matchParentHeight ? 'rck-flex-layout--match-parent-height' : '',
    spacingClassNames(layoutSpacingProps),
    flexItemClassNames(layoutflexItemProps),
    layoutTextClassNames(layoutTextProps),
    display ? `rck-flex-layout--${display}` : '',
    position !== 'inherit' ? `rck-layout--position-${position}` : '',
    scrollY ? 'rck-layout--scroll-y' : '',
    hasBox ? 'rck-layout--box' : '',
    backgroundColor ? `rck-layout--bck-color-${backgroundColor}` : '',
    textAlignement ? `rck-layout--text-alignement-${textAlignement}` : '',
    className
  );

export default memoize(flexLayoutClassNames);
