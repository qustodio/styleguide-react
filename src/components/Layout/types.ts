import React from 'react';

import { BaseComponentProps } from '../../common/types';

export type LayoutSpacing =
  | '0'
  | '4'
  | '8'
  | '16'
  | '24'
  | '32'
  | '40'
  | '48'
  | '56'
  | '72'
  | '80'
  | '88'
  | '96'
  | '104'
  | '112'
  | '160'
  | '200';

export type FlexLayoutMainaxis =
  | 'row'
  | 'column'
  | 'row-reverse'
  | 'column-reverse';

export type FlexLayoutWrap = 'wrap' | 'nowrap' | 'wrap-reverse';

export type FlexLayoutMainaxisAlignment =
  | 'flex-start'
  | 'flex-end'
  | 'start'
  | 'end'
  | 'left'
  | 'right'
  | 'center'
  | 'space-between'
  | 'space-around'
  | 'space-evenly';

export type FlexLayoutCrossaxisAlignment =
  | 'stretch'
  | 'flex-start'
  | 'start'
  | 'self-start'
  | 'flex-end'
  | 'end'
  | 'self-end'
  | 'center'
  | 'baseline';

export type FlexLayoutCrossaxisLineSpacing =
  | 'normal'
  | 'flex-start'
  | 'start'
  | 'flex-end'
  | 'end'
  | 'center'
  | 'space-between'
  | 'space-around'
  | 'space-evenly'
  | 'stretch';

export type FlexLayoutFlexOptions =
  | '0'
  | '1'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9';

export type FlexLayoutFlexBasisOptions =
  | '0'
  | 'auto'
  | 'max-content'
  | 'min-content'
  | 'fit-content'
  | '25%'
  | '50%'
  | '75%'
  | '100%';

export type LayoutContentSpacing =
  | '0'
  | '24'
  | '32'
  | '40'
  | '48'
  | '56'
  | '72'
  | '80'
  | '104'
  | '120'
  | '160'
  | '200'
  | '240'
  | '280'
  | '296'
  | '320'
  | '360'
  | '400'
  | '440'
  | '480'
  | '520'
  | '584'
  | '800'
  | '1000'
  | '25%'
  | '50%'
  | '75%'
  | '100%'
  | '25vh'
  | '50vh'
  | '75vh'
  | '100vh'
  | '25vw'
  | '50vw'
  | '75vw'
  | '100vw'
  | 'auto';

export type LayoutTextAlignement = 'center' | 'left' | 'right' | 'revert';

export type LayoutColors = 'white' | 'gray-ligther' | 'transparent';

export interface LayoutSpacingProps {
  marginLeft?: LayoutSpacing;
  marginRight?: LayoutSpacing;
  marginTop?: LayoutSpacing;
  marginBottom?: LayoutSpacing;
  paddingLeft?: LayoutSpacing;
  paddingRight?: LayoutSpacing;
  paddingTop?: LayoutSpacing;
  paddingBottom?: LayoutSpacing;
  padding?: LayoutSpacing;
  margin?: LayoutSpacing;
  minWidth?: LayoutContentSpacing;
  maxWidth?: LayoutContentSpacing;
  minHeight?: LayoutContentSpacing;
  maxHeight?: LayoutContentSpacing;
  height?: LayoutContentSpacing;
  width?: LayoutContentSpacing;
}

export interface LayoutBoxProps {
  hasBox?: boolean;
}

export interface FlexItemProps {
  order?: FlexLayoutFlexOptions;
  flexGrow?: FlexLayoutFlexOptions;
  flexShrink?: FlexLayoutFlexOptions;
  flexBasis?: FlexLayoutFlexBasisOptions;
  alignSelf?: FlexLayoutCrossaxisAlignment;
  gap?: LayoutSpacing;
}
export interface FlexLayoutItemProps
  extends FlexItemProps,
    BaseComponentProps {}

export interface LayoutScrollProps {
  scrollY?: boolean;
}

export type LineHeightProps =
  | '1'
  | '1.1'
  | '1.2'
  | '1.3'
  | '1.4'
  | '1.5'
  | '1.6'
  | '1.7'
  | '1.8'
  | '1.9'
  | '2';
export interface LayoutTextProps {
  noWrap?: boolean;
  lineHeight?: LineHeightProps;
}

export type FlexLayoutDisplay = 'flex' | 'inline-flex' | 'contents';

export type LayoutPosition = 'inherit' | 'relative' | 'absolute' | 'fixed';
export interface LayoutPositionProps {
  position?: LayoutPosition;
  left?: number;
  right?: number;
  top?: number;
  bottom?: number;
}
export interface FlexLayoutProps
  extends LayoutBoxProps,
    LayoutSpacingProps,
    FlexLayoutItemProps,
    LayoutTextProps,
    LayoutPositionProps,
    LayoutScrollProps,
    FlexItemProps,
    BaseComponentProps {
  mainaxis: FlexLayoutMainaxis;
  wrap?: FlexLayoutWrap;
  mainaxisAlignment?: FlexLayoutMainaxisAlignment;
  crossaxisAlignment?: FlexLayoutCrossaxisAlignment;
  crossaxisLineSpacing?: FlexLayoutCrossaxisLineSpacing;
  matchParentHeight?: boolean;
  display?: FlexLayoutDisplay;
  backgroundColor?: LayoutColors;
  textAlignement?: LayoutTextAlignement;
  ref?: React.Ref<HTMLDivElement>;
}

export type LayoutRenderElement = 'span' | 'div';

export type LayoutDisplay = 'block' | 'inline' | 'inline-block';
export interface LayoutProps
  extends LayoutSpacingProps,
    LayoutTextProps,
    LayoutPositionProps,
    LayoutScrollProps,
    BaseComponentProps {
  renderAs?: LayoutRenderElement;
  display?: LayoutDisplay;
  backgroundColor?: LayoutColors;
  textAlignement?: LayoutTextAlignement;
  centerX?: boolean;
  ref?: React.Ref<HTMLDivElement>;
}
