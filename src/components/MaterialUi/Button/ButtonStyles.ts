import { makeStyles, Theme } from '@material-ui/core/styles';
import { Spacing } from '@material-ui/core/styles/createSpacing';

const shadowHover = '4px';

const sizeStyle = (spacing: Spacing, padding: number) => ({
  padding: `0 ${spacing(padding)}px`,
  lineHeight: `${spacing(padding * 2)}px`,
  height: `${spacing(padding * 2)}px`,
});

const styles = (theme: Theme) =>
  makeStyles({
    root: {
      fontWeight: 600,
      ...sizeStyle(theme.spacing, 3),
    },
    containedSizeSmall: sizeStyle(theme.spacing, 2),
    outlinedSizeSmall: sizeStyle(theme.spacing, 2),
    containedSizeLarge: sizeStyle(theme.spacing, 4),
    outlinedSizeLarge: sizeStyle(theme.spacing, 4),
    rounded: {
      borderRadius: '30px',
      height: '60px',
      lineHeight: '60px',
      padding: '0 30px',
    },
    roundedSizeSmall: {
      padding: '0 25px',
      fontSize: '14px',
      height: '46px',
      lineHeight: '46px',
      borderRadius: '23px',
    },
    roundedSizeLarge: {
      padding: '0 50px',
      fontSize: '19px',
      height: '72px',
      lineHeight: '72px',
      borderRadius: '36px',
    },
    contained: {
      color: '#FFF',
    },
    containedPrimary: {
      '&:hover': {
        textDecoration: 'none',
        boxShadow: `0 0 0 ${shadowHover} ${theme.palette.primary.light}`,
        backgroundColor: theme.palette.primary.main,
      },
    },
    containedSecondary: {
      '&:hover': {
        textDecoration: 'none',
        boxShadow: `0 0 0 ${shadowHover} ${theme.palette.secondary.light}`,
        backgroundColor: theme.palette.secondary.main,
      },
    },
    containedWarning: {
      backgroundColor: theme.palette.warning.main,
      '&:hover': {
        textDecoration: 'none',
        boxShadow: `0 0 0 ${shadowHover} ${theme.palette.warning.light}`,
        backgroundColor: theme.palette.primary.main,
      },
    },
    containedError: {
      backgroundColor: theme.palette.error.main,
      '&:hover': {
        textDecoration: 'none',
        boxShadow: `0 0 0 ${shadowHover} ${theme.palette.error.light}`,
        backgroundColor: theme.palette.error.main,
      },
    },
    outlinedWarning: {
      border: `1px solid ${theme.palette.warning.main}`,
      color: theme.palette.warning.main,
    },
    outlinedError: {
      border: `1px solid ${theme.palette.error.main}`,
      color: theme.palette.error.main,
    },
    textWarning: {
      color: theme.palette.warning.main,
    },
    textError: {
      color: theme.palette.error.main,
    },
  });

export default styles;
