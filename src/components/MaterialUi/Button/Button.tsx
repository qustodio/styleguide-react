import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import theme from '../../../muiTheme';
import { classNames } from '../../../helpers';
import styles from './ButtonStyles';
import { DisabledProps, BaseComponentProps } from '../../../common/types';

export type MuiButtonSize = 'small' | 'medium' | 'large';

export interface MuiButtonProps extends BaseComponentProps, DisabledProps {
  rounded?: boolean;
  fullWidth?: boolean;
  type: 'primary' | 'inherit' | 'default' | 'secondary' | undefined; // We need to investigate how extend button color type
  variant: 'text' | 'contained' | 'outlined';
  size: MuiButtonSize;
  onClick?: () => void;
}

const MuiButton: React.FunctionComponent<MuiButtonProps> = ({
  children,
  rounded = false,
  disabled = false,
  fullWidth = false,
  type = 'primary',
  variant = 'text',
  size = 'medium',
  className = '',
  onClick = undefined,
}) => {
  const classes = styles(theme)();

  return (
    <ThemeProvider theme={theme}>
      <Button
        className={classNames(
          rounded ? classes.rounded : '',
          rounded && size === 'large' ? classes.roundedSizeLarge : '',
          rounded && size === 'small' ? classes.roundedSizeSmall : '',
          className
        )}
        classes={classes}
        variant={variant}
        size={size}
        fullWidth={fullWidth}
        disabled={disabled}
        color={type}
        disableElevation
        onClick={onClick}
      >
        {children}
      </Button>
    </ThemeProvider>
  );
};

export default MuiButton;
