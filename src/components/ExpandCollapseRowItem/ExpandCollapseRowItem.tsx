import * as React from 'react';
import Text from '../Text';
import { classNames, getTestId } from '../../helpers';
import TransparentButton from '../Button/TransparentButton';
import FlexLayout from '../Layout';
import Icon, { IconColor, IconType } from '../Icons';

interface ExpandCollapseRowItemProps {
  iconSrc?: string;
  title: string;
  subtitle: JSX.Element;
  rightElement: JSX.Element;
  className?: string;
  testId?: string;
  isDisabled?: boolean;
  onInfoActionClick?: () => void;
}

export enum ExpandableRowItemTest {
  prefix = 'Expandable-row-item',
  title = 'Title',
  rightElement = 'Right-element',
}

const ExpandCollapseRowItem = ({
  iconSrc,
  title,
  subtitle,
  rightElement,
  className,
  testId,
  isDisabled,
  onInfoActionClick,
}: ExpandCollapseRowItemProps) => {
  const leftSide = (
    <div className="rck-expand-collapse-row-item__left-element">
      {iconSrc && (
        <img
          src={iconSrc}
          alt={title}
          className={classNames(
            'rck-expand-collapse-row-item__icon',
            isDisabled ? 'rck-expand-collapse-row-item__icon--disabled' : ''
          )}
        />
      )}
      <div
        className="rck-expand-collapse-row-item__title"
        data-testId={getTestId(
          ExpandableRowItemTest.prefix,
          testId,
          ExpandableRowItemTest.title
        )}
      >
        <FlexLayout mainaxis="row">
          <Text variant="body-1-semibold">{title}</Text>
          {onInfoActionClick ? (
            <Icon
              type={IconType.infoCircle}
              color={isDisabled ? IconColor.disabled : IconColor.secondary}
              className={classNames(
                'rck-expand-collapse-row-item__action-icon',
                isDisabled
                  ? 'rck-expand-collapse-row-item__action-icon--disabled'
                  : ''
              )}
            />
          ) : null}
        </FlexLayout>
        {subtitle && (
          <span className="rck-expand-collapse-row-item__subtitle">
            {subtitle}
          </span>
        )}
      </div>
    </div>
  );

  const onClickInfo = (e: React.SyntheticEvent) => {
    e.stopPropagation();
    if (isDisabled || !onInfoActionClick) return;
    onInfoActionClick();
  };

  return (
    <div
      className={classNames('rck-expand-collapse-row-item', className)}
      data-testId={getTestId(ExpandableRowItemTest.prefix, testId)}
    >
      {onInfoActionClick && !isDisabled ? (
        <TransparentButton
          onClick={onClickInfo}
          className="rck-expand-collapse-row-item__action-button"
        >
          {leftSide}
        </TransparentButton>
      ) : (
        leftSide
      )}
      <div
        className="rck-expand-collapse-row-item__right-element"
        data-testId={getTestId(
          ExpandableRowItemTest.prefix,
          testId,
          ExpandableRowItemTest.rightElement
        )}
      >
        {rightElement}
      </div>
    </div>
  );
};

export default ExpandCollapseRowItem;
