import React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';
import AffiliateBadge from '../Badge/AffiliateBadge';
import FlexLayout from '../Layout';
import Link from '../Link/Link';
import { AnchorPropTypes } from '../../adapters/router/Anchor';

export interface LicenseInfoContainerProps extends BaseComponentProps {
  affiliateBadgeIconSrc?: string;
  affiliateBadgeDescription?: string;
  licenseTitle?: string;
  licenseSubtitle?: string;
  licenseBadgeText?: string;
  licenseBadgeClick?: () => void;
  licenseLinkTo?: string;
  useAsLink?: React.FunctionComponent<AnchorPropTypes>;
}

export enum LicenseInfoContainerTest {
  prefix = 'LicenseInfoContainer',
}

const LicenseInfoContainer: React.FunctionComponent<LicenseInfoContainerProps> = ({
  className = '',
  affiliateBadgeDescription,
  affiliateBadgeIconSrc = '',
  licenseTitle,
  licenseSubtitle,
  licenseBadgeText,
  licenseBadgeClick,
  licenseLinkTo,
  useAsLink,
  testId,
}) => (
  <div
    className={classNames('rck-license-info-container', className)}
    datatest-id={getTestId(LicenseInfoContainerTest.prefix, testId)}
  >
    <ul className="rck-license-info-container__optionals">
      {affiliateBadgeIconSrc ? (
        <li className="rck-license-info-container__affiliate-badge">
          <AffiliateBadge
            iconSrc={affiliateBadgeIconSrc}
            description={affiliateBadgeDescription}
          />
        </li>
      ) : (
        <Link href={licenseLinkTo} useAsLink={useAsLink}>
          {licenseTitle && (
            <li className="rck-license-info-container__license-title">
              <span>{licenseTitle}</span>
            </li>
          )}
          {licenseSubtitle && (
            <li className="rck-license-info-container__license-subtitle">
              <span>{licenseSubtitle}</span>
            </li>
          )}
          {licenseBadgeText && licenseBadgeClick && (
            <li>
              <FlexLayout mainaxis="row" mainaxisAlignment="center">
                <button
                  className="rck-license-info-container__button"
                  type="button"
                  onClick={licenseBadgeClick}
                >
                  <span>{licenseBadgeText}</span>
                </button>
              </FlexLayout>
            </li>
          )}
        </Link>
      )}
    </ul>
  </div>
);

LicenseInfoContainer.displayName = 'LicenseInfoContainer';

export default LicenseInfoContainer;
