import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export enum ScrollableContentTest {
  prefix = 'ScrollableContent',
  scroll = 'Scroll',
}

export interface ScrollableContentProps extends BaseComponentProps {
  height?: number;
  hideScrollbar?: boolean;
}

const ScrollableContent: React.FunctionComponent<ScrollableContentProps> = ({
  height,
  hideScrollbar,
  className = '',
  testId,
  children,
}) => (
  <div
    className={classNames('rck-scrollable-content', className)}
    data-testid={getTestId(ScrollableContentTest.prefix, testId)}
    style={{ height }}
  >
    <div
      className={classNames(
        'rck-scrollable-content__scroll',
        hideScrollbar ? 'rck-scrollable-content__scroll--no-scrollbar' : ''
      )}
      data-testid={getTestId(
        ScrollableContentTest.prefix,
        testId,
        ScrollableContentTest.scroll
      )}
    >
      {children}
    </div>
  </div>
);

ScrollableContent.displayName = 'ScrollableContent';

export default ScrollableContent;
