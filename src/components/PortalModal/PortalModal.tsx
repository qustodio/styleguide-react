/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */

import * as React from 'react';
import { RemoveScroll } from 'react-remove-scroll';
import Portal from '../Portal/Portal';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconType, IconFamily } from '../Icons';
import { TestableComponentProps } from '../../common/types';
import FlexLayout from '../Layout';

export interface PortalModalProps extends TestableComponentProps {
  overlayId?: string;
  overlayClassName?: string;
  modalWrapperClassName?: string;
  modalWrapperWidth?: string | number;
  modalWrapperHeight?: string | number;
  modalClassName?: string;
  closeButtonClassName?: string;
  animationEnabled?: boolean;
  hideCloseButton?: boolean;
  showBackButton?: boolean;
  onClose: () => void;
}

export enum PortalModalTest {
  prefix = 'PortalModal',
  closeButton = 'Action-close',
  backButton = 'Action-back',
}

class PortalModal extends React.Component<PortalModalProps> {
  static readonly portalId = 'styleguide-react-portal-modal';

  handleClose = (evt: React.SyntheticEvent): void => {
    const { onClose } = this.props;
    this.stopEventPropagation(evt);
    onClose();
  };

  stopEventPropagation = (evt: React.SyntheticEvent): void => {
    evt.stopPropagation();
  };

  render(): JSX.Element {
    const {
      overlayId,
      overlayClassName = '',
      modalWrapperClassName = '',
      modalWrapperWidth,
      modalWrapperHeight,
      closeButtonClassName = '',
      animationEnabled = true,
      hideCloseButton = false,
      showBackButton = false,
      children,
      testId,
    } = this.props;

    return (
      <Portal name={PortalModal.portalId}>
        <RemoveScroll>
          <div
            id={overlayId}
            className={`rck-portal-modal__overlay ${overlayClassName}`}
            onClick={this.handleClose}
            data-testid={getTestId(PortalModalTest.prefix, testId)}
          >
            <div
              className={`rck-portal-modal__wrapper ${
                animationEnabled ? 'rck-portal-modal__wrapper--animated' : ''
              } ${modalWrapperClassName}`}
              style={{
                width: modalWrapperWidth,
                height: modalWrapperHeight,
              }}
              onClick={this.stopEventPropagation}
              role="dialog"
            >
              <FlexLayout mainaxis="row">
                <span
                  className={classNames(
                    'rck-portal-modal__back-btn',
                    !showBackButton ? 'rck-portal-modal__back-btn--hide' : ''
                  )}
                >
                  <Icon
                    type={IconType.arrowLeft}
                    family={IconFamily.regular}
                    className={classNames(
                      /* 'rck-portal-modal__back-btn', */
                      closeButtonClassName || ''
                    )}
                    onClick={this.handleClose}
                    ariaLabel="back"
                    testId={getTestId(
                      PortalModalTest.prefix,
                      testId,
                      PortalModalTest.backButton
                    )}
                  />
                </span>

                <span
                  className={classNames(
                    'rck-portal-modal__close-btn',
                    hideCloseButton ? 'rck-portal-modal__close-btn--hide' : ''
                  )}
                >
                  <Icon
                    type={IconType.times}
                    family={IconFamily.regular}
                    className={classNames(
                      /* 'rck-portal-modal__close-btn', */
                      closeButtonClassName || ''
                    )}
                    onClick={this.handleClose}
                    ariaLabel="close"
                    testId={getTestId(
                      PortalModalTest.prefix,
                      testId,
                      PortalModalTest.closeButton
                    )}
                  />
                </span>
              </FlexLayout>
              {children}
            </div>
          </div>
        </RemoveScroll>
      </Portal>
    );
  }
}

export default PortalModal;
