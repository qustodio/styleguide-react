import React from 'react';
import { RckShadow } from '../../common/shadow.types';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';

export interface ShadowedCardProps extends BaseComponentProps {
  shadow?: RckShadow;
  children?: React.ReactNode;
}

export enum ShadowedCardTest {
  prefix = 'ShadowedCard',
}

const ShadowedCard = ({
  testId,
  shadow = 100,
  children = [],
}: ShadowedCardProps) => (
  <div
    className={classNames(
      'rck-shadowed-card',
      `rck-shadowed-card--shadow-${shadow}`
    )}
    data-testid={getTestId(ShadowedCardTest.prefix, testId)}
  >
    {children}
  </div>
);

ShadowedCard.displayName = 'ShadowedCard';

export default ShadowedCard;
