import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';
import ContentSeparator from '../ContentSeparator/ContentSeparator';
import Icon, { IconColor, IconSize, IconType } from '../Icons';
import FlexLayout from '../Layout';
import Link from '../Link/Link';

export enum InstructionCardTest {
  prefix = 'InstructionCard',
}

interface IconAsLinkProps {
  asLink: boolean;
  href: string;
}

const IconAsLink: React.FunctionComponent<IconAsLinkProps> = ({
  asLink,
  href,
}: IconAsLinkProps) =>
  asLink ? (
    <Link href={href}>
      <Icon
        type={IconType.angleRight}
        color={IconColor.regular}
        size={IconSize.x2}
      />
    </Link>
  ) : (
    <Icon
      type={IconType.angleRight}
      color={IconColor.regular}
      size={IconSize.x2}
    />
  );
interface InstructionCardProps extends BaseComponentProps {
  iconType: IconType;
  iconColor: IconColor;
  title: string;
  description: string;
  instructions: string;
  url: string;
  compact: boolean;
  iconAsLink?: boolean;
}

const InstructionCard: React.FunctionComponent<InstructionCardProps> = ({
  iconType,
  iconColor,
  title,
  description,
  instructions,
  url,
  compact = false,
  className = '',
  testId,
  iconAsLink = false,
}) => (
  <FlexLayout
    mainaxis="column"
    crossaxisAlignment="center"
    data-testid={getTestId(InstructionCardTest.prefix, testId)}
    className={classNames(
      'rck-instruction-card',
      compact ? 'rck-instruction-card--compact' : '',
      className
    )}
  >
    <Icon
      type={iconType}
      color={iconColor}
      size={compact ? IconSize.x2 : IconSize.x3}
      className="rck-instruction-card__icon"
    />

    <p className="rck-instruction-card__title">{title}</p>

    {!compact && (
      <p className="rck-instruction-card__description">{description}</p>
    )}

    {url && (
      <>
        <FlexLayout
          mainaxis="row"
          mainaxisAlignment="space-between"
          crossaxisAlignment="center"
          alignSelf="stretch"
          paddingBottom="8"
        >
          <Link href={url} className="rck-instruction-card__link">
            {instructions}
          </Link>

          <IconAsLink asLink={iconAsLink} href={url} />
        </FlexLayout>
        <ContentSeparator />
      </>
    )}
  </FlexLayout>
);

InstructionCard.displayName = 'InstructionCard';

export default InstructionCard;
