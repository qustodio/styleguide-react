import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export type InfoFieldLabelLines = '1' | '2' | '3' | '4' | '5';

export type InfoFieldLabelMode = 'info' | 'error';

export interface InfoFieldLabelProps extends BaseComponentProps {
  mode?: InfoFieldLabelMode;
  htmlFor?: string;
  allowedLines?: InfoFieldLabelLines;
  keepSpace?: InfoFieldLabelLines;
}

export enum InfoFieldLabelTest {
  prefix = 'InfoFieldLabel',
}

const InfoFieldLabel: React.FunctionComponent<InfoFieldLabelProps> = ({
  htmlFor,
  allowedLines,
  mode = 'info',
  testId,
  className = '',
  children,
}) => (
  <div className={classNames('rck-info-field-label', className)}>
    {children && (
      <label
        htmlFor={htmlFor}
        className={classNames(
          'rck-info-field-label__label',
          allowedLines
            ? `rck-info-field-label__label--lines-${allowedLines}`
            : '',
          `rck-info-field-label__label--mode-${mode}`
        )}
        data-testid={getTestId(InfoFieldLabelTest.prefix, testId)}
      >
        {children}
      </label>
    )}
  </div>
);

InfoFieldLabel.displayName = 'InfoFieldLabel';

export default React.memo(InfoFieldLabel);
