import React from 'react';
import { BaseComponentProps, TestableComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export interface LabelProps extends BaseComponentProps, TestableComponentProps {
  htmlFor?: string;
  ellipsis?: boolean;
}

export enum LabelTest {
  prefix = 'Label',
}

const Label: React.FunctionComponent<LabelProps> = ({
  htmlFor,
  className = '',
  children,
  testId,
  ellipsis,
}) => (
  <label
    htmlFor={htmlFor}
    className={classNames(
      'rck-label',
      ellipsis ? 'rck-label--ellipsis' : '',
      className
    )}
    data-testid={getTestId(LabelTest.prefix, testId)}
  >
    {children}
  </label>
);

Label.displayName = 'Label';

export default React.memo(Label);
