import React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';

export interface AccordionMenuProps extends BaseComponentProps {
  isVisible: boolean;
}

export enum AccordionMenuTest {
  prefix = 'AccordionMenu',
}

const AccordionMenu: React.FunctionComponent<AccordionMenuProps> = ({
  isVisible,
  className = '',
  testId,
  children,
}) => (
  <ul
    className={classNames(
      'rck-accordionMenu',
      isVisible ? 'rck-accordionMenu--is-visible' : '',
      className
    )}
    data-testid={getTestId(AccordionMenuTest.prefix, testId)}
  >
    {children}
  </ul>
);

AccordionMenu.displayName = 'AccordionMenu';

export default AccordionMenu;
