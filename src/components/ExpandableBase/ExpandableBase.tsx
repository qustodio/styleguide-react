import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Opener, { OpenerIconType } from '../Opener/Opener';
import { IconColor } from '../Icons';

interface ExpandableBaseProps extends BaseComponentProps {
  leftContent: JSX.Element;
  rightContent?: JSX.Element;
  initialOpenState?: boolean;
  isDisabled?: boolean;
  testId: string;
  openerType?: OpenerIconType;
  openerColor?: IconColor;
  onClick?: () => void;
}

export enum ExpandableTest {
  prefix = 'Expandable',
  opener = 'Opener',
  content = 'Content',
}

const ExpandableBase = ({
  children,
  className,
  leftContent,
  rightContent,
  initialOpenState = false,
  isDisabled = false,
  testId,
  openerType = 'chevron',
  openerColor,
  onClick,
}: ExpandableBaseProps) => {
  const [open, setOpen] = React.useState(initialOpenState);

  const expand = () => {
    if (isDisabled) return;
    setOpen(isOpen => !isOpen);
    if (onClick) {
      onClick();
    }
  };

  return (
    <div
      className={classNames(
        'rck-expandable',
        isDisabled ? 'rck-expandable--disabled' : '',
        className
      )}
      data-testId={getTestId(ExpandableTest.prefix, testId)}
    >
      <div
        className={classNames(
          'rck-expandable__header',
          isDisabled ? 'rck-expandable__header--disabled' : ''
        )}
        onClick={expand}
        role="button"
        onKeyDown={expand}
        tabIndex={0}
        aria-expanded={open}
        aria-disabled={isDisabled}
      >
        <span className="rck-expandable__header--left-content">
          {leftContent}
        </span>
        <span className="rck-expandable__header--right-content">
          {rightContent}
        </span>
        <span
          className="rck-expandable__header--open-icon"
          data-testId={getTestId(
            ExpandableTest.prefix,
            testId,
            ExpandableTest.opener
          )}
        >
          <Opener type={openerType} open={open} color={openerColor} />
        </span>
      </div>
      <div
        className={classNames(
          'rck-expandable__container',
          open && 'rck-expandable__container--open'
        )}
        data-testId={getTestId(
          ExpandableTest.prefix,
          testId,
          ExpandableTest.content
        )}
      >
        {children}
      </div>
    </div>
  );
};

export default ExpandableBase;
