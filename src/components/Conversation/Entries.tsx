import React from 'react';
import Avatar from '../Avatar/Avatar';
import { AvatarSize } from '../Avatar/Avatar.types';
import { classNames } from '../../helpers';
import { GlobalType } from '../../common/types';
import Icon, { IconColor, IconFamily, IconType } from '../Icons';
import Tag from '../Tag';
import {
  ConversationMessage,
  ConversationContact,
  ConversationHighlights,
} from './types/Conversation.types';

const TextBubble = ({ message }: { message: ConversationMessage }) => {
  if (typeof message.content !== 'string') return null;
  return <div className="rck-conversation__bubble">{message.content}</div>;
};

const ImageBubble = ({ message }: { message: ConversationMessage }) => {
  if (typeof message.content !== 'string') return null;
  return (
    <div className="rck-conversation__bubble rck-conversation__bubble--image">
      <img src={message.content} alt={message.content} />
    </div>
  );
};

const MixedContent = ({ message }: { message: ConversationMessage }) => {
  if (typeof message.content === 'string') return null;
  return <>{message.content(message)}</>;
};

export const MessageEntry = ({ message }: { message: ConversationMessage }) => {
  if (message.contentType === 'image') return <ImageBubble message={message} />;
  if (message.contentType === 'mixed')
    return <MixedContent message={message} />;
  return <TextBubble message={message} />;
};

export const HighlightIcon = ({
  highlight,
}: {
  highlight: ConversationHighlights | undefined;
}) => {
  if (highlight === 'warning') {
    return (
      <Icon
        type={IconType.exclamationCircle}
        color={IconColor.warning}
        family={IconFamily.solid}
      />
    );
  }
  if (highlight === 'error') {
    return <Icon type={IconType.exclamationCircle} color={IconColor.error} />;
  }
  return null;
};

export const ThreadEntry = ({
  highlight,
  children,
}: {
  highlight: ConversationHighlights | undefined;
  children: React.ReactNode;
}) => (
  <div
    className={classNames(
      'rck-conversation__entry',
      highlight ? `rck-conversation__entry--highlight-${highlight}` : ''
    )}
  >
    {children}
  </div>
);

export const ContactEntry = ({ contact }: { contact: ConversationContact }) => (
  <div className="rck-conversation__contact">
    {contact.pictureUrl ? (
      <Avatar
        className="rck-conversation__contact-avatar"
        size={AvatarSize.extraExtraSmall}
      >
        <img src={contact.pictureUrl} alt={contact.name} />
      </Avatar>
    ) : null}
    <div className="rck-conversation__contact-name">{contact.name}</div>
  </div>
);

export const DateIndicator = ({ date }: { date: string }) => (
  <div className="rck-conversation__date-indicator">
    <div>
      <Tag text={date} type={GlobalType.gray} invertColor />
    </div>
  </div>
);
