import {
  ConversationMessage,
  ConversationContact,
  ConversationThread,
  ConversationContactId,
} from './types/Conversation.types';

const prepareConversation = (
  conversation: { date?: string; messages: ConversationMessage[] }[],
  contacts: ConversationContact[]
): { date?: string; threads: ConversationThread[] }[] => {
  const contactsMap = contacts.reduce(
    (acc, curr) => ({ ...acc, [curr.id]: curr }),
    {} as Record<ConversationContactId, ConversationContact>
  );

  return conversation.map(({ date, messages }) => ({
    date,
    threads: messages.reduce((acc, curr) => {
      const lastGroup = acc[acc.length - 1];

      if (lastGroup && lastGroup.contact.id === curr.contactId) {
        lastGroup.messages.push(curr);
      } else {
        acc.push({
          id: `${curr.contactId}-${acc.length}`,
          contact: contactsMap[curr.contactId],
          messages: [curr],
          outgoing: curr.outgoing,
        });
      }

      return acc;
    }, [] as ConversationThread[]),
  }));
};

export default prepareConversation;
