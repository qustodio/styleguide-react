import React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';
import {
  ConversationMessage,
  ConversationContact,
  ConversationThread,
} from './types/Conversation.types';
import prepareConversation from './helpers';
import {
  ContactEntry,
  DateIndicator,
  HighlightIcon,
  MessageEntry,
  ThreadEntry,
} from './Entries';

export interface ConversationProps extends BaseComponentProps {
  conversation: { date?: string; messages: ConversationMessage[] }[];
  contacts: ConversationContact[];
}

const ConversationTest = {
  prefix: 'Conversation',
};

const MessagesThread = ({ thread }: { thread: ConversationThread }) => (
  <div
    key={thread.id}
    className={classNames(
      'rck-conversation__thread',
      thread.outgoing
        ? 'rck-conversation__outgoing'
        : 'rck-conversation__incoming'
    )}
  >
    <ContactEntry contact={thread.contact} />
    <div className="rck-conversation__entries">
      {thread.messages.map(message => (
        <ThreadEntry key={message.id} highlight={message.highlight}>
          <MessageEntry message={message} />
          <HighlightIcon highlight={message.highlight} />
        </ThreadEntry>
      ))}
    </div>
  </div>
);

const Conversation = ({
  conversation,
  contacts,
  className = '',
  testId,
}: ConversationProps) => {
  const datedThreads = prepareConversation(conversation, contacts);
  return (
    <div
      className={classNames('rck-conversation', className)}
      data-testid={getTestId(ConversationTest.prefix, testId)}
      // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
      tabIndex={0}
    >
      {datedThreads.map(datedThread => (
        <>
          {datedThread.date && <DateIndicator date={datedThread.date} />}
          {datedThread.threads.map(thread => (
            <MessagesThread key={thread.id} thread={thread} />
          ))}
        </>
      ))}
    </div>
  );
};

export default Conversation;
