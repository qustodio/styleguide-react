import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export enum ContentSeparatorTest {
  prefix = 'ContentSeparator',
}

const ContentSeparator: React.FunctionComponent<BaseComponentProps> = ({
  className = '',
  testId,
}) => (
  <div
    className={classNames('rck-content-separator', className)}
    data-testid={getTestId(ContentSeparatorTest.prefix, testId)}
  />
);

ContentSeparator.displayName = 'ContentSeparator';

export default React.memo(ContentSeparator);
