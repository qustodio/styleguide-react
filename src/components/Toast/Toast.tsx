import React from 'react';
import { ToastProps, ToastType } from './types/toast.types';
import { classNames, getTestId } from '../../helpers/index';
import Icon, { IconColor, IconFamily, IconSize, IconType } from '../Icons';
import TransparentButton from '../Button/TransparentButton';

const ToastTest = {
  prefix: 'Toast',
};

const getToastIconType = (type: ToastType) => {
  switch (type) {
    case 'warning':
      return IconType.exclamationTriangle;
    case 'error':
      return IconType.timesCircle;
    case 'success':
      return IconType.checkCircle;
    case 'info':
      return IconType.infoCircle;
    default:
      return undefined;
  }
};

const Toast: React.FC<ToastProps> = ({
  message,
  title,
  type = 'info',
  position = 'bottom-center',
  icon,
  onClose = () => {},
  testId,
  className = '',
}) => {
  const toastIcon = icon ?? getToastIconType(type);
  return (
    <div
      className={classNames(
        'rck-toast',
        `rck-toast--position-${position}`,
        `rck-toast--type-${type}`,
        className
      )}
      data-testid={getTestId(ToastTest.prefix, testId)}
    >
      <div className="rck-toast__wrapper">
        {toastIcon && (
          <div className="rck-toast__icon">
            {icon ?? (
              <Icon
                type={getToastIconType(type) as IconType}
                color={IconColor.notSet}
                size={IconSize.x2}
                family={IconFamily.solid}
              />
            )}
          </div>
        )}
        <div className="rck-toast__content">
          {title && <h4 className="rck-toast__title">{title}</h4>}
          {message && <div className="rck-toast__message">{message}</div>}
        </div>
        <div className="rck-toast__close">
          <TransparentButton
            onClick={onClose}
            className="rck-toast__close-button"
          >
            <Icon
              type={IconType.times}
              color={IconColor.notSet}
              size={IconSize.lg}
            />
          </TransparentButton>
        </div>
      </div>
    </div>
  );
};
Toast.displayName = 'Toast';

export default Toast;
