import { SyntheticEvent } from 'react';
import { BaseComponentProps } from '../../../common/types';

export type ToastType = 'error' | 'success' | 'warning' | 'info';

export type ToastPosition =
  | 'top-left'
  | 'top-right'
  | 'top-center'
  | 'bottom-left'
  | 'bottom-right'
  | 'bottom-center';

export interface ToastProps extends BaseComponentProps {
  message?: string | JSX.Element;
  title?: string;
  position?: ToastPosition;
  type?: ToastType;
  icon?: JSX.Element;
  onClose?: (ev: SyntheticEvent) => void;
}
