import React, { SyntheticEvent } from 'react';
import ReactDOM from 'react-dom';
import { ToastPosition, ToastType } from './types/toast.types';
import Toast from './Toast';
import { isTestEnv } from '../../helpers';

class ToastManager {
  private static instance: ToastManager;

  private timeout?: NodeJS.Timeout;

  defaultPosition: ToastPosition;

  defaultHook: string;

  defaultTimeout: number;

  private constructor() {
    this.defaultPosition = 'bottom-center';
    this.defaultHook = 'portal-toasts';
    this.defaultTimeout = 4000;
    this.createPortal();
  }

  private createPortal() {
    if (isTestEnv()) return;

    if (document.getElementById(this.defaultHook) === null) {
      const portal = document.createElement('div');
      portal.setAttribute('id', this.defaultHook);
      document.body.appendChild(portal);
    }
  }

  private removeCurrentToast(): void {
    if (isTestEnv()) return;

    const currNotif = document.getElementById(this.defaultHook);
    if (currNotif?.firstChild) {
      ReactDOM.unmountComponentAtNode(currNotif as Element);
    }
  }

  private scheduleTaskToRemoveToast(ms?: number): void {
    this.timeout = (setTimeout(() => {
      this.removeCurrentToast();
    }, ms ?? this.defaultTimeout) as unknown) as NodeJS.Timeout;
  }

  private removeScheduledTaskToRemoveToast(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = undefined;
    }
  }

  private onCloseToastHandler = () => {
    this.removeScheduledTaskToRemoveToast();
    this.removeCurrentToast();
  };

  public static getInstance(): ToastManager {
    if (!ToastManager.instance) {
      ToastManager.instance = new ToastManager();
    }

    return ToastManager.instance;
  }

  public setDefaultPosition(position: ToastPosition): void {
    this.defaultPosition = position;
  }

  public setDefaultHook(defaultHook: string): void {
    const portal = document.getElementById(this.defaultHook);
    if (portal) {
      portal.remove();
    }
    this.defaultHook = defaultHook;
    this.createPortal();
  }

  public setDefaultTimeout(ms: number): void {
    this.defaultTimeout = ms;
  }

  public sendWithoutTimeout(
    title: string,
    message: string | JSX.Element,
    type?: ToastType,
    icon?: JSX.Element,
    onClose?: (ev: SyntheticEvent) => void,
    position?: ToastPosition
  ): void {
    if (isTestEnv()) return;

    ReactDOM.render(
      <Toast
        message={message}
        type={type}
        title={title}
        position={position ?? this.defaultPosition}
        icon={icon}
        onClose={(ev: SyntheticEvent) => {
          this.onCloseToastHandler();
          if (onClose) {
            onClose(ev);
          }
        }}
      />,
      document.getElementById(this.defaultHook)
    );
  }

  public send(
    title: string,
    message: string | JSX.Element,
    type?: ToastType,
    icon?: JSX.Element,
    position?: ToastPosition,
    timeout?: number,
    onClose?: (ev: SyntheticEvent) => void
  ): void {
    this.removeScheduledTaskToRemoveToast();
    this.removeCurrentToast();
    this.sendWithoutTimeout(title, message, type, icon, onClose, position);
    this.scheduleTaskToRemoveToast(timeout);
  }
}

export default ToastManager;
