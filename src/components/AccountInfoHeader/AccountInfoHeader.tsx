import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconColor, IconFamily, IconSize, IconType } from '../Icons';
import TransparentButton from '../Button/TransparentButton';

export interface AccountInfoHeaderProps extends BaseComponentProps {
  name: string;
  logoImage: JSX.Element;
  showGift?: boolean;
  expand: boolean;
  onClickGift: () => void;
}

export enum AccountInfoHeaderTest {
  prefix = 'AccountInfoHeader',
}

const AccountInfoHeader: React.FunctionComponent<AccountInfoHeaderProps> = ({
  name,
  logoImage,
  showGift = false,
  onClickGift,
  expand,
  testId,
  className = '',
}) => {
  const renderContent = () => (
    <div className="rck-account-info-header__user-info__name">
      {showGift && (
        <Icon
          type={IconType.gift}
          size={IconSize.xs}
          color={IconColor.contrastBlue}
          family={IconFamily.solid}
          square
          className="rck-account-info-header__user-info__gift"
        />
      )}
      {name}
    </div>
  );

  return (
    <div
      className={classNames(
        'rck-account-info-header',
        expand ? 'rck-account-info-header--expanded' : '',
        className
      )}
      data-testid={getTestId(AccountInfoHeaderTest.prefix, testId)}
    >
      <div className="rck-account-info-header__header">{logoImage}</div>
      <div className="rck-account-info-header__user-info">
        {expand && showGift && (
          <TransparentButton onClick={onClickGift}>
            {renderContent()}
          </TransparentButton>
        )}
        {expand && !showGift && renderContent()}
      </div>
    </div>
  );
};

AccountInfoHeader.displayName = 'AccountInfoHeader';

export default React.memo(AccountInfoHeader);
