import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import { AvatarProps, AvatarSize } from '../Avatar/Avatar.types';
import TransparentButton from '../Button/TransparentButton';
import { DropdownProps } from '../Dropdown/Dropdown';
import Icon, { IconColor, IconFamily, IconType } from '../Icons';

export type ProfileStatus =
  | 'active'
  | 'no-device'
  | 'offline'
  | 'standby'
  | 'unknown'
  | 'tampered-device';

export type ProfileInfoHeaderSize = 'regular' | 'small';
export interface ProfileInfoHeaderProps extends BaseComponentProps {
  name: string;
  avatar: React.ReactElement<AvatarProps>;
  status?: ProfileStatus;
  statusMessage?: string;
  size?: ProfileInfoHeaderSize;
  actionElement?: React.ReactElement<DropdownProps>;
  onEdit?: () => void;
}

export enum ProfileInfoHeaderTest {
  prefix = 'ProfileInfoHeader',
}

const ProfileInfoHeader: React.FunctionComponent<ProfileInfoHeaderProps> = ({
  name,
  avatar,
  status,
  statusMessage,
  size = 'regular',
  actionElement,
  onEdit,
  testId,
  className = '',
}) => (
  <div
    className={classNames(
      'rck-profile-info-header',
      `rck-profile-info-header--${size}`,
      className
    )}
    data-testid={getTestId(ProfileInfoHeaderTest.prefix, testId)}
  >
    <div className="rck-profile-info-header__avatar">
      {React.cloneElement(avatar, {
        size: size === 'small' ? AvatarSize.extraSmall : AvatarSize.medium,
        icon: onEdit ? (
          <TransparentButton onClick={onEdit}>
            <Icon
              type={IconType.pencil}
              color={IconColor.greenDecorationContrast}
              family={IconFamily.solid}
              circle
            />
          </TransparentButton>
        ) : (
          undefined
        ),
      })}
    </div>
    <div className="rck-profile-info-header__content">
      <div className="rck-profile-info-header__content__name">{name}</div>
      <div
        className={classNames(
          'rck-profile-info-header__content__status',
          `rck-profile-info-header__content__status--${status}`
        )}
      >
        {statusMessage}
      </div>
    </div>
    {actionElement && (
      <div className="rck-profile-info-header__action-element">
        {actionElement}
      </div>
    )}
  </div>
);

ProfileInfoHeader.displayName = 'ProfileInfoHeader';

export default React.memo(ProfileInfoHeader);
