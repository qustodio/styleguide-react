import * as React from 'react';
import Modal, { ModalProps } from '../Modal/Modal';
import AlertBox, { AlertBoxType } from '../AlertBox';
import Button, { ButtonColor, ButtonSize, ButtonType } from '../Button/Button';
import Text from '../Text';

export interface ModalInfoProps extends Omit<ModalProps, 'fixedContent'> {
  addCancelButton?: boolean;
  cancelButtonText?: string;
  alertBoxText?: string;
  alertBoxType?: AlertBoxType;
}

const getModalInfoButtons = (
  buttons: ModalProps['buttons'],
  addCancelButton: ModalInfoProps['addCancelButton'],
  cancelButtonText: ModalInfoProps['cancelButtonText'],
  onClickClose: ModalInfoProps['onClickClose']
): ModalProps['buttons'] => {
  const newButtons = buttons ? [...buttons] : [];

  if (addCancelButton) {
    newButtons.push(
      <Button
        key="cancel_button"
        buttonType={ButtonType.plain}
        color={ButtonColor.secondary}
        size={ButtonSize.medium}
        block
        onClick={onClickClose}
      >
        {cancelButtonText}
      </Button>
    );
  }

  return newButtons;
};

const getAlertBox = (
  alertBoxText: ModalInfoProps['alertBoxText'],
  alertBoxType: ModalInfoProps['alertBoxType']
): Array<JSX.Element> | undefined => {
  if (alertBoxText) {
    return [
      <AlertBox
        key="modal_info_alert_box"
        type={alertBoxType ?? AlertBoxType.info}
      >
        <Text variant="caption-1-semibold">{alertBoxText}</Text>
      </AlertBox>,
    ];
  }
  return undefined;
};

const ModalInfo: React.FC<ModalInfoProps> = ({
  header,
  className,
  size,
  title,
  buttons,
  animationEnabled,
  hideCloseButton,
  testId,
  showBackButton,
  onClickClose,
  children,
  addCancelButton,
  cancelButtonText,
  alertBoxText,
  alertBoxType,
}) => (
  <Modal
    size={size}
    header={header}
    title={title}
    buttons={getModalInfoButtons(
      buttons,
      addCancelButton,
      cancelButtonText,
      onClickClose
    )}
    animationEnabled={animationEnabled}
    hideCloseButton={hideCloseButton}
    testId={testId}
    showBackButton={showBackButton}
    fixedContent={getAlertBox(alertBoxText, alertBoxType)}
    className={className}
    onClickClose={onClickClose}
  >
    {children}
  </Modal>
);

ModalInfo.displayName = 'ModalInfo';

export default ModalInfo;
