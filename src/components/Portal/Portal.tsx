import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { isTestEnv } from '../../helpers';

export interface PortalProps {
  name: string;
}

class Portal extends React.Component<PortalProps> {
  // eslint-disable-next-line react/sort-comp
  portalContainer: HTMLElement | null = null;

  portal = document.createElement('div');

  componentDidMount(): void {
    this.portalContainer = this.createPortalContainer() || null;
    // TODO: review this, it will fail silently. Is that what we want?
    this.portalContainer?.appendChild(this.portal);
  }

  componentWillUnmount(): void {
    if (this.portalContainer) {
      this.portalContainer.removeChild(this.portal);
    }
  }

  createPortalContainer = (): HTMLElement | undefined => {
    const { name } = this.props;
    const portalContainer = document.getElementById(name);

    if (!portalContainer && !isTestEnv()) {
      throw new Error(
        `Remember to add an HTML element with id "${name}" in the index.html file.`
      );
    }

    return portalContainer || document.body;
  };

  render(): JSX.Element {
    const { children } = this.props;
    return ReactDOM.createPortal(children, this.portal);
  }
}

export default Portal;
