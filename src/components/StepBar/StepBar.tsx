import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export interface StepBarProp extends BaseComponentProps {
  activeStep: number;
  steps: string[];
  hideLabels?: boolean;
}

export enum StepBarTest {
  prefix = 'StepBar',
}

const StepBar: React.FunctionComponent<StepBarProp> = ({
  activeStep = 0,
  hideLabels,
  className,
  steps,
  testId,
}) => (
  <div
    className={classNames(`rck-stepbar`, className || '')}
    data-testid={getTestId(StepBarTest.prefix, testId)}
  >
    {steps.map((step, key) => {
      const currentStep = key + 1;
      const doneStepClass = key <= activeStep ? 'rck-step--done' : '';
      const activeStepClass = key === activeStep ? 'rck-step--active' : '';
      return (
        <div
          className={classNames(
            'rck-stepbar__step',
            'rck-step',
            activeStepClass,
            doneStepClass
          )}
          key={step}
          data-testid={getTestId(StepBarTest.prefix, testId, step)}
        >
          <div className="rck-step__label">
            <span className="rck-step__label-title">
              {' '}
              {hideLabels ? '' : step}{' '}
            </span>
          </div>

          <div className="rck-step__circle">
            <span>{currentStep}</span>
          </div>
        </div>
      );
    })}
  </div>
);

StepBar.displayName = 'Stepbar';

export default StepBar;
