import * as React from 'react';
import Text from '../Text';
import Button, { ButtonProps } from '../Button/Button';
import { classNames } from '../../helpers';

export type FixedCtaCardBackgroundColor =
  | 'brand-primary-300'
  | 'brand-primary-600'
  | 'gradient-brand';
export type FixedCtaCardType = {
  title: string;
  description?: string;
  backgroundColor: FixedCtaCardBackgroundColor;
  button: ButtonProps;
};

const FixedCtaCard = ({
  title,
  description,
  button,
  backgroundColor,
}: FixedCtaCardType) => (
  <div
    className={classNames(
      'rck-fixed-cta-card',
      `rck-fixed-cta-card--${backgroundColor}`
    )}
  >
    <Text
      className="rck-fixed-cta-card__title"
      renderAs="h1"
      variant="title-1-semibold-responsive"
    >
      {title}
    </Text>
    <Text
      className="rck-fixed-cta-card__description"
      renderAs="p"
      variant="caption-1-semibold"
    >
      {description}
    </Text>
    {React.createElement(Button, button)}
  </div>
);

FixedCtaCard.displayName = 'FixedCtaCard';

export default FixedCtaCard;
