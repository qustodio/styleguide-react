import React from 'react';
import { classNames, getTestId } from '../../helpers';
import BoxSelectorItem, { BoxSelectorItemProps } from './BoxSelectorItem';
import { BaseComponentProps } from '../../common/types';

export interface BoxSelectorProps extends BaseComponentProps {
  fieldGroupName: string;
  handleInputCheckedChange: (value: string, checked: boolean) => void;
  items: Array<Omit<BoxSelectorItemProps, 'fieldGroupName' | 'handleChange'>>;
}

export enum BoxSelectorTest {
  prefix = 'BoxSelector',
}

const BoxSelector = ({
  testId,
  handleInputCheckedChange,
  fieldGroupName,
  items = [],
}: BoxSelectorProps) => (
  <ul
    className={classNames('rck-box-selector')}
    data-testid={getTestId(BoxSelectorTest.prefix, testId)}
  >
    {items.map(itemProps => (
      <BoxSelectorItem
        {...itemProps}
        fieldGroupName={fieldGroupName}
        handleChange={handleInputCheckedChange}
      />
    ))}
  </ul>
);

BoxSelector.displayName = 'BoxSelector';

export default BoxSelector;
