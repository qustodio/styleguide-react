import React, { useState } from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps } from '../../common/types';
import Icon, { IconType, IconFamily } from '../Icons';

export interface BoxSelectorItemProps extends BaseComponentProps {
  fieldGroupName: string;
  value: string | number | readonly string[] | undefined;
  label: string;
  isSelected: boolean;
  isDisabled: boolean;
  icon: string;
  handleChange: (value: string, checked: boolean) => void;
}

export enum BoxSelectorItemTest {
  prefix = 'BoxSelectorItem',
}

const BoxSelectorItem = ({
  label,
  icon,
  fieldGroupName,
  value,
  isSelected = false,
  isDisabled = false,
  handleChange,
  testId,
}: BoxSelectorItemProps) => {
  const [selected, setSelected] = useState<boolean>(isSelected && !isDisabled);

  const handleClick = (
    event: React.MouseEvent<HTMLInputElement, MouseEvent>
  ) => {
    if (!isDisabled) {
      if (selected) {
        // If option is selected and we click on it again, force blur so that it
        // doesn't display the focus/hover styles.
        event.currentTarget.blur();
      }
      setSelected(!selected);
    }
  };

  return (
    <li className="rck-box-selector__item">
      <label
        className={classNames(
          'rck-box-selector-item',
          selected && !isDisabled ? 'rck-box-selector-item--selected' : '',
          isDisabled ? 'rck-box-selector-item--disabled' : ''
        )}
      >
        <Icon
          type={icon as IconType}
          family={
            selected && !isDisabled ? IconFamily.solid : IconFamily.regular
          }
          className="rck-box-selector-item__icon"
        />

        <p className="rck-box-selector-item__label">{label}</p>

        <input
          className="rck-box-selector-item__checkbox"
          type="checkbox"
          checked={selected && !isDisabled}
          disabled={isDisabled}
          name={fieldGroupName}
          value={value}
          onClick={event => handleClick(event)}
          onChange={e => handleChange(e.target.value, e.target.checked)}
          data-testid={getTestId(BoxSelectorItemTest.prefix, testId)}
        />
      </label>
    </li>
  );
};

BoxSelectorItem.displayName = 'BoxSelectorItem';

export default BoxSelectorItem;
