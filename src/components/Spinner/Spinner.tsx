import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';

export type SpinnerSize = 'small' | 'medium' | 'large';

export type SpinnerColor = 'contrast' | 'regular';

export interface SpinnerProps extends BaseComponentProps {
  size: SpinnerSize;
  color: SpinnerColor;
}

export enum SpinnerTest {
  prefix = 'Spinner',
}

const Spinner: React.FunctionComponent<SpinnerProps> = ({
  size,
  color,
  className = '',
  testId,
}) => (
  <div
    className={classNames(
      'rck-spinner',
      `rck-spinner--size-${size}`,
      `rck-spinner--color-${color}`,
      className
    )}
    data-testid={getTestId(SpinnerTest.prefix, testId)}
  >
    <div className="rck-spinner__bounce1" />
    <div className="rck-spinner__bounce2" />
    <div className="rck-spinner__bounce3" />
  </div>
);

Spinner.displayName = 'Spinner';

export default Spinner;
