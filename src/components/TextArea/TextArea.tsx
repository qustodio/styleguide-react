import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';

export enum TextAreaTest {
  prefix = 'TextArea',
}

export interface TextAreaProps extends BaseComponentProps {
  rows?: number;
  maxRows?: number;
  maxLength?: number;
  error?: boolean;
  disabled?: boolean;
  placeholder?: string;
  defaultValue?: string;
  helperText?: string;
  onChange?: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
}

const isMaxRowsExceeded = (value: string, maxRows: number) => {
  const numRows = (value.match(/\n/g) || []).length + 1;
  return numRows > maxRows;
};

const useResize = (defaultValue: string, maxRows: number, rows?: number) => {
  const [value, setValue] = React.useState(defaultValue);
  const [initialHeight, setInitialHeight] = React.useState(0);
  const textareaRef = React.useRef<HTMLTextAreaElement>(null);

  React.useEffect(() => {
    const element = textareaRef.current;
    if (!element) return;

    setInitialHeight(element.clientHeight);
  }, []);

  React.useEffect(() => {
    const element = textareaRef.current;
    if (!element) return;

    const updatedHeight = element.scrollHeight;

    if (!value && initialHeight === 0 && rows) {
      // to work it depends on the font-size defined in scss
      const fontSize = 16;
      element.style.height = `${(rows + 1) * fontSize}px`;
    }
    if (!value && initialHeight > 0) {
      element.style.height = `${initialHeight}px`;
    } else if (
      updatedHeight > initialHeight &&
      !isMaxRowsExceeded(value, maxRows)
    ) {
      element.style.height = `${element.scrollHeight}px`;
    }
  }, [value]);

  return { value, setValue, textareaRef };
};

const TextArea: React.FC<TextAreaProps> = ({
  maxLength,
  rows,
  maxRows = Infinity,
  error = false,
  disabled = false,
  helperText,
  placeholder,
  onChange,
  defaultValue = '',
  testId,
}) => {
  const { value, setValue, textareaRef } = useResize(
    defaultValue,
    maxRows,
    rows
  );

  return (
    <div
      data-testid={getTestId(TextAreaTest.prefix, testId)}
      className={classNames(
        'rck-text-area',
        disabled ? 'rck-text-area--disabled' : '',
        error ? 'rck-text-area--error' : '',
        isMaxRowsExceeded(value, maxRows) ? 'rck-text-area--scrollable' : ''
      )}
    >
      <textarea
        ref={textareaRef}
        value={value}
        rows={rows}
        placeholder={placeholder}
        data-testid={getTestId(TextAreaTest.prefix, testId, 'textarea')}
        maxLength={maxLength}
        onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
          if (onChange) onChange(e);
          setValue(e.target.value);
        }}
        name="textarea"
        disabled={disabled}
      />
      {helperText ? (
        <div>
          <label
            className={classNames(
              'rck-text-area__text-helper',
              error ? 'rck-text-area__text-helper--error' : ''
            )}
            htmlFor="textarea"
          >
            {helperText}
          </label>
        </div>
      ) : null}
    </div>
  );
};

TextArea.displayName = 'TextArea';

export default TextArea;
