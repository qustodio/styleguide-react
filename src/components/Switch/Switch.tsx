import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps, DisabledProps } from '../../common/types';

export interface SwitchProps extends BaseComponentProps, DisabledProps {
  id?: string;
  className?: string;
  name?: string;
  checked?: boolean;
  disabled?: boolean;
  onClick?: (ev: React.SyntheticEvent) => void;
  onBlur?: (ev: React.SyntheticEvent) => void;
  onFocus?: (ev: React.SyntheticEvent) => void;
  onMouseDown?: (ev: React.SyntheticEvent) => void;
  onMouseUp?: (ev: React.SyntheticEvent) => void;
  onChange?: (ev: React.SyntheticEvent) => void;
}

export enum SwitchTest {
  prefix = 'Switch',
  actionElement = 'Action-element',
}

const Switch: React.FunctionComponent<SwitchProps> = ({
  className = '',
  id,
  name,
  checked = false,
  disabled = false,
  testId,
  onClick,
  onBlur,
  onFocus,
  onMouseDown,
  onMouseUp,
  onChange,
}) => {
  const handleOnClick = (ev: React.SyntheticEvent) => {
    if (!disabled) {
      if (onClick) {
        onClick(ev);
      }
    }
  };

  return (
    <label
      htmlFor={id}
      className={classNames(
        'rck-switch',
        disabled ? 'rck-switch--disabled' : '',
        className
      )}
      data-testid={getTestId(SwitchTest.prefix, testId)}
    >
      <input
        id={id}
        className="rck-switch__input"
        name={name}
        type="checkbox"
        checked={checked}
        disabled={disabled}
        onChange={onChange}
        onClick={handleOnClick}
        onBlur={onBlur}
        onFocus={onFocus}
        onMouseDown={onMouseDown}
        onMouseUp={onMouseUp}
        data-testid={getTestId(
          SwitchTest.prefix,
          testId,
          SwitchTest.actionElement
        )}
      />
      <span
        className={classNames(
          'rck-switch__slider',
          disabled ? 'rck-switch__slider--disabled' : ''
        )}
      />
    </label>
  );
};

Switch.displayName = 'Switch';

export default Switch;
