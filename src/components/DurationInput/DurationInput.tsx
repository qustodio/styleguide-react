import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import FlexLayout from '../Layout';
import TextFieldBase from '../TextFieldBase/TextFieldBase';
import { DurationInputProps } from './types/DurationInput.types';
import { TextFieldBaseProps } from '../TextFieldBase/types/TextFieldBase.types';

type DurationInputType = 'hours' | 'minutes';

enum DurationInputTest {
  prefix = 'DurationInput',
}

const DEFAULT_MAX_MINUTES = 24 * 60;
const getMinutes = (totalMinutes: number) => Math.floor(totalMinutes % 60);
const getHours = (totalMinutes: number) => Math.floor(totalMinutes / 60);
const getPadSize = (max: number) => Math.max(max.toString().length, 2);

/** Pad a number with zeros from the left. */
const lzpad = (num: number, totalPad = 2) =>
  num.toString().padStart(totalPad, '0');

const defaultLabelProp: DurationInputProps['labels'] = {
  hours: 'h',
  minutes: 'm',
};

const defaultErrorConfig: TextFieldBaseProps['errorConfig'] = {
  border: true,
  text: false,
};

const DurationInput: React.FunctionComponent<DurationInputProps> = ({
  value,
  testId,
  className = '',
  maxMinutes: initialMaxMinutes,
  labels = defaultLabelProp,
  hasError,
  disabled,
  onChange,
}: DurationInputProps) => {
  const maxMinutes = initialMaxMinutes || DEFAULT_MAX_MINUTES;

  const getTotalMinutes = (
    inputValue: number,
    inputType?: DurationInputType
  ) => {
    let totalMinutesValue = inputValue;

    if (inputType === 'hours') {
      totalMinutesValue = inputValue * 60 + getMinutes(value);
    }
    if (inputType === 'minutes') {
      totalMinutesValue = Math.min(inputValue, 59) + getHours(value) * 60;
    }

    return Math.min(maxMinutes || Infinity, totalMinutesValue);
  };

  const onChangeHandler = (inputType: DurationInputType) => (
    e: React.FormEvent<HTMLInputElement>
  ) => {
    const totalMinutesValue = getTotalMinutes(
      parseInt(e.currentTarget.value, 10) || 0,
      inputType
    );

    onChange(totalMinutesValue, e, inputType);
  };

  return (
    <FlexLayout
      mainaxis="row"
      mainaxisAlignment="center"
      crossaxisAlignment="center"
      className={classNames('rck-duration-input', className)}
      data-testid={getTestId(DurationInputTest.prefix, testId)}
    >
      <TextFieldBase
        type="number"
        name="duration-hours"
        min={0}
        // We want to have at least a digit with 2 numbers to mantain a consistent width
        max={Math.max(10, getHours(maxMinutes))}
        value={lzpad(getHours(value), getPadSize(getHours(maxMinutes)))}
        onChange={onChangeHandler('hours')}
        selectAllOnFocus
        hasError={hasError}
        errorConfig={defaultErrorConfig}
        disabled={disabled}
        width="hug-content"
        contentRight={
          <span className="rck-duration-input__text">{labels.hours}</span>
        }
      />
      <TextFieldBase
        type="number"
        name="duration-minutes"
        min={0}
        max={59}
        value={lzpad(getMinutes(value))}
        onChange={onChangeHandler('minutes')}
        selectAllOnFocus
        hasError={hasError}
        errorConfig={defaultErrorConfig}
        disabled={disabled}
        width="hug-content"
        contentRight={
          <span className="rck-duration-input__text">{labels.minutes}</span>
        }
      />
    </FlexLayout>
  );
};

DurationInput.displayName = 'DurationInput';

export default DurationInput;
