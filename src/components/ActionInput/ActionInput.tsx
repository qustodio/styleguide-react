import React, { SyntheticEvent } from 'react';
import Link from '../Link/Link';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps, GlobalType } from '../../common/types';
import { AnchorPropTypes } from '../../adapters/router/Anchor';

export enum ActionInputIconPosition {
  left = 'left',
  right = 'right',
}

export enum ActionInputButtonType {
  button = 'button',
  submit = 'submit',
}
export interface ActionInputProps extends BaseComponentProps {
  /** @deprecated use iconLeft or iconRight instead */
  icon?: JSX.Element;
  /** @deprecated use iconLeft or iconRight instead */
  iconPosition?: ActionInputIconPosition;
  iconLeft?: JSX.Element;
  iconRight?: JSX.Element;
  iconWithColor?: boolean;
  placeholder?: string;
  text?: string;
  block?: boolean;
  href?: string;
  buttonType: ActionInputButtonType;
  useAsLink?: React.FunctionComponent<AnchorPropTypes>;
  ariaLabel?: string;
  error?: boolean;
  disabled?: boolean;
  onClick?: (ev: SyntheticEvent) => void;
  onKeyDown?: (ev: SyntheticEvent) => void;
  /** @deprecated use backgroundColor and textColor instead */
  color?: 'primary';
  backgroundColor?: GlobalType;
  textColor?: GlobalType;
  compact?: boolean;
}

export enum ActionInputTest {
  prefix = 'ActionInput',
  label = 'ActionInput__label',
}

const renderIcon = (
  icon: JSX.Element | undefined,
  iconWithColor: boolean,
  position: ActionInputIconPosition | undefined
) => {
  if (!icon || !position) return null;
  return (
    <div
      className={classNames(
        'rck-action-input__container-icon',
        `rck-action-input__container-icon--${position}`
      )}
    >
      {React.cloneElement(icon, {
        className: classNames(
          'rck-action-input__icon',
          iconWithColor ? 'rck-action-input__icon--with-color' : ''
        ),
      })}
    </div>
  );
};

const ActionInput: React.FunctionComponent<ActionInputProps> = ({
  className = '',
  icon,
  iconPosition = ActionInputIconPosition.left,
  iconLeft,
  iconRight,
  iconWithColor = true,
  placeholder = '',
  text = '',
  block = false,
  href = '',
  useAsLink,
  ariaLabel,
  testId,
  buttonType = ActionInputButtonType.button,
  error,
  disabled = false,
  onClick = undefined,
  onKeyDown,
  color = undefined,
  textColor = undefined,
  backgroundColor = undefined,
  compact = false,
}) => {
  // We do this to be able to apply disable and error state Colors to an ActionInput with color prop set.
  const applyColor = !error && !disabled;

  const classes = classNames(
    `rck-action-input`,
    error ? 'rck-action-input--error' : '',
    className,
    block ? 'rck-action-input--block' : '',
    disabled ? 'rck-action-input--disabled' : '',
    compact ? 'rck-action-input--compact' : '',
    applyColor && color ? `rck-action-input--color-${color}` : '', // To be removed in v6
    applyColor && textColor ? `rck-action-input--text-color-${textColor}` : '',
    applyColor && backgroundColor
      ? `rck-action-input--background-color-${backgroundColor}`
      : ''
  );

  const renderLabel = (dataTestId?: string) =>
    text || placeholder ? (
      // TODO: review this
      // eslint-disable-next-line jsx-a11y/label-has-associated-control
      <label
        className={classNames(
          'rck-action-input__text',
          text ? '' : 'rck-action-input__text__placeholder'
        )}
        data-testid={getTestId(ActionInputTest.label, dataTestId)}
      >
        {text || placeholder}
      </label>
    ) : null;

  if (href || useAsLink) {
    return (
      <Link
        useAsLink={useAsLink}
        href={href}
        className={classNames(classes, 'rck-action-input__anchor')}
        ariaLabel={ariaLabel}
        disabled={disabled}
        testId={getTestId(ActionInputTest.prefix, testId)}
      >
        {/** To be removed in v6 */}
        {!iconLeft &&
          renderIcon(icon, iconWithColor, ActionInputIconPosition.left)}

        {renderIcon(iconLeft, iconWithColor, ActionInputIconPosition.left)}
        {renderLabel(testId)}
        {renderIcon(iconRight, iconWithColor, ActionInputIconPosition.right)}
      </Link>
    );
  }

  return (
    <button
      type={buttonType}
      className={classes}
      onClick={onClick}
      onKeyDown={onKeyDown}
      aria-label={ariaLabel}
      disabled={disabled}
      data-testid={getTestId(ActionInputTest.prefix, testId)}
    >
      {/** To be removed in v6 */}
      {!iconLeft &&
        renderIcon(
          icon,
          iconWithColor,
          iconPosition === ActionInputIconPosition.left
            ? ActionInputIconPosition.left
            : undefined
        )}

      {renderIcon(iconLeft, iconWithColor, ActionInputIconPosition.left)}
      {renderLabel(testId)}
      {renderIcon(iconRight, iconWithColor, ActionInputIconPosition.right)}

      {/** To be removed v6 */}
      {!iconRight &&
        renderIcon(
          icon,
          iconWithColor,
          iconPosition === ActionInputIconPosition.right
            ? ActionInputIconPosition.right
            : undefined
        )}
    </button>
  );
};

ActionInput.displayName = 'ActionInput';

export default React.memo(ActionInput);
