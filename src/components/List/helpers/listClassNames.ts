import memoize from 'memoizee';
import { classNames } from '../../../helpers';
import { AllowedListColumns, AllowedListSpacings } from '../types';

const listClassNames = (
  showSeparator: boolean,
  listItemPaddingTop?: AllowedListSpacings,
  listItemPaddingBottom?: AllowedListSpacings,
  listItemPaddingRight?: AllowedListSpacings,
  listItemPaddingLeft?: AllowedListSpacings,
  listItemMarginTop?: AllowedListSpacings,
  listItemMarginBottom?: AllowedListSpacings,
  columns?: AllowedListColumns,
  horizontalOrientation?: boolean,
  allowWrapping?: boolean,
  useNativeColumns?: boolean,
  className = ''
): string =>
  classNames(
    'rck-list',
    showSeparator ? '' : `rck-list--no-separator`,
    listItemPaddingTop
      ? `rck-list--item-top-padding-spacing-${listItemPaddingTop}`
      : '',
    listItemPaddingBottom
      ? `rck-list--item-bottom-padding-spacing-${listItemPaddingBottom}`
      : '',
    listItemPaddingRight
      ? `rck-list--item-right-padding-spacing-${listItemPaddingRight}`
      : '',
    listItemPaddingLeft
      ? `rck-list--item-left-padding-spacing-${listItemPaddingLeft}`
      : '',
    listItemMarginTop
      ? `rck-list--item-top-margin-spacing-${listItemMarginTop}`
      : '',
    listItemMarginBottom
      ? `rck-list--item-bottom-margin-spacing-${listItemMarginBottom}`
      : '',
    `rck-list--columns-${columns}`,
    horizontalOrientation ? 'rck-list--horizontal' : '',
    allowWrapping ? 'rck-list--wrapped' : '',
    useNativeColumns ? '' : 'rck-list--no-native-columns',
    className || ''
  );

export default memoize(listClassNames);
