import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import { CloudListProps } from './types';

export enum CloudListTest {
  prefix = 'CloudList',
}

const CloudList: React.FunctionComponent<CloudListProps> = ({
  allowedListItemType,
  listItemMaxWidth = '100%',
  children,
  className = '',
  testId,
}) => (
  <ul
    className={classNames(
      'rck-cloud-list',
      `rck-cloud-list--maxwidth-${listItemMaxWidth.replace('%', 'pc')}`,
      className
    )}
    data-testid={getTestId(CloudListTest.prefix, testId)}
  >
    {React.Children.map(children, child => {
      const listItem = child as React.ReactElement;
      if (listItem === null) {
        return null;
      }
      if (listItem.type !== allowedListItemType) {
        throw new Error(
          `CloudList children have an invalid type: ${listItem.type}`
        );
      }

      return <li>{child}</li>;
    })}
  </ul>
);

CloudList.displayName = 'CloudList';

export default CloudList;
