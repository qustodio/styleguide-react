import React, { useRef } from 'react';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers/index';
import IconAdapter from '../IconAdapter';
import { ListItemVerticalAlign, RegularStyleListItemProps } from '../types';
import ListTitle from '../utils/ListTitle';
import ListSubtitle from '../utils/ListSubtitle';
import useElementDisable from '../../../hooks/useElementDisable';

export enum RegularStyleListItemTest {
  prefix = 'ListItem',
  actionElement = 'Action-element',
}

const RegularStyleListItem: React.FunctionComponent<RegularStyleListItemProps> = ({
  icon,
  iconAltText,
  iconVerticalAlign = ListItemVerticalAlign.center,
  title,
  upperSubtitle,
  lowerSubtitle,
  centerMiddleContent,
  className = '',
  actionElement,
  actionElementVerticalAlign = ListItemVerticalAlign.center,
  testId,
  disabled = false,
  parentTestId,
}) => {
  const listItemRef = useRef(document.createElement('li'));
  useElementDisable(
    listItemRef,
    disabled,
    'rck-list-item--disabled',
    'rck-list-item__overlap'
  );

  return (
    <li
      className={classNames(
        'rck-list-item',
        `rck-regular-list-item`,
        className
      )}
      data-testid={getTestId(
        RegularStyleListItemTest.prefix,
        getTestIdWithParentTestId(parentTestId, testId)
      )}
      ref={listItemRef}
    >
      {icon && (
        <div
          className={classNames(
            'rck-regular-list-item__left',
            `rck-regular-list-item__left--${iconVerticalAlign}`
          )}
        >
          <div className="rck-regular-list-item__left__icon">
            <IconAdapter icon={icon} altText={iconAltText} />
          </div>
        </div>
      )}

      <div
        className={classNames(
          'rck-regular-list-item__middle-left',
          centerMiddleContent
            ? 'rck-regular-list-item__middle-left--centered'
            : ''
        )}
      >
        {title &&
          (typeof title === 'string' ? (
            <ListTitle
              className="rck-regular-list-item__middle-left__title"
              testId={testId}
              parentTestId={parentTestId}
            >
              {title}
            </ListTitle>
          ) : (
            React.cloneElement(title, {
              className: 'rck-regular-list-item__middle-left__title',
              isBold: false,
            })
          ))}
        {upperSubtitle &&
          (typeof upperSubtitle === 'string' ? (
            <ListSubtitle
              className="rck-regular-list-item__middle-left__subtitle"
              testId={testId}
              parentTestId={parentTestId}
            >
              {upperSubtitle}
            </ListSubtitle>
          ) : (
            React.cloneElement(upperSubtitle, {
              className: 'rck-regular-list-item__middle-left__subtitle',
            })
          ))}
        {lowerSubtitle &&
          (typeof lowerSubtitle === 'string' ? (
            <ListSubtitle
              className="rck-regular-list-item__middle-left__subtitle"
              testId={testId}
              parentTestId={parentTestId}
            >
              {lowerSubtitle}
            </ListSubtitle>
          ) : (
            React.cloneElement(lowerSubtitle, {
              className: 'rck-regular-list-item__middle-left__subtitle',
            })
          ))}
      </div>
      {actionElement && (
        <div
          className={classNames(
            'rck-regular-list-item__right',
            `rck-regular-list-item__right--${actionElementVerticalAlign}`
          )}
        >
          <div
            className="rck-regular-list-item__right__action-element"
            data-testid={getTestId(
              RegularStyleListItemTest.prefix,
              getTestIdWithParentTestId(parentTestId, testId),
              RegularStyleListItemTest.actionElement
            )}
          >
            {actionElement}
          </div>
        </div>
      )}
    </li>
  );
};

RegularStyleListItem.displayName = 'RegularStyleListItem';

export default RegularStyleListItem;
