import React, { useRef } from 'react';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers';
import IconAdapter from '../IconAdapter';
import ListTitle from '../utils/ListTitle';
import useElementDisable from '../../../hooks/useElementDisable';
import { CustomListItemProps } from '../types';
import ClickableListItem from '../utils/ClickableListItem';
import Icon, { IconColor, IconType } from '../../Icons';

export enum CustomStyleListItemTest {
  prefix = 'ListItem',
}

const CustomStyleListItem: React.FunctionComponent<CustomListItemProps> = ({
  className,
  title,
  icon,
  iconAltText,
  children,
  rightContent,
  footerContent,
  showSeparator,
  disabled = false,
  testId,
  parentTestId,
  onInfoActionClick,
}) => {
  const listItemRef = useRef(document.createElement('li'));
  useElementDisable(
    listItemRef,
    disabled,
    'rck-list-item--disabled',
    'rck-list-item__overlap'
  );

  const leftContent = (
    <>
      <div className="rck-custom-list-item__body__left">
        <div className="rck-custom-list-item__body__left__icon">
          <IconAdapter icon={icon} altText={iconAltText} />
        </div>
      </div>
      <div className="rck-custom-list-item__body__middle-left">
        <div className="rck-custom-list-item__body__middle-left__title-container">
          {typeof title === 'string' ? (
            <ListTitle
              className="rck-custom-list-item__body__middle-left__title"
              testId={testId}
              parentTestId={parentTestId}
            >
              {title}
            </ListTitle>
          ) : (
            React.cloneElement(title, {
              className: 'rck-custom-list-item__body__middle-left__title',
            })
          )}
          {onInfoActionClick ? (
            <Icon
              type={IconType.infoCircle}
              color={IconColor.secondary}
              className="rck-custom-list-item__body__middle-left__action-icon"
            />
          ) : null}
        </div>
        <div className="rck-custom-list-item__body__middle-left__content">
          {children}
        </div>
      </div>
    </>
  );

  return (
    <li
      className={classNames(
        'rck-list-item',
        'rck-custom-list-item',
        showSeparator ? '' : 'rck-custom-list-item--no-border',
        className || ''
      )}
      data-testid={getTestId(
        CustomStyleListItemTest.prefix,
        getTestIdWithParentTestId(parentTestId, testId)
      )}
      ref={listItemRef}
    >
      <div className="rck-custom-list-item__body">
        {onInfoActionClick ? (
          <ClickableListItem
            onClick={onInfoActionClick}
            className="rck-custom-list-item__info-button"
          >
            {leftContent}
          </ClickableListItem>
        ) : (
          leftContent
        )}

        {rightContent && (
          <div className="rck-custom-list-item__body__right">
            {rightContent}
          </div>
        )}
      </div>
      {footerContent && (
        <div className="rck-custom-list-item__footer">{footerContent}</div>
      )}
    </li>
  );
};

CustomStyleListItem.displayName = 'CustomStyleListItem';

export default CustomStyleListItem;
