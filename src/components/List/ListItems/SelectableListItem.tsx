import * as React from 'react';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
  handleKeyboardSelection,
} from '../../../helpers';
import IconAdapter from '../IconAdapter';
import ListTitle from '../utils/ListTitle';
import { SelectableListItemProps } from '../types';
import Icon, { IconColor, IconFamily, IconType } from '../../Icons';
import useElementDisable from '../../../hooks/useElementDisable';
import Text from '../../Text';
import ClickableListItem from '../utils/ClickableListItem';

export enum SelectableListItemTest {
  prefix = 'ListItem',
}

type ActionElementRenderer = (
  selected: SelectableListItemProps['selected']
) => JSX.Element;

const getCheckbox: ActionElementRenderer = selected =>
  selected ? (
    <Icon
      type={IconType.checkCircle}
      color={IconColor.secondary}
      family={IconFamily.solid}
    />
  ) : (
    <Icon type={IconType.circle} color={IconColor.secondary} />
  );

const getRadio: ActionElementRenderer = selected =>
  selected ? (
    <Icon type={IconType.circleDot} color={IconColor.secondary} />
  ) : (
    <Icon type={IconType.circle} color={IconColor.black} />
  );

const SelectableListItem: React.FunctionComponent<SelectableListItemProps> = ({
  id,
  className,
  type = 'checkbox',
  title,
  subtitle,
  description,
  isDisabled = false,
  selected = false,
  boldTitleOnSelected = false,
  icon,
  iconAltText,
  testId,
  parentTestId,
  onClick,
  onInfoActionClick,
}) => {
  const listItemRef = React.useRef(document.createElement('li'));
  useElementDisable(
    listItemRef,
    isDisabled,
    'rck-list-item--disabled',
    'rck-list-item__overlap'
  );

  const leftContent = (
    <>
      {icon && (
        <div className="rck-selectable-list-item__icon">
          <IconAdapter icon={icon} altText={iconAltText} />
        </div>
      )}
      <ListTitle
        className="rck-selectable-list-item__title"
        testId={testId}
        parentTestId={parentTestId}
        isBold={!boldTitleOnSelected || (boldTitleOnSelected && selected)}
        allowLines={1}
      >
        {title}
      </ListTitle>
      {onInfoActionClick ? (
        <Icon
          type={IconType.infoCircle}
          color={isDisabled ? IconColor.disabled : IconColor.secondary}
          className="rck-selectable-list-item__action-icon"
        />
      ) : null}
      {subtitle ? (
        <div className="rck-selectable-list-item__subtitle">{subtitle}</div>
      ) : null}
    </>
  );

  const onInfoClick = (e: React.SyntheticEvent) => {
    e.stopPropagation();
    if (!onInfoActionClick) return;
    onInfoActionClick();
  };

  const getLeftClassNames = () =>
    classNames(
      'rck-selectable-list-item__left',
      subtitle ? 'rck-selectable-list-item__left-subtitle' : ''
    );

  return (
    <li
      // eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role
      role={type}
      // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
      tabIndex={0}
      ref={listItemRef}
      aria-checked={selected}
      aria-disabled={isDisabled}
      className={classNames(
        'rck-list-item',
        'rck-list-item__no-border',
        'rck-selectable-list-item',
        `rck-selectable-list-item--${type}`,
        selected ? 'rck-selectable-list-item--selected' : '',
        subtitle ? 'rck-selectable-list-item--has-subtitle' : '',
        className || ''
      )}
      data-testid={getTestId(
        SelectableListItemTest.prefix,
        getTestIdWithParentTestId(parentTestId, testId)
      )}
      onClick={!isDisabled ? e => onClick?.(id, e) : undefined}
      onKeyDown={
        !isDisabled ? handleKeyboardSelection(e => onClick?.(id, e)) : undefined
      }
    >
      <div className="rck-selectable-list-item__body">
        {onInfoActionClick ? (
          <ClickableListItem
            onClick={onInfoClick}
            className={getLeftClassNames()}
          >
            {leftContent}
          </ClickableListItem>
        ) : (
          <div className={getLeftClassNames()}>{leftContent}</div>
        )}
        <div className="rck-selectable-list-item__right">
          {type === 'checkbox' && getCheckbox(selected)}
          {type === 'radio' && getRadio(selected)}
        </div>

        {description ? (
          <div className="rck-selectable-list-item__footer">
            <div className="rck-selectable-list-item__description">
              {typeof description === 'string' ? (
                <Text color="grey-500" variant="body-1-regular">
                  {description}
                </Text>
              ) : (
                description
              )}
            </div>
          </div>
        ) : null}
      </div>
    </li>
  );
};

SelectableListItem.displayName = 'SelectableListItem';

export default SelectableListItem;
