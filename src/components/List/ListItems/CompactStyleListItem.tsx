import React, { useRef } from 'react';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers';
import useElementDisable from '../../../hooks/useElementDisable';
import IconAdapter from '../IconAdapter';
import { CompactListItemProps } from '../types';
import ListCompactRightSubtitle from '../utils/ListCompactRightSubtitle';
import ListCompactSubtitle from '../utils/ListCompactSubtitle';
import ListCompactTitle from '../utils/ListCompactTitle';
import ClickableListItem from '../utils/ClickableListItem';
import Icon, { IconColor, IconType } from '../../Icons';

export enum CompactStyleListItemTest {
  prefix = 'ListItem',
  title = 'Title-content',
  rightTitle = 'Title-right-content',
  subtitle = 'Subtitle-content',
  rightSubtitle = 'Subtitle-right-content',
}

const CompactStyleListItem: React.FunctionComponent<CompactListItemProps> = ({
  className,
  title,
  subtitle,
  subtitleClassName,
  rightTitle,
  rightSubtitle,
  icon,
  iconAltText,
  bottomPadding = 'compact',
  disabled = false,
  testId,
  parentTestId,
  onInfoActionClick,
}) => {
  const listItemRef = useRef(document.createElement('li'));
  useElementDisable(
    listItemRef,
    disabled,
    'rck-list-item--disabled',
    'rck-list-item__overlap'
  );

  const leftContent = (
    <>
      {icon && (
        <div className="rck-compact-list-item__icon-wrapper">
          <IconAdapter
            icon={icon}
            altText={iconAltText}
            className="rck-compact-list-item__icon"
          />
        </div>
      )}

      <div className="rck-compact-list-item__left-content">
        <div className="rck-compact-list-item__left-title">
          {title &&
            (typeof title === 'string' ? (
              <ListCompactTitle
                className={classNames('rck-compact-list-item__title')}
                testId={getTestId(
                  CompactStyleListItemTest.title,
                  getTestIdWithParentTestId(parentTestId, testId)
                )}
              >
                {title}
              </ListCompactTitle>
            ) : (
              React.cloneElement(title, {
                className: classNames('rck-compact-list-item__title'),
                testId: getTestId(
                  CompactStyleListItemTest.title,
                  getTestIdWithParentTestId(parentTestId, testId)
                ),
              })
            ))}
          {onInfoActionClick ? (
            <Icon
              type={IconType.infoCircle}
              color={disabled ? IconColor.disabled : IconColor.secondary}
              className="rck-compact-list-item__action-icon"
            />
          ) : null}
        </div>
        {subtitle &&
          (typeof subtitle === 'string' ? (
            <ListCompactSubtitle
              className={classNames(
                'rck-compact-list-item__subtitle',
                subtitleClassName || ''
              )}
              testId={getTestId(
                CompactStyleListItemTest.subtitle,
                getTestIdWithParentTestId(parentTestId, testId)
              )}
            >
              {subtitle}
            </ListCompactSubtitle>
          ) : (
            React.cloneElement(subtitle, {
              className: classNames(
                'rck-compact-list-item__subtitle',
                subtitleClassName || ''
              ),
              testId: getTestId(
                CompactStyleListItemTest.subtitle,
                getTestIdWithParentTestId(parentTestId, testId)
              ),
            })
          ))}
      </div>
    </>
  );
  return (
    <li
      className={classNames(
        'rck-list-item',
        'rck-compact-list-item',
        `rck-compact-list-item--padding-b-${bottomPadding}`,
        className || ''
      )}
      data-testid={getTestId(
        CompactStyleListItemTest.prefix,
        getTestIdWithParentTestId(parentTestId, testId)
      )}
      ref={listItemRef}
    >
      <div className="rck-compact-list-item__content">
        {onInfoActionClick ? (
          <ClickableListItem
            onClick={onInfoActionClick}
            className="rck-compact-list-item__info-button"
          >
            {leftContent}
          </ClickableListItem>
        ) : (
          leftContent
        )}
        <div className="rck-compact-list-item__right-content">
          {rightTitle &&
            (typeof rightTitle === 'string' ? (
              <ListCompactRightSubtitle
                className="rck-compact-list-item__right-title"
                testId={getTestId(
                  CompactStyleListItemTest.rightTitle,
                  getTestIdWithParentTestId(parentTestId, testId)
                )}
              >
                {rightTitle}
              </ListCompactRightSubtitle>
            ) : (
              React.cloneElement(rightTitle, {
                className: 'rck-compact-list-item__right-title',
                testId: getTestId(
                  CompactStyleListItemTest.rightTitle,
                  getTestIdWithParentTestId(parentTestId, testId)
                ),
              })
            ))}
          {rightSubtitle &&
            (typeof rightSubtitle === 'string' ? (
              <ListCompactRightSubtitle
                className="rck-compact-list-item__right-subtitle"
                testId={getTestId(
                  CompactStyleListItemTest.rightSubtitle,
                  getTestIdWithParentTestId(parentTestId, testId)
                )}
              >
                {rightSubtitle}
              </ListCompactRightSubtitle>
            ) : (
              React.cloneElement(rightSubtitle, {
                className: 'rck-compact-list-item__right-subtitle',
                testId: getTestId(
                  CompactStyleListItemTest.rightSubtitle,
                  getTestIdWithParentTestId(parentTestId, testId)
                ),
              })
            ))}
        </div>
      </div>
    </li>
  );
};

CompactStyleListItem.displayName = 'CompactStyleListItem';

export default CompactStyleListItem;
