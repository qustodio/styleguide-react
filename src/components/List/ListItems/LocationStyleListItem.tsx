import React, { useRef } from 'react';
import { TextAlign } from '../../../common/types';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers/index';
import useElementDisable from '../../../hooks/useElementDisable';
import { LocationListItemProps } from '../types';
import ListSubtitle from '../utils/ListSubtitle';
import ListTitle from '../utils/ListTitle';

export enum LocationStyleListItemTest {
  prefix = 'ListItem',
}

const LocationStyleListItem: React.FunctionComponent<LocationListItemProps> = ({
  className = '',
  leftSubtitle,
  upperTitle,
  lowerTitle,
  wideLeftSubtitle = false,
  showActions,
  actions,
  children,
  testId,
  disabled = false,
  parentTestId,
}) => {
  const listItemRef = useRef(document.createElement('li'));
  useElementDisable(
    listItemRef,
    disabled,
    'rck-list-item--disabled',
    'rck-list-item__overlap'
  );

  return (
    <li
      className={classNames(
        `rck-list-item`,
        'rck-location-list-item',
        wideLeftSubtitle ? 'rck-location-list-item--wide' : '',
        className
      )}
      data-testid={getTestId(
        LocationStyleListItemTest.prefix,
        getTestIdWithParentTestId(parentTestId, testId)
      )}
      ref={listItemRef}
    >
      <div className="rck-location-list-item__left">
        <div className="rck-location-list-item__left__text">
          {typeof leftSubtitle === 'string' ? (
            <ListSubtitle
              align={TextAlign.right}
              fontSize="medium"
              testId={testId}
              parentTestId={parentTestId}
            >
              {leftSubtitle}
            </ListSubtitle>
          ) : (
            React.cloneElement(leftSubtitle, {
              align: TextAlign.right,
              fontSize: 'medium',
            })
          )}
        </div>
        <div className="rck-location-list-item__left__oval" />
      </div>

      <div className="rck-location-list-item__middle-left">
        {!children &&
          upperTitle &&
          (typeof upperTitle === 'string' ? (
            <ListTitle
              className="rck-location-list__middle-left__text"
              testId={testId}
              parentTestId={parentTestId}
            >
              {upperTitle}
            </ListTitle>
          ) : (
            React.cloneElement(upperTitle, {
              className: 'rck-location-list__middle-left__text',
            })
          ))}
        {!children &&
          lowerTitle &&
          (typeof lowerTitle === 'string' ? (
            <ListTitle
              className="rck-location-list__middle-left__text"
              testId={testId}
              parentTestId={parentTestId}
            >
              {lowerTitle}
            </ListTitle>
          ) : (
            React.cloneElement(lowerTitle, {
              className: 'rck-location-list__middle-left__text',
            })
          ))}

        <div
          className={classNames(
            'rck-location-list-item__actions',
            showActions ? 'rck-location-list-item__actions--visible' : ''
          )}
        >
          {!children && actions}
        </div>

        {children}
      </div>
    </li>
  );
};

LocationStyleListItem.displayName = 'LocationStyleListItem';

export default LocationStyleListItem;
