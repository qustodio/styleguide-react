import React, { useRef } from 'react';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers';
import useElementDisable from '../../../hooks/useElementDisable';
import { FreeStyleListItemProps } from '../types';

export enum FreeStyleListItemTest {
  prefix = 'ListItem',
}

const FreeStyleListItem: React.FunctionComponent<FreeStyleListItemProps> = ({
  className = '',
  children,
  testId,
  disabled = false,
  noBorder = false,
  parentTestId,
}) => {
  const listItemRef = useRef(document.createElement('li'));
  useElementDisable(
    listItemRef,
    disabled,
    'rck-list-item--disabled',
    'rck-list-item__overlap'
  );

  return (
    <li
      className={classNames(
        `rck-list-item`,
        'rck-free-list-item',
        noBorder ? 'rck-list-item__no-border' : '',
        className
      )}
      data-testid={getTestId(
        FreeStyleListItemTest.prefix,
        getTestIdWithParentTestId(parentTestId, testId)
      )}
      ref={listItemRef}
    >
      {children}
    </li>
  );
};

FreeStyleListItem.displayName = 'FreeStyleListItem';

export default FreeStyleListItem;
