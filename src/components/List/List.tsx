import * as React from 'react';
import { getTestId } from '../../helpers';
import CompactStyleListItem from './ListItems/CompactStyleListItem';
import CustomStyleListItem from './ListItems/CustomStyleListItem';
import FreeStyleListItem from './ListItems/FreeStyleListItem';
import LocationStyleListItem from './ListItems/LocationStyleListItem';
import RegularStyleListItem from './ListItems/RegularStyleListItem';
import SelectableListItem from './ListItems/SelectableListItem';
import {
  AllowedListColumns,
  AllowedListSpacings,
  ListItemProps,
  ListProps,
} from './types';
import listClassNames from './helpers/listClassNames';

export enum ListTest {
  prefix = 'List',
}

const List: React.FunctionComponent<ListProps> = ({
  showSeparator = true,
  listItemPaddingTop = 'small',
  listItemPaddingBottom,
  listItemPaddingRight,
  listItemPaddingLeft,
  listItemMarginTop,
  listItemMarginBottom,
  allowCustomListItemStyle,
  columns = '1',
  horizontalOrientation,
  allowWrapping,
  useNativeColumns = true,
  className = '',
  testId,
  disabled = false,
  children,
  ariaRole,
  ariaLabelledBy,
  onClickItem,
}) => (
  <ul
    className={listClassNames(
      showSeparator,
      listItemPaddingTop as AllowedListSpacings,
      listItemPaddingBottom,
      listItemPaddingRight,
      listItemPaddingLeft,
      listItemMarginTop,
      listItemMarginBottom,
      columns as AllowedListColumns,
      horizontalOrientation,
      allowWrapping,
      useNativeColumns,
      className
    )}
    data-testid={getTestId(ListTest.prefix, testId)}
    role={ariaRole}
    aria-labelledby={ariaLabelledBy}
  >
    {React.Children.map(children, child => {
      const listItem = child as React.ReactElement<ListItemProps>;
      if (listItem === null) {
        return null;
      }

      if (
        (allowCustomListItemStyle &&
          listItem.type !== allowCustomListItemStyle) ||
        (!allowCustomListItemStyle &&
          listItem.type !== CompactStyleListItem &&
          listItem.type !== FreeStyleListItem &&
          listItem.type !== RegularStyleListItem &&
          listItem.type !== LocationStyleListItem &&
          listItem.type !== CustomStyleListItem &&
          listItem.type !== SelectableListItem)
      ) {
        throw new Error(`List children have an invalid type: ${listItem.type}`);
      }

      return React.cloneElement(listItem, {
        disabled,
        parentTestId: testId,
        ...(onClickItem && { onClick: onClickItem }),
      });
    })}
  </ul>
);

List.displayName = 'List';

export default List;
