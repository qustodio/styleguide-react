import { SyntheticEvent } from 'react';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
import {
  BaseComponentProps,
  DisabledProps,
  TestableNestedCompomentProps,
  TextAlign,
  TextWordBreak,
} from '../../common/types';
import { AriaRole } from '../../common/aria.types';

export enum ListItemVerticalAlign {
  top = 'top',
  center = 'center',
  bottom = 'bottom',
}

export enum ListItemTopPadding {
  none = 'padding-none',
  short = 'padding-s',
  medium = 'padding-m',
  large = 'padding-l',
}

export type ListItemTextAllowLines = 1 | 2 | 3 | 4;

export interface ListLinkProps
  extends BaseComponentProps,
    TestableNestedCompomentProps {
  url: string;
  useAsLink?: React.FunctionComponent<AnchorPropTypes>;
  fontSize?: 'medium' | 'small';
  color?: 'regular' | 'danger';
  isBold?: boolean;
  onClick?: (ev: SyntheticEvent) => void;
}

export interface ListSubtitleProps
  extends BaseComponentProps,
    TestableNestedCompomentProps {
  align?: TextAlign;
  fontSize?: 'medium' | 'small';
  color?: 'neutral' | 'error' | 'warning' | 'success';
  allowLines?: ListItemTextAllowLines;
  renderAs?: ListItemRenderElement;
}

export type ListThumbnaImageDimensions =
  | '50'
  | '100'
  | '150'
  | '200'
  | '250'
  | '300'
  | '350'
  | '400';
export interface ListThumbnailProps
  extends BaseComponentProps,
    TestableNestedCompomentProps {
  thumbnailUrl: string;
  altTextThumnail?: string;
  imageMinHeight?: ListThumbnaImageDimensions;
  imageMinWidth?: ListThumbnaImageDimensions;
}

export type ListItemRenderElement = 'p' | 'div';
export interface ListTitleProps
  extends BaseComponentProps,
    TestableNestedCompomentProps {
  isBold?: boolean;
  align?: TextAlign;
  allowLines?: ListItemTextAllowLines;
  renderAs?: ListItemRenderElement;
}

export interface CompactListItemProps
  extends BaseComponentProps,
    TestableNestedCompomentProps,
    DisabledProps {
  title: string | React.ReactElement<BaseComponentProps>;
  subtitle?: string | React.ReactElement<BaseComponentProps>;
  subtitleClassName?: string;
  rightTitle?: string | React.ReactElement<BaseComponentProps>;
  rightSubtitle: string | React.ReactElement<BaseComponentProps>;
  icon?: string | JSX.Element;
  iconAltText?: string;
  bottomPadding: 'compact' | 'extra';
  extraRightSpace?: boolean;
  onInfoActionClick?: () => void;
}

export interface CustomListItemProps
  extends BaseComponentProps,
    TestableNestedCompomentProps {
  title: string | React.ReactElement<ListTitleProps>;
  icon: string | JSX.Element;
  iconAltText?: string;
  rightContent: React.ReactNode;
  footerContent: React.ReactNode;
  showSeparator?: boolean;
  disabled?: boolean;
  onInfoActionClick?: () => void;
}

export interface FreeStyleListItemProps
  extends BaseComponentProps,
    TestableNestedCompomentProps,
    DisabledProps {
  noBorder?: boolean;
}

export interface LocationListItemProps
  extends BaseComponentProps,
    TestableNestedCompomentProps,
    DisabledProps {
  leftSubtitle: string | React.ReactElement<ListSubtitleProps>;
  upperTitle?: string | React.ReactElement<ListTitleProps>;
  lowerTitle?: string | React.ReactElement<ListTitleProps>;
  wideLeftSubtitle: boolean;
  selected: boolean;
  showActions: boolean;
  actions?: JSX.Element;
}

export interface RegularStyleListItemProps
  extends BaseComponentProps,
    TestableNestedCompomentProps,
    TestableNestedCompomentProps,
    DisabledProps {
  icon: JSX.Element | string;
  iconAltText?: string;
  iconVerticalAlign: ListItemVerticalAlign;
  title: string | React.ReactElement<ListTitleProps>;
  upperSubtitle: string | React.ReactElement<ListSubtitleProps>;
  lowerSubtitle: string | React.ReactElement<ListSubtitleProps>;
  centerMiddleContent?: boolean;
  actionElement?: JSX.Element;
  actionElementVerticalAlign: ListItemVerticalAlign;
}

export interface SelectableListItemProps
  extends BaseComponentProps,
    TestableNestedCompomentProps {
  id: string;
  type?: 'checkbox' | 'radio';
  selected?: boolean;
  isDisabled?: boolean;
  title: string;
  subtitle?: React.ReactNode | string;
  description?: React.ReactNode | string;
  icon?: string | JSX.Element;
  iconAltText?: string;
  boldTitleOnSelected?: boolean;
  onClick?: (id: unknown, ev: SyntheticEvent) => void;
  onInfoActionClick?: () => void;
}

export type ValidListItemProps =
  | CompactListItemProps
  | CustomListItemProps
  | FreeStyleListItemProps
  | LocationListItemProps
  | RegularStyleListItemProps
  | SelectableListItemProps;

export interface ClickableListItemProps extends BaseComponentProps {
  onClick?: (ev: SyntheticEvent) => void;
  useAsLink?: React.FunctionComponent<AnchorPropTypes>;
  href?: string;
  role?: AriaRole;
}

export type AllowedListSpacings = '0' | '24' | '16' | '8';
export type AllowedListColumns = 1 | 2 | 3 | 4;

export type ListItemProps =
  | CompactListItemProps
  | FreeStyleListItemProps
  | RegularStyleListItemProps
  | LocationListItemProps
  | CustomListItemProps
  | SelectableListItemProps;

export interface ListProps extends BaseComponentProps, DisabledProps {
  showSeparator?: boolean;
  listItemPaddingTop?: AllowedListSpacings;
  listItemPaddingBottom?: AllowedListSpacings;
  listItemPaddingRight?: AllowedListSpacings;
  listItemPaddingLeft?: AllowedListSpacings;
  listItemMarginTop?: AllowedListSpacings;
  listItemMarginBottom?: AllowedListSpacings;
  allowCustomListItemStyle?: unknown;
  columns?: AllowedListColumns;
  horizontalOrientation?: boolean;
  allowWrapping?: boolean;
  useNativeColumns?: boolean;
  ariaRole?: AriaRole;
  ariaLabelledBy?: string;
  onClickItem?: (id: unknown, ev: SyntheticEvent) => void;
}

export type ListTextColors = 'warning' | 'error' | 'secondary' | 'success';

export interface ListColoredTextProps extends BaseComponentProps {
  color: ListTextColors;
  wordBreak?: TextWordBreak;
}

export interface ListBckImageIconProps extends BaseComponentProps {
  url: string;
}

export type CloudListMaxWidthListItem =
  | '20%'
  | '25%'
  | '30%'
  | '35%'
  | '40%'
  | '45%'
  | '50%'
  | '55%'
  | '60%'
  | '65%'
  | '70%'
  | '75%'
  | '80%'
  | '100%';

export interface CloudListProps extends BaseComponentProps {
  allowedListItemType: unknown;
  listItemMaxWidth?: CloudListMaxWidthListItem;
}

export interface ListSecondaryTextProps
  extends BaseComponentProps,
    TestableNestedCompomentProps {
  align?: TextAlign;
  fontSize?: 'medium' | 'small';
  allowLines?: ListItemTextAllowLines;
  renderAs?: ListItemRenderElement;
}
