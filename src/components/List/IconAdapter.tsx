import * as React from 'react';

export interface IconAdapterProps {
  icon: string | JSX.Element;
  className?: string;
  altText?: string;
}

const IconAdapter: React.FunctionComponent<IconAdapterProps> = ({
  icon,
  className,
  altText,
}) => {
  if (typeof icon === 'string') {
    return (
      <img
        className={className}
        src={icon}
        role="presentation"
        alt={altText}
        referrerPolicy="no-referrer"
      />
    );
  }

  return icon;
};

IconAdapter.displayName = 'IconAdapter';

export default React.memo(IconAdapter);
