import React from 'react';
import Link from '../../Link/Link';
import {
  ClickableListItemProps,
  ListLinkProps,
  ValidListItemProps,
  ListSubtitleProps,
  ListTitleProps,
} from '../types';
import { classNames, getTestId } from '../../../helpers';

type ValidItemListProps = ListLinkProps | ListSubtitleProps | ListTitleProps;

type ClickableListItemPropsExtended = ClickableListItemProps & {
  clickableClassName: string;
};

const makeClickableListItem = <P extends ValidItemListProps>(
  Component: React.ComponentType<P>
): React.FunctionComponent<P & ClickableListItemPropsExtended> => ({
  onClick,
  useAsLink,
  href,
  role,
  clickableClassName = '',
  ...props
}: ClickableListItemPropsExtended) =>
  onClick || href || useAsLink ? (
    <Link
      className={classNames('rck-list-item--clickable', clickableClassName)}
      useAsLink={useAsLink}
      role={role}
      href={href}
      onClick={onClick}
      data-testid={getTestId('clickable', (props as ValidListItemProps).testId)}
    >
      <Component {...(props as P)} />
    </Link>
  ) : (
    <Component {...(props as P)} />
  );

export default makeClickableListItem;
