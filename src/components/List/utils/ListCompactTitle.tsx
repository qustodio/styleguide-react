import React from 'react';
import { classNames } from '../../../helpers';
import { BaseComponentProps } from '../../../common/types';

export enum ListCompactTitleTest {
  prefix = 'ListCompactTitle',
}

const ListCompactTitle: React.FunctionComponent<BaseComponentProps> = ({
  testId,
  className = '',
  children,
}) => (
  <p
    className={classNames('rck-list-compact-title', className)}
    data-testid={testId}
  >
    {children}
  </p>
);

ListCompactTitle.displayName = 'ListCompactTitle';

export default React.memo(ListCompactTitle);
