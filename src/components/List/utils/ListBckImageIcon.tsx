import React from 'react';
import { getTestId, classNames } from '../../../helpers/index';
import { ListBckImageIconProps } from '../types';

export enum ListBckImageIconTest {
  prefix = 'ListItemBrandIcon',
}

const ListBckImageIcon: React.FunctionComponent<ListBckImageIconProps> = ({
  url,
  testId,
  className = '',
}) => (
  <div
    data-testid={getTestId(ListBckImageIconTest.prefix, testId)}
    className={classNames('rck-list-item-bck-image-icon', className)}
    style={{ backgroundImage: `url(${url})` }}
  />
);

ListBckImageIcon.displayName = 'ListBckImageIcon';

export default React.memo(ListBckImageIcon);
