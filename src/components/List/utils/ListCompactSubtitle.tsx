import React from 'react';
import { classNames } from '../../../helpers';
import { BaseComponentProps } from '../../../common/types';

export enum ListCompactSubtitleTest {
  prefix = 'ListCompactSubtitle',
}

const ListCompactSubtitle: React.FunctionComponent<BaseComponentProps> = ({
  testId,
  className = '',
  children,
}) => (
  <p
    className={classNames('rck-list-compact-subtitle', className)}
    data-testid={testId}
  >
    {children}
  </p>
);

ListCompactSubtitle.displayName = 'ListCompactSubtitle';

export default React.memo(ListCompactSubtitle);
