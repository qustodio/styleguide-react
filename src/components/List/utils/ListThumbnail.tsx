import React from 'react';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers/index';
import Icon, { IconColor, IconFamily, IconType } from '../../Icons';
import { ListThumbnailProps } from '../types';

export enum ListThumbnailTest {
  prefix = 'ListThumbnail',
}
const ListThumbnail: React.FunctionComponent<ListThumbnailProps> = ({
  thumbnailUrl,
  altTextThumnail,
  imageMinHeight,
  imageMinWidth,
  className = '',
  testId,
  parentTestId,
}) => (
  <div
    className={classNames('rck-list-thumbnail', className)}
    data-testid={getTestId(
      ListThumbnailTest.prefix,
      getTestIdWithParentTestId(parentTestId, testId)
    )}
  >
    <div className="rck-list-thumbnail__wrapper">
      <div className="rck-list-thumbnail__wrapper__opacity-layer" />
      <img
        className={classNames(
          'rck-list-thumbnail__wrapper__thumb',
          imageMinWidth
            ? `rck-list-thumbnail__wrapper__thumb--min-width-${imageMinWidth}`
            : '',
          imageMinHeight
            ? `rck-list-thumbnail__wrapper__thumb--min-height-${imageMinHeight}`
            : ''
        )}
        src={thumbnailUrl}
        role="presentation"
        alt={altTextThumnail}
      />
      <Icon
        type={IconType.playCircle}
        className="rck-list-thumbnail__wrapper__icon"
        family={IconFamily.regular}
        color={IconColor.contrast}
      />
    </div>
  </div>
);

ListThumbnail.displayName = 'ListThumbnail';

export default React.memo(ListThumbnail);
