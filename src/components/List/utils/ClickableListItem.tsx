import React from 'react';
import Link from '../../Link/Link';
import { ClickableListItemProps } from '../types';
import { classNames, getTestId } from '../../../helpers';
import TransparentButton from '../../Button/TransparentButton';

export enum ClickableListItemTest {
  prefix = 'ClickableListItemTest',
}

const ClickableListItem: React.FunctionComponent<ClickableListItemProps> = ({
  onClick,
  role,
  testId,
  useAsLink,
  href,
  children,
  className = '',
}) =>
  !href && onClick ? (
    <TransparentButton
      onClick={onClick}
      className={classNames('rck-list-item--clickable', className)}
      testId={getTestId(ClickableListItemTest.prefix, testId)}
    >
      {children}
    </TransparentButton>
  ) : (
    <Link
      className={classNames('rck-list-item--clickable', className)}
      useAsLink={useAsLink}
      role={role}
      href={href}
      onClick={onClick}
      testId={getTestId(ClickableListItemTest.prefix, testId)}
    >
      {children}
    </Link>
  );

ClickableListItem.displayName = 'ClickableListItem';

export default ClickableListItem;
