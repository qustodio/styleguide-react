import React from 'react';
import { getWordBreakClass } from '../../../common/styles';
import { classNames, getTestId } from '../../../helpers';
import { ListColoredTextProps } from '../types';

export enum ListColoredTextTest {
  prefix = 'ListColoredText',
}

const ListColoredText: React.FunctionComponent<ListColoredTextProps> = ({
  color,
  wordBreak,
  testId,
  className = '',
  children,
}) => (
  <span
    className={classNames(
      'rck-list-colored-text',
      `rck-list-colored-text--${color}`,
      getWordBreakClass('rck-list-colored-text', wordBreak),
      className
    )}
    data-testid={getTestId(ListColoredTextTest.prefix, testId)}
  >
    {children}
  </span>
);

ListColoredText.displayName = 'ListColoredText';

export default React.memo(ListColoredText);
