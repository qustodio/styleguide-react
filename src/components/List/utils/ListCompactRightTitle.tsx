import React from 'react';
import { classNames } from '../../../helpers';
import { BaseComponentProps } from '../../../common/types';

export enum ListCompactRightTitleTest {
  prefix = 'ListCompactRightTitle',
}

const ListCompactRightTitle: React.FunctionComponent<BaseComponentProps> = ({
  testId,
  className = '',
  children,
}) => (
  <p
    className={classNames('rck-list-compact-right-title', className)}
    data-testid={testId}
  >
    {children}
  </p>
);

ListCompactRightTitle.displayName = 'ListCompactRightTitle';

export default React.memo(ListCompactRightTitle);
