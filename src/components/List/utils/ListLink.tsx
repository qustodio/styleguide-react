import React from 'react';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers';
import Link from '../../Link/Link';
import { ListLinkProps } from '../types';

export enum ListLinkTest {
  prefix = 'ListLink',
}

const ListLink: React.FunctionComponent<ListLinkProps> = ({
  url = '',
  useAsLink,
  fontSize = 'small',
  color = 'regular',
  isBold,
  className = '',
  testId,
  parentTestId,
  onClick,
  children,
}) => (
  <Link
    useAsLink={useAsLink}
    href={url}
    className={classNames(
      'rck-list-link',
      `rck-list-link--fsize-${fontSize}`,
      `rck-list-link--${color}`,
      className
    )}
    onClick={onClick}
    testId={getTestId(
      ListLinkTest.prefix,
      getTestIdWithParentTestId(parentTestId, testId)
    )}
  >
    {isBold ? <strong>{children}</strong> : children}
  </Link>
);

ListLink.displayName = 'ListLink';

export default React.memo(ListLink);
