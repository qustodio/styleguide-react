import React from 'react';
import { TextAlign } from '../../../common/types';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers';
import { ListSecondaryTextProps } from '../types';

export enum ListSecondaryTextTest {
  prefix = 'ListSecondaryText',
}

const ListSecondaryText: React.FunctionComponent<ListSecondaryTextProps> = ({
  align = TextAlign.left,
  fontSize = 's',
  allowLines,
  renderAs: Tag = 'p',
  children,
  className = '',
  testId,
  parentTestId,
}) => (
  <Tag
    className={classNames(
      `rck-list-secondary-text`,
      `rck-list-secondary-text--align-${align}`,
      `rck-list-secondary-text--fsize-${fontSize}`,
      allowLines ? `rck-list-secondary-text--ellipsis-for-${allowLines}` : '',
      className
    )}
    data-testid={getTestId(
      ListSecondaryTextTest.prefix,
      getTestIdWithParentTestId(parentTestId, testId)
    )}
  >
    {children}
  </Tag>
);

ListSecondaryText.displayName = 'ListSecondaryText';

export default React.memo(ListSecondaryText);
