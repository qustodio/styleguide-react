import React from 'react';
import { classNames } from '../../../helpers';
import { BaseComponentProps } from '../../../common/types';

export enum ListCompactRightSubtitleTest {
  prefix = 'ListCompactRightSubtitle',
}

const ListCompactRightSubtitle: React.FunctionComponent<BaseComponentProps> = ({
  testId,
  className = '',
  children,
}) => (
  <p
    className={classNames('rck-list-compact-right-subtitle', className)}
    data-testid={testId}
  >
    {children}
  </p>
);

ListCompactRightSubtitle.displayName = 'ListCompactRightSubtitle';

export default React.memo(ListCompactRightSubtitle);
