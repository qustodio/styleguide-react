import React from 'react';
import { TextAlign } from '../../../common/types';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers/index';
import { ListTitleProps } from '../types';

export enum ListTitleTest {
  prefix = 'ListTitle',
}

const ListTitle: React.FunctionComponent<ListTitleProps> = ({
  align = TextAlign.left,
  allowLines,
  children,
  className = '',
  testId,
  parentTestId,
  isBold = true,
  renderAs: Tag = 'p',
}) => (
  <Tag
    className={classNames(
      'rck-list-title',
      `rck-list-title--align-${align}`,
      allowLines ? `rck-list-title--ellipsis-for-${allowLines}` : '',
      className
    )}
    data-testid={getTestId(
      ListTitleTest.prefix,
      getTestIdWithParentTestId(parentTestId, testId)
    )}
  >
    {isBold ? <strong>{children}</strong> : children}
  </Tag>
);

ListTitle.displayName = 'ListTitle';

export default React.memo(ListTitle);
