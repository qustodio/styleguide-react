import React from 'react';
import { TextAlign } from '../../../common/types';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../../helpers';
import { ListSubtitleProps } from '../types';

export enum ListSubtitleTest {
  prefix = 'ListSubtitle',
}

const ListSubtitle: React.FunctionComponent<ListSubtitleProps> = ({
  align = TextAlign.left,
  fontSize = 's',
  color = 'neutral',
  allowLines,
  renderAs: Tag = 'p',
  children,
  className = '',
  testId,
  parentTestId,
}) => (
  <Tag
    className={classNames(
      `rck-list-subtitle`,
      `rck-list-subtitle--align-${align}`,
      `rck-list-subtitle--fsize-${fontSize}`,
      allowLines ? `rck-list-subtitle--ellipsis-for-${allowLines}` : '',
      `rck-list-subtitle--${color}`,
      className
    )}
    data-testid={getTestId(
      ListSubtitleTest.prefix,
      getTestIdWithParentTestId(parentTestId, testId)
    )}
  >
    {children}
  </Tag>
);

ListSubtitle.displayName = 'ListSubtitle';

export default React.memo(ListSubtitle);
