/* eslint-disable import/prefer-default-export */
export enum RocketCustomIconType {
  outgoingCall = 'outgoing-call',
  incomingCall = 'incoming-call',
}
