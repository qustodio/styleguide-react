/* eslint-disable import/prefer-default-export, @typescript-eslint/no-redeclare  */
import { ValueOf } from '../../../../common/types';
import { BaseIconSize } from '../../types/BaseIconSize.types';
import { IconSize } from '../../types/IconSize.types';

export const RocketCustomIconSize: { [size in IconSize]: string } = {
  ...BaseIconSize,
  xs: 'rck-xs',
  sm: 'rck-sm',
  lg: 'rck-lg',
  x2: 'rck-2x',
  x3: 'rck-3x',
  x4: 'rck-4x',
  x5: 'rck-5x',
  x7: 'rck-7x',
  x10: 'rck-10x',
};

export type RocketCustomIconSize = ValueOf<typeof RocketCustomIconSize>;
