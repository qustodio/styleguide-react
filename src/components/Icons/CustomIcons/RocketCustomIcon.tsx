import React from 'react';

import IcomoonReact from 'icomoon-react';
import iconSet from '../../../assets/icons/rocket-icons.json';

import { classNames } from '../../../helpers';
import { RocketCustomIconProps } from './types/RocketCustomIconProps.types';
import { RocketCustomIconSize } from './types/RocketCustomIconSize.types';
import { IconSize } from '../types/IconSize.types';

const RocketCustomIcon: React.FC<RocketCustomIconProps> = props => {
  const { size = IconSize.regular, type } = props;
  return (
    <i
      className={classNames(
        RocketCustomIconSize[size],
        `rck-${type}`,
        'rck-custom-icon'
      )}
    >
      <IcomoonReact iconSet={iconSet} icon={type} />
    </i>
  );
};

export default RocketCustomIcon;
