import React from 'react';
import { classNames, getTestId } from '../../helpers/index';
import { SvgProps } from './Svg/types/SvgProps.types';

export enum SvgTest {
  prefix = 'Svg',
}

const Svg: React.FunctionComponent<SvgProps> = ({
  height,
  width,
  color,
  testId,
  children,
  className = '',
}) => (
  <span
    className={classNames(
      'rck-svg',
      height ? `rck-svg--size-height-${height}` : '',
      width ? `rck-svg--size-width-${width}` : '',
      color ? `rck-svg--color-${color}` : '',
      className
    )}
    data-testid={getTestId(SvgTest.prefix, testId)}
  >
    {children}
  </span>
);

Svg.displayName = 'Svgs';

export default React.memo(Svg);
