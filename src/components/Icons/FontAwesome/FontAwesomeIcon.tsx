import React from 'react';
import { classNames } from '../../../helpers';
import { IconSize } from '../types/IconSize.types';
import { FontAwesomeIconFamily } from './types/FontAwesomeIconFamily.types';
import { FontAwesomeIconProps } from './types/FontAwesomeIconProps.types';
import { FontAwesomeIconSize } from './types/FontAwesomeIconSize.types';

const FontAwesomeIcon: React.FC<FontAwesomeIconProps> = props => {
  const {
    size = IconSize.regular,
    type,
    family = FontAwesomeIconFamily.regular,
  } = props;
  return <i className={classNames(family, type, FontAwesomeIconSize[size])} />;
};

export default FontAwesomeIcon;
