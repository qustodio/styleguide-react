// eslint-disable-next-line import/prefer-default-export
export enum FontAwesomeIconFamily {
  brands = 'fa-brands',
  solid = 'fa-solid',
  regular = 'fa-regular',
}
