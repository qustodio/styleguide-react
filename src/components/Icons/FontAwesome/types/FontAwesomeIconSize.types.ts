/* eslint-disable import/prefer-default-export, @typescript-eslint/no-redeclare  */
import { ValueOf } from '../../../../common/types';
import { IconSize } from '../../types/IconSize.types';
import { BaseIconSize } from '../../types/BaseIconSize.types';

export const FontAwesomeIconSize: { [size in IconSize]: string } = {
  ...BaseIconSize,
  xs: 'fa-xs',
  sm: 'fa-sm',
  lg: 'fa-lg',
  x2: 'fa-2x',
  x3: 'fa-3x',
  x4: 'fa-4x',
  x5: 'fa-5x',
  x7: 'fa-7x',
  x10: 'fa-10x',
};

export type FontAwesomeIconSize = ValueOf<typeof FontAwesomeIconSize>;
