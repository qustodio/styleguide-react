/* eslint-disable @typescript-eslint/no-redeclare */

import { ValueOf } from '../../../common/types';

import { BaseIconSize } from './BaseIconSize.types';

export const IconSize = {
  ...BaseIconSize,
  xs: 'xs',
  sm: 'sm',
  lg: 'lg',
  x2: 'x2',
  x3: 'x3',
  x4: 'x4',
  x5: 'x5',
  x7: 'x7',
  x10: 'x10',
} as const;

export type IconSize = ValueOf<typeof IconSize>;
