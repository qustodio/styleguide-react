export interface IconContainer {
  square?: boolean;
  circle?: boolean;
}
