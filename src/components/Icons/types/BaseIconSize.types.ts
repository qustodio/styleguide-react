/* eslint-disable @typescript-eslint/no-redeclare */

import { ValueOf } from '../../../common/types';

export const BaseIconSize = {
  regular: 'regular',
} as const;

export type BaseIconSize = ValueOf<typeof BaseIconSize>;
