/* eslint-disable @typescript-eslint/no-redeclare */
import { ValueOf } from '../../../common/types';
import { RocketCustomIconType } from '../CustomIcons/types/RocketCustomIconType.types';
import { FontAwesomeIconType } from '../FontAwesome/types/FontAwesomeIconType.types';

export const IconType = {
  ...FontAwesomeIconType,
  ...RocketCustomIconType,
} as const;

export type IconType = ValueOf<typeof FontAwesomeIconType> &
  ValueOf<typeof RocketCustomIconType>;
