import * as React from 'react';
import { classNames, getTestId } from '../../helpers/index';
import RocketCustomIcon from './CustomIcons/RocketCustomIcon';
import FontAwesomeIcon from './FontAwesome/FontAwesomeIcon';
import { IconProps } from './types/IconProps.types';
import { IconColor } from './types/IconColor.types';
import { IconSize } from './types/IconSize.types';

export enum IconTest {
  prefix = 'Icon',
}

const selectIcon = (props: IconProps) => {
  switch (props.source) {
    case 'fontAwesome':
    case undefined:
      return <FontAwesomeIcon {...props} />;
    case 'custom':
      return <RocketCustomIcon {...props} />;
    default:
      return <></>;
  }
};

const Icon: React.FunctionComponent<IconProps> = props => {
  const {
    size = IconSize.regular,
    className = '',
    ariaHidden = true,
    ariaLabel,
    role,
    title,
    tabIndex,
    color = IconColor.notSet,
    square,
    circle,
    testId,
    onClick,
    onFocus,
    onBlur,
  } = props;
  return (
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions
    <div
      className={classNames(
        'rck-icon',
        `rck-icon--${color}`,
        square ? `rck-icon--square` : '',
        square ? `rck-icon--square-${size}` : '',
        circle ? 'rck-icon--circle' : '',
        className
      )}
      onClick={onClick}
      onFocus={onFocus}
      onBlur={onBlur}
      aria-hidden={ariaHidden}
      aria-label={ariaLabel}
      title={title}
      role={role}
      tabIndex={tabIndex}
      onKeyDown={onClick}
      data-testid={getTestId(IconTest.prefix, testId)}
    >
      {selectIcon({ ...props })}
    </div>
  );
};

Icon.displayName = 'Icon';

export default React.memo(Icon);
