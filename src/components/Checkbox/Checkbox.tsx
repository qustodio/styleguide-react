import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconType, IconFamily } from '../Icons';

export interface CheckboxProps extends BaseComponentProps, DisabledProps {
  name: string;
  label?: string | JSX.Element;
  checked?: boolean;
  block?: boolean;
  onClick?: (isChecked: boolean) => void;
  onBlur?: (ev: React.SyntheticEvent) => void;
  onFocus?: (ev: React.SyntheticEvent) => void;
  onMouseDown?: (ev: React.SyntheticEvent) => void;
  onMouseUp?: (ev: React.SyntheticEvent) => void;
}

export enum CheckboxTest {
  prefix = 'Checkbox',
  box = 'Box',
}

const Checkbox: React.FunctionComponent<CheckboxProps> = ({
  name,
  className = '',
  children,
  label,
  checked,
  disabled,
  block,
  testId,
  onClick,
  onBlur,
  onFocus,
  onMouseDown,
  onMouseUp,
}) => {
  const handleOnClick = () => {
    if (!disabled && onClick) {
      onClick(!checked);
    }
  };

  return (
    <label
      htmlFor={name}
      className={classNames(
        className,
        'rck-checkbox',
        block ? 'rck-checkbox--block' : '',
        disabled ? 'rck-checkbox--disabled' : ''
      )}
      data-testid={getTestId(CheckboxTest.prefix, testId)}
    >
      {label || children}
      <input
        className="rck-checkbox__input"
        type="checkbox"
        checked={checked}
        name={name}
        onChange={() => {}}
      />
      <div
        className={classNames(
          'rck-checkbox__square',
          checked ? 'rck-checkbox__square--checked' : '',
          disabled ? 'rck-checkbox__square--disabled' : ''
        )}
        onClick={handleOnClick}
        onBlur={onBlur}
        onFocus={onFocus}
        onMouseDown={onMouseDown}
        onMouseUp={onMouseUp}
        role="checkbox"
        onKeyPress={handleOnClick}
        tabIndex={0}
        aria-checked={checked}
        data-testid={getTestId(CheckboxTest.prefix, testId, CheckboxTest.box)}
      >
        {checked && (
          <Icon
            family={IconFamily.solid}
            type={IconType.check}
            className={classNames(
              'rck-checkbox__square__icon',
              disabled ? 'rck-checkbox__square__icon--disabled' : ''
            )}
          />
        )}
      </div>
    </label>
  );
};

Checkbox.displayName = 'Checkbox';

export default Checkbox;
