import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconColor, IconType } from '../Icons';

export interface CopyBoxProps extends BaseComponentProps {
  text: string;
  url: string;
  confirmationText: string;
  onClickCallback: () => void;
}

export enum CopyBoxTest {
  prefix = 'CopyBox',
}

const CopyBox: React.FunctionComponent<CopyBoxProps> = ({
  text,
  url,
  confirmationText,
  className = '',
  testId,
  onClickCallback,
}) => {
  const [confirmationVisible, setConfirmationVisible] = React.useState(false);
  const clickHandler = () => {
    window.navigator.clipboard.writeText(url);
    setConfirmationVisible(true);
    onClickCallback();
  };
  return (
    <div
      className={classNames('rck-copybox', className)}
      data-testid={getTestId(CopyBoxTest.prefix, testId)}
    >
      <div className="rck-copybox__widget">
        <p className={classNames('rck-copybox__text')}>{text}</p>

        <div
          className="rck-copybox__icon"
          role="button"
          tabIndex={0}
          onKeyDown={clickHandler}
          onClick={clickHandler}
        >
          <Icon type={IconType.copy} color={IconColor.contrast} />
        </div>
      </div>
      {confirmationVisible && (
        <small className="rck-copybox__copy-confirmation">
          {confirmationText}
        </small>
      )}
    </div>
  );
};

CopyBox.displayName = 'CopyBox';

export default CopyBox;
