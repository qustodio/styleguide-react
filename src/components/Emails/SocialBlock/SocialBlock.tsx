import * as React from 'react';
import Icon, { IconFamily, IconType } from '../../Icons';

const SocialBlock: React.FunctionComponent<Record<string, never>> = () => (
  <div className="social-share">
    <a
      className="social-share--facebook"
      href="http://www.facebook.com/qustodio"
    >
      <Icon family={IconFamily.brands} type={IconType.facebookSquare} />
    </a>
    <a className="social-share--twitter" href="http://twitter.com/qustodio">
      <Icon family={IconFamily.brands} type={IconType.twitterSquare} />
    </a>
    <a
      className="social-share--linkedin"
      href="https://www.linkedin.com/company/qustodio"
    >
      <Icon family={IconFamily.brands} type={IconType.linkedin} />
    </a>
    <a
      className="social-share--youtube"
      href="https://www.youtube.com/channel/UCfqkXPNpfUGT2gpQ20jlmQA"
    >
      <Icon family={IconFamily.brands} type={IconType.youtubeSquare} />
    </a>
    <a
      className="social-share--instagram"
      href="https://www.instagram.com/qustodiohq/"
    >
      <Icon family={IconFamily.brands} type={IconType.instagram} />
    </a>
  </div>
);

export default SocialBlock;
