import * as React from 'react';
import * as SupportIcon from '../../../assets/images/emails/icon-support.png';

const SupportBlock: React.FunctionComponent<{
  IsPremium: boolean;
}> = ({ IsPremium }) => (
  <div className="r-support">
    <img className="r-support__icon" src={SupportIcon} alt="" />
    <h2 className="r-support__title">We&apos;re here to help</h2>
    <p className="r-support__text">
      {IsPremium
        ? `As a Premium user you have Priority Support. If you have any questions please contact our ${(
            <a href="{{ link }}">Help Center</a>
          )}`
        : `If you have any questions, please contact our ${(
            <a href="{{ link }}">Help Center</a>
          )}`}
    </p>
  </div>
);

SupportBlock.displayName = 'SupportBlock';

export default SupportBlock;
