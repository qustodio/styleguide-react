import * as React from 'react';
import Icon, { IconColor, IconType } from '../Icons';
import { ValueOf } from '../../common/types';
import { FontAwesomeIconType } from '../Icons/FontAwesome/types/FontAwesomeIconType.types';
import { RocketCustomIconType } from '../Icons/CustomIcons/types/RocketCustomIconType.types';
import { classNames, getTestId } from '../../helpers';

export type OpenerIconType = 'chevron' | 'more';
export type OpenerAnimations = 'rotate' | null;

export interface OpenerProps {
  type: OpenerIconType;
  open: boolean;
  testId?: string;
  color?: IconColor;
}

export enum OpenerTest {
  prefix = 'Opener',
}

const Opener = ({
  type,
  open,
  testId,
  color = IconColor.secondary,
}: OpenerProps) => {
  const iconMap: {
    [key in OpenerIconType]: {
      open: ValueOf<typeof IconType>;
      close: ValueOf<typeof IconType>;
      animation: OpenerAnimations;
    };
  } = {
    chevron: {
      open: IconType.angleUp,
      close: IconType.angleUp,
      animation: 'rotate',
    },
    more: {
      open: IconType.minus,
      close: IconType.plus,
      animation: null,
    },
  };

  const iconInfo = iconMap[type];

  return (
    <Icon
      type={
        open
          ? (iconInfo.open as FontAwesomeIconType & RocketCustomIconType)
          : (iconInfo.close as FontAwesomeIconType & RocketCustomIconType)
      }
      color={color}
      className={classNames(
        'rck-opener',
        'rck-opener--animation',
        `rck-opener--animation-${iconInfo.animation}${open ? '-opened' : ''}`
      )}
      testId={getTestId(OpenerTest.prefix, testId)}
    />
  );
};

export default Opener;
