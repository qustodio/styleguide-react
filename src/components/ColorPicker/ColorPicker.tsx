import * as React from 'react';
import {
  classNames,
  getTestId,
  handleKeyboardSelection,
} from '../../helpers/index';
import { ColorPickerProps } from './types/ColorPickerProps.types';
import ColorPickerTest from './types/ColorPickerTest.types';

const ColorPicker: React.FC<ColorPickerProps> = ({
  palette,
  activeColor,
  onClick,
  testId,
  className = '',
}) => {
  let colors: ColorPickerProps['palette'] = palette;
  if (Array.isArray(palette)) {
    colors = palette.reduce((a, b) => ({ ...a, [b]: b }), {});
  }

  return (
    <div
      role="grid"
      className={classNames('rck-color-picker', className)}
      data-testid={getTestId(ColorPickerTest.prefix, testId)}
    >
      {Object.entries(colors).map(([colorName, colorValue]) => (
        <span
          key={colorName}
          role="gridcell"
          aria-label={colorName}
          aria-selected={colorName === activeColor}
          tabIndex={0}
          style={{
            background: colorValue,
            ...{ '--react-rck-colorpicker-active-color': colorValue },
          }}
          className={classNames(
            'rck-color-picker__item',
            colorName === activeColor ? `rck-color-picker__item--active` : ''
          )}
          onClick={e => onClick(colorName, e)}
          onKeyDown={handleKeyboardSelection(e => onClick(colorName, e))}
        />
      ))}
    </div>
  );
};

ColorPicker.displayName = 'ColorPicker';

export default ColorPicker;
