import { BaseComponentProps } from '../../../common/types';

export interface ColorPickerProps extends BaseComponentProps {
  palette: Record<string, string> | string[];
  activeColor?: string;
  onClick: (color: string, ev: React.SyntheticEvent) => void;
}
