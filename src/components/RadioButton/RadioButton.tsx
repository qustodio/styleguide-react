import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps, DisabledProps } from '../../common/types';
import Icon, { IconFamily, IconType } from '../Icons';

export interface RadioButtonProps extends BaseComponentProps, DisabledProps {
  id: string;
  name?: string;
  checked?: boolean;
  block?: boolean;
  defaultChecked?: boolean;
  value: string;
  onClick?: (id: string) => void;
  onBlur?: (ev: React.SyntheticEvent) => void;
  onFocus?: (ev: React.SyntheticEvent) => void;
  onMouseDown?: (ev: React.SyntheticEvent) => void;
  onMouseUp?: (ev: React.SyntheticEvent) => void;
  onChange?: (ev: React.SyntheticEvent) => void;
}

export enum RadioButtonTest {
  prefix = 'RadioButton',
}

const RadioButton: React.FunctionComponent<RadioButtonProps> = ({
  className = '',
  name,
  id,
  checked,
  disabled,
  block,
  defaultChecked,
  value,
  testId,
  onClick,
  onBlur,
  onFocus,
  onMouseDown,
  onMouseUp,
  onChange,
}) => {
  const handleOnClick = () => {
    if (!disabled) {
      if (onClick) {
        onClick(value);
      }
    }
  };

  return (
    <label
      className={classNames(
        className,
        'rck-radiobutton',
        block ? 'rck-radiobutton--block' : '',
        disabled ? 'rck-radiobutton--disabled' : ''
      )}
      htmlFor={id}
      data-testid={getTestId(RadioButtonTest.prefix, testId)}
    >
      <input
        id={id}
        className="rck-radiobutton__input"
        name={name}
        type="radio"
        checked={checked}
        defaultChecked={defaultChecked}
        onClick={handleOnClick}
        onBlur={onBlur}
        onFocus={onFocus}
        onMouseDown={onMouseDown}
        onMouseUp={onMouseUp}
        onChange={onChange}
        value={value}
      />
      <span
        className={classNames(
          'rck-radiobutton__square',
          checked ? 'rck-radiobutton__square--checked' : '',
          disabled ? 'rck-radiobutton__square--disabled' : ''
        )}
      >
        {checked && (
          <Icon
            family={IconFamily.solid}
            type={IconType.circle}
            className={classNames(
              'rck-radiobutton__square__icon',
              disabled ? 'rck-radiobutton__square__icon--disabled' : ''
            )}
            role="presentation"
          />
        )}
      </span>
    </label>
  );
};

RadioButton.displayName = 'RadioButton';

export default RadioButton;
