import React from 'react';
import { getTestId, classNames } from '../../helpers/index';
import { BaseComponentProps } from '../../common/types';
import Icon, { IconColor, IconSize, IconType } from '../Icons';

export enum DomainTrustSealTest {
  prefix = 'DomainTrustSeal',
}

export type DomainTrustSealReputation =
  | 'unknown'
  | 'very-poor'
  | 'poor'
  | 'unsatisfactory'
  | 'good'
  | 'excellent';

export type DomainTrustSealMode = 'trustworthiness' | 'child-safety';

export interface DomainTrustSealProps extends BaseComponentProps {
  mode: DomainTrustSealMode;
  level: DomainTrustSealReputation;
  title: string;
  description: string;
}

const getConfigByReputation = (
  mode: DomainTrustSealMode,
  level: DomainTrustSealReputation
): { icon: JSX.Element; className: string } => {
  switch (level) {
    case 'excellent':
      return {
        icon:
          mode === 'trustworthiness' ? (
            <Icon
              type={IconType.grin}
              color={IconColor.success}
              size={IconSize.x2}
            />
          ) : (
            <Icon
              type={IconType.thumbsUp}
              color={IconColor.success}
              size={IconSize.x2}
            />
          ),
        className: 'good',
      };
    case 'good':
      return {
        icon:
          mode === 'trustworthiness' ? (
            <Icon
              type={IconType.smile}
              color={IconColor.success}
              size={IconSize.x2}
            />
          ) : (
            <Icon
              type={IconType.thumbsUp}
              color={IconColor.success}
              size={IconSize.x2}
            />
          ),
        className: 'good',
      };
    case 'unsatisfactory':
      return {
        icon:
          mode === 'trustworthiness' ? (
            <Icon
              type={IconType.meh}
              color={IconColor.warning}
              size={IconSize.x2}
            />
          ) : (
            <Icon
              type={IconType.thumbsDown}
              color={IconColor.warning}
              size={IconSize.x2}
            />
          ),
        className: 'unsatisfactory',
      };
    case 'poor':
      return {
        icon:
          mode === 'trustworthiness' ? (
            <Icon
              type={IconType.frown}
              color={IconColor.error}
              size={IconSize.x2}
            />
          ) : (
            <Icon
              type={IconType.thumbsDown}
              color={IconColor.error}
              size={IconSize.x2}
            />
          ),
        className: 'poor',
      };
    case 'very-poor':
      return {
        icon:
          mode === 'trustworthiness' ? (
            <Icon
              type={IconType.sadTear}
              color={IconColor.error}
              size={IconSize.x2}
            />
          ) : (
            <Icon
              type={IconType.thumbsDown}
              color={IconColor.error}
              size={IconSize.x2}
            />
          ),
        className: 'poor',
      };
    default:
      return {
        icon:
          mode === 'trustworthiness' ? (
            <Icon
              type={IconType.questionCircle}
              color={IconColor.neutral}
              size={IconSize.x2}
            />
          ) : (
            <Icon
              type={IconType.questionCircle}
              color={IconColor.neutral}
              size={IconSize.x2}
            />
          ),
        className: 'neutral',
      };
  }
};

const DomainTrustSeal: React.FunctionComponent<DomainTrustSealProps> = ({
  mode,
  level = 'unknown',
  title,
  description: levelDescription,
  testId,
  className = '',
}) => {
  const configuration = getConfigByReputation(mode, level);
  return (
    <div
      data-testid={getTestId(DomainTrustSealTest.prefix, testId)}
      className={classNames(
        'rck-domain-trust',
        `rck-domain-trust--${configuration.className}`,
        className
      )}
    >
      <div>{configuration.icon}</div>
      <div className="rck-domain-trust__info">
        <p className="rck-domain-trust__info__title">{title}</p>
        <p className="rck-domain-trust__info__description">
          {levelDescription}
        </p>
      </div>
    </div>
  );
};

DomainTrustSeal.displayName = 'DomainTrustSeal';

export default React.memo(DomainTrustSeal);
