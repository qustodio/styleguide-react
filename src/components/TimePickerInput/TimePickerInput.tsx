import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import TextFieldBase from '../TextFieldBase/TextFieldBase';
import Icon, { IconColor, IconSize, IconType } from '../Icons';
import { TimePickerInputProps } from './types/TimePickerInput.types';
import { TextFieldBaseProps } from '../TextFieldBase/types/TextFieldBase.types';

enum TimePickerInputTest {
  prefix = 'TimePickerInput',
}

const defaultErrorConfig: TextFieldBaseProps['errorConfig'] = {
  border: true,
  text: false,
};

const TimePickerInput: React.FunctionComponent<TimePickerInputProps> = ({
  name,
  value,
  testId,
  className = '',
  hasError = false,
  disabled = false,
  readOnly = false,
  placeholder = 'HH:mm',
  onChange,
  onClick,
  onBlur,
  onFocus,
  onClickTrigger,
}: TimePickerInputProps) => {
  const onChangeHandler = React.useCallback(
    (e: React.FormEvent<HTMLInputElement>) => {
      onChange?.(e.currentTarget.value, e);
    },
    [onChange]
  );

  return (
    <TextFieldBase
      type="text"
      name={name}
      value={value}
      selectAllOnFocus={!readOnly}
      readOnly={readOnly}
      hasError={hasError}
      errorConfig={defaultErrorConfig}
      disabled={disabled}
      placeholder={placeholder}
      width="block"
      className={classNames('rck-time-picker-input', className)}
      testId={getTestId(TimePickerInputTest.prefix, testId)}
      onChange={onChangeHandler}
      onClick={onClick}
      onBlur={onBlur}
      onFocus={onFocus}
      contentRight={
        <Icon
          onClick={onClickTrigger}
          type={IconType.clock}
          size={IconSize.lg}
          color={IconColor.secondary}
          square
        />
      }
    />
  );
};

TimePickerInput.displayName = 'TimePickerInput';

export default React.memo<TimePickerInputProps>(TimePickerInput);
