import * as React from 'react';
import { useMediaPredicate } from 'react-media-hook';
import { classNames, getTestId, isTestEnv } from '../../helpers';
import Button, { ButtonColor, ButtonSize } from '../Button/Button';
import Icon, { IconSize, IconType } from '../Icons';
import FlexLayout from '../Layout';
import Checkbox from '../Checkbox/Checkbox';
import AccordionMenu from '../AccordionMenu/AccordionMenu';
import Ribbon, { RibbonType } from '../Ribbon/Ribbon';
import TransparentButton from '../Button/TransparentButton';
import { PlanBoxBaseProps } from './types/PlanBoxBaseProps';

export enum PlanBoxTest {
  prefix = 'PlanBox',
}

export interface PlanBoxProps extends PlanBoxBaseProps {
  deviceInformation: string;
  hasDiscount?: boolean;
  ribbon?: {
    type: RibbonType;
    text: string;
  };
  carePlusInfo?: {
    showCheckbox: boolean;
    carePlusIncluded: boolean;
    label: JSX.Element;
    description: string;
    list: JSX.Element;
    onClick: () => void;
    isChecked: boolean;
  };
}

const useIsBiggerThanSmallScreen = () =>
  isTestEnv() ? false : useMediaPredicate('(min-width: 768px)');

const PlanBox: React.FunctionComponent<PlanBoxProps> = ({
  title,
  price,
  years,
  priceAdditionalInformation,
  deviceInformation,
  highlighted = false,
  hasDiscount = false,
  current,
  currentText,
  ribbon,
  button,
  carePlusInfo,
  className,
  testId,
}) => {
  const biggerThanSmallScreen = useIsBiggerThanSmallScreen();
  const [showCheckboxHelpInfo, setShowCheckboxHelpInfo] = React.useState(false);
  return (
    <div
      className={classNames(
        `rck-plan-box`,
        current ? `rck-plan-box--current` : '',
        highlighted ? 'rck-plan-box--highlighted' : '',
        hasDiscount ? 'rck-plan-box--with-discount' : '',
        className || ''
      )}
      data-testid={getTestId(PlanBoxTest.prefix, testId)}
    >
      {ribbon?.type !== undefined && ribbon?.text ? (
        <Ribbon type={ribbon.type}>{ribbon.text}</Ribbon>
      ) : null}
      <FlexLayout mainaxis="column" display="contents">
        <>
          <div className="rck-plan-box__body">
            <h2 className="rck-plan-box__title">{title}</h2>

            <div className="rck-plan-box__price rck-plan-box__price-per-year-text">
              <FlexLayout mainaxis="row" crossaxisAlignment="flex-end">
                <span className="price">{price}</span>
                {years ? <span className="price-period">/{years}</span> : null}
              </FlexLayout>
            </div>

            {priceAdditionalInformation && (
              <div className="rck-plan-box__price-additional-information">
                {priceAdditionalInformation}
              </div>
            )}
          </div>

          <div className="rck-plan-box__device-information">
            <FlexLayout mainaxis="row" crossaxisAlignment="center">
              <FlexLayout mainaxis="row" marginRight="8">
                <Icon type={IconType.phoneLaptop} size={IconSize.xs} />
              </FlexLayout>
              <span>{deviceInformation}</span>
            </FlexLayout>
          </div>

          {carePlusInfo && (
            <div className="rck-plan-box__checkbox-container">
              <FlexLayout mainaxis="column">
                <FlexLayout mainaxis="row" mainaxisAlignment="center">
                  {carePlusInfo?.carePlusIncluded ? (
                    carePlusInfo?.label
                  ) : (
                    <Checkbox
                      name="test"
                      label={carePlusInfo?.label}
                      checked={carePlusInfo?.isChecked}
                      onClick={carePlusInfo?.onClick}
                    />
                  )}
                  <TransparentButton
                    onClick={() =>
                      setShowCheckboxHelpInfo(!showCheckboxHelpInfo)
                    }
                  >
                    <Icon
                      type={IconType.questionCircle}
                      size={IconSize.regular}
                    />
                  </TransparentButton>
                </FlexLayout>
                <AccordionMenu isVisible={showCheckboxHelpInfo}>
                  <p className="rck-plan-box__checkbox-description">
                    {carePlusInfo?.description}
                  </p>
                  {carePlusInfo.list}
                </AccordionMenu>
              </FlexLayout>
            </div>
          )}
        </>
        {current ? (
          <div className="rck-plan-box__current-text">{currentText}</div>
        ) : (
          <Button
            loading={button.isLoading}
            disabled={button.isLoading}
            onClick={button.onClick}
            size={biggerThanSmallScreen ? ButtonSize.big : ButtonSize.medium}
            testId={testId}
            color={highlighted ? ButtonColor.white : ButtonColor.secondary}
            block
          >
            {button.text}
          </Button>
        )}
      </FlexLayout>
    </div>
  );
};

PlanBox.displayName = 'PlanBox';

export default PlanBox;
