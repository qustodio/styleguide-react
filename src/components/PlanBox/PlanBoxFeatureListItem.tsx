import * as React from 'react';
import { PlanBoxFeatureListItemProps } from './types/PlanBoxFeatureListItemProps';
import { FreeStyleListItem } from '../List';
import FlexLayout from '../Layout';

const PlanBoxFeatureListItem: React.FC<PlanBoxFeatureListItemProps> = ({
  icon,
  text,
}: PlanBoxFeatureListItemProps) => (
  <FreeStyleListItem className="rck-feature-list-item">
    <FlexLayout mainaxis="row" marginTop="8">
      <FlexLayout mainaxis="row" marginRight="8" minWidth="24">
        {icon}
      </FlexLayout>
      {text}
    </FlexLayout>
  </FreeStyleListItem>
);

PlanBoxFeatureListItem.displayName = 'PlanBoxFeatureListItem';

export default PlanBoxFeatureListItem;
