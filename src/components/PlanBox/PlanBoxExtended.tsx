import * as React from 'react';
import Button, { ButtonColor, ButtonSize, ButtonType } from '../Button/Button';
import Icon, { IconColor, IconFamily, IconType } from '../Icons';
import FlexLayout from '../Layout';
import { PlanBoxBaseProps } from './types/PlanBoxBaseProps';
import { getTestId } from '../../helpers';
import PlanBoxFeatureListItem from './PlanBoxFeatureListItem';
import { PlanBoxAddonCheckboxProps } from './types/PlanBoxAddonCheckboxProps';
import PlanBoxAddonCheckbox from './PlanBoxAddonCheckbox';

export interface PlanBoxExtendedProps extends PlanBoxBaseProps {
  icon: IconType;
  subtitle: string;
  priceAdditionalInformation: string;
  ribbon?: {
    text: string;
  };
  features?: {
    title: string;
    list: string[];
  };
  addonInfo?: PlanBoxAddonCheckboxProps;
}

const PlanBoxExtended = ({
  icon,
  title,
  subtitle,
  price,
  years,
  priceAdditionalInformation,
  highlighted,
  current,
  currentText,
  addonInfo,
  ribbon,
  button,
  features,
  testId,
}: PlanBoxExtendedProps) => {
  const highlightedClass = highlighted
    ? 'rck-plan-box-extended--highlighted'
    : '';

  return (
    <div
      className={`rck-plan-box-extended ${highlightedClass}`}
      data-testid={getTestId('rck-plan-box-extended', testId)}
    >
      {ribbon && (
        <div className="rck-plan-box-extended__ribbon">{ribbon.text}</div>
      )}
      <FlexLayout mainaxis="column" display="contents">
        <>
          {/* Plan Box Body */}
          <div className="rck-plan-box-extended__body">
            <h1 className="rck-plan-box-extended__title">
              <Icon
                className="rck-plan-box-extended__title-icon"
                type={icon}
                family={IconFamily.regular}
                color={IconColor.greenDecorationContrast}
              />
              {title}
            </h1>
            <h2 className="rck-plan-box-extended__subtitle">{subtitle}</h2>
            <span className="rck-plan-box-extended__divider" />
            <p className="rck-plan-box-extended__price">
              {price}
              {years && (
                <span className="rck-plan-box-extended__price--period">
                  /{years}
                </span>
              )}
            </p>
            <p className="rck-plan-box-extended__price-info">
              {priceAdditionalInformation}
            </p>
          </div>
          {/* Plan Box Care Plus */}
          {addonInfo && (
            <div className="rck-plan-box-extended__addon-info">
              <PlanBoxAddonCheckbox {...addonInfo} />
            </div>
          )}
          {/* Plan Box Call to action */}
          <div className="rck-plan-box-extended__call-to-action">
            {current ? (
              <div className="rck-plan-box-extended__current-text">
                {currentText}
              </div>
            ) : (
              <Button
                loading={button.isLoading}
                disabled={button.isLoading}
                onClick={button.onClick}
                size={ButtonSize.big}
                color={highlighted ? ButtonColor.white : ButtonColor.secondary}
                buttonType={
                  highlighted ? ButtonType.primary : ButtonType.secondary
                }
                block
              >
                {button.text}
              </Button>
            )}
          </div>
          {/* Plan Box Features */}
          {features && (
            <div className="rck-plan-box-extended__features rck-feature-list">
              <div className="rck-feature-list__title">{features.title}</div>
              <ul className="rck-feature-list__list">
                {features.list?.map((text, index) => (
                  <PlanBoxFeatureListItem
                    key={`plan-box-feature-${index.toString()}`}
                    icon={
                      <Icon
                        family={IconFamily.solid}
                        color={IconColor.greenDecorationContrast}
                        type={IconType.checkCircle}
                      />
                    }
                    text={text}
                  />
                ))}
              </ul>
            </div>
          )}
        </>
      </FlexLayout>
    </div>
  );
};

PlanBoxExtended.displayName = 'PlanBoxExtended';

export default PlanBoxExtended;
