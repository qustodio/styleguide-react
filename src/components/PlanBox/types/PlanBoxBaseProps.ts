import { BaseComponentProps } from '../../../common/types';

export interface PlanBoxBaseProps extends BaseComponentProps {
  title: string;
  price: string;
  years?: string;
  priceAdditionalInformation?: string;
  deviceInformation: string;
  highlighted?: boolean;
  current?: boolean;
  currentText?: string;
  button: {
    text: string;
    isLoading: boolean;
    onClick: () => void;
  };
}
