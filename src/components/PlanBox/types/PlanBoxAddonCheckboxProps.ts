import { PlanBoxFeatureListItemProps } from './PlanBoxFeatureListItemProps';

export interface PlanBoxAddonCheckboxProps {
  label: JSX.Element;
  showCheckbox: boolean;
  description: string;
  list: PlanBoxFeatureListItemProps[];
  onClick: () => void;
  isChecked: boolean;
}
