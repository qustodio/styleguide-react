import * as React from 'react';
import { PlanBoxAddonCheckboxProps } from './types/PlanBoxAddonCheckboxProps';
import AccordionMenu from '../AccordionMenu/AccordionMenu';
import PlanBoxFeatureListItem from './PlanBoxFeatureListItem';
import TransparentButton from '../Button/TransparentButton';
import Icon, { IconFamily, IconType } from '../Icons';
import Checkbox from '../Checkbox/Checkbox';
import FlexLayout from '../Layout';

const PlanBoxAddonCheckbox = ({
  label,
  showCheckbox,
  isChecked,
  onClick,
  description,
  list,
}: PlanBoxAddonCheckboxProps) => {
  const [showCheckboxHelpInfo, setShowCheckboxHelpInfo] = React.useState(false);

  return (
    <div className="rck-addon-checkbox">
      <FlexLayout mainaxis="column">
        <FlexLayout mainaxis="row" mainaxisAlignment="left">
          {showCheckbox ? (
            <Checkbox
              name="addon-checkbox"
              label={label}
              checked={isChecked}
              onClick={onClick}
            />
          ) : (
            label
          )}
          <TransparentButton
            onClick={() => setShowCheckboxHelpInfo(!showCheckboxHelpInfo)}
            className={`rck-addon-checkbox__icon-info ${showCheckboxHelpInfo &&
              'rck-addon-checkbox__icon-info--open'}`}
          >
            <Icon
              type={IconType.questionCircle}
              family={
                showCheckboxHelpInfo ? IconFamily.solid : IconFamily.regular
              }
            />
          </TransparentButton>
        </FlexLayout>

        <AccordionMenu isVisible={showCheckboxHelpInfo}>
          <div className="rck-addon-checkbox__content">
            <p className="rck-addon-checkbox__description">{description}</p>
            {list.map(({ icon, text }, index) => (
              <PlanBoxFeatureListItem
                key={`addon-checkbox-feature-${index.toString()}`}
                icon={icon}
                text={text}
              />
            ))}
          </div>
        </AccordionMenu>
      </FlexLayout>
    </div>
  );
};

PlanBoxAddonCheckbox.displayName = 'PlanBoxAddonCheckbox';

export default PlanBoxAddonCheckbox;
