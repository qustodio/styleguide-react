import * as React from 'react';
import { useRef } from 'react';
import {
  BaseComponentProps,
  DisabledProps,
  GlobalType,
} from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import useDomAnimation from '../../hooks/useDomAnimation';

export interface TabPanelProps extends BaseComponentProps, DisabledProps {
  content: string | JSX.Element;
  active: boolean;
  name: string;
  animateWhenChange?: string;
  children: React.ReactNode;
  activeColor?: GlobalType;
}

export enum TabPanelTest {
  prefix = 'TabPanel',
}

const TabPanel: React.FunctionComponent<TabPanelProps> = ({
  name,
  className = '',
  animateWhenChange,
  testId,
  children,
}) => {
  const tabPanelRef = useRef(document.createElement('div'));
  useDomAnimation(
    tabPanelRef,
    'rck-tab-panel--entering',
    200,
    [animateWhenChange],
    !animateWhenChange
  );

  return (
    <div
      id={name}
      className={classNames(`rck-tab-panel`, className)}
      role="tabpanel"
      data-testid={getTestId(TabPanelTest.prefix, testId, name)}
      ref={tabPanelRef}
    >
      {children}
    </div>
  );
};

TabPanel.displayName = 'TabPanel';

export default TabPanel;
