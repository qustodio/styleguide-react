import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import TabPanel, { TabPanelProps } from './TabPanel';

export interface TabProps extends BaseComponentProps {
  onChange: (name: string) => void;
  animateOnChange?: boolean;
  showSeparator?: boolean;
  hugContent?: boolean;
}

export enum TabsTest {
  prefix = 'Tabs',
  tab = 'Tab',
}

const Tabs: React.FunctionComponent<TabProps> = ({
  className = '',
  onChange,
  animateOnChange,
  testId,
  children,
  showSeparator = true,
  hugContent = false,
}) => {
  const handleClick = (name: string, disabled?: boolean) => () => {
    if (onChange && !disabled) {
      onChange(name);
    }
  };
  let panel = null;
  let name;
  return (
    <div
      className={classNames(
        `rck-tabs`,
        hugContent ? 'rck-tabs--hug-content' : '',
        className
      )}
      data-testid={getTestId(TabsTest.prefix, testId)}
    >
      <ul className="rck-tabs__list">
        {React.Children.map(children, child => {
          const tabPanel = child as React.ReactElement<TabPanelProps>;
          if (tabPanel === null) {
            return null;
          }

          if (tabPanel.type !== TabPanel) {
            throw Error('Tabs must containn TabPanel children');
          }

          const isActive = !tabPanel.props.disabled && tabPanel.props.active;
          const activeColor =
            isActive && tabPanel.props.activeColor
              ? tabPanel.props.activeColor
              : null;

          if (isActive) {
            name = tabPanel.props.name;
            panel = tabPanel;
          }

          return (
            <li
              className={classNames(
                `rck-tab`,
                isActive ? `rck-tab--active` : '',
                activeColor ? `rck-tab--active-color-${activeColor}` : '',
                tabPanel.props.disabled ? 'rck-tab--disabled' : '',
                !showSeparator ? 'rck-tab--no-separator' : '',
                className
              )}
              data-name={tabPanel.props.name}
              role="tab"
              key={tabPanel.props.name}
              onClick={handleClick(
                tabPanel.props.name,
                tabPanel.props.disabled
              )}
              onKeyDown={handleClick(
                tabPanel.props.name,
                tabPanel.props.disabled
              )}
              tabIndex={0}
              data-testid={getTestId(TabsTest.tab, testId, tabPanel.props.name)}
            >
              {isActive ? (
                <strong>{tabPanel.props.content}</strong>
              ) : (
                tabPanel.props.content
              )}
            </li>
          );
        })}
      </ul>
      {panel &&
        React.cloneElement(panel, {
          animateWhenChange: animateOnChange ? name : undefined,
        })}
    </div>
  );
};

Tabs.displayName = 'Tabs';

export default Tabs;
