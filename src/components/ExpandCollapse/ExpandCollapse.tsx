import * as React from 'react';
import ExpandableBase from '../ExpandableBase/ExpandableBase';
import { BaseComponentProps } from '../../common/types';
import { OpenerIconType } from '../Opener/Opener';
import Text from '../Text';
import Icon, { IconColor, IconType } from '../Icons';
import { classNames } from '../../helpers';

interface ExpandCollapseProps extends BaseComponentProps {
  leftIcon?: IconType;
  leftImgSrc?: string;
  title: string;
  rightText?: string;
  rightElement?: JSX.Element;
  initialOpenState?: boolean;
  isDisabled?: boolean;
  testId: string;
  openerType?: OpenerIconType;
  onClick?: () => void;
}

const ExpandCollapse = ({
  leftIcon,
  leftImgSrc,
  title,
  rightText,
  initialOpenState,
  isDisabled,
  testId,
  openerType,
  children,
  className,
  rightElement,
  onClick,
}: ExpandCollapseProps) => {
  const leftContent = (): JSX.Element => (
    <div className="rck-expand-collapse__header-right">
      <span className="rck-expand-collapse__header-right-icon">
        {leftIcon && (
          <Icon
            type={leftIcon}
            className="rck-expand-collapse__header-icon"
            circle
            color={isDisabled ? IconColor.grayLight : IconColor.secondary}
          />
        )}
        {leftImgSrc && (
          <img
            src={leftImgSrc}
            alt={title}
            className={classNames(
              'rck-expand-collapse__header-image',
              isDisabled ? 'rck-expand-collapse__header-image--disabled' : ''
            )}
          />
        )}
      </span>
      <Text variant="body-2-regular">{title}</Text>
    </div>
  );

  const rightContent = () => {
    if (rightElement) return rightElement;
    if (rightText)
      return <Text variant="title-2-semibold-responsive">{rightText}</Text>;
    return undefined;
  };

  return (
    <ExpandableBase
      leftContent={leftContent()}
      rightContent={rightContent()}
      className={className}
      initialOpenState={initialOpenState}
      isDisabled={isDisabled}
      openerType={openerType}
      openerColor={isDisabled ? IconColor.grayLight : IconColor.secondary}
      testId={testId}
      onClick={onClick}
    >
      {children}
    </ExpandableBase>
  );
};

export default ExpandCollapse;
