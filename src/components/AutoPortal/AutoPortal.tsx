import * as React from 'react';
import ReactDOM from 'react-dom';
import { BaseComponentProps } from '../../common/types';
import { getTestId } from '../../helpers';

export enum AutoPortalTest {
  prefix = 'AutoPortal',
}

type Tag = keyof HTMLElementTagNameMap;

const getElement = ({
  id,
  className,
  tag,
}: {
  id: string;
  className: string;
  tag: Tag;
}) => {
  const element: HTMLElement =
    document.getElementById(id) || document.createElement(tag);

  if (className) {
    element.className = className;
  }
  element.setAttribute(
    'data-testid',
    getTestId(AutoPortalTest.prefix, id) || ''
  );
  return element;
};

interface PortalProps extends BaseComponentProps {
  id: string;
  className?: string;
}

/**
 *
 * Portal that uses `document.body` as container
 *
 * Behavior
 *
 * 1. Portals are added as div chid of body.
 * 2. Must define id.
 *
 *
 * @example
 *
 *```tsx
 * import { AutoPortal } from 'styleguide-react';
 * import { Modal } from '../components/Modal'
 *
 * const Modal: React.FC<ModalProps> = ({ children }) => {
 *   return <AutoPortal id="global-modal" className="custom-modal">
 *           <Modal>
 *            {children}
 *           </Modal>
 *          </AutoPortal>
 *
 * }
 *
 *```
 *
 */
const AutoPortal: React.FC<PortalProps> = React.memo(
  ({ id, children, className = '' }) => {
    const [, rerender] = React.useState({});
    const [tmpNode, setTmpNode] = React.useState<HTMLElement | null>(null);
    const portal = React.useRef<HTMLElement | null>(null);

    React.useEffect(() => {
      const portalNode = getElement({ id, className, tag: 'div' });
      portal.current = portalNode;

      const isNewlyCreatedDomNode = !portalNode.isConnected;
      if (!isNewlyCreatedDomNode) {
        rerender({});
        return undefined;
      }

      if (!tmpNode) return undefined;
      const host = tmpNode?.ownerDocument.body;
      if (!host) return undefined;

      host.appendChild(portal.current);
      rerender({}); // we need to re-render in order for the host.appendChild to take effect

      // cleanup
      return () => {
        if (isNewlyCreatedDomNode && host.contains(portalNode)) {
          host.removeChild(portalNode);
        }
      };
    }, [tmpNode]);

    return portal.current ? (
      ReactDOM.createPortal(children, portal.current)
    ) : (
      <span
        ref={el => {
          if (el) {
            setTmpNode(el);
          }
        }}
      />
    );
  }
);

export default AutoPortal;
