import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps, DisabledProps } from '../../common/types';
import TransparentButton from '../Button/TransparentButton';
import Icon, { IconColor, IconSize, IconType } from '../Icons';
import InstructionBlock from '../InstructionBlock/InstructionBlock';

export interface SelectionBarProps extends BaseComponentProps, DisabledProps {
  title: string;
  hasNext: boolean;
  hasPrev: boolean;
  onClickNext: (ev: React.SyntheticEvent) => void;
  onClickPrev: (ev: React.SyntheticEvent) => void;
}

export enum SelectionBarTest {
  prefix = 'SelectionBar',
}

const SelectionBar: React.FunctionComponent<SelectionBarProps> = ({
  className = '',
  title,
  testId,
  hasNext,
  hasPrev,
  onClickNext,
  onClickPrev,
}) => (
  <div
    className={classNames(className, 'rck-selectionbar')}
    data-testid={getTestId(SelectionBarTest.prefix, testId)}
  >
    <div className="rck-selectionbar__content">
      <TransparentButton
        onClick={onClickPrev}
        className={classNames(
          'rck-selectionbar__content__button',
          !hasPrev ? 'rck-selectionbar__content__button--hidden' : ''
        )}
      >
        <Icon
          type={IconType.angleLeft}
          color={IconColor.regular}
          size={IconSize.lg}
        />
      </TransparentButton>
      <InstructionBlock
        className="rck-selectionbar__content__title"
        block={false}
      >
        {title}
      </InstructionBlock>
      <TransparentButton
        onClick={onClickNext}
        className={classNames(
          'rck-selectionbar__content__button',
          !hasNext ? 'rck-selectionbar__content__button--hidden' : ''
        )}
      >
        <Icon
          type={IconType.angleRight}
          color={IconColor.regular}
          size={IconSize.lg}
        />
      </TransparentButton>
    </div>
  </div>
);

SelectionBar.displayName = 'SelectionBar';

export default SelectionBar;
