// components
export { default } from './Tag';

// types
export { TagProps } from './types/TagProps.types';
export { TagSize } from './types/TagSize.types';
export { TagType } from './types/TagType.types';

// enums
export { default as TagTest } from './types/TagTest.types';
