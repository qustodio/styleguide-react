/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import * as React from 'react';
import { classNames, getTestId } from '../../helpers/index';
import { TagProps } from './types/TagProps.types';
import TagTest from './types/TagTest.types';
import Icon, { IconType } from '../Icons';
import { GlobalType } from '../../common/types';

const Tag: React.FC<TagProps> = ({
  text,
  variant = 'squared',
  size = 'regular',
  type = GlobalType.primary,
  invertColor = false,
  iconType,
  iconProps,
  onClick,
  testId,
  className = '',
}) => (
  <div
    title={text}
    className={classNames(
      'rck-tag',
      `rck-tag--variant-${variant}`,
      `rck-tag--size-${size}`,
      `rck-tag--type-${type}`,
      invertColor ? `rck-tag--invert-color` : '',
      iconType && !text ? 'rck-tag--only-icon' : '',
      onClick ? 'rck-tag--clickable' : '',
      className
    )}
    data-testid={getTestId(TagTest.prefix, testId)}
    onClick={onClick}
  >
    {iconType ? (
      <Icon
        {...iconProps}
        type={iconType as IconType}
        className={classNames('rck-tag__icon', iconProps?.className || '')}
      />
    ) : null}
    {iconType && text && <span className="rck-tag__spacer" />}
    {text}
  </div>
);

Tag.displayName = 'Tag';

export default Tag;
