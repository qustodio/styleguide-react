import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export enum RibbonType {
  cornerRight,
  cornerLeft,
}
export interface RibbonProps extends BaseComponentProps {
  type: RibbonType;
}

export enum RibbonTest {
  prefix = 'Ribbon',
}

const Ribbon: React.FunctionComponent<RibbonProps> = ({
  type,
  className = '',
  testId,
  children,
}) => {
  const ribbonTypeToString: string = {
    [RibbonType.cornerRight]: 'corner-right',
    [RibbonType.cornerLeft]: 'corner-left',
  }[type];

  return (
    <div
      className={classNames(
        'rck-ribbon',
        `rck-ribbon--${ribbonTypeToString}`,
        className
      )}
      data-testid={getTestId(RibbonTest.prefix, testId)}
    >
      <span>{children}</span>
    </div>
  );
};

Ribbon.displayName = 'Ribbon';

export default Ribbon;
