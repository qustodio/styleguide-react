import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Text from '../Text/Text';

export type StyledHeaderType = 'h1' | 'h2' | 'h3' | 'h4' | 'h5';

export interface StyledHeaderProps extends BaseComponentProps {
  type?: StyledHeaderType;
}

export enum StyledHeaderTest {
  prefix = 'StyledHeader',
}

const StyledHeader: React.FunctionComponent<StyledHeaderProps> = ({
  type = 'h1',
  className = '',
  testId,
  children,
}) => {
  const classes = classNames('rck-styled-header', className);
  const dataTestId = getTestId(StyledHeaderTest.prefix, testId);
  const h1Element = (
    <Text
      className={classes}
      renderAs="h1"
      variant="headline-1-semibold-responsive"
      parentTestId={dataTestId}
    >
      {children}
    </Text>
  );
  switch (type) {
    case 'h1':
      return h1Element;
    case 'h2':
      return (
        <Text
          className={classes}
          renderAs="h2"
          variant="headline-2-semibold-responsive"
          parentTestId={dataTestId}
        >
          {children}
        </Text>
      );
    case 'h3':
      return (
        <Text
          className={classes}
          renderAs="h3"
          variant="title-1-semibold-responsive"
          parentTestId={dataTestId}
        >
          {children}
        </Text>
      );
    case 'h4':
      return (
        <Text
          className={classes}
          renderAs="h4"
          variant="title-2-semibold-responsive"
          parentTestId={dataTestId}
        >
          {children}
        </Text>
      );
    case 'h5':
      return (
        <Text
          className={classes}
          renderAs="h5"
          variant="title-3-semibold-responsive"
          parentTestId={dataTestId}
        >
          {children}
        </Text>
      );
    default:
      return h1Element;
  }
};

export default React.memo(StyledHeader);
