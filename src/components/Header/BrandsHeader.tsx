import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconColor, IconFamily, IconSize, IconType } from '../Icons';
import FlexLayout from '../Layout';

export enum BrandsHeaderTest {
  prefix = 'BrandsHeader',
}

const StyledHeader: React.FunctionComponent<BaseComponentProps> = ({
  className = '',
  testId,
  children,
}) => {
  const dataTestId = getTestId(BrandsHeaderTest.prefix, testId);

  return (
    <div
      className={classNames('rck-brands-header', className)}
      data-testid={dataTestId}
    >
      {children || (
        <FlexLayout mainaxis="row" mainaxisAlignment="center">
          <Icon
            family={IconFamily.brands}
            type={IconType.windows}
            color={IconColor.primary}
            size={IconSize.lg}
          />
          <Icon
            family={IconFamily.brands}
            type={IconType.apple}
            color={IconColor.primary}
            size={IconSize.lg}
          />
          <Icon
            family={IconFamily.brands}
            type={IconType.chrome}
            color={IconColor.primary}
            size={IconSize.lg}
          />
          <Icon
            family={IconFamily.brands}
            type={IconType.android}
            color={IconColor.primary}
            size={IconSize.lg}
          />
        </FlexLayout>
      )}
    </div>
  );
};

export default React.memo(StyledHeader);
