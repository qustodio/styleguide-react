import * as React from 'react';
import { useState, SyntheticEvent } from 'react';
import { DisabledProps, BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import useAssignRef from '../../hooks/useAssignRef';

export type InputType =
  | 'text'
  | 'password'
  | 'email'
  | 'tel'
  | 'number'
  | 'search';

export type InputIconConfiguration = {
  icon: JSX.Element;
  showIconWhen: InputShowIconWhen;
};

export enum InputShowIconWhen {
  always,
  onError,
  edit,
}

export enum GroupedInputTest {
  prefix = 'GroupedInput',
  actionElement = 'Action-element',
  input = 'Input',
}

const isEditing = (hasContent: boolean, hasFocus: boolean) =>
  hasContent && hasFocus;

const showIcon = (
  hasContent: boolean,
  hasFocus: boolean,
  iconConfiguration?: InputIconConfiguration,
  error?: boolean
) =>
  iconConfiguration &&
  ((iconConfiguration.showIconWhen === InputShowIconWhen.onError && error) ||
    iconConfiguration.showIconWhen === InputShowIconWhen.always ||
    (isEditing(hasContent, hasFocus) &&
      iconConfiguration.showIconWhen === InputShowIconWhen.edit));

type GroupedInputProps = DisabledProps &
  BaseComponentProps & {
    type: InputType;
    value?: string;
    id?: string;
    defaultValue?: string;
    error?: boolean;
    placeholder?: string;
    iconConfiguration?: InputIconConfiguration;
    autoComplete?: string;
    autoCorrect?: string;
    autoCapitalize?: string;
    spellCheck?: boolean;
    maxlength?: number;
    minlength?: number;
    required?: boolean;
    min?: number;
    max?: number;
    pattern?: string;
    name: string;
    iconLeft?: JSX.Element;
    onChange?: (ev: React.SyntheticEvent, value: string) => void;
    onBlur?: (ev: React.SyntheticEvent) => void;
    onDragStart?: (ev: React.SyntheticEvent) => void;
    onFocus?: (ev: React.SyntheticEvent) => void;
    onDrop?: (ev: React.SyntheticEvent) => void;
    onIconClick?: (ev: React.SyntheticEvent) => void;
  };

const GroupedInput = React.forwardRef<HTMLInputElement, GroupedInputProps>(
  (
    {
      id,
      value,
      defaultValue,
      className = '',
      disabled = false,
      type,
      error,
      placeholder = '',
      iconConfiguration,
      autoComplete,
      autoCorrect,
      autoCapitalize,
      spellCheck,
      maxlength,
      minlength,
      required,
      min,
      max,
      pattern,
      name,
      iconLeft,
      testId,
      onChange,
      onBlur,
      onDragStart,
      onFocus,
      onDrop,
      onIconClick,
    },
    ref
  ) => {
    const textInput = React.useRef(document.createElement('input'));
    const containerRef = React.useRef(document.createElement('div'));
    const [isFocused, setIsFocused] = useState(false);
    const [hasContent, setHasContent] = useState(false);

    const handleOnChange = (ev: SyntheticEvent) => {
      setHasContent(textInput.current.value !== '');
      if (onChange) {
        onChange(ev, textInput.current.value);
      }
    };

    const handleOnIconClick = (ev: React.SyntheticEvent) => {
      if (iconConfiguration?.showIconWhen === InputShowIconWhen.edit) {
        if (textInput !== null && textInput.current !== null) {
          textInput.current.value = '';
          handleOnChange(ev);
        }
      }
      if (onIconClick) {
        onIconClick(ev);
      }
    };

    const handleOnFocus = (ev: React.SyntheticEvent) => {
      setIsFocused(true);
      setHasContent(textInput.current.value !== '');
      containerRef.current.classList.add('rck-text-field__grouped--focused');

      if (onFocus) {
        onFocus(ev);
      }
    };

    const handleOnBlur = (ev: React.SyntheticEvent) => {
      setIsFocused(false);
      setHasContent(textInput.current.value !== '');
      containerRef.current.classList.remove('rck-text-field__grouped--focused');

      if (onBlur) {
        onBlur(ev);
      }
    };

    const onMouseEnterHandler = () => {
      containerRef.current.classList.add('rck-text-field__grouped--hover');
    };

    const onMouseLeaveHandler = () => {
      containerRef.current.classList.remove('rck-text-field__grouped--hover');
    };

    return (
      <div
        className={classNames(
          'rck-text-field__grouped',
          disabled ? 'rck-text-field__grouped--disabled' : ''
        )}
        ref={containerRef}
        data-testid={getTestId(GroupedInputTest.prefix, testId)}
      >
        {iconLeft && (
          <span className="rck-text-field__icon-left">
            <span className="rck-text-field__icon__wrapper">{iconLeft}</span>
          </span>
        )}
        <input
          ref={useAssignRef(textInput, ref)}
          id={id}
          value={value}
          defaultValue={defaultValue}
          disabled={disabled}
          type={type}
          placeholder={placeholder}
          onChange={handleOnChange}
          className={classNames(
            'rck-text-field__input',
            className,
            iconConfiguration ? 'rck-text-field__input--with-icon' : '',
            iconLeft ? 'rck-text-field__input--with-icon-left' : ''
          )}
          name={name}
          autoComplete={autoComplete}
          autoCorrect={autoCorrect}
          autoCapitalize={autoCapitalize}
          spellCheck={spellCheck}
          maxLength={maxlength}
          minLength={minlength}
          required={required}
          min={min}
          max={max}
          pattern={pattern}
          data-testid={getTestId(
            GroupedInputTest.prefix,
            testId,
            GroupedInputTest.input
          )}
          onFocus={handleOnFocus}
          onBlur={handleOnBlur}
          onDragStart={onDragStart}
          onDrop={onDrop}
          onMouseEnter={onMouseEnterHandler}
          onMouseLeave={onMouseLeaveHandler}
        />
        {iconConfiguration && (
          <span
            className="rck-text-field__icon"
            onClick={handleOnIconClick}
            onKeyDown={handleOnIconClick}
            role="button"
            tabIndex={0}
            data-testid={getTestId(
              GroupedInputTest.prefix,
              testId,
              GroupedInputTest.actionElement
            )}
          >
            <span
              className={classNames(
                'rck-text-field__icon__wrapper',
                showIcon(hasContent, isFocused, iconConfiguration, error)
                  ? ''
                  : 'rck-text-field__icon__wrapper--hide'
              )}
            >
              {React.cloneElement(iconConfiguration.icon)}
            </span>
          </span>
        )}
      </div>
    );
  }
);

GroupedInput.displayName = 'GroupedInput';

export default GroupedInput;
