import * as React from 'react';
import { useState } from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconType, IconFamily } from '../Icons';
import InfoFieldLabel, { InfoFieldLabelLines } from '../Label/InfoFieldLabel';
import GroupedInput, {
  InputType,
  InputIconConfiguration,
  InputShowIconWhen,
} from './GroupedInput';

export { InputType, InputIconConfiguration, InputShowIconWhen };

export type SupportTextConfiguration = {
  text: string;
  allowedLines?: InfoFieldLabelLines;
};

enum PasswordMode {
  none,
  hide,
  show,
}

export enum TextFieldTest {
  prefix = 'TextField',
  supportText = 'Support_content',
}

export type TextInputSize = 'small' | 'regular';
export interface TextFieldProps extends BaseComponentProps, DisabledProps {
  id?: string;
  label?: string;
  block?: boolean;
  type: InputType;
  error?: boolean;
  supportTextConfiguration?: SupportTextConfiguration;
  placeholder?: string;
  autoComplete?: string;
  autoCorrect?: string;
  autoCapitalize?: string;
  spellCheck?: boolean;
  maxlength?: number;
  minlength?: number;
  required?: boolean;
  min?: number;
  max?: number;
  pattern?: string;
  name: string;
  value?: string;
  defaultValue?: string;
  canBeCleared?: boolean;
  showAlertOnError?: boolean;
  iconLeft?: JSX.Element;
  keepErrorLabelArea?: boolean;
  size?: TextInputSize;
  textCentered?: boolean;
  onIconClick?: (ev: React.SyntheticEvent) => void;
  onChange?: (ev: React.SyntheticEvent, value: string) => void;
  onBlur?: (ev: React.SyntheticEvent) => void;
  onDragStart?: (ev: React.SyntheticEvent) => void;
  onFocus?: (ev: React.SyntheticEvent) => void;
  onDrop?: (ev: React.SyntheticEvent) => void;
}

const TextField = React.forwardRef<HTMLInputElement, TextFieldProps>(
  (
    {
      id,
      label,
      className = '',
      disabled,
      block,
      type,
      error,
      supportTextConfiguration,
      placeholder = '',
      autoComplete,
      autoCorrect,
      autoCapitalize,
      spellCheck,
      maxlength,
      minlength,
      required,
      min,
      max,
      pattern,
      name,
      value,
      defaultValue,
      testId,
      canBeCleared,
      showAlertOnError,
      iconLeft,
      keepErrorLabelArea = true,
      size = 'regular',
      textCentered,
      onIconClick,
      onChange,
      onBlur,
      onDragStart,
      onFocus,
      onDrop,
    },
    ref
  ) => {
    const [passwordMode, setPasswordMode] = useState(PasswordMode.none);
    const handlePasswordModeChange = (ev: React.SyntheticEvent) => {
      setPasswordMode(
        passwordMode === PasswordMode.show
          ? PasswordMode.hide
          : PasswordMode.show
      );
      if (onIconClick) {
        onIconClick(ev);
      }
    };

    const renderGroupedInput = () => {
      const otherProps = {
        id,
        disabled,
        className,
        placeholder,
        defaultValue,
        error,
        autoComplete,
        autoCorrect,
        autoCapitalize,
        spellCheck,
        maxlength,
        minlength,
        required,
        min,
        max,
        pattern,
        iconLeft,
        name,
        onBlur,
        onDragStart,
        onDrop,
        onFocus,
        testId,
        ref,
      };
      if (type === 'password') {
        return (
          <GroupedInput
            type={passwordMode === PasswordMode.show ? 'text' : 'password'}
            iconConfiguration={{
              showIconWhen: InputShowIconWhen.always,
              icon: (
                <Icon
                  type={
                    passwordMode === PasswordMode.show
                      ? IconType.eyeSlash
                      : IconType.eye
                  }
                />
              ),
            }}
            onIconClick={handlePasswordModeChange}
            {...otherProps}
            onChange={onChange}
          />
        );
      }
      if (showAlertOnError && error) {
        return (
          <GroupedInput
            type={type}
            iconConfiguration={{
              showIconWhen: InputShowIconWhen.onError,
              icon: (
                <Icon
                  family={IconFamily.regular}
                  type={IconType.exclamationCircle}
                />
              ),
            }}
            value={value}
            onIconClick={onIconClick}
            onChange={onChange}
            {...otherProps}
          />
        );
      }

      return (
        <GroupedInput
          type={type}
          iconConfiguration={
            canBeCleared
              ? {
                  showIconWhen: InputShowIconWhen.edit,
                  icon: (
                    <Icon family={IconFamily.regular} type={IconType.times} />
                  ),
                }
              : undefined
          }
          value={value}
          onIconClick={onIconClick}
          onChange={onChange}
          {...otherProps}
        />
      );
    };

    return (
      <div
        className={classNames(
          'rck-text-field',
          block ? 'rck-text-field--block' : '',
          error ? 'rck-text-field--error' : '',
          disabled ? 'rck-text-field--disabled' : '',
          !keepErrorLabelArea ? 'rck-text-field--no-area-for-error' : '',
          size ? `rck-text-field--size-${size}` : '',
          textCentered ? 'rck-text-field--text-centered' : '',
          className
        )}
        data-testid={getTestId(TextFieldTest.prefix, testId)}
      >
        {label ? (
          <label htmlFor={name} className="rck-text-field__label">
            {label}
          </label>
        ) : null}
        {renderGroupedInput()}
        {supportTextConfiguration ? (
          <InfoFieldLabel
            allowedLines={supportTextConfiguration.allowedLines}
            testId={name}
            htmlFor={name}
            mode={error ? 'error' : 'info'}
          >
            {supportTextConfiguration.text}
          </InfoFieldLabel>
        ) : null}
      </div>
    );
  }
);

TextField.displayName = 'TextField';

export default TextField;
