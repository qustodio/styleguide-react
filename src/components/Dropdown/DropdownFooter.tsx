import React, { SyntheticEvent } from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export enum DropdownFooterTest {
  prefix = 'DropdownFooter',
}
export interface DropdownFooterProps extends BaseComponentProps {
  onClick: (ev: SyntheticEvent) => void;
}

const DropdownFooter: React.FunctionComponent<DropdownFooterProps> = ({
  children,
  onClick,
  className = '',
  testId,
}) => (
  <ul role="listbox" className={classNames('rck-dropdown__footer', className)}>
    <li
      onClick={onClick}
      role="option"
      onKeyDown={onClick}
      aria-selected={false}
      data-testid={getTestId(DropdownFooterTest.prefix, testId)}
    >
      {children}
    </li>
  </ul>
);

DropdownFooter.displayName = 'DropdownFooter';

export default DropdownFooter;
