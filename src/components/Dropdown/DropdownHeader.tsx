import React from 'react';
import { getTestId, classNames } from '../../helpers/index';
import { BaseComponentProps } from '../../common/types';

export enum DropdownHeaderTest {
  prefix = 'DropdownHeader',
}

const DropdownHeader: React.FunctionComponent<BaseComponentProps> = ({
  children,
  testId,
  className = '',
}) => (
  <li
    className={classNames('rck-dropdown__list__header', className)}
    data-testid={getTestId(DropdownHeaderTest.prefix, testId)}
  >
    {children}
  </li>
);

DropdownHeader.displayName = 'DropdownHeader';

export default DropdownHeader;
