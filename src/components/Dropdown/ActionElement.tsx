import React, { SyntheticEvent } from 'react';
import Icon, { IconType, IconSize } from '../Icons';
import TextField, { TextFieldProps } from '../TextField/TextField';
import ActionInput, {
  ActionInputIconPosition,
  ActionInputProps,
} from '../ActionInput/ActionInput';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps, DisabledProps } from '../../common/types';

export interface DropdownActionElementProps
  extends BaseComponentProps,
    DisabledProps {
  text?: string;
  icon?: JSX.Element;
  iconRight?: JSX.Element;
  onClick?: (ev: SyntheticEvent) => void;
}

export enum DropdownActionElementTest {
  prefix = 'DropdownActionElement',
}

const DropdownActionElement: React.FunctionComponent<DropdownActionElementProps> = ({
  text,
  icon,
  iconRight,
  className = '',
  disabled,
  testId,
  onClick,
}) => (
  <div
    className={classNames(
      'rck-dropdown-action-name',
      disabled ? 'rck-dropdown-action-name--disabled' : '',
      className
    )}
    onClick={onClick}
    onKeyDown={onClick}
    role="button"
    tabIndex={0}
    data-testid={getTestId(DropdownActionElementTest.prefix, testId)}
  >
    {icon && <span className="rck-dropdown-action-name__icon">{icon}</span>}
    <span className="rck-dropdown-action-name__text">{text}</span>
    {iconRight && (
      <span className="rck-dropdown-action-name__icon rck-dropdown-action-name__icon--right">
        {iconRight}
      </span>
    )}
  </div>
);

DropdownActionElement.displayName = 'ActionElement';

export const createDefaultActionElement = (
  props: DropdownActionElementProps
): JSX.Element => (
  <DropdownActionElement
    {...props}
    iconRight={<Icon type={IconType.angleDown} size={IconSize.x2} />}
  />
);

export const createDefaultActionTextFieldElement = (
  props: TextFieldProps
): JSX.Element => (
  <TextField
    {...props}
    type="text"
    iconLeft={<Icon type={IconType.search} />}
    block
    autoComplete="off"
    keepErrorLabelArea={false}
  />
);

export const createDefaultActionInputElement = (
  props: ActionInputProps
): JSX.Element => (
  <ActionInput
    icon={<Icon type={IconType.angleDown} size={IconSize.x2} />}
    iconPosition={ActionInputIconPosition.right}
    {...props}
    block
    onKeyDown={(ev: SyntheticEvent) => {
      ev.preventDefault();
    }}
  />
);

export default DropdownActionElement;
