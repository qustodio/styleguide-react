import ReactDOM from 'react-dom';
import React, { useRef, useEffect, useState, SyntheticEvent } from 'react';
import { RemoveScroll } from 'react-remove-scroll';
import { classNames, getTestId } from '../../helpers';
import useDetectOutsideClick from '../../hooks/useDetectsOutsideClick';
import usePortal from '../../hooks/usePortal';
import DropdownOption, {
  DropdownOptionProps,
  DropdownOptionValue,
} from './DropdownOption';
import DropdownActionElement from './ActionElement';
import useFloatingPlacement, {
  FloatingMenuPlacement,
} from '../../hooks/useFloatingPlacement';
import useAttachedDomPosition from '../../hooks/useAttachedDomPosition';
import { DropdownAlignment, DropdownElementOrientation, Keys } from './types';
import DropdownFooter from './DropdownFooter';
import DropdownHeader from './DropdownHeader';
import { BaseComponentProps, DisabledProps } from '../../common/types';

interface ActiveOptionState {
  ndx: number;
  reason: 'unset' | 'cursor' | Keys;
}

export interface DropdownProps extends BaseComponentProps, DisabledProps {
  actionElement: JSX.Element;
  portalId?: string;
  useSinglePortalHook?: boolean;
  menuPlacement?: FloatingMenuPlacement;
  fixedMenuPlacement?: boolean;
  attachedMenuOriention?: DropdownElementOrientation;
  showSelectedOption?: boolean;
  className?: string;
  classNameList?: string;
  asModal?: boolean;
  header?: JSX.Element;
  footer?: JSX.Element;
  maxHeight?: number;
  maxWidth: number;
  minWidth?: number;
  showScrollbars?: boolean;
  alignment?: DropdownAlignment;
  id?: string;
  active?: boolean;
  enableRemoveScroll?: boolean;
  onChange?: (value: DropdownOptionValue) => void;
  onActiveChange?: (isActive: boolean) => void;
  onClose?: () => void;
  onOpen?: () => void;
}

export enum DropdownTest {
  prefix = 'Dropdown',
  container = 'Dropdown__container',
}

const Dropdown: React.FunctionComponent<DropdownProps> = ({
  actionElement,
  portalId = 'portal-dropdown',
  useSinglePortalHook,
  menuPlacement = FloatingMenuPlacement.bottom,
  fixedMenuPlacement = false,
  attachedMenuOriention = DropdownElementOrientation.left,
  showSelectedOption = true,
  id,
  active,
  className = '',
  classNameList = '',
  disabled,
  asModal,
  header,
  footer,
  testId,
  maxHeight,
  maxWidth,
  minWidth,
  showScrollbars = true,
  alignment,
  enableRemoveScroll = true,
  onChange,
  onActiveChange,
  onClose,
  onOpen,
  children,
}) => {
  const dropdownList = React.Children.toArray(children) as React.ReactElement<
    DropdownOptionProps
  >[];
  const dropdownRef = useRef(document.createElement('ul'));
  const actionElementRef = useRef(document.createElement('div'));
  const activeOptionRef = useRef(document.createElement('li'));
  const containerRef = useRef(document.createElement('div'));

  const target = usePortal(portalId, useSinglePortalHook);
  const [isActive, setIsActive, prevIsActive] = useDetectOutsideClick(
    [actionElementRef, containerRef],
    false
  );
  const [placement] = useFloatingPlacement(
    dropdownRef,
    menuPlacement,
    isActive,
    fixedMenuPlacement
  );
  const [activeOption, setActiveOption] = useState<ActiveOptionState>({
    ndx: 0,
    reason: 'unset',
  });
  const [isScrolling, setIsScrolling] = useState(false);
  const [
    actionElementDOMPosition,
    updateActionElementDOMPosition,
  ] = useAttachedDomPosition(
    actionElementRef,
    dropdownRef,
    placement,
    attachedMenuOriention
  );

  const findIndexById = (value: DropdownOptionValue) =>
    dropdownList.findIndex(child => child.props.value === value);

  const listStyles = {
    overflow: 'hidden',
    overflowY: 'auto',
    maxHeight,
    maxWidth,
    minWidth,
  } as React.CSSProperties;

  const selected = dropdownList.length
    ? dropdownList.find(child => child.props.selected)?.props.value
    : undefined;

  const isActiveElementVisible = () =>
    activeOptionRef.current.offsetTop > dropdownRef.current.scrollTop &&
    activeOptionRef.current.offsetTop <
      dropdownRef.current.scrollTop + dropdownRef.current.offsetHeight;

  const startScrolling = () => {
    setIsScrolling(true);
    // Do not trigger onMouseEnter handler while is scrolling.
    setTimeout(() => {
      setIsScrolling(false);
    }, 200);
  };

  const handleScroll = () => {
    if (!isActive || !activeOptionRef.current || !dropdownRef.current) return;

    const activeOptionHeight = activeOptionRef.current.offsetHeight;

    if (activeOption.reason === Keys.down && !isActiveElementVisible()) {
      startScrolling();
      dropdownRef.current.scrollTop += activeOptionHeight;
    } else if (activeOption.reason === Keys.up && !isActiveElementVisible()) {
      startScrolling();
      dropdownRef.current.scrollTop -= activeOptionHeight;
    } else if (activeOption.reason === 'unset' && !isActiveElementVisible()) {
      // when there is a selected option by default
      startScrolling();
      // UX: having overflowed dropdown, we remove slight scroll from the active
      // option to indicate that there are more options above the active option.
      // This will show half of the previous option from active.
      const padScroll = activeOptionHeight / 2;
      dropdownRef.current.scrollTop +=
        activeOption.ndx * activeOptionHeight - padScroll;
    }
  };

  const handleMouseEnterOption = (
    ev: SyntheticEvent,
    value: DropdownOptionValue
  ) => {
    if (!isActive || isScrolling) return;
    setActiveOption({ ndx: findIndexById(value), reason: 'cursor' });
  };

  const handleChange = (value: DropdownOptionValue) => {
    if (!dropdownList || disabled) {
      return;
    }

    setIsActive(false);

    if (onChange) {
      onChange(value);
    }
  };

  const handleKeydown = (ev: KeyboardEvent) => {
    if (!isActive) return;
    if (ev.key === Keys.down && activeOption.ndx < dropdownList.length - 1) {
      setActiveOption({ ndx: activeOption.ndx + 1, reason: ev.key });
    }

    if (ev.key === Keys.up && activeOption.ndx > 0) {
      setActiveOption({ ndx: activeOption.ndx - 1, reason: ev.key });
    }

    if (ev.key === Keys.enter) {
      const dropdownOption = dropdownList[activeOption.ndx];
      if (dropdownOption && !dropdownOption.props.disabled) {
        handleChange(dropdownOption.props.value);
      }
    }

    if (ev.ctrlKey) {
      const dropdownOption = dropdownList.find(
        child => child.props.assignKey === ev.key
      );
      if (dropdownOption && !dropdownOption.props.disabled) {
        handleChange(dropdownOption.props.value);
      }
    }
  };

  const handleActionElementClick = () => {
    if (disabled) {
      return;
    }

    setIsActive(!isActive);
    if (!isActive) {
      setActiveOption({ ndx: 0, reason: 'unset' });
    }
  };

  useEffect(() => {
    handleScroll();
  }, [activeOption]);

  useEffect(() => {
    if (prevIsActive !== isActive) {
      if (onActiveChange) {
        onActiveChange(isActive);
      }
      if (!isActive && onClose) {
        onClose();
      }
      if (isActive && onOpen) {
        onOpen();
      }
    }
  }, [isActive, menuPlacement]);

  useEffect(() => {
    if (disabled) {
      setIsActive(false);
    }
  }, [disabled]);

  useEffect(() => {
    if (active === true && !isActive) {
      setIsActive(true);
    }
  }, [active]);

  // If there is a default selected option, when opening
  // the dropdown we set it to active and scroll to it.
  useEffect(() => {
    if (isActive && selected) {
      setActiveOption({ reason: 'unset', ndx: findIndexById(selected) });
    }
  }, [isActive, selected]);

  useEffect(() => {
    document.addEventListener('keydown', handleKeydown);
    return () => {
      document.removeEventListener('keydown', handleKeydown);
    };
  });

  const renderActionElement = () => {
    const triggerClassNames = 'rck-dropdown__action-name-container';

    const sel = dropdownList.find(child => child.props.value === selected);

    if (actionElement) {
      return <div className={triggerClassNames}>{actionElement}</div>;
    }

    return (
      <div className={triggerClassNames}>
        <DropdownActionElement
          text={sel?.props.text}
          icon={sel?.props.icon}
          disabled={disabled}
        />
      </div>
    );
  };

  const getListStyles = () => {
    const { right, left } = actionElementRef.current.getBoundingClientRect();

    return {
      ...listStyles,
      minWidth: asModal ? '' : `${minWidth || right - left}px`,
      maxWidth: asModal ? '' : `${maxWidth || right - left}px`,
    };
  };

  const getList = () =>
    !disabled && isActive && dropdownList.length ? (
      <div className={classNames(asModal ? 'rck-dropdown__modal-overlay' : '')}>
        <div
          className={classNames(
            'rck-dropdown-list-container',
            asModal ? 'rck-dropdown-list-container--modal' : '',
            classNameList
          )}
          style={portalId && !asModal ? actionElementDOMPosition : {}}
          data-testid={getTestId(DropdownTest.prefix, testId)}
          ref={containerRef}
        >
          <ul
            ref={dropdownRef}
            className={classNames(
              'rck-dropdown__list',
              `rck-dropdown__list--${placement}`,
              `rck-dropdown__list--attached`,
              `rck-dropdown__list--${
                asModal
                  ? DropdownElementOrientation.notSet
                  : attachedMenuOriention
              }`,
              !showScrollbars ? 'rck-dropdown__list--no-scrollbar' : ''
            )}
            role="listbox"
            style={getListStyles()}
            id={id}
          >
            {header && (
              <DropdownHeader testId={testId}>{header}</DropdownHeader>
            )}
            {React.Children.map(children, (child, ndx) => {
              const dropdownOption = child as React.ReactElement<
                DropdownOptionProps
              >;
              if (dropdownOption === null) {
                return null;
              }

              if (dropdownOption.type !== DropdownOption) {
                throw new Error('Dropdown must contain DropdownOptions');
              }

              if (dropdownOption.props.value === selected && !actionElement) {
                return null;
              }

              return React.cloneElement(dropdownOption, {
                onClick: (
                  value: DropdownOptionValue,
                  event?: SyntheticEvent
                ) => {
                  if (dropdownOption.props.onClick) {
                    dropdownOption.props.onClick(value, event);
                  }
                  handleChange(value);
                },
                selected: dropdownOption.props.value === selected,
                onMouseEnter: handleMouseEnterOption,
                active: ndx === activeOption.ndx,
                ref: ndx === activeOption.ndx ? activeOptionRef : undefined,
                showAsSelected: showSelectedOption,
                testId,
              });
            })}
          </ul>

          {footer && (
            <DropdownFooter onClick={handleActionElementClick} testId={testId}>
              {footer}
            </DropdownFooter>
          )}
        </div>
      </div>
    ) : null;

  const getListWithRemoveScroll = () => {
    const list = getList();
    return list ? <RemoveScroll>{list}</RemoveScroll> : null;
  };

  return (
    <div
      className={classNames(
        'rck-dropdown',
        alignment ? `rck-dropdown--${alignment}` : '',
        className
      )}
      data-testid={getTestId(
        DropdownTest.prefix,
        testId,
        DropdownTest.container
      )}
    >
      {React.cloneElement(renderActionElement(), {
        onClick: () => {
          updateActionElementDOMPosition();
          handleActionElementClick();
        },
        ref: actionElementRef,
      })}
      {portalId &&
        target &&
        ReactDOM.createPortal(
          enableRemoveScroll ? getListWithRemoveScroll() : getList(),
          target
        )}
    </div>
  );
};

Dropdown.displayName = 'Dropdown';

export default Dropdown;
