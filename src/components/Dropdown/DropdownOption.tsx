import React, { SyntheticEvent } from 'react';
import { classNames, getTestId } from '../../helpers';
import { BaseComponentProps, DisabledProps } from '../../common/types';

export type DropdownOptionValue = string | number | boolean;

export interface DropdownOptionProps extends BaseComponentProps, DisabledProps {
  value: DropdownOptionValue;
  text?: string;
  icon?: JSX.Element;
  selected?: boolean;
  active?: boolean;
  assignKey?: string;
  showAssignKey?: boolean;
  ref?: React.MutableRefObject<HTMLLIElement>;
  showAsSelected?: boolean;
  onClick?: (value: DropdownOptionValue, ev?: SyntheticEvent) => void;
  onMouseEnter?: (ev: SyntheticEvent, value: DropdownOptionValue) => void;
  onMouseLeave?: (ev: SyntheticEvent, value: DropdownOptionValue) => void;
}

export enum DropdownOptionTest {
  prefix = 'DropdownOption',
}

const DropdownOption = React.forwardRef<HTMLLIElement, DropdownOptionProps>(
  (
    {
      value,
      text,
      icon,
      className = '',
      showAsSelected = true,
      selected,
      disabled,
      active,
      assignKey,
      showAssignKey,
      onClick,
      onMouseEnter,
      onMouseLeave,
      testId,
      children,
    },
    ref
  ) => {
    const handleClick = (event: SyntheticEvent) => {
      if (!disabled && onClick) {
        onClick(value, event);
      }
    };

    const handleMouseEnter = (ev: SyntheticEvent) => {
      if (onMouseEnter) {
        onMouseEnter(ev, value);
      }
    };

    const handleMouseLeave = (ev: SyntheticEvent) => {
      if (onMouseLeave) {
        onMouseLeave(ev, value);
      }
    };

    const renderText = () => {
      if (active && !disabled) {
        return <strong>{text}</strong>;
      }

      return text;
    };

    return (
      <li
        className={classNames(
          'rck-dropdown-option',
          active ? 'rck-dropdown-option--active' : '',
          showAsSelected && selected ? 'rck-dropdown-option--selected' : '',
          disabled ? 'rck-dropdown-option--disabled' : '',
          className
        )}
        onClick={handleClick}
        onKeyDown={handleClick}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        role="option"
        aria-selected={selected}
        ref={ref}
        data-testid={getTestId(
          DropdownOptionTest.prefix,
          testId,
          value.toString()
        )}
      >
        {!children && icon && (
          <span className="rck-dropdown-option__icon">{icon}</span>
        )}
        {!children && (
          <span className="rck-dropdown-option__text">{renderText()}</span>
        )}
        {!children && showAssignKey && assignKey && (
          <span className="rck-dropdown-option__key-assign">
            [ctrl+{assignKey}]
          </span>
        )}
        {children}
      </li>
    );
  }
);

DropdownOption.displayName = 'DropdownOption';

export default DropdownOption;
