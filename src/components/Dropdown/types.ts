import { DropdownOptionValue } from './DropdownOption';
import { FloatingMenuPlacement } from '../../hooks/useFloatingPlacement';

export enum DropdownElementOrientation {
  notSet = 'not-set',
  left = 'left',
  right = 'right',
  center = 'center',
}

export enum DropdownAlignment {
  top = 'top',
  center = 'center',
  bottom = 'bottom',
}

export { FloatingMenuPlacement as DropdownMenuPlacement };

export { DropdownOptionValue };

export enum Keys {
  up = 'ArrowUp',
  down = 'ArrowDown',
  enter = 'Enter',
}
