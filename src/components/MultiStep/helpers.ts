import {
  MultiStepState,
  Step,
  StepName,
  Steps,
  Transition,
} from './MultiStep.types';

const size = (steps: Steps) => steps.length;

const dec = (n: number) => n - 1;

const inc = (n: number) => n + 1;

const isLastPosition = (stepsSize: number, pos: number) =>
  dec(stepsSize) === pos;

const isFirstPosition = (pos: number) => pos === 0;

const nextPosition = (steps: Steps, pos: number) =>
  isLastPosition(size(steps), pos) ? pos : inc(pos);

const prevPosition = (pos: number) => (isFirstPosition(pos) ? pos : dec(pos));

const unsafeNextPositionByName = (name: StepName, steps: Steps): number =>
  steps.findIndex(step => step.name === name);

export const unsafeInitialPosition = (
  steps: Steps,
  name?: StepName | null
): number => (name ? unsafeNextPositionByName(name, steps) : 0);

const nextTransition = (currentPos: number, nextPos: number): Transition => {
  if (currentPos === nextPos) return 'none';
  if (currentPos < nextPos) return 'next';
  return 'prev';
};

export const next = (steps: Steps) => ({
  position,
}: MultiStepState): MultiStepState => {
  const newPosition = nextPosition(steps, position);
  return {
    transition: nextTransition(position, newPosition),
    position: newPosition,
  };
};

export const prev = ({ position }: MultiStepState): MultiStepState => {
  const newPosition = prevPosition(position);
  return {
    transition: nextTransition(position, newPosition),
    position: newPosition,
  };
};

export const go = (name: StepName, steps: Steps) => ({
  position,
}: MultiStepState): MultiStepState => {
  const newPosition = unsafeNextPositionByName(name, steps);
  return {
    position: newPosition,
    transition: nextTransition(position, newPosition),
  };
};

export const safeAsNone = (selectedStep: Step): Step =>
  selectedStep ?? { step: () => null, name: '' };
