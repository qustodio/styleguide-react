import { BaseComponentProps } from '../../common/types';

export enum MultiStepTest {
  prefix = 'MultiStep',
}

export type Transition = 'prev' | 'next' | 'none';

export type StepName = string;

export interface Step {
  name: StepName;
  step: (stepNavigation: {
    next: () => void;
    prev: () => void;
    go: (name: StepName) => void;
    transition: Transition;
  }) => React.ReactElement;
}

export type Steps = [Step, Step, ...Array<Step>];

export interface MultiStepProps extends BaseComponentProps {
  activeStep?: StepName | null;
  steps: Steps;
}

export interface MultiStepState {
  position: number;
  transition: Transition;
}
