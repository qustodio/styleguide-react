// components
export { default } from './Text';

// types
export { TextColor } from './types/TextColor.types';
export { TextProps } from './types/TextProps.types';
export { TextType } from './types/TextType.types';

// enums
export { default as TextTest } from './types/TextTest.types';
