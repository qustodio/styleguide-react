import * as React from 'react';
import { classNames, getTestId } from '../../helpers/index';
import TextTest from './types/TextTest.types';
import { TextProps } from './types/TextProps.types';
import { textVariantElementMappings } from './constants';

const Text = ({
  variant = 'body-1-regular',
  color,
  align,
  renderAs,
  noWrap,
  marginTop,
  marginBottom,
  parentTestId,
  testId,
  children,
  className = '',
}: TextProps) => {
  const commonProps = {
    className: classNames(
      'rck-text',
      `rck-text--type-${variant}`,
      color ? `rck-text--color-${color}` : '',
      align ? `rck-text--align-${align}` : '',
      noWrap ? `rck-text--no-wrap` : '',
      marginTop ? `rck-text--margin-top-${marginTop}` : '',
      marginBottom ? `rck-text--margin-bottom-${marginBottom}` : '',
      className
    ),
    'data-testid': parentTestId ?? getTestId(TextTest.prefix, testId),
  };

  if (renderAs) {
    if (typeof renderAs === 'string') {
      return React.createElement(renderAs, commonProps, children);
    }

    if (React.isValidElement(renderAs)) {
      const combinedClasses = classNames(
        commonProps.className,
        renderAs.props?.className ?? ''
      );
      return React.cloneElement(
        renderAs,
        {
          ...renderAs.props,
          className: combinedClasses,
          'data-testid':
            renderAs.props.testId ?? getTestId(TextTest.prefix, testId),
        },
        children
      );
    }
  }

  return React.createElement(
    textVariantElementMappings[variant],
    commonProps,
    children
  );
};

Text.displayName = 'Text';

export default Text;
