import {
  BaseComponentProps,
  TestableNestedCompomentProps,
} from '../../../common/types';
import { LayoutSpacing } from '../../Layout/types';
import { TextColor } from './TextColor.types';
import { TextType } from './TextType.types';

export type TextProps = BaseComponentProps &
  TestableNestedCompomentProps & {
    /**
     * The text variant to render. This will determine the size and
     * style of the text (weight, italic, etc.).
     *
     * @example 'heading-1-semibold'
     */
    variant: TextType;

    /**
     * color selected from the available color palette for typography.
     */
    color?: TextColor;

    align?: 'center' | 'left' | 'right';

    /**
     * The HTML element or React component to render the Text as. If a
     * string is provided, it will be rendered as an HTML element.
     */
    renderAs?: keyof React.ReactHTML | React.ReactNode;

    marginTop?: LayoutSpacing;
    marginBottom?: LayoutSpacing;

    /**
     * If `true`, the text will not wrap, but instead will truncate with a text overflow ellipsis.
     * Note that text overflow can only happen with block or inline-block level elements (the element needs to have a width in order to overflow).
     */
    noWrap?: boolean;
  };
