import { textVariants } from '../constants';

export type TextType = typeof textVariants[number];
