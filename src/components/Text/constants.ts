/**
 * Fixed typography variants
 *
 * These are the base typography types that are used in the design system.
 */
export const textBaseVariants = [
  'hero-1-semibold',
  'hero-1-regular',
  'hero-2-semibold',
  'hero-2-regular',
  'headline-1-semibold',
  'headline-1-regular',
  'headline-2-semibold',
  'headline-2-regular',
  'title-1-semibold',
  'title-1-regular',
  'title-2-semibold',
  'title-2-regular',
  'title-3-semibold',
  'title-3-regular',
  'body-1-semibold',
  'body-1-regular',
  'body-2-semibold',
  'body-2-regular',
  'caption-1-semibold',
  'caption-1-regular',
  'caption-1-italic',
  'caption-2-semibold',
  'caption-2-regular',
  'caption-2-italic',
] as const;

/**
 * These typographies change their size based on the viewport width.
 */
export const textResponsiveVariants = [
  'headline-1-semibold-responsive',
  'headline-1-regular-responsive',
  'headline-2-semibold-responsive',
  'headline-2-regular-responsive',
  'title-1-semibold-responsive',
  'title-1-regular-responsive',
  'title-2-semibold-responsive',
  'title-2-regular-responsive',
  'title-3-semibold-responsive',
  'title-3-regular-responsive',
] as const;

export const textVariants = [
  ...textBaseVariants,
  ...textResponsiveVariants,
] as const;

export const textColors = [
  'grey-100',
  'grey-200',
  'grey-300',
  'grey-400',
  'grey-500',
  'error',
  'warning',
  'success',
  'white',
  'brand',
] as const;

/**
 * It's recommended to use the `renderAs` prop to avoid coupling styles with
 * specific HTML elements.
 * By default, typography types are mapped to corresponding HTML elements, but
 * using `renderAs` allows for more flexibility in defining the heading level
 * hierarchy.
 */
export const textVariantElementMappings: Record<
  typeof textVariants[number],
  keyof React.ReactHTML
> = {
  'hero-1-semibold': 'h1',
  'hero-1-regular': 'h1',
  'hero-2-semibold': 'h2',
  'hero-2-regular': 'h2',
  'headline-1-semibold': 'h1',
  'headline-1-regular': 'h1',
  'headline-2-semibold': 'h2',
  'headline-2-regular': 'h2',
  'title-1-semibold': 'h3',
  'title-1-regular': 'h3',
  'title-2-semibold': 'h4',
  'title-2-regular': 'h4',
  'title-3-semibold': 'h5',
  'title-3-regular': 'h5',
  'body-1-semibold': 'div',
  'body-1-regular': 'div',
  'body-2-semibold': 'div',
  'body-2-regular': 'div',
  'caption-1-semibold': 'span',
  'caption-1-regular': 'span',
  'caption-1-italic': 'span',
  'caption-2-semibold': 'span',
  'caption-2-regular': 'span',
  'caption-2-italic': 'span',
  'headline-1-semibold-responsive': 'h1',
  'headline-1-regular-responsive': 'h1',
  'headline-2-semibold-responsive': 'h2',
  'headline-2-regular-responsive': 'h2',
  'title-1-semibold-responsive': 'h3',
  'title-1-regular-responsive': 'h3',
  'title-2-semibold-responsive': 'h4',
  'title-2-regular-responsive': 'h4',
  'title-3-semibold-responsive': 'h5',
  'title-3-regular-responsive': 'h5',
};
