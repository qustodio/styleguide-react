/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
// TODO: review that

import React from 'react';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
import {
  classNames,
  getTestId,
  getTestIdWithParentTestId,
} from '../../helpers';
import Icon, { IconType, IconFamily } from '../Icons';
import {
  BaseComponentProps,
  TestableComponentProps,
  TestableNestedCompomentProps,
} from '../../common/types';
import Badge, { BadgeProps, BadgeType } from '../Badge/Badge';
import ScrollableContent from '../ScrollableContent/ScrollableContent';
import { AffiliateBadgeProps } from '../Badge/AffiliateBadge';
import { AccountInfoHeaderProps } from '../AccountInfoHeader/AccountInfoHeader';
import { LicenseInfoContainerProps } from '../LicenseInfoContainer/LicenseInfoContainer';

export interface SideNavigationMenuProps
  extends BaseComponentProps,
    TestableNestedCompomentProps {
  userName: string;
  expand: boolean;
  accountInfo: React.ReactElement<AccountInfoHeaderProps>;
  licenseInfo?:
    | React.ReactElement<LicenseInfoContainerProps>
    | React.ReactElement<BadgeProps>;
  affiliateBadge?: React.ReactElement<AffiliateBadgeProps>;
  licenseBadge?: JSX.Element;
  items: SideNavigationMenuItemType[];
  showGift?: boolean;
  onClickGift: () => void;
}

export interface MenuItemExtensionProps
  extends TestableComponentProps,
    TestableNestedCompomentProps {
  title: string;
  showBadge: boolean | undefined;
  showAccent: boolean | undefined;
  badge?: JSX.Element;
}

export interface SideNavigationMenuAnchorProps
  extends TestableComponentProps,
    TestableNestedCompomentProps {
  item: SideNavigationMenuItemType;
}

export interface SideNavigationMenuItemProps
  extends TestableComponentProps,
    TestableNestedCompomentProps {
  item: SideNavigationMenuItemType;
}

export type SideNavigationMenuItemType = {
  key: string;
  title: string;
  icon: JSX.Element;
  useAsLink?: React.FunctionComponent<AnchorPropTypes>;
  isActive: boolean;
  onClick: () => void;
  linkTo?: string;
  shouldShow?: boolean;
  showBadge?: boolean;
  showAccent?: boolean;
  badge?: JSX.Element;
};

export enum SideNavigationMenuTest {
  prefix = 'SideNavigationMenu',
  menuItem = 'SideNavigationMenuItem',
  anchor = 'SideNavigationMenuAnchor',
  extension = 'SideNavigationMenuItemExtension',
  menuItemWrapper = 'SideNavigationMenuItem-wrapper',
}

/**
 * Resolves the badge component to render based on the provided props.
 * If a badge element is provided, it takes precedence over the showBadge to maintain retro compatibility.
 */
const ResolveBadge = ({
  showBadge,
  badge,
}: {
  showBadge?: boolean;
  badge?: JSX.Element;
}) => {
  if (badge) {
    return badge;
  }

  if (showBadge) {
    return (
      <Badge
        className="rck-side-navigation-menu__badge"
        type={BadgeType.contrast}
        text="Premium"
        size="small"
      />
    );
  }
  return null;
};

const MenuItemExtension: React.FunctionComponent<MenuItemExtensionProps> = React.memo(
  ({ title, showBadge, showAccent, testId, parentTestId, badge }) => (
    <div
      className="rck-side-navigation-menu__anchor__extension"
      data-testid={getTestId(
        SideNavigationMenuTest.extension,
        getTestIdWithParentTestId(parentTestId, testId)
      )}
    >
      <span className="rck-side-navigation-menu__anchor__text">
        {title}
        {showAccent && (
          <span className="rck-side-navigation-menu__accent">
            <Icon type={IconType.circle} family={IconFamily.solid} square />
          </span>
        )}
      </span>

      <div className="rck-side-navigation-menu__anchor__optional-content">
        <ResolveBadge showBadge={showBadge} badge={badge} />
      </div>
    </div>
  )
);

const SideNavigationMenuAnchor: React.FunctionComponent<SideNavigationMenuAnchorProps> = ({
  item,
  testId,
  parentTestId,
}) => {
  const Anchor = item.useAsLink;
  const { linkTo = '' } = item;
  return Anchor ? (
    <Anchor
      className={classNames(
        'rck-side-navigation-menu__anchor',
        item.isActive ? 'rck-side-navigation-menu__anchor--active' : ''
      )}
      to={linkTo}
      data-testid={getTestId(
        SideNavigationMenuTest.anchor,
        getTestIdWithParentTestId(parentTestId, testId)
      )}
    >
      <div className="rck-side-navigation-menu__anchor__content">
        <div className="rck-side-navigation-menu__anchor__content__activable">
          <div className="rck-side-navigation-menu__anchor__content__activable__layout">
            <div className="rck-side-navigation-menu__anchor__icon">
              {item.icon}
            </div>

            <MenuItemExtension
              title={item.title}
              showBadge={item.showBadge}
              showAccent={item.showAccent}
              testId={testId}
              parentTestId={parentTestId}
              badge={item.badge}
            />
          </div>
        </div>
      </div>
    </Anchor>
  ) : null;
};

const SideNavigationMenuItem: React.FunctionComponent<SideNavigationMenuItemProps> = ({
  item,
  testId,
  parentTestId,
}) => (
  <a
    className={classNames(
      'rck-side-navigation-menu__anchor',
      item.isActive ? 'rck-side-navigation-menu__anchor--active' : ''
    )}
    href={item.linkTo}
    data-testid={getTestId(
      SideNavigationMenuTest.menuItem,
      getTestIdWithParentTestId(parentTestId, testId)
    )}
  >
    <div className="rck-side-navigation-menu__anchor__content">
      <div className="rck-side-navigation-menu__anchor__content__activable">
        <div className="rck-side-navigation-menu__anchor__content__activable__layout">
          <div className="rck-side-navigation-menu__anchor__icon">
            {item.icon}
          </div>

          <MenuItemExtension
            title={item.title}
            showBadge={item.showBadge}
            showAccent={item.showAccent}
            testId={testId}
            parentTestId={parentTestId}
            badge={item.badge}
          />
        </div>
      </div>
    </div>
  </a>
);

const SideNavigationMenu: React.FunctionComponent<SideNavigationMenuProps> = ({
  className = '',
  accountInfo,
  licenseInfo,
  testId,
  expand,
  items,
}) => (
  <div
    className={classNames(
      'rck-side-navigation-menu',
      className,
      expand ? 'rck-side-navigation-menu--expanded' : ''
    )}
    datatest-id={getTestId(SideNavigationMenuTest.prefix, testId)}
  >
    {React.cloneElement(accountInfo, {
      expand,
    })}
    <div className="rck-side-navigation-menu__wrapper">
      <ScrollableContent hideScrollbar>
        <ul className="rck-side-navigation-menu__list">
          {items.map(item => (
            <React.Fragment key={item.key}>
              {item.shouldShow && (
                <li
                  title={item.title}
                  className={classNames('rck-side-navigation-menu__list__item')}
                  onClick={item.onClick}
                  role="navigation"
                  onKeyPress={item.onClick}
                  data-testid={getTestId(
                    SideNavigationMenuTest.prefix,
                    testId,
                    SideNavigationMenuTest.menuItemWrapper,
                    item.key
                  )}
                >
                  {item.useAsLink ? (
                    <SideNavigationMenuAnchor
                      item={item}
                      testId={item.key}
                      parentTestId={testId}
                    />
                  ) : (
                    <SideNavigationMenuItem
                      item={item}
                      testId={item.key}
                      parentTestId={testId}
                    />
                  )}
                </li>
              )}
            </React.Fragment>
          ))}
        </ul>
      </ScrollableContent>
    </div>
    {licenseInfo}
  </div>
);

export default SideNavigationMenu;
