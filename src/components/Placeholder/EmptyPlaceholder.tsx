import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';
import { LayoutContentSpacing } from '../Layout/types';
import FlexLayout from '../Layout/index';

export interface EmptyPlaceholderProps extends BaseComponentProps {
  text: string;
  icon: JSX.Element;
  coloredBackground: boolean;
  centered: boolean;
  fillAvailableSpace?: boolean;
  maxWidth: LayoutContentSpacing;
  smallText?: boolean;
}

export enum EmptyPlaceholderTest {
  prefix = 'EmptyPlaceholder',
}

const EmptyPlaceholder: React.FunctionComponent<EmptyPlaceholderProps> = ({
  text,
  icon,
  coloredBackground,
  fillAvailableSpace,
  maxWidth = '100%',
  centered,
  smallText,
  className = '',
  testId,
}) => (
  <div
    className={classNames(
      'rck-empty-placeholder',
      fillAvailableSpace ? 'rck-empty-placeholder--fill' : '',
      centered ? 'rck-empty-placeholder--center' : '',
      coloredBackground ? 'rck-empty-placeholder--colored-background' : '',
      smallText ? 'rck-empty-placeholder--small-text' : '',
      className
    )}
    data-testid={getTestId(EmptyPlaceholderTest.prefix, testId)}
  >
    <FlexLayout mainaxis="column" maxWidth={maxWidth}>
      {icon && <div className="rck-empty-placeholder__icon">{icon}</div>}
      {text}
    </FlexLayout>
  </div>
);

EmptyPlaceholder.displayName = 'EmptyPlaceholder';

export default React.memo(EmptyPlaceholder);
