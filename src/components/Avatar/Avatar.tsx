import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import AvatarIcon from './AvatarIcon';
import useAriaProps from '../../hooks/useAriaProps';
import TransparentButton from '../Button/TransparentButton';
import {
  AvatarSize,
  AvatarProps,
  AvatarTest,
  AvatarButtonBase,
  AvatarLinkBase,
} from './Avatar.types';
import { ImageLikeComponentProps } from '../../common/types';
import Link from '../Link/Link';

const ImageVariantAvatar = ({
  src,
  srcSet,
  imgSizes,
  alt,
  imgLoading,
  imgReferrerPolicy,
}: ImageLikeComponentProps) => (
  <img
    src={src}
    alt={alt}
    srcSet={srcSet}
    sizes={imgSizes}
    loading={imgLoading}
    // React 16 does not accept all the `referrerPolicy` values,
    // we will shove them doen its throat anyways.
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    referrerPolicy={imgReferrerPolicy as any}
  />
);

const AvatarLink = ({ className, ...props }: AvatarLinkBase) => (
  <Link {...props} className={classNames('rck-avatar--link-like', className)} />
);
const AvatarButton = (props: AvatarButtonBase) => (
  <TransparentButton {...props} />
);

const rootComponent = {
  link: AvatarLink,
  button: AvatarButton,
} as const;

const Avatar = React.forwardRef((props: AvatarProps, ref) => {
  const {
    id,
    role,
    tabIndex,
    testId,
    className,
    size = AvatarSize.medium,
    emptyBackground = false,
    isFreeForm = false,
    src,
    icon,
    iconColor = 'brand',
    iconBackground = 'white',
    children,
    renderAs,
    ...rest
  } = props;

  const Component = renderAs ? rootComponent[renderAs] : 'div';

  const isDefaultRootEl = renderAs == null;
  const ariaProps = useAriaProps(isDefaultRootEl ? rest : {});
  const restProps = isDefaultRootEl ? {} : rest;

  const isTextVariant = ['number', 'string'].includes(typeof children);
  const textFirstLetter = isTextVariant
    ? (children as string).charAt(0)
    : undefined;

  const computedTestId = getTestId(AvatarTest.prefix, testId);

  return (
    <Component
      {...restProps}
      {...ariaProps}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ref={ref as any}
      id={id}
      role={role}
      tabIndex={tabIndex}
      className={classNames(
        'rck-avatar',
        `rck-avatar--size-${size}`,
        isTextVariant ? `rck-avatar--variant-text` : '',
        className
      )}
      testId={computedTestId}
      data-testid={computedTestId}
    >
      <span
        className={classNames(
          'rck-avatar__content',
          emptyBackground ? 'rck-avatar__content--empty' : '',
          isFreeForm ? 'rck-avatar__content--free-form' : ''
        )}
      >
        {src ? (
          <ImageVariantAvatar src={src} {...rest} />
        ) : (
          textFirstLetter ?? children
        )}
      </span>
      <AvatarIcon
        icon={icon}
        iconColor={iconColor}
        iconBackground={iconBackground}
      />
    </Component>
  );
});

Avatar.displayName = 'Avatar';

export default Avatar;
