import * as React from 'react';
import { classNames, isEnumMember } from '../../helpers';
import { AvatarProps, DefaultAvatarIconsSet } from './Avatar.types';
import Icon, { IconFamily, IconType } from '../Icons';

const defaultIconsMap: Record<
  DefaultAvatarIconsSet,
  { icon: string | null; solid?: boolean }
> = {
  pencil: { icon: IconType.pen, solid: true },
  camera: { icon: IconType.camera, solid: true },
  status_tampered: { icon: IconType.exclamationTriangle, solid: true },
  status_paused: { icon: IconType.pause, solid: true },
  status_online: { icon: null },
};

const RendererIcon = ({
  defaultIcon,
  iconColor,
  iconBackground,
  children,
}: Pick<AvatarProps, 'iconBackground' | 'iconColor'> & {
  children: React.ReactNode | string;
  defaultIcon?: DefaultAvatarIconsSet;
}) => (
  <span
    className={classNames(
      'rck-avatar__icon',
      iconColor ? `rck-avatar__icon--color-${iconColor}` : '',
      iconBackground ? `rck-avatar__icon--bgColor-${iconBackground}` : '',
      defaultIcon ? `rck-avatar__icon--default-icon-${defaultIcon}` : ''
    )}
  >
    {children}
  </span>
);

const AvatarIcon = ({
  icon,
  iconColor,
  iconBackground,
}: Pick<AvatarProps, 'icon' | 'iconBackground' | 'iconColor'>) => {
  if (!icon) return null;

  if (React.isValidElement(icon)) {
    return (
      <RendererIcon iconBackground={iconBackground} iconColor={iconColor}>
        {icon}
      </RendererIcon>
    );
  }

  if (isEnumMember(icon, IconType)) {
    return (
      <RendererIcon iconBackground={iconBackground} iconColor={iconColor}>
        <Icon type={icon as IconType} family={IconFamily.regular} />
      </RendererIcon>
    );
  }

  if (typeof icon !== 'string') return null;

  const defaultIconProps = defaultIconsMap[icon] ?? icon;
  return (
    <RendererIcon defaultIcon={icon}>
      <Icon
        type={(defaultIconProps.icon ?? icon) as IconType}
        family={defaultIconProps.solid ? IconFamily.solid : IconFamily.regular}
      />
    </RendererIcon>
  );
};

AvatarIcon.displayName = 'AvatarIcon';

export default AvatarIcon;
