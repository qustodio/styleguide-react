import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export enum ButtonType {
  primary = 'primary',
  secondary = 'secondary',
  plain = 'plain',
}

export enum ButtonSize {
  small = 'small',
  medium = 'medium',
  big = 'big',
}

export enum ButtonColor {
  secondary = 'secondary',
  error = 'error',
  white = 'white',
  wellbeing = 'wellbeing',
}

export enum ButtonIconPosition {
  left = 'left',
  right = 'right',
}

export interface ButtonProps extends BaseComponentProps, DisabledProps {
  label?: string;
  htmlType?: 'button' | 'reset' | 'submit';
  buttonType?: ButtonType;
  color?: ButtonColor;
  loading?: boolean;
  block?: boolean;
  form?: string;
  centered?: boolean;
  iconPosition?: ButtonIconPosition;
  icon?: JSX.Element;
  size?: ButtonSize;
  onClick?: () => void;
}

export enum ButtonTest {
  prefix = 'Button',
}

const Button: React.FunctionComponent<ButtonProps> = ({
  children,
  label,
  htmlType = 'button',
  buttonType = ButtonType.primary,
  color = ButtonColor.secondary,
  loading = false,
  disabled = false,
  block = false,
  centered = true,
  iconPosition = false,
  form,
  icon,
  size = undefined,
  className = '',
  testId,
  onClick = undefined,
}) => {
  const renderRightIcon =
    iconPosition === ButtonIconPosition.right && icon
      ? React.cloneElement(icon, { className: 'rck-btn__icon' })
      : '';
  const renderLeftIcon =
    iconPosition === ButtonIconPosition.left && icon
      ? React.cloneElement(icon, { className: 'rck-btn__icon' })
      : '';
  return (
    <button
      disabled={disabled}
      className={classNames(
        'rck-btn',
        buttonType ? `rck-btn--type-${buttonType}` : '',
        color ? `rck-btn--color-${color}` : '',
        loading ? 'rck-btn--loading' : '',
        block ? 'rck-btn--block' : '',
        size ? `rck-btn--${size}` : '',
        centered ? `rck-btn--centered` : '',
        iconPosition
          ? `rck-btn--with-icon
          rck-btn--with-icon-${iconPosition}`
          : '',
        className
      )}
      type={htmlType}
      form={form}
      onClick={onClick}
      data-testid={getTestId(ButtonTest.prefix, testId)}
    >
      {renderLeftIcon}
      <span className="rck-btn__label">{children || label}</span>
      {renderRightIcon}
    </button>
  );
};

Button.displayName = 'Button';

export default React.memo(Button);
