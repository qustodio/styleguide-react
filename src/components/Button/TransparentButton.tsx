import React, { SyntheticEvent } from 'react';
import { ButtonAriaProps } from '../../common/aria.types';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import useAriaProps from '../../hooks/useAriaProps';

export interface TransparentButtonProps
  extends BaseComponentProps,
    ButtonAriaProps {
  onClick?: (ev: SyntheticEvent) => void;
  fillAvailableSpace?: boolean;
  htmlType?: 'button' | 'reset' | 'submit';
  /** Displays the element as an inline element */
  inline?: boolean;
  /**
   * A boolean attribute specifies that the button should have input focus when the page loads, or when
   * a <dialog> is opened.
   *
   * __Only one element in a document can have this attribute.__
   * */
  autoFocus?: boolean;

  /**
   * This props does not use the native <button> `disabled` prop but instead uses `aria-disabled.`
   * When true, it does not register any keyboard interaction but still allows keyboard focus for a11y.
   * @see https://css-tricks.com/making-disabled-buttons-more-inclusive/
   * */
  disabled?: boolean;
}

export enum TransparentButtonTest {
  prefix = 'TransparentButton',
}

const TransparentButton = React.forwardRef<
  HTMLButtonElement,
  TransparentButtonProps
>((props, ref) => {
  const {
    id,
    htmlType = 'button',
    testId,
    role,
    tabIndex,
    inline,
    className = '',
    disabled,
    onClick,
    children,
    fillAvailableSpace,
    skipTab,
    ...rest
  } = props;

  const ariaProps = useAriaProps(rest);

  return (
    <button
      {...ariaProps}
      ref={ref}
      id={id}
      role={role}
      tabIndex={tabIndex ?? (skipTab ? -1 : undefined)}
      className={classNames(
        className,
        'rck-transparent-button',
        disabled ? 'rck-transparent-button--disabled' : '',
        fillAvailableSpace ? 'rck-transparent-button--fill' : '',
        inline ? 'rck-transparent-button--inline' : ''
      )}
      data-testid={getTestId(TransparentButtonTest.prefix, testId)}
      onClick={!disabled ? onClick : undefined}
      aria-disabled={disabled}
      type={htmlType}
    >
      {children}
    </button>
  );
});

export default React.memo(TransparentButton);
