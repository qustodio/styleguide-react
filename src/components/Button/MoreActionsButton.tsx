import React, { SyntheticEvent } from 'react';
import { DisabledProps, BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconColor, IconSize, IconType } from '../Icons';
import TransparentButton from './TransparentButton';

export interface MoreButtonProps extends BaseComponentProps, DisabledProps {
  onClick: (ev: SyntheticEvent) => void;
  size?: IconSize;
}

export enum MoreActionsButtonTest {
  prefix = 'MoreActionsButton',
}

const MoreActionsButton: React.FunctionComponent<MoreButtonProps> = ({
  onClick,
  disabled,
  size = IconSize.lg,
  className = '',
  testId,
}) => (
  <TransparentButton
    className={classNames(
      'rck-btn-more',
      disabled ? 'rck-btn-more--disabled' : '',
      className
    )}
    onClick={onClick}
    disabled={disabled}
    testId={getTestId(MoreActionsButtonTest.prefix, testId)}
  >
    <div className="rck-btn-more__icon-wrapper">
      <Icon
        type={IconType.ellipsisHoriz}
        color={IconColor.regular}
        size={size}
      />
    </div>
  </TransparentButton>
);

MoreActionsButton.displayName = 'MoreActionsButton';

export default React.memo(MoreActionsButton);
