import React, { SyntheticEvent } from 'react';
import { DisabledProps, BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';

export interface ActionableContentProps
  extends BaseComponentProps,
    DisabledProps {
  onClick: (ev: SyntheticEvent) => void;
  fillAvailableSpace?: boolean;
}

export enum ActionableContentTest {
  prefix = 'ActionableContent',
}

const ActionableContent: React.FunctionComponent<ActionableContentProps> = ({
  onClick,
  fillAvailableSpace,
  disabled,
  testId,
  className = '',
  children,
}) => (
  <div
    className={classNames(
      'rck-actionable-content',
      fillAvailableSpace ? 'rck-actionable-content--fill' : '',
      className
    )}
    data-testid={getTestId(ActionableContentTest.prefix, testId)}
    onClick={disabled ? undefined : onClick}
    aria-hidden="true"
  >
    {children}
  </div>
);

ActionableContent.displayName = 'ActionableContent';

export default ActionableContent;
