import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import useAssignRef from '../../hooks/useAssignRef';
import { TextFieldBaseProps } from './types/TextFieldBase.types';

const TextFieldBaseTest = {
  prefix: 'TextFieldBase',
  input: 'Input',
  contentRight: 'ContentRight',
};

const autoFocusElement = (element: HTMLInputElement) => {
  // We add a slight delay to let the component settle down after re-renders
  setTimeout(() => {
    if (element?.select) element.select();
  }, 100);
};

const defaultErrorConfig: TextFieldBaseProps['errorConfig'] = {
  border: true,
  text: true,
};

const TextFieldBase = React.forwardRef<HTMLInputElement, TextFieldBaseProps>(
  (props, ref) => {
    const {
      testId,
      required,
      className = '',
      disabled = false,
      hasError = false,
      errorConfig = defaultErrorConfig,
      size = 'small',
      type = 'text',
      width = 'block',
      autoFocus = false,
      selectAllOnFocus = false,
      contentLeft,
      contentRight,
      children,
      onFocus,
      onMouseDown,
      ...inputProps
    } = props;

    const textInput = React.useRef<HTMLInputElement>(null);
    const containerRef = React.useRef<HTMLDivElement>(null);

    // Autofocus on mount
    React.useEffect(() => {
      if (autoFocus && textInput?.current) {
        textInput.current.focus();
      }
    }, [autoFocus]);

    const onFocusHandler = React.useCallback(
      (e: React.FocusEvent<HTMLInputElement>) => {
        // Select all on focus
        if (selectAllOnFocus) {
          autoFocusElement(e.target);
        }

        // propagate
        onFocus?.(e);
      },
      [onFocus, selectAllOnFocus]
    );

    // Focus inner input component when parent is clicked
    const handleOnMouseDown = React.useCallback(
      (e: React.MouseEvent<HTMLInputElement>) => {
        // Running e.preventDefault() on the INPUT prevents double click behaviour
        const target: HTMLInputElement = e.target as HTMLInputElement;
        if (target.tagName !== 'INPUT') {
          e.preventDefault();
        }

        setTimeout(() => {
          if (
            textInput?.current &&
            !disabled &&
            document.activeElement !== textInput.current
          ) {
            textInput.current.focus();
          }
        }, 100);

        // propagate
        onMouseDown?.(e);
      },
      [onMouseDown, disabled]
    );

    /**
     * Function to determine if render a JSX component or
     * pass the current value to the passed in function.
     */
    const computedChildrenProp = React.useCallback(() => {
      if (!children) return null;

      // If the component recieves a JSX component, we render it
      if (React.isValidElement(children)) return children;

      // If the component recieves a function that returns JSX component,
      // we pass the current value to the funtion.
      if (typeof children === 'function') return children(inputProps.value);

      return null;
    }, [children, inputProps.value]);

    const renderInputComponent = () => (
      <input
        data-rck-input-base--input
        {...inputProps}
        type={type}
        required={required}
        aria-invalid={hasError || undefined}
        ref={useAssignRef(textInput, ref)}
        className="rck-text-field-base__input"
        disabled={disabled}
        onFocus={onFocusHandler}
        data-testid={getTestId(
          TextFieldBaseTest.prefix,
          testId,
          TextFieldBaseTest.input
        )}
      />
    );

    const applyErrorToBorder = hasError && errorConfig?.border;
    const applyErrorToText = hasError && errorConfig?.text;

    return (
      <div
        data-rck-input-base--container
        role="presentation"
        className={classNames(
          'rck-text-field-base',
          `rck-text-field-base--size-${size}`,
          width ? `rck-text-field-base--width-${width}` : undefined,
          applyErrorToBorder ? `rck-text-field-base--error-border` : undefined,
          applyErrorToText ? `rck-text-field-base--error-text` : undefined,
          className
        )}
        ref={containerRef}
        data-disabled={disabled || undefined}
        data-error={hasError || undefined}
        data-testid={getTestId(TextFieldBaseTest.prefix, testId)}
        onMouseDown={handleOnMouseDown}
      >
        {contentLeft && <div data-area--left>{contentLeft}</div>}
        <div
          data-area--body-wrapper
          className="rck-text-field-base__body-wrapper"
        >
          {computedChildrenProp() ?? renderInputComponent()}
        </div>
        {contentRight && (
          <div
            data-area--right
            data-testid={getTestId(
              TextFieldBaseTest.prefix,
              testId,
              TextFieldBaseTest.contentRight
            )}
          >
            {contentRight}
          </div>
        )}
      </div>
    );
  }
);

TextFieldBase.displayName = 'TextFieldBase';

/**
 * __TextFieldBase__
 *
 * A text field is an input that allows a user to write or edit text.
 *
 * - Use this component to create others that interit from TextField styles.
 * - Do not export this component to the end user, it is meant for internal use.
 *
 * You can use the following data selector for customizations:
 * - `[data-rck-input-base--container]`
 * - `[data-rck-input-base--input]`
 * -  __Areas:__
 *      - `[data-area--left]`
 *      - `[data-area--body-wrapper]`
 *      - `[data-area--right]`
 * -  __States:__
 *      - `[data-disabled]`
 *      - `[data-error]`
 *
 * @example
 * ```scss
 * .my-component {
 *      & > [data-rck-input-base--container] {
 *           &:focus-within: { ... }
 *      }
 * }
 * ```
 */
export default React.memo<
  TextFieldBaseProps & React.RefAttributes<HTMLInputElement>
>(TextFieldBase);
