import React, { useState, useRef, useEffect } from 'react';
import { BaseComponentProps, TestableComponentProps } from '../../common/types';
import Button, { ButtonSize, ButtonType } from '../Button/Button';
import { classNames, getTestId } from '../../helpers';

interface ExpandableTextProps
  extends BaseComponentProps,
    TestableComponentProps {
  maxLines?: number;
  expandText: string;
  collapseText: string;
  isTextExpanded?: boolean;
  onToggle?: () => void;
}

export enum ExpandableTextTest {
  prefix = 'Expandable-text',
  content = 'Content',
}

const ExpandableText = ({
  children,
  maxLines = 3,
  expandText,
  collapseText,
  className,
  testId,
  isTextExpanded,
  onToggle,
}: ExpandableTextProps) => {
  const [isExpanded, setIsExpanded] = useState(isTextExpanded ?? false);
  const [showButton, setShowButton] = useState(false);
  const textRef = useRef<HTMLDivElement>(null);

  /**
   * Check if the text overflows the container
   */
  const checkOverflow = () => {
    if (textRef.current) {
      // Get the lineHeight from the text element
      const lineHeight = Number(
        window.getComputedStyle(textRef.current).lineHeight.replace('px', '')
      );
      const maxHeight = lineHeight * maxLines;
      // check if the text overflows the container
      if (textRef.current.scrollHeight > maxHeight) {
        setShowButton(true);
      } else {
        setShowButton(false);
      }
    }
  };

  useEffect(() => {
    checkOverflow();
    const resizeObserver = new ResizeObserver(() => {
      checkOverflow();
    });

    if (textRef.current) {
      resizeObserver.observe(textRef.current);
    }

    return () => {
      if (textRef.current) {
        resizeObserver.unobserve(textRef.current);
      }
    };
  }, [maxLines, children]);

  const getIsExpanded = () => {
    if (isTextExpanded !== undefined) {
      return isTextExpanded;
    }
    return isExpanded;
  };

  const toggleExpand = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <div
      className={classNames('expandable-text', className)}
      data-testid={getTestId(ExpandableTextTest.prefix, testId)}
    >
      <div
        ref={textRef}
        className={classNames(
          'expandable-text__body',
          `expandable-text--${getIsExpanded() ? 'expanded' : 'collapsed'}`
        )}
        style={
          {
            '--max-lines': maxLines,
          } as React.CSSProperties
        }
        data-testid={getTestId(
          ExpandableTextTest.prefix,
          testId,
          ExpandableTextTest.content
        )}
        aria-expanded={getIsExpanded()}
      >
        {children}
      </div>
      {showButton && (
        <Button
          onClick={onToggle ?? toggleExpand}
          buttonType={ButtonType.plain}
          block
          size={ButtonSize.medium}
          centered
          className="expandable-text__expand-button"
        >
          {getIsExpanded() ? collapseText : expandText}
        </Button>
      )}
    </div>
  );
};

export default ExpandableText;
