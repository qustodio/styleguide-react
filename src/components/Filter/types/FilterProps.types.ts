import { BaseComponentProps } from '../../../common/types';
import { FilterSize } from './FilterSize.types';
import { FilterType } from './FilterType.types';

export interface FilterProps extends BaseComponentProps {
  text: string;
  active?: boolean;
  size?: FilterSize;
  type?: FilterType;
  onClick?: (ev: React.SyntheticEvent) => void;
}
