import { BaseComponentProps } from '../../../common/types';
import { FilterProps } from './FilterProps.types';

export interface FilterGroupProps extends BaseComponentProps {
  /**
   * This optional prop if used overrides `active` prop in Filter component.
   * It requires usage of `id` in `Filter` component.
   * */
  active?: number | string;

  /** This optional prop if used overrides `size` prop in `Filter` component. */
  size?: FilterProps['size'];

  /** This optional prop if used overrides `onClick` prop in `Filter` component. */
  onClick?: (id: number | string, ev: React.SyntheticEvent) => void;
}
