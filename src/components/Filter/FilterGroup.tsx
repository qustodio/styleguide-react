import * as React from 'react';
import { classNames, getTestId } from '../../helpers/index';
import Filter from './Filter';
import { FilterGroupProps } from './types/FilterGroupProps.types';
import FilterGroupTest from './types/FilterGroupTest.types';

const evaluateExceptions = (
  filter: React.ReactElement,
  active: FilterGroupProps['active'],
  onClick: FilterGroupProps['onClick'],
  size: FilterGroupProps['size']
) => {
  if (filter.type !== Filter) {
    throw new Error('FilterGroup children must be a Filter component');
  }
  if (active) {
    if (!filter.props?.id) {
      throw new Error(
        'Must use `id` prop in Filter component when using `active` prop in FilterGroup'
      );
    }
    if (filter.props?.active) {
      throw new Error(
        'Must cannot use `active` prop in Filter component when using `active` prop in FilterGroup'
      );
    }
  }
  if (onClick && filter.props?.onClick) {
    throw new Error(
      'You cannot use `onClick` prop in Filter component when FilterGroup has `onClick` prop'
    );
  }
  if (size && filter.props?.size) {
    throw new Error(
      'You cannot use `size` prop in Filter component when FilterGroup has `size` prop'
    );
  }
};

/**
 *
 * Component to wrap filters into a group a gives them spacing and the option to pass `active` prop and `onClick` prop for all Filter children.
 *
 * Behavior
 *
 * - It only allows `Filter` component as child `(1..n)`.
 * - If `active` prop is provided, you have to use `id` prop in `Filter` component.
 * - If `onClick` prop is provided, you cannot use `onClick` prop in `Filter` component
 *
 * @example
 *```tsx
 * import { FilterGroup, Filter } from 'styleguide-react';
 *
 * const Component = () => {
 *   const [active, setActive] = React.useState('1');
 *   const onClick = (id, _e) => {
 *     setActive(id);
 *   };
 *
 *   return <FilterGroup active={active} onClick={onClick} size="regular">
 *      <Filter id="1" text="Filter 1" />
 *      <Filter id="2" text="Primary" type="primary" />
 *      <Filter id="3" text="Error" type="error" />
 *      <Filter id="4" text="Secondary" type="secondary" />
 *      <Filter id="5" text="Filter 5" />
 *    </FilterGroup>
 * }
 *```
 */

const FilterGroup: React.FC<FilterGroupProps> = ({
  active,
  size,
  testId,
  className = '',
  children,
  onClick,
}) => {
  if (!React.Children.count(children)) {
    throw new Error('FilterGroup must have children');
  }
  return (
    <div
      className={classNames('rck-filter-group', className)}
      data-testid={getTestId(FilterGroupTest.prefix, testId)}
    >
      {React.Children.map(children, child => {
        const filter = child as React.ReactElement;
        if (!filter) return null;
        evaluateExceptions(filter, active, onClick, size);
        return React.cloneElement(filter, {
          ...(size && { size }),
          ...(filter.props?.id && { active: filter.props?.id === active }),
          ...(onClick && {
            onClick: (e: React.SyntheticEvent) =>
              onClick?.(filter.props?.id, e),
          }),
        });
      })}
    </div>
  );
};

FilterGroup.displayName = 'FilterGroup';

export default FilterGroup;
