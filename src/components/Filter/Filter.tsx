/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import * as React from 'react';
import { classNames, getTestId } from '../../helpers/index';
import { FilterProps } from './types/FilterProps.types';
import FilterTest from './types/FilterTest.types';

const Filter: React.FC<FilterProps> = ({
  text,
  size = 'regular',
  active = false,
  type = 'secondary',
  onClick,
  testId,
  className = '',
}) => (
  <div
    title={text}
    className={classNames(
      'rck-filter',
      `rck-filter--size-${size}`,
      active ? 'rck-filter--active' : '',
      active ? `rck-filter--active-color-${type}` : '',
      className
    )}
    data-testid={getTestId(FilterTest.prefix, testId)}
    onClick={onClick}
  >
    {text}
  </div>
);

Filter.displayName = 'Filter';

export default Filter;
