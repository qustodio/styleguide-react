export { default, AlertBoxTest } from './AlertBox';

export { AlertBoxType, AlertBoxProps } from './types/AlertBoxProps.types';
