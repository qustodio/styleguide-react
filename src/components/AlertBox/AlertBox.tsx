import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import Icon, { IconFamily, IconSize, IconType as IconTypeEnum } from '../Icons';
import { RocketCustomIconType } from '../Icons/CustomIcons/types/RocketCustomIconType.types';
import { FontAwesomeIconType } from '../Icons/FontAwesome/types/FontAwesomeIconType.types';
import { AlertBoxProps, AlertBoxType } from './types/AlertBoxProps.types';

export enum AlertBoxTest {
  prefix = 'AlertBox',
  actionElement = 'Action__close',
}

const getInfoIcon = () => (
  <Icon type={IconTypeEnum.infoCircle} size={IconSize.lg} />
);

const getLeftIcon = (icon: AlertBoxProps['icon'], showInfoIcon: boolean) => {
  if (!icon && showInfoIcon) return getInfoIcon();
  if (!icon) return null;

  if (React.isValidElement(icon))
    return React.cloneElement(icon, { size: IconSize.lg } as never);

  return (
    <Icon
      // FIXME(Icon): `type` prop should be able to accept `IconType` type
      type={icon as FontAwesomeIconType & RocketCustomIconType}
      size={IconSize.lg}
      family={IconFamily.solid}
    />
  );
};

const AlertBox: React.FunctionComponent<AlertBoxProps> = ({
  type = AlertBoxType.error,
  showInfoIcon,
  showCloseIcon,
  centered,
  fullWidth,
  rounded = true,
  solid = false,
  icon,
  testId,
  className = '',
  children,
  onClose,
}) => {
  const leftIcon = getLeftIcon(icon, Boolean(showInfoIcon));

  return (
    <div
      className={classNames(
        'rck-alert-box',
        `rck-alert-box--${type}`,
        fullWidth ? 'rck-alert-box--full-width' : '',
        !rounded ? 'rck-alert-box--no-rounded' : '',
        solid ? 'rck-alert-box--solid' : '',
        className
      )}
      data-testid={getTestId(AlertBoxTest.prefix, testId)}
    >
      <div
        className={classNames(
          'rck-alert-box__body',
          centered ? 'rck-alert-box__body--centered' : ''
        )}
      >
        {leftIcon && <div className="rck-alert-box__icon">{leftIcon}</div>}
        <div className="rck-alert-box__text">{children}</div>
      </div>
      {showCloseIcon && (
        <div
          className="rck-alert-box__icon-right"
          onClick={onClose}
          role="button"
          onKeyDown={onClose}
          tabIndex={0}
          data-testid={getTestId(
            AlertBoxTest.prefix,
            testId,
            AlertBoxTest.actionElement
          )}
        >
          <Icon type={IconTypeEnum.times} size={IconSize.lg} />
        </div>
      )}
    </div>
  );
};

AlertBox.displayName = 'AlertBox';

export default React.memo(AlertBox);
