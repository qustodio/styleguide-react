import { BaseComponentProps } from '../../../common/types';
import { IconType as IconTypeEnum } from '../../Icons';

export enum AlertBoxType {
  error = 'error',
  warning = 'warning',
  success = 'success',
  info = 'info',
  booster = 'booster',
  blue = 'blue',
}

// FIXME(Icon): we need to fix the IconType so we don't have to do this.
type IconType = typeof IconTypeEnum[keyof typeof IconTypeEnum];

export interface AlertBoxProps extends BaseComponentProps {
  /** @default AlertBoxType.error */
  type: AlertBoxType;
  /** @deprecated Use `icon` prop instead */
  showInfoIcon?: boolean;
  showCloseIcon?: boolean;
  /** When providng `<Icon />` size will be set to `IconSize.lg` to maintain consistency. */
  icon?: JSX.Element | IconType;
  centered?: boolean;
  fullWidth?: boolean;
  /** @default true */
  rounded?: boolean;
  /** @default false */
  solid?: boolean;
  onClose?: (ev: React.SyntheticEvent) => void;
}
