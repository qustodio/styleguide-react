import * as React from 'react';
import { classNames, getTestId } from '../../helpers';
import PortalModal from '../PortalModal/PortalModal';
import { BaseComponentProps, GlobalType } from '../../common/types';
import ModalStyledText from './utils/ModalStyledText';
import Text from '../Text';

export interface ModalHeader {
  icon?: JSX.Element;
  illustration?: JSX.Element;
}
/** @deprecated The close icon should always use the grey color */
export enum ModalCloseIconType {
  black = 'black',
  white = 'white',
}

/** @deprecated Buttons should always be in column */
export enum ModalButtonsAlignment {
  row = 'row',
  column = 'column',
}

export { ModalStyledText };

export enum ModalTest {
  prefix = 'Modal',
  title = 'Title_content',
  body = 'Body_content',
}

export type ModalSizes = 'small' | 'medium' | 'large';

export interface ModalProps extends BaseComponentProps {
  header?: ModalHeader;
  size: ModalSizes;
  /** @deprecated Use `size` prop instead */
  width?: number; // the type number has been deprecated, and is only kept for backward compatibility reasons.
  /** @deprecated modals should not have a defined height */
  height?: number; // this prop is only keep for backwards compatibility
  type?: GlobalType;
  title?: string;
  buttons?: JSX.Element[];
  buttonsAlignment?: ModalButtonsAlignment;
  /** @deprecated - does nothing */
  actionLabel?: string;
  /** @deprecated - does nothing */
  actionLoading?: boolean;
  closeIconType?: ModalCloseIconType;
  animationEnabled?: boolean;
  hideCloseButton?: boolean;
  /** When clicking back button, `onClickClose` will be invoked. */
  showBackButton?: boolean;
  /** @deprecated use the `size` property with `large` value, which goes fullscreen on mobile. */
  isFullScreen?: boolean;
  fixedContent?: JSX.Element[];
  onClickClose: () => void;
  onClickAction?: () => void;
}

const isBodyText = (children: React.ReactNode): React.ReactNode =>
  typeof children === 'string';

const getDimension = (value?: number) => {
  if (value) return `${value}px`;
  return undefined;
};

const Modal: React.FunctionComponent<ModalProps> = ({
  header,
  className,
  size,
  width,
  height,
  type = GlobalType.primary,
  title,
  buttons,
  buttonsAlignment = ModalButtonsAlignment.column,
  animationEnabled,
  hideCloseButton,
  testId,
  isFullScreen,
  showBackButton,
  fixedContent,
  onClickClose,
  children,
}) => {
  const renderHeader = header && (
    <div className="rck-modal__header">
      {header.icon &&
        React.cloneElement(header.icon, {
          className: classNames(
            'rck-modal__header-icon',
            header.icon.props.className || ''
          ),
        })}
      {header.illustration &&
        React.cloneElement(header.illustration, {
          className: classNames(
            'rck-modal__header-illustration',
            header.illustration.props.className
              ? header.illustration.props.className
              : ''
          ),
        })}
    </div>
  );

  const renderButtons = buttons && (
    <div
      className={classNames(
        buttonsAlignment === ModalButtonsAlignment.row
          ? 'rck-modal__buttons--row'
          : '',
        'rck-modal__buttons'
      )}
    >
      {// eslint-disable-next-line react/no-array-index-key
      buttons.map((button, key) => React.cloneElement(button, { key }))}
    </div>
  );

  const renderBody = (
    <div
      className={classNames(
        'rck-modal__body',
        header ? 'rck-modal__body--with-header' : ''
      )}
    >
      {title && (
        <Text
          variant="title-2-semibold"
          renderAs="h1"
          className="rck-modal__title"
          testId={getTestId(ModalTest.prefix, testId, ModalTest.title)}
        >
          {title}
        </Text>
      )}
      {children && (
        <div
          data-testid={getTestId(ModalTest.prefix, testId, ModalTest.body)}
          className="rck-modal__content-scrollable"
        >
          {isBodyText(children) ? (
            <ModalStyledText>{children}</ModalStyledText>
          ) : (
            children
          )}
        </div>
      )}
      {fixedContent && (
        <div
          data-testid={getTestId(
            ModalTest.prefix,
            testId,
            ModalTest.body,
            'fixed-content'
          )}
          className="rck-modal__content-fixed"
        >
          {fixedContent}
        </div>
      )}
      {renderButtons}
    </div>
  );

  return (
    <PortalModal
      closeButtonClassName="rck-modal__close-btn--negative"
      onClose={onClickClose}
      modalWrapperClassName={classNames(
        isFullScreen ? 'rck-modal--fullscreen' : '',
        `rck-modal--${size}`,
        className
      )}
      modalWrapperWidth={getDimension(width)}
      modalWrapperHeight={getDimension(height)}
      animationEnabled={animationEnabled}
      hideCloseButton={hideCloseButton}
      showBackButton={showBackButton}
      testId={testId}
    >
      <div
        className={classNames('rck-modal', type ? `rck-modal--${type}` : '')}
      >
        {renderHeader}
        {renderBody}
      </div>
    </PortalModal>
  );
};

Modal.displayName = 'Modal';

export default Modal;
