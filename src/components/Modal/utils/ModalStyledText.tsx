import React from 'react';
import { BaseComponentProps } from '../../../common/types';
import { classNames } from '../../../helpers/index';
import Text from '../../Text';

export interface ModalStyledTextProps extends BaseComponentProps {
  textAlign?: 'center' | 'left' | 'right';
  marginBottom?: '8' | '16' | '24';
}

const ModalStyledText: React.FunctionComponent<ModalStyledTextProps> = ({
  textAlign = 'center',
  marginBottom = '16',
  className = '',
  children,
}) => (
  <Text
    variant="body-1-regular"
    renderAs="p"
    className={classNames(
      'rck-modal__text',
      `rck-modal__text--${textAlign}`,
      `rck-modal__text--mbottom-${marginBottom}`,
      className
    )}
  >
    {children}
  </Text>
);

export default ModalStyledText;
