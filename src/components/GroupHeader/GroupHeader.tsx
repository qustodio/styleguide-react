/* eslint-disable react/jsx-wrap-multilines */
import React from 'react';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers';
import TransparentButton from '../Button/TransparentButton';
import Icon, { IconColor, IconType } from '../Icons';
import Link from '../Link/Link';
import Header from '../Header/StyledHeader';

export interface GroupHeaderProps extends BaseComponentProps {
  onClickHelp: () => void;
  helpUrl?: string;
  useAsLink?: React.FunctionComponent<AnchorPropTypes>;
}

export enum GroupHeaderTest {
  prefix = 'GroupHeader',
}

const GroupHeader: React.FunctionComponent<GroupHeaderProps> = ({
  onClickHelp,
  helpUrl,
  useAsLink,
  children,
  className = '',
  testId,
}) => (
  <div
    className={classNames('rck-group-header', className)}
    data-testid={getTestId(GroupHeaderTest.prefix, testId)}
  >
    <Header type="h4">{children}</Header>
    {(onClickHelp || helpUrl) && (
      <div className="rck-group-header__action-element">
        {!helpUrl && (
          <TransparentButton onClick={onClickHelp}>
            <Icon type={IconType.questionCircle} color={IconColor.regular} />
          </TransparentButton>
        )}
        {helpUrl && (
          <Link href={helpUrl} useAsLink={useAsLink}>
            <Icon type={IconType.questionCircle} color={IconColor.regular} />
          </Link>
        )}
      </div>
    )}
  </div>
);

GroupHeader.displayName = 'GroupHeader';

export default React.memo(GroupHeader);
