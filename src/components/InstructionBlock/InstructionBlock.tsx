import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';

export interface InstructionBlockProps extends BaseComponentProps {
  block: boolean;
}

export enum InstructionBlockTest {
  prefix = 'InstructionBlock',
}

const InstructionBlock: React.FunctionComponent<InstructionBlockProps> = ({
  block,
  className = '',
  testId,
  children,
}) => (
  <div
    data-testid={getTestId(InstructionBlockTest.prefix, testId)}
    className={classNames(
      'rck-instruction-block',
      block ? 'rck-instruction-block--block' : '',
      className
    )}
  >
    {children}
  </div>
);

InstructionBlock.displayName = 'InstructionBlock';

export default React.memo(InstructionBlock);
