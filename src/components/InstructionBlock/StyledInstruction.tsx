import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';

export interface StyledInstructionProps extends BaseComponentProps {
  block: boolean;
  color?: 'primary' | 'secondary' | 'error' | 'warning' | 'success';
}

export enum StyledInstructionTest {
  prefix = 'StyledInstruction',
}

const StyledInstruction: React.FunctionComponent<StyledInstructionProps> = ({
  className = '',
  testId,
  children,
  color,
}) => (
  <div
    data-testid={getTestId(StyledInstructionTest.prefix, testId)}
    className={classNames(
      'rck-styled-instruction',
      color ? `rck-styled-instruction--color-${color}` : '',
      className
    )}
  >
    {children}
  </div>
);

StyledInstruction.displayName = 'StyledInstruction';

export default React.memo(StyledInstruction);
