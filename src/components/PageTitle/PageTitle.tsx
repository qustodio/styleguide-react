import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';
import { ProfileInfoHeaderProps } from '../AccountInfoHeader/ProfileInfoHeader';
import TransparentButton from '../Button/TransparentButton';
import Icon, { IconColor, IconSize, IconType } from '../Icons';
import Link from '../Link/Link';

export type PageTitleDefaultActionNames =
  | 'back'
  | 'edit'
  | 'delete'
  | 'search'
  | 'settings'
  | 'menu';

export type PageTitleSize = 'regular' | 'small' | 'medium';

export type PageTitleActionName = PageTitleDefaultActionNames | string | 'none';

export interface PageTitleActionElement {
  actionName: PageTitleActionName;
  icon?: JSX.Element;
  iconColor?: IconColor;
  location?: string;
  size?: PageTitleSize;
  onClick?: () => void;
}

export interface PageTitleProps extends BaseComponentProps {
  title?: string;
  subtitle?: string | JSX.Element;
  subtitleEllipsis?: boolean;
  centered?: boolean;
  profileInfoHeader?: React.ReactElement<ProfileInfoHeaderProps>;
  size: PageTitleSize;
  actions?: PageTitleActionElement[];
}

interface PageSizeProps extends Omit<PageTitleProps, 'actions'> {
  backAction?: PageTitleActionElement;
  menuAction?: PageTitleActionElement;
  otherActions: PageTitleActionElement[] | undefined;
}

export enum PageTitleTest {
  prefix = 'PageTitle',
}

const renderIcon = (action: PageTitleActionElement): JSX.Element | null => {
  const iconColor = action.iconColor ?? IconColor.black;
  const iconSize = IconSize.lg;
  if (action.icon) return action.icon;
  switch (action.actionName) {
    case 'menu':
      return <Icon type={IconType.bars} color={iconColor} size={iconSize} />;
    case 'back':
      return (
        <Icon type={IconType.arrowLeft} color={iconColor} size={iconSize} />
      );
    case 'edit':
      return <Icon type={IconType.edit} color={iconColor} size={iconSize} />;
    case 'delete':
      return (
        <Icon type={IconType.trashAlt} color={iconColor} size={iconSize} />
      );
    case 'search':
      return <Icon type={IconType.search} color={iconColor} size={iconSize} />;
    case 'settings':
      return <Icon type={IconType.cog} color={iconColor} size={iconSize} />;
    default:
      return action.icon
        ? React.cloneElement(action.icon, { color: iconColor })
        : null;
  }
};

const renderAction = ({
  action,
  isRightSide,
  testId,
}: {
  action: PageTitleActionElement;
  isRightSide?: boolean;
  testId?: string;
}): JSX.Element => (
  <div
    key={action.actionName}
    className={classNames(
      'rck-page-title__action',
      isRightSide ? 'rck-page-title__action--right' : ''
    )}
  >
    {action.location && (
      <Link
        href={action.location}
        onClick={() => {
          if (action.onClick) action.onClick();
        }}
      >
        {renderIcon(action)}
      </Link>
    )}
    {!action.location && (
      <TransparentButton
        fillAvailableSpace
        testId={getTestId(PageTitleTest.prefix, testId, action.actionName)}
        onClick={() => {
          if (action.onClick) action.onClick();
        }}
      >
        {renderIcon(action)}
      </TransparentButton>
    )}
  </div>
);

const PageTitleMedium: React.FC<PageSizeProps> = ({
  title,
  subtitle,
  profileInfoHeader,
  size = 'regular',
  centered,
  className = '',
  subtitleEllipsis = false,
  testId,
  backAction,
  menuAction,
  otherActions,
}) => (
  <div
    className={classNames(
      'rck-page-title',
      `rck-page-title--${size}`,
      className
    )}
    data-testid={getTestId(PageTitleTest.prefix, testId)}
  >
    <div className="rck-page-title--left">
      {profileInfoHeader && (
        <div className="rck-page-title__header-info">{profileInfoHeader}</div>
      )}
      <div className="rck-page-title__title-wrapper">
        {backAction && renderAction({ action: backAction, testId })}

        {title && (
          <div
            className={classNames(
              `rck-page-title__title`,
              backAction ? 'rck-page-title__title--level-2' : '',
              centered ? 'rck-page-title__title--centered' : ''
            )}
          >
            {title}
          </div>
        )}

        {menuAction && renderAction({ action: menuAction, testId })}

        {otherActions && otherActions?.length !== 0 && (
          <div className="rck-page-title--right">
            {otherActions?.map((action: PageTitleActionElement) =>
              renderAction({ action, isRightSide: true, testId })
            )}
          </div>
        )}
      </div>
    </div>
    {subtitle && (
      <div
        className={classNames(
          'rck-page-title__subtitle',
          centered ? 'rck-page-title__subtitle--centered' : '',
          subtitleEllipsis ? 'rck-page-title__subtitle--ellipsis' : ''
        )}
      >
        {subtitle}
      </div>
    )}
  </div>
);

const PageTitleRegularOrSmall: React.FunctionComponent<PageSizeProps> = ({
  title,
  subtitle,
  profileInfoHeader,
  size = 'regular',
  subtitleEllipsis = true,
  backAction,
  menuAction,
  otherActions,
  centered,
  testId,
}) => (
  <>
    <div className="rck-page-title--left">
      {backAction && renderAction({ action: backAction, testId })}
      {menuAction && renderAction({ action: menuAction, testId })}
      {profileInfoHeader && (
        <div className="rck-page-title__header-info">{profileInfoHeader}</div>
      )}
      <div className="rck-page-title__title-wrapper">
        {title && (
          <div
            className={classNames(
              `rck-page-title__title`,
              backAction ? 'rck-page-title__title--level-2' : '',
              centered ? 'rck-page-title__title--centered' : ''
            )}
          >
            {title}
          </div>
        )}
        {subtitle && size === 'regular' && (
          <div
            className={classNames(
              'rck-page-title__subtitle',
              centered ? 'rck-page-title__subtitle--centered' : '',
              subtitleEllipsis ? 'rck-page-title__subtitle--ellipsis' : ''
            )}
          >
            {subtitle}
          </div>
        )}
      </div>
    </div>

    {otherActions && otherActions?.length !== 0 && (
      <div className="rck-page-title--right">
        {otherActions?.map(action =>
          renderAction({ action, isRightSide: true, testId })
        )}
      </div>
    )}
  </>
);

const isOtherAction = (action: PageTitleActionElement) =>
  action.actionName !== 'back' &&
  action.actionName !== 'menu' &&
  action.actionName !== '';

const hasNameAction = (action: PageTitleActionElement) =>
  action.actionName !== '';

const PageTitle: React.FunctionComponent<PageTitleProps> = ({
  title,
  subtitle,
  profileInfoHeader,
  size = 'regular',
  subtitleEllipsis = false,
  actions,
  centered,
  className = '',
  testId,
}) => {
  const backAction = actions?.find(action => action.actionName === 'back');
  const menuAction = actions?.find(action => action.actionName === 'menu');
  const otherActions = actions?.filter(isOtherAction).filter(hasNameAction);

  return (
    <div
      className={classNames(
        'rck-page-title',
        `rck-page-title--${size}`,
        className
      )}
      data-testid={getTestId(PageTitleTest.prefix, testId)}
    >
      {size === 'medium' ? (
        <PageTitleMedium
          title={title}
          subtitle={subtitle}
          profileInfoHeader={profileInfoHeader}
          size={size}
          subtitleEllipsis={subtitleEllipsis}
          backAction={backAction}
          menuAction={menuAction}
          otherActions={otherActions}
          centered={centered}
          className={className}
          testId={testId}
        />
      ) : (
        <PageTitleRegularOrSmall
          title={title}
          subtitle={subtitle}
          profileInfoHeader={profileInfoHeader}
          size={size}
          subtitleEllipsis={subtitleEllipsis}
          backAction={backAction}
          menuAction={menuAction}
          otherActions={otherActions}
          centered={centered}
          className={className}
          testId={testId}
        />
      )}
    </div>
  );
};

PageTitle.displayName = 'PageTitle';

export default PageTitle;
