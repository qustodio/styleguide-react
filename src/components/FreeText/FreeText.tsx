import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { classNames, getTestId } from '../../helpers/index';

export type FreeTextFontSize =
  | '10px'
  | '12px'
  | '14px'
  | '16px'
  | '18px'
  | '20px'
  | '24px'
  | '30px'
  | '36px'
  | '48px';

export type FreeTextColor =
  | 'gray'
  | 'gray-dark'
  | 'gray-semi'
  | 'gray-light'
  | 'gray-lighter'
  | 'error-dark'
  | 'warning-dark'
  | 'success-dark'
  | 'white';

export type FreeTextFontWeight = 'normal' | 'semi-bold';

export interface FreeTextProps extends BaseComponentProps {
  fontSize: FreeTextFontSize;
  color: FreeTextColor;
  fontWeight: FreeTextFontWeight;
}

export enum FreeTextTest {
  prefix = 'FreeText',
}

const FreeText: React.FC<FreeTextProps> = ({
  fontSize,
  fontWeight,
  color,
  testId,
  children,
  className = '',
}) => (
  <span
    className={classNames(
      'rck-free-text',
      `rck-free-text--font-size-${fontSize}`,
      `rck-free-text--color-${color}`,
      `rck-free-text--font-weight-${fontWeight}`,
      className
    )}
    data-testid={getTestId(FreeTextTest.prefix, testId)}
  >
    {children}
  </span>
);

FreeText.displayName = 'FreeText';

export default FreeText;
