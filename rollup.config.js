import typescript from 'rollup-plugin-typescript2';
import sass from 'rollup-plugin-sass';
import commonjs from 'rollup-plugin-commonjs';
import external from 'rollup-plugin-peer-deps-external';
import resolve from 'rollup-plugin-node-resolve';
import json from '@rollup/plugin-json';
import pkg from './package.json';
import extractSassVariablesToJS from './scripts/rollup-plugin-extract-sass-variables';

export default {
  input: 'src/index.tsx',
  external: ['react', 'react-dom', 'memoizee', 'react-remove-scroll'],
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: true,
    },
    {
      file: pkg.module,
      format: 'es',
      exports: 'named',
      sourcemap: true,
    },
  ],
  plugins: [
    extractSassVariablesToJS(),
    external(),
    resolve({
      browser: true,
    }),
    typescript({
      exclude: '**/__tests__/**',
      clean: true,
      tsconfig: 'tsconfig.build.json',
    }),
    json(),
    commonjs(),
    {
      exclude: ['**/*.stories.js'],
    },
    sass(),
  ],
};
