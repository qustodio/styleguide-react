const fs = require('fs').promises;
const path = require('path');
const prettier = require('prettier');

async function indexingRelease() {
  const packageJson = require('../package.json');
  const version = packageJson.version;
  const htmlFilePath = path.resolve(__dirname, '../public/index.html');
  const prettierConfigPath = path.join(__dirname, '../.prettierrc');

  try {
    let htmlContent = await fs.readFile(htmlFilePath, 'utf8');

    if (htmlContent.includes(`href="/${version}/index.html"`)) {
      return;
    }

    const newListItem = `<li><a href="/${version}/index.html">${version}</a></li>`;
    const startIndex = htmlContent.indexOf('<div class="rck-versions-list">');
    const ulStartIndex = htmlContent.indexOf('<ul>', startIndex) + 4;

    htmlContent =
      htmlContent.slice(0, ulStartIndex) +
      newListItem +
      htmlContent.slice(ulStartIndex);

    const prettierConfig = await prettier.resolveConfig(prettierConfigPath);
    const formattedHtmlContent = prettier.format(htmlContent, {
      ...prettierConfig,
      parser: 'html',
    });

    await fs.writeFile(htmlFilePath, formattedHtmlContent, 'utf8');

    process.exit(0);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

indexingRelease();
