const { execSync } = require('child_process');
const fs = require('fs');
const path = require('path');
const sass = require('sass');

/**
 * This script has the following workflow:
 * 1. Reads sass variables file to a string.
 * 2. Create a string and use all the variable from sass inside `.css {[...]}`.
 *  - ex.: .css {rck-color-primary: #hex|$color; ...}
 * 3. Complile the output from the previous step using sass.
 * 4. Read the output and extract each line to generate a JS object.
 */
const EXPORT_VARIABLE_NAME = 'themeArtifact';
const regex = /^\$.*?:.*?;/gm;
const disclaimer = `/**
* WARNING: AUTO-GENERATED FILE
*
* This file was generated automatically by a script. Any modifications made to this file may be overwritten
* the next time the script runs. It is strongly recommended NOT to edit this file directly.
* Instead, modify the appropriate source file(s) and regenerate this file using the script.
*
* @see scripts/rollup-plugin-extract-sass-variables.js
*/\n\n`;

const themeDescription = `/** Rocket theme object */\n`;

const camelize = str => {
  return str
    .split('-')
    .map((word, index) =>
      index === 0
        ? word.toLowerCase()
        : word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()
    )
    .join('');
};

const _extractSassVariables = (content, transform) => {
  return Object.assign(
    {},
    ...content
      .split('\n')
      .filter(v => v.match(regex))
      .map(v => v.replace('$', '').split(';')[0])
      .map(v => {
        const [key, value] = v.split(':');
        if (transform) {
          const [nkey, nvalue] = transform(key, value);
          return { [camelize(nkey)]: nvalue.trim() };
        }
        return { [camelize(key)]: value.trim() };
      })
  );
};

const compileSASS = (inputPath, content, transform) => {
  const varsObject = _extractSassVariables(content, transform);
  let cssString = `@import "${inputPath}";\n.dummy-css {\n`;

  Object.entries(varsObject).forEach(([key, value]) => {
    cssString += `${key}: ${value};\n`;
  });

  cssString += '}\n';

  return sass.renderSync({ data: cssString }).css.toString();
};

const CSStoJS = cssString => {
  const varsList = cssString
    .replace('.dummy-css {\n', '')
    .replace('\n}', '')
    .replace(/;/g, '')
    .split('\n')
    .map(line => {
      const [k, v] = line.trim().split(': ');
      return `"${k}": "${v}"`;
    })
    .join(',');

  return JSON.parse(`{${varsList}}`);
};

/**
 * Extracts Sass variables to a JavaScript object and generates a file.
 * @param {Object} options - The options for extracting and generating the variables.
 * @param {Array<Object>} options.input - An array of input objects specifying the Sass files to extract variables from.
 * Each object should contain the following properties:
 *   - path: The path to the Sass file.
 *   - transform: A transform function that takes a variable key and value as parameters and returns an array with the transformed key-value pair.
 *   - objectKey: The key to use for the object that will contain the extracted variables.
 * @param {string} options.output - The path to the output file.
 * @example
 * // Extract Sass variables and generate a theme file
 * extractSassVariablesToJs({
 *   input: [
 *     {
 *       path: '_colors.scss',
 *       transform: (k, v) => {
 *         const newKey = k.replace('color-', '');
 *         return [newKey, v];
 *       },
 *       objectKey: 'colors',
 *     },
 *   ],
 *   output: 'artefacts.ts',
 * });
 */
const extractSassVariablesToJs = (options = {}) => {
  const { input, output } = options;

  let tsContent = `${disclaimer}${themeDescription}`;
  tsContent += `const ${EXPORT_VARIABLE_NAME} = {`;

  input.forEach(({ path, transform, objectKey }) => {
    const sass = fs.readFileSync(path, 'utf8');
    const css = compileSASS(path, sass, transform);
    const js = CSStoJS(css);
    tsContent += `${objectKey}: ${JSON.stringify(js, null, 2)},`;
  });

  tsContent += `}\n`;
  tsContent += `export default ${EXPORT_VARIABLE_NAME};`;

  const outputPath = path.resolve(output);
  fs.writeFileSync(outputPath, tsContent, 'utf8');
  execSync(`yarn prettier --write ${outputPath}`);
};

/**
 * Rollup plugin
 */
const rollupPluginExtractSassVariables = () => {
  return {
    name: 'rollup-plugin-extract-sass-variables',

    buildStart() {
      extractSassVariablesToJs({
        input: [
          {
            path: 'src/assets/scss/_rocket-colors.scss',
            transform: (k, v) => {
              const nk = k.replace('rck-color-', '');
              return [nk, v];
            },
            objectKey: 'palette',
          },
          {
            path: 'src/assets/scss/routines/_colors.scss',
            transform: (k, v) => {
              const nk = k.replace('rck-routine-color-', '');
              return [nk, v];
            },
            objectKey: 'routinesPalette',
          },
        ],
        output: 'src/artefacts/themeArtifact.ts',
      });
    },
  };
};

export default rollupPluginExtractSassVariables;
