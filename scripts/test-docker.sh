#!/bin/bash

GIT_HASH=$(git describe --tags)
DOCKER_IMAGE_NAME="rocket:${GIT_HASH}"
docker build -f Dockerfile -t ${DOCKER_IMAGE_NAME} .
docker run --rm -v $PWD/stories/__snapshots__:/opt/qustodio/rocket/stories/__snapshots__ ${DOCKER_IMAGE_NAME} test
