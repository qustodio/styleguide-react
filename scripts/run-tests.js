const { spawn } = require('child_process');

const runTest = () => {
  const shouldUpdateSnapshot = process.argv.includes('-u');
  const testCommand = shouldUpdateSnapshot
    ? 'yarn test:stories -u'
    : 'yarn test:stories';

  const concurrentlyArgs = [
    '-k',
    '-s',
    'first',
    '-n',
    'SB,TEST',
    '-c',
    'magenta,blue',
    '"yarn storybook --ci > /dev/null 2>&1"',
    `\"wait-on tcp:6006 && ${testCommand}\"`,
  ];

  spawn('yarn', ['concurrently', ...concurrentlyArgs], {
    stdio: 'inherit',
    shell: true,
  });
};

runTest();
