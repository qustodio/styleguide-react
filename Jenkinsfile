#!/usr/bin/env groovy

pipeline {
    agent {
        label 'andslavedocker'
    }
    parameters {
        booleanParam(name: 'deploy_storybook', defaultValue: false, description: 'If checked, the storybook will be uploaded to aws s3 bucket')
    }
    options {
      disableConcurrentBuilds()
      timeout(60)
    }
    stages {
        stage('Build Docker image') {
            steps {
                script {
                    sh "git fetch --tags" // The current implementation doesn't fetch the tags from Git
                    GIT_HASH = sh(script: 'git describe --tags', returnStdout: true)
                        .toLowerCase() // Docker requires a lowercase name for the tag
                        .trim() // Removes final line break to avoid issues when embedding in a shell command
                    DOCKER_IMAGE_NAME = "rocket:${GIT_HASH}"
                    sh "docker build -f Dockerfile -t ${DOCKER_IMAGE_NAME} ."
                }
            }
        }
        stage('Tests') {
            steps {
                sh "docker run --rm -v ${WORKSPACE}/stories/__snapshots__:/opt/qustodio/rocket/stories/__snapshots__ ${DOCKER_IMAGE_NAME} test"
            }
        }
        stage ('Sonar: Adding PR info') {
            when { changeRequest() }
            steps {
                sh "echo \"sonar.pullrequest.base=$CHANGE_TARGET\" >> sonar-project.properties"
                sh "echo \"sonar.pullrequest.branch=$CHANGE_BRANCH\" >> sonar-project.properties"
                sh "echo \"sonar.pullrequest.key=$CHANGE_ID\" >> sonar-project.properties"
            }
        }
        stage ('Sonar: Adding Branch Info') {
            when { environment name: 'CHANGE_ID', value: '' }
            steps {
                sh "echo \"sonar.branch.name=$BRANCH_NAME\" >> sonar-project.properties"
            }
        }
        stage('Sonar: Scanner') {
            environment {
                ROCKET_VERSION=sh(script: "git describe --tags --abbrev=8", returnStdout: true).trim()
            }
            steps {
                sh 'echo "sonar.projectVersion=${ROCKET_VERSION}" >> sonar-project.properties'
                catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                    withSonarQubeEnv("Sonarcloud bitbucket") {
                        sh "docker run -e SONAR_HOST_URL=${env.SONAR_HOST_URL} -e SONAR_TOKEN=${env.SONAR_AUTH_TOKEN} -v \$(pwd):/usr/src sonarsource/sonar-scanner-cli -X -Dsonar.projectBaseDir=/usr/src -Dproject.settings=/usr/src/sonar-project.properties"
                    }
                }
            }
        }
        stage('Build Storybook') {
            steps {
                sh "docker run --rm -v ${WORKSPACE}/storybook-static:/opt/qustodio/rocket/storybook-static ${DOCKER_IMAGE_NAME} build-storybook"
            }
        }
        stage('Deploy Storybook') {
            when {
              anyOf {
                expression { params.deploy_storybook == true }
                buildingTag()
              }
            }
            steps {
                script {
                    def version = GIT_HASH.split("-")[0]
                    // Uploads the built Storybook to ${AWS_BUCKET_ROCKET_STORYBOOK_PATH}/${version}
                    sh "docker run --rm -v $HOME/.aws/credentials:/root/.aws/credentials:ro -v ${WORKSPACE}/storybook-static:/opt/qustodio/rocket/storybook-static ${DOCKER_IMAGE_NAME} deploy-storybook --bucket-path=${AWS_BUCKET_ROCKET_STORYBOOK_PATH}/${version} --aws-profile=dev --existing-output-dir=storybook-static"
                    // Uploads the built Storybook to ${AWS_BUCKET_ROCKET_STORYBOOK_PATH}/latest
                    sh "docker run --rm -v $HOME/.aws/credentials:/root/.aws/credentials:ro -v ${WORKSPACE}/storybook-static:/opt/qustodio/rocket/storybook-static ${DOCKER_IMAGE_NAME} deploy-storybook --bucket-path=${AWS_BUCKET_ROCKET_STORYBOOK_PATH}/latest --aws-profile=dev --existing-output-dir=storybook-static --s3-sync-options '--delete'"
                    // Uploads the index.html that references all the Storybook versions
                    sh "docker run --rm -v $HOME/.aws/credentials:/root/.aws/credentials:ro -v ${WORKSPACE}/public:/opt/qustodio/rocket/public ${DOCKER_IMAGE_NAME} deploy-storybook --bucket-path=${AWS_BUCKET_ROCKET_STORYBOOK_PATH}/ --aws-profile=dev --existing-output-dir=public"
                }
            }
        }
    }
    post {
        always {
            cleanWs()
        }
    }
}
