# Rocket

## Table of Contents

- [Rocket](#rocket)
  - [Table of Contents](#table-of-contents)
  - [Requirements](#requirements)
  - [Available Scripts](#available-scripts)
    - [`yarn build`](#yarn-build)
    - [`yarn storybook`](#yarn-storybook)
    - [`yarn build-storybook`](#yarn-build-storybook)
    - [`yarn test`](#yarn-test)
    - [`yarn ci`](#yarn-ci)
  - [Release](#release)
  - [Adding a new component](#adding-a-new-component)
  - [External libraries](#external-libraries)
  - [Rocket usage in external projects](#rocket-usage-in-external-projects)
  - [Update Font Awesome icons](#update-font-awesome-icons)

## Requirements

- Node required version: 20.10.0

## Available Scripts

In the project directory, you can run:

### `yarn build`

Builds the components as library to use in external projects

### `yarn storybook`

Runs the storybook<br />
Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

### `yarn build-storybook`

Builds the Storybook as static app. The build will be placed in "/storybook-static" folder. You can run the build locally with: `npx http-server storybook-static`

See the section about [exporting Storybook](https://storybook.js.org/docs/basics/exporting-storybook/) for more information.

More info [Storybook deployer](https://github.com/storybookjs/storybook-deployer)

### `yarn test`

Run the tests suite, we are using snapshot test. If you want to update the snapshot, use -u.

More info [Storybook test-runner](https://github.com/piratetaco/storybook-test-runner)

> [!IMPORTANT]
> Before running the tests, make sure to run `yarn playwright install` to install the browser binaries.

### `yarn ci`

Run all the test suit, use only inside a docker container

## Release

The release process is semi-automated; you only need to input the new version to be released.

```sh
yarn release 9.2.0
```

There is more information for the release process in the [Confluence release Rocket](https://qustodio.atlassian.net/wiki/spaces/FE/pages/3633938512/Release+Rocket) file.

## Adding a new component

You need to do the following:

- Add new folder in `src/components`
- Create the component `tsx` and `scss` with the styles
- Add the component to `src/index.tsx` and export it
- Add the styles to `src/styles.scss`
- Create your component stories in `stories/Component.stories.js`

## External libraries

This project uses

- React
- React-dom
- memoizee

as third party libs. In order to use this component library from git you should add these dependencies in your code

## Rocket usage in external projects

- Make sure to override scss variables `$fa-font-path` and `$images-path` relative from `node_modules/styleguide-react/src/assets/...` of your project.

## Update Font Awesome icons

1. Using Qustodio credentials [download pro web version](https://fontawesome.com/download)
2. Replace all files from `font-awesome<VERSION>/scss` to `src/assets/scss/vendor/fontawesome`
3. Replace all files from `font-awesome<VERSION>/webfonts` to `src/assets/scss/vendor/webfonts/fontawesome`
4. Ensure class names in file `src/components/Icons/FontAwesome/types/FontAwesomeIconFamily.types.ts`
