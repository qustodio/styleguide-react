
TROUBLESHOOTING

## The chromium binary is not available for arm64: 
This problem is caused by incompatibilities with the version of Puppeteer and M1 Macs. To solve:

```sh
PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true yarn install
```

## Cannot find the version of Chromium

This problem occurs on Mac M1. If it doesn't find the Chromium version, it installs it using:

```sh
# install te correct chromium version
yarn add --dev playwright 
yarn playwright install
```

## Host system is missing dependencies to run browsers (running test on docker)

To run the tests, Chromium and some additional system-level dependencies are required. Sometimes, cached Docker layers might have inconsistencies when installing these dependencies. To resolve this, it is recommended to delete the cached layers.

```sh
docker system prune   
docker system df
docker builder prune
```

## Updating the tests command is failing

- Make sure that you have playwright installed (`yarn playwright install`).
- Make sure that storybook local server is NOT running.