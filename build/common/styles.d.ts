import { TextWordBreak } from './types';
export declare const getWordBreakClass: (prefix: string, type?: TextWordBreak) => string;
