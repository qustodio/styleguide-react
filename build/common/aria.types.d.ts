import { AriaAttributes } from 'react';
export type AriaRole = 'alert' | 'application' | 'timer' | 'article' | 'banner' | 'button' | 'cell' | 'checkbox' | 'comment' | 'dialog' | 'document' | 'feed' | 'form' | 'grid' | 'gridcell' | 'heding' | 'listbox' | 'listitem' | 'main' | 'img' | 'row' | 'rowgroup' | 'search' | 'suggestion' | 'switch' | 'radio' | 'radiogroup' | 'tab' | 'tabpanel' | 'textbox' | 'option' | 'group' | 'presentation';
/**
 * Note: You are recommended to only use 0 and -1 as tabindex values.
 * @see https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex
 */
export type TabIndex = -1 | 0;
export interface AriaLabelingProps {
    /** Defines a string value that labels the current element. */
    'aria-label'?: string;
    /** Identifies the element (or elements) that labels the current element. */
    'aria-labelledby'?: string;
    /** Identifies the element (or elements) that describes the object. */
    'aria-describedby'?: string;
    /** Identifies the element (or elements) that provide a detailed, extended description for the object. */
    'aria-details'?: string;
}
export type ButtonAriaProps = AriaLabelingProps & Pick<AriaAttributes, 'aria-expanded' | 'aria-haspopup' | 'aria-controls' | 'aria-pressed' | 'aria-current'> & {
    /**
     * Removes element from the tab sequence.
     * The element will become non-focusable via the keyboard by tabbing.
     *
     * This should be used sparingly where there are other ways available of
     * accessing the element or its functionality via the keyboard.
     */
    skipTab?: boolean;
};
