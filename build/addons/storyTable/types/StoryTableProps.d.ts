import React from 'react';
import { PropConfig } from './PropConfig';
export interface StoryTableProps {
    component: <T extends object>(props: T, content?: React.ReactNode) => JSX.Element;
    content?: React.ReactNode;
    all?: PropConfig[];
    rows?: PropConfig[];
    cols?: PropConfig[];
}
