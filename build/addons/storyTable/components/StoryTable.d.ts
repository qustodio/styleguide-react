import React from 'react';
import { StoryTableProps } from '../types/StoryTableProps';
declare const _default: React.MemoExoticComponent<(props: StoryTableProps) => JSX.Element>;
export default _default;
