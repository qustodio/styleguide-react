'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var reactRemoveScroll = require('react-remove-scroll');
var ReactDOM = require('react-dom');
var memoize = require('memoizee');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
    if (e && e.__esModule) return e;
    var n = Object.create(null);
    if (e) {
        Object.keys(e).forEach(function (k) {
            if (k !== 'default') {
                var d = Object.getOwnPropertyDescriptor(e, k);
                Object.defineProperty(n, k, d.get ? d : {
                    enumerable: true,
                    get: function () { return e[k]; }
                });
            }
        });
    }
    n["default"] = e;
    return Object.freeze(n);
}

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);
var React__namespace = /*#__PURE__*/_interopNamespace(React);
var ReactDOM__namespace = /*#__PURE__*/_interopNamespace(ReactDOM);
var ReactDOM__default = /*#__PURE__*/_interopDefaultLegacy(ReactDOM);
var memoize__default = /*#__PURE__*/_interopDefaultLegacy(memoize);

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __spreadArray(to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

/**
 * WARNING: AUTO-GENERATED FILE
 *
 * This file was generated automatically by a script. Any modifications made to this file may be overwritten
 * the next time the script runs. It is strongly recommended NOT to edit this file directly.
 * Instead, modify the appropriate source file(s) and regenerate this file using the script.
 *
 * @see scripts/rollup-plugin-extract-sass-variables.js
 */
/** Rocket theme object */
var themeArtifact = {
    palette: {
        peach400: '#ffe8dc',
        peach500: '#ff965c',
        peach600: '#fd752c',
        peach700: '#d94d02',
        yellow400: '#ffedcf',
        yellow500: '#ffa710',
        yellow600: '#b37100',
        rckGrey500: '#36383b',
        rckGrey400: '#55585d',
        rckGrey300: '#989ba0',
        rckGrey200: '#dddedf',
        rckGrey100: '#fafafb',
        primary: '#3b9e8a',
        primaryDark: '#2f7768',
        primaryLight: '#9dcfc4',
        primaryLighter: '#e9fff8',
        secondary: '#6161ff',
        secondaryDark: '#4949b9',
        secondaryDarker: '#1f6178',
        secondaryLight: '#d7d7ff',
        secondaryLighter: '#f1f1ff',
        grayDark: '#36383b',
        gray: '#55585d',
        graySemi: '#989ba0',
        grayLight: '#dddedf',
        grayLighter: '#fafafb',
        black: '#36383b',
        white: '#ffffff',
        white70: 'rgba(255, 255, 255, 0.7)',
        white50: 'rgba(255, 255, 255, 0.5)',
        success: '#9dcfc4',
        successDark: '#2f7768',
        successLight: '#9dcfc4',
        successLighter: '#e9fff8',
        error: '#f3656f',
        errorDarker: '#a03941',
        errorDark: '#c84851',
        errorLight: '#fe9296',
        errorLighter: '#ffdde1',
        warningDark: '#d94d02',
        warning: '#fd752c',
        warningLight: '#ff965c',
        warningLighter: '#ffe8dc',
        decorationOrange: '#ff965c',
        decorationOrangeDark: '#c85618',
        decorationOrangeLight: '#ffdbc7',
        decorationBlue: '#60d0ec',
        decorationBlueDark: '#117892',
        decorationBlueLight: '#dff6fb',
        decorationGreen: '#49c592',
        decorationGreenDark: '#236c4e',
        decorationGreenLight: '#dbf3e9',
        booster: '#d70469',
        boosterDark: '#890343',
        boosterLight: '#f26faf',
        boosterLighter: '#fee1ef',
        school: '#0a95c2',
        schoolDark: '#054b61',
        schoolLight: '#6dd6f8',
        schoolLighter: '#cef1fd',
        marketingRed: '#e54141',
        marketingOrange: '#fd752c',
        marketingOrangeLight: '#ffe6d8',
        secondaryGradientFrom: '#6161ff',
        secondaryGradientTo: '#5252cc',
        orangeGradientFrom: '#ff965c',
        orangeGradientTo: '#c85618',
    },
    routinesPalette: {
        redDark: '#9d1515',
        red: '#e54141',
        redLight: '#fad9d9',
        purpleDark: '#7e0ca7',
        purple: '#b210ea',
        purpleLight: '#f0cffb',
        blueDark: '#008099',
        blue: '#00a9ca',
        blueLight: '#cceef4',
        yellowDark: '#b27100',
        yellow: '#ffa710',
        yellowLight: '#ffedcf',
        pinkDark: '#cc004a',
        pink: '#ff357e',
        pinkLight: '#ffd7e5',
        greenDark: '#319b6f',
        green: '#49c592',
        greenLight: '#dbf3e9',
        marineDark: '#25255b',
        marine: '#2f2f75',
        marineLight: '#d5d5e3',
    },
};

var createRckTheme = function () {
    var overrides = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        overrides[_i] = arguments[_i];
    }
    return Object.assign.apply(Object, __spreadArray([{}], __spreadArray([themeArtifact], overrides, true), false));
};

var defaultThemeProperties = {};
var theme = createRckTheme(defaultThemeProperties);

exports.GlobalType = void 0;
(function (GlobalType) {
    GlobalType["default"] = "default";
    GlobalType["primary"] = "primary";
    GlobalType["secondary"] = "secondary";
    GlobalType["gray"] = "gray";
    GlobalType["success"] = "success";
    GlobalType["warning"] = "warning";
    GlobalType["error"] = "error";
    GlobalType["white"] = "white";
    GlobalType["booster"] = "booster";
    GlobalType["school"] = "school";
    GlobalType["marketing"] = "marketing";
    GlobalType["decorationGreen"] = "decoration-green";
    GlobalType["decorationBlue"] = "decoration-blue";
    GlobalType["decorationOrange"] = "decoration-orange";
})(exports.GlobalType || (exports.GlobalType = {}));
exports.RoutineColor = void 0;
(function (RoutineColor) {
    RoutineColor["yellow"] = "yellow";
    RoutineColor["red"] = "red";
    RoutineColor["purple"] = "purple";
    RoutineColor["pink"] = "pink";
    RoutineColor["blue"] = "blue";
    RoutineColor["green"] = "green";
    RoutineColor["marine"] = "marine";
})(exports.RoutineColor || (exports.RoutineColor = {}));
var TextAlign;
(function (TextAlign) {
    TextAlign["notSet"] = "not-set";
    TextAlign["left"] = "left";
    TextAlign["right"] = "right";
    TextAlign["center"] = "center";
})(TextAlign || (TextAlign = {}));

var classNames = function () {
    var classes = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        classes[_i] = arguments[_i];
    }
    return classes.filter(function (className) { return className !== ''; }).join(' ');
};
var testEnvironments = ['test', 'testing'];
var isTestEnv = function () { var _a; return testEnvironments.includes((_a = process.env.NODE_ENV) !== null && _a !== void 0 ? _a : ''); };
var getTestId = function (prefix, id, suffix, suffix2) {
    return id
        ? "".concat(prefix, "__").concat(id).concat(suffix ? "--".concat(suffix) : '').concat(suffix2 ? "--".concat(suffix2) : '')
        : undefined;
};
var getTestIdWithParentTestId = function (parentTestId, testId) {
    if (!parentTestId && !testId)
        return undefined;
    if (!parentTestId)
        return testId;
    return "".concat(parentTestId).concat(testId ? "__".concat(testId) : '');
};
var handleKeyboardSelection = function (handlerFn) { return function (e) {
    var spaceCode = 32;
    if (e.keyCode === spaceCode)
        handlerFn(e);
}; };
/** Checks if a given value is a member of the specified enum. */
var isEnumMember = function (value, enumArg) { return Object.values(enumArg).includes(value); };

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function unwrapExports (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var IcomoonReact$1 = createCommonjsModule(function (module, exports) {
var __assign = (commonjsGlobal && commonjsGlobal.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (commonjsGlobal && commonjsGlobal.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IcomoonReact = exports.iconList = void 0;
var react_1 = __importDefault(React__default["default"]);
exports.iconList = function (iconSet) {
    var list = [];
    iconSet.icons.forEach(function (icon) {
        list.push(icon.properties.name.split(", ")[0]);
    });
    return list;
};
function getSvg(icon, iconSet, styles, size, className, rest) {
    var find = function (iconEl) { return iconEl.properties.name.split(", ").includes(icon); };
    var currentIcon = iconSet.icons.find(find);
    var renderPath = function (iconObj) { return function (path, index) {
        var attrs = (iconObj.attrs && iconObj.attrs[index]) || {};
        return react_1.default.createElement("path", __assign({ style: styles.path, key: index, d: path }, attrs));
    }; };
    if (currentIcon) {
        return (react_1.default.createElement("svg", __assign({ className: className, style: styles.svg, width: size, height: size, viewBox: "0 0 " + (currentIcon.icon.width || "1024") + " 1024", xmlns: "http://www.w3.org/2000/svg" }, rest), currentIcon.icon.paths.map(renderPath(currentIcon.icon))));
    }
    console.warn("icon " + icon + " does not exist.");
    return null;
}
exports.IcomoonReact = function (props) {
    var color = props.color, _a = props.size, size = _a === void 0 ? "100%" : _a, icon = props.icon, iconSet = props.iconSet, _b = props.className, className = _b === void 0 ? "" : _b, _c = props.style, style = _c === void 0 ? {} : _c, rest = __rest(props, ["color", "size", "icon", "iconSet", "className", "style"]);
    var styles = {
        svg: __assign({ display: "inline-block", verticalAlign: "middle" }, style),
        path: {
            fill: color
        }
    };
    return getSvg(icon, iconSet, styles, size, className, rest);
};
exports.IcomoonReact.displayName = "IcomoonReact";
exports.default = exports.IcomoonReact;

});

unwrapExports(IcomoonReact$1);
IcomoonReact$1.IcomoonReact;
IcomoonReact$1.iconList;

var src = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

var IcomoonReact_2 = IcomoonReact$1;
Object.defineProperty(exports, "iconList", { enumerable: true, get: function () { return IcomoonReact_2.iconList; } });
exports.default = IcomoonReact$1.IcomoonReact;

});

var IcomoonReact = unwrapExports(src);

var IcoMoonType = "selection";
var icons = [
	{
		icon: {
			paths: [
				"M986.163 702.4l-216.992-93c-25.555-11.062-55.597-3.624-72.954 17.938l-88.099 107.619c-138.495-68-250.991-180.559-318.989-318.999l107.656-88.18c21.499-17.562 28.839-47.32 17.967-72.88l-93.165-217.038c-12.2-27.806-42.198-43.056-73.397-36.244l-199.753 46.504c-28.501 6.5-48.438 31.56-48.438 60.88 0 504.6 410.385 915 914.97 915 29.338 0 54.394-19.936 60.934-48.442l46.502-201.6c6.758-29.158-8.442-59.558-36.243-71.558zM959.968 0h-191.994c-35.341 0-64 28.66-64 64s28.659 64 64 64h37.498l-210.793 210.8c-25 25-25 65.5 0 90.5s65.5 25 90.499 0l210.79-210.7v37.4c0 35.34 28.659 64 64 64 35.334 0 63.994-28.66 63.994-64v-192c0-35.34-28.595-64-63.994-64z"
			],
			attrs: [
				{
				}
			],
			isMulticolor: false,
			isMulticolor2: false,
			grid: 0,
			tags: [
				"outgoing-call"
			]
		},
		attrs: [
			{
			}
		],
		properties: {
			order: 4,
			id: 1,
			name: "outgoing-call",
			prevSize: 32,
			code: 59649
		},
		setIdx: 0,
		setId: 0,
		iconIdx: 0
	},
	{
		icon: {
			paths: [
				"M987.014 687.763l-212.474-91.059c-12.205-5.245-25.786-6.363-38.682-3.181-12.89 3.181-24.397 10.488-32.755 20.806l-86.266 105.377c-135.763-66.765-245.596-176.604-312.349-312.372l105.415-86.343c10.262-8.387 17.528-19.876 20.706-32.744s2.097-26.417-3.081-38.618l-91.257-212.557c-11.946-27.28-41.32-42.163-71.87-35.485l-195.596 45.531c-13.514 3.047-25.582 10.617-34.207 21.458s-13.29 24.301-13.223 38.154c0 494.087 401.844 895.939 895.923 895.939 13.869 0.070 27.347-4.589 38.208-13.21 10.861-8.627 18.451-20.698 21.523-34.227l45.472-197.357c3.187-13.99 1.389-28.659-5.088-41.459-6.483-12.8-17.235-22.938-30.4-28.653z",
				"M648.41 436.455h182.47c16.128 0 31.597-6.586 43.008-18.31 11.405-11.723 17.811-27.624 17.811-44.203s-6.406-32.479-17.811-44.203c-11.411-11.724-26.88-18.309-43.008-18.309h-35.68l200.32-205.861c10.925-11.819 16.922-27.556 16.717-43.857-0.198-16.301-6.592-31.875-17.805-43.403-11.219-11.527-26.374-18.096-42.234-18.304s-31.168 5.959-42.669 17.187l-200.301 205.783v-36.57c0-16.579-6.406-32.479-17.811-44.203s-26.88-18.309-43.008-18.309c-16.132 0-31.603 6.586-43.009 18.309s-17.814 27.624-17.814 44.203v187.537c0 16.579 6.408 32.48 17.814 44.203s26.877 18.31 43.009 18.31z"
			],
			attrs: [
				{
				},
				{
				}
			],
			isMulticolor: false,
			isMulticolor2: false,
			grid: 0,
			tags: [
				"incoming-call"
			]
		},
		attrs: [
			{
			},
			{
			}
		],
		properties: {
			order: 2,
			id: 0,
			name: "incoming-call",
			prevSize: 32,
			code: 59648
		},
		setIdx: 0,
		setId: 0,
		iconIdx: 1
	}
];
var height = 1024;
var metadata = {
	name: "qicons"
};
var preferences = {
	showGlyphs: true,
	showCodes: true,
	showQuickUse: true,
	showQuickUse2: true,
	showSVGs: true,
	fontPref: {
		prefix: "",
		metadata: {
			fontFamily: "qicons",
			majorVersion: 1,
			minorVersion: 0
		},
		metrics: {
			emSize: 1024,
			baseline: 6.25,
			whitespace: 50
		},
		embed: false,
		noie8: true,
		ie7: false,
		postfix: "",
		cssVars: true,
		cssVarsFormat: "scss",
		showSelector: true,
		selector: "class",
		classSelector: ".q-icon",
		showMetrics: true,
		showMetadata: true,
		showVersion: true
	},
	imagePref: {
		prefix: "q-",
		png: true,
		useClassSelector: true,
		color: 0,
		bgColor: 16777215,
		name: "qustodio",
		classSelector: ".q-icon"
	},
	historySize: 50,
	gridSize: 16
};
var iconSet = {
	IcoMoonType: IcoMoonType,
	icons: icons,
	height: height,
	metadata: metadata,
	preferences: preferences
};

/* eslint-disable @typescript-eslint/no-redeclare */
var BaseIconSize = {
    regular: 'regular',
};

var RocketCustomIconSize = __assign(__assign({}, BaseIconSize), { xs: 'rck-xs', sm: 'rck-sm', lg: 'rck-lg', x2: 'rck-2x', x3: 'rck-3x', x4: 'rck-4x', x5: 'rck-5x', x7: 'rck-7x', x10: 'rck-10x' });

/* eslint-disable @typescript-eslint/no-redeclare */
var IconSize = __assign(__assign({}, BaseIconSize), { xs: 'xs', sm: 'sm', lg: 'lg', x2: 'x2', x3: 'x3', x4: 'x4', x5: 'x5', x7: 'x7', x10: 'x10' });

var RocketCustomIcon = function (props) {
    var _a = props.size, size = _a === void 0 ? IconSize.regular : _a, type = props.type;
    return (React__default["default"].createElement("i", { className: classNames(RocketCustomIconSize[size], "rck-".concat(type), 'rck-custom-icon') },
        React__default["default"].createElement(IcomoonReact, { iconSet: iconSet, icon: type })));
};

// eslint-disable-next-line import/prefer-default-export
exports.IconFamily = void 0;
(function (FontAwesomeIconFamily) {
    FontAwesomeIconFamily["brands"] = "fa-brands";
    FontAwesomeIconFamily["solid"] = "fa-solid";
    FontAwesomeIconFamily["regular"] = "fa-regular";
})(exports.IconFamily || (exports.IconFamily = {}));

var FontAwesomeIconSize = __assign(__assign({}, BaseIconSize), { xs: 'fa-xs', sm: 'fa-sm', lg: 'fa-lg', x2: 'fa-2x', x3: 'fa-3x', x4: 'fa-4x', x5: 'fa-5x', x7: 'fa-7x', x10: 'fa-10x' });

var FontAwesomeIcon = function (props) {
    var _a = props.size, size = _a === void 0 ? IconSize.regular : _a, type = props.type, _b = props.family, family = _b === void 0 ? exports.IconFamily.regular : _b;
    return React__default["default"].createElement("i", { className: classNames(family, type, FontAwesomeIconSize[size]) });
};

// eslint-disable-next-line import/prefer-default-export
exports.IconColor = void 0;
(function (IconColor) {
    IconColor["notSet"] = "not-set";
    IconColor["error"] = "error";
    IconColor["warning"] = "warning";
    IconColor["success"] = "success";
    IconColor["secondary"] = "secondary";
    IconColor["neutral"] = "neutral";
    IconColor["black"] = "black";
    IconColor["primary"] = "primary";
    IconColor["regular"] = "regular";
    IconColor["contrast"] = "contrast";
    IconColor["contrastBlue"] = "contrast-secondary";
    IconColor["greenDecorationContrast"] = "green-decoration-contrast";
    IconColor["grayLight"] = "gray-light";
    IconColor["disabled"] = "disabled";
})(exports.IconColor || (exports.IconColor = {}));

exports.IconTest = void 0;
(function (IconTest) {
    IconTest["prefix"] = "Icon";
})(exports.IconTest || (exports.IconTest = {}));
var selectIcon = function (props) {
    switch (props.source) {
        case 'fontAwesome':
        case undefined:
            return React__namespace.createElement(FontAwesomeIcon, __assign({}, props));
        case 'custom':
            return React__namespace.createElement(RocketCustomIcon, __assign({}, props));
        default:
            return React__namespace.createElement(React__namespace.Fragment, null);
    }
};
var Icon = function (props) {
    var _a = props.size, size = _a === void 0 ? IconSize.regular : _a, _b = props.className, className = _b === void 0 ? '' : _b, _c = props.ariaHidden, ariaHidden = _c === void 0 ? true : _c, ariaLabel = props.ariaLabel, role = props.role, title = props.title, tabIndex = props.tabIndex, _d = props.color, color = _d === void 0 ? exports.IconColor.notSet : _d, square = props.square, circle = props.circle, testId = props.testId, onClick = props.onClick, onFocus = props.onFocus, onBlur = props.onBlur;
    return (
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions
    React__namespace.createElement("div", { className: classNames('rck-icon', "rck-icon--".concat(color), square ? "rck-icon--square" : '', square ? "rck-icon--square-".concat(size) : '', circle ? 'rck-icon--circle' : '', className), onClick: onClick, onFocus: onFocus, onBlur: onBlur, "aria-hidden": ariaHidden, "aria-label": ariaLabel, title: title, role: role, tabIndex: tabIndex, onKeyDown: onClick, "data-testid": getTestId(exports.IconTest.prefix, testId) }, selectIcon(__assign({}, props))));
};
Icon.displayName = 'Icon';
var Icon$1 = React__namespace.memo(Icon);

/* eslint-disable import/prefer-default-export */
var RocketCustomIconType;
(function (RocketCustomIconType) {
    RocketCustomIconType["outgoingCall"] = "outgoing-call";
    RocketCustomIconType["incomingCall"] = "incoming-call";
})(RocketCustomIconType || (RocketCustomIconType = {}));

/* eslint-disable import/prefer-default-export */
var FontAwesomeIconType;
(function (FontAwesomeIconType) {
    FontAwesomeIconType["android"] = "fa-android";
    FontAwesomeIconType["angleDown"] = "fa-angle-down";
    FontAwesomeIconType["angleDoubleDown"] = "fa-angle-double-down";
    FontAwesomeIconType["angleUp"] = "fa-angle-up";
    FontAwesomeIconType["angleRight"] = "fa-angle-right";
    FontAwesomeIconType["angleLeft"] = "fa-angle-left";
    FontAwesomeIconType["apple"] = "fa-apple";
    FontAwesomeIconType["arrowRight"] = "fa-arrow-right";
    FontAwesomeIconType["arrowUpRightFromSquare"] = "fa-arrow-up-right-from-square";
    FontAwesomeIconType["arrowCircleRight"] = "fas fa-arrow-circle-right";
    FontAwesomeIconType["arrowLeft"] = "fa-arrow-left";
    FontAwesomeIconType["backpack"] = "fa-backpack";
    FontAwesomeIconType["balanceScale"] = "fa-balance-scale";
    FontAwesomeIconType["ban"] = "fa-ban";
    FontAwesomeIconType["bars"] = "fa-bars";
    FontAwesomeIconType["basketball"] = "fa-basketball";
    FontAwesomeIconType["beerMug"] = "fa-beer-mug";
    FontAwesomeIconType["bell"] = "fa-bell";
    FontAwesomeIconType["book"] = "fa-book";
    FontAwesomeIconType["bookOpen"] = "fa-book-open";
    FontAwesomeIconType["books"] = "fa-books";
    FontAwesomeIconType["borderAll"] = "fa-border-all";
    FontAwesomeIconType["building"] = "fa-building";
    FontAwesomeIconType["buildingColumns"] = "fa-building-columns";
    FontAwesomeIconType["briefcase"] = "fa-briefcase";
    FontAwesomeIconType["bullseye"] = "fa-bullseye";
    FontAwesomeIconType["desktop"] = "fa-desktop";
    FontAwesomeIconType["calendarAlt"] = "far fa-calendar-alt";
    FontAwesomeIconType["calendarCheck"] = "fa-calendar-check";
    FontAwesomeIconType["calendarEdit"] = "fa-calendar-edit";
    FontAwesomeIconType["camera"] = "fa-camera";
    FontAwesomeIconType["cartShopping"] = "fa-cart-shopping";
    FontAwesomeIconType["chartBar"] = "fa-chart-bar";
    FontAwesomeIconType["chartColumn"] = "fa-chart-column";
    FontAwesomeIconType["chartPieSimple"] = "fa-chart-pie-simple";
    FontAwesomeIconType["check"] = "fa-check";
    FontAwesomeIconType["checkCircle"] = "fa-check-circle";
    FontAwesomeIconType["chevronRight"] = "fa-chevron-right";
    FontAwesomeIconType["chevronLeft"] = "fa-chevron-left";
    FontAwesomeIconType["chrome"] = "fab fa-chrome";
    FontAwesomeIconType["circleArrowUp"] = "fa-circle-arrow-up";
    FontAwesomeIconType["circleArrowDown"] = "fa-circle-arrow-down";
    FontAwesomeIconType["circleDot"] = "fa-circle-dot";
    FontAwesomeIconType["circleO"] = "fa-circle-o";
    FontAwesomeIconType["circleUser"] = "fa-circle-user";
    FontAwesomeIconType["placeOfWorship"] = "fa-place-of-worship";
    FontAwesomeIconType["clock"] = "fa-clock";
    FontAwesomeIconType["circle"] = "fa-circle";
    FontAwesomeIconType["comment"] = "fa-comment";
    FontAwesomeIconType["comments"] = "fa-comments";
    FontAwesomeIconType["commentsQuestionCheck"] = "fa-comments-question-check";
    FontAwesomeIconType["copy"] = "fa-copy";
    FontAwesomeIconType["cog"] = "fa-cog";
    FontAwesomeIconType["dots"] = "fa-comment-dots";
    FontAwesomeIconType["download"] = "far fa-download";
    FontAwesomeIconType["edit"] = "fa-edit";
    FontAwesomeIconType["ellipsisHoriz"] = "fa-ellipsis-h";
    FontAwesomeIconType["envelope"] = "fa-envelope";
    FontAwesomeIconType["exclamation"] = "fa-exclamation";
    FontAwesomeIconType["exclamationCircle"] = "fa-exclamation-circle";
    FontAwesomeIconType["exclamationTriangle"] = "fa-exclamation-triangle";
    FontAwesomeIconType["eye"] = "fa-eye";
    FontAwesomeIconType["eyeSlash"] = "fa-eye-slash";
    FontAwesomeIconType["familyPants"] = "fa-family-pants";
    FontAwesomeIconType["facebookSquare"] = "fa-facebook-square";
    FontAwesomeIconType["faceSmile"] = "fa-face-smile";
    FontAwesomeIconType["faceSmileRelaxed"] = "fa-face-smile-relaxed";
    FontAwesomeIconType["faceSwear"] = "fa-face-swear";
    FontAwesomeIconType["fileCircleInfo"] = "fa-file-circle-info";
    FontAwesomeIconType["filter"] = "fa-filter";
    FontAwesomeIconType["film"] = "fa-film";
    FontAwesomeIconType["flag"] = "fa-flag";
    FontAwesomeIconType["folderOpen"] = "fa-folder-open";
    FontAwesomeIconType["frown"] = "fa-frown";
    FontAwesomeIconType["gamepad"] = "fa-gamepad";
    FontAwesomeIconType["gamepadModern"] = "fa-gamepad-modern";
    FontAwesomeIconType["gem"] = "fa-gem";
    FontAwesomeIconType["globe"] = "fa-globe";
    FontAwesomeIconType["gift"] = "fa-gift";
    FontAwesomeIconType["graduationCap"] = "fa-graduation-cap";
    FontAwesomeIconType["grid2"] = "fa-grid-2";
    FontAwesomeIconType["grin"] = "fa-grin";
    FontAwesomeIconType["gun"] = "fa-gun";
    FontAwesomeIconType["handFist"] = "fa-hand-fist";
    FontAwesomeIconType["handHoldingHeard"] = "fa-hand-holding-heart";
    FontAwesomeIconType["handSparkles"] = "fa-hand-sparkles";
    FontAwesomeIconType["headphones"] = "fa-headphones";
    FontAwesomeIconType["headSet"] = "fa-headset";
    FontAwesomeIconType["headSideHeart"] = "fa-head-side-heart";
    FontAwesomeIconType["heart"] = "fa-heart";
    FontAwesomeIconType["heartbeat"] = "fa-heartbeat";
    FontAwesomeIconType["heartCircleCheck"] = "fa-heart-circle-check";
    FontAwesomeIconType["heartPulse"] = "fa-heart-pulse";
    FontAwesomeIconType["home"] = "fa-home-alt";
    FontAwesomeIconType["hourglassEnd"] = "fa-hourglass-end";
    FontAwesomeIconType["hourglassHalf"] = "fa-hourglass-half";
    FontAwesomeIconType["icons"] = "fa-icons";
    FontAwesomeIconType["infoCircle"] = "fa-info-circle";
    FontAwesomeIconType["instagram"] = "fa-instagram";
    FontAwesomeIconType["joystick"] = "fa-joystick";
    FontAwesomeIconType["laptop"] = "fa-laptop";
    FontAwesomeIconType["lightbulbOn"] = "fa-lightbulb-on";
    FontAwesomeIconType["link"] = "fa-link";
    FontAwesomeIconType["linkedin"] = "fa-linkedin";
    FontAwesomeIconType["listUl"] = "fa-list-ul";
    FontAwesomeIconType["listTimeline"] = "fa-list-timeline";
    FontAwesomeIconType["locationPlus"] = "fa-location-plus";
    FontAwesomeIconType["locationUnavailable"] = "fa-map-marker-slash";
    FontAwesomeIconType["lockAlt"] = "far fa-lock-alt";
    FontAwesomeIconType["lock"] = "fa-lock";
    FontAwesomeIconType["unlock"] = "fa-unlock";
    FontAwesomeIconType["mapMarker"] = "fa-map-marker";
    FontAwesomeIconType["mapMarkerAlt"] = "fa-map-marker-alt";
    FontAwesomeIconType["mapMarkerSlash"] = "far fa-map-marker-slash";
    FontAwesomeIconType["mapMarked"] = "fa-map-marked";
    FontAwesomeIconType["mapMarkedAlt"] = "fa-map-marked-alt";
    FontAwesomeIconType["meh"] = "fa-meh";
    FontAwesomeIconType["messages"] = "fa-messages";
    FontAwesomeIconType["messageSmile"] = "fa-message-smile";
    FontAwesomeIconType["minus"] = "fa-minus";
    FontAwesomeIconType["minusCircle"] = "fa-minus-circle";
    FontAwesomeIconType["mobileAlt"] = "fa-mobile-alt";
    FontAwesomeIconType["mobile"] = "far fa-mobile";
    FontAwesomeIconType["moon"] = "fa-moon";
    FontAwesomeIconType["music"] = "fa-music";
    FontAwesomeIconType["newspaper"] = "fa-newspaper";
    FontAwesomeIconType["palette"] = "fa-palette";
    FontAwesomeIconType["pause"] = "fa-pause";
    FontAwesomeIconType["pauseCircle"] = "fa-pause-circle";
    FontAwesomeIconType["pen"] = "fa-pen";
    FontAwesomeIconType["phone"] = "fa-phone";
    FontAwesomeIconType["phoneAlt"] = "fa-phone-alt";
    FontAwesomeIconType["phoneLaptop"] = "fa-phone-laptop";
    FontAwesomeIconType["phoneSlash"] = "fa-phone-slash";
    FontAwesomeIconType["phonePlus"] = "fa-phone-plus";
    FontAwesomeIconType["photoFilmMusic"] = "fa-photo-film-music";
    FontAwesomeIconType["pills"] = "fa-pills";
    FontAwesomeIconType["pizza"] = "fa-pizza-slice";
    FontAwesomeIconType["plane"] = "fa-plane";
    FontAwesomeIconType["play"] = "fa-play";
    FontAwesomeIconType["playCircle"] = "fa-play-circle";
    FontAwesomeIconType["plus"] = "fa-plus";
    FontAwesomeIconType["plusCircle"] = "fa-plus-circle";
    FontAwesomeIconType["pencil"] = "fa-pencil";
    FontAwesomeIconType["peopleGroup"] = "fa-people-group";
    FontAwesomeIconType["powerOff"] = "fa-power-off";
    FontAwesomeIconType["puzzlePiece"] = "fa-puzzle-piece";
    FontAwesomeIconType["questionCircle"] = "fa-question-circle";
    FontAwesomeIconType["rocket"] = "fa-rocket";
    FontAwesomeIconType["rocketLaunch"] = "fa-rocket-launch";
    FontAwesomeIconType["sadTear"] = "fa-sad-tear";
    FontAwesomeIconType["search"] = "fa-search";
    FontAwesomeIconType["searchMinus"] = "fa-search-minus";
    FontAwesomeIconType["server"] = "fa-server";
    FontAwesomeIconType["settings"] = "fa-cog";
    FontAwesomeIconType["shareNodes"] = "fa-share-nodes";
    FontAwesomeIconType["shieldAlt"] = "fa-shield-alt";
    FontAwesomeIconType["shieldCheck"] = "fa-shield-check";
    FontAwesomeIconType["shieldSlash"] = "fa-shield-slash";
    FontAwesomeIconType["shuffle"] = "fa-shuffle";
    FontAwesomeIconType["signOut"] = "fa-sign-out-alt";
    FontAwesomeIconType["sirenOn"] = "fa-siren-on";
    FontAwesomeIconType["sliders"] = "fas fa-sliders-v";
    FontAwesomeIconType["slidersHorizontal"] = "fa-sliders";
    FontAwesomeIconType["slotMachine"] = "fa-slot-machine";
    FontAwesomeIconType["smile"] = "fa-smile";
    FontAwesomeIconType["smoking"] = "fa-smoking";
    FontAwesomeIconType["sortDown"] = "fa-sort-down";
    FontAwesomeIconType["sortUp"] = "fa-sort-up";
    FontAwesomeIconType["sportsball"] = "fa-sportsball";
    FontAwesomeIconType["star"] = "fa-star";
    FontAwesomeIconType["stopWatch"] = "fa-stopwatch";
    FontAwesomeIconType["syncAlt"] = "fas fa-sync-alt";
    FontAwesomeIconType["tablet"] = "fa-tablet-alt";
    FontAwesomeIconType["thumbsUp"] = "fa-thumbs-up";
    FontAwesomeIconType["thumbsDown"] = "fa-thumbs-down";
    FontAwesomeIconType["times"] = "fa-times";
    FontAwesomeIconType["timesCircle"] = "fa-times-circle";
    FontAwesomeIconType["trashAlt"] = "fa-trash-alt";
    FontAwesomeIconType["twitterSquare"] = "fa-twitter-square";
    FontAwesomeIconType["user"] = "fa-user";
    FontAwesomeIconType["userFriends"] = "fa-user-friends";
    FontAwesomeIconType["userGroup"] = "fa-user-group";
    FontAwesomeIconType["userHeadSet"] = "fa-user-headset";
    FontAwesomeIconType["users"] = "fa-users";
    FontAwesomeIconType["userShield"] = "fa-user-shield";
    FontAwesomeIconType["utensils"] = "fa-utensils";
    FontAwesomeIconType["wandMagicSparkles"] = "fa-wand-magic-sparkles";
    FontAwesomeIconType["windows"] = "fa-windows";
    FontAwesomeIconType["wifi"] = "fa-wifi";
    FontAwesomeIconType["wifiSlash"] = "fa-wifi-slash";
    FontAwesomeIconType["xmarksLines"] = "fa-xmarks-lines";
    FontAwesomeIconType["xmark"] = "fa-xmark";
    FontAwesomeIconType["youtube"] = "fa-youtube";
    FontAwesomeIconType["youtubeSquare"] = "fa-youtube-square";
})(FontAwesomeIconType || (FontAwesomeIconType = {}));

var IconType = __assign(__assign({}, FontAwesomeIconType), RocketCustomIconType);

exports.AccordionItemTest = void 0;
(function (AccordionItemTest) {
    AccordionItemTest["prefix"] = "AccordionItem";
})(exports.AccordionItemTest || (exports.AccordionItemTest = {}));
var AccordionItem = function (_a) {
    var title = _a.title, content = _a.content, _b = _a.isOpen, isOpen = _b === void 0 ? false : _b, icon = _a.icon, onClick = _a.onClick, testId = _a.testId;
    return (React__default["default"].createElement("li", { className: classNames('rck-accordion__item', 'rck-accordion-item', isOpen ? 'rck-accordion-item--open' : ''), "data-testid": getTestId(exports.AccordionItemTest.prefix, testId), role: "menuitem", tabIndex: 0, "aria-expanded": isOpen, onClick: onClick, onKeyDown: onClick },
        React__default["default"].createElement(Icon$1, { type: icon, family: isOpen ? exports.IconFamily.solid : exports.IconFamily.regular, className: "rck-accordion-item__icon" }),
        React__default["default"].createElement("div", { className: "rck-accordion-item__title" }, title),
        React__default["default"].createElement("button", { className: "rck-accordion-item__button", type: "button" },
            React__default["default"].createElement(Icon$1, { type: isOpen ? IconType.minus : IconType.plus })),
        React__default["default"].createElement("div", { className: "rck-accordion-item__content" }, content)));
};
AccordionItem.displayName = 'AccordionItem';

exports.AccordionTest = void 0;
(function (AccordionTest) {
    AccordionTest["prefix"] = "Accordion";
})(exports.AccordionTest || (exports.AccordionTest = {}));
var Accordion = function (_a) {
    var testId = _a.testId, color = _a.color, _b = _a.highlightOnOpen, highlightOnOpen = _b === void 0 ? false : _b, _c = _a.maxColumns, maxColumns = _c === void 0 ? 1 : _c, _d = _a.items, items = _d === void 0 ? [] : _d;
    // Start with an empty `<ul></ul>` to avoid null checks.
    var elementRef = React.useRef(document.createElement('ul'));
    /**
     * When first loading the accordion, check if any item is forced open.
     */
    var getInitialOpenItemKey = function () {
        var openItem = items.find(function (item) { return item.isOpen === true; });
        return openItem ? openItem.key : null;
    };
    var _e = React.useState(getInitialOpenItemKey()), openItemKey = _e[0], setOpenItemKey = _e[1];
    /*
     * To create a responsive, masonry-like multi-column layout for the Accordion,
     * we use the hack described here:
     * @link https://tobiasahlin.com/blog/masonry-with-css/
     *
     * This hack requires the height of the Accordion to be set explicitely,
     * and to be at least as tall as the tallest column inside it. So we start by
     * setting the height to a very large number, and then we calculate the actual
     * height.
     *
     * Because the height is calculated on `useLayoutEffect()`, the calculations
     * are made before the component is rendered, so there are no layout shifts.
     */
    var resizeCalcHeight = 99999;
    /*
     * Sizes are stored as integers using `Math.ceil` to avoid re-rendering the
     * accordion when the difference in size is negligible. We round up to avoid
     * accordion items not fitting due to differences caused by decimal values.
     */
    var _f = React.useState(resizeCalcHeight), accordionHeight = _f[0], setAccordionHeight = _f[1];
    var _g = React.useState(0), accordionWidth = _g[0], setAccordionWidth = _g[1];
    var _h = React.useState(false), isResizing = _h[0], setIsResizing = _h[1];
    /**
     * Get the actual height of the accordion based on the bottom-most item offset.
     *
     * We need this workaround because for the masonry-like layout to work, the
     * accordion's height must be set explicitly, and it must be at least as tall
     * as the tallest column.
     *
     * For this reason, we set the accordion height to an absurdly large number,
     * and then calculate the actual height based on its contents.
     */
    var calcAccordionHeight = function (accordionEl) {
        var parentOffset = accordionEl.getBoundingClientRect().top;
        var bottomRowItems = Array.from(accordionEl.children).splice(maxColumns * -1);
        var bottomRowItemsOffset = bottomRowItems.map(function (item) { return item.getBoundingClientRect().bottom - parentOffset; });
        return Math.ceil(Math.max.apply(Math, bottomRowItemsOffset));
    };
    /*
     * Create observer that triggers height re-calculation when the Accordion
     * width changes.
     */
    var previousWidth = React.useRef(accordionWidth);
    React.useEffect(function () {
        var observerCallback = function (entries) {
            var accordionEl = entries[0];
            var currentWidth = Math.ceil(accordionEl.borderBoxSize[0].inlineSize);
            if (Math.ceil(previousWidth.current) !== Math.ceil(currentWidth)) {
                previousWidth.current = currentWidth;
                setAccordionWidth(currentWidth);
            }
        };
        var observer = new ResizeObserver(observerCallback);
        observer.observe(elementRef.current, { box: 'border-box' });
        return function () { return observer.disconnect(); };
    }, []);
    /*
     * Trigger height re-calculation when a property that can affect height changes.
     */
    React.useLayoutEffect(function () {
        // On the fist render, we just calculate and set the Accordions width. After
        // changing the width, this effect will run again, so we calculate the height
        // then.
        var isFirstRender = accordionWidth === 0;
        if (isFirstRender) {
            var elementWidth = Math.ceil(elementRef.current.getBoundingClientRect().width);
            previousWidth.current = elementWidth;
            setAccordionWidth(elementWidth);
        }
        else {
            setIsResizing(true);
            setAccordionHeight(resizeCalcHeight);
        }
    }, [openItemKey, items, maxColumns, accordionWidth]);
    /*
     * Calculates and updates the actual accordion height based on its contents,
     * only after ensuring the accordion height has been set to a large number to
     * allow calculation.
     */
    React.useLayoutEffect(function () {
        if (isResizing) {
            setIsResizing(false);
            setAccordionHeight(calcAccordionHeight(elementRef.current));
        }
    }, [accordionHeight, isResizing]);
    var renderItem = function (itemProps) { return (React__default["default"].createElement(AccordionItem, __assign({}, itemProps, { isOpen: itemProps.key === openItemKey, onClick: function () {
            return setOpenItemKey(itemProps.key === openItemKey ? null : itemProps.key);
        } }))); };
    return (
    /* The wrapper is needed to enable @container CSS queries. */
    React__default["default"].createElement("div", { className: "rck-accordion__wrapper" },
        React__default["default"].createElement("ul", { className: classNames('rck-accordion', color ? "rck-accordion--color-".concat(color) : '', maxColumns > 1 ? "rck-accordion--columns-".concat(maxColumns) : '', highlightOnOpen ? 'rck-accordion--highlight-on-open' : ''), "data-testid": getTestId(exports.AccordionTest.prefix, testId), role: "menu", ref: elementRef, style: { height: "".concat(accordionHeight, "px") } }, items.map(renderItem))));
};
Accordion.displayName = 'Accordion';

exports.AccordionMenuTest = void 0;
(function (AccordionMenuTest) {
    AccordionMenuTest["prefix"] = "AccordionMenu";
})(exports.AccordionMenuTest || (exports.AccordionMenuTest = {}));
var AccordionMenu = function (_a) {
    var isVisible = _a.isVisible, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId, children = _a.children;
    return (React__default["default"].createElement("ul", { className: classNames('rck-accordionMenu', isVisible ? 'rck-accordionMenu--is-visible' : '', className), "data-testid": getTestId(exports.AccordionMenuTest.prefix, testId) }, children));
};
AccordionMenu.displayName = 'AccordionMenu';

/**
 * Extracts ARIA attributes from a props object.
 *
 * @example
 * const ariaProps = useAriaProps({
 *   'aria-label': 'test',
 *   'aria-describedby': 'description',
 *   className: 'text-red' // Will be filtered out
 * });
 * // Returns { 'aria-label': 'test', 'aria-describedby': 'description' }
 */
var useAriaProps = function (props
// eslint-disable-next-line @typescript-eslint/no-explicit-any
) {
    return Object.entries(props).reduce(function (acc, _a) {
        var key = _a[0], value = _a[1];
        if (key.startsWith('aria-') && value != null) {
            acc[key] = value;
        }
        return acc;
    }, {});
};

exports.TransparentButtonTest = void 0;
(function (TransparentButtonTest) {
    TransparentButtonTest["prefix"] = "TransparentButton";
})(exports.TransparentButtonTest || (exports.TransparentButtonTest = {}));
var TransparentButton = React__default["default"].forwardRef(function (props, ref) {
    var id = props.id, _a = props.htmlType, htmlType = _a === void 0 ? 'button' : _a, testId = props.testId, role = props.role, tabIndex = props.tabIndex, inline = props.inline, _b = props.className, className = _b === void 0 ? '' : _b, disabled = props.disabled, onClick = props.onClick, children = props.children, fillAvailableSpace = props.fillAvailableSpace, skipTab = props.skipTab, rest = __rest(props, ["id", "htmlType", "testId", "role", "tabIndex", "inline", "className", "disabled", "onClick", "children", "fillAvailableSpace", "skipTab"]);
    var ariaProps = useAriaProps(rest);
    return (React__default["default"].createElement("button", __assign({}, ariaProps, { ref: ref, id: id, role: role, tabIndex: tabIndex !== null && tabIndex !== void 0 ? tabIndex : (skipTab ? -1 : undefined), className: classNames(className, 'rck-transparent-button', disabled ? 'rck-transparent-button--disabled' : '', fillAvailableSpace ? 'rck-transparent-button--fill' : '', inline ? 'rck-transparent-button--inline' : ''), "data-testid": getTestId(exports.TransparentButtonTest.prefix, testId), onClick: !disabled ? onClick : undefined, "aria-disabled": disabled, type: htmlType }), children));
});
var TransparentButton$1 = React__default["default"].memo(TransparentButton);

exports.AccountInfoHeaderTest = void 0;
(function (AccountInfoHeaderTest) {
    AccountInfoHeaderTest["prefix"] = "AccountInfoHeader";
})(exports.AccountInfoHeaderTest || (exports.AccountInfoHeaderTest = {}));
var AccountInfoHeader = function (_a) {
    var name = _a.name, logoImage = _a.logoImage, _b = _a.showGift, showGift = _b === void 0 ? false : _b, onClickGift = _a.onClickGift, expand = _a.expand, testId = _a.testId, _c = _a.className, className = _c === void 0 ? '' : _c;
    var renderContent = function () { return (React__default["default"].createElement("div", { className: "rck-account-info-header__user-info__name" },
        showGift && (React__default["default"].createElement(Icon$1, { type: IconType.gift, size: IconSize.xs, color: exports.IconColor.contrastBlue, family: exports.IconFamily.solid, square: true, className: "rck-account-info-header__user-info__gift" })),
        name)); };
    return (React__default["default"].createElement("div", { className: classNames('rck-account-info-header', expand ? 'rck-account-info-header--expanded' : '', className), "data-testid": getTestId(exports.AccountInfoHeaderTest.prefix, testId) },
        React__default["default"].createElement("div", { className: "rck-account-info-header__header" }, logoImage),
        React__default["default"].createElement("div", { className: "rck-account-info-header__user-info" },
            expand && showGift && (React__default["default"].createElement(TransparentButton$1, { onClick: onClickGift }, renderContent())),
            expand && !showGift && renderContent())));
};
AccountInfoHeader.displayName = 'AccountInfoHeader';
var AccountInfoHeader$1 = React__default["default"].memo(AccountInfoHeader);

exports.AlertBoxType = void 0;
(function (AlertBoxType) {
    AlertBoxType["error"] = "error";
    AlertBoxType["warning"] = "warning";
    AlertBoxType["success"] = "success";
    AlertBoxType["info"] = "info";
    AlertBoxType["booster"] = "booster";
    AlertBoxType["blue"] = "blue";
})(exports.AlertBoxType || (exports.AlertBoxType = {}));

exports.AlertBoxTest = void 0;
(function (AlertBoxTest) {
    AlertBoxTest["prefix"] = "AlertBox";
    AlertBoxTest["actionElement"] = "Action__close";
})(exports.AlertBoxTest || (exports.AlertBoxTest = {}));
var getInfoIcon = function () { return (React__namespace.createElement(Icon$1, { type: IconType.infoCircle, size: IconSize.lg })); };
var getLeftIcon = function (icon, showInfoIcon) {
    if (!icon && showInfoIcon)
        return getInfoIcon();
    if (!icon)
        return null;
    if (React__namespace.isValidElement(icon))
        return React__namespace.cloneElement(icon, { size: IconSize.lg });
    return (React__namespace.createElement(Icon$1
    // FIXME(Icon): `type` prop should be able to accept `IconType` type
    , { 
        // FIXME(Icon): `type` prop should be able to accept `IconType` type
        type: icon, size: IconSize.lg, family: exports.IconFamily.solid }));
};
var AlertBox = function (_a) {
    var _b = _a.type, type = _b === void 0 ? exports.AlertBoxType.error : _b, showInfoIcon = _a.showInfoIcon, showCloseIcon = _a.showCloseIcon, centered = _a.centered, fullWidth = _a.fullWidth, _c = _a.rounded, rounded = _c === void 0 ? true : _c, _d = _a.solid, solid = _d === void 0 ? false : _d, icon = _a.icon, testId = _a.testId, _e = _a.className, className = _e === void 0 ? '' : _e, children = _a.children, onClose = _a.onClose;
    var leftIcon = getLeftIcon(icon, Boolean(showInfoIcon));
    return (React__namespace.createElement("div", { className: classNames('rck-alert-box', "rck-alert-box--".concat(type), fullWidth ? 'rck-alert-box--full-width' : '', !rounded ? 'rck-alert-box--no-rounded' : '', solid ? 'rck-alert-box--solid' : '', className), "data-testid": getTestId(exports.AlertBoxTest.prefix, testId) },
        React__namespace.createElement("div", { className: classNames('rck-alert-box__body', centered ? 'rck-alert-box__body--centered' : '') },
            leftIcon && React__namespace.createElement("div", { className: "rck-alert-box__icon" }, leftIcon),
            React__namespace.createElement("div", { className: "rck-alert-box__text" }, children)),
        showCloseIcon && (React__namespace.createElement("div", { className: "rck-alert-box__icon-right", onClick: onClose, role: "button", onKeyDown: onClose, tabIndex: 0, "data-testid": getTestId(exports.AlertBoxTest.prefix, testId, exports.AlertBoxTest.actionElement) },
            React__namespace.createElement(Icon$1, { type: IconType.times, size: IconSize.lg })))));
};
AlertBox.displayName = 'AlertBox';
var AlertBox$1 = React__namespace.memo(AlertBox);

var Link = function (_a) {
    var Anchor = _a.useAsLink, ariaLabel = _a.ariaLabel, testId = _a.testId, id = _a.id, role = _a.role, tabIndex = _a.tabIndex, href = _a.href, hrefLang = _a.hrefLang, rel = _a.rel, target = _a.target, referrerPolicy = _a.referrerPolicy, icon = _a.icon, _b = _a.iconPosition, iconPosition = _b === void 0 ? 'right' : _b, disabled = _a.disabled, _c = _a.className, className = _c === void 0 ? '' : _c, onClick = _a.onClick, children = _a.children, rest = __rest(_a, ["useAsLink", "ariaLabel", "testId", "id", "role", "tabIndex", "href", "hrefLang", "rel", "target", "referrerPolicy", "icon", "iconPosition", "disabled", "className", "onClick", "children"]);
    var ariaProps = useAriaProps(rest);
    return (React__default["default"].createElement("span", { className: classNames('rck-link', disabled ? 'rck-link--disabled' : '', className) },
        Anchor && (React__default["default"].createElement(Anchor, { to: href, className: "", "aria-label": ariaLabel, "data-testid": testId, onClick: disabled ? undefined : onClick },
            icon && iconPosition === 'left' && (React__default["default"].createElement("span", { className: "rck-link__icon--left" }, icon)),
            children,
            icon && iconPosition === 'right' && (React__default["default"].createElement("span", { className: "rck-link__icon--right" }, icon)))),
        !Anchor && (React__default["default"].createElement("a", __assign({}, ariaProps, { id: id, href: href, "aria-label": ariaLabel, "data-testid": testId, onClick: disabled ? undefined : onClick, role: role, tabIndex: tabIndex, hrefLang: hrefLang, rel: rel, target: target, referrerPolicy: referrerPolicy }),
            icon && iconPosition === 'left' && (React__default["default"].createElement("span", { className: "rck-link__icon--left" }, icon)),
            children,
            icon && iconPosition === 'right' && (React__default["default"].createElement("span", { className: "rck-link__icon--right" }, icon))))));
};
Link.displayName = 'Link';
var Link$1 = React__default["default"].memo(Link);

exports.ActionInputIconPosition = void 0;
(function (ActionInputIconPosition) {
    ActionInputIconPosition["left"] = "left";
    ActionInputIconPosition["right"] = "right";
})(exports.ActionInputIconPosition || (exports.ActionInputIconPosition = {}));
var ActionInputButtonType;
(function (ActionInputButtonType) {
    ActionInputButtonType["button"] = "button";
    ActionInputButtonType["submit"] = "submit";
})(ActionInputButtonType || (ActionInputButtonType = {}));
exports.ActionInputTest = void 0;
(function (ActionInputTest) {
    ActionInputTest["prefix"] = "ActionInput";
    ActionInputTest["label"] = "ActionInput__label";
})(exports.ActionInputTest || (exports.ActionInputTest = {}));
var renderIcon$1 = function (icon, iconWithColor, position) {
    if (!icon || !position)
        return null;
    return (React__default["default"].createElement("div", { className: classNames('rck-action-input__container-icon', "rck-action-input__container-icon--".concat(position)) }, React__default["default"].cloneElement(icon, {
        className: classNames('rck-action-input__icon', iconWithColor ? 'rck-action-input__icon--with-color' : ''),
    })));
};
var ActionInput = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, icon = _a.icon, _c = _a.iconPosition, iconPosition = _c === void 0 ? exports.ActionInputIconPosition.left : _c, iconLeft = _a.iconLeft, iconRight = _a.iconRight, _d = _a.iconWithColor, iconWithColor = _d === void 0 ? true : _d, _e = _a.placeholder, placeholder = _e === void 0 ? '' : _e, _f = _a.text, text = _f === void 0 ? '' : _f, _g = _a.block, block = _g === void 0 ? false : _g, _h = _a.href, href = _h === void 0 ? '' : _h, useAsLink = _a.useAsLink, ariaLabel = _a.ariaLabel, testId = _a.testId, _j = _a.buttonType, buttonType = _j === void 0 ? ActionInputButtonType.button : _j, error = _a.error, _k = _a.disabled, disabled = _k === void 0 ? false : _k, _l = _a.onClick, onClick = _l === void 0 ? undefined : _l, onKeyDown = _a.onKeyDown, _m = _a.color, color = _m === void 0 ? undefined : _m, _o = _a.textColor, textColor = _o === void 0 ? undefined : _o, _p = _a.backgroundColor, backgroundColor = _p === void 0 ? undefined : _p, _q = _a.compact, compact = _q === void 0 ? false : _q;
    // We do this to be able to apply disable and error state Colors to an ActionInput with color prop set.
    var applyColor = !error && !disabled;
    var classes = classNames("rck-action-input", error ? 'rck-action-input--error' : '', className, block ? 'rck-action-input--block' : '', disabled ? 'rck-action-input--disabled' : '', compact ? 'rck-action-input--compact' : '', applyColor && color ? "rck-action-input--color-".concat(color) : '', // To be removed in v6
    applyColor && textColor ? "rck-action-input--text-color-".concat(textColor) : '', applyColor && backgroundColor
        ? "rck-action-input--background-color-".concat(backgroundColor)
        : '');
    var renderLabel = function (dataTestId) {
        return text || placeholder ? (
        // TODO: review this
        // eslint-disable-next-line jsx-a11y/label-has-associated-control
        React__default["default"].createElement("label", { className: classNames('rck-action-input__text', text ? '' : 'rck-action-input__text__placeholder'), "data-testid": getTestId(exports.ActionInputTest.label, dataTestId) }, text || placeholder)) : null;
    };
    if (href || useAsLink) {
        return (React__default["default"].createElement(Link$1, { useAsLink: useAsLink, href: href, className: classNames(classes, 'rck-action-input__anchor'), ariaLabel: ariaLabel, disabled: disabled, testId: getTestId(exports.ActionInputTest.prefix, testId) },
            !iconLeft &&
                renderIcon$1(icon, iconWithColor, exports.ActionInputIconPosition.left),
            renderIcon$1(iconLeft, iconWithColor, exports.ActionInputIconPosition.left),
            renderLabel(testId),
            renderIcon$1(iconRight, iconWithColor, exports.ActionInputIconPosition.right)));
    }
    return (React__default["default"].createElement("button", { type: buttonType, className: classes, onClick: onClick, onKeyDown: onKeyDown, "aria-label": ariaLabel, disabled: disabled, "data-testid": getTestId(exports.ActionInputTest.prefix, testId) },
        !iconLeft &&
            renderIcon$1(icon, iconWithColor, iconPosition === exports.ActionInputIconPosition.left
                ? exports.ActionInputIconPosition.left
                : undefined),
        renderIcon$1(iconLeft, iconWithColor, exports.ActionInputIconPosition.left),
        renderLabel(testId),
        renderIcon$1(iconRight, iconWithColor, exports.ActionInputIconPosition.right),
        !iconRight &&
            renderIcon$1(icon, iconWithColor, iconPosition === exports.ActionInputIconPosition.right
                ? exports.ActionInputIconPosition.right
                : undefined)));
};
ActionInput.displayName = 'ActionInput';
var ActionInput$1 = React__default["default"].memo(ActionInput);

var defaultIconsMap = {
    pencil: { icon: IconType.pen, solid: true },
    camera: { icon: IconType.camera, solid: true },
    status_tampered: { icon: IconType.exclamationTriangle, solid: true },
    status_paused: { icon: IconType.pause, solid: true },
    status_online: { icon: null },
};
var RendererIcon = function (_a) {
    var defaultIcon = _a.defaultIcon, iconColor = _a.iconColor, iconBackground = _a.iconBackground, children = _a.children;
    return (React__namespace.createElement("span", { className: classNames('rck-avatar__icon', iconColor ? "rck-avatar__icon--color-".concat(iconColor) : '', iconBackground ? "rck-avatar__icon--bgColor-".concat(iconBackground) : '', defaultIcon ? "rck-avatar__icon--default-icon-".concat(defaultIcon) : '') }, children));
};
var AvatarIcon = function (_a) {
    var _b, _c;
    var icon = _a.icon, iconColor = _a.iconColor, iconBackground = _a.iconBackground;
    if (!icon)
        return null;
    if (React__namespace.isValidElement(icon)) {
        return (React__namespace.createElement(RendererIcon, { iconBackground: iconBackground, iconColor: iconColor }, icon));
    }
    if (isEnumMember(icon, IconType)) {
        return (React__namespace.createElement(RendererIcon, { iconBackground: iconBackground, iconColor: iconColor },
            React__namespace.createElement(Icon$1, { type: icon, family: exports.IconFamily.regular })));
    }
    if (typeof icon !== 'string')
        return null;
    var defaultIconProps = (_b = defaultIconsMap[icon]) !== null && _b !== void 0 ? _b : icon;
    return (React__namespace.createElement(RendererIcon, { defaultIcon: icon },
        React__namespace.createElement(Icon$1, { type: ((_c = defaultIconProps.icon) !== null && _c !== void 0 ? _c : icon), family: defaultIconProps.solid ? exports.IconFamily.solid : exports.IconFamily.regular })));
};
AvatarIcon.displayName = 'AvatarIcon';

exports.AvatarSize = void 0;
(function (AvatarSize) {
    AvatarSize["extraExtraSmall"] = "xx-small";
    AvatarSize["extraSmall"] = "x-small";
    AvatarSize["small"] = "small";
    AvatarSize["medium"] = "medium";
    AvatarSize["large"] = "large";
    AvatarSize["extraLarge"] = "x-large";
})(exports.AvatarSize || (exports.AvatarSize = {}));
exports.AvatarTest = void 0;
(function (AvatarTest) {
    AvatarTest["prefix"] = "Avatar";
})(exports.AvatarTest || (exports.AvatarTest = {}));

var ImageVariantAvatar = function (_a) {
    var src = _a.src, srcSet = _a.srcSet, imgSizes = _a.imgSizes, alt = _a.alt, imgLoading = _a.imgLoading, imgReferrerPolicy = _a.imgReferrerPolicy;
    return (React__namespace.createElement("img", { src: src, alt: alt, srcSet: srcSet, sizes: imgSizes, loading: imgLoading, 
        // React 16 does not accept all the `referrerPolicy` values,
        // we will shove them doen its throat anyways.
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        referrerPolicy: imgReferrerPolicy }));
};
var AvatarLink = function (_a) {
    var className = _a.className, props = __rest(_a, ["className"]);
    return (React__namespace.createElement(Link$1, __assign({}, props, { className: classNames('rck-avatar--link-like', className) })));
};
var AvatarButton = function (props) { return (React__namespace.createElement(TransparentButton$1, __assign({}, props))); };
var rootComponent = {
    link: AvatarLink,
    button: AvatarButton,
};
var Avatar = React__namespace.forwardRef(function (props, ref) {
    var id = props.id, role = props.role, tabIndex = props.tabIndex, testId = props.testId, className = props.className, _a = props.size, size = _a === void 0 ? exports.AvatarSize.medium : _a, _b = props.emptyBackground, emptyBackground = _b === void 0 ? false : _b, _c = props.isFreeForm, isFreeForm = _c === void 0 ? false : _c, src = props.src, icon = props.icon, _d = props.iconColor, iconColor = _d === void 0 ? 'brand' : _d, _e = props.iconBackground, iconBackground = _e === void 0 ? 'white' : _e, children = props.children, renderAs = props.renderAs, rest = __rest(props, ["id", "role", "tabIndex", "testId", "className", "size", "emptyBackground", "isFreeForm", "src", "icon", "iconColor", "iconBackground", "children", "renderAs"]);
    var Component = renderAs ? rootComponent[renderAs] : 'div';
    var isDefaultRootEl = renderAs == null;
    var ariaProps = useAriaProps(isDefaultRootEl ? rest : {});
    var restProps = isDefaultRootEl ? {} : rest;
    var isTextVariant = ['number', 'string'].includes(typeof children);
    var textFirstLetter = isTextVariant
        ? children.charAt(0)
        : undefined;
    var computedTestId = getTestId(exports.AvatarTest.prefix, testId);
    return (React__namespace.createElement(Component, __assign({}, restProps, ariaProps, { 
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        ref: ref, id: id, role: role, tabIndex: tabIndex, className: classNames('rck-avatar', "rck-avatar--size-".concat(size), isTextVariant ? "rck-avatar--variant-text" : '', className), testId: computedTestId, "data-testid": computedTestId }),
        React__namespace.createElement("span", { className: classNames('rck-avatar__content', emptyBackground ? 'rck-avatar__content--empty' : '', isFreeForm ? 'rck-avatar__content--free-form' : '') }, src ? (React__namespace.createElement(ImageVariantAvatar, __assign({ src: src }, rest))) : (textFirstLetter !== null && textFirstLetter !== void 0 ? textFirstLetter : children)),
        React__namespace.createElement(AvatarIcon, { icon: icon, iconColor: iconColor, iconBackground: iconBackground })));
});
Avatar.displayName = 'Avatar';

exports.BadgeType = void 0;
(function (BadgeType) {
    BadgeType["error"] = "error";
    BadgeType["warning"] = "warning";
    BadgeType["neutral"] = "neutral";
    BadgeType["brand"] = "brand";
    BadgeType["secondary"] = "secondary";
    BadgeType["success"] = "success";
    BadgeType["dark"] = "dark";
    BadgeType["contrast"] = "contrast";
    BadgeType["school"] = "school";
})(exports.BadgeType || (exports.BadgeType = {}));
exports.BadgeTest = void 0;
(function (BadgeTest) {
    BadgeTest["prefix"] = "Badge";
})(exports.BadgeTest || (exports.BadgeTest = {}));
/**
 * @deprecated Please use `Tag` component instead.
 * ```jsx
 * <Tag type="squared" {...} />
 * ```
 */
var Badge = function (_a) {
    var text = _a.text, type = _a.type, _b = _a.size, size = _b === void 0 ? 'regular' : _b, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId;
    return (React__default["default"].createElement("div", { className: classNames('rck-badge', "rck-badge--".concat(type), "rck-badge--".concat(size), className), "data-testid": getTestId(exports.BadgeTest.prefix, testId) }, text));
};
Badge.displayName = 'Badge';
var Badge$1 = React__default["default"].memo(Badge);

exports.ButtonType = void 0;
(function (ButtonType) {
    ButtonType["primary"] = "primary";
    ButtonType["secondary"] = "secondary";
    ButtonType["plain"] = "plain";
})(exports.ButtonType || (exports.ButtonType = {}));
exports.ButtonSize = void 0;
(function (ButtonSize) {
    ButtonSize["small"] = "small";
    ButtonSize["medium"] = "medium";
    ButtonSize["big"] = "big";
})(exports.ButtonSize || (exports.ButtonSize = {}));
exports.ButtonColor = void 0;
(function (ButtonColor) {
    ButtonColor["secondary"] = "secondary";
    ButtonColor["error"] = "error";
    ButtonColor["white"] = "white";
    ButtonColor["wellbeing"] = "wellbeing";
})(exports.ButtonColor || (exports.ButtonColor = {}));
exports.ButtonIconPosition = void 0;
(function (ButtonIconPosition) {
    ButtonIconPosition["left"] = "left";
    ButtonIconPosition["right"] = "right";
})(exports.ButtonIconPosition || (exports.ButtonIconPosition = {}));
exports.ButtonTest = void 0;
(function (ButtonTest) {
    ButtonTest["prefix"] = "Button";
})(exports.ButtonTest || (exports.ButtonTest = {}));
var Button = function (_a) {
    var children = _a.children, label = _a.label, _b = _a.htmlType, htmlType = _b === void 0 ? 'button' : _b, _c = _a.buttonType, buttonType = _c === void 0 ? exports.ButtonType.primary : _c, _d = _a.color, color = _d === void 0 ? exports.ButtonColor.secondary : _d, _e = _a.loading, loading = _e === void 0 ? false : _e, _f = _a.disabled, disabled = _f === void 0 ? false : _f, _g = _a.block, block = _g === void 0 ? false : _g, _h = _a.centered, centered = _h === void 0 ? true : _h, _j = _a.iconPosition, iconPosition = _j === void 0 ? false : _j, form = _a.form, icon = _a.icon, _k = _a.size, size = _k === void 0 ? undefined : _k, _l = _a.className, className = _l === void 0 ? '' : _l, testId = _a.testId, _m = _a.onClick, onClick = _m === void 0 ? undefined : _m;
    var renderRightIcon = iconPosition === exports.ButtonIconPosition.right && icon
        ? React__namespace.cloneElement(icon, { className: 'rck-btn__icon' })
        : '';
    var renderLeftIcon = iconPosition === exports.ButtonIconPosition.left && icon
        ? React__namespace.cloneElement(icon, { className: 'rck-btn__icon' })
        : '';
    return (React__namespace.createElement("button", { disabled: disabled, className: classNames('rck-btn', buttonType ? "rck-btn--type-".concat(buttonType) : '', color ? "rck-btn--color-".concat(color) : '', loading ? 'rck-btn--loading' : '', block ? 'rck-btn--block' : '', size ? "rck-btn--".concat(size) : '', centered ? "rck-btn--centered" : '', iconPosition
            ? "rck-btn--with-icon\n          rck-btn--with-icon-".concat(iconPosition)
            : '', className), type: htmlType, form: form, onClick: onClick, "data-testid": getTestId(exports.ButtonTest.prefix, testId) },
        renderLeftIcon,
        React__namespace.createElement("span", { className: "rck-btn__label" }, children || label),
        renderRightIcon));
};
Button.displayName = 'Button';
var Button$1 = React__namespace.memo(Button);

var ActionableContentTest;
(function (ActionableContentTest) {
    ActionableContentTest["prefix"] = "ActionableContent";
})(ActionableContentTest || (ActionableContentTest = {}));
var ActionableContent = function (_a) {
    var onClick = _a.onClick, fillAvailableSpace = _a.fillAvailableSpace, disabled = _a.disabled, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children;
    return (React__default["default"].createElement("div", { className: classNames('rck-actionable-content', fillAvailableSpace ? 'rck-actionable-content--fill' : '', className), "data-testid": getTestId(ActionableContentTest.prefix, testId), onClick: disabled ? undefined : onClick, "aria-hidden": "true" }, children));
};
ActionableContent.displayName = 'ActionableContent';

exports.MoreActionsButtonTest = void 0;
(function (MoreActionsButtonTest) {
    MoreActionsButtonTest["prefix"] = "MoreActionsButton";
})(exports.MoreActionsButtonTest || (exports.MoreActionsButtonTest = {}));
var MoreActionsButton = function (_a) {
    var onClick = _a.onClick, disabled = _a.disabled, _b = _a.size, size = _b === void 0 ? IconSize.lg : _b, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId;
    return (React__default["default"].createElement(TransparentButton$1, { className: classNames('rck-btn-more', disabled ? 'rck-btn-more--disabled' : '', className), onClick: onClick, disabled: disabled, testId: getTestId(exports.MoreActionsButtonTest.prefix, testId) },
        React__default["default"].createElement("div", { className: "rck-btn-more__icon-wrapper" },
            React__default["default"].createElement(Icon$1, { type: IconType.ellipsisHoriz, color: exports.IconColor.regular, size: size }))));
};
MoreActionsButton.displayName = 'MoreActionsButton';
var MoreActionsButton$1 = React__default["default"].memo(MoreActionsButton);

/* eslint-disable import/prefer-default-export */
exports.CardTest = void 0;
(function (CardTest) {
    CardTest["prefix"] = "Card";
    CardTest["footerActionLeft"] = "CardFooter__action__left";
    CardTest["footerActionRight"] = "CardFooter__action__right";
    CardTest["headerTitle"] = "CardHeader__title";
    CardTest["headerAction"] = "CardHeader__action";
    CardTest["body"] = "CardBody";
})(exports.CardTest || (exports.CardTest = {}));

/* eslint-disable jsx-a11y/anchor-is-valid */
var CardFooter = function (_a) {
    var footerActionElement = _a.actionElement, testId = _a.testId, onClickAction = _a.onClickAction;
    return (React__namespace.createElement("div", { className: "rck-card__footer" }, footerActionElement && (React__namespace.createElement("span", { className: "rck-card__action rck-card__action--center" },
        React__namespace.createElement(Link$1, { onClick: onClickAction, "data-testid": getTestId(exports.CardTest.prefix, testId, exports.CardTest.footerActionRight) }, footerActionElement)))));
};
CardFooter.displayName = 'CardFooter';

/* eslint-disable jsx-a11y/anchor-is-valid */
var getTitle = function (title, testId) {
    if (!title)
        return null;
    if (typeof title !== 'string') {
        return React__namespace.cloneElement(title, {
            className: 'rck-card__header__title rck-card__header__title--custom-component',
            'data-testid': getTestId(exports.CardTest.prefix, testId, exports.CardTest.headerTitle),
        });
    }
    return (React__namespace.createElement("h4", { className: "rck-card__header__title rck-card__header__title--only-text", "data-testid": getTestId(exports.CardTest.prefix, testId, exports.CardTest.headerTitle) }, title));
};
var getActionElement = function (actionElement, onClickAction, testId) {
    if (!actionElement)
        return null;
    if (onClickAction || typeof actionElement === 'string') {
        return (React__namespace.createElement("span", { className: "rck-card__action rck-card__action--right" },
            React__namespace.createElement(Link$1, { onClick: onClickAction, testId: getTestId(exports.CardTest.prefix, testId, exports.CardTest.headerAction) }, actionElement)));
    }
    return (React__namespace.createElement("span", { className: "rck-card__action rck-card__action--right" }, React__namespace.cloneElement(actionElement, {
        'data-testid': getTestId(exports.CardTest.prefix, testId, exports.CardTest.headerAction),
    })));
};
var CardHeader = function (_a) {
    var title = _a.title, type = _a.type, border = _a.border, testId = _a.testId, banner = _a.banner, actionElement = _a.actionElement, wrapper = _a.wrapper, onClickAction = _a.onClickAction;
    var classes = classNames('rck-card__header', border ? 'rck-card__header--with-border' : '', banner ? 'rck-card__header--with-banner' : '', "rck-card__header--header-colors-".concat(type));
    var renderedBody = !banner ? (React__namespace.createElement(React__namespace.Fragment, null,
        getTitle(title, testId),
        getActionElement(actionElement, onClickAction, testId))) : (banner);
    return wrapper ? (wrapper({ children: renderedBody, className: classes })) : (React__namespace.createElement("div", { className: classes }, renderedBody));
};
CardHeader.displayName = 'CardHeader';

var Card = function (_a) {
    var header = _a.header, footer = _a.footer, displayAll = _a.displayAll, fullHeight = _a.fullHeight, fullWidth = _a.fullWidth, minHeight = _a.minHeight, _b = _a.className, className = _b === void 0 ? '' : _b, width = _a.width, height = _a.height, children = _a.children, _c = _a.type, type = _c === void 0 ? exports.GlobalType.default : _c, backgroundGradient = _a.backgroundGradient, backgroundShapes = _a.backgroundShapes, testId = _a.testId;
    var backgroundColorClass = !backgroundGradient
        ? "rck-card--bck-color-".concat(type)
        : "rck-card--bck-color-".concat(type, "-gradient");
    var noBodyAndFooter = footer === undefined && children === undefined;
    return (React__namespace.createElement("div", { className: classNames('rck-card', displayAll ? 'rck-card--display-all' : '', fullHeight ? 'rck-card--full-height' : '', fullWidth ? 'rck-card--full-width' : '', type
            ? "rck-card--".concat(type, " ").concat(backgroundColorClass)
            : 'rck-card--bck-color-default', backgroundShapes ? 'rck-card--with-shapes' : '', noBodyAndFooter ? 'rck-card--only-header' : '', className), style: { width: width, height: height }, "data-testid": getTestId(exports.CardTest.prefix, testId) },
        header && React__namespace.createElement(CardHeader, __assign({ type: type }, header)),
        children ? (React__namespace.createElement("div", { className: classNames('rck-card__body', minHeight ? "rck-card__body--min-height-".concat(minHeight) : ''), "data-testid": getTestId(exports.CardTest.prefix, testId, exports.CardTest.body) }, children)) : null,
        footer && React__namespace.createElement(CardFooter, __assign({}, footer))));
};
Card.displayName = 'Card';

exports.CheckboxTest = void 0;
(function (CheckboxTest) {
    CheckboxTest["prefix"] = "Checkbox";
    CheckboxTest["box"] = "Box";
})(exports.CheckboxTest || (exports.CheckboxTest = {}));
var Checkbox = function (_a) {
    var name = _a.name, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children, label = _a.label, checked = _a.checked, disabled = _a.disabled, block = _a.block, testId = _a.testId, onClick = _a.onClick, onBlur = _a.onBlur, onFocus = _a.onFocus, onMouseDown = _a.onMouseDown, onMouseUp = _a.onMouseUp;
    var handleOnClick = function () {
        if (!disabled && onClick) {
            onClick(!checked);
        }
    };
    return (React__namespace.createElement("label", { htmlFor: name, className: classNames(className, 'rck-checkbox', block ? 'rck-checkbox--block' : '', disabled ? 'rck-checkbox--disabled' : ''), "data-testid": getTestId(exports.CheckboxTest.prefix, testId) },
        label || children,
        React__namespace.createElement("input", { className: "rck-checkbox__input", type: "checkbox", checked: checked, name: name, onChange: function () { } }),
        React__namespace.createElement("div", { className: classNames('rck-checkbox__square', checked ? 'rck-checkbox__square--checked' : '', disabled ? 'rck-checkbox__square--disabled' : ''), onClick: handleOnClick, onBlur: onBlur, onFocus: onFocus, onMouseDown: onMouseDown, onMouseUp: onMouseUp, role: "checkbox", onKeyPress: handleOnClick, tabIndex: 0, "aria-checked": checked, "data-testid": getTestId(exports.CheckboxTest.prefix, testId, exports.CheckboxTest.box) }, checked && (React__namespace.createElement(Icon$1, { family: exports.IconFamily.solid, type: IconType.check, className: classNames('rck-checkbox__square__icon', disabled ? 'rck-checkbox__square__icon--disabled' : '') })))));
};
Checkbox.displayName = 'Checkbox';

exports.CopyBoxTest = void 0;
(function (CopyBoxTest) {
    CopyBoxTest["prefix"] = "CopyBox";
})(exports.CopyBoxTest || (exports.CopyBoxTest = {}));
var CopyBox = function (_a) {
    var text = _a.text, url = _a.url, confirmationText = _a.confirmationText, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId, onClickCallback = _a.onClickCallback;
    var _c = React__namespace.useState(false), confirmationVisible = _c[0], setConfirmationVisible = _c[1];
    var clickHandler = function () {
        window.navigator.clipboard.writeText(url);
        setConfirmationVisible(true);
        onClickCallback();
    };
    return (React__namespace.createElement("div", { className: classNames('rck-copybox', className), "data-testid": getTestId(exports.CopyBoxTest.prefix, testId) },
        React__namespace.createElement("div", { className: "rck-copybox__widget" },
            React__namespace.createElement("p", { className: classNames('rck-copybox__text') }, text),
            React__namespace.createElement("div", { className: "rck-copybox__icon", role: "button", tabIndex: 0, onKeyDown: clickHandler, onClick: clickHandler },
                React__namespace.createElement(Icon$1, { type: IconType.copy, color: exports.IconColor.contrast }))),
        confirmationVisible && (React__namespace.createElement("small", { className: "rck-copybox__copy-confirmation" }, confirmationText))));
};
CopyBox.displayName = 'CopyBox';

var TextTest;
(function (TextTest) {
    TextTest["prefix"] = "Text";
})(TextTest || (TextTest = {}));
var TextTest$1 = TextTest;

/**
 * Fixed typography variants
 *
 * These are the base typography types that are used in the design system.
 */
var textBaseVariants = [
    'hero-1-semibold',
    'hero-1-regular',
    'hero-2-semibold',
    'hero-2-regular',
    'headline-1-semibold',
    'headline-1-regular',
    'headline-2-semibold',
    'headline-2-regular',
    'title-1-semibold',
    'title-1-regular',
    'title-2-semibold',
    'title-2-regular',
    'title-3-semibold',
    'title-3-regular',
    'body-1-semibold',
    'body-1-regular',
    'body-2-semibold',
    'body-2-regular',
    'caption-1-semibold',
    'caption-1-regular',
    'caption-1-italic',
    'caption-2-semibold',
    'caption-2-regular',
    'caption-2-italic',
];
/**
 * These typographies change their size based on the viewport width.
 */
var textResponsiveVariants = [
    'headline-1-semibold-responsive',
    'headline-1-regular-responsive',
    'headline-2-semibold-responsive',
    'headline-2-regular-responsive',
    'title-1-semibold-responsive',
    'title-1-regular-responsive',
    'title-2-semibold-responsive',
    'title-2-regular-responsive',
    'title-3-semibold-responsive',
    'title-3-regular-responsive',
];
__spreadArray(__spreadArray([], textBaseVariants, true), textResponsiveVariants, true);
/**
 * It's recommended to use the `renderAs` prop to avoid coupling styles with
 * specific HTML elements.
 * By default, typography types are mapped to corresponding HTML elements, but
 * using `renderAs` allows for more flexibility in defining the heading level
 * hierarchy.
 */
var textVariantElementMappings = {
    'hero-1-semibold': 'h1',
    'hero-1-regular': 'h1',
    'hero-2-semibold': 'h2',
    'hero-2-regular': 'h2',
    'headline-1-semibold': 'h1',
    'headline-1-regular': 'h1',
    'headline-2-semibold': 'h2',
    'headline-2-regular': 'h2',
    'title-1-semibold': 'h3',
    'title-1-regular': 'h3',
    'title-2-semibold': 'h4',
    'title-2-regular': 'h4',
    'title-3-semibold': 'h5',
    'title-3-regular': 'h5',
    'body-1-semibold': 'div',
    'body-1-regular': 'div',
    'body-2-semibold': 'div',
    'body-2-regular': 'div',
    'caption-1-semibold': 'span',
    'caption-1-regular': 'span',
    'caption-1-italic': 'span',
    'caption-2-semibold': 'span',
    'caption-2-regular': 'span',
    'caption-2-italic': 'span',
    'headline-1-semibold-responsive': 'h1',
    'headline-1-regular-responsive': 'h1',
    'headline-2-semibold-responsive': 'h2',
    'headline-2-regular-responsive': 'h2',
    'title-1-semibold-responsive': 'h3',
    'title-1-regular-responsive': 'h3',
    'title-2-semibold-responsive': 'h4',
    'title-2-regular-responsive': 'h4',
    'title-3-semibold-responsive': 'h5',
    'title-3-regular-responsive': 'h5',
};

var Text = function (_a) {
    var _b, _c, _d;
    var _e = _a.variant, variant = _e === void 0 ? 'body-1-regular' : _e, color = _a.color, align = _a.align, renderAs = _a.renderAs, noWrap = _a.noWrap, marginTop = _a.marginTop, marginBottom = _a.marginBottom, parentTestId = _a.parentTestId, testId = _a.testId, children = _a.children, _f = _a.className, className = _f === void 0 ? '' : _f;
    var commonProps = {
        className: classNames('rck-text', "rck-text--type-".concat(variant), color ? "rck-text--color-".concat(color) : '', align ? "rck-text--align-".concat(align) : '', noWrap ? "rck-text--no-wrap" : '', marginTop ? "rck-text--margin-top-".concat(marginTop) : '', marginBottom ? "rck-text--margin-bottom-".concat(marginBottom) : '', className),
        'data-testid': parentTestId !== null && parentTestId !== void 0 ? parentTestId : getTestId(TextTest$1.prefix, testId),
    };
    if (renderAs) {
        if (typeof renderAs === 'string') {
            return React__namespace.createElement(renderAs, commonProps, children);
        }
        if (React__namespace.isValidElement(renderAs)) {
            var combinedClasses = classNames(commonProps.className, (_c = (_b = renderAs.props) === null || _b === void 0 ? void 0 : _b.className) !== null && _c !== void 0 ? _c : '');
            return React__namespace.cloneElement(renderAs, __assign(__assign({}, renderAs.props), { className: combinedClasses, 'data-testid': (_d = renderAs.props.testId) !== null && _d !== void 0 ? _d : getTestId(TextTest$1.prefix, testId) }), children);
        }
    }
    return React__namespace.createElement(textVariantElementMappings[variant], commonProps, children);
};
Text.displayName = 'Text';

var FixedCtaCard = function (_a) {
    var title = _a.title, description = _a.description, button = _a.button, backgroundColor = _a.backgroundColor;
    return (React__namespace.createElement("div", { className: classNames('rck-fixed-cta-card', "rck-fixed-cta-card--".concat(backgroundColor)) },
        React__namespace.createElement(Text, { className: "rck-fixed-cta-card__title", renderAs: "h1", variant: "title-1-semibold-responsive" }, title),
        React__namespace.createElement(Text, { className: "rck-fixed-cta-card__description", renderAs: "p", variant: "caption-1-semibold" }, description),
        React__namespace.createElement(Button$1, button)));
};
FixedCtaCard.displayName = 'FixedCtaCard';

var MarketingBanner = function (_a) {
    var _b = _a.backgroundType, backgroundType = _b === void 0 ? 'plain' : _b, backgroundColor = _a.backgroundColor, _c = _a.backgroundShapes, backgroundShapes = _c === void 0 ? false : _c, imageSrc = _a.imageSrc, tag = _a.tag, title = _a.title, description = _a.description, button = _a.button, _d = _a.showCloseIcon, showCloseIcon = _d === void 0 ? false : _d, closeIconColor = _a.closeIconColor, _e = _a.alignment, alignment = _e === void 0 ? 'center' : _e, onClose = _a.onClose;
    return (React__default["default"].createElement("div", { className: classNames('rck-marketing-banner', "rck-marketing-banner--alignment-".concat(alignment), backgroundType === 'gradient'
            ? "rck-marketing-banner--background-gradient-".concat(backgroundColor)
            : "rck-marketing-banner--background-".concat(backgroundColor), backgroundShapes ? "rck-marketing-banner--background-with-shapes" : '') },
        React__default["default"].createElement("div", { className: "rck-marketing-banner__header" }, showCloseIcon && (React__default["default"].createElement("div", { className: classNames('rck-marketing-banner__close-icon', "rck-marketing-banner__close-icon--".concat(closeIconColor)) },
            React__default["default"].createElement(Icon$1, { type: IconType.times, onClick: onClose })))),
        React__default["default"].createElement("div", { className: "rck-marketing-banner__body" },
            imageSrc && (React__default["default"].createElement("img", { src: imageSrc, className: "rck-marketing-banner__image", alt: "marketing banner" })),
            React__default["default"].createElement("div", { className: "rck-marketing-banner__right-content" },
                tag,
                React__default["default"].createElement(Text, { variant: "title-2-semibold-responsive" }, title),
                description && React__default["default"].createElement(Text, { variant: "body-2-regular" }, description),
                React__default["default"].createElement("div", { className: "rck-marketing-banner__button" }, button)))));
};
MarketingBanner.displayName = 'MarketingBanner';

var Portal = /** @class */ (function (_super) {
    __extends(Portal, _super);
    function Portal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // eslint-disable-next-line react/sort-comp
        _this.portalContainer = null;
        _this.portal = document.createElement('div');
        _this.createPortalContainer = function () {
            var name = _this.props.name;
            var portalContainer = document.getElementById(name);
            if (!portalContainer && !isTestEnv()) {
                throw new Error("Remember to add an HTML element with id \"".concat(name, "\" in the index.html file."));
            }
            return portalContainer || document.body;
        };
        return _this;
    }
    Portal.prototype.componentDidMount = function () {
        var _a;
        this.portalContainer = this.createPortalContainer() || null;
        // TODO: review this, it will fail silently. Is that what we want?
        (_a = this.portalContainer) === null || _a === void 0 ? void 0 : _a.appendChild(this.portal);
    };
    Portal.prototype.componentWillUnmount = function () {
        if (this.portalContainer) {
            this.portalContainer.removeChild(this.portal);
        }
    };
    Portal.prototype.render = function () {
        var children = this.props.children;
        return ReactDOM__namespace.createPortal(children, this.portal);
    };
    return Portal;
}(React__namespace.Component));

var flexItemClassNames = function (_a) {
    var _b, _c, _d, _e;
    var order = _a.order, flexBasis = _a.flexBasis, flexGrow = _a.flexGrow, flexShrink = _a.flexShrink, alignSelf = _a.alignSelf, gap = _a.gap;
    var hasProps = (_e = (_d = (_c = (_b = order !== null && order !== void 0 ? order : flexBasis) !== null && _b !== void 0 ? _b : flexGrow) !== null && _c !== void 0 ? _c : flexShrink) !== null && _d !== void 0 ? _d : alignSelf) !== null && _e !== void 0 ? _e : gap;
    if (!hasProps)
        return '';
    return classNames('rck-layout-item', 'rck-flex-layout-item', order ? "rck-flex-layout-item--order-".concat(order) : '', flexBasis
        ? "rck-flex-layout-item--flex-basis-".concat(flexBasis.replace('%', 'pc'))
        : '', flexGrow
        ? "rck-flex-layout-item--flex-grow-".concat(flexGrow.replace('%', 'pc'))
        : '', flexShrink
        ? "rck-flex-layout-item--flex-shrink-".concat(flexShrink.replace('%', 'pc'))
        : '', alignSelf ? "rck-flex-layout-item--flex-alignself-".concat(alignSelf) : '', gap ? "rck-flex-layout-item--gap-".concat(gap) : '');
};
var flexItemClassNames$1 = memoize__default["default"](flexItemClassNames);

var layoutTextClassNames = function (_a) {
    var lineHeight = _a.lineHeight, noWrap = _a.noWrap;
    return classNames(lineHeight ? "rck-layout--line-height-".concat(lineHeight.replace('.', '-')) : '', noWrap ? 'rck-layout--nowrap' : '');
};
var layoutTextClassNames$1 = memoize__default["default"](layoutTextClassNames);

var spacingClassNames = function (_a) {
    var marginLeft = _a.marginLeft, marginBottom = _a.marginBottom, marginTop = _a.marginTop, marginRight = _a.marginRight, paddingLeft = _a.paddingLeft, paddingRight = _a.paddingRight, paddingTop = _a.paddingTop, paddingBottom = _a.paddingBottom, padding = _a.padding, margin = _a.margin, minHeight = _a.minHeight, maxHeight = _a.maxHeight, minWidth = _a.minWidth, maxWidth = _a.maxWidth, height = _a.height, width = _a.width;
    return classNames(padding ? "rck-layout--padding-spacing-".concat(padding) : '', margin ? "rck-layout--margin-spacing-".concat(margin) : '', marginLeft ? "rck-layout--margin-left-spacing-".concat(marginLeft) : '', marginRight ? "rck-layout--margin-right-spacing-".concat(marginRight) : '', marginTop ? "rck-layout--margin-top-spacing-".concat(marginTop) : '', marginBottom ? "rck-layout--margin-bottom-spacing-".concat(marginBottom) : '', paddingLeft ? "rck-layout--padding-left-spacing-".concat(paddingLeft) : '', paddingRight ? "rck-layout--padding-right-spacing-".concat(paddingRight) : '', paddingTop ? "rck-layout--padding-top-spacing-".concat(paddingTop) : '', paddingBottom ? "rck-layout--padding-bottom-spacing-".concat(paddingBottom) : '', minHeight ? "rck-layout--min-height-".concat(minHeight.replace('%', 'pc')) : '', maxHeight ? "rck-layout--max-height-".concat(maxHeight.replace('%', 'pc')) : '', minWidth ? "rck-layout--min-width-".concat(minWidth.replace('%', 'pc')) : "", maxWidth ? "rck-layout--max-width-".concat(maxWidth.replace('%', 'pc')) : '', height ? "rck-layout--height-".concat(height.replace('%', 'pc')) : '', width ? "rck-layout--width-".concat(width.replace('%', 'pc')) : '');
};
var spacingClassNames$1 = memoize__default["default"](spacingClassNames);

var flexLayoutClassNames = function (mainaxis, layoutTextProps, layoutSpacingProps, layoutflexItemProps, wrap, mainaxisAlignment, crossaxisAlignment, crossaxisLineSpacing, matchParentHeight, display, position, hasBox, scrollY, backgroundColor, textAlignement, className) {
    if (position === void 0) { position = 'inherit'; }
    if (className === void 0) { className = ''; }
    return classNames('rck-layout', 'rck-flex-layout', mainaxis ? "rck-flex-layout--main-axis-".concat(mainaxis) : '', wrap ? "rck-flex-layout--wrap-".concat(wrap) : '', mainaxisAlignment
        ? "rck-flex-layout--main-axis-alignment-".concat(mainaxisAlignment)
        : '', crossaxisAlignment
        ? "rck-flex-layout--cross-axis-alignment-".concat(crossaxisAlignment)
        : '', crossaxisLineSpacing
        ? "rck-flex-layout--cross-axis-line-spacing".concat(crossaxisLineSpacing)
        : '', matchParentHeight ? 'rck-flex-layout--match-parent-height' : '', spacingClassNames$1(layoutSpacingProps), flexItemClassNames$1(layoutflexItemProps), layoutTextClassNames$1(layoutTextProps), display ? "rck-flex-layout--".concat(display) : '', position !== 'inherit' ? "rck-layout--position-".concat(position) : '', scrollY ? 'rck-layout--scroll-y' : '', hasBox ? 'rck-layout--box' : '', backgroundColor ? "rck-layout--bck-color-".concat(backgroundColor) : '', textAlignement ? "rck-layout--text-alignement-".concat(textAlignement) : '', className);
};
var flexLayoutClassNames$1 = memoize__default["default"](flexLayoutClassNames);

exports.FlexLayoutTest = void 0;
(function (FlexLayoutTest) {
    FlexLayoutTest["prefix"] = "FlexLayout";
})(exports.FlexLayoutTest || (exports.FlexLayoutTest = {}));
var FlexLayout = React__namespace.forwardRef(function (props, ref) {
    var mainaxis = props.mainaxis, wrap = props.wrap, mainaxisAlignment = props.mainaxisAlignment, crossaxisAlignment = props.crossaxisAlignment, crossaxisLineSpacing = props.crossaxisLineSpacing, marginLeft = props.marginLeft, marginRight = props.marginRight, marginTop = props.marginTop, marginBottom = props.marginBottom, paddingLeft = props.paddingLeft, paddingRight = props.paddingRight, paddingTop = props.paddingTop, paddingBottom = props.paddingBottom, margin = props.margin, padding = props.padding, minHeight = props.minHeight, minWidth = props.minWidth, maxHeight = props.maxHeight, maxWidth = props.maxWidth, width = props.width, height = props.height, noWrap = props.noWrap, lineHeight = props.lineHeight, hasBox = props.hasBox, order = props.order, flexGrow = props.flexGrow, flexShrink = props.flexShrink, flexBasis = props.flexBasis, alignSelf = props.alignSelf, gap = props.gap, _a = props.matchParentHeight, matchParentHeight = _a === void 0 ? false : _a, _b = props.display, display = _b === void 0 ? 'flex' : _b, position = props.position, left = props.left, right = props.right, bottom = props.bottom, top = props.top, scrollY = props.scrollY, backgroundColor = props.backgroundColor, textAlignement = props.textAlignement, _c = props.className, className = _c === void 0 ? '' : _c, children = props.children, testId = props.testId;
    return (React__namespace.createElement("div", { ref: ref, style: {
            left: left,
            right: right,
            top: top,
            bottom: bottom,
        }, className: flexLayoutClassNames$1(mainaxis, { lineHeight: lineHeight, noWrap: noWrap }, {
            marginBottom: marginBottom,
            marginLeft: marginLeft,
            marginRight: marginRight,
            marginTop: marginTop,
            paddingBottom: paddingBottom,
            paddingLeft: paddingLeft,
            paddingRight: paddingRight,
            paddingTop: paddingTop,
            padding: padding,
            margin: margin,
            minHeight: minHeight,
            minWidth: minWidth,
            maxHeight: maxHeight,
            maxWidth: maxWidth,
            width: width,
            height: height,
        }, {
            order: order,
            flexGrow: flexGrow,
            flexShrink: flexShrink,
            flexBasis: flexBasis,
            alignSelf: alignSelf,
            gap: gap,
        }, wrap, mainaxisAlignment, crossaxisAlignment, crossaxisLineSpacing, matchParentHeight, display, position, hasBox, scrollY, backgroundColor, textAlignement, className), "data-testid": getTestId(exports.FlexLayoutTest.prefix, testId) }, children));
});
FlexLayout.displayName = 'FlexLayout';

var getCentered = function (centerX, position) {
    if (!centerX)
        return '';
    return centerX && (position === 'absolute' || position === 'fixed')
        ? 'rck-layout--center-x-absolute'
        : 'rck-layout--center-x';
};
var layoutClassNames = function (layoutTextProps, layoutSpacingProps, display, position, scrollY, backgroundColor, textAlignement, centerX, className) {
    if (position === void 0) { position = 'inherit'; }
    if (className === void 0) { className = ''; }
    return classNames('rck-layout', spacingClassNames$1(layoutSpacingProps), layoutTextClassNames$1(layoutTextProps), display ? "rck-layout--display-".concat(display) : '', position !== 'inherit' ? "rck-layout--position-".concat(position) : '', scrollY ? 'rck-layout--scroll-y' : '', backgroundColor ? "rck-layout--bck-color-".concat(backgroundColor) : '', textAlignement ? "rck-layout--text-alignement-".concat(textAlignement) : '', getCentered(centerX, position), className);
};
var layoutClassNames$1 = memoize__default["default"](layoutClassNames);

exports.LayoutTest = void 0;
(function (LayoutTest) {
    LayoutTest["prefix"] = "Layout";
})(exports.LayoutTest || (exports.LayoutTest = {}));
var Layout = React__namespace.forwardRef(function (props, ref) {
    var display = props.display, position = props.position, left = props.left, right = props.right, top = props.top, bottom = props.bottom, marginLeft = props.marginLeft, marginRight = props.marginRight, marginTop = props.marginTop, marginBottom = props.marginBottom, paddingLeft = props.paddingLeft, paddingRight = props.paddingRight, paddingTop = props.paddingTop, paddingBottom = props.paddingBottom, margin = props.margin, padding = props.padding, minHeight = props.minHeight, minWidth = props.minWidth, maxHeight = props.maxHeight, maxWidth = props.maxWidth, width = props.width, height = props.height, noWrap = props.noWrap, lineHeight = props.lineHeight, scrollY = props.scrollY, centerX = props.centerX, backgroundColor = props.backgroundColor, _a = props.renderAs, Tag = _a === void 0 ? 'div' : _a, textAlignement = props.textAlignement, _b = props.className, className = _b === void 0 ? '' : _b, children = props.children, testId = props.testId;
    return (React__namespace.createElement(Tag, { ref: ref, style: {
            left: left,
            right: right,
            top: top,
            bottom: bottom,
        }, className: layoutClassNames$1({ lineHeight: lineHeight, noWrap: noWrap }, {
            marginBottom: marginBottom,
            marginLeft: marginLeft,
            marginRight: marginRight,
            marginTop: marginTop,
            paddingBottom: paddingBottom,
            paddingLeft: paddingLeft,
            paddingRight: paddingRight,
            paddingTop: paddingTop,
            padding: padding,
            margin: margin,
            minHeight: minHeight,
            minWidth: minWidth,
            maxHeight: maxHeight,
            maxWidth: maxWidth,
            width: width,
            height: height,
        }, display, position, scrollY, backgroundColor, textAlignement, centerX, className), "data-testid": getTestId(exports.LayoutTest.prefix, testId) }, children));
});
Layout.displayName = 'Layout';

/* eslint-disable jsx-a11y/no-static-element-interactions */
exports.PortalModalTest = void 0;
(function (PortalModalTest) {
    PortalModalTest["prefix"] = "PortalModal";
    PortalModalTest["closeButton"] = "Action-close";
    PortalModalTest["backButton"] = "Action-back";
})(exports.PortalModalTest || (exports.PortalModalTest = {}));
var PortalModal = /** @class */ (function (_super) {
    __extends(PortalModal, _super);
    function PortalModal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleClose = function (evt) {
            var onClose = _this.props.onClose;
            _this.stopEventPropagation(evt);
            onClose();
        };
        _this.stopEventPropagation = function (evt) {
            evt.stopPropagation();
        };
        return _this;
    }
    PortalModal.prototype.render = function () {
        var _a = this.props, overlayId = _a.overlayId, _b = _a.overlayClassName, overlayClassName = _b === void 0 ? '' : _b, _c = _a.modalWrapperClassName, modalWrapperClassName = _c === void 0 ? '' : _c, modalWrapperWidth = _a.modalWrapperWidth, modalWrapperHeight = _a.modalWrapperHeight, _d = _a.closeButtonClassName, closeButtonClassName = _d === void 0 ? '' : _d, _e = _a.animationEnabled, animationEnabled = _e === void 0 ? true : _e, _f = _a.hideCloseButton, hideCloseButton = _f === void 0 ? false : _f, _g = _a.showBackButton, showBackButton = _g === void 0 ? false : _g, children = _a.children, testId = _a.testId;
        return (React__namespace.createElement(Portal, { name: PortalModal.portalId },
            React__namespace.createElement(reactRemoveScroll.RemoveScroll, null,
                React__namespace.createElement("div", { id: overlayId, className: "rck-portal-modal__overlay ".concat(overlayClassName), onClick: this.handleClose, "data-testid": getTestId(exports.PortalModalTest.prefix, testId) },
                    React__namespace.createElement("div", { className: "rck-portal-modal__wrapper ".concat(animationEnabled ? 'rck-portal-modal__wrapper--animated' : '', " ").concat(modalWrapperClassName), style: {
                            width: modalWrapperWidth,
                            height: modalWrapperHeight,
                        }, onClick: this.stopEventPropagation, role: "dialog" },
                        React__namespace.createElement(FlexLayout, { mainaxis: "row" },
                            React__namespace.createElement("span", { className: classNames('rck-portal-modal__back-btn', !showBackButton ? 'rck-portal-modal__back-btn--hide' : '') },
                                React__namespace.createElement(Icon$1, { type: IconType.arrowLeft, family: exports.IconFamily.regular, className: classNames(
                                    /* 'rck-portal-modal__back-btn', */
                                    closeButtonClassName || ''), onClick: this.handleClose, ariaLabel: "back", testId: getTestId(exports.PortalModalTest.prefix, testId, exports.PortalModalTest.backButton) })),
                            React__namespace.createElement("span", { className: classNames('rck-portal-modal__close-btn', hideCloseButton ? 'rck-portal-modal__close-btn--hide' : '') },
                                React__namespace.createElement(Icon$1, { type: IconType.times, family: exports.IconFamily.regular, className: classNames(
                                    /* 'rck-portal-modal__close-btn', */
                                    closeButtonClassName || ''), onClick: this.handleClose, ariaLabel: "close", testId: getTestId(exports.PortalModalTest.prefix, testId, exports.PortalModalTest.closeButton) }))),
                        children)))));
    };
    PortalModal.portalId = 'styleguide-react-portal-modal';
    return PortalModal;
}(React__namespace.Component));

var ModalStyledText = function (_a) {
    var _b = _a.textAlign, textAlign = _b === void 0 ? 'center' : _b, _c = _a.marginBottom, marginBottom = _c === void 0 ? '16' : _c, _d = _a.className, className = _d === void 0 ? '' : _d, children = _a.children;
    return (React__default["default"].createElement(Text, { variant: "body-1-regular", renderAs: "p", className: classNames('rck-modal__text', "rck-modal__text--".concat(textAlign), "rck-modal__text--mbottom-".concat(marginBottom), className) }, children));
};

/** @deprecated The close icon should always use the grey color */
exports.ModalCloseIconType = void 0;
(function (ModalCloseIconType) {
    ModalCloseIconType["black"] = "black";
    ModalCloseIconType["white"] = "white";
})(exports.ModalCloseIconType || (exports.ModalCloseIconType = {}));
/** @deprecated Buttons should always be in column */
exports.ModalButtonsAlignment = void 0;
(function (ModalButtonsAlignment) {
    ModalButtonsAlignment["row"] = "row";
    ModalButtonsAlignment["column"] = "column";
})(exports.ModalButtonsAlignment || (exports.ModalButtonsAlignment = {}));
exports.ModalTest = void 0;
(function (ModalTest) {
    ModalTest["prefix"] = "Modal";
    ModalTest["title"] = "Title_content";
    ModalTest["body"] = "Body_content";
})(exports.ModalTest || (exports.ModalTest = {}));
var isBodyText = function (children) {
    return typeof children === 'string';
};
var getDimension = function (value) {
    if (value)
        return "".concat(value, "px");
    return undefined;
};
var Modal = function (_a) {
    var header = _a.header, className = _a.className, size = _a.size, width = _a.width, height = _a.height, _b = _a.type, type = _b === void 0 ? exports.GlobalType.primary : _b, title = _a.title, buttons = _a.buttons, _c = _a.buttonsAlignment, buttonsAlignment = _c === void 0 ? exports.ModalButtonsAlignment.column : _c, animationEnabled = _a.animationEnabled, hideCloseButton = _a.hideCloseButton, testId = _a.testId, isFullScreen = _a.isFullScreen, showBackButton = _a.showBackButton, fixedContent = _a.fixedContent, onClickClose = _a.onClickClose, children = _a.children;
    var renderHeader = header && (React__namespace.createElement("div", { className: "rck-modal__header" },
        header.icon &&
            React__namespace.cloneElement(header.icon, {
                className: classNames('rck-modal__header-icon', header.icon.props.className || ''),
            }),
        header.illustration &&
            React__namespace.cloneElement(header.illustration, {
                className: classNames('rck-modal__header-illustration', header.illustration.props.className
                    ? header.illustration.props.className
                    : ''),
            })));
    var renderButtons = buttons && (React__namespace.createElement("div", { className: classNames(buttonsAlignment === exports.ModalButtonsAlignment.row
            ? 'rck-modal__buttons--row'
            : '', 'rck-modal__buttons') }, // eslint-disable-next-line react/no-array-index-key
    buttons.map(function (button, key) { return React__namespace.cloneElement(button, { key: key }); })));
    var renderBody = (React__namespace.createElement("div", { className: classNames('rck-modal__body', header ? 'rck-modal__body--with-header' : '') },
        title && (React__namespace.createElement(Text, { variant: "title-2-semibold", renderAs: "h1", className: "rck-modal__title", testId: getTestId(exports.ModalTest.prefix, testId, exports.ModalTest.title) }, title)),
        children && (React__namespace.createElement("div", { "data-testid": getTestId(exports.ModalTest.prefix, testId, exports.ModalTest.body), className: "rck-modal__content-scrollable" }, isBodyText(children) ? (React__namespace.createElement(ModalStyledText, null, children)) : (children))),
        fixedContent && (React__namespace.createElement("div", { "data-testid": getTestId(exports.ModalTest.prefix, testId, exports.ModalTest.body, 'fixed-content'), className: "rck-modal__content-fixed" }, fixedContent)),
        renderButtons));
    return (React__namespace.createElement(PortalModal, { closeButtonClassName: "rck-modal__close-btn--negative", onClose: onClickClose, modalWrapperClassName: classNames(isFullScreen ? 'rck-modal--fullscreen' : '', "rck-modal--".concat(size), className), modalWrapperWidth: getDimension(width), modalWrapperHeight: getDimension(height), animationEnabled: animationEnabled, hideCloseButton: hideCloseButton, showBackButton: showBackButton, testId: testId },
        React__namespace.createElement("div", { className: classNames('rck-modal', type ? "rck-modal--".concat(type) : '') },
            renderHeader,
            renderBody)));
};
Modal.displayName = 'Modal';

var getModalInfoButtons = function (buttons, addCancelButton, cancelButtonText, onClickClose) {
    var newButtons = buttons ? __spreadArray([], buttons, true) : [];
    if (addCancelButton) {
        newButtons.push(React__namespace.createElement(Button$1, { key: "cancel_button", buttonType: exports.ButtonType.plain, color: exports.ButtonColor.secondary, size: exports.ButtonSize.medium, block: true, onClick: onClickClose }, cancelButtonText));
    }
    return newButtons;
};
var getAlertBox = function (alertBoxText, alertBoxType) {
    if (alertBoxText) {
        return [
            React__namespace.createElement(AlertBox$1, { key: "modal_info_alert_box", type: alertBoxType !== null && alertBoxType !== void 0 ? alertBoxType : exports.AlertBoxType.info },
                React__namespace.createElement(Text, { variant: "caption-1-semibold" }, alertBoxText)),
        ];
    }
    return undefined;
};
var ModalInfo = function (_a) {
    var header = _a.header, className = _a.className, size = _a.size, title = _a.title, buttons = _a.buttons, animationEnabled = _a.animationEnabled, hideCloseButton = _a.hideCloseButton, testId = _a.testId, showBackButton = _a.showBackButton, onClickClose = _a.onClickClose, children = _a.children, addCancelButton = _a.addCancelButton, cancelButtonText = _a.cancelButtonText, alertBoxText = _a.alertBoxText, alertBoxType = _a.alertBoxType;
    return (React__namespace.createElement(Modal, { size: size, header: header, title: title, buttons: getModalInfoButtons(buttons, addCancelButton, cancelButtonText, onClickClose), animationEnabled: animationEnabled, hideCloseButton: hideCloseButton, testId: testId, showBackButton: showBackButton, fixedContent: getAlertBox(alertBoxText, alertBoxType), className: className, onClickClose: onClickClose }, children));
};
ModalInfo.displayName = 'ModalInfo';

// TODO: We have to review this because the main role of the menu should be "navigation" but we are using
exports.MenuItemTest = void 0;
(function (MenuItemTest) {
    MenuItemTest["prefix"] = "MenuItem";
    MenuItemTest["actionElement"] = "Action-link";
})(exports.MenuItemTest || (exports.MenuItemTest = {}));
var MenuItemContent = function (_a) {
    var menuItem = _a.menuItem, showBadge = _a.showBadge, isLoading = _a.isLoading, isActive = _a.isActive, isHeader = _a.isHeader, expanded = _a.expanded, onClick = _a.onClick, testId = _a.testId;
    return (React__namespace.createElement("li", { key: menuItem.key, className: "rck-menu-item__item-wrapper", "data-testid": getTestId(exports.MenuItemTest.prefix, testId, menuItem.key) },
        React__namespace.createElement("div", { className: classNames('rck-menu-item__item', "rck-menu-item__".concat(menuItem.key), isLoading ? 'rck-menu-item__item--blank' : '', isHeader ? 'rck-menu-item__item--header' : '', isActive ? 'rck-menu-item__item--active' : '', !expanded ? 'rck-menu-item__item--no-expanded' : ''), key: menuItem.key, onClick: isLoading ? undefined : onClick, onKeyDown: isLoading ? undefined : onClick, "data-testid": getTestId(exports.MenuItemTest.prefix, testId, menuItem.key, exports.MenuItemTest.actionElement), tabIndex: isHeader ? undefined : 0, role: isHeader ? undefined : 'button' },
            React__namespace.createElement("span", { className: classNames('rck-menu-item__text', isActive ? 'rck-menu-item__text--active' : '') },
                isHeader && (React__namespace.createElement("strong", null, menuItem.content ? menuItem.content : menuItem.translation)),
                !isHeader &&
                    (menuItem.content ? menuItem.content : menuItem.translation)),
            showBadge && menuItem.isPremium && (React__namespace.createElement(Badge$1, { className: "rck-menu-item__badge", type: menuItem.isPremium ? exports.BadgeType.brand : exports.BadgeType.success, text: menuItem.isPremium ? 'Premium' : '' })))));
};
var MenuItem = function (props) {
    var children = props.children, menuItem = props.menuItem;
    return (React__namespace.createElement(React__namespace.Fragment, null,
        children,
        menuItem.showContent && React__namespace.createElement(MenuItemContent, __assign({}, props))));
};
MenuItem.displayName = 'MenuItem';
var MenuItem$1 = React__namespace.memo(MenuItem);

exports.ScrollableContentTest = void 0;
(function (ScrollableContentTest) {
    ScrollableContentTest["prefix"] = "ScrollableContent";
    ScrollableContentTest["scroll"] = "Scroll";
})(exports.ScrollableContentTest || (exports.ScrollableContentTest = {}));
var ScrollableContent = function (_a) {
    var height = _a.height, hideScrollbar = _a.hideScrollbar, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId, children = _a.children;
    return (React__namespace.createElement("div", { className: classNames('rck-scrollable-content', className), "data-testid": getTestId(exports.ScrollableContentTest.prefix, testId), style: { height: height } },
        React__namespace.createElement("div", { className: classNames('rck-scrollable-content__scroll', hideScrollbar ? 'rck-scrollable-content__scroll--no-scrollbar' : ''), "data-testid": getTestId(exports.ScrollableContentTest.prefix, testId, exports.ScrollableContentTest.scroll) }, children)));
};
ScrollableContent.displayName = 'ScrollableContent';

exports.MenuTest = void 0;
(function (MenuTest) {
    MenuTest["prefix"] = "Menu";
})(exports.MenuTest || (exports.MenuTest = {}));
var Menu = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId, items = _a.items, selectedRule = _a.selectedRule, _c = _a.showBadge, showBadge = _c === void 0 ? true : _c, _d = _a.isDataReady, isDataReady = _d === void 0 ? true : _d, scrollable = _a.scrollable, onClick = _a.onClick, _e = _a.onClickAccordionMenu, onClickAccordionMenu = _e === void 0 ? function () { } : _e, _f = _a.accordionMenuIsVisible, accordionMenuIsVisible = _f === void 0 ? false : _f, profileInfoHeader = _a.profileInfoHeader, expanded = _a.expanded;
    var renderChildren = function (children, isLoading) {
        return children.map(function (item) {
            var isChildrenActive = selectedRule === item.key;
            return (React__namespace.createElement(MenuItem$1, { key: item.key, menuItem: item, onClick: function () { return onClick(item.key); }, showBadge: showBadge, isLoading: isLoading, isActive: isChildrenActive, expanded: expanded, testId: testId }));
        });
    };
    var renderMenu = function () {
        return items.map(function (rule) {
            var isLoading = !isDataReady && rule.needsData;
            var isActive = selectedRule === rule.key;
            if (rule.children) {
                var children = rule.children;
                return (React__namespace.createElement(React__namespace.Fragment, { key: rule.key },
                    React__namespace.createElement(MenuItem$1, { menuItem: rule, onClick: function () { return onClickAccordionMenu(!accordionMenuIsVisible); }, showBadge: showBadge, isLoading: isLoading, isActive: isActive, isHeader: true, expanded: expanded, testId: testId }),
                    React__namespace.createElement("ul", { className: classNames('rck-menu__section', className) }, renderChildren(children, isLoading))));
            }
            return (React__namespace.createElement(MenuItem$1, { key: rule.key, menuItem: rule, onClick: function () { return onClick(rule.key); }, showBadge: showBadge, isLoading: isLoading, isActive: isActive, expanded: expanded, testId: testId }));
        });
    };
    return (React__namespace.createElement("div", { className: classNames('rck-menu', !expanded ? 'rck-menu--no-expanded' : '', className), "datatest-id": getTestId(exports.MenuTest.prefix, testId) },
        profileInfoHeader && (React__namespace.createElement("div", { className: "rck-menu__profile-info-header" }, profileInfoHeader)),
        React__namespace.createElement("ul", { className: "rck-menu__list" },
            scrollable && (React__namespace.createElement(ScrollableContent, { hideScrollbar: true }, renderMenu())),
            !scrollable && renderMenu())));
};
Menu.displayName = 'Menu';

function fallbackMathMedia (query) {
  if (typeof matchMedia !== 'function') {
    return null
  }
  return matchMedia(query)
}

function omitMatchMediaResult (matchMediaResult) {
  if (!matchMediaResult) {
    return null
  }
  return { media: matchMediaResult.media, matches: matchMediaResult.matches }
}

function useMedia (query) {
  var result = React__default["default"].useState(function () {
    return omitMatchMediaResult(fallbackMathMedia(query))
  });
  var setResult = result[1];

  var callback = React__default["default"].useCallback(function (matchMediaResult) {
    return setResult(omitMatchMediaResult(matchMediaResult))
  }, [setResult]);

  React__default["default"].useEffect(
    function () {
      var matchMediaResult = fallbackMathMedia(query);
      callback(matchMediaResult);
      matchMediaResult.addListener(callback);
      return function () { return matchMediaResult.removeListener(callback) }
    },
    [callback, query]
  );

  return result[0]
}

function useMediaPredicate (query) {
  var result = useMedia(query);
  return (result && result.matches) || false
}

var reactMediaHook = {
  useMedia: useMedia,
  useMediaPredicate: useMediaPredicate
};
var reactMediaHook_2 = reactMediaHook.useMediaPredicate;

exports.RibbonType = void 0;
(function (RibbonType) {
    RibbonType[RibbonType["cornerRight"] = 0] = "cornerRight";
    RibbonType[RibbonType["cornerLeft"] = 1] = "cornerLeft";
})(exports.RibbonType || (exports.RibbonType = {}));
exports.RibbonTest = void 0;
(function (RibbonTest) {
    RibbonTest["prefix"] = "Ribbon";
})(exports.RibbonTest || (exports.RibbonTest = {}));
var Ribbon = function (_a) {
    var _b;
    var type = _a.type, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId, children = _a.children;
    var ribbonTypeToString = (_b = {},
        _b[exports.RibbonType.cornerRight] = 'corner-right',
        _b[exports.RibbonType.cornerLeft] = 'corner-left',
        _b)[type];
    return (React__namespace.createElement("div", { className: classNames('rck-ribbon', "rck-ribbon--".concat(ribbonTypeToString), className), "data-testid": getTestId(exports.RibbonTest.prefix, testId) },
        React__namespace.createElement("span", null, children)));
};
Ribbon.displayName = 'Ribbon';

exports.PlanBoxTest = void 0;
(function (PlanBoxTest) {
    PlanBoxTest["prefix"] = "PlanBox";
})(exports.PlanBoxTest || (exports.PlanBoxTest = {}));
var useIsBiggerThanSmallScreen = function () {
    return isTestEnv() ? false : reactMediaHook_2('(min-width: 768px)');
};
var PlanBox = function (_a) {
    var title = _a.title, price = _a.price, years = _a.years, priceAdditionalInformation = _a.priceAdditionalInformation, deviceInformation = _a.deviceInformation, _b = _a.highlighted, highlighted = _b === void 0 ? false : _b, _c = _a.hasDiscount, hasDiscount = _c === void 0 ? false : _c, current = _a.current, currentText = _a.currentText, ribbon = _a.ribbon, button = _a.button, carePlusInfo = _a.carePlusInfo, className = _a.className, testId = _a.testId;
    var biggerThanSmallScreen = useIsBiggerThanSmallScreen();
    var _d = React__namespace.useState(false), showCheckboxHelpInfo = _d[0], setShowCheckboxHelpInfo = _d[1];
    return (React__namespace.createElement("div", { className: classNames("rck-plan-box", current ? "rck-plan-box--current" : '', highlighted ? 'rck-plan-box--highlighted' : '', hasDiscount ? 'rck-plan-box--with-discount' : '', className || ''), "data-testid": getTestId(exports.PlanBoxTest.prefix, testId) },
        (ribbon === null || ribbon === void 0 ? void 0 : ribbon.type) !== undefined && (ribbon === null || ribbon === void 0 ? void 0 : ribbon.text) ? (React__namespace.createElement(Ribbon, { type: ribbon.type }, ribbon.text)) : null,
        React__namespace.createElement(FlexLayout, { mainaxis: "column", display: "contents" },
            React__namespace.createElement(React__namespace.Fragment, null,
                React__namespace.createElement("div", { className: "rck-plan-box__body" },
                    React__namespace.createElement("h2", { className: "rck-plan-box__title" }, title),
                    React__namespace.createElement("div", { className: "rck-plan-box__price rck-plan-box__price-per-year-text" },
                        React__namespace.createElement(FlexLayout, { mainaxis: "row", crossaxisAlignment: "flex-end" },
                            React__namespace.createElement("span", { className: "price" }, price),
                            years ? React__namespace.createElement("span", { className: "price-period" },
                                "/",
                                years) : null)),
                    priceAdditionalInformation && (React__namespace.createElement("div", { className: "rck-plan-box__price-additional-information" }, priceAdditionalInformation))),
                React__namespace.createElement("div", { className: "rck-plan-box__device-information" },
                    React__namespace.createElement(FlexLayout, { mainaxis: "row", crossaxisAlignment: "center" },
                        React__namespace.createElement(FlexLayout, { mainaxis: "row", marginRight: "8" },
                            React__namespace.createElement(Icon$1, { type: IconType.phoneLaptop, size: IconSize.xs })),
                        React__namespace.createElement("span", null, deviceInformation))),
                carePlusInfo && (React__namespace.createElement("div", { className: "rck-plan-box__checkbox-container" },
                    React__namespace.createElement(FlexLayout, { mainaxis: "column" },
                        React__namespace.createElement(FlexLayout, { mainaxis: "row", mainaxisAlignment: "center" },
                            (carePlusInfo === null || carePlusInfo === void 0 ? void 0 : carePlusInfo.carePlusIncluded) ? (carePlusInfo === null || carePlusInfo === void 0 ? void 0 : carePlusInfo.label) : (React__namespace.createElement(Checkbox, { name: "test", label: carePlusInfo === null || carePlusInfo === void 0 ? void 0 : carePlusInfo.label, checked: carePlusInfo === null || carePlusInfo === void 0 ? void 0 : carePlusInfo.isChecked, onClick: carePlusInfo === null || carePlusInfo === void 0 ? void 0 : carePlusInfo.onClick })),
                            React__namespace.createElement(TransparentButton$1, { onClick: function () {
                                    return setShowCheckboxHelpInfo(!showCheckboxHelpInfo);
                                } },
                                React__namespace.createElement(Icon$1, { type: IconType.questionCircle, size: IconSize.regular }))),
                        React__namespace.createElement(AccordionMenu, { isVisible: showCheckboxHelpInfo },
                            React__namespace.createElement("p", { className: "rck-plan-box__checkbox-description" }, carePlusInfo === null || carePlusInfo === void 0 ? void 0 : carePlusInfo.description),
                            carePlusInfo.list))))),
            current ? (React__namespace.createElement("div", { className: "rck-plan-box__current-text" }, currentText)) : (React__namespace.createElement(Button$1, { loading: button.isLoading, disabled: button.isLoading, onClick: button.onClick, size: biggerThanSmallScreen ? exports.ButtonSize.big : exports.ButtonSize.medium, testId: testId, color: highlighted ? exports.ButtonColor.white : exports.ButtonColor.secondary, block: true }, button.text)))));
};
PlanBox.displayName = 'PlanBox';

var useElementDisable = function (el, disable, disableClass, overlapClass) {
    var createOverlap = function () {
        var div = document.createElement('div');
        div.className = overlapClass;
        return div;
    };
    React.useEffect(function () {
        if (el && el.current) {
            if (disable && !el.current.classList.contains(disableClass)) {
                el.current.classList.add(disableClass);
                if (el.current.prepend) {
                    el.current.prepend(createOverlap());
                }
            }
            if (!disable && el.current.classList.contains(disableClass)) {
                el.current.classList.remove(disableClass);
                var overlap = el.current.querySelector(".".concat(overlapClass));
                if (overlap) {
                    el.current.removeChild(overlap);
                }
            }
        }
    }, [disable]);
};

var IconAdapter = function (_a) {
    var icon = _a.icon, className = _a.className, altText = _a.altText;
    if (typeof icon === 'string') {
        return (React__namespace.createElement("img", { className: className, src: icon, role: "presentation", alt: altText, referrerPolicy: "no-referrer" }));
    }
    return icon;
};
IconAdapter.displayName = 'IconAdapter';
var IconAdapter$1 = React__namespace.memo(IconAdapter);

exports.ListCompactRightSubtitleTest = void 0;
(function (ListCompactRightSubtitleTest) {
    ListCompactRightSubtitleTest["prefix"] = "ListCompactRightSubtitle";
})(exports.ListCompactRightSubtitleTest || (exports.ListCompactRightSubtitleTest = {}));
var ListCompactRightSubtitle = function (_a) {
    var testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children;
    return (React__default["default"].createElement("p", { className: classNames('rck-list-compact-right-subtitle', className), "data-testid": testId }, children));
};
ListCompactRightSubtitle.displayName = 'ListCompactRightSubtitle';
var ListCompactRightSubtitle$1 = React__default["default"].memo(ListCompactRightSubtitle);

exports.ListCompactSubtitleTest = void 0;
(function (ListCompactSubtitleTest) {
    ListCompactSubtitleTest["prefix"] = "ListCompactSubtitle";
})(exports.ListCompactSubtitleTest || (exports.ListCompactSubtitleTest = {}));
var ListCompactSubtitle = function (_a) {
    var testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children;
    return (React__default["default"].createElement("p", { className: classNames('rck-list-compact-subtitle', className), "data-testid": testId }, children));
};
ListCompactSubtitle.displayName = 'ListCompactSubtitle';
var ListCompactSubtitle$1 = React__default["default"].memo(ListCompactSubtitle);

exports.ListCompactTitleTest = void 0;
(function (ListCompactTitleTest) {
    ListCompactTitleTest["prefix"] = "ListCompactTitle";
})(exports.ListCompactTitleTest || (exports.ListCompactTitleTest = {}));
var ListCompactTitle = function (_a) {
    var testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children;
    return (React__default["default"].createElement("p", { className: classNames('rck-list-compact-title', className), "data-testid": testId }, children));
};
ListCompactTitle.displayName = 'ListCompactTitle';
var ListCompactTitle$1 = React__default["default"].memo(ListCompactTitle);

exports.ClickableListItemTest = void 0;
(function (ClickableListItemTest) {
    ClickableListItemTest["prefix"] = "ClickableListItemTest";
})(exports.ClickableListItemTest || (exports.ClickableListItemTest = {}));
var ClickableListItem = function (_a) {
    var onClick = _a.onClick, role = _a.role, testId = _a.testId, useAsLink = _a.useAsLink, href = _a.href, children = _a.children, _b = _a.className, className = _b === void 0 ? '' : _b;
    return !href && onClick ? (React__default["default"].createElement(TransparentButton$1, { onClick: onClick, className: classNames('rck-list-item--clickable', className), testId: getTestId(exports.ClickableListItemTest.prefix, testId) }, children)) : (React__default["default"].createElement(Link$1, { className: classNames('rck-list-item--clickable', className), useAsLink: useAsLink, role: role, href: href, onClick: onClick, testId: getTestId(exports.ClickableListItemTest.prefix, testId) }, children));
};
ClickableListItem.displayName = 'ClickableListItem';

exports.CompactStyleListItemTest = void 0;
(function (CompactStyleListItemTest) {
    CompactStyleListItemTest["prefix"] = "ListItem";
    CompactStyleListItemTest["title"] = "Title-content";
    CompactStyleListItemTest["rightTitle"] = "Title-right-content";
    CompactStyleListItemTest["subtitle"] = "Subtitle-content";
    CompactStyleListItemTest["rightSubtitle"] = "Subtitle-right-content";
})(exports.CompactStyleListItemTest || (exports.CompactStyleListItemTest = {}));
var CompactStyleListItem = function (_a) {
    var className = _a.className, title = _a.title, subtitle = _a.subtitle, subtitleClassName = _a.subtitleClassName, rightTitle = _a.rightTitle, rightSubtitle = _a.rightSubtitle, icon = _a.icon, iconAltText = _a.iconAltText, _b = _a.bottomPadding, bottomPadding = _b === void 0 ? 'compact' : _b, _c = _a.disabled, disabled = _c === void 0 ? false : _c, testId = _a.testId, parentTestId = _a.parentTestId, onInfoActionClick = _a.onInfoActionClick;
    var listItemRef = React.useRef(document.createElement('li'));
    useElementDisable(listItemRef, disabled, 'rck-list-item--disabled', 'rck-list-item__overlap');
    var leftContent = (React__default["default"].createElement(React__default["default"].Fragment, null,
        icon && (React__default["default"].createElement("div", { className: "rck-compact-list-item__icon-wrapper" },
            React__default["default"].createElement(IconAdapter$1, { icon: icon, altText: iconAltText, className: "rck-compact-list-item__icon" }))),
        React__default["default"].createElement("div", { className: "rck-compact-list-item__left-content" },
            React__default["default"].createElement("div", { className: "rck-compact-list-item__left-title" },
                title &&
                    (typeof title === 'string' ? (React__default["default"].createElement(ListCompactTitle$1, { className: classNames('rck-compact-list-item__title'), testId: getTestId(exports.CompactStyleListItemTest.title, getTestIdWithParentTestId(parentTestId, testId)) }, title)) : (React__default["default"].cloneElement(title, {
                        className: classNames('rck-compact-list-item__title'),
                        testId: getTestId(exports.CompactStyleListItemTest.title, getTestIdWithParentTestId(parentTestId, testId)),
                    }))),
                onInfoActionClick ? (React__default["default"].createElement(Icon$1, { type: IconType.infoCircle, color: disabled ? exports.IconColor.disabled : exports.IconColor.secondary, className: "rck-compact-list-item__action-icon" })) : null),
            subtitle &&
                (typeof subtitle === 'string' ? (React__default["default"].createElement(ListCompactSubtitle$1, { className: classNames('rck-compact-list-item__subtitle', subtitleClassName || ''), testId: getTestId(exports.CompactStyleListItemTest.subtitle, getTestIdWithParentTestId(parentTestId, testId)) }, subtitle)) : (React__default["default"].cloneElement(subtitle, {
                    className: classNames('rck-compact-list-item__subtitle', subtitleClassName || ''),
                    testId: getTestId(exports.CompactStyleListItemTest.subtitle, getTestIdWithParentTestId(parentTestId, testId)),
                }))))));
    return (React__default["default"].createElement("li", { className: classNames('rck-list-item', 'rck-compact-list-item', "rck-compact-list-item--padding-b-".concat(bottomPadding), className || ''), "data-testid": getTestId(exports.CompactStyleListItemTest.prefix, getTestIdWithParentTestId(parentTestId, testId)), ref: listItemRef },
        React__default["default"].createElement("div", { className: "rck-compact-list-item__content" },
            onInfoActionClick ? (React__default["default"].createElement(ClickableListItem, { onClick: onInfoActionClick, className: "rck-compact-list-item__info-button" }, leftContent)) : (leftContent),
            React__default["default"].createElement("div", { className: "rck-compact-list-item__right-content" },
                rightTitle &&
                    (typeof rightTitle === 'string' ? (React__default["default"].createElement(ListCompactRightSubtitle$1, { className: "rck-compact-list-item__right-title", testId: getTestId(exports.CompactStyleListItemTest.rightTitle, getTestIdWithParentTestId(parentTestId, testId)) }, rightTitle)) : (React__default["default"].cloneElement(rightTitle, {
                        className: 'rck-compact-list-item__right-title',
                        testId: getTestId(exports.CompactStyleListItemTest.rightTitle, getTestIdWithParentTestId(parentTestId, testId)),
                    }))),
                rightSubtitle &&
                    (typeof rightSubtitle === 'string' ? (React__default["default"].createElement(ListCompactRightSubtitle$1, { className: "rck-compact-list-item__right-subtitle", testId: getTestId(exports.CompactStyleListItemTest.rightSubtitle, getTestIdWithParentTestId(parentTestId, testId)) }, rightSubtitle)) : (React__default["default"].cloneElement(rightSubtitle, {
                        className: 'rck-compact-list-item__right-subtitle',
                        testId: getTestId(exports.CompactStyleListItemTest.rightSubtitle, getTestIdWithParentTestId(parentTestId, testId)),
                    })))))));
};
CompactStyleListItem.displayName = 'CompactStyleListItem';

exports.ListTitleTest = void 0;
(function (ListTitleTest) {
    ListTitleTest["prefix"] = "ListTitle";
})(exports.ListTitleTest || (exports.ListTitleTest = {}));
var ListTitle = function (_a) {
    var _b = _a.align, align = _b === void 0 ? TextAlign.left : _b, allowLines = _a.allowLines, children = _a.children, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId, parentTestId = _a.parentTestId, _d = _a.isBold, isBold = _d === void 0 ? true : _d, _e = _a.renderAs, Tag = _e === void 0 ? 'p' : _e;
    return (React__default["default"].createElement(Tag, { className: classNames('rck-list-title', "rck-list-title--align-".concat(align), allowLines ? "rck-list-title--ellipsis-for-".concat(allowLines) : '', className), "data-testid": getTestId(exports.ListTitleTest.prefix, getTestIdWithParentTestId(parentTestId, testId)) }, isBold ? React__default["default"].createElement("strong", null, children) : children));
};
ListTitle.displayName = 'ListTitle';
var ListTitle$1 = React__default["default"].memo(ListTitle);

exports.CustomStyleListItemTest = void 0;
(function (CustomStyleListItemTest) {
    CustomStyleListItemTest["prefix"] = "ListItem";
})(exports.CustomStyleListItemTest || (exports.CustomStyleListItemTest = {}));
var CustomStyleListItem = function (_a) {
    var className = _a.className, title = _a.title, icon = _a.icon, iconAltText = _a.iconAltText, children = _a.children, rightContent = _a.rightContent, footerContent = _a.footerContent, showSeparator = _a.showSeparator, _b = _a.disabled, disabled = _b === void 0 ? false : _b, testId = _a.testId, parentTestId = _a.parentTestId, onInfoActionClick = _a.onInfoActionClick;
    var listItemRef = React.useRef(document.createElement('li'));
    useElementDisable(listItemRef, disabled, 'rck-list-item--disabled', 'rck-list-item__overlap');
    var leftContent = (React__default["default"].createElement(React__default["default"].Fragment, null,
        React__default["default"].createElement("div", { className: "rck-custom-list-item__body__left" },
            React__default["default"].createElement("div", { className: "rck-custom-list-item__body__left__icon" },
                React__default["default"].createElement(IconAdapter$1, { icon: icon, altText: iconAltText }))),
        React__default["default"].createElement("div", { className: "rck-custom-list-item__body__middle-left" },
            React__default["default"].createElement("div", { className: "rck-custom-list-item__body__middle-left__title-container" },
                typeof title === 'string' ? (React__default["default"].createElement(ListTitle$1, { className: "rck-custom-list-item__body__middle-left__title", testId: testId, parentTestId: parentTestId }, title)) : (React__default["default"].cloneElement(title, {
                    className: 'rck-custom-list-item__body__middle-left__title',
                })),
                onInfoActionClick ? (React__default["default"].createElement(Icon$1, { type: IconType.infoCircle, color: exports.IconColor.secondary, className: "rck-custom-list-item__body__middle-left__action-icon" })) : null),
            React__default["default"].createElement("div", { className: "rck-custom-list-item__body__middle-left__content" }, children))));
    return (React__default["default"].createElement("li", { className: classNames('rck-list-item', 'rck-custom-list-item', showSeparator ? '' : 'rck-custom-list-item--no-border', className || ''), "data-testid": getTestId(exports.CustomStyleListItemTest.prefix, getTestIdWithParentTestId(parentTestId, testId)), ref: listItemRef },
        React__default["default"].createElement("div", { className: "rck-custom-list-item__body" },
            onInfoActionClick ? (React__default["default"].createElement(ClickableListItem, { onClick: onInfoActionClick, className: "rck-custom-list-item__info-button" }, leftContent)) : (leftContent),
            rightContent && (React__default["default"].createElement("div", { className: "rck-custom-list-item__body__right" }, rightContent))),
        footerContent && (React__default["default"].createElement("div", { className: "rck-custom-list-item__footer" }, footerContent))));
};
CustomStyleListItem.displayName = 'CustomStyleListItem';

exports.FreeStyleListItemTest = void 0;
(function (FreeStyleListItemTest) {
    FreeStyleListItemTest["prefix"] = "ListItem";
})(exports.FreeStyleListItemTest || (exports.FreeStyleListItemTest = {}));
var FreeStyleListItem = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children, testId = _a.testId, _c = _a.disabled, disabled = _c === void 0 ? false : _c, _d = _a.noBorder, noBorder = _d === void 0 ? false : _d, parentTestId = _a.parentTestId;
    var listItemRef = React.useRef(document.createElement('li'));
    useElementDisable(listItemRef, disabled, 'rck-list-item--disabled', 'rck-list-item__overlap');
    return (React__default["default"].createElement("li", { className: classNames("rck-list-item", 'rck-free-list-item', noBorder ? 'rck-list-item__no-border' : '', className), "data-testid": getTestId(exports.FreeStyleListItemTest.prefix, getTestIdWithParentTestId(parentTestId, testId)), ref: listItemRef }, children));
};
FreeStyleListItem.displayName = 'FreeStyleListItem';

exports.ListSubtitleTest = void 0;
(function (ListSubtitleTest) {
    ListSubtitleTest["prefix"] = "ListSubtitle";
})(exports.ListSubtitleTest || (exports.ListSubtitleTest = {}));
var ListSubtitle = function (_a) {
    var _b = _a.align, align = _b === void 0 ? TextAlign.left : _b, _c = _a.fontSize, fontSize = _c === void 0 ? 's' : _c, _d = _a.color, color = _d === void 0 ? 'neutral' : _d, allowLines = _a.allowLines, _e = _a.renderAs, Tag = _e === void 0 ? 'p' : _e, children = _a.children, _f = _a.className, className = _f === void 0 ? '' : _f, testId = _a.testId, parentTestId = _a.parentTestId;
    return (React__default["default"].createElement(Tag, { className: classNames("rck-list-subtitle", "rck-list-subtitle--align-".concat(align), "rck-list-subtitle--fsize-".concat(fontSize), allowLines ? "rck-list-subtitle--ellipsis-for-".concat(allowLines) : '', "rck-list-subtitle--".concat(color), className), "data-testid": getTestId(exports.ListSubtitleTest.prefix, getTestIdWithParentTestId(parentTestId, testId)) }, children));
};
ListSubtitle.displayName = 'ListSubtitle';
var ListSubtitle$1 = React__default["default"].memo(ListSubtitle);

exports.LocationStyleListItemTest = void 0;
(function (LocationStyleListItemTest) {
    LocationStyleListItemTest["prefix"] = "ListItem";
})(exports.LocationStyleListItemTest || (exports.LocationStyleListItemTest = {}));
var LocationStyleListItem = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, leftSubtitle = _a.leftSubtitle, upperTitle = _a.upperTitle, lowerTitle = _a.lowerTitle, _c = _a.wideLeftSubtitle, wideLeftSubtitle = _c === void 0 ? false : _c, showActions = _a.showActions, actions = _a.actions, children = _a.children, testId = _a.testId, _d = _a.disabled, disabled = _d === void 0 ? false : _d, parentTestId = _a.parentTestId;
    var listItemRef = React.useRef(document.createElement('li'));
    useElementDisable(listItemRef, disabled, 'rck-list-item--disabled', 'rck-list-item__overlap');
    return (React__default["default"].createElement("li", { className: classNames("rck-list-item", 'rck-location-list-item', wideLeftSubtitle ? 'rck-location-list-item--wide' : '', className), "data-testid": getTestId(exports.LocationStyleListItemTest.prefix, getTestIdWithParentTestId(parentTestId, testId)), ref: listItemRef },
        React__default["default"].createElement("div", { className: "rck-location-list-item__left" },
            React__default["default"].createElement("div", { className: "rck-location-list-item__left__text" }, typeof leftSubtitle === 'string' ? (React__default["default"].createElement(ListSubtitle$1, { align: TextAlign.right, fontSize: "medium", testId: testId, parentTestId: parentTestId }, leftSubtitle)) : (React__default["default"].cloneElement(leftSubtitle, {
                align: TextAlign.right,
                fontSize: 'medium',
            }))),
            React__default["default"].createElement("div", { className: "rck-location-list-item__left__oval" })),
        React__default["default"].createElement("div", { className: "rck-location-list-item__middle-left" },
            !children &&
                upperTitle &&
                (typeof upperTitle === 'string' ? (React__default["default"].createElement(ListTitle$1, { className: "rck-location-list__middle-left__text", testId: testId, parentTestId: parentTestId }, upperTitle)) : (React__default["default"].cloneElement(upperTitle, {
                    className: 'rck-location-list__middle-left__text',
                }))),
            !children &&
                lowerTitle &&
                (typeof lowerTitle === 'string' ? (React__default["default"].createElement(ListTitle$1, { className: "rck-location-list__middle-left__text", testId: testId, parentTestId: parentTestId }, lowerTitle)) : (React__default["default"].cloneElement(lowerTitle, {
                    className: 'rck-location-list__middle-left__text',
                }))),
            React__default["default"].createElement("div", { className: classNames('rck-location-list-item__actions', showActions ? 'rck-location-list-item__actions--visible' : '') }, !children && actions),
            children)));
};
LocationStyleListItem.displayName = 'LocationStyleListItem';

exports.ListItemVerticalAlign = void 0;
(function (ListItemVerticalAlign) {
    ListItemVerticalAlign["top"] = "top";
    ListItemVerticalAlign["center"] = "center";
    ListItemVerticalAlign["bottom"] = "bottom";
})(exports.ListItemVerticalAlign || (exports.ListItemVerticalAlign = {}));
exports.ListItemTopPadding = void 0;
(function (ListItemTopPadding) {
    ListItemTopPadding["none"] = "padding-none";
    ListItemTopPadding["short"] = "padding-s";
    ListItemTopPadding["medium"] = "padding-m";
    ListItemTopPadding["large"] = "padding-l";
})(exports.ListItemTopPadding || (exports.ListItemTopPadding = {}));

exports.RegularStyleListItemTest = void 0;
(function (RegularStyleListItemTest) {
    RegularStyleListItemTest["prefix"] = "ListItem";
    RegularStyleListItemTest["actionElement"] = "Action-element";
})(exports.RegularStyleListItemTest || (exports.RegularStyleListItemTest = {}));
var RegularStyleListItem = function (_a) {
    var icon = _a.icon, iconAltText = _a.iconAltText, _b = _a.iconVerticalAlign, iconVerticalAlign = _b === void 0 ? exports.ListItemVerticalAlign.center : _b, title = _a.title, upperSubtitle = _a.upperSubtitle, lowerSubtitle = _a.lowerSubtitle, centerMiddleContent = _a.centerMiddleContent, _c = _a.className, className = _c === void 0 ? '' : _c, actionElement = _a.actionElement, _d = _a.actionElementVerticalAlign, actionElementVerticalAlign = _d === void 0 ? exports.ListItemVerticalAlign.center : _d, testId = _a.testId, _e = _a.disabled, disabled = _e === void 0 ? false : _e, parentTestId = _a.parentTestId;
    var listItemRef = React.useRef(document.createElement('li'));
    useElementDisable(listItemRef, disabled, 'rck-list-item--disabled', 'rck-list-item__overlap');
    return (React__default["default"].createElement("li", { className: classNames('rck-list-item', "rck-regular-list-item", className), "data-testid": getTestId(exports.RegularStyleListItemTest.prefix, getTestIdWithParentTestId(parentTestId, testId)), ref: listItemRef },
        icon && (React__default["default"].createElement("div", { className: classNames('rck-regular-list-item__left', "rck-regular-list-item__left--".concat(iconVerticalAlign)) },
            React__default["default"].createElement("div", { className: "rck-regular-list-item__left__icon" },
                React__default["default"].createElement(IconAdapter$1, { icon: icon, altText: iconAltText })))),
        React__default["default"].createElement("div", { className: classNames('rck-regular-list-item__middle-left', centerMiddleContent
                ? 'rck-regular-list-item__middle-left--centered'
                : '') },
            title &&
                (typeof title === 'string' ? (React__default["default"].createElement(ListTitle$1, { className: "rck-regular-list-item__middle-left__title", testId: testId, parentTestId: parentTestId }, title)) : (React__default["default"].cloneElement(title, {
                    className: 'rck-regular-list-item__middle-left__title',
                    isBold: false,
                }))),
            upperSubtitle &&
                (typeof upperSubtitle === 'string' ? (React__default["default"].createElement(ListSubtitle$1, { className: "rck-regular-list-item__middle-left__subtitle", testId: testId, parentTestId: parentTestId }, upperSubtitle)) : (React__default["default"].cloneElement(upperSubtitle, {
                    className: 'rck-regular-list-item__middle-left__subtitle',
                }))),
            lowerSubtitle &&
                (typeof lowerSubtitle === 'string' ? (React__default["default"].createElement(ListSubtitle$1, { className: "rck-regular-list-item__middle-left__subtitle", testId: testId, parentTestId: parentTestId }, lowerSubtitle)) : (React__default["default"].cloneElement(lowerSubtitle, {
                    className: 'rck-regular-list-item__middle-left__subtitle',
                })))),
        actionElement && (React__default["default"].createElement("div", { className: classNames('rck-regular-list-item__right', "rck-regular-list-item__right--".concat(actionElementVerticalAlign)) },
            React__default["default"].createElement("div", { className: "rck-regular-list-item__right__action-element", "data-testid": getTestId(exports.RegularStyleListItemTest.prefix, getTestIdWithParentTestId(parentTestId, testId), exports.RegularStyleListItemTest.actionElement) }, actionElement)))));
};
RegularStyleListItem.displayName = 'RegularStyleListItem';

exports.SelectableListItemTest = void 0;
(function (SelectableListItemTest) {
    SelectableListItemTest["prefix"] = "ListItem";
})(exports.SelectableListItemTest || (exports.SelectableListItemTest = {}));
var getCheckbox = function (selected) {
    return selected ? (React__namespace.createElement(Icon$1, { type: IconType.checkCircle, color: exports.IconColor.secondary, family: exports.IconFamily.solid })) : (React__namespace.createElement(Icon$1, { type: IconType.circle, color: exports.IconColor.secondary }));
};
var getRadio = function (selected) {
    return selected ? (React__namespace.createElement(Icon$1, { type: IconType.circleDot, color: exports.IconColor.secondary })) : (React__namespace.createElement(Icon$1, { type: IconType.circle, color: exports.IconColor.black }));
};
var SelectableListItem = function (_a) {
    var id = _a.id, className = _a.className, _b = _a.type, type = _b === void 0 ? 'checkbox' : _b, title = _a.title, subtitle = _a.subtitle, description = _a.description, _c = _a.isDisabled, isDisabled = _c === void 0 ? false : _c, _d = _a.selected, selected = _d === void 0 ? false : _d, _e = _a.boldTitleOnSelected, boldTitleOnSelected = _e === void 0 ? false : _e, icon = _a.icon, iconAltText = _a.iconAltText, testId = _a.testId, parentTestId = _a.parentTestId, onClick = _a.onClick, onInfoActionClick = _a.onInfoActionClick;
    var listItemRef = React__namespace.useRef(document.createElement('li'));
    useElementDisable(listItemRef, isDisabled, 'rck-list-item--disabled', 'rck-list-item__overlap');
    var leftContent = (React__namespace.createElement(React__namespace.Fragment, null,
        icon && (React__namespace.createElement("div", { className: "rck-selectable-list-item__icon" },
            React__namespace.createElement(IconAdapter$1, { icon: icon, altText: iconAltText }))),
        React__namespace.createElement(ListTitle$1, { className: "rck-selectable-list-item__title", testId: testId, parentTestId: parentTestId, isBold: !boldTitleOnSelected || (boldTitleOnSelected && selected), allowLines: 1 }, title),
        onInfoActionClick ? (React__namespace.createElement(Icon$1, { type: IconType.infoCircle, color: isDisabled ? exports.IconColor.disabled : exports.IconColor.secondary, className: "rck-selectable-list-item__action-icon" })) : null,
        subtitle ? (React__namespace.createElement("div", { className: "rck-selectable-list-item__subtitle" }, subtitle)) : null));
    var onInfoClick = function (e) {
        e.stopPropagation();
        if (!onInfoActionClick)
            return;
        onInfoActionClick();
    };
    var getLeftClassNames = function () {
        return classNames('rck-selectable-list-item__left', subtitle ? 'rck-selectable-list-item__left-subtitle' : '');
    };
    return (React__namespace.createElement("li", { 
        // eslint-disable-next-line jsx-a11y/no-noninteractive-element-to-interactive-role
        role: type, 
        // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
        tabIndex: 0, ref: listItemRef, "aria-checked": selected, "aria-disabled": isDisabled, className: classNames('rck-list-item', 'rck-list-item__no-border', 'rck-selectable-list-item', "rck-selectable-list-item--".concat(type), selected ? 'rck-selectable-list-item--selected' : '', subtitle ? 'rck-selectable-list-item--has-subtitle' : '', className || ''), "data-testid": getTestId(exports.SelectableListItemTest.prefix, getTestIdWithParentTestId(parentTestId, testId)), onClick: !isDisabled ? function (e) { return onClick === null || onClick === void 0 ? void 0 : onClick(id, e); } : undefined, onKeyDown: !isDisabled ? handleKeyboardSelection(function (e) { return onClick === null || onClick === void 0 ? void 0 : onClick(id, e); }) : undefined },
        React__namespace.createElement("div", { className: "rck-selectable-list-item__body" },
            onInfoActionClick ? (React__namespace.createElement(ClickableListItem, { onClick: onInfoClick, className: getLeftClassNames() }, leftContent)) : (React__namespace.createElement("div", { className: getLeftClassNames() }, leftContent)),
            React__namespace.createElement("div", { className: "rck-selectable-list-item__right" },
                type === 'checkbox' && getCheckbox(selected),
                type === 'radio' && getRadio(selected)),
            description ? (React__namespace.createElement("div", { className: "rck-selectable-list-item__footer" },
                React__namespace.createElement("div", { className: "rck-selectable-list-item__description" }, typeof description === 'string' ? (React__namespace.createElement(Text, { color: "grey-500", variant: "body-1-regular" }, description)) : (description)))) : null)));
};
SelectableListItem.displayName = 'SelectableListItem';

var listClassNames = function (showSeparator, listItemPaddingTop, listItemPaddingBottom, listItemPaddingRight, listItemPaddingLeft, listItemMarginTop, listItemMarginBottom, columns, horizontalOrientation, allowWrapping, useNativeColumns, className) {
    if (className === void 0) { className = ''; }
    return classNames('rck-list', showSeparator ? '' : "rck-list--no-separator", listItemPaddingTop
        ? "rck-list--item-top-padding-spacing-".concat(listItemPaddingTop)
        : '', listItemPaddingBottom
        ? "rck-list--item-bottom-padding-spacing-".concat(listItemPaddingBottom)
        : '', listItemPaddingRight
        ? "rck-list--item-right-padding-spacing-".concat(listItemPaddingRight)
        : '', listItemPaddingLeft
        ? "rck-list--item-left-padding-spacing-".concat(listItemPaddingLeft)
        : '', listItemMarginTop
        ? "rck-list--item-top-margin-spacing-".concat(listItemMarginTop)
        : '', listItemMarginBottom
        ? "rck-list--item-bottom-margin-spacing-".concat(listItemMarginBottom)
        : '', "rck-list--columns-".concat(columns), horizontalOrientation ? 'rck-list--horizontal' : '', allowWrapping ? 'rck-list--wrapped' : '', useNativeColumns ? '' : 'rck-list--no-native-columns', className || '');
};
var listClassNames$1 = memoize__default["default"](listClassNames);

exports.ListTest = void 0;
(function (ListTest) {
    ListTest["prefix"] = "List";
})(exports.ListTest || (exports.ListTest = {}));
var List = function (_a) {
    var _b = _a.showSeparator, showSeparator = _b === void 0 ? true : _b, _c = _a.listItemPaddingTop, listItemPaddingTop = _c === void 0 ? 'small' : _c, listItemPaddingBottom = _a.listItemPaddingBottom, listItemPaddingRight = _a.listItemPaddingRight, listItemPaddingLeft = _a.listItemPaddingLeft, listItemMarginTop = _a.listItemMarginTop, listItemMarginBottom = _a.listItemMarginBottom, allowCustomListItemStyle = _a.allowCustomListItemStyle, _d = _a.columns, columns = _d === void 0 ? '1' : _d, horizontalOrientation = _a.horizontalOrientation, allowWrapping = _a.allowWrapping, _e = _a.useNativeColumns, useNativeColumns = _e === void 0 ? true : _e, _f = _a.className, className = _f === void 0 ? '' : _f, testId = _a.testId, _g = _a.disabled, disabled = _g === void 0 ? false : _g, children = _a.children, ariaRole = _a.ariaRole, ariaLabelledBy = _a.ariaLabelledBy, onClickItem = _a.onClickItem;
    return (React__namespace.createElement("ul", { className: listClassNames$1(showSeparator, listItemPaddingTop, listItemPaddingBottom, listItemPaddingRight, listItemPaddingLeft, listItemMarginTop, listItemMarginBottom, columns, horizontalOrientation, allowWrapping, useNativeColumns, className), "data-testid": getTestId(exports.ListTest.prefix, testId), role: ariaRole, "aria-labelledby": ariaLabelledBy }, React__namespace.Children.map(children, function (child) {
        var listItem = child;
        if (listItem === null) {
            return null;
        }
        if ((allowCustomListItemStyle &&
            listItem.type !== allowCustomListItemStyle) ||
            (!allowCustomListItemStyle &&
                listItem.type !== CompactStyleListItem &&
                listItem.type !== FreeStyleListItem &&
                listItem.type !== RegularStyleListItem &&
                listItem.type !== LocationStyleListItem &&
                listItem.type !== CustomStyleListItem &&
                listItem.type !== SelectableListItem)) {
            throw new Error("List children have an invalid type: ".concat(listItem.type));
        }
        return React__namespace.cloneElement(listItem, __assign({ disabled: disabled, parentTestId: testId }, (onClickItem && { onClick: onClickItem })));
    })));
};
List.displayName = 'List';

exports.ListLinkTest = void 0;
(function (ListLinkTest) {
    ListLinkTest["prefix"] = "ListLink";
})(exports.ListLinkTest || (exports.ListLinkTest = {}));
var ListLink = function (_a) {
    var _b = _a.url, url = _b === void 0 ? '' : _b, useAsLink = _a.useAsLink, _c = _a.fontSize, fontSize = _c === void 0 ? 'small' : _c, _d = _a.color, color = _d === void 0 ? 'regular' : _d, isBold = _a.isBold, _e = _a.className, className = _e === void 0 ? '' : _e, testId = _a.testId, parentTestId = _a.parentTestId, onClick = _a.onClick, children = _a.children;
    return (React__default["default"].createElement(Link$1, { useAsLink: useAsLink, href: url, className: classNames('rck-list-link', "rck-list-link--fsize-".concat(fontSize), "rck-list-link--".concat(color), className), onClick: onClick, testId: getTestId(exports.ListLinkTest.prefix, getTestIdWithParentTestId(parentTestId, testId)) }, isBold ? React__default["default"].createElement("strong", null, children) : children));
};
ListLink.displayName = 'ListLink';
var ListLink$1 = React__default["default"].memo(ListLink);

exports.ListThumbnailTest = void 0;
(function (ListThumbnailTest) {
    ListThumbnailTest["prefix"] = "ListThumbnail";
})(exports.ListThumbnailTest || (exports.ListThumbnailTest = {}));
var ListThumbnail = function (_a) {
    var thumbnailUrl = _a.thumbnailUrl, altTextThumnail = _a.altTextThumnail, imageMinHeight = _a.imageMinHeight, imageMinWidth = _a.imageMinWidth, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId, parentTestId = _a.parentTestId;
    return (React__default["default"].createElement("div", { className: classNames('rck-list-thumbnail', className), "data-testid": getTestId(exports.ListThumbnailTest.prefix, getTestIdWithParentTestId(parentTestId, testId)) },
        React__default["default"].createElement("div", { className: "rck-list-thumbnail__wrapper" },
            React__default["default"].createElement("div", { className: "rck-list-thumbnail__wrapper__opacity-layer" }),
            React__default["default"].createElement("img", { className: classNames('rck-list-thumbnail__wrapper__thumb', imageMinWidth
                    ? "rck-list-thumbnail__wrapper__thumb--min-width-".concat(imageMinWidth)
                    : '', imageMinHeight
                    ? "rck-list-thumbnail__wrapper__thumb--min-height-".concat(imageMinHeight)
                    : ''), src: thumbnailUrl, role: "presentation", alt: altTextThumnail }),
            React__default["default"].createElement(Icon$1, { type: IconType.playCircle, className: "rck-list-thumbnail__wrapper__icon", family: exports.IconFamily.regular, color: exports.IconColor.contrast }))));
};
ListThumbnail.displayName = 'ListThumbnail';
var ListThumbnail$1 = React__default["default"].memo(ListThumbnail);

var makeClickableListItem = function (Component) { return function (_a) {
    var onClick = _a.onClick, useAsLink = _a.useAsLink, href = _a.href, role = _a.role, _b = _a.clickableClassName, clickableClassName = _b === void 0 ? '' : _b, props = __rest(_a, ["onClick", "useAsLink", "href", "role", "clickableClassName"]);
    return onClick || href || useAsLink ? (React__default["default"].createElement(Link$1, { className: classNames('rck-list-item--clickable', clickableClassName), useAsLink: useAsLink, role: role, href: href, onClick: onClick, "data-testid": getTestId('clickable', props.testId) },
        React__default["default"].createElement(Component, __assign({}, props)))) : (React__default["default"].createElement(Component, __assign({}, props)));
}; };

// eslint-disable-next-line import/prefer-default-export
var getWordBreakClass = function (prefix, type) { return (type ? "".concat(prefix, "--word-break-").concat(type) : ''); };

exports.ListColoredTextTest = void 0;
(function (ListColoredTextTest) {
    ListColoredTextTest["prefix"] = "ListColoredText";
})(exports.ListColoredTextTest || (exports.ListColoredTextTest = {}));
var ListColoredText = function (_a) {
    var color = _a.color, wordBreak = _a.wordBreak, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children;
    return (React__default["default"].createElement("span", { className: classNames('rck-list-colored-text', "rck-list-colored-text--".concat(color), getWordBreakClass('rck-list-colored-text', wordBreak), className), "data-testid": getTestId(exports.ListColoredTextTest.prefix, testId) }, children));
};
ListColoredText.displayName = 'ListColoredText';
var ListColoredText$1 = React__default["default"].memo(ListColoredText);

exports.ListCompactRightTitleTest = void 0;
(function (ListCompactRightTitleTest) {
    ListCompactRightTitleTest["prefix"] = "ListCompactRightTitle";
})(exports.ListCompactRightTitleTest || (exports.ListCompactRightTitleTest = {}));
var ListCompactRightTitle = function (_a) {
    var testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children;
    return (React__default["default"].createElement("p", { className: classNames('rck-list-compact-right-title', className), "data-testid": testId }, children));
};
ListCompactRightTitle.displayName = 'ListCompactRightTitle';
var ListCompactRightTitle$1 = React__default["default"].memo(ListCompactRightTitle);

exports.ListBckImageIconTest = void 0;
(function (ListBckImageIconTest) {
    ListBckImageIconTest["prefix"] = "ListItemBrandIcon";
})(exports.ListBckImageIconTest || (exports.ListBckImageIconTest = {}));
var ListBckImageIcon = function (_a) {
    var url = _a.url, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b;
    return (React__default["default"].createElement("div", { "data-testid": getTestId(exports.ListBckImageIconTest.prefix, testId), className: classNames('rck-list-item-bck-image-icon', className), style: { backgroundImage: "url(".concat(url, ")") } }));
};
ListBckImageIcon.displayName = 'ListBckImageIcon';
var ListBckImageIcon$1 = React__default["default"].memo(ListBckImageIcon);

exports.ListSecondaryTextTest = void 0;
(function (ListSecondaryTextTest) {
    ListSecondaryTextTest["prefix"] = "ListSecondaryText";
})(exports.ListSecondaryTextTest || (exports.ListSecondaryTextTest = {}));
var ListSecondaryText = function (_a) {
    var _b = _a.align, align = _b === void 0 ? TextAlign.left : _b, _c = _a.fontSize, fontSize = _c === void 0 ? 's' : _c, allowLines = _a.allowLines, _d = _a.renderAs, Tag = _d === void 0 ? 'p' : _d, children = _a.children, _e = _a.className, className = _e === void 0 ? '' : _e, testId = _a.testId, parentTestId = _a.parentTestId;
    return (React__default["default"].createElement(Tag, { className: classNames("rck-list-secondary-text", "rck-list-secondary-text--align-".concat(align), "rck-list-secondary-text--fsize-".concat(fontSize), allowLines ? "rck-list-secondary-text--ellipsis-for-".concat(allowLines) : '', className), "data-testid": getTestId(exports.ListSecondaryTextTest.prefix, getTestIdWithParentTestId(parentTestId, testId)) }, children));
};
ListSecondaryText.displayName = 'ListSecondaryText';
var ListSecondaryText$1 = React__default["default"].memo(ListSecondaryText);

var PlanBoxFeatureListItem = function (_a) {
    var icon = _a.icon, text = _a.text;
    return (React__namespace.createElement(FreeStyleListItem, { className: "rck-feature-list-item" },
        React__namespace.createElement(FlexLayout, { mainaxis: "row", marginTop: "8" },
            React__namespace.createElement(FlexLayout, { mainaxis: "row", marginRight: "8", minWidth: "24" }, icon),
            text)));
};
PlanBoxFeatureListItem.displayName = 'PlanBoxFeatureListItem';

var PlanBoxAddonCheckbox = function (_a) {
    var label = _a.label, showCheckbox = _a.showCheckbox, isChecked = _a.isChecked, onClick = _a.onClick, description = _a.description, list = _a.list;
    var _b = React__namespace.useState(false), showCheckboxHelpInfo = _b[0], setShowCheckboxHelpInfo = _b[1];
    return (React__namespace.createElement("div", { className: "rck-addon-checkbox" },
        React__namespace.createElement(FlexLayout, { mainaxis: "column" },
            React__namespace.createElement(FlexLayout, { mainaxis: "row", mainaxisAlignment: "left" },
                showCheckbox ? (React__namespace.createElement(Checkbox, { name: "addon-checkbox", label: label, checked: isChecked, onClick: onClick })) : (label),
                React__namespace.createElement(TransparentButton$1, { onClick: function () { return setShowCheckboxHelpInfo(!showCheckboxHelpInfo); }, className: "rck-addon-checkbox__icon-info ".concat(showCheckboxHelpInfo &&
                        'rck-addon-checkbox__icon-info--open') },
                    React__namespace.createElement(Icon$1, { type: IconType.questionCircle, family: showCheckboxHelpInfo ? exports.IconFamily.solid : exports.IconFamily.regular }))),
            React__namespace.createElement(AccordionMenu, { isVisible: showCheckboxHelpInfo },
                React__namespace.createElement("div", { className: "rck-addon-checkbox__content" },
                    React__namespace.createElement("p", { className: "rck-addon-checkbox__description" }, description),
                    list.map(function (_a, index) {
                        var icon = _a.icon, text = _a.text;
                        return (React__namespace.createElement(PlanBoxFeatureListItem, { key: "addon-checkbox-feature-".concat(index.toString()), icon: icon, text: text }));
                    }))))));
};
PlanBoxAddonCheckbox.displayName = 'PlanBoxAddonCheckbox';

var PlanBoxExtended = function (_a) {
    var _b;
    var icon = _a.icon, title = _a.title, subtitle = _a.subtitle, price = _a.price, years = _a.years, priceAdditionalInformation = _a.priceAdditionalInformation, highlighted = _a.highlighted, current = _a.current, currentText = _a.currentText, addonInfo = _a.addonInfo, ribbon = _a.ribbon, button = _a.button, features = _a.features, testId = _a.testId;
    var highlightedClass = highlighted
        ? 'rck-plan-box-extended--highlighted'
        : '';
    return (React__namespace.createElement("div", { className: "rck-plan-box-extended ".concat(highlightedClass), "data-testid": getTestId('rck-plan-box-extended', testId) },
        ribbon && (React__namespace.createElement("div", { className: "rck-plan-box-extended__ribbon" }, ribbon.text)),
        React__namespace.createElement(FlexLayout, { mainaxis: "column", display: "contents" },
            React__namespace.createElement(React__namespace.Fragment, null,
                React__namespace.createElement("div", { className: "rck-plan-box-extended__body" },
                    React__namespace.createElement("h1", { className: "rck-plan-box-extended__title" },
                        React__namespace.createElement(Icon$1, { className: "rck-plan-box-extended__title-icon", type: icon, family: exports.IconFamily.regular, color: exports.IconColor.greenDecorationContrast }),
                        title),
                    React__namespace.createElement("h2", { className: "rck-plan-box-extended__subtitle" }, subtitle),
                    React__namespace.createElement("span", { className: "rck-plan-box-extended__divider" }),
                    React__namespace.createElement("p", { className: "rck-plan-box-extended__price" },
                        price,
                        years && (React__namespace.createElement("span", { className: "rck-plan-box-extended__price--period" },
                            "/",
                            years))),
                    React__namespace.createElement("p", { className: "rck-plan-box-extended__price-info" }, priceAdditionalInformation)),
                addonInfo && (React__namespace.createElement("div", { className: "rck-plan-box-extended__addon-info" },
                    React__namespace.createElement(PlanBoxAddonCheckbox, __assign({}, addonInfo)))),
                React__namespace.createElement("div", { className: "rck-plan-box-extended__call-to-action" }, current ? (React__namespace.createElement("div", { className: "rck-plan-box-extended__current-text" }, currentText)) : (React__namespace.createElement(Button$1, { loading: button.isLoading, disabled: button.isLoading, onClick: button.onClick, size: exports.ButtonSize.big, color: highlighted ? exports.ButtonColor.white : exports.ButtonColor.secondary, buttonType: highlighted ? exports.ButtonType.primary : exports.ButtonType.secondary, block: true }, button.text))),
                features && (React__namespace.createElement("div", { className: "rck-plan-box-extended__features rck-feature-list" },
                    React__namespace.createElement("div", { className: "rck-feature-list__title" }, features.title),
                    React__namespace.createElement("ul", { className: "rck-feature-list__list" }, (_b = features.list) === null || _b === void 0 ? void 0 : _b.map(function (text, index) { return (React__namespace.createElement(PlanBoxFeatureListItem, { key: "plan-box-feature-".concat(index.toString()), icon: React__namespace.createElement(Icon$1, { family: exports.IconFamily.solid, color: exports.IconColor.greenDecorationContrast, type: IconType.checkCircle }), text: text })); }))))))));
};
PlanBoxExtended.displayName = 'PlanBoxExtended';

exports.RadioButtonTest = void 0;
(function (RadioButtonTest) {
    RadioButtonTest["prefix"] = "RadioButton";
})(exports.RadioButtonTest || (exports.RadioButtonTest = {}));
var RadioButton = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, name = _a.name, id = _a.id, checked = _a.checked, disabled = _a.disabled, block = _a.block, defaultChecked = _a.defaultChecked, value = _a.value, testId = _a.testId, onClick = _a.onClick, onBlur = _a.onBlur, onFocus = _a.onFocus, onMouseDown = _a.onMouseDown, onMouseUp = _a.onMouseUp, onChange = _a.onChange;
    var handleOnClick = function () {
        if (!disabled) {
            if (onClick) {
                onClick(value);
            }
        }
    };
    return (React__namespace.createElement("label", { className: classNames(className, 'rck-radiobutton', block ? 'rck-radiobutton--block' : '', disabled ? 'rck-radiobutton--disabled' : ''), htmlFor: id, "data-testid": getTestId(exports.RadioButtonTest.prefix, testId) },
        React__namespace.createElement("input", { id: id, className: "rck-radiobutton__input", name: name, type: "radio", checked: checked, defaultChecked: defaultChecked, onClick: handleOnClick, onBlur: onBlur, onFocus: onFocus, onMouseDown: onMouseDown, onMouseUp: onMouseUp, onChange: onChange, value: value }),
        React__namespace.createElement("span", { className: classNames('rck-radiobutton__square', checked ? 'rck-radiobutton__square--checked' : '', disabled ? 'rck-radiobutton__square--disabled' : '') }, checked && (React__namespace.createElement(Icon$1, { family: exports.IconFamily.solid, type: IconType.circle, className: classNames('rck-radiobutton__square__icon', disabled ? 'rck-radiobutton__square__icon--disabled' : ''), role: "presentation" })))));
};
RadioButton.displayName = 'RadioButton';

/* eslint-disable jsx-a11y/click-events-have-key-events */
exports.SideNavigationMenuTest = void 0;
(function (SideNavigationMenuTest) {
    SideNavigationMenuTest["prefix"] = "SideNavigationMenu";
    SideNavigationMenuTest["menuItem"] = "SideNavigationMenuItem";
    SideNavigationMenuTest["anchor"] = "SideNavigationMenuAnchor";
    SideNavigationMenuTest["extension"] = "SideNavigationMenuItemExtension";
    SideNavigationMenuTest["menuItemWrapper"] = "SideNavigationMenuItem-wrapper";
})(exports.SideNavigationMenuTest || (exports.SideNavigationMenuTest = {}));
/**
 * Resolves the badge component to render based on the provided props.
 * If a badge element is provided, it takes precedence over the showBadge to maintain retro compatibility.
 */
var ResolveBadge = function (_a) {
    var showBadge = _a.showBadge, badge = _a.badge;
    if (badge) {
        return badge;
    }
    if (showBadge) {
        return (React__default["default"].createElement(Badge$1, { className: "rck-side-navigation-menu__badge", type: exports.BadgeType.contrast, text: "Premium", size: "small" }));
    }
    return null;
};
var MenuItemExtension = React__default["default"].memo(function (_a) {
    var title = _a.title, showBadge = _a.showBadge, showAccent = _a.showAccent, testId = _a.testId, parentTestId = _a.parentTestId, badge = _a.badge;
    return (React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__extension", "data-testid": getTestId(exports.SideNavigationMenuTest.extension, getTestIdWithParentTestId(parentTestId, testId)) },
        React__default["default"].createElement("span", { className: "rck-side-navigation-menu__anchor__text" },
            title,
            showAccent && (React__default["default"].createElement("span", { className: "rck-side-navigation-menu__accent" },
                React__default["default"].createElement(Icon$1, { type: IconType.circle, family: exports.IconFamily.solid, square: true })))),
        React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__optional-content" },
            React__default["default"].createElement(ResolveBadge, { showBadge: showBadge, badge: badge }))));
});
var SideNavigationMenuAnchor = function (_a) {
    var item = _a.item, testId = _a.testId, parentTestId = _a.parentTestId;
    var Anchor = item.useAsLink;
    var _b = item.linkTo, linkTo = _b === void 0 ? '' : _b;
    return Anchor ? (React__default["default"].createElement(Anchor, { className: classNames('rck-side-navigation-menu__anchor', item.isActive ? 'rck-side-navigation-menu__anchor--active' : ''), to: linkTo, "data-testid": getTestId(exports.SideNavigationMenuTest.anchor, getTestIdWithParentTestId(parentTestId, testId)) },
        React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__content" },
            React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__content__activable" },
                React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__content__activable__layout" },
                    React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__icon" }, item.icon),
                    React__default["default"].createElement(MenuItemExtension, { title: item.title, showBadge: item.showBadge, showAccent: item.showAccent, testId: testId, parentTestId: parentTestId, badge: item.badge })))))) : null;
};
var SideNavigationMenuItem = function (_a) {
    var item = _a.item, testId = _a.testId, parentTestId = _a.parentTestId;
    return (React__default["default"].createElement("a", { className: classNames('rck-side-navigation-menu__anchor', item.isActive ? 'rck-side-navigation-menu__anchor--active' : ''), href: item.linkTo, "data-testid": getTestId(exports.SideNavigationMenuTest.menuItem, getTestIdWithParentTestId(parentTestId, testId)) },
        React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__content" },
            React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__content__activable" },
                React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__content__activable__layout" },
                    React__default["default"].createElement("div", { className: "rck-side-navigation-menu__anchor__icon" }, item.icon),
                    React__default["default"].createElement(MenuItemExtension, { title: item.title, showBadge: item.showBadge, showAccent: item.showAccent, testId: testId, parentTestId: parentTestId, badge: item.badge }))))));
};
var SideNavigationMenu = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, accountInfo = _a.accountInfo, licenseInfo = _a.licenseInfo, testId = _a.testId, expand = _a.expand, items = _a.items;
    return (React__default["default"].createElement("div", { className: classNames('rck-side-navigation-menu', className, expand ? 'rck-side-navigation-menu--expanded' : ''), "datatest-id": getTestId(exports.SideNavigationMenuTest.prefix, testId) },
        React__default["default"].cloneElement(accountInfo, {
            expand: expand,
        }),
        React__default["default"].createElement("div", { className: "rck-side-navigation-menu__wrapper" },
            React__default["default"].createElement(ScrollableContent, { hideScrollbar: true },
                React__default["default"].createElement("ul", { className: "rck-side-navigation-menu__list" }, items.map(function (item) { return (React__default["default"].createElement(React__default["default"].Fragment, { key: item.key }, item.shouldShow && (React__default["default"].createElement("li", { title: item.title, className: classNames('rck-side-navigation-menu__list__item'), onClick: item.onClick, role: "navigation", onKeyPress: item.onClick, "data-testid": getTestId(exports.SideNavigationMenuTest.prefix, testId, exports.SideNavigationMenuTest.menuItemWrapper, item.key) }, item.useAsLink ? (React__default["default"].createElement(SideNavigationMenuAnchor, { item: item, testId: item.key, parentTestId: testId })) : (React__default["default"].createElement(SideNavigationMenuItem, { item: item, testId: item.key, parentTestId: testId })))))); })))),
        licenseInfo));
};

exports.StepBarTest = void 0;
(function (StepBarTest) {
    StepBarTest["prefix"] = "StepBar";
})(exports.StepBarTest || (exports.StepBarTest = {}));
var StepBar = function (_a) {
    var _b = _a.activeStep, activeStep = _b === void 0 ? 0 : _b, hideLabels = _a.hideLabels, className = _a.className, steps = _a.steps, testId = _a.testId;
    return (React__namespace.createElement("div", { className: classNames("rck-stepbar", className || ''), "data-testid": getTestId(exports.StepBarTest.prefix, testId) }, steps.map(function (step, key) {
        var currentStep = key + 1;
        var doneStepClass = key <= activeStep ? 'rck-step--done' : '';
        var activeStepClass = key === activeStep ? 'rck-step--active' : '';
        return (React__namespace.createElement("div", { className: classNames('rck-stepbar__step', 'rck-step', activeStepClass, doneStepClass), key: step, "data-testid": getTestId(exports.StepBarTest.prefix, testId, step) },
            React__namespace.createElement("div", { className: "rck-step__label" },
                React__namespace.createElement("span", { className: "rck-step__label-title" },
                    ' ',
                    hideLabels ? '' : step,
                    ' ')),
            React__namespace.createElement("div", { className: "rck-step__circle" },
                React__namespace.createElement("span", null, currentStep))));
    })));
};
StepBar.displayName = 'Stepbar';

exports.SwitchTest = void 0;
(function (SwitchTest) {
    SwitchTest["prefix"] = "Switch";
    SwitchTest["actionElement"] = "Action-element";
})(exports.SwitchTest || (exports.SwitchTest = {}));
var Switch = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, id = _a.id, name = _a.name, _c = _a.checked, checked = _c === void 0 ? false : _c, _d = _a.disabled, disabled = _d === void 0 ? false : _d, testId = _a.testId, onClick = _a.onClick, onBlur = _a.onBlur, onFocus = _a.onFocus, onMouseDown = _a.onMouseDown, onMouseUp = _a.onMouseUp, onChange = _a.onChange;
    var handleOnClick = function (ev) {
        if (!disabled) {
            if (onClick) {
                onClick(ev);
            }
        }
    };
    return (React__namespace.createElement("label", { htmlFor: id, className: classNames('rck-switch', disabled ? 'rck-switch--disabled' : '', className), "data-testid": getTestId(exports.SwitchTest.prefix, testId) },
        React__namespace.createElement("input", { id: id, className: "rck-switch__input", name: name, type: "checkbox", checked: checked, disabled: disabled, onChange: onChange, onClick: handleOnClick, onBlur: onBlur, onFocus: onFocus, onMouseDown: onMouseDown, onMouseUp: onMouseUp, "data-testid": getTestId(exports.SwitchTest.prefix, testId, exports.SwitchTest.actionElement) }),
        React__namespace.createElement("span", { className: classNames('rck-switch__slider', disabled ? 'rck-switch__slider--disabled' : '') })));
};
Switch.displayName = 'Switch';

var InfoFieldLabelTest;
(function (InfoFieldLabelTest) {
    InfoFieldLabelTest["prefix"] = "InfoFieldLabel";
})(InfoFieldLabelTest || (InfoFieldLabelTest = {}));
var InfoFieldLabel = function (_a) {
    var htmlFor = _a.htmlFor, allowedLines = _a.allowedLines, _b = _a.mode, mode = _b === void 0 ? 'info' : _b, testId = _a.testId, _c = _a.className, className = _c === void 0 ? '' : _c, children = _a.children;
    return (React__namespace.createElement("div", { className: classNames('rck-info-field-label', className) }, children && (React__namespace.createElement("label", { htmlFor: htmlFor, className: classNames('rck-info-field-label__label', allowedLines
            ? "rck-info-field-label__label--lines-".concat(allowedLines)
            : '', "rck-info-field-label__label--mode-".concat(mode)), "data-testid": getTestId(InfoFieldLabelTest.prefix, testId) }, children))));
};
InfoFieldLabel.displayName = 'InfoFieldLabel';
var InfoFieldLabel$1 = React__namespace.memo(InfoFieldLabel);

var assignRefs = function () {
    var refs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        refs[_i] = arguments[_i];
    }
    return function (node) {
        refs.forEach(function (currRef) {
            if (typeof currRef === 'function') {
                currRef(node);
            }
            else if (currRef) {
                // eslint-disable-next-line no-param-reassign
                currRef.current = node;
            }
        });
    };
};
var useAssignRef = function () {
    var refs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        refs[_i] = arguments[_i];
    }
    /**
     * This will create a new function if the refs passed to this hook change and are all defined.
     * This means react will call the old ref copy with `null` and the new ref copy
     * with the ref. Cleanup naturally emerges from this behavior.
     */
    return React.useMemo(function () {
        if (refs.every(function (ref) { return ref == null; })) {
            return null;
        }
        return function (instance) {
            assignRefs.apply(void 0, refs)(instance);
        };
    }, refs);
};

exports.InputShowIconWhen = void 0;
(function (InputShowIconWhen) {
    InputShowIconWhen[InputShowIconWhen["always"] = 0] = "always";
    InputShowIconWhen[InputShowIconWhen["onError"] = 1] = "onError";
    InputShowIconWhen[InputShowIconWhen["edit"] = 2] = "edit";
})(exports.InputShowIconWhen || (exports.InputShowIconWhen = {}));
var GroupedInputTest;
(function (GroupedInputTest) {
    GroupedInputTest["prefix"] = "GroupedInput";
    GroupedInputTest["actionElement"] = "Action-element";
    GroupedInputTest["input"] = "Input";
})(GroupedInputTest || (GroupedInputTest = {}));
var isEditing = function (hasContent, hasFocus) {
    return hasContent && hasFocus;
};
var showIcon = function (hasContent, hasFocus, iconConfiguration, error) {
    return iconConfiguration &&
        ((iconConfiguration.showIconWhen === exports.InputShowIconWhen.onError && error) ||
            iconConfiguration.showIconWhen === exports.InputShowIconWhen.always ||
            (isEditing(hasContent, hasFocus) &&
                iconConfiguration.showIconWhen === exports.InputShowIconWhen.edit));
};
var GroupedInput = React__namespace.forwardRef(function (_a, ref) {
    var id = _a.id, value = _a.value, defaultValue = _a.defaultValue, _b = _a.className, className = _b === void 0 ? '' : _b, _c = _a.disabled, disabled = _c === void 0 ? false : _c, type = _a.type, error = _a.error, _d = _a.placeholder, placeholder = _d === void 0 ? '' : _d, iconConfiguration = _a.iconConfiguration, autoComplete = _a.autoComplete, autoCorrect = _a.autoCorrect, autoCapitalize = _a.autoCapitalize, spellCheck = _a.spellCheck, maxlength = _a.maxlength, minlength = _a.minlength, required = _a.required, min = _a.min, max = _a.max, pattern = _a.pattern, name = _a.name, iconLeft = _a.iconLeft, testId = _a.testId, onChange = _a.onChange, onBlur = _a.onBlur, onDragStart = _a.onDragStart, onFocus = _a.onFocus, onDrop = _a.onDrop, onIconClick = _a.onIconClick;
    var textInput = React__namespace.useRef(document.createElement('input'));
    var containerRef = React__namespace.useRef(document.createElement('div'));
    var _e = React.useState(false), isFocused = _e[0], setIsFocused = _e[1];
    var _f = React.useState(false), hasContent = _f[0], setHasContent = _f[1];
    var handleOnChange = function (ev) {
        setHasContent(textInput.current.value !== '');
        if (onChange) {
            onChange(ev, textInput.current.value);
        }
    };
    var handleOnIconClick = function (ev) {
        if ((iconConfiguration === null || iconConfiguration === void 0 ? void 0 : iconConfiguration.showIconWhen) === exports.InputShowIconWhen.edit) {
            if (textInput !== null && textInput.current !== null) {
                textInput.current.value = '';
                handleOnChange(ev);
            }
        }
        if (onIconClick) {
            onIconClick(ev);
        }
    };
    var handleOnFocus = function (ev) {
        setIsFocused(true);
        setHasContent(textInput.current.value !== '');
        containerRef.current.classList.add('rck-text-field__grouped--focused');
        if (onFocus) {
            onFocus(ev);
        }
    };
    var handleOnBlur = function (ev) {
        setIsFocused(false);
        setHasContent(textInput.current.value !== '');
        containerRef.current.classList.remove('rck-text-field__grouped--focused');
        if (onBlur) {
            onBlur(ev);
        }
    };
    var onMouseEnterHandler = function () {
        containerRef.current.classList.add('rck-text-field__grouped--hover');
    };
    var onMouseLeaveHandler = function () {
        containerRef.current.classList.remove('rck-text-field__grouped--hover');
    };
    return (React__namespace.createElement("div", { className: classNames('rck-text-field__grouped', disabled ? 'rck-text-field__grouped--disabled' : ''), ref: containerRef, "data-testid": getTestId(GroupedInputTest.prefix, testId) },
        iconLeft && (React__namespace.createElement("span", { className: "rck-text-field__icon-left" },
            React__namespace.createElement("span", { className: "rck-text-field__icon__wrapper" }, iconLeft))),
        React__namespace.createElement("input", { ref: useAssignRef(textInput, ref), id: id, value: value, defaultValue: defaultValue, disabled: disabled, type: type, placeholder: placeholder, onChange: handleOnChange, className: classNames('rck-text-field__input', className, iconConfiguration ? 'rck-text-field__input--with-icon' : '', iconLeft ? 'rck-text-field__input--with-icon-left' : ''), name: name, autoComplete: autoComplete, autoCorrect: autoCorrect, autoCapitalize: autoCapitalize, spellCheck: spellCheck, maxLength: maxlength, minLength: minlength, required: required, min: min, max: max, pattern: pattern, "data-testid": getTestId(GroupedInputTest.prefix, testId, GroupedInputTest.input), onFocus: handleOnFocus, onBlur: handleOnBlur, onDragStart: onDragStart, onDrop: onDrop, onMouseEnter: onMouseEnterHandler, onMouseLeave: onMouseLeaveHandler }),
        iconConfiguration && (React__namespace.createElement("span", { className: "rck-text-field__icon", onClick: handleOnIconClick, onKeyDown: handleOnIconClick, role: "button", tabIndex: 0, "data-testid": getTestId(GroupedInputTest.prefix, testId, GroupedInputTest.actionElement) },
            React__namespace.createElement("span", { className: classNames('rck-text-field__icon__wrapper', showIcon(hasContent, isFocused, iconConfiguration, error)
                    ? ''
                    : 'rck-text-field__icon__wrapper--hide') }, React__namespace.cloneElement(iconConfiguration.icon))))));
});
GroupedInput.displayName = 'GroupedInput';

var PasswordMode;
(function (PasswordMode) {
    PasswordMode[PasswordMode["none"] = 0] = "none";
    PasswordMode[PasswordMode["hide"] = 1] = "hide";
    PasswordMode[PasswordMode["show"] = 2] = "show";
})(PasswordMode || (PasswordMode = {}));
exports.TextFieldTest = void 0;
(function (TextFieldTest) {
    TextFieldTest["prefix"] = "TextField";
    TextFieldTest["supportText"] = "Support_content";
})(exports.TextFieldTest || (exports.TextFieldTest = {}));
var TextField = React__namespace.forwardRef(function (_a, ref) {
    var id = _a.id, label = _a.label, _b = _a.className, className = _b === void 0 ? '' : _b, disabled = _a.disabled, block = _a.block, type = _a.type, error = _a.error, supportTextConfiguration = _a.supportTextConfiguration, _c = _a.placeholder, placeholder = _c === void 0 ? '' : _c, autoComplete = _a.autoComplete, autoCorrect = _a.autoCorrect, autoCapitalize = _a.autoCapitalize, spellCheck = _a.spellCheck, maxlength = _a.maxlength, minlength = _a.minlength, required = _a.required, min = _a.min, max = _a.max, pattern = _a.pattern, name = _a.name, value = _a.value, defaultValue = _a.defaultValue, testId = _a.testId, canBeCleared = _a.canBeCleared, showAlertOnError = _a.showAlertOnError, iconLeft = _a.iconLeft, _d = _a.keepErrorLabelArea, keepErrorLabelArea = _d === void 0 ? true : _d, _e = _a.size, size = _e === void 0 ? 'regular' : _e, textCentered = _a.textCentered, onIconClick = _a.onIconClick, onChange = _a.onChange, onBlur = _a.onBlur, onDragStart = _a.onDragStart, onFocus = _a.onFocus, onDrop = _a.onDrop;
    var _f = React.useState(PasswordMode.none), passwordMode = _f[0], setPasswordMode = _f[1];
    var handlePasswordModeChange = function (ev) {
        setPasswordMode(passwordMode === PasswordMode.show
            ? PasswordMode.hide
            : PasswordMode.show);
        if (onIconClick) {
            onIconClick(ev);
        }
    };
    var renderGroupedInput = function () {
        var otherProps = {
            id: id,
            disabled: disabled,
            className: className,
            placeholder: placeholder,
            defaultValue: defaultValue,
            error: error,
            autoComplete: autoComplete,
            autoCorrect: autoCorrect,
            autoCapitalize: autoCapitalize,
            spellCheck: spellCheck,
            maxlength: maxlength,
            minlength: minlength,
            required: required,
            min: min,
            max: max,
            pattern: pattern,
            iconLeft: iconLeft,
            name: name,
            onBlur: onBlur,
            onDragStart: onDragStart,
            onDrop: onDrop,
            onFocus: onFocus,
            testId: testId,
            ref: ref,
        };
        if (type === 'password') {
            return (React__namespace.createElement(GroupedInput, __assign({ type: passwordMode === PasswordMode.show ? 'text' : 'password', iconConfiguration: {
                    showIconWhen: exports.InputShowIconWhen.always,
                    icon: (React__namespace.createElement(Icon$1, { type: passwordMode === PasswordMode.show
                            ? IconType.eyeSlash
                            : IconType.eye })),
                }, onIconClick: handlePasswordModeChange }, otherProps, { onChange: onChange })));
        }
        if (showAlertOnError && error) {
            return (React__namespace.createElement(GroupedInput, __assign({ type: type, iconConfiguration: {
                    showIconWhen: exports.InputShowIconWhen.onError,
                    icon: (React__namespace.createElement(Icon$1, { family: exports.IconFamily.regular, type: IconType.exclamationCircle })),
                }, value: value, onIconClick: onIconClick, onChange: onChange }, otherProps)));
        }
        return (React__namespace.createElement(GroupedInput, __assign({ type: type, iconConfiguration: canBeCleared
                ? {
                    showIconWhen: exports.InputShowIconWhen.edit,
                    icon: (React__namespace.createElement(Icon$1, { family: exports.IconFamily.regular, type: IconType.times })),
                }
                : undefined, value: value, onIconClick: onIconClick, onChange: onChange }, otherProps)));
    };
    return (React__namespace.createElement("div", { className: classNames('rck-text-field', block ? 'rck-text-field--block' : '', error ? 'rck-text-field--error' : '', disabled ? 'rck-text-field--disabled' : '', !keepErrorLabelArea ? 'rck-text-field--no-area-for-error' : '', size ? "rck-text-field--size-".concat(size) : '', textCentered ? 'rck-text-field--text-centered' : '', className), "data-testid": getTestId(exports.TextFieldTest.prefix, testId) },
        label ? (React__namespace.createElement("label", { htmlFor: name, className: "rck-text-field__label" }, label)) : null,
        renderGroupedInput(),
        supportTextConfiguration ? (React__namespace.createElement(InfoFieldLabel$1, { allowedLines: supportTextConfiguration.allowedLines, testId: name, htmlFor: name, mode: error ? 'error' : 'info' }, supportTextConfiguration.text)) : null));
});
TextField.displayName = 'TextField';

exports.StyledHeaderTest = void 0;
(function (StyledHeaderTest) {
    StyledHeaderTest["prefix"] = "StyledHeader";
})(exports.StyledHeaderTest || (exports.StyledHeaderTest = {}));
var StyledHeader$1 = function (_a) {
    var _b = _a.type, type = _b === void 0 ? 'h1' : _b, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId, children = _a.children;
    var classes = classNames('rck-styled-header', className);
    var dataTestId = getTestId(exports.StyledHeaderTest.prefix, testId);
    var h1Element = (React__default["default"].createElement(Text, { className: classes, renderAs: "h1", variant: "headline-1-semibold-responsive", parentTestId: dataTestId }, children));
    switch (type) {
        case 'h1':
            return h1Element;
        case 'h2':
            return (React__default["default"].createElement(Text, { className: classes, renderAs: "h2", variant: "headline-2-semibold-responsive", parentTestId: dataTestId }, children));
        case 'h3':
            return (React__default["default"].createElement(Text, { className: classes, renderAs: "h3", variant: "title-1-semibold-responsive", parentTestId: dataTestId }, children));
        case 'h4':
            return (React__default["default"].createElement(Text, { className: classes, renderAs: "h4", variant: "title-2-semibold-responsive", parentTestId: dataTestId }, children));
        case 'h5':
            return (React__default["default"].createElement(Text, { className: classes, renderAs: "h5", variant: "title-3-semibold-responsive", parentTestId: dataTestId }, children));
        default:
            return h1Element;
    }
};
var Header = React__default["default"].memo(StyledHeader$1);

exports.CardActionTitleTest = void 0;
(function (CardActionTitleTest) {
    CardActionTitleTest["prefix"] = "CardActionTitle";
})(exports.CardActionTitleTest || (exports.CardActionTitleTest = {}));
var getIcon = function (icon) {
    if (!icon)
        return null;
    if (React__namespace.isValidElement(icon))
        return icon;
    if (Object.values(IconType).includes(icon)) {
        return (React__namespace.createElement(Icon$1, { type: icon, className: "rck-card-action-title__icon" }));
    }
    return null;
};
var CardActionTitle = function (_a) {
    var icon = _a.icon, _b = _a.color, color = _b === void 0 ? exports.GlobalType.default : _b, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId, children = _a.children;
    return (React__namespace.createElement("div", { className: classNames('rck-card-action-title', "rck-card-action-title--color-".concat(color), className), "data-testid": getTestId(exports.CardActionTitleTest.prefix, testId) },
        getIcon(icon),
        children));
};
var CardActionTitle$1 = React__namespace.memo(CardActionTitle);

exports.BrandsHeaderTest = void 0;
(function (BrandsHeaderTest) {
    BrandsHeaderTest["prefix"] = "BrandsHeader";
})(exports.BrandsHeaderTest || (exports.BrandsHeaderTest = {}));
var StyledHeader = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId, children = _a.children;
    var dataTestId = getTestId(exports.BrandsHeaderTest.prefix, testId);
    return (React__default["default"].createElement("div", { className: classNames('rck-brands-header', className), "data-testid": dataTestId }, children || (React__default["default"].createElement(FlexLayout, { mainaxis: "row", mainaxisAlignment: "center" },
        React__default["default"].createElement(Icon$1, { family: exports.IconFamily.brands, type: IconType.windows, color: exports.IconColor.primary, size: IconSize.lg }),
        React__default["default"].createElement(Icon$1, { family: exports.IconFamily.brands, type: IconType.apple, color: exports.IconColor.primary, size: IconSize.lg }),
        React__default["default"].createElement(Icon$1, { family: exports.IconFamily.brands, type: IconType.chrome, color: exports.IconColor.primary, size: IconSize.lg }),
        React__default["default"].createElement(Icon$1, { family: exports.IconFamily.brands, type: IconType.android, color: exports.IconColor.primary, size: IconSize.lg })))));
};
var BrandsHeader = React__default["default"].memo(StyledHeader);

var TextFieldAdapter = function (_a) {
    var label = _a.label, className = _a.className, disabled = _a.disabled, block = _a.block, type = _a.type, error = _a.error, supportTextConfiguration = _a.supportTextConfiguration, placeholder = _a.placeholder, autoComplete = _a.autoComplete, autoCorrect = _a.autoCorrect, autoCapitalize = _a.autoCapitalize, spellCheck = _a.spellCheck, maxlength = _a.maxlength, minlength = _a.minlength, required = _a.required, min = _a.min, max = _a.max, pattern = _a.pattern, defaultValue = _a.defaultValue, canBeCleared = _a.canBeCleared, showAlertOnError = _a.showAlertOnError, iconLeft = _a.iconLeft, meta = _a.meta, input = _a.input;
    return (React__namespace.createElement(TextField, __assign({ label: label, className: className, disabled: disabled, block: block, type: type, error: (!!(meta === null || meta === void 0 ? void 0 : meta.error) && (meta === null || meta === void 0 ? void 0 : meta.touched)) || error, supportTextConfiguration: (meta === null || meta === void 0 ? void 0 : meta.error)
            ? __assign({ text: meta === null || meta === void 0 ? void 0 : meta.error }, supportTextConfiguration) : supportTextConfiguration, placeholder: placeholder, autoComplete: autoComplete, autoCorrect: autoCorrect, autoCapitalize: autoCapitalize, spellCheck: spellCheck, maxlength: maxlength, minlength: minlength, required: required, min: min, max: max, pattern: pattern, defaultValue: defaultValue, canBeCleared: canBeCleared, showAlertOnError: showAlertOnError, iconLeft: iconLeft }, input)));
};
TextFieldAdapter.displayName = 'TextFieldAdapter';

exports.DropdownActionElementTest = void 0;
(function (DropdownActionElementTest) {
    DropdownActionElementTest["prefix"] = "DropdownActionElement";
})(exports.DropdownActionElementTest || (exports.DropdownActionElementTest = {}));
var DropdownActionElement = function (_a) {
    var text = _a.text, icon = _a.icon, iconRight = _a.iconRight, _b = _a.className, className = _b === void 0 ? '' : _b, disabled = _a.disabled, testId = _a.testId, onClick = _a.onClick;
    return (React__default["default"].createElement("div", { className: classNames('rck-dropdown-action-name', disabled ? 'rck-dropdown-action-name--disabled' : '', className), onClick: onClick, onKeyDown: onClick, role: "button", tabIndex: 0, "data-testid": getTestId(exports.DropdownActionElementTest.prefix, testId) },
        icon && React__default["default"].createElement("span", { className: "rck-dropdown-action-name__icon" }, icon),
        React__default["default"].createElement("span", { className: "rck-dropdown-action-name__text" }, text),
        iconRight && (React__default["default"].createElement("span", { className: "rck-dropdown-action-name__icon rck-dropdown-action-name__icon--right" }, iconRight))));
};
DropdownActionElement.displayName = 'ActionElement';

exports.DropdownFooterTest = void 0;
(function (DropdownFooterTest) {
    DropdownFooterTest["prefix"] = "DropdownFooter";
})(exports.DropdownFooterTest || (exports.DropdownFooterTest = {}));
var DropdownFooter = function (_a) {
    var children = _a.children, onClick = _a.onClick, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId;
    return (React__default["default"].createElement("ul", { role: "listbox", className: classNames('rck-dropdown__footer', className) },
        React__default["default"].createElement("li", { onClick: onClick, role: "option", onKeyDown: onClick, "aria-selected": false, "data-testid": getTestId(exports.DropdownFooterTest.prefix, testId) }, children)));
};
DropdownFooter.displayName = 'DropdownFooter';

exports.DropdownHeaderTest = void 0;
(function (DropdownHeaderTest) {
    DropdownHeaderTest["prefix"] = "DropdownHeader";
})(exports.DropdownHeaderTest || (exports.DropdownHeaderTest = {}));
var DropdownHeader = function (_a) {
    var children = _a.children, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b;
    return (React__default["default"].createElement("li", { className: classNames('rck-dropdown__list__header', className), "data-testid": getTestId(exports.DropdownHeaderTest.prefix, testId) }, children));
};
DropdownHeader.displayName = 'DropdownHeader';

exports.DropdownOptionTest = void 0;
(function (DropdownOptionTest) {
    DropdownOptionTest["prefix"] = "DropdownOption";
})(exports.DropdownOptionTest || (exports.DropdownOptionTest = {}));
var DropdownOption = React__default["default"].forwardRef(function (_a, ref) {
    var value = _a.value, text = _a.text, icon = _a.icon, _b = _a.className, className = _b === void 0 ? '' : _b, _c = _a.showAsSelected, showAsSelected = _c === void 0 ? true : _c, selected = _a.selected, disabled = _a.disabled, active = _a.active, assignKey = _a.assignKey, showAssignKey = _a.showAssignKey, onClick = _a.onClick, onMouseEnter = _a.onMouseEnter, onMouseLeave = _a.onMouseLeave, testId = _a.testId, children = _a.children;
    var handleClick = function (event) {
        if (!disabled && onClick) {
            onClick(value, event);
        }
    };
    var handleMouseEnter = function (ev) {
        if (onMouseEnter) {
            onMouseEnter(ev, value);
        }
    };
    var handleMouseLeave = function (ev) {
        if (onMouseLeave) {
            onMouseLeave(ev, value);
        }
    };
    var renderText = function () {
        if (active && !disabled) {
            return React__default["default"].createElement("strong", null, text);
        }
        return text;
    };
    return (React__default["default"].createElement("li", { className: classNames('rck-dropdown-option', active ? 'rck-dropdown-option--active' : '', showAsSelected && selected ? 'rck-dropdown-option--selected' : '', disabled ? 'rck-dropdown-option--disabled' : '', className), onClick: handleClick, onKeyDown: handleClick, onMouseEnter: handleMouseEnter, onMouseLeave: handleMouseLeave, role: "option", "aria-selected": selected, ref: ref, "data-testid": getTestId(exports.DropdownOptionTest.prefix, testId, value.toString()) },
        !children && icon && (React__default["default"].createElement("span", { className: "rck-dropdown-option__icon" }, icon)),
        !children && (React__default["default"].createElement("span", { className: "rck-dropdown-option__text" }, renderText())),
        !children && showAssignKey && assignKey && (React__default["default"].createElement("span", { className: "rck-dropdown-option__key-assign" },
            "[ctrl+",
            assignKey,
            "]")),
        children));
});
DropdownOption.displayName = 'DropdownOption';

var useDetectOutsideClick = function (elements, initialState) {
    var _a = React.useState(initialState), isActive = _a[0], setInnerIsActive = _a[1];
    var _b = React.useState(initialState), prevIsActive = _b[0], setPrevIsActive = _b[1];
    var setIsActive = function (active) {
        if (active !== isActive) {
            setPrevIsActive(isActive);
        }
        setInnerIsActive(active);
    };
    React.useEffect(function () {
        var pageClickEvent = function (e) {
            if (!elements.some(function (el) { return el.current && el.current.contains(e.target); })) {
                setIsActive(false);
            }
        };
        if (isActive) {
            window.addEventListener('click', pageClickEvent);
        }
        return function () {
            window.removeEventListener('click', pageClickEvent);
        };
    }, [isActive, elements]);
    return [isActive, setIsActive, prevIsActive];
};

var usePortal = function (id, useSingleHook) {
    var rootElemRef = React.useRef(null);
    var uniqueId = 'portal-container';
    var getRootElem = function () {
        if (!rootElemRef.current) {
            rootElemRef.current = document.createElement('div');
            if (useSingleHook) {
                rootElemRef.current.setAttribute('name', uniqueId);
            }
        }
        return rootElemRef.current;
    };
    React.useEffect(function () {
        var _a;
        var parentElem = document.querySelector("#".concat(id));
        if (!parentElem && !isTestEnv)
            throw new Error('usePortal: Element to append for portal is invalid');
        var container = parentElem === null || parentElem === void 0 ? void 0 : parentElem.querySelector("[name=\"".concat(uniqueId, "\"]"));
        if (!useSingleHook || (useSingleHook && !container)) {
            parentElem === null || parentElem === void 0 ? void 0 : parentElem.appendChild(getRootElem());
        }
        if (useSingleHook && !((_a = rootElemRef.current) === null || _a === void 0 ? void 0 : _a.parentElement)) {
            rootElemRef.current = container;
        }
        return function () {
            if (!useSingleHook) {
                getRootElem().remove();
            }
        };
    }, [id]);
    return getRootElem();
};

exports.DropdownMenuPlacement = void 0;
(function (FloatingMenuPlacement) {
    FloatingMenuPlacement["top"] = "top";
    FloatingMenuPlacement["bottom"] = "bottom";
})(exports.DropdownMenuPlacement || (exports.DropdownMenuPlacement = {}));
var useFloatingPlacement = function (el, initialPlacement, isActive, fixed) {
    var _a = React.useState(initialPlacement), placement = _a[0], setPlacement = _a[1];
    var updatePlacement = function (newPlacement) {
        if (el.current && isActive) {
            if (fixed && initialPlacement) {
                setPlacement(initialPlacement);
                return;
            }
            var dimensions = el.current.getBoundingClientRect();
            var totalHeight = window.innerHeight;
            var hasAvailableTopSpace = false;
            var hasAvailableBottomSpace = false;
            if (placement === exports.DropdownMenuPlacement.bottom) {
                hasAvailableTopSpace = dimensions.top - dimensions.height > 0;
                hasAvailableBottomSpace =
                    dimensions.top + dimensions.height < totalHeight;
            }
            else {
                hasAvailableTopSpace = dimensions.bottom - dimensions.height > 0;
                hasAvailableBottomSpace =
                    dimensions.bottom + dimensions.height < totalHeight;
            }
            if (newPlacement && hasAvailableBottomSpace && hasAvailableTopSpace) {
                setPlacement(newPlacement);
            }
            else if (!hasAvailableBottomSpace && !hasAvailableTopSpace) {
                var availableSpaceTop = 0;
                var availableSpaceBottom = 0;
                if (placement === exports.DropdownMenuPlacement.bottom) {
                    availableSpaceTop = dimensions.top;
                    availableSpaceBottom = totalHeight - dimensions.top;
                }
                else {
                    availableSpaceTop = dimensions.bottom;
                    availableSpaceBottom = totalHeight - dimensions.bottom;
                }
                if (availableSpaceBottom > availableSpaceTop &&
                    placement !== exports.DropdownMenuPlacement.bottom) {
                    setPlacement(exports.DropdownMenuPlacement.bottom);
                }
                else if (availableSpaceTop > availableSpaceBottom &&
                    placement !== exports.DropdownMenuPlacement.top) {
                    setPlacement(exports.DropdownMenuPlacement.top);
                }
            }
            else if (placement === exports.DropdownMenuPlacement.bottom &&
                !hasAvailableBottomSpace) {
                setPlacement(exports.DropdownMenuPlacement.top);
            }
            else if (placement === exports.DropdownMenuPlacement.top &&
                !hasAvailableTopSpace) {
                setPlacement(exports.DropdownMenuPlacement.bottom);
            }
        }
    };
    var updateAfterScroll = function () {
        updatePlacement();
    };
    React.useEffect(function () {
        document.addEventListener('scroll', updateAfterScroll);
        updatePlacement();
        return function () {
            document.removeEventListener('scroll', updateAfterScroll);
        };
    });
    React.useEffect(function () {
        updatePlacement(initialPlacement);
    }, [initialPlacement]);
    React.useEffect(function () {
        updatePlacement();
    }, [isActive]);
    return [placement, updatePlacement];
};

exports.DropdownElementOrientation = void 0;
(function (DropdownElementOrientation) {
    DropdownElementOrientation["notSet"] = "not-set";
    DropdownElementOrientation["left"] = "left";
    DropdownElementOrientation["right"] = "right";
    DropdownElementOrientation["center"] = "center";
})(exports.DropdownElementOrientation || (exports.DropdownElementOrientation = {}));
exports.DropdownAlignment = void 0;
(function (DropdownAlignment) {
    DropdownAlignment["top"] = "top";
    DropdownAlignment["center"] = "center";
    DropdownAlignment["bottom"] = "bottom";
})(exports.DropdownAlignment || (exports.DropdownAlignment = {}));
var Keys;
(function (Keys) {
    Keys["up"] = "ArrowUp";
    Keys["down"] = "ArrowDown";
    Keys["enter"] = "Enter";
})(Keys || (Keys = {}));

var useAttachedDomPosition = function (el, elToAttach, placement, actionElementPosition, marginFromAttached, initialTop, initialLeft) {
    var defaultPosition = { top: 0, left: 0 };
    // top already has margin: 8px;
    var defaultMarginTop = -8;
    var _a = React.useState({
        top: initialTop || defaultPosition.top,
        left: initialLeft || defaultPosition.left,
    }), actionElementDOMPosition = _a[0], setActionElementDOMPos = _a[1];
    var getLeft = function (left, right, widthActionEl) {
        if (actionElementPosition === exports.DropdownElementOrientation.center) {
            var widthAttachedEl = elToAttach.current.getBoundingClientRect().width;
            var offset = Math.abs((widthAttachedEl - widthActionEl) / 2);
            return left - offset + window.scrollX;
        }
        return ((actionElementPosition === exports.DropdownElementOrientation.right
            ? left
            : right) + window.scrollX);
    };
    var updateActionElementDOMPosition = function () {
        if (el && el.current && elToAttach && elToAttach.current) {
            var _a = el.current.getBoundingClientRect(), bottom = _a.bottom, right = _a.right, top_1 = _a.top, left = _a.left, widthActionEl = _a.width;
            var updatedDOMPos = {
                top: (placement === exports.DropdownMenuPlacement.bottom
                    ? bottom - (marginFromAttached || defaultMarginTop)
                    : top_1 + (marginFromAttached || defaultMarginTop)) + window.scrollY,
                left: getLeft(left, right, widthActionEl),
            };
            setActionElementDOMPos({
                top: updatedDOMPos.top,
                left: updatedDOMPos.left,
            });
        }
    };
    React.useLayoutEffect(function () {
        window.addEventListener('resize', updateActionElementDOMPosition);
        window.addEventListener('scroll', updateActionElementDOMPosition, true);
        updateActionElementDOMPosition();
        return function () {
            window.removeEventListener('resize', updateActionElementDOMPosition);
            window.removeEventListener('scroll', updateActionElementDOMPosition);
        };
    }, [placement, actionElementPosition, elToAttach.current]);
    return [actionElementDOMPosition, updateActionElementDOMPosition];
};

exports.DropdownTest = void 0;
(function (DropdownTest) {
    DropdownTest["prefix"] = "Dropdown";
    DropdownTest["container"] = "Dropdown__container";
})(exports.DropdownTest || (exports.DropdownTest = {}));
var Dropdown = function (_a) {
    var _b;
    var actionElement = _a.actionElement, _c = _a.portalId, portalId = _c === void 0 ? 'portal-dropdown' : _c, useSinglePortalHook = _a.useSinglePortalHook, _d = _a.menuPlacement, menuPlacement = _d === void 0 ? exports.DropdownMenuPlacement.bottom : _d, _e = _a.fixedMenuPlacement, fixedMenuPlacement = _e === void 0 ? false : _e, _f = _a.attachedMenuOriention, attachedMenuOriention = _f === void 0 ? exports.DropdownElementOrientation.left : _f, _g = _a.showSelectedOption, showSelectedOption = _g === void 0 ? true : _g, id = _a.id, active = _a.active, _h = _a.className, className = _h === void 0 ? '' : _h, _j = _a.classNameList, classNameList = _j === void 0 ? '' : _j, disabled = _a.disabled, asModal = _a.asModal, header = _a.header, footer = _a.footer, testId = _a.testId, maxHeight = _a.maxHeight, maxWidth = _a.maxWidth, minWidth = _a.minWidth, _k = _a.showScrollbars, showScrollbars = _k === void 0 ? true : _k, alignment = _a.alignment, _l = _a.enableRemoveScroll, enableRemoveScroll = _l === void 0 ? true : _l, onChange = _a.onChange, onActiveChange = _a.onActiveChange, onClose = _a.onClose, onOpen = _a.onOpen, children = _a.children;
    var dropdownList = React__default["default"].Children.toArray(children);
    var dropdownRef = React.useRef(document.createElement('ul'));
    var actionElementRef = React.useRef(document.createElement('div'));
    var activeOptionRef = React.useRef(document.createElement('li'));
    var containerRef = React.useRef(document.createElement('div'));
    var target = usePortal(portalId, useSinglePortalHook);
    var _m = useDetectOutsideClick([actionElementRef, containerRef], false), isActive = _m[0], setIsActive = _m[1], prevIsActive = _m[2];
    var placement = useFloatingPlacement(dropdownRef, menuPlacement, isActive, fixedMenuPlacement)[0];
    var _o = React.useState({
        ndx: 0,
        reason: 'unset',
    }), activeOption = _o[0], setActiveOption = _o[1];
    var _p = React.useState(false), isScrolling = _p[0], setIsScrolling = _p[1];
    var _q = useAttachedDomPosition(actionElementRef, dropdownRef, placement, attachedMenuOriention), actionElementDOMPosition = _q[0], updateActionElementDOMPosition = _q[1];
    var findIndexById = function (value) {
        return dropdownList.findIndex(function (child) { return child.props.value === value; });
    };
    var listStyles = {
        overflow: 'hidden',
        overflowY: 'auto',
        maxHeight: maxHeight,
        maxWidth: maxWidth,
        minWidth: minWidth,
    };
    var selected = dropdownList.length
        ? (_b = dropdownList.find(function (child) { return child.props.selected; })) === null || _b === void 0 ? void 0 : _b.props.value
        : undefined;
    var isActiveElementVisible = function () {
        return activeOptionRef.current.offsetTop > dropdownRef.current.scrollTop &&
            activeOptionRef.current.offsetTop <
                dropdownRef.current.scrollTop + dropdownRef.current.offsetHeight;
    };
    var startScrolling = function () {
        setIsScrolling(true);
        // Do not trigger onMouseEnter handler while is scrolling.
        setTimeout(function () {
            setIsScrolling(false);
        }, 200);
    };
    var handleScroll = function () {
        if (!isActive || !activeOptionRef.current || !dropdownRef.current)
            return;
        var activeOptionHeight = activeOptionRef.current.offsetHeight;
        if (activeOption.reason === Keys.down && !isActiveElementVisible()) {
            startScrolling();
            dropdownRef.current.scrollTop += activeOptionHeight;
        }
        else if (activeOption.reason === Keys.up && !isActiveElementVisible()) {
            startScrolling();
            dropdownRef.current.scrollTop -= activeOptionHeight;
        }
        else if (activeOption.reason === 'unset' && !isActiveElementVisible()) {
            // when there is a selected option by default
            startScrolling();
            // UX: having overflowed dropdown, we remove slight scroll from the active
            // option to indicate that there are more options above the active option.
            // This will show half of the previous option from active.
            var padScroll = activeOptionHeight / 2;
            dropdownRef.current.scrollTop +=
                activeOption.ndx * activeOptionHeight - padScroll;
        }
    };
    var handleMouseEnterOption = function (ev, value) {
        if (!isActive || isScrolling)
            return;
        setActiveOption({ ndx: findIndexById(value), reason: 'cursor' });
    };
    var handleChange = function (value) {
        if (!dropdownList || disabled) {
            return;
        }
        setIsActive(false);
        if (onChange) {
            onChange(value);
        }
    };
    var handleKeydown = function (ev) {
        if (!isActive)
            return;
        if (ev.key === Keys.down && activeOption.ndx < dropdownList.length - 1) {
            setActiveOption({ ndx: activeOption.ndx + 1, reason: ev.key });
        }
        if (ev.key === Keys.up && activeOption.ndx > 0) {
            setActiveOption({ ndx: activeOption.ndx - 1, reason: ev.key });
        }
        if (ev.key === Keys.enter) {
            var dropdownOption = dropdownList[activeOption.ndx];
            if (dropdownOption && !dropdownOption.props.disabled) {
                handleChange(dropdownOption.props.value);
            }
        }
        if (ev.ctrlKey) {
            var dropdownOption = dropdownList.find(function (child) { return child.props.assignKey === ev.key; });
            if (dropdownOption && !dropdownOption.props.disabled) {
                handleChange(dropdownOption.props.value);
            }
        }
    };
    var handleActionElementClick = function () {
        if (disabled) {
            return;
        }
        setIsActive(!isActive);
        if (!isActive) {
            setActiveOption({ ndx: 0, reason: 'unset' });
        }
    };
    React.useEffect(function () {
        handleScroll();
    }, [activeOption]);
    React.useEffect(function () {
        if (prevIsActive !== isActive) {
            if (onActiveChange) {
                onActiveChange(isActive);
            }
            if (!isActive && onClose) {
                onClose();
            }
            if (isActive && onOpen) {
                onOpen();
            }
        }
    }, [isActive, menuPlacement]);
    React.useEffect(function () {
        if (disabled) {
            setIsActive(false);
        }
    }, [disabled]);
    React.useEffect(function () {
        if (active === true && !isActive) {
            setIsActive(true);
        }
    }, [active]);
    // If there is a default selected option, when opening
    // the dropdown we set it to active and scroll to it.
    React.useEffect(function () {
        if (isActive && selected) {
            setActiveOption({ reason: 'unset', ndx: findIndexById(selected) });
        }
    }, [isActive, selected]);
    React.useEffect(function () {
        document.addEventListener('keydown', handleKeydown);
        return function () {
            document.removeEventListener('keydown', handleKeydown);
        };
    });
    var renderActionElement = function () {
        var triggerClassNames = 'rck-dropdown__action-name-container';
        var sel = dropdownList.find(function (child) { return child.props.value === selected; });
        if (actionElement) {
            return React__default["default"].createElement("div", { className: triggerClassNames }, actionElement);
        }
        return (React__default["default"].createElement("div", { className: triggerClassNames },
            React__default["default"].createElement(DropdownActionElement, { text: sel === null || sel === void 0 ? void 0 : sel.props.text, icon: sel === null || sel === void 0 ? void 0 : sel.props.icon, disabled: disabled })));
    };
    var getListStyles = function () {
        var _a = actionElementRef.current.getBoundingClientRect(), right = _a.right, left = _a.left;
        return __assign(__assign({}, listStyles), { minWidth: asModal ? '' : "".concat(minWidth || right - left, "px"), maxWidth: asModal ? '' : "".concat(maxWidth || right - left, "px") });
    };
    var getList = function () {
        return !disabled && isActive && dropdownList.length ? (React__default["default"].createElement("div", { className: classNames(asModal ? 'rck-dropdown__modal-overlay' : '') },
            React__default["default"].createElement("div", { className: classNames('rck-dropdown-list-container', asModal ? 'rck-dropdown-list-container--modal' : '', classNameList), style: portalId && !asModal ? actionElementDOMPosition : {}, "data-testid": getTestId(exports.DropdownTest.prefix, testId), ref: containerRef },
                React__default["default"].createElement("ul", { ref: dropdownRef, className: classNames('rck-dropdown__list', "rck-dropdown__list--".concat(placement), "rck-dropdown__list--attached", "rck-dropdown__list--".concat(asModal
                        ? exports.DropdownElementOrientation.notSet
                        : attachedMenuOriention), !showScrollbars ? 'rck-dropdown__list--no-scrollbar' : ''), role: "listbox", style: getListStyles(), id: id },
                    header && (React__default["default"].createElement(DropdownHeader, { testId: testId }, header)),
                    React__default["default"].Children.map(children, function (child, ndx) {
                        var dropdownOption = child;
                        if (dropdownOption === null) {
                            return null;
                        }
                        if (dropdownOption.type !== DropdownOption) {
                            throw new Error('Dropdown must contain DropdownOptions');
                        }
                        if (dropdownOption.props.value === selected && !actionElement) {
                            return null;
                        }
                        return React__default["default"].cloneElement(dropdownOption, {
                            onClick: function (value, event) {
                                if (dropdownOption.props.onClick) {
                                    dropdownOption.props.onClick(value, event);
                                }
                                handleChange(value);
                            },
                            selected: dropdownOption.props.value === selected,
                            onMouseEnter: handleMouseEnterOption,
                            active: ndx === activeOption.ndx,
                            ref: ndx === activeOption.ndx ? activeOptionRef : undefined,
                            showAsSelected: showSelectedOption,
                            testId: testId,
                        });
                    })),
                footer && (React__default["default"].createElement(DropdownFooter, { onClick: handleActionElementClick, testId: testId }, footer))))) : null;
    };
    var getListWithRemoveScroll = function () {
        var list = getList();
        return list ? React__default["default"].createElement(reactRemoveScroll.RemoveScroll, null, list) : null;
    };
    return (React__default["default"].createElement("div", { className: classNames('rck-dropdown', alignment ? "rck-dropdown--".concat(alignment) : '', className), "data-testid": getTestId(exports.DropdownTest.prefix, testId, exports.DropdownTest.container) },
        React__default["default"].cloneElement(renderActionElement(), {
            onClick: function () {
                updateActionElementDOMPosition();
                handleActionElementClick();
            },
            ref: actionElementRef,
        }),
        portalId &&
            target &&
            ReactDOM__default["default"].createPortal(enableRemoveScroll ? getListWithRemoveScroll() : getList(), target)));
};
Dropdown.displayName = 'Dropdown';

exports.CloudListTest = void 0;
(function (CloudListTest) {
    CloudListTest["prefix"] = "CloudList";
})(exports.CloudListTest || (exports.CloudListTest = {}));
var CloudList = function (_a) {
    var allowedListItemType = _a.allowedListItemType, _b = _a.listItemMaxWidth, listItemMaxWidth = _b === void 0 ? '100%' : _b, children = _a.children, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId;
    return (React__namespace.createElement("ul", { className: classNames('rck-cloud-list', "rck-cloud-list--maxwidth-".concat(listItemMaxWidth.replace('%', 'pc')), className), "data-testid": getTestId(exports.CloudListTest.prefix, testId) }, React__namespace.Children.map(children, function (child) {
        var listItem = child;
        if (listItem === null) {
            return null;
        }
        if (listItem.type !== allowedListItemType) {
            throw new Error("CloudList children have an invalid type: ".concat(listItem.type));
        }
        return React__namespace.createElement("li", null, child);
    })));
};
CloudList.displayName = 'CloudList';

exports.QuoteTest = void 0;
(function (QuoteTest) {
    QuoteTest["prefix"] = "Quote";
})(exports.QuoteTest || (exports.QuoteTest = {}));
var Quote = function (_a) {
    var _b = _a.maxLines, maxLines = _b === void 0 ? 'one' : _b, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId, children = _a.children;
    return (React__default["default"].createElement("div", { className: classNames('rck-quote', "rck-quote--".concat(maxLines), className), "data-testid": getTestId(exports.QuoteTest.prefix, testId) },
        React__default["default"].createElement("i", null, children)));
};
Quote.displayName = 'Quote';
var Quote$1 = React__default["default"].memo(Quote);

var useDomAnimation = function (el, animationClass, milliseconds, trigger, disabled) {
    if (milliseconds === void 0) { milliseconds = 200; }
    if (trigger === void 0) { trigger = undefined; }
    if (disabled === void 0) { disabled = false; }
    React.useEffect(function () {
        if (el && el.current) {
            if (!disabled && !el.current.classList.contains(animationClass)) {
                el.current.classList.add(animationClass);
                setTimeout(function () {
                    if (el && el.current) {
                        el.current.classList.remove(animationClass);
                    }
                }, milliseconds);
            }
        }
    }, trigger);
};

exports.TabPanelTest = void 0;
(function (TabPanelTest) {
    TabPanelTest["prefix"] = "TabPanel";
})(exports.TabPanelTest || (exports.TabPanelTest = {}));
var TabPanel = function (_a) {
    var name = _a.name, _b = _a.className, className = _b === void 0 ? '' : _b, animateWhenChange = _a.animateWhenChange, testId = _a.testId, children = _a.children;
    var tabPanelRef = React.useRef(document.createElement('div'));
    useDomAnimation(tabPanelRef, 'rck-tab-panel--entering', 200, [animateWhenChange], !animateWhenChange);
    return (React__namespace.createElement("div", { id: name, className: classNames("rck-tab-panel", className), role: "tabpanel", "data-testid": getTestId(exports.TabPanelTest.prefix, testId, name), ref: tabPanelRef }, children));
};
TabPanel.displayName = 'TabPanel';

exports.TabsTest = void 0;
(function (TabsTest) {
    TabsTest["prefix"] = "Tabs";
    TabsTest["tab"] = "Tab";
})(exports.TabsTest || (exports.TabsTest = {}));
var Tabs = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, onChange = _a.onChange, animateOnChange = _a.animateOnChange, testId = _a.testId, children = _a.children, _c = _a.showSeparator, showSeparator = _c === void 0 ? true : _c, _d = _a.hugContent, hugContent = _d === void 0 ? false : _d;
    var handleClick = function (name, disabled) { return function () {
        if (onChange && !disabled) {
            onChange(name);
        }
    }; };
    var panel = null;
    var name;
    return (React__namespace.createElement("div", { className: classNames("rck-tabs", hugContent ? 'rck-tabs--hug-content' : '', className), "data-testid": getTestId(exports.TabsTest.prefix, testId) },
        React__namespace.createElement("ul", { className: "rck-tabs__list" }, React__namespace.Children.map(children, function (child) {
            var tabPanel = child;
            if (tabPanel === null) {
                return null;
            }
            if (tabPanel.type !== TabPanel) {
                throw Error('Tabs must containn TabPanel children');
            }
            var isActive = !tabPanel.props.disabled && tabPanel.props.active;
            var activeColor = isActive && tabPanel.props.activeColor
                ? tabPanel.props.activeColor
                : null;
            if (isActive) {
                name = tabPanel.props.name;
                panel = tabPanel;
            }
            return (React__namespace.createElement("li", { className: classNames("rck-tab", isActive ? "rck-tab--active" : '', activeColor ? "rck-tab--active-color-".concat(activeColor) : '', tabPanel.props.disabled ? 'rck-tab--disabled' : '', !showSeparator ? 'rck-tab--no-separator' : '', className), "data-name": tabPanel.props.name, role: "tab", key: tabPanel.props.name, onClick: handleClick(tabPanel.props.name, tabPanel.props.disabled), onKeyDown: handleClick(tabPanel.props.name, tabPanel.props.disabled), tabIndex: 0, "data-testid": getTestId(exports.TabsTest.tab, testId, tabPanel.props.name) }, isActive ? (React__namespace.createElement("strong", null, tabPanel.props.content)) : (tabPanel.props.content)));
        })),
        panel &&
            React__namespace.cloneElement(panel, {
                animateWhenChange: animateOnChange ? name : undefined,
            })));
};
Tabs.displayName = 'Tabs';

exports.InstructionBlockTest = void 0;
(function (InstructionBlockTest) {
    InstructionBlockTest["prefix"] = "InstructionBlock";
})(exports.InstructionBlockTest || (exports.InstructionBlockTest = {}));
var InstructionBlock = function (_a) {
    var block = _a.block, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId, children = _a.children;
    return (React__default["default"].createElement("div", { "data-testid": getTestId(exports.InstructionBlockTest.prefix, testId), className: classNames('rck-instruction-block', block ? 'rck-instruction-block--block' : '', className) }, children));
};
InstructionBlock.displayName = 'InstructionBlock';
var InstructionBlock$1 = React__default["default"].memo(InstructionBlock);

exports.ContentSeparatorTest = void 0;
(function (ContentSeparatorTest) {
    ContentSeparatorTest["prefix"] = "ContentSeparator";
})(exports.ContentSeparatorTest || (exports.ContentSeparatorTest = {}));
var ContentSeparator = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId;
    return (React__namespace.createElement("div", { className: classNames('rck-content-separator', className), "data-testid": getTestId(exports.ContentSeparatorTest.prefix, testId) }));
};
ContentSeparator.displayName = 'ContentSeparator';
var ContentSeparator$1 = React__namespace.memo(ContentSeparator);

exports.InstructionCardTest = void 0;
(function (InstructionCardTest) {
    InstructionCardTest["prefix"] = "InstructionCard";
})(exports.InstructionCardTest || (exports.InstructionCardTest = {}));
var IconAsLink = function (_a) {
    var asLink = _a.asLink, href = _a.href;
    return asLink ? (React__default["default"].createElement(Link$1, { href: href },
        React__default["default"].createElement(Icon$1, { type: IconType.angleRight, color: exports.IconColor.regular, size: IconSize.x2 }))) : (React__default["default"].createElement(Icon$1, { type: IconType.angleRight, color: exports.IconColor.regular, size: IconSize.x2 }));
};
var InstructionCard = function (_a) {
    var iconType = _a.iconType, iconColor = _a.iconColor, title = _a.title, description = _a.description, instructions = _a.instructions, url = _a.url, _b = _a.compact, compact = _b === void 0 ? false : _b, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId, _d = _a.iconAsLink, iconAsLink = _d === void 0 ? false : _d;
    return (React__default["default"].createElement(FlexLayout, { mainaxis: "column", crossaxisAlignment: "center", "data-testid": getTestId(exports.InstructionCardTest.prefix, testId), className: classNames('rck-instruction-card', compact ? 'rck-instruction-card--compact' : '', className) },
        React__default["default"].createElement(Icon$1, { type: iconType, color: iconColor, size: compact ? IconSize.x2 : IconSize.x3, className: "rck-instruction-card__icon" }),
        React__default["default"].createElement("p", { className: "rck-instruction-card__title" }, title),
        !compact && (React__default["default"].createElement("p", { className: "rck-instruction-card__description" }, description)),
        url && (React__default["default"].createElement(React__default["default"].Fragment, null,
            React__default["default"].createElement(FlexLayout, { mainaxis: "row", mainaxisAlignment: "space-between", crossaxisAlignment: "center", alignSelf: "stretch", paddingBottom: "8" },
                React__default["default"].createElement(Link$1, { href: url, className: "rck-instruction-card__link" }, instructions),
                React__default["default"].createElement(IconAsLink, { asLink: iconAsLink, href: url })),
            React__default["default"].createElement(ContentSeparator$1, null)))));
};
InstructionCard.displayName = 'InstructionCard';

exports.StyledInstructionTest = void 0;
(function (StyledInstructionTest) {
    StyledInstructionTest["prefix"] = "StyledInstruction";
})(exports.StyledInstructionTest || (exports.StyledInstructionTest = {}));
var StyledInstruction = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId, children = _a.children, color = _a.color;
    return (React__default["default"].createElement("div", { "data-testid": getTestId(exports.StyledInstructionTest.prefix, testId), className: classNames('rck-styled-instruction', color ? "rck-styled-instruction--color-".concat(color) : '', className) }, children));
};
StyledInstruction.displayName = 'StyledInstruction';
var StyledInstruction$1 = React__default["default"].memo(StyledInstruction);

exports.SelectionBarTest = void 0;
(function (SelectionBarTest) {
    SelectionBarTest["prefix"] = "SelectionBar";
})(exports.SelectionBarTest || (exports.SelectionBarTest = {}));
var SelectionBar = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, title = _a.title, testId = _a.testId, hasNext = _a.hasNext, hasPrev = _a.hasPrev, onClickNext = _a.onClickNext, onClickPrev = _a.onClickPrev;
    return (React__namespace.createElement("div", { className: classNames(className, 'rck-selectionbar'), "data-testid": getTestId(exports.SelectionBarTest.prefix, testId) },
        React__namespace.createElement("div", { className: "rck-selectionbar__content" },
            React__namespace.createElement(TransparentButton$1, { onClick: onClickPrev, className: classNames('rck-selectionbar__content__button', !hasPrev ? 'rck-selectionbar__content__button--hidden' : '') },
                React__namespace.createElement(Icon$1, { type: IconType.angleLeft, color: exports.IconColor.regular, size: IconSize.lg })),
            React__namespace.createElement(InstructionBlock$1, { className: "rck-selectionbar__content__title", block: false }, title),
            React__namespace.createElement(TransparentButton$1, { onClick: onClickNext, className: classNames('rck-selectionbar__content__button', !hasNext ? 'rck-selectionbar__content__button--hidden' : '') },
                React__namespace.createElement(Icon$1, { type: IconType.angleRight, color: exports.IconColor.regular, size: IconSize.lg })))));
};
SelectionBar.displayName = 'SelectionBar';

exports.SpinnerTest = void 0;
(function (SpinnerTest) {
    SpinnerTest["prefix"] = "Spinner";
})(exports.SpinnerTest || (exports.SpinnerTest = {}));
var Spinner = function (_a) {
    var size = _a.size, color = _a.color, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId;
    return (React__namespace.createElement("div", { className: classNames('rck-spinner', "rck-spinner--size-".concat(size), "rck-spinner--color-".concat(color), className), "data-testid": getTestId(exports.SpinnerTest.prefix, testId) },
        React__namespace.createElement("div", { className: "rck-spinner__bounce1" }),
        React__namespace.createElement("div", { className: "rck-spinner__bounce2" }),
        React__namespace.createElement("div", { className: "rck-spinner__bounce3" })));
};
Spinner.displayName = 'Spinner';

/* eslint-disable react/jsx-wrap-multilines */
exports.GroupHeaderTest = void 0;
(function (GroupHeaderTest) {
    GroupHeaderTest["prefix"] = "GroupHeader";
})(exports.GroupHeaderTest || (exports.GroupHeaderTest = {}));
var GroupHeader = function (_a) {
    var onClickHelp = _a.onClickHelp, helpUrl = _a.helpUrl, useAsLink = _a.useAsLink, children = _a.children, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId;
    return (React__default["default"].createElement("div", { className: classNames('rck-group-header', className), "data-testid": getTestId(exports.GroupHeaderTest.prefix, testId) },
        React__default["default"].createElement(Header, { type: "h4" }, children),
        (onClickHelp || helpUrl) && (React__default["default"].createElement("div", { className: "rck-group-header__action-element" },
            !helpUrl && (React__default["default"].createElement(TransparentButton$1, { onClick: onClickHelp },
                React__default["default"].createElement(Icon$1, { type: IconType.questionCircle, color: exports.IconColor.regular }))),
            helpUrl && (React__default["default"].createElement(Link$1, { href: helpUrl, useAsLink: useAsLink },
                React__default["default"].createElement(Icon$1, { type: IconType.questionCircle, color: exports.IconColor.regular })))))));
};
GroupHeader.displayName = 'GroupHeader';
var GroupHeader$1 = React__default["default"].memo(GroupHeader);

exports.LabelTest = void 0;
(function (LabelTest) {
    LabelTest["prefix"] = "Label";
})(exports.LabelTest || (exports.LabelTest = {}));
var Label = function (_a) {
    var htmlFor = _a.htmlFor, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children, testId = _a.testId, ellipsis = _a.ellipsis;
    return (React__default["default"].createElement("label", { htmlFor: htmlFor, className: classNames('rck-label', ellipsis ? 'rck-label--ellipsis' : '', className), "data-testid": getTestId(exports.LabelTest.prefix, testId) }, children));
};
Label.displayName = 'Label';
var Label$1 = React__default["default"].memo(Label);

var EmptyPlaceholderTest;
(function (EmptyPlaceholderTest) {
    EmptyPlaceholderTest["prefix"] = "EmptyPlaceholder";
})(EmptyPlaceholderTest || (EmptyPlaceholderTest = {}));
var EmptyPlaceholder = function (_a) {
    var text = _a.text, icon = _a.icon, coloredBackground = _a.coloredBackground, fillAvailableSpace = _a.fillAvailableSpace, _b = _a.maxWidth, maxWidth = _b === void 0 ? '100%' : _b, centered = _a.centered, smallText = _a.smallText, _c = _a.className, className = _c === void 0 ? '' : _c, testId = _a.testId;
    return (React__default["default"].createElement("div", { className: classNames('rck-empty-placeholder', fillAvailableSpace ? 'rck-empty-placeholder--fill' : '', centered ? 'rck-empty-placeholder--center' : '', coloredBackground ? 'rck-empty-placeholder--colored-background' : '', smallText ? 'rck-empty-placeholder--small-text' : '', className), "data-testid": getTestId(EmptyPlaceholderTest.prefix, testId) },
        React__default["default"].createElement(FlexLayout, { mainaxis: "column", maxWidth: maxWidth },
            icon && React__default["default"].createElement("div", { className: "rck-empty-placeholder__icon" }, icon),
            text)));
};
EmptyPlaceholder.displayName = 'EmptyPlaceholder';
var EmptyPlaceholder$1 = React__default["default"].memo(EmptyPlaceholder);

exports.DeactivableContentTest = void 0;
(function (DeactivableContentTest) {
    DeactivableContentTest["prefix"] = "DeactivableListItem";
})(exports.DeactivableContentTest || (exports.DeactivableContentTest = {}));
var DeactivableContent = function (_a) {
    var _b = _a.disabled, disabled = _b === void 0 ? false : _b, testId = _a.testId, children = _a.children, _c = _a.className, className = _c === void 0 ? '' : _c;
    var listItemRef = React.useRef(document.createElement('div'));
    useElementDisable(listItemRef, disabled, 'rck-deactivable-content--disabled', 'rck-deactivable-content__overlap');
    return (React__default["default"].createElement("div", { "data-testid": getTestId(exports.DeactivableContentTest.prefix, testId), ref: listItemRef, className: classNames('rck-deactivable-content', className), "aria-disabled": disabled }, children));
};
DeactivableContent.displayName = 'DeactivableContent';

exports.SliderTest = void 0;
(function (SliderTest) {
    SliderTest["prefix"] = "Slider";
})(exports.SliderTest || (exports.SliderTest = {}));
var Slider = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, name = _a.name, id = _a.id, disabled = _a.disabled, block = _a.block, minValue = _a.minValue, maxValue = _a.maxValue, value = _a.value, testId = _a.testId, formatLabel = _a.formatLabel, onInput = _a.onInput, onChange = _a.onChange;
    var inputRef = React__namespace.useRef(document.createElement('input'));
    var handleOnInput = function (ev) {
        if (!disabled) {
            if (onInput) {
                onInput(ev, inputRef.current.value);
            }
        }
    };
    var handleOnChange = function (ev) {
        if (!disabled) {
            if (onChange) {
                onChange(ev, inputRef.current.value);
            }
        }
    };
    return (React__namespace.createElement("div", { className: classNames(className, 'rck-slider', block ? 'rck-slider--block' : '', disabled ? 'rck-slider--disabled' : '') },
        React__namespace.createElement("label", { htmlFor: id, "data-testid": getTestId(exports.SliderTest.prefix, testId) },
            value,
            " ",
            formatLabel),
        React__namespace.createElement("input", { id: id, className: "rck-slider", name: name, ref: inputRef, type: "range", min: minValue, max: maxValue, value: value, onInput: handleOnInput, onChange: handleOnChange })));
};
Slider.displayName = 'Slider';

exports.DomainTrustSealTest = void 0;
(function (DomainTrustSealTest) {
    DomainTrustSealTest["prefix"] = "DomainTrustSeal";
})(exports.DomainTrustSealTest || (exports.DomainTrustSealTest = {}));
var getConfigByReputation = function (mode, level) {
    switch (level) {
        case 'excellent':
            return {
                icon: mode === 'trustworthiness' ? (React__default["default"].createElement(Icon$1, { type: IconType.grin, color: exports.IconColor.success, size: IconSize.x2 })) : (React__default["default"].createElement(Icon$1, { type: IconType.thumbsUp, color: exports.IconColor.success, size: IconSize.x2 })),
                className: 'good',
            };
        case 'good':
            return {
                icon: mode === 'trustworthiness' ? (React__default["default"].createElement(Icon$1, { type: IconType.smile, color: exports.IconColor.success, size: IconSize.x2 })) : (React__default["default"].createElement(Icon$1, { type: IconType.thumbsUp, color: exports.IconColor.success, size: IconSize.x2 })),
                className: 'good',
            };
        case 'unsatisfactory':
            return {
                icon: mode === 'trustworthiness' ? (React__default["default"].createElement(Icon$1, { type: IconType.meh, color: exports.IconColor.warning, size: IconSize.x2 })) : (React__default["default"].createElement(Icon$1, { type: IconType.thumbsDown, color: exports.IconColor.warning, size: IconSize.x2 })),
                className: 'unsatisfactory',
            };
        case 'poor':
            return {
                icon: mode === 'trustworthiness' ? (React__default["default"].createElement(Icon$1, { type: IconType.frown, color: exports.IconColor.error, size: IconSize.x2 })) : (React__default["default"].createElement(Icon$1, { type: IconType.thumbsDown, color: exports.IconColor.error, size: IconSize.x2 })),
                className: 'poor',
            };
        case 'very-poor':
            return {
                icon: mode === 'trustworthiness' ? (React__default["default"].createElement(Icon$1, { type: IconType.sadTear, color: exports.IconColor.error, size: IconSize.x2 })) : (React__default["default"].createElement(Icon$1, { type: IconType.thumbsDown, color: exports.IconColor.error, size: IconSize.x2 })),
                className: 'poor',
            };
        default:
            return {
                icon: mode === 'trustworthiness' ? (React__default["default"].createElement(Icon$1, { type: IconType.questionCircle, color: exports.IconColor.neutral, size: IconSize.x2 })) : (React__default["default"].createElement(Icon$1, { type: IconType.questionCircle, color: exports.IconColor.neutral, size: IconSize.x2 })),
                className: 'neutral',
            };
    }
};
var DomainTrustSeal = function (_a) {
    var mode = _a.mode, _b = _a.level, level = _b === void 0 ? 'unknown' : _b, title = _a.title, levelDescription = _a.description, testId = _a.testId, _c = _a.className, className = _c === void 0 ? '' : _c;
    var configuration = getConfigByReputation(mode, level);
    return (React__default["default"].createElement("div", { "data-testid": getTestId(exports.DomainTrustSealTest.prefix, testId), className: classNames('rck-domain-trust', "rck-domain-trust--".concat(configuration.className), className) },
        React__default["default"].createElement("div", null, configuration.icon),
        React__default["default"].createElement("div", { className: "rck-domain-trust__info" },
            React__default["default"].createElement("p", { className: "rck-domain-trust__info__title" }, title),
            React__default["default"].createElement("p", { className: "rck-domain-trust__info__description" }, levelDescription))));
};
DomainTrustSeal.displayName = 'DomainTrustSeal';
var DomainTrustSeal$1 = React__default["default"].memo(DomainTrustSeal);

exports.SvgTest = void 0;
(function (SvgTest) {
    SvgTest["prefix"] = "Svg";
})(exports.SvgTest || (exports.SvgTest = {}));
var Svg = function (_a) {
    var height = _a.height, width = _a.width, color = _a.color, testId = _a.testId, children = _a.children, _b = _a.className, className = _b === void 0 ? '' : _b;
    return (React__default["default"].createElement("span", { className: classNames('rck-svg', height ? "rck-svg--size-height-".concat(height) : '', width ? "rck-svg--size-width-".concat(width) : '', color ? "rck-svg--color-".concat(color) : '', className), "data-testid": getTestId(exports.SvgTest.prefix, testId) }, children));
};
Svg.displayName = 'Svgs';
var Svg$1 = React__default["default"].memo(Svg);

exports.AffiliateBadgeTest = void 0;
(function (AffiliateBadgeTest) {
    AffiliateBadgeTest["prefix"] = "AffiliateBadge";
})(exports.AffiliateBadgeTest || (exports.AffiliateBadgeTest = {}));
var AffiliateBadge = function (_a) {
    var iconSrc = _a.iconSrc, description = _a.description, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b;
    return (React__namespace.createElement("div", { className: classNames('rck-affiliate-badge', className), "data-testid": getTestId(exports.AffiliateBadgeTest.prefix, testId) },
        React__namespace.createElement("img", { src: iconSrc, alt: description })));
};
AffiliateBadge.displayName = 'AffiliateBadge';
var AffiliateBadge$1 = React__namespace.memo(AffiliateBadge);

exports.PageTitleTest = void 0;
(function (PageTitleTest) {
    PageTitleTest["prefix"] = "PageTitle";
})(exports.PageTitleTest || (exports.PageTitleTest = {}));
var renderIcon = function (action) {
    var _a;
    var iconColor = (_a = action.iconColor) !== null && _a !== void 0 ? _a : exports.IconColor.black;
    var iconSize = IconSize.lg;
    if (action.icon)
        return action.icon;
    switch (action.actionName) {
        case 'menu':
            return React__default["default"].createElement(Icon$1, { type: IconType.bars, color: iconColor, size: iconSize });
        case 'back':
            return (React__default["default"].createElement(Icon$1, { type: IconType.arrowLeft, color: iconColor, size: iconSize }));
        case 'edit':
            return React__default["default"].createElement(Icon$1, { type: IconType.edit, color: iconColor, size: iconSize });
        case 'delete':
            return (React__default["default"].createElement(Icon$1, { type: IconType.trashAlt, color: iconColor, size: iconSize }));
        case 'search':
            return React__default["default"].createElement(Icon$1, { type: IconType.search, color: iconColor, size: iconSize });
        case 'settings':
            return React__default["default"].createElement(Icon$1, { type: IconType.cog, color: iconColor, size: iconSize });
        default:
            return action.icon
                ? React__default["default"].cloneElement(action.icon, { color: iconColor })
                : null;
    }
};
var renderAction = function (_a) {
    var action = _a.action, isRightSide = _a.isRightSide, testId = _a.testId;
    return (React__default["default"].createElement("div", { key: action.actionName, className: classNames('rck-page-title__action', isRightSide ? 'rck-page-title__action--right' : '') },
        action.location && (React__default["default"].createElement(Link$1, { href: action.location, onClick: function () {
                if (action.onClick)
                    action.onClick();
            } }, renderIcon(action))),
        !action.location && (React__default["default"].createElement(TransparentButton$1, { fillAvailableSpace: true, testId: getTestId(exports.PageTitleTest.prefix, testId, action.actionName), onClick: function () {
                if (action.onClick)
                    action.onClick();
            } }, renderIcon(action)))));
};
var PageTitleMedium = function (_a) {
    var title = _a.title, subtitle = _a.subtitle, profileInfoHeader = _a.profileInfoHeader, _b = _a.size, size = _b === void 0 ? 'regular' : _b, centered = _a.centered, _c = _a.className, className = _c === void 0 ? '' : _c, _d = _a.subtitleEllipsis, subtitleEllipsis = _d === void 0 ? false : _d, testId = _a.testId, backAction = _a.backAction, menuAction = _a.menuAction, otherActions = _a.otherActions;
    return (React__default["default"].createElement("div", { className: classNames('rck-page-title', "rck-page-title--".concat(size), className), "data-testid": getTestId(exports.PageTitleTest.prefix, testId) },
        React__default["default"].createElement("div", { className: "rck-page-title--left" },
            profileInfoHeader && (React__default["default"].createElement("div", { className: "rck-page-title__header-info" }, profileInfoHeader)),
            React__default["default"].createElement("div", { className: "rck-page-title__title-wrapper" },
                backAction && renderAction({ action: backAction, testId: testId }),
                title && (React__default["default"].createElement("div", { className: classNames("rck-page-title__title", backAction ? 'rck-page-title__title--level-2' : '', centered ? 'rck-page-title__title--centered' : '') }, title)),
                menuAction && renderAction({ action: menuAction, testId: testId }),
                otherActions && (otherActions === null || otherActions === void 0 ? void 0 : otherActions.length) !== 0 && (React__default["default"].createElement("div", { className: "rck-page-title--right" }, otherActions === null || otherActions === void 0 ? void 0 : otherActions.map(function (action) {
                    return renderAction({ action: action, isRightSide: true, testId: testId });
                }))))),
        subtitle && (React__default["default"].createElement("div", { className: classNames('rck-page-title__subtitle', centered ? 'rck-page-title__subtitle--centered' : '', subtitleEllipsis ? 'rck-page-title__subtitle--ellipsis' : '') }, subtitle))));
};
var PageTitleRegularOrSmall = function (_a) {
    var title = _a.title, subtitle = _a.subtitle, profileInfoHeader = _a.profileInfoHeader, _b = _a.size, size = _b === void 0 ? 'regular' : _b, _c = _a.subtitleEllipsis, subtitleEllipsis = _c === void 0 ? true : _c, backAction = _a.backAction, menuAction = _a.menuAction, otherActions = _a.otherActions, centered = _a.centered, testId = _a.testId;
    return (React__default["default"].createElement(React__default["default"].Fragment, null,
        React__default["default"].createElement("div", { className: "rck-page-title--left" },
            backAction && renderAction({ action: backAction, testId: testId }),
            menuAction && renderAction({ action: menuAction, testId: testId }),
            profileInfoHeader && (React__default["default"].createElement("div", { className: "rck-page-title__header-info" }, profileInfoHeader)),
            React__default["default"].createElement("div", { className: "rck-page-title__title-wrapper" },
                title && (React__default["default"].createElement("div", { className: classNames("rck-page-title__title", backAction ? 'rck-page-title__title--level-2' : '', centered ? 'rck-page-title__title--centered' : '') }, title)),
                subtitle && size === 'regular' && (React__default["default"].createElement("div", { className: classNames('rck-page-title__subtitle', centered ? 'rck-page-title__subtitle--centered' : '', subtitleEllipsis ? 'rck-page-title__subtitle--ellipsis' : '') }, subtitle)))),
        otherActions && (otherActions === null || otherActions === void 0 ? void 0 : otherActions.length) !== 0 && (React__default["default"].createElement("div", { className: "rck-page-title--right" }, otherActions === null || otherActions === void 0 ? void 0 : otherActions.map(function (action) {
            return renderAction({ action: action, isRightSide: true, testId: testId });
        })))));
};
var isOtherAction = function (action) {
    return action.actionName !== 'back' &&
        action.actionName !== 'menu' &&
        action.actionName !== '';
};
var hasNameAction = function (action) {
    return action.actionName !== '';
};
var PageTitle = function (_a) {
    var title = _a.title, subtitle = _a.subtitle, profileInfoHeader = _a.profileInfoHeader, _b = _a.size, size = _b === void 0 ? 'regular' : _b, _c = _a.subtitleEllipsis, subtitleEllipsis = _c === void 0 ? false : _c, actions = _a.actions, centered = _a.centered, _d = _a.className, className = _d === void 0 ? '' : _d, testId = _a.testId;
    var backAction = actions === null || actions === void 0 ? void 0 : actions.find(function (action) { return action.actionName === 'back'; });
    var menuAction = actions === null || actions === void 0 ? void 0 : actions.find(function (action) { return action.actionName === 'menu'; });
    var otherActions = actions === null || actions === void 0 ? void 0 : actions.filter(isOtherAction).filter(hasNameAction);
    return (React__default["default"].createElement("div", { className: classNames('rck-page-title', "rck-page-title--".concat(size), className), "data-testid": getTestId(exports.PageTitleTest.prefix, testId) }, size === 'medium' ? (React__default["default"].createElement(PageTitleMedium, { title: title, subtitle: subtitle, profileInfoHeader: profileInfoHeader, size: size, subtitleEllipsis: subtitleEllipsis, backAction: backAction, menuAction: menuAction, otherActions: otherActions, centered: centered, className: className, testId: testId })) : (React__default["default"].createElement(PageTitleRegularOrSmall, { title: title, subtitle: subtitle, profileInfoHeader: profileInfoHeader, size: size, subtitleEllipsis: subtitleEllipsis, backAction: backAction, menuAction: menuAction, otherActions: otherActions, centered: centered, className: className, testId: testId }))));
};
PageTitle.displayName = 'PageTitle';

exports.ProfileInfoHeaderTest = void 0;
(function (ProfileInfoHeaderTest) {
    ProfileInfoHeaderTest["prefix"] = "ProfileInfoHeader";
})(exports.ProfileInfoHeaderTest || (exports.ProfileInfoHeaderTest = {}));
var ProfileInfoHeader = function (_a) {
    var name = _a.name, avatar = _a.avatar, status = _a.status, statusMessage = _a.statusMessage, _b = _a.size, size = _b === void 0 ? 'regular' : _b, actionElement = _a.actionElement, onEdit = _a.onEdit, testId = _a.testId, _c = _a.className, className = _c === void 0 ? '' : _c;
    return (React__default["default"].createElement("div", { className: classNames('rck-profile-info-header', "rck-profile-info-header--".concat(size), className), "data-testid": getTestId(exports.ProfileInfoHeaderTest.prefix, testId) },
        React__default["default"].createElement("div", { className: "rck-profile-info-header__avatar" }, React__default["default"].cloneElement(avatar, {
            size: size === 'small' ? exports.AvatarSize.extraSmall : exports.AvatarSize.medium,
            icon: onEdit ? (React__default["default"].createElement(TransparentButton$1, { onClick: onEdit },
                React__default["default"].createElement(Icon$1, { type: IconType.pencil, color: exports.IconColor.greenDecorationContrast, family: exports.IconFamily.solid, circle: true }))) : (undefined),
        })),
        React__default["default"].createElement("div", { className: "rck-profile-info-header__content" },
            React__default["default"].createElement("div", { className: "rck-profile-info-header__content__name" }, name),
            React__default["default"].createElement("div", { className: classNames('rck-profile-info-header__content__status', "rck-profile-info-header__content__status--".concat(status)) }, statusMessage)),
        actionElement && (React__default["default"].createElement("div", { className: "rck-profile-info-header__action-element" }, actionElement))));
};
ProfileInfoHeader.displayName = 'ProfileInfoHeader';
var ProfileInfoHeader$1 = React__default["default"].memo(ProfileInfoHeader);

exports.LicenseInfoContainerTest = void 0;
(function (LicenseInfoContainerTest) {
    LicenseInfoContainerTest["prefix"] = "LicenseInfoContainer";
})(exports.LicenseInfoContainerTest || (exports.LicenseInfoContainerTest = {}));
var LicenseInfoContainer = function (_a) {
    var _b = _a.className, className = _b === void 0 ? '' : _b, affiliateBadgeDescription = _a.affiliateBadgeDescription, _c = _a.affiliateBadgeIconSrc, affiliateBadgeIconSrc = _c === void 0 ? '' : _c, licenseTitle = _a.licenseTitle, licenseSubtitle = _a.licenseSubtitle, licenseBadgeText = _a.licenseBadgeText, licenseBadgeClick = _a.licenseBadgeClick, licenseLinkTo = _a.licenseLinkTo, useAsLink = _a.useAsLink, testId = _a.testId;
    return (React__default["default"].createElement("div", { className: classNames('rck-license-info-container', className), "datatest-id": getTestId(exports.LicenseInfoContainerTest.prefix, testId) },
        React__default["default"].createElement("ul", { className: "rck-license-info-container__optionals" }, affiliateBadgeIconSrc ? (React__default["default"].createElement("li", { className: "rck-license-info-container__affiliate-badge" },
            React__default["default"].createElement(AffiliateBadge$1, { iconSrc: affiliateBadgeIconSrc, description: affiliateBadgeDescription }))) : (React__default["default"].createElement(Link$1, { href: licenseLinkTo, useAsLink: useAsLink },
            licenseTitle && (React__default["default"].createElement("li", { className: "rck-license-info-container__license-title" },
                React__default["default"].createElement("span", null, licenseTitle))),
            licenseSubtitle && (React__default["default"].createElement("li", { className: "rck-license-info-container__license-subtitle" },
                React__default["default"].createElement("span", null, licenseSubtitle))),
            licenseBadgeText && licenseBadgeClick && (React__default["default"].createElement("li", null,
                React__default["default"].createElement(FlexLayout, { mainaxis: "row", mainaxisAlignment: "center" },
                    React__default["default"].createElement("button", { className: "rck-license-info-container__button", type: "button", onClick: licenseBadgeClick },
                        React__default["default"].createElement("span", null, licenseBadgeText))))))))));
};
LicenseInfoContainer.displayName = 'LicenseInfoContainer';

exports.BannerTest = void 0;
(function (BannerTest) {
    BannerTest["prefix"] = "Banner";
    BannerTest["actionElement"] = "Action__close";
})(exports.BannerTest || (exports.BannerTest = {}));
var Banner = function (_a) {
    var type = _a.type, icon = _a.icon, showCloseIcon = _a.showCloseIcon, centered = _a.centered, fullWidth = _a.fullWidth, _b = _a.rounded, rounded = _b === void 0 ? true : _b, testId = _a.testId, _c = _a.className, className = _c === void 0 ? '' : _c, children = _a.children, onClose = _a.onClose;
    return (React__default["default"].createElement("div", { className: classNames('rck-banner', "rck-banner--".concat(type), fullWidth ? 'rck-banner--full-width' : '', !rounded ? 'rck-banner--no-rounded' : '', className), "data-testid": getTestId(exports.BannerTest.prefix, testId) },
        icon && React__default["default"].createElement("div", { className: "rck-banner__icon" }, icon),
        React__default["default"].createElement("div", { className: classNames('rck-banner__text', centered ? "rck-banner__text--centered" : '') }, children),
        showCloseIcon && (React__default["default"].createElement("div", { className: "rck-banner__icon-right", onClick: onClose, role: "button", onKeyDown: onClose, tabIndex: 0, "data-testid": getTestId(exports.BannerTest.prefix, testId, exports.BannerTest.actionElement) },
            React__default["default"].createElement(Icon$1, { type: IconType.times, size: IconSize.lg })))));
};
Banner.displayName = 'Banner';
var Banner$1 = React__default["default"].memo(Banner);

var SafeArea = function () { return React__namespace.createElement("div", { className: "rck-safe-area" }); };
SafeArea.displayName = 'SafeArea';

var ToastTest = {
    prefix: 'Toast',
};
var getToastIconType = function (type) {
    switch (type) {
        case 'warning':
            return IconType.exclamationTriangle;
        case 'error':
            return IconType.timesCircle;
        case 'success':
            return IconType.checkCircle;
        case 'info':
            return IconType.infoCircle;
        default:
            return undefined;
    }
};
var Toast = function (_a) {
    var message = _a.message, title = _a.title, _b = _a.type, type = _b === void 0 ? 'info' : _b, _c = _a.position, position = _c === void 0 ? 'bottom-center' : _c, icon = _a.icon, _d = _a.onClose, onClose = _d === void 0 ? function () { } : _d, testId = _a.testId, _e = _a.className, className = _e === void 0 ? '' : _e;
    var toastIcon = icon !== null && icon !== void 0 ? icon : getToastIconType(type);
    return (React__default["default"].createElement("div", { className: classNames('rck-toast', "rck-toast--position-".concat(position), "rck-toast--type-".concat(type), className), "data-testid": getTestId(ToastTest.prefix, testId) },
        React__default["default"].createElement("div", { className: "rck-toast__wrapper" },
            toastIcon && (React__default["default"].createElement("div", { className: "rck-toast__icon" }, icon !== null && icon !== void 0 ? icon : (React__default["default"].createElement(Icon$1, { type: getToastIconType(type), color: exports.IconColor.notSet, size: IconSize.x2, family: exports.IconFamily.solid })))),
            React__default["default"].createElement("div", { className: "rck-toast__content" },
                title && React__default["default"].createElement("h4", { className: "rck-toast__title" }, title),
                message && React__default["default"].createElement("div", { className: "rck-toast__message" }, message)),
            React__default["default"].createElement("div", { className: "rck-toast__close" },
                React__default["default"].createElement(TransparentButton$1, { onClick: onClose, className: "rck-toast__close-button" },
                    React__default["default"].createElement(Icon$1, { type: IconType.times, color: exports.IconColor.notSet, size: IconSize.lg }))))));
};
Toast.displayName = 'Toast';

var ToastManager = /** @class */ (function () {
    function ToastManager() {
        var _this = this;
        this.onCloseToastHandler = function () {
            _this.removeScheduledTaskToRemoveToast();
            _this.removeCurrentToast();
        };
        this.defaultPosition = 'bottom-center';
        this.defaultHook = 'portal-toasts';
        this.defaultTimeout = 4000;
        this.createPortal();
    }
    ToastManager.prototype.createPortal = function () {
        if (isTestEnv())
            return;
        if (document.getElementById(this.defaultHook) === null) {
            var portal = document.createElement('div');
            portal.setAttribute('id', this.defaultHook);
            document.body.appendChild(portal);
        }
    };
    ToastManager.prototype.removeCurrentToast = function () {
        if (isTestEnv())
            return;
        var currNotif = document.getElementById(this.defaultHook);
        if (currNotif === null || currNotif === void 0 ? void 0 : currNotif.firstChild) {
            ReactDOM__default["default"].unmountComponentAtNode(currNotif);
        }
    };
    ToastManager.prototype.scheduleTaskToRemoveToast = function (ms) {
        var _this = this;
        this.timeout = setTimeout(function () {
            _this.removeCurrentToast();
        }, ms !== null && ms !== void 0 ? ms : this.defaultTimeout);
    };
    ToastManager.prototype.removeScheduledTaskToRemoveToast = function () {
        if (this.timeout) {
            clearTimeout(this.timeout);
            this.timeout = undefined;
        }
    };
    ToastManager.getInstance = function () {
        if (!ToastManager.instance) {
            ToastManager.instance = new ToastManager();
        }
        return ToastManager.instance;
    };
    ToastManager.prototype.setDefaultPosition = function (position) {
        this.defaultPosition = position;
    };
    ToastManager.prototype.setDefaultHook = function (defaultHook) {
        var portal = document.getElementById(this.defaultHook);
        if (portal) {
            portal.remove();
        }
        this.defaultHook = defaultHook;
        this.createPortal();
    };
    ToastManager.prototype.setDefaultTimeout = function (ms) {
        this.defaultTimeout = ms;
    };
    ToastManager.prototype.sendWithoutTimeout = function (title, message, type, icon, onClose, position) {
        var _this = this;
        if (isTestEnv())
            return;
        ReactDOM__default["default"].render(React__default["default"].createElement(Toast, { message: message, type: type, title: title, position: position !== null && position !== void 0 ? position : this.defaultPosition, icon: icon, onClose: function (ev) {
                _this.onCloseToastHandler();
                if (onClose) {
                    onClose(ev);
                }
            } }), document.getElementById(this.defaultHook));
    };
    ToastManager.prototype.send = function (title, message, type, icon, position, timeout, onClose) {
        this.removeScheduledTaskToRemoveToast();
        this.removeCurrentToast();
        this.sendWithoutTimeout(title, message, type, icon, onClose, position);
        this.scheduleTaskToRemoveToast(timeout);
    };
    return ToastManager;
}());

exports.ExpandableCardTest = void 0;
(function (ExpandableCardTest) {
    ExpandableCardTest["prefix"] = "ExpandableCard";
})(exports.ExpandableCardTest || (exports.ExpandableCardTest = {}));
var ExpandableCardVertical = function (_a) {
    var _b = _a.opener, opener = _b === void 0 ? true : _b, children = _a.children, height = _a.height, testId = _a.testId;
    var _c = React__namespace.useState(true), closed = _c[0], setClosed = _c[1];
    var toggle = function () { return setClosed(function (isClosed) { return !isClosed; }); };
    return (React__namespace.createElement("div", { className: classNames('rck-expandable-card-vertical__container', closed
            ? 'rck-expandable-card-vertical__container--closed'
            : 'rck-expandable-card-vertical__container--opened'), style: { height: closed ? height : 'auto' } },
        React__namespace.createElement("div", { className: "rck-expandable-card-vertical__items", "data-testid": getTestId(exports.ExpandableCardTest.prefix, testId) }, children),
        opener ? (React__namespace.createElement("div", { className: "rck-expandable-card-vertical__icon" },
            React__namespace.createElement(Icon$1, { testId: getTestId(exports.ExpandableCardTest.prefix, testId), type: closed ? IconType.angleDown : IconType.angleUp, color: exports.IconColor.regular, onClick: toggle }))) : null));
};
var ExpandableCardHorizontal = function (_a) {
    var children = _a.children, testId = _a.testId;
    return (React__namespace.createElement("div", { className: "rck-expandable-card-horizontal__container", "data-testid": getTestId(exports.ExpandableCardTest.prefix, testId) }, children));
};
var ExpandableCard = function (_a) {
    var mode = _a.mode, children = _a.children, height = _a.height, opener = _a.opener, testId = _a.testId;
    return mode === 'vertical' ? (React__namespace.createElement(ExpandableCardVertical, { height: height, opener: opener, testId: testId }, children)) : (React__namespace.createElement(ExpandableCardHorizontal, { testId: testId }, children));
};

exports.FreeTextTest = void 0;
(function (FreeTextTest) {
    FreeTextTest["prefix"] = "FreeText";
})(exports.FreeTextTest || (exports.FreeTextTest = {}));
var FreeText = function (_a) {
    var fontSize = _a.fontSize, fontWeight = _a.fontWeight, color = _a.color, testId = _a.testId, children = _a.children, _b = _a.className, className = _b === void 0 ? '' : _b;
    return (React__default["default"].createElement("span", { className: classNames('rck-free-text', "rck-free-text--font-size-".concat(fontSize), "rck-free-text--color-".concat(color), "rck-free-text--font-weight-".concat(fontWeight), className), "data-testid": getTestId(exports.FreeTextTest.prefix, testId) }, children));
};
FreeText.displayName = 'FreeText';

var TextFieldBaseTest = {
    prefix: 'TextFieldBase',
    input: 'Input',
    contentRight: 'ContentRight',
};
var autoFocusElement = function (element) {
    // We add a slight delay to let the component settle down after re-renders
    setTimeout(function () {
        if (element === null || element === void 0 ? void 0 : element.select)
            element.select();
    }, 100);
};
var defaultErrorConfig$2 = {
    border: true,
    text: true,
};
var TextFieldBase = React__namespace.forwardRef(function (props, ref) {
    var _a;
    var testId = props.testId, required = props.required, _b = props.className, className = _b === void 0 ? '' : _b, _c = props.disabled, disabled = _c === void 0 ? false : _c, _d = props.hasError, hasError = _d === void 0 ? false : _d, _e = props.errorConfig, errorConfig = _e === void 0 ? defaultErrorConfig$2 : _e, _f = props.size, size = _f === void 0 ? 'small' : _f, _g = props.type, type = _g === void 0 ? 'text' : _g, _h = props.width, width = _h === void 0 ? 'block' : _h, _j = props.autoFocus, autoFocus = _j === void 0 ? false : _j, _k = props.selectAllOnFocus, selectAllOnFocus = _k === void 0 ? false : _k, contentLeft = props.contentLeft, contentRight = props.contentRight, children = props.children, onFocus = props.onFocus, onMouseDown = props.onMouseDown, inputProps = __rest(props, ["testId", "required", "className", "disabled", "hasError", "errorConfig", "size", "type", "width", "autoFocus", "selectAllOnFocus", "contentLeft", "contentRight", "children", "onFocus", "onMouseDown"]);
    var textInput = React__namespace.useRef(null);
    var containerRef = React__namespace.useRef(null);
    // Autofocus on mount
    React__namespace.useEffect(function () {
        if (autoFocus && (textInput === null || textInput === void 0 ? void 0 : textInput.current)) {
            textInput.current.focus();
        }
    }, [autoFocus]);
    var onFocusHandler = React__namespace.useCallback(function (e) {
        // Select all on focus
        if (selectAllOnFocus) {
            autoFocusElement(e.target);
        }
        // propagate
        onFocus === null || onFocus === void 0 ? void 0 : onFocus(e);
    }, [onFocus, selectAllOnFocus]);
    // Focus inner input component when parent is clicked
    var handleOnMouseDown = React__namespace.useCallback(function (e) {
        // Running e.preventDefault() on the INPUT prevents double click behaviour
        var target = e.target;
        if (target.tagName !== 'INPUT') {
            e.preventDefault();
        }
        setTimeout(function () {
            if ((textInput === null || textInput === void 0 ? void 0 : textInput.current) &&
                !disabled &&
                document.activeElement !== textInput.current) {
                textInput.current.focus();
            }
        }, 100);
        // propagate
        onMouseDown === null || onMouseDown === void 0 ? void 0 : onMouseDown(e);
    }, [onMouseDown, disabled]);
    /**
     * Function to determine if render a JSX component or
     * pass the current value to the passed in function.
     */
    var computedChildrenProp = React__namespace.useCallback(function () {
        if (!children)
            return null;
        // If the component recieves a JSX component, we render it
        if (React__namespace.isValidElement(children))
            return children;
        // If the component recieves a function that returns JSX component,
        // we pass the current value to the funtion.
        if (typeof children === 'function')
            return children(inputProps.value);
        return null;
    }, [children, inputProps.value]);
    var renderInputComponent = function () { return (React__namespace.createElement("input", __assign({ "data-rck-input-base--input": true }, inputProps, { type: type, required: required, "aria-invalid": hasError || undefined, ref: useAssignRef(textInput, ref), className: "rck-text-field-base__input", disabled: disabled, onFocus: onFocusHandler, "data-testid": getTestId(TextFieldBaseTest.prefix, testId, TextFieldBaseTest.input) }))); };
    var applyErrorToBorder = hasError && (errorConfig === null || errorConfig === void 0 ? void 0 : errorConfig.border);
    var applyErrorToText = hasError && (errorConfig === null || errorConfig === void 0 ? void 0 : errorConfig.text);
    return (React__namespace.createElement("div", { "data-rck-input-base--container": true, role: "presentation", className: classNames('rck-text-field-base', "rck-text-field-base--size-".concat(size), width ? "rck-text-field-base--width-".concat(width) : undefined, applyErrorToBorder ? "rck-text-field-base--error-border" : undefined, applyErrorToText ? "rck-text-field-base--error-text" : undefined, className), ref: containerRef, "data-disabled": disabled || undefined, "data-error": hasError || undefined, "data-testid": getTestId(TextFieldBaseTest.prefix, testId), onMouseDown: handleOnMouseDown },
        contentLeft && React__namespace.createElement("div", { "data-area--left": true }, contentLeft),
        React__namespace.createElement("div", { "data-area--body-wrapper": true, className: "rck-text-field-base__body-wrapper" }, (_a = computedChildrenProp()) !== null && _a !== void 0 ? _a : renderInputComponent()),
        contentRight && (React__namespace.createElement("div", { "data-area--right": true, "data-testid": getTestId(TextFieldBaseTest.prefix, testId, TextFieldBaseTest.contentRight) }, contentRight))));
});
TextFieldBase.displayName = 'TextFieldBase';
/**
 * __TextFieldBase__
 *
 * A text field is an input that allows a user to write or edit text.
 *
 * - Use this component to create others that interit from TextField styles.
 * - Do not export this component to the end user, it is meant for internal use.
 *
 * You can use the following data selector for customizations:
 * - `[data-rck-input-base--container]`
 * - `[data-rck-input-base--input]`
 * -  __Areas:__
 *      - `[data-area--left]`
 *      - `[data-area--body-wrapper]`
 *      - `[data-area--right]`
 * -  __States:__
 *      - `[data-disabled]`
 *      - `[data-error]`
 *
 * @example
 * ```scss
 * .my-component {
 *      & > [data-rck-input-base--container] {
 *           &:focus-within: { ... }
 *      }
 * }
 * ```
 */
var TextFieldBase$1 = React__namespace.memo(TextFieldBase);

var DurationInputTest;
(function (DurationInputTest) {
    DurationInputTest["prefix"] = "DurationInput";
})(DurationInputTest || (DurationInputTest = {}));
var DEFAULT_MAX_MINUTES = 24 * 60;
var getMinutes = function (totalMinutes) { return Math.floor(totalMinutes % 60); };
var getHours = function (totalMinutes) { return Math.floor(totalMinutes / 60); };
var getPadSize = function (max) { return Math.max(max.toString().length, 2); };
/** Pad a number with zeros from the left. */
var lzpad = function (num, totalPad) {
    if (totalPad === void 0) { totalPad = 2; }
    return num.toString().padStart(totalPad, '0');
};
var defaultLabelProp = {
    hours: 'h',
    minutes: 'm',
};
var defaultErrorConfig$1 = {
    border: true,
    text: false,
};
var DurationInput = function (_a) {
    var value = _a.value, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, initialMaxMinutes = _a.maxMinutes, _c = _a.labels, labels = _c === void 0 ? defaultLabelProp : _c, hasError = _a.hasError, disabled = _a.disabled, onChange = _a.onChange;
    var maxMinutes = initialMaxMinutes || DEFAULT_MAX_MINUTES;
    var getTotalMinutes = function (inputValue, inputType) {
        var totalMinutesValue = inputValue;
        if (inputType === 'hours') {
            totalMinutesValue = inputValue * 60 + getMinutes(value);
        }
        if (inputType === 'minutes') {
            totalMinutesValue = Math.min(inputValue, 59) + getHours(value) * 60;
        }
        return Math.min(maxMinutes || Infinity, totalMinutesValue);
    };
    var onChangeHandler = function (inputType) { return function (e) {
        var totalMinutesValue = getTotalMinutes(parseInt(e.currentTarget.value, 10) || 0, inputType);
        onChange(totalMinutesValue, e, inputType);
    }; };
    return (React__namespace.createElement(FlexLayout, { mainaxis: "row", mainaxisAlignment: "center", crossaxisAlignment: "center", className: classNames('rck-duration-input', className), "data-testid": getTestId(DurationInputTest.prefix, testId) },
        React__namespace.createElement(TextFieldBase$1, { type: "number", name: "duration-hours", min: 0, 
            // We want to have at least a digit with 2 numbers to mantain a consistent width
            max: Math.max(10, getHours(maxMinutes)), value: lzpad(getHours(value), getPadSize(getHours(maxMinutes))), onChange: onChangeHandler('hours'), selectAllOnFocus: true, hasError: hasError, errorConfig: defaultErrorConfig$1, disabled: disabled, width: "hug-content", contentRight: React__namespace.createElement("span", { className: "rck-duration-input__text" }, labels.hours) }),
        React__namespace.createElement(TextFieldBase$1, { type: "number", name: "duration-minutes", min: 0, max: 59, value: lzpad(getMinutes(value)), onChange: onChangeHandler('minutes'), selectAllOnFocus: true, hasError: hasError, errorConfig: defaultErrorConfig$1, disabled: disabled, width: "hug-content", contentRight: React__namespace.createElement("span", { className: "rck-duration-input__text" }, labels.minutes) })));
};
DurationInput.displayName = 'DurationInput';

var TimePickerInputTest;
(function (TimePickerInputTest) {
    TimePickerInputTest["prefix"] = "TimePickerInput";
})(TimePickerInputTest || (TimePickerInputTest = {}));
var defaultErrorConfig = {
    border: true,
    text: false,
};
var TimePickerInput = function (_a) {
    var name = _a.name, value = _a.value, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, _c = _a.hasError, hasError = _c === void 0 ? false : _c, _d = _a.disabled, disabled = _d === void 0 ? false : _d, _e = _a.readOnly, readOnly = _e === void 0 ? false : _e, _f = _a.placeholder, placeholder = _f === void 0 ? 'HH:mm' : _f, onChange = _a.onChange, onClick = _a.onClick, onBlur = _a.onBlur, onFocus = _a.onFocus, onClickTrigger = _a.onClickTrigger;
    var onChangeHandler = React__namespace.useCallback(function (e) {
        onChange === null || onChange === void 0 ? void 0 : onChange(e.currentTarget.value, e);
    }, [onChange]);
    return (React__namespace.createElement(TextFieldBase$1, { type: "text", name: name, value: value, selectAllOnFocus: !readOnly, readOnly: readOnly, hasError: hasError, errorConfig: defaultErrorConfig, disabled: disabled, placeholder: placeholder, width: "block", className: classNames('rck-time-picker-input', className), testId: getTestId(TimePickerInputTest.prefix, testId), onChange: onChangeHandler, onClick: onClick, onBlur: onBlur, onFocus: onFocus, contentRight: React__namespace.createElement(Icon$1, { onClick: onClickTrigger, type: IconType.clock, size: IconSize.lg, color: exports.IconColor.secondary, square: true }) }));
};
TimePickerInput.displayName = 'TimePickerInput';
var TimePickerInput$1 = React__namespace.memo(TimePickerInput);

exports.AutoPortalTest = void 0;
(function (AutoPortalTest) {
    AutoPortalTest["prefix"] = "AutoPortal";
})(exports.AutoPortalTest || (exports.AutoPortalTest = {}));
var getElement = function (_a) {
    var id = _a.id, className = _a.className, tag = _a.tag;
    var element = document.getElementById(id) || document.createElement(tag);
    if (className) {
        element.className = className;
    }
    element.setAttribute('data-testid', getTestId(exports.AutoPortalTest.prefix, id) || '');
    return element;
};
/**
 *
 * Portal that uses `document.body` as container
 *
 * Behavior
 *
 * 1. Portals are added as div chid of body.
 * 2. Must define id.
 *
 *
 * @example
 *
 *```tsx
 * import { AutoPortal } from 'styleguide-react';
 * import { Modal } from '../components/Modal'
 *
 * const Modal: React.FC<ModalProps> = ({ children }) => {
 *   return <AutoPortal id="global-modal" className="custom-modal">
 *           <Modal>
 *            {children}
 *           </Modal>
 *          </AutoPortal>
 *
 * }
 *
 *```
 *
 */
var AutoPortal = React__namespace.memo(function (_a) {
    var id = _a.id, children = _a.children, _b = _a.className, className = _b === void 0 ? '' : _b;
    var _c = React__namespace.useState({}), rerender = _c[1];
    var _d = React__namespace.useState(null), tmpNode = _d[0], setTmpNode = _d[1];
    var portal = React__namespace.useRef(null);
    React__namespace.useEffect(function () {
        var portalNode = getElement({ id: id, className: className, tag: 'div' });
        portal.current = portalNode;
        var isNewlyCreatedDomNode = !portalNode.isConnected;
        if (!isNewlyCreatedDomNode) {
            rerender({});
            return undefined;
        }
        if (!tmpNode)
            return undefined;
        var host = tmpNode === null || tmpNode === void 0 ? void 0 : tmpNode.ownerDocument.body;
        if (!host)
            return undefined;
        host.appendChild(portal.current);
        rerender({}); // we need to re-render in order for the host.appendChild to take effect
        // cleanup
        return function () {
            if (isNewlyCreatedDomNode && host.contains(portalNode)) {
                host.removeChild(portalNode);
            }
        };
    }, [tmpNode]);
    return portal.current ? (ReactDOM__default["default"].createPortal(children, portal.current)) : (React__namespace.createElement("span", { ref: function (el) {
            if (el) {
                setTmpNode(el);
            }
        } }));
});

exports.DrawerTest = void 0;
(function (DrawerTest) {
    DrawerTest["prefix"] = "Drawer";
})(exports.DrawerTest || (exports.DrawerTest = {}));
var getDrawerTestId = function (isOpen, from) {
    if (from === 'left' && isOpen)
        return getTestId(exports.DrawerTest.prefix, 'left-opened');
    if (from === 'left' && !isOpen)
        return getTestId(exports.DrawerTest.prefix, 'left-closed');
    if (from === 'right' && isOpen)
        return getTestId(exports.DrawerTest.prefix, 'right-opened');
    return getTestId(exports.DrawerTest.prefix, 'right-closed');
};
var getDrawerTransitionClass = function (isOpen, from) {
    if (isOpen && from === 'left')
        return 'rck-drawer--transition-left-opened';
    if (!isOpen && from === 'left')
        return 'rck-drawer--transition-left-closed';
    if (isOpen && from === 'right')
        return 'rck-drawer--transition-right-opened';
    return 'rck-drawer--transition-right-closed';
};
/**
 *
 * Defines drawer behavior without specifying content sizes, is used as base abstraction without context.
 *
 * Behavior
 *
 * 1. It Adapts size of children components.
 * 2. It adapts position of parent component.
 *
 * Important
 *
 * 1. It's recommended to use NoContextDrawer component together with useToggle hook to manage its state.
 * 2. it's recommended to use NoContextDrawer component together with Portal component to define parent position.
 *
 * @example
 *
 *```tsx
 * import { useToggle, NoContextDrawer, AutoPortal } from 'styleguide-react';
 *
 * const LeftSideMenu = () => {
 *   const [isOpen toggle] = useToggle(false);
 *
 *   return <div>
 *            <button onClick={toggle}>Toggle menu</button>
 *
 *            <AutoPortal id="left-drawer" className="custom-portal">
 *              <NoContextDrawer from="left" isOpen={isOpen}>
 *                  <Menu />
 *              <NoContextDrawer />
 *            </AutoPortal>
 *          </div>
 *
 * }
 *
 *```
 */
var NoContextDrawer = function (_a) {
    var children = _a.children, isOpen = _a.isOpen, from = _a.from, _b = _a.className, className = _b === void 0 ? '' : _b, baseClassName = _a.baseClassName;
    return (React__namespace.createElement("div", { className: classNames('rck-drawer__container', baseClassName ? "".concat(baseClassName, "__drawer") : '', className) },
        React__namespace.createElement("div", { "data-testid": getDrawerTestId(isOpen, from), className: classNames(getDrawerTransitionClass(isOpen, from), baseClassName ? "".concat(baseClassName, "__body") : '') }, children)));
};
var getDrawerPositionClass = function (from) {
    return from === 'right'
        ? 'rck-drawer__container--position-right'
        : 'rck-drawer__container--position-left';
};
/**
 *
 * Behavior
 *
 * 1. Uses all available screen height.
 * 2. It defines the positions in which it can be used (fixed left or fixed right).
 * 3. If an id is not provided works as singleton. If its need multiple instances must provide id for each instance.
 *
 * Important
 *
 * 1. It's recommended to use Drawer component together with useToggle hook to manage its state.
 *
 * @example
 *
 *```tsx
 * import { useToggle, Drawer } from 'styleguide-react';
 *
 * const LeftSideMenu = () => {
 *   const [isOpen toggle] = useToggle(false);
 *
 *   return <div>
 *            <button onClick={toggle}>Toggle menu</button>
 *
 *            <Drawer from="left" isOpen={isOpen}>
 *              <Menu />
 *            <Drawer />
 *          </div>
 *
 * }
 *
 *```
 *
 * If it's necessary to define where the drawer will be displayed, the NoContextDrawer component can be used.
 *
 */
var Drawer = function (_a) {
    var children = _a.children, isOpen = _a.isOpen, from = _a.from, id = _a.id, _b = _a.overlay, overlay = _b === void 0 ? true : _b, className = _a.className;
    return (React__namespace.createElement(AutoPortal, { id: id !== null && id !== void 0 ? id : 'rck-drawer-singleton-portal', className: classNames('rck-drawer-portal', className) },
        React__namespace.createElement(NoContextDrawer, { isOpen: isOpen, from: from, className: getDrawerPositionClass(from), baseClassName: className }, children),
        isOpen && overlay ? React__namespace.createElement("div", { className: "rck-drawer__overlay" }) : null));
};

var useToggle = function (initial) {
    var _a = React__namespace.useState(initial), isOpen = _a[0], setIsOpen = _a[1];
    return [
        isOpen,
        {
            toggle: function () { return setIsOpen(function (s) { return !s; }); },
            open: function () { return setIsOpen(true); },
            close: function () { return setIsOpen(false); },
        }
    ];
};

var size = function (steps) { return steps.length; };
var dec = function (n) { return n - 1; };
var inc = function (n) { return n + 1; };
var isLastPosition = function (stepsSize, pos) {
    return dec(stepsSize) === pos;
};
var isFirstPosition = function (pos) { return pos === 0; };
var nextPosition = function (steps, pos) {
    return isLastPosition(size(steps), pos) ? pos : inc(pos);
};
var prevPosition = function (pos) { return (isFirstPosition(pos) ? pos : dec(pos)); };
var unsafeNextPositionByName = function (name, steps) {
    return steps.findIndex(function (step) { return step.name === name; });
};
var unsafeInitialPosition = function (steps, name) { return (name ? unsafeNextPositionByName(name, steps) : 0); };
var nextTransition = function (currentPos, nextPos) {
    if (currentPos === nextPos)
        return 'none';
    if (currentPos < nextPos)
        return 'next';
    return 'prev';
};
var next = function (steps) { return function (_a) {
    var position = _a.position;
    var newPosition = nextPosition(steps, position);
    return {
        transition: nextTransition(position, newPosition),
        position: newPosition,
    };
}; };
var prev = function (_a) {
    var position = _a.position;
    var newPosition = prevPosition(position);
    return {
        transition: nextTransition(position, newPosition),
        position: newPosition,
    };
};
var go = function (name, steps) { return function (_a) {
    var position = _a.position;
    var newPosition = unsafeNextPositionByName(name, steps);
    return {
        position: newPosition,
        transition: nextTransition(position, newPosition),
    };
}; };
var safeAsNone = function (selectedStep) {
    return selectedStep !== null && selectedStep !== void 0 ? selectedStep : { step: function () { return null; }, name: '' };
};

exports.MultiStepTest = void 0;
(function (MultiStepTest) {
    MultiStepTest["prefix"] = "MultiStep";
})(exports.MultiStepTest || (exports.MultiStepTest = {}));

/**
 *
 * Renders one step at a time allowing transition to next, previous or a selected step.
 *
 * Behavior
 *
 * 1. Can define default step using activeStep with value as step name.
 * 1. When is in te last step and call next the multiStep does not change step.
 * 2. When is in the first step and call prev the multiStep does not change step.
 * 3. If go is used with the name of a step that does not exist the multiStep does not render anything (Render null).
 *
 * Important
 *
 * 1. To navigate between the different steps it is recommended to use go instead of next or prev so as not depends to order of the steps.
 * 2. MultiStep use step name prop to select active step.
 *
 *
 * @example
 *
 * ```tsx
 * <MultiStep
 *    activeStep="three"
 *    steps={[{
 *      name: "one",
 *      step: ({ next }) => <div>
 *        <h2>Step One</h2>
 *        <button onClick={next}>Go to step 2</button>
 *      </div>
 *    }, {
 *      name: "two",
 *      step: ({ next, prev, transition }) => <div>
 *        <h2>Step two</h2>
 *        <div>
 *          <button onClick={prev}>Go to step 1</button>
 *          <button onClick={next}>Go to step 3</button>
 *        </div>
 *      </div>
 *    }, {
 *     name: "three",
 *     step: ({ next, prev, go, transition }) => <div>
 *       <h2>Step three</h2>
 *       <div>
 *          <button onClick={prev}>Go to step 2</button>
 *          <button onClick={next}>Go to step 3</button>
 *          <button onClick={() => go("one")}>Go to step 1</button>
 *        </div>
 *       </div>
 *    }]}
 *  />
 * ```
 */
var MultiStep = function (_a) {
    var activeStep = _a.activeStep, steps = _a.steps, testId = _a.testId;
    var _b = React__namespace.useState({
        position: unsafeInitialPosition(steps, activeStep),
        transition: 'none',
    }), state = _b[0], setState = _b[1];
    React__namespace.useEffect(function () {
        setState(function (currentState) { return (__assign(__assign({}, currentState), { position: unsafeInitialPosition(steps, activeStep) })); });
    }, [activeStep, steps]);
    var position = state.position, transition = state.transition;
    var step = safeAsNone(steps[position]).step;
    return (React__namespace.createElement("div", { "data-test-id": getTestId(exports.MultiStepTest.prefix, testId), className: "rck-multi-step-container" }, step({
        next: function () { return setState(next(steps)); },
        prev: function () { return setState(prev); },
        go: function (stepName) { return setState(go(stepName, steps)); },
        transition: transition,
    })));
};

var FilterTest;
(function (FilterTest) {
    FilterTest["prefix"] = "Filter";
})(FilterTest || (FilterTest = {}));
var FilterTest$1 = FilterTest;

/* eslint-disable jsx-a11y/click-events-have-key-events */
var Filter = function (_a) {
    var text = _a.text, _b = _a.size, size = _b === void 0 ? 'regular' : _b, _c = _a.active, active = _c === void 0 ? false : _c, _d = _a.type, type = _d === void 0 ? 'secondary' : _d, onClick = _a.onClick, testId = _a.testId, _e = _a.className, className = _e === void 0 ? '' : _e;
    return (React__namespace.createElement("div", { title: text, className: classNames('rck-filter', "rck-filter--size-".concat(size), active ? 'rck-filter--active' : '', active ? "rck-filter--active-color-".concat(type) : '', className), "data-testid": getTestId(FilterTest$1.prefix, testId), onClick: onClick }, text));
};
Filter.displayName = 'Filter';

var FilterGroupTest;
(function (FilterGroupTest) {
    FilterGroupTest["prefix"] = "FilterGroup";
})(FilterGroupTest || (FilterGroupTest = {}));
var FilterGroupTest$1 = FilterGroupTest;

var evaluateExceptions = function (filter, active, onClick, size) {
    var _a, _b, _c, _d;
    if (filter.type !== Filter) {
        throw new Error('FilterGroup children must be a Filter component');
    }
    if (active) {
        if (!((_a = filter.props) === null || _a === void 0 ? void 0 : _a.id)) {
            throw new Error('Must use `id` prop in Filter component when using `active` prop in FilterGroup');
        }
        if ((_b = filter.props) === null || _b === void 0 ? void 0 : _b.active) {
            throw new Error('Must cannot use `active` prop in Filter component when using `active` prop in FilterGroup');
        }
    }
    if (onClick && ((_c = filter.props) === null || _c === void 0 ? void 0 : _c.onClick)) {
        throw new Error('You cannot use `onClick` prop in Filter component when FilterGroup has `onClick` prop');
    }
    if (size && ((_d = filter.props) === null || _d === void 0 ? void 0 : _d.size)) {
        throw new Error('You cannot use `size` prop in Filter component when FilterGroup has `size` prop');
    }
};
/**
 *
 * Component to wrap filters into a group a gives them spacing and the option to pass `active` prop and `onClick` prop for all Filter children.
 *
 * Behavior
 *
 * - It only allows `Filter` component as child `(1..n)`.
 * - If `active` prop is provided, you have to use `id` prop in `Filter` component.
 * - If `onClick` prop is provided, you cannot use `onClick` prop in `Filter` component
 *
 * @example
 *```tsx
 * import { FilterGroup, Filter } from 'styleguide-react';
 *
 * const Component = () => {
 *   const [active, setActive] = React.useState('1');
 *   const onClick = (id, _e) => {
 *     setActive(id);
 *   };
 *
 *   return <FilterGroup active={active} onClick={onClick} size="regular">
 *      <Filter id="1" text="Filter 1" />
 *      <Filter id="2" text="Primary" type="primary" />
 *      <Filter id="3" text="Error" type="error" />
 *      <Filter id="4" text="Secondary" type="secondary" />
 *      <Filter id="5" text="Filter 5" />
 *    </FilterGroup>
 * }
 *```
 */
var FilterGroup = function (_a) {
    var active = _a.active, size = _a.size, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b, children = _a.children, onClick = _a.onClick;
    if (!React__namespace.Children.count(children)) {
        throw new Error('FilterGroup must have children');
    }
    return (React__namespace.createElement("div", { className: classNames('rck-filter-group', className), "data-testid": getTestId(FilterGroupTest$1.prefix, testId) }, React__namespace.Children.map(children, function (child) {
        var _a, _b;
        var filter = child;
        if (!filter)
            return null;
        evaluateExceptions(filter, active, onClick, size);
        return React__namespace.cloneElement(filter, __assign(__assign(__assign({}, (size && { size: size })), (((_a = filter.props) === null || _a === void 0 ? void 0 : _a.id) && { active: ((_b = filter.props) === null || _b === void 0 ? void 0 : _b.id) === active })), (onClick && {
            onClick: function (e) { var _a; return onClick === null || onClick === void 0 ? void 0 : onClick((_a = filter.props) === null || _a === void 0 ? void 0 : _a.id, e); },
        })));
    })));
};
FilterGroup.displayName = 'FilterGroup';

var ColorPickerTest;
(function (ColorPickerTest) {
    ColorPickerTest["prefix"] = "ColorPicker";
})(ColorPickerTest || (ColorPickerTest = {}));
var ColorPickerTest$1 = ColorPickerTest;

var ColorPicker = function (_a) {
    var palette = _a.palette, activeColor = _a.activeColor, onClick = _a.onClick, testId = _a.testId, _b = _a.className, className = _b === void 0 ? '' : _b;
    var colors = palette;
    if (Array.isArray(palette)) {
        colors = palette.reduce(function (a, b) {
            var _a;
            return (__assign(__assign({}, a), (_a = {}, _a[b] = b, _a)));
        }, {});
    }
    return (React__namespace.createElement("div", { role: "grid", className: classNames('rck-color-picker', className), "data-testid": getTestId(ColorPickerTest$1.prefix, testId) }, Object.entries(colors).map(function (_a) {
        var colorName = _a[0], colorValue = _a[1];
        return (React__namespace.createElement("span", { key: colorName, role: "gridcell", "aria-label": colorName, "aria-selected": colorName === activeColor, tabIndex: 0, style: __assign({ background: colorValue }, { '--react-rck-colorpicker-active-color': colorValue }), className: classNames('rck-color-picker__item', colorName === activeColor ? "rck-color-picker__item--active" : ''), onClick: function (e) { return onClick(colorName, e); }, onKeyDown: handleKeyboardSelection(function (e) { return onClick(colorName, e); }) }));
    })));
};
ColorPicker.displayName = 'ColorPicker';

var Table = function (_a) {
    var border = _a.border, children = _a.children, className = _a.className, testId = _a.testId;
    return (React__namespace.createElement("table", { className: classNames('rck-table', border !== 'default' ? "rck-table--border-".concat(border) : '', className !== null && className !== void 0 ? className : ''), "data-testid": getTestId('rck-table', testId) }, children));
};

var TableBody = function (_a) {
    var children = _a.children;
    return (React__namespace.createElement("tbody", null, children));
};

var TableCell = function (_a) {
    var head = _a.head, alignement = _a.alignement, children = _a.children;
    return head ? (React__namespace.createElement("th", { className: classNames('rck-table-cell', 'rck-table-cell--head', alignement ? "rck-table-cell--".concat(alignement) : '') }, children)) : (React__namespace.createElement("td", { className: classNames('rck-table-cell', alignement ? "rck-table-cell--".concat(alignement) : '') }, children));
};

var TableHead = function (_a) {
    var children = _a.children;
    return (React__namespace.createElement("thead", { className: "rck-table-head" }, children));
};

var TableRow = function (_a) {
    var children = _a.children;
    return (React__namespace.createElement("tr", { className: "rck-table-row" }, children));
};

var renderCell = function (cellContent, index, tableKey) {
    if (typeof cellContent === 'string') {
        // if it is the first column align the element to the left, otherwise center it
        var alignement = index === 0 ? 'left' : 'center';
        return (React__namespace.createElement(TableCell, { alignement: alignement, key: "table-head-cell-".concat(tableKey, "-").concat(index) }, cellContent));
    }
    if (typeof cellContent === 'boolean') {
        return (React__namespace.createElement(TableCell, { alignement: "center", key: "table-head-cell-".concat(tableKey, "-").concat(index) }, cellContent ? (React__namespace.createElement(Icon$1, { type: IconType.check, color: exports.IconColor.greenDecorationContrast, size: IconSize.lg, family: exports.IconFamily.solid })) : (React__namespace.createElement(Icon$1, { type: IconType.xmark, color: exports.IconColor.error, size: IconSize.lg, family: exports.IconFamily.solid }))));
    }
    if (cellContent === null) {
        return (React__namespace.createElement(TableCell, { alignement: "center", key: "table-head-cell-".concat(tableKey, "-").concat(index) }, "-"));
    }
    return '';
};
var ComparisonTable = function (_a) {
    var data = _a.data, key = _a.key, _b = _a.border, border = _b === void 0 ? 'default' : _b;
    return (React__namespace.createElement(Table, { border: border, className: "rck-comparison-table" },
        React__namespace.createElement(TableHead, null,
            React__namespace.createElement(TableRow, null, data.head.map(function (cell, index) {
                var cellHeadKey = "table-head-cell-".concat(key, "-").concat(index);
                if (index === 0) {
                    return (React__namespace.createElement(TableCell, { head: true, alignement: "left", key: cellHeadKey },
                        cell.icon && (React__namespace.createElement(Icon$1, { type: cell.icon, className: "rck-comparison-table__head-icon" })),
                        cell.text));
                }
                return (React__namespace.createElement(TableCell, { alignement: "center", key: cellHeadKey }, cell.text));
            }))),
        React__namespace.createElement(TableBody, null, data.rows.map(function (row, index) {
            var rowBodyKey = "table-row-".concat(key, "-").concat(index);
            return (React__namespace.createElement(TableRow, { key: rowBodyKey }, row.map(function (cell, indexCell) { return renderCell(cell, indexCell, key); })));
        }))));
};
ComparisonTable.displayName = 'ComparisonTable';

var TagTest;
(function (TagTest) {
    TagTest["prefix"] = "Tag";
})(TagTest || (TagTest = {}));
var TagTest$1 = TagTest;

var Tag = function (_a) {
    var text = _a.text, _b = _a.variant, variant = _b === void 0 ? 'squared' : _b, _c = _a.size, size = _c === void 0 ? 'regular' : _c, _d = _a.type, type = _d === void 0 ? exports.GlobalType.primary : _d, _e = _a.invertColor, invertColor = _e === void 0 ? false : _e, iconType = _a.iconType, iconProps = _a.iconProps, onClick = _a.onClick, testId = _a.testId, _f = _a.className, className = _f === void 0 ? '' : _f;
    return (React__namespace.createElement("div", { title: text, className: classNames('rck-tag', "rck-tag--variant-".concat(variant), "rck-tag--size-".concat(size), "rck-tag--type-".concat(type), invertColor ? "rck-tag--invert-color" : '', iconType && !text ? 'rck-tag--only-icon' : '', onClick ? 'rck-tag--clickable' : '', className), "data-testid": getTestId(TagTest$1.prefix, testId), onClick: onClick },
        iconType ? (React__namespace.createElement(Icon$1, __assign({}, iconProps, { type: iconType, className: classNames('rck-tag__icon', (iconProps === null || iconProps === void 0 ? void 0 : iconProps.className) || '') }))) : null,
        iconType && text && React__namespace.createElement("span", { className: "rck-tag__spacer" }),
        text));
};
Tag.displayName = 'Tag';

exports.TextAreaTest = void 0;
(function (TextAreaTest) {
    TextAreaTest["prefix"] = "TextArea";
})(exports.TextAreaTest || (exports.TextAreaTest = {}));
var isMaxRowsExceeded = function (value, maxRows) {
    var numRows = (value.match(/\n/g) || []).length + 1;
    return numRows > maxRows;
};
var useResize = function (defaultValue, maxRows, rows) {
    var _a = React__namespace.useState(defaultValue), value = _a[0], setValue = _a[1];
    var _b = React__namespace.useState(0), initialHeight = _b[0], setInitialHeight = _b[1];
    var textareaRef = React__namespace.useRef(null);
    React__namespace.useEffect(function () {
        var element = textareaRef.current;
        if (!element)
            return;
        setInitialHeight(element.clientHeight);
    }, []);
    React__namespace.useEffect(function () {
        var element = textareaRef.current;
        if (!element)
            return;
        var updatedHeight = element.scrollHeight;
        if (!value && initialHeight === 0 && rows) {
            // to work it depends on the font-size defined in scss
            var fontSize = 16;
            element.style.height = "".concat((rows + 1) * fontSize, "px");
        }
        if (!value && initialHeight > 0) {
            element.style.height = "".concat(initialHeight, "px");
        }
        else if (updatedHeight > initialHeight &&
            !isMaxRowsExceeded(value, maxRows)) {
            element.style.height = "".concat(element.scrollHeight, "px");
        }
    }, [value]);
    return { value: value, setValue: setValue, textareaRef: textareaRef };
};
var TextArea = function (_a) {
    var maxLength = _a.maxLength, rows = _a.rows, _b = _a.maxRows, maxRows = _b === void 0 ? Infinity : _b, _c = _a.error, error = _c === void 0 ? false : _c, _d = _a.disabled, disabled = _d === void 0 ? false : _d, helperText = _a.helperText, placeholder = _a.placeholder, onChange = _a.onChange, _e = _a.defaultValue, defaultValue = _e === void 0 ? '' : _e, testId = _a.testId;
    var _f = useResize(defaultValue, maxRows, rows), value = _f.value, setValue = _f.setValue, textareaRef = _f.textareaRef;
    return (React__namespace.createElement("div", { "data-testid": getTestId(exports.TextAreaTest.prefix, testId), className: classNames('rck-text-area', disabled ? 'rck-text-area--disabled' : '', error ? 'rck-text-area--error' : '', isMaxRowsExceeded(value, maxRows) ? 'rck-text-area--scrollable' : '') },
        React__namespace.createElement("textarea", { ref: textareaRef, value: value, rows: rows, placeholder: placeholder, "data-testid": getTestId(exports.TextAreaTest.prefix, testId, 'textarea'), maxLength: maxLength, onChange: function (e) {
                if (onChange)
                    onChange(e);
                setValue(e.target.value);
            }, name: "textarea", disabled: disabled }),
        helperText ? (React__namespace.createElement("div", null,
            React__namespace.createElement("label", { className: classNames('rck-text-area__text-helper', error ? 'rck-text-area__text-helper--error' : ''), htmlFor: "textarea" }, helperText))) : null));
};
TextArea.displayName = 'TextArea';

/**
 * Returns an object with events handlers to manage enter an leave an Element in desktop and mobile.
 *
 * @param handler - The handler that will manage the fired events.
 * @returns - An object containing the onMouseEnter and onMouseLeave if the platform is Desktop.
 *              If the platform is Mobile, the object will have onTouchStart and onTouchEnd.
 */
var getTouchPointHandlers = function (handler, isMobile) {
    if (isMobile === void 0) { isMobile = true; }
    function onWhenIn(a) {
        var b = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            b[_i - 1] = arguments[_i];
        }
        handler('in').apply(void 0, __spreadArray([a], b, false));
    }
    function onWhenOut(a) {
        var b = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            b[_i - 1] = arguments[_i];
        }
        handler('out').apply(void 0, __spreadArray([a], b, false));
    }
    var handlers = function () {
        var events = {
            onMouseEnter: onWhenIn,
            onMouseLeave: onWhenOut,
        };
        if (isMobile) {
            events = {
                onTouchStart: onWhenIn,
                onTouchEnd: onWhenOut,
            };
        }
        var focusEvents = {
            onFocus: onWhenIn,
            onBlur: onWhenOut,
        };
        return {
            touchEvents: events,
            focusEvents: focusEvents,
            isMobile: isMobile,
        };
    };
    return handlers();
};

var getTooltipContent = function (label, title) {
    var content = [];
    if (title) {
        content.push(React__namespace.createElement("div", { key: "1", className: "rck-tooltip__title" }, title));
    }
    if (typeof label === 'string') {
        content.push(React__namespace.createElement("div", { key: "2", className: "rck-tooltip__label" }, label));
    }
    else {
        content.push(React__namespace.createElement(React__namespace.Fragment, { key: "3" }, label));
    }
    return React__namespace.createElement.apply(React__namespace, __spreadArray([React__namespace.Fragment, null], content, false));
};
var getTooltipTransitionClass = function (transition) {
    if (!transition)
        return '';
    if (typeof transition === 'boolean') {
        return 'rck-tooltip--smooth-transition-500';
    }
    if (typeof transition === 'number') {
        return "rck-tooltip--smooth-transition-".concat(transition);
    }
    return '';
};
var generateGetBoundingClientRect = function (x, y) {
    if (x === void 0) { x = 0; }
    if (y === void 0) { y = 0; }
    return function () {
        return ({
            width: 0,
            height: 0,
            top: y,
            right: x,
            bottom: y,
            left: x,
        });
    };
};

var top = 'top';
var bottom = 'bottom';
var right = 'right';
var left = 'left';
var auto = 'auto';
var basePlacements = [top, bottom, right, left];
var start = 'start';
var end = 'end';
var clippingParents = 'clippingParents';
var viewport = 'viewport';
var popper = 'popper';
var reference = 'reference';
var variationPlacements = /*#__PURE__*/basePlacements.reduce(function (acc, placement) {
  return acc.concat([placement + "-" + start, placement + "-" + end]);
}, []);
var placements = /*#__PURE__*/[].concat(basePlacements, [auto]).reduce(function (acc, placement) {
  return acc.concat([placement, placement + "-" + start, placement + "-" + end]);
}, []); // modifiers that need to read the DOM

var beforeRead = 'beforeRead';
var read = 'read';
var afterRead = 'afterRead'; // pure-logic modifiers

var beforeMain = 'beforeMain';
var main = 'main';
var afterMain = 'afterMain'; // modifier with the purpose to write to the DOM (or write into a framework state)

var beforeWrite = 'beforeWrite';
var write = 'write';
var afterWrite = 'afterWrite';
var modifierPhases = [beforeRead, read, afterRead, beforeMain, main, afterMain, beforeWrite, write, afterWrite];

function getNodeName(element) {
  return element ? (element.nodeName || '').toLowerCase() : null;
}

function getWindow(node) {
  if (node == null) {
    return window;
  }

  if (node.toString() !== '[object Window]') {
    var ownerDocument = node.ownerDocument;
    return ownerDocument ? ownerDocument.defaultView || window : window;
  }

  return node;
}

function isElement(node) {
  var OwnElement = getWindow(node).Element;
  return node instanceof OwnElement || node instanceof Element;
}

function isHTMLElement(node) {
  var OwnElement = getWindow(node).HTMLElement;
  return node instanceof OwnElement || node instanceof HTMLElement;
}

function isShadowRoot(node) {
  // IE 11 has no ShadowRoot
  if (typeof ShadowRoot === 'undefined') {
    return false;
  }

  var OwnElement = getWindow(node).ShadowRoot;
  return node instanceof OwnElement || node instanceof ShadowRoot;
}

// and applies them to the HTMLElements such as popper and arrow

function applyStyles(_ref) {
  var state = _ref.state;
  Object.keys(state.elements).forEach(function (name) {
    var style = state.styles[name] || {};
    var attributes = state.attributes[name] || {};
    var element = state.elements[name]; // arrow is optional + virtual elements

    if (!isHTMLElement(element) || !getNodeName(element)) {
      return;
    } // Flow doesn't support to extend this property, but it's the most
    // effective way to apply styles to an HTMLElement
    // $FlowFixMe[cannot-write]


    Object.assign(element.style, style);
    Object.keys(attributes).forEach(function (name) {
      var value = attributes[name];

      if (value === false) {
        element.removeAttribute(name);
      } else {
        element.setAttribute(name, value === true ? '' : value);
      }
    });
  });
}

function effect$2(_ref2) {
  var state = _ref2.state;
  var initialStyles = {
    popper: {
      position: state.options.strategy,
      left: '0',
      top: '0',
      margin: '0'
    },
    arrow: {
      position: 'absolute'
    },
    reference: {}
  };
  Object.assign(state.elements.popper.style, initialStyles.popper);
  state.styles = initialStyles;

  if (state.elements.arrow) {
    Object.assign(state.elements.arrow.style, initialStyles.arrow);
  }

  return function () {
    Object.keys(state.elements).forEach(function (name) {
      var element = state.elements[name];
      var attributes = state.attributes[name] || {};
      var styleProperties = Object.keys(state.styles.hasOwnProperty(name) ? state.styles[name] : initialStyles[name]); // Set all values to an empty string to unset them

      var style = styleProperties.reduce(function (style, property) {
        style[property] = '';
        return style;
      }, {}); // arrow is optional + virtual elements

      if (!isHTMLElement(element) || !getNodeName(element)) {
        return;
      }

      Object.assign(element.style, style);
      Object.keys(attributes).forEach(function (attribute) {
        element.removeAttribute(attribute);
      });
    });
  };
} // eslint-disable-next-line import/no-unused-modules


var applyStyles$1 = {
  name: 'applyStyles',
  enabled: true,
  phase: 'write',
  fn: applyStyles,
  effect: effect$2,
  requires: ['computeStyles']
};

function getBasePlacement(placement) {
  return placement.split('-')[0];
}

var max = Math.max;
var min = Math.min;
var round = Math.round;

function getUAString() {
  var uaData = navigator.userAgentData;

  if (uaData != null && uaData.brands && Array.isArray(uaData.brands)) {
    return uaData.brands.map(function (item) {
      return item.brand + "/" + item.version;
    }).join(' ');
  }

  return navigator.userAgent;
}

function isLayoutViewport() {
  return !/^((?!chrome|android).)*safari/i.test(getUAString());
}

function getBoundingClientRect(element, includeScale, isFixedStrategy) {
  if (includeScale === void 0) {
    includeScale = false;
  }

  if (isFixedStrategy === void 0) {
    isFixedStrategy = false;
  }

  var clientRect = element.getBoundingClientRect();
  var scaleX = 1;
  var scaleY = 1;

  if (includeScale && isHTMLElement(element)) {
    scaleX = element.offsetWidth > 0 ? round(clientRect.width) / element.offsetWidth || 1 : 1;
    scaleY = element.offsetHeight > 0 ? round(clientRect.height) / element.offsetHeight || 1 : 1;
  }

  var _ref = isElement(element) ? getWindow(element) : window,
      visualViewport = _ref.visualViewport;

  var addVisualOffsets = !isLayoutViewport() && isFixedStrategy;
  var x = (clientRect.left + (addVisualOffsets && visualViewport ? visualViewport.offsetLeft : 0)) / scaleX;
  var y = (clientRect.top + (addVisualOffsets && visualViewport ? visualViewport.offsetTop : 0)) / scaleY;
  var width = clientRect.width / scaleX;
  var height = clientRect.height / scaleY;
  return {
    width: width,
    height: height,
    top: y,
    right: x + width,
    bottom: y + height,
    left: x,
    x: x,
    y: y
  };
}

// means it doesn't take into account transforms.

function getLayoutRect(element) {
  var clientRect = getBoundingClientRect(element); // Use the clientRect sizes if it's not been transformed.
  // Fixes https://github.com/popperjs/popper-core/issues/1223

  var width = element.offsetWidth;
  var height = element.offsetHeight;

  if (Math.abs(clientRect.width - width) <= 1) {
    width = clientRect.width;
  }

  if (Math.abs(clientRect.height - height) <= 1) {
    height = clientRect.height;
  }

  return {
    x: element.offsetLeft,
    y: element.offsetTop,
    width: width,
    height: height
  };
}

function contains(parent, child) {
  var rootNode = child.getRootNode && child.getRootNode(); // First, attempt with faster native method

  if (parent.contains(child)) {
    return true;
  } // then fallback to custom implementation with Shadow DOM support
  else if (rootNode && isShadowRoot(rootNode)) {
      var next = child;

      do {
        if (next && parent.isSameNode(next)) {
          return true;
        } // $FlowFixMe[prop-missing]: need a better way to handle this...


        next = next.parentNode || next.host;
      } while (next);
    } // Give up, the result is false


  return false;
}

function getComputedStyle(element) {
  return getWindow(element).getComputedStyle(element);
}

function isTableElement(element) {
  return ['table', 'td', 'th'].indexOf(getNodeName(element)) >= 0;
}

function getDocumentElement(element) {
  // $FlowFixMe[incompatible-return]: assume body is always available
  return ((isElement(element) ? element.ownerDocument : // $FlowFixMe[prop-missing]
  element.document) || window.document).documentElement;
}

function getParentNode(element) {
  if (getNodeName(element) === 'html') {
    return element;
  }

  return (// this is a quicker (but less type safe) way to save quite some bytes from the bundle
    // $FlowFixMe[incompatible-return]
    // $FlowFixMe[prop-missing]
    element.assignedSlot || // step into the shadow DOM of the parent of a slotted node
    element.parentNode || ( // DOM Element detected
    isShadowRoot(element) ? element.host : null) || // ShadowRoot detected
    // $FlowFixMe[incompatible-call]: HTMLElement is a Node
    getDocumentElement(element) // fallback

  );
}

function getTrueOffsetParent(element) {
  if (!isHTMLElement(element) || // https://github.com/popperjs/popper-core/issues/837
  getComputedStyle(element).position === 'fixed') {
    return null;
  }

  return element.offsetParent;
} // `.offsetParent` reports `null` for fixed elements, while absolute elements
// return the containing block


function getContainingBlock(element) {
  var isFirefox = /firefox/i.test(getUAString());
  var isIE = /Trident/i.test(getUAString());

  if (isIE && isHTMLElement(element)) {
    // In IE 9, 10 and 11 fixed elements containing block is always established by the viewport
    var elementCss = getComputedStyle(element);

    if (elementCss.position === 'fixed') {
      return null;
    }
  }

  var currentNode = getParentNode(element);

  if (isShadowRoot(currentNode)) {
    currentNode = currentNode.host;
  }

  while (isHTMLElement(currentNode) && ['html', 'body'].indexOf(getNodeName(currentNode)) < 0) {
    var css = getComputedStyle(currentNode); // This is non-exhaustive but covers the most common CSS properties that
    // create a containing block.
    // https://developer.mozilla.org/en-US/docs/Web/CSS/Containing_block#identifying_the_containing_block

    if (css.transform !== 'none' || css.perspective !== 'none' || css.contain === 'paint' || ['transform', 'perspective'].indexOf(css.willChange) !== -1 || isFirefox && css.willChange === 'filter' || isFirefox && css.filter && css.filter !== 'none') {
      return currentNode;
    } else {
      currentNode = currentNode.parentNode;
    }
  }

  return null;
} // Gets the closest ancestor positioned element. Handles some edge cases,
// such as table ancestors and cross browser bugs.


function getOffsetParent(element) {
  var window = getWindow(element);
  var offsetParent = getTrueOffsetParent(element);

  while (offsetParent && isTableElement(offsetParent) && getComputedStyle(offsetParent).position === 'static') {
    offsetParent = getTrueOffsetParent(offsetParent);
  }

  if (offsetParent && (getNodeName(offsetParent) === 'html' || getNodeName(offsetParent) === 'body' && getComputedStyle(offsetParent).position === 'static')) {
    return window;
  }

  return offsetParent || getContainingBlock(element) || window;
}

function getMainAxisFromPlacement(placement) {
  return ['top', 'bottom'].indexOf(placement) >= 0 ? 'x' : 'y';
}

function within(min$1, value, max$1) {
  return max(min$1, min(value, max$1));
}
function withinMaxClamp(min, value, max) {
  var v = within(min, value, max);
  return v > max ? max : v;
}

function getFreshSideObject() {
  return {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  };
}

function mergePaddingObject(paddingObject) {
  return Object.assign({}, getFreshSideObject(), paddingObject);
}

function expandToHashMap(value, keys) {
  return keys.reduce(function (hashMap, key) {
    hashMap[key] = value;
    return hashMap;
  }, {});
}

var toPaddingObject = function toPaddingObject(padding, state) {
  padding = typeof padding === 'function' ? padding(Object.assign({}, state.rects, {
    placement: state.placement
  })) : padding;
  return mergePaddingObject(typeof padding !== 'number' ? padding : expandToHashMap(padding, basePlacements));
};

function arrow(_ref) {
  var _state$modifiersData$;

  var state = _ref.state,
      name = _ref.name,
      options = _ref.options;
  var arrowElement = state.elements.arrow;
  var popperOffsets = state.modifiersData.popperOffsets;
  var basePlacement = getBasePlacement(state.placement);
  var axis = getMainAxisFromPlacement(basePlacement);
  var isVertical = [left, right].indexOf(basePlacement) >= 0;
  var len = isVertical ? 'height' : 'width';

  if (!arrowElement || !popperOffsets) {
    return;
  }

  var paddingObject = toPaddingObject(options.padding, state);
  var arrowRect = getLayoutRect(arrowElement);
  var minProp = axis === 'y' ? top : left;
  var maxProp = axis === 'y' ? bottom : right;
  var endDiff = state.rects.reference[len] + state.rects.reference[axis] - popperOffsets[axis] - state.rects.popper[len];
  var startDiff = popperOffsets[axis] - state.rects.reference[axis];
  var arrowOffsetParent = getOffsetParent(arrowElement);
  var clientSize = arrowOffsetParent ? axis === 'y' ? arrowOffsetParent.clientHeight || 0 : arrowOffsetParent.clientWidth || 0 : 0;
  var centerToReference = endDiff / 2 - startDiff / 2; // Make sure the arrow doesn't overflow the popper if the center point is
  // outside of the popper bounds

  var min = paddingObject[minProp];
  var max = clientSize - arrowRect[len] - paddingObject[maxProp];
  var center = clientSize / 2 - arrowRect[len] / 2 + centerToReference;
  var offset = within(min, center, max); // Prevents breaking syntax highlighting...

  var axisProp = axis;
  state.modifiersData[name] = (_state$modifiersData$ = {}, _state$modifiersData$[axisProp] = offset, _state$modifiersData$.centerOffset = offset - center, _state$modifiersData$);
}

function effect$1(_ref2) {
  var state = _ref2.state,
      options = _ref2.options;
  var _options$element = options.element,
      arrowElement = _options$element === void 0 ? '[data-popper-arrow]' : _options$element;

  if (arrowElement == null) {
    return;
  } // CSS selector


  if (typeof arrowElement === 'string') {
    arrowElement = state.elements.popper.querySelector(arrowElement);

    if (!arrowElement) {
      return;
    }
  }

  if (!contains(state.elements.popper, arrowElement)) {
    return;
  }

  state.elements.arrow = arrowElement;
} // eslint-disable-next-line import/no-unused-modules


var arrow$1 = {
  name: 'arrow',
  enabled: true,
  phase: 'main',
  fn: arrow,
  effect: effect$1,
  requires: ['popperOffsets'],
  requiresIfExists: ['preventOverflow']
};

function getVariation(placement) {
  return placement.split('-')[1];
}

var unsetSides = {
  top: 'auto',
  right: 'auto',
  bottom: 'auto',
  left: 'auto'
}; // Round the offsets to the nearest suitable subpixel based on the DPR.
// Zooming can change the DPR, but it seems to report a value that will
// cleanly divide the values into the appropriate subpixels.

function roundOffsetsByDPR(_ref, win) {
  var x = _ref.x,
      y = _ref.y;
  var dpr = win.devicePixelRatio || 1;
  return {
    x: round(x * dpr) / dpr || 0,
    y: round(y * dpr) / dpr || 0
  };
}

function mapToStyles(_ref2) {
  var _Object$assign2;

  var popper = _ref2.popper,
      popperRect = _ref2.popperRect,
      placement = _ref2.placement,
      variation = _ref2.variation,
      offsets = _ref2.offsets,
      position = _ref2.position,
      gpuAcceleration = _ref2.gpuAcceleration,
      adaptive = _ref2.adaptive,
      roundOffsets = _ref2.roundOffsets,
      isFixed = _ref2.isFixed;
  var _offsets$x = offsets.x,
      x = _offsets$x === void 0 ? 0 : _offsets$x,
      _offsets$y = offsets.y,
      y = _offsets$y === void 0 ? 0 : _offsets$y;

  var _ref3 = typeof roundOffsets === 'function' ? roundOffsets({
    x: x,
    y: y
  }) : {
    x: x,
    y: y
  };

  x = _ref3.x;
  y = _ref3.y;
  var hasX = offsets.hasOwnProperty('x');
  var hasY = offsets.hasOwnProperty('y');
  var sideX = left;
  var sideY = top;
  var win = window;

  if (adaptive) {
    var offsetParent = getOffsetParent(popper);
    var heightProp = 'clientHeight';
    var widthProp = 'clientWidth';

    if (offsetParent === getWindow(popper)) {
      offsetParent = getDocumentElement(popper);

      if (getComputedStyle(offsetParent).position !== 'static' && position === 'absolute') {
        heightProp = 'scrollHeight';
        widthProp = 'scrollWidth';
      }
    } // $FlowFixMe[incompatible-cast]: force type refinement, we compare offsetParent with window above, but Flow doesn't detect it


    offsetParent = offsetParent;

    if (placement === top || (placement === left || placement === right) && variation === end) {
      sideY = bottom;
      var offsetY = isFixed && offsetParent === win && win.visualViewport ? win.visualViewport.height : // $FlowFixMe[prop-missing]
      offsetParent[heightProp];
      y -= offsetY - popperRect.height;
      y *= gpuAcceleration ? 1 : -1;
    }

    if (placement === left || (placement === top || placement === bottom) && variation === end) {
      sideX = right;
      var offsetX = isFixed && offsetParent === win && win.visualViewport ? win.visualViewport.width : // $FlowFixMe[prop-missing]
      offsetParent[widthProp];
      x -= offsetX - popperRect.width;
      x *= gpuAcceleration ? 1 : -1;
    }
  }

  var commonStyles = Object.assign({
    position: position
  }, adaptive && unsetSides);

  var _ref4 = roundOffsets === true ? roundOffsetsByDPR({
    x: x,
    y: y
  }, getWindow(popper)) : {
    x: x,
    y: y
  };

  x = _ref4.x;
  y = _ref4.y;

  if (gpuAcceleration) {
    var _Object$assign;

    return Object.assign({}, commonStyles, (_Object$assign = {}, _Object$assign[sideY] = hasY ? '0' : '', _Object$assign[sideX] = hasX ? '0' : '', _Object$assign.transform = (win.devicePixelRatio || 1) <= 1 ? "translate(" + x + "px, " + y + "px)" : "translate3d(" + x + "px, " + y + "px, 0)", _Object$assign));
  }

  return Object.assign({}, commonStyles, (_Object$assign2 = {}, _Object$assign2[sideY] = hasY ? y + "px" : '', _Object$assign2[sideX] = hasX ? x + "px" : '', _Object$assign2.transform = '', _Object$assign2));
}

function computeStyles(_ref5) {
  var state = _ref5.state,
      options = _ref5.options;
  var _options$gpuAccelerat = options.gpuAcceleration,
      gpuAcceleration = _options$gpuAccelerat === void 0 ? true : _options$gpuAccelerat,
      _options$adaptive = options.adaptive,
      adaptive = _options$adaptive === void 0 ? true : _options$adaptive,
      _options$roundOffsets = options.roundOffsets,
      roundOffsets = _options$roundOffsets === void 0 ? true : _options$roundOffsets;
  var commonStyles = {
    placement: getBasePlacement(state.placement),
    variation: getVariation(state.placement),
    popper: state.elements.popper,
    popperRect: state.rects.popper,
    gpuAcceleration: gpuAcceleration,
    isFixed: state.options.strategy === 'fixed'
  };

  if (state.modifiersData.popperOffsets != null) {
    state.styles.popper = Object.assign({}, state.styles.popper, mapToStyles(Object.assign({}, commonStyles, {
      offsets: state.modifiersData.popperOffsets,
      position: state.options.strategy,
      adaptive: adaptive,
      roundOffsets: roundOffsets
    })));
  }

  if (state.modifiersData.arrow != null) {
    state.styles.arrow = Object.assign({}, state.styles.arrow, mapToStyles(Object.assign({}, commonStyles, {
      offsets: state.modifiersData.arrow,
      position: 'absolute',
      adaptive: false,
      roundOffsets: roundOffsets
    })));
  }

  state.attributes.popper = Object.assign({}, state.attributes.popper, {
    'data-popper-placement': state.placement
  });
} // eslint-disable-next-line import/no-unused-modules


var computeStyles$1 = {
  name: 'computeStyles',
  enabled: true,
  phase: 'beforeWrite',
  fn: computeStyles,
  data: {}
};

var passive = {
  passive: true
};

function effect(_ref) {
  var state = _ref.state,
      instance = _ref.instance,
      options = _ref.options;
  var _options$scroll = options.scroll,
      scroll = _options$scroll === void 0 ? true : _options$scroll,
      _options$resize = options.resize,
      resize = _options$resize === void 0 ? true : _options$resize;
  var window = getWindow(state.elements.popper);
  var scrollParents = [].concat(state.scrollParents.reference, state.scrollParents.popper);

  if (scroll) {
    scrollParents.forEach(function (scrollParent) {
      scrollParent.addEventListener('scroll', instance.update, passive);
    });
  }

  if (resize) {
    window.addEventListener('resize', instance.update, passive);
  }

  return function () {
    if (scroll) {
      scrollParents.forEach(function (scrollParent) {
        scrollParent.removeEventListener('scroll', instance.update, passive);
      });
    }

    if (resize) {
      window.removeEventListener('resize', instance.update, passive);
    }
  };
} // eslint-disable-next-line import/no-unused-modules


var eventListeners = {
  name: 'eventListeners',
  enabled: true,
  phase: 'write',
  fn: function fn() {},
  effect: effect,
  data: {}
};

var hash$1 = {
  left: 'right',
  right: 'left',
  bottom: 'top',
  top: 'bottom'
};
function getOppositePlacement(placement) {
  return placement.replace(/left|right|bottom|top/g, function (matched) {
    return hash$1[matched];
  });
}

var hash = {
  start: 'end',
  end: 'start'
};
function getOppositeVariationPlacement(placement) {
  return placement.replace(/start|end/g, function (matched) {
    return hash[matched];
  });
}

function getWindowScroll(node) {
  var win = getWindow(node);
  var scrollLeft = win.pageXOffset;
  var scrollTop = win.pageYOffset;
  return {
    scrollLeft: scrollLeft,
    scrollTop: scrollTop
  };
}

function getWindowScrollBarX(element) {
  // If <html> has a CSS width greater than the viewport, then this will be
  // incorrect for RTL.
  // Popper 1 is broken in this case and never had a bug report so let's assume
  // it's not an issue. I don't think anyone ever specifies width on <html>
  // anyway.
  // Browsers where the left scrollbar doesn't cause an issue report `0` for
  // this (e.g. Edge 2019, IE11, Safari)
  return getBoundingClientRect(getDocumentElement(element)).left + getWindowScroll(element).scrollLeft;
}

function getViewportRect(element, strategy) {
  var win = getWindow(element);
  var html = getDocumentElement(element);
  var visualViewport = win.visualViewport;
  var width = html.clientWidth;
  var height = html.clientHeight;
  var x = 0;
  var y = 0;

  if (visualViewport) {
    width = visualViewport.width;
    height = visualViewport.height;
    var layoutViewport = isLayoutViewport();

    if (layoutViewport || !layoutViewport && strategy === 'fixed') {
      x = visualViewport.offsetLeft;
      y = visualViewport.offsetTop;
    }
  }

  return {
    width: width,
    height: height,
    x: x + getWindowScrollBarX(element),
    y: y
  };
}

// of the `<html>` and `<body>` rect bounds if horizontally scrollable

function getDocumentRect(element) {
  var _element$ownerDocumen;

  var html = getDocumentElement(element);
  var winScroll = getWindowScroll(element);
  var body = (_element$ownerDocumen = element.ownerDocument) == null ? void 0 : _element$ownerDocumen.body;
  var width = max(html.scrollWidth, html.clientWidth, body ? body.scrollWidth : 0, body ? body.clientWidth : 0);
  var height = max(html.scrollHeight, html.clientHeight, body ? body.scrollHeight : 0, body ? body.clientHeight : 0);
  var x = -winScroll.scrollLeft + getWindowScrollBarX(element);
  var y = -winScroll.scrollTop;

  if (getComputedStyle(body || html).direction === 'rtl') {
    x += max(html.clientWidth, body ? body.clientWidth : 0) - width;
  }

  return {
    width: width,
    height: height,
    x: x,
    y: y
  };
}

function isScrollParent(element) {
  // Firefox wants us to check `-x` and `-y` variations as well
  var _getComputedStyle = getComputedStyle(element),
      overflow = _getComputedStyle.overflow,
      overflowX = _getComputedStyle.overflowX,
      overflowY = _getComputedStyle.overflowY;

  return /auto|scroll|overlay|hidden/.test(overflow + overflowY + overflowX);
}

function getScrollParent(node) {
  if (['html', 'body', '#document'].indexOf(getNodeName(node)) >= 0) {
    // $FlowFixMe[incompatible-return]: assume body is always available
    return node.ownerDocument.body;
  }

  if (isHTMLElement(node) && isScrollParent(node)) {
    return node;
  }

  return getScrollParent(getParentNode(node));
}

/*
given a DOM element, return the list of all scroll parents, up the list of ancesors
until we get to the top window object. This list is what we attach scroll listeners
to, because if any of these parent elements scroll, we'll need to re-calculate the
reference element's position.
*/

function listScrollParents(element, list) {
  var _element$ownerDocumen;

  if (list === void 0) {
    list = [];
  }

  var scrollParent = getScrollParent(element);
  var isBody = scrollParent === ((_element$ownerDocumen = element.ownerDocument) == null ? void 0 : _element$ownerDocumen.body);
  var win = getWindow(scrollParent);
  var target = isBody ? [win].concat(win.visualViewport || [], isScrollParent(scrollParent) ? scrollParent : []) : scrollParent;
  var updatedList = list.concat(target);
  return isBody ? updatedList : // $FlowFixMe[incompatible-call]: isBody tells us target will be an HTMLElement here
  updatedList.concat(listScrollParents(getParentNode(target)));
}

function rectToClientRect(rect) {
  return Object.assign({}, rect, {
    left: rect.x,
    top: rect.y,
    right: rect.x + rect.width,
    bottom: rect.y + rect.height
  });
}

function getInnerBoundingClientRect(element, strategy) {
  var rect = getBoundingClientRect(element, false, strategy === 'fixed');
  rect.top = rect.top + element.clientTop;
  rect.left = rect.left + element.clientLeft;
  rect.bottom = rect.top + element.clientHeight;
  rect.right = rect.left + element.clientWidth;
  rect.width = element.clientWidth;
  rect.height = element.clientHeight;
  rect.x = rect.left;
  rect.y = rect.top;
  return rect;
}

function getClientRectFromMixedType(element, clippingParent, strategy) {
  return clippingParent === viewport ? rectToClientRect(getViewportRect(element, strategy)) : isElement(clippingParent) ? getInnerBoundingClientRect(clippingParent, strategy) : rectToClientRect(getDocumentRect(getDocumentElement(element)));
} // A "clipping parent" is an overflowable container with the characteristic of
// clipping (or hiding) overflowing elements with a position different from
// `initial`


function getClippingParents(element) {
  var clippingParents = listScrollParents(getParentNode(element));
  var canEscapeClipping = ['absolute', 'fixed'].indexOf(getComputedStyle(element).position) >= 0;
  var clipperElement = canEscapeClipping && isHTMLElement(element) ? getOffsetParent(element) : element;

  if (!isElement(clipperElement)) {
    return [];
  } // $FlowFixMe[incompatible-return]: https://github.com/facebook/flow/issues/1414


  return clippingParents.filter(function (clippingParent) {
    return isElement(clippingParent) && contains(clippingParent, clipperElement) && getNodeName(clippingParent) !== 'body';
  });
} // Gets the maximum area that the element is visible in due to any number of
// clipping parents


function getClippingRect(element, boundary, rootBoundary, strategy) {
  var mainClippingParents = boundary === 'clippingParents' ? getClippingParents(element) : [].concat(boundary);
  var clippingParents = [].concat(mainClippingParents, [rootBoundary]);
  var firstClippingParent = clippingParents[0];
  var clippingRect = clippingParents.reduce(function (accRect, clippingParent) {
    var rect = getClientRectFromMixedType(element, clippingParent, strategy);
    accRect.top = max(rect.top, accRect.top);
    accRect.right = min(rect.right, accRect.right);
    accRect.bottom = min(rect.bottom, accRect.bottom);
    accRect.left = max(rect.left, accRect.left);
    return accRect;
  }, getClientRectFromMixedType(element, firstClippingParent, strategy));
  clippingRect.width = clippingRect.right - clippingRect.left;
  clippingRect.height = clippingRect.bottom - clippingRect.top;
  clippingRect.x = clippingRect.left;
  clippingRect.y = clippingRect.top;
  return clippingRect;
}

function computeOffsets(_ref) {
  var reference = _ref.reference,
      element = _ref.element,
      placement = _ref.placement;
  var basePlacement = placement ? getBasePlacement(placement) : null;
  var variation = placement ? getVariation(placement) : null;
  var commonX = reference.x + reference.width / 2 - element.width / 2;
  var commonY = reference.y + reference.height / 2 - element.height / 2;
  var offsets;

  switch (basePlacement) {
    case top:
      offsets = {
        x: commonX,
        y: reference.y - element.height
      };
      break;

    case bottom:
      offsets = {
        x: commonX,
        y: reference.y + reference.height
      };
      break;

    case right:
      offsets = {
        x: reference.x + reference.width,
        y: commonY
      };
      break;

    case left:
      offsets = {
        x: reference.x - element.width,
        y: commonY
      };
      break;

    default:
      offsets = {
        x: reference.x,
        y: reference.y
      };
  }

  var mainAxis = basePlacement ? getMainAxisFromPlacement(basePlacement) : null;

  if (mainAxis != null) {
    var len = mainAxis === 'y' ? 'height' : 'width';

    switch (variation) {
      case start:
        offsets[mainAxis] = offsets[mainAxis] - (reference[len] / 2 - element[len] / 2);
        break;

      case end:
        offsets[mainAxis] = offsets[mainAxis] + (reference[len] / 2 - element[len] / 2);
        break;
    }
  }

  return offsets;
}

function detectOverflow(state, options) {
  if (options === void 0) {
    options = {};
  }

  var _options = options,
      _options$placement = _options.placement,
      placement = _options$placement === void 0 ? state.placement : _options$placement,
      _options$strategy = _options.strategy,
      strategy = _options$strategy === void 0 ? state.strategy : _options$strategy,
      _options$boundary = _options.boundary,
      boundary = _options$boundary === void 0 ? clippingParents : _options$boundary,
      _options$rootBoundary = _options.rootBoundary,
      rootBoundary = _options$rootBoundary === void 0 ? viewport : _options$rootBoundary,
      _options$elementConte = _options.elementContext,
      elementContext = _options$elementConte === void 0 ? popper : _options$elementConte,
      _options$altBoundary = _options.altBoundary,
      altBoundary = _options$altBoundary === void 0 ? false : _options$altBoundary,
      _options$padding = _options.padding,
      padding = _options$padding === void 0 ? 0 : _options$padding;
  var paddingObject = mergePaddingObject(typeof padding !== 'number' ? padding : expandToHashMap(padding, basePlacements));
  var altContext = elementContext === popper ? reference : popper;
  var popperRect = state.rects.popper;
  var element = state.elements[altBoundary ? altContext : elementContext];
  var clippingClientRect = getClippingRect(isElement(element) ? element : element.contextElement || getDocumentElement(state.elements.popper), boundary, rootBoundary, strategy);
  var referenceClientRect = getBoundingClientRect(state.elements.reference);
  var popperOffsets = computeOffsets({
    reference: referenceClientRect,
    element: popperRect,
    strategy: 'absolute',
    placement: placement
  });
  var popperClientRect = rectToClientRect(Object.assign({}, popperRect, popperOffsets));
  var elementClientRect = elementContext === popper ? popperClientRect : referenceClientRect; // positive = overflowing the clipping rect
  // 0 or negative = within the clipping rect

  var overflowOffsets = {
    top: clippingClientRect.top - elementClientRect.top + paddingObject.top,
    bottom: elementClientRect.bottom - clippingClientRect.bottom + paddingObject.bottom,
    left: clippingClientRect.left - elementClientRect.left + paddingObject.left,
    right: elementClientRect.right - clippingClientRect.right + paddingObject.right
  };
  var offsetData = state.modifiersData.offset; // Offsets can be applied only to the popper element

  if (elementContext === popper && offsetData) {
    var offset = offsetData[placement];
    Object.keys(overflowOffsets).forEach(function (key) {
      var multiply = [right, bottom].indexOf(key) >= 0 ? 1 : -1;
      var axis = [top, bottom].indexOf(key) >= 0 ? 'y' : 'x';
      overflowOffsets[key] += offset[axis] * multiply;
    });
  }

  return overflowOffsets;
}

function computeAutoPlacement(state, options) {
  if (options === void 0) {
    options = {};
  }

  var _options = options,
      placement = _options.placement,
      boundary = _options.boundary,
      rootBoundary = _options.rootBoundary,
      padding = _options.padding,
      flipVariations = _options.flipVariations,
      _options$allowedAutoP = _options.allowedAutoPlacements,
      allowedAutoPlacements = _options$allowedAutoP === void 0 ? placements : _options$allowedAutoP;
  var variation = getVariation(placement);
  var placements$1 = variation ? flipVariations ? variationPlacements : variationPlacements.filter(function (placement) {
    return getVariation(placement) === variation;
  }) : basePlacements;
  var allowedPlacements = placements$1.filter(function (placement) {
    return allowedAutoPlacements.indexOf(placement) >= 0;
  });

  if (allowedPlacements.length === 0) {
    allowedPlacements = placements$1;
  } // $FlowFixMe[incompatible-type]: Flow seems to have problems with two array unions...


  var overflows = allowedPlacements.reduce(function (acc, placement) {
    acc[placement] = detectOverflow(state, {
      placement: placement,
      boundary: boundary,
      rootBoundary: rootBoundary,
      padding: padding
    })[getBasePlacement(placement)];
    return acc;
  }, {});
  return Object.keys(overflows).sort(function (a, b) {
    return overflows[a] - overflows[b];
  });
}

function getExpandedFallbackPlacements(placement) {
  if (getBasePlacement(placement) === auto) {
    return [];
  }

  var oppositePlacement = getOppositePlacement(placement);
  return [getOppositeVariationPlacement(placement), oppositePlacement, getOppositeVariationPlacement(oppositePlacement)];
}

function flip(_ref) {
  var state = _ref.state,
      options = _ref.options,
      name = _ref.name;

  if (state.modifiersData[name]._skip) {
    return;
  }

  var _options$mainAxis = options.mainAxis,
      checkMainAxis = _options$mainAxis === void 0 ? true : _options$mainAxis,
      _options$altAxis = options.altAxis,
      checkAltAxis = _options$altAxis === void 0 ? true : _options$altAxis,
      specifiedFallbackPlacements = options.fallbackPlacements,
      padding = options.padding,
      boundary = options.boundary,
      rootBoundary = options.rootBoundary,
      altBoundary = options.altBoundary,
      _options$flipVariatio = options.flipVariations,
      flipVariations = _options$flipVariatio === void 0 ? true : _options$flipVariatio,
      allowedAutoPlacements = options.allowedAutoPlacements;
  var preferredPlacement = state.options.placement;
  var basePlacement = getBasePlacement(preferredPlacement);
  var isBasePlacement = basePlacement === preferredPlacement;
  var fallbackPlacements = specifiedFallbackPlacements || (isBasePlacement || !flipVariations ? [getOppositePlacement(preferredPlacement)] : getExpandedFallbackPlacements(preferredPlacement));
  var placements = [preferredPlacement].concat(fallbackPlacements).reduce(function (acc, placement) {
    return acc.concat(getBasePlacement(placement) === auto ? computeAutoPlacement(state, {
      placement: placement,
      boundary: boundary,
      rootBoundary: rootBoundary,
      padding: padding,
      flipVariations: flipVariations,
      allowedAutoPlacements: allowedAutoPlacements
    }) : placement);
  }, []);
  var referenceRect = state.rects.reference;
  var popperRect = state.rects.popper;
  var checksMap = new Map();
  var makeFallbackChecks = true;
  var firstFittingPlacement = placements[0];

  for (var i = 0; i < placements.length; i++) {
    var placement = placements[i];

    var _basePlacement = getBasePlacement(placement);

    var isStartVariation = getVariation(placement) === start;
    var isVertical = [top, bottom].indexOf(_basePlacement) >= 0;
    var len = isVertical ? 'width' : 'height';
    var overflow = detectOverflow(state, {
      placement: placement,
      boundary: boundary,
      rootBoundary: rootBoundary,
      altBoundary: altBoundary,
      padding: padding
    });
    var mainVariationSide = isVertical ? isStartVariation ? right : left : isStartVariation ? bottom : top;

    if (referenceRect[len] > popperRect[len]) {
      mainVariationSide = getOppositePlacement(mainVariationSide);
    }

    var altVariationSide = getOppositePlacement(mainVariationSide);
    var checks = [];

    if (checkMainAxis) {
      checks.push(overflow[_basePlacement] <= 0);
    }

    if (checkAltAxis) {
      checks.push(overflow[mainVariationSide] <= 0, overflow[altVariationSide] <= 0);
    }

    if (checks.every(function (check) {
      return check;
    })) {
      firstFittingPlacement = placement;
      makeFallbackChecks = false;
      break;
    }

    checksMap.set(placement, checks);
  }

  if (makeFallbackChecks) {
    // `2` may be desired in some cases – research later
    var numberOfChecks = flipVariations ? 3 : 1;

    var _loop = function _loop(_i) {
      var fittingPlacement = placements.find(function (placement) {
        var checks = checksMap.get(placement);

        if (checks) {
          return checks.slice(0, _i).every(function (check) {
            return check;
          });
        }
      });

      if (fittingPlacement) {
        firstFittingPlacement = fittingPlacement;
        return "break";
      }
    };

    for (var _i = numberOfChecks; _i > 0; _i--) {
      var _ret = _loop(_i);

      if (_ret === "break") break;
    }
  }

  if (state.placement !== firstFittingPlacement) {
    state.modifiersData[name]._skip = true;
    state.placement = firstFittingPlacement;
    state.reset = true;
  }
} // eslint-disable-next-line import/no-unused-modules


var flip$1 = {
  name: 'flip',
  enabled: true,
  phase: 'main',
  fn: flip,
  requiresIfExists: ['offset'],
  data: {
    _skip: false
  }
};

function getSideOffsets(overflow, rect, preventedOffsets) {
  if (preventedOffsets === void 0) {
    preventedOffsets = {
      x: 0,
      y: 0
    };
  }

  return {
    top: overflow.top - rect.height - preventedOffsets.y,
    right: overflow.right - rect.width + preventedOffsets.x,
    bottom: overflow.bottom - rect.height + preventedOffsets.y,
    left: overflow.left - rect.width - preventedOffsets.x
  };
}

function isAnySideFullyClipped(overflow) {
  return [top, right, bottom, left].some(function (side) {
    return overflow[side] >= 0;
  });
}

function hide(_ref) {
  var state = _ref.state,
      name = _ref.name;
  var referenceRect = state.rects.reference;
  var popperRect = state.rects.popper;
  var preventedOffsets = state.modifiersData.preventOverflow;
  var referenceOverflow = detectOverflow(state, {
    elementContext: 'reference'
  });
  var popperAltOverflow = detectOverflow(state, {
    altBoundary: true
  });
  var referenceClippingOffsets = getSideOffsets(referenceOverflow, referenceRect);
  var popperEscapeOffsets = getSideOffsets(popperAltOverflow, popperRect, preventedOffsets);
  var isReferenceHidden = isAnySideFullyClipped(referenceClippingOffsets);
  var hasPopperEscaped = isAnySideFullyClipped(popperEscapeOffsets);
  state.modifiersData[name] = {
    referenceClippingOffsets: referenceClippingOffsets,
    popperEscapeOffsets: popperEscapeOffsets,
    isReferenceHidden: isReferenceHidden,
    hasPopperEscaped: hasPopperEscaped
  };
  state.attributes.popper = Object.assign({}, state.attributes.popper, {
    'data-popper-reference-hidden': isReferenceHidden,
    'data-popper-escaped': hasPopperEscaped
  });
} // eslint-disable-next-line import/no-unused-modules


var hide$1 = {
  name: 'hide',
  enabled: true,
  phase: 'main',
  requiresIfExists: ['preventOverflow'],
  fn: hide
};

function distanceAndSkiddingToXY(placement, rects, offset) {
  var basePlacement = getBasePlacement(placement);
  var invertDistance = [left, top].indexOf(basePlacement) >= 0 ? -1 : 1;

  var _ref = typeof offset === 'function' ? offset(Object.assign({}, rects, {
    placement: placement
  })) : offset,
      skidding = _ref[0],
      distance = _ref[1];

  skidding = skidding || 0;
  distance = (distance || 0) * invertDistance;
  return [left, right].indexOf(basePlacement) >= 0 ? {
    x: distance,
    y: skidding
  } : {
    x: skidding,
    y: distance
  };
}

function offset(_ref2) {
  var state = _ref2.state,
      options = _ref2.options,
      name = _ref2.name;
  var _options$offset = options.offset,
      offset = _options$offset === void 0 ? [0, 0] : _options$offset;
  var data = placements.reduce(function (acc, placement) {
    acc[placement] = distanceAndSkiddingToXY(placement, state.rects, offset);
    return acc;
  }, {});
  var _data$state$placement = data[state.placement],
      x = _data$state$placement.x,
      y = _data$state$placement.y;

  if (state.modifiersData.popperOffsets != null) {
    state.modifiersData.popperOffsets.x += x;
    state.modifiersData.popperOffsets.y += y;
  }

  state.modifiersData[name] = data;
} // eslint-disable-next-line import/no-unused-modules


var offset$1 = {
  name: 'offset',
  enabled: true,
  phase: 'main',
  requires: ['popperOffsets'],
  fn: offset
};

function popperOffsets(_ref) {
  var state = _ref.state,
      name = _ref.name;
  // Offsets are the actual position the popper needs to have to be
  // properly positioned near its reference element
  // This is the most basic placement, and will be adjusted by
  // the modifiers in the next step
  state.modifiersData[name] = computeOffsets({
    reference: state.rects.reference,
    element: state.rects.popper,
    strategy: 'absolute',
    placement: state.placement
  });
} // eslint-disable-next-line import/no-unused-modules


var popperOffsets$1 = {
  name: 'popperOffsets',
  enabled: true,
  phase: 'read',
  fn: popperOffsets,
  data: {}
};

function getAltAxis(axis) {
  return axis === 'x' ? 'y' : 'x';
}

function preventOverflow(_ref) {
  var state = _ref.state,
      options = _ref.options,
      name = _ref.name;
  var _options$mainAxis = options.mainAxis,
      checkMainAxis = _options$mainAxis === void 0 ? true : _options$mainAxis,
      _options$altAxis = options.altAxis,
      checkAltAxis = _options$altAxis === void 0 ? false : _options$altAxis,
      boundary = options.boundary,
      rootBoundary = options.rootBoundary,
      altBoundary = options.altBoundary,
      padding = options.padding,
      _options$tether = options.tether,
      tether = _options$tether === void 0 ? true : _options$tether,
      _options$tetherOffset = options.tetherOffset,
      tetherOffset = _options$tetherOffset === void 0 ? 0 : _options$tetherOffset;
  var overflow = detectOverflow(state, {
    boundary: boundary,
    rootBoundary: rootBoundary,
    padding: padding,
    altBoundary: altBoundary
  });
  var basePlacement = getBasePlacement(state.placement);
  var variation = getVariation(state.placement);
  var isBasePlacement = !variation;
  var mainAxis = getMainAxisFromPlacement(basePlacement);
  var altAxis = getAltAxis(mainAxis);
  var popperOffsets = state.modifiersData.popperOffsets;
  var referenceRect = state.rects.reference;
  var popperRect = state.rects.popper;
  var tetherOffsetValue = typeof tetherOffset === 'function' ? tetherOffset(Object.assign({}, state.rects, {
    placement: state.placement
  })) : tetherOffset;
  var normalizedTetherOffsetValue = typeof tetherOffsetValue === 'number' ? {
    mainAxis: tetherOffsetValue,
    altAxis: tetherOffsetValue
  } : Object.assign({
    mainAxis: 0,
    altAxis: 0
  }, tetherOffsetValue);
  var offsetModifierState = state.modifiersData.offset ? state.modifiersData.offset[state.placement] : null;
  var data = {
    x: 0,
    y: 0
  };

  if (!popperOffsets) {
    return;
  }

  if (checkMainAxis) {
    var _offsetModifierState$;

    var mainSide = mainAxis === 'y' ? top : left;
    var altSide = mainAxis === 'y' ? bottom : right;
    var len = mainAxis === 'y' ? 'height' : 'width';
    var offset = popperOffsets[mainAxis];
    var min$1 = offset + overflow[mainSide];
    var max$1 = offset - overflow[altSide];
    var additive = tether ? -popperRect[len] / 2 : 0;
    var minLen = variation === start ? referenceRect[len] : popperRect[len];
    var maxLen = variation === start ? -popperRect[len] : -referenceRect[len]; // We need to include the arrow in the calculation so the arrow doesn't go
    // outside the reference bounds

    var arrowElement = state.elements.arrow;
    var arrowRect = tether && arrowElement ? getLayoutRect(arrowElement) : {
      width: 0,
      height: 0
    };
    var arrowPaddingObject = state.modifiersData['arrow#persistent'] ? state.modifiersData['arrow#persistent'].padding : getFreshSideObject();
    var arrowPaddingMin = arrowPaddingObject[mainSide];
    var arrowPaddingMax = arrowPaddingObject[altSide]; // If the reference length is smaller than the arrow length, we don't want
    // to include its full size in the calculation. If the reference is small
    // and near the edge of a boundary, the popper can overflow even if the
    // reference is not overflowing as well (e.g. virtual elements with no
    // width or height)

    var arrowLen = within(0, referenceRect[len], arrowRect[len]);
    var minOffset = isBasePlacement ? referenceRect[len] / 2 - additive - arrowLen - arrowPaddingMin - normalizedTetherOffsetValue.mainAxis : minLen - arrowLen - arrowPaddingMin - normalizedTetherOffsetValue.mainAxis;
    var maxOffset = isBasePlacement ? -referenceRect[len] / 2 + additive + arrowLen + arrowPaddingMax + normalizedTetherOffsetValue.mainAxis : maxLen + arrowLen + arrowPaddingMax + normalizedTetherOffsetValue.mainAxis;
    var arrowOffsetParent = state.elements.arrow && getOffsetParent(state.elements.arrow);
    var clientOffset = arrowOffsetParent ? mainAxis === 'y' ? arrowOffsetParent.clientTop || 0 : arrowOffsetParent.clientLeft || 0 : 0;
    var offsetModifierValue = (_offsetModifierState$ = offsetModifierState == null ? void 0 : offsetModifierState[mainAxis]) != null ? _offsetModifierState$ : 0;
    var tetherMin = offset + minOffset - offsetModifierValue - clientOffset;
    var tetherMax = offset + maxOffset - offsetModifierValue;
    var preventedOffset = within(tether ? min(min$1, tetherMin) : min$1, offset, tether ? max(max$1, tetherMax) : max$1);
    popperOffsets[mainAxis] = preventedOffset;
    data[mainAxis] = preventedOffset - offset;
  }

  if (checkAltAxis) {
    var _offsetModifierState$2;

    var _mainSide = mainAxis === 'x' ? top : left;

    var _altSide = mainAxis === 'x' ? bottom : right;

    var _offset = popperOffsets[altAxis];

    var _len = altAxis === 'y' ? 'height' : 'width';

    var _min = _offset + overflow[_mainSide];

    var _max = _offset - overflow[_altSide];

    var isOriginSide = [top, left].indexOf(basePlacement) !== -1;

    var _offsetModifierValue = (_offsetModifierState$2 = offsetModifierState == null ? void 0 : offsetModifierState[altAxis]) != null ? _offsetModifierState$2 : 0;

    var _tetherMin = isOriginSide ? _min : _offset - referenceRect[_len] - popperRect[_len] - _offsetModifierValue + normalizedTetherOffsetValue.altAxis;

    var _tetherMax = isOriginSide ? _offset + referenceRect[_len] + popperRect[_len] - _offsetModifierValue - normalizedTetherOffsetValue.altAxis : _max;

    var _preventedOffset = tether && isOriginSide ? withinMaxClamp(_tetherMin, _offset, _tetherMax) : within(tether ? _tetherMin : _min, _offset, tether ? _tetherMax : _max);

    popperOffsets[altAxis] = _preventedOffset;
    data[altAxis] = _preventedOffset - _offset;
  }

  state.modifiersData[name] = data;
} // eslint-disable-next-line import/no-unused-modules


var preventOverflow$1 = {
  name: 'preventOverflow',
  enabled: true,
  phase: 'main',
  fn: preventOverflow,
  requiresIfExists: ['offset']
};

function getHTMLElementScroll(element) {
  return {
    scrollLeft: element.scrollLeft,
    scrollTop: element.scrollTop
  };
}

function getNodeScroll(node) {
  if (node === getWindow(node) || !isHTMLElement(node)) {
    return getWindowScroll(node);
  } else {
    return getHTMLElementScroll(node);
  }
}

function isElementScaled(element) {
  var rect = element.getBoundingClientRect();
  var scaleX = round(rect.width) / element.offsetWidth || 1;
  var scaleY = round(rect.height) / element.offsetHeight || 1;
  return scaleX !== 1 || scaleY !== 1;
} // Returns the composite rect of an element relative to its offsetParent.
// Composite means it takes into account transforms as well as layout.


function getCompositeRect(elementOrVirtualElement, offsetParent, isFixed) {
  if (isFixed === void 0) {
    isFixed = false;
  }

  var isOffsetParentAnElement = isHTMLElement(offsetParent);
  var offsetParentIsScaled = isHTMLElement(offsetParent) && isElementScaled(offsetParent);
  var documentElement = getDocumentElement(offsetParent);
  var rect = getBoundingClientRect(elementOrVirtualElement, offsetParentIsScaled, isFixed);
  var scroll = {
    scrollLeft: 0,
    scrollTop: 0
  };
  var offsets = {
    x: 0,
    y: 0
  };

  if (isOffsetParentAnElement || !isOffsetParentAnElement && !isFixed) {
    if (getNodeName(offsetParent) !== 'body' || // https://github.com/popperjs/popper-core/issues/1078
    isScrollParent(documentElement)) {
      scroll = getNodeScroll(offsetParent);
    }

    if (isHTMLElement(offsetParent)) {
      offsets = getBoundingClientRect(offsetParent, true);
      offsets.x += offsetParent.clientLeft;
      offsets.y += offsetParent.clientTop;
    } else if (documentElement) {
      offsets.x = getWindowScrollBarX(documentElement);
    }
  }

  return {
    x: rect.left + scroll.scrollLeft - offsets.x,
    y: rect.top + scroll.scrollTop - offsets.y,
    width: rect.width,
    height: rect.height
  };
}

function order(modifiers) {
  var map = new Map();
  var visited = new Set();
  var result = [];
  modifiers.forEach(function (modifier) {
    map.set(modifier.name, modifier);
  }); // On visiting object, check for its dependencies and visit them recursively

  function sort(modifier) {
    visited.add(modifier.name);
    var requires = [].concat(modifier.requires || [], modifier.requiresIfExists || []);
    requires.forEach(function (dep) {
      if (!visited.has(dep)) {
        var depModifier = map.get(dep);

        if (depModifier) {
          sort(depModifier);
        }
      }
    });
    result.push(modifier);
  }

  modifiers.forEach(function (modifier) {
    if (!visited.has(modifier.name)) {
      // check for visited object
      sort(modifier);
    }
  });
  return result;
}

function orderModifiers(modifiers) {
  // order based on dependencies
  var orderedModifiers = order(modifiers); // order based on phase

  return modifierPhases.reduce(function (acc, phase) {
    return acc.concat(orderedModifiers.filter(function (modifier) {
      return modifier.phase === phase;
    }));
  }, []);
}

function debounce(fn) {
  var pending;
  return function () {
    if (!pending) {
      pending = new Promise(function (resolve) {
        Promise.resolve().then(function () {
          pending = undefined;
          resolve(fn());
        });
      });
    }

    return pending;
  };
}

function mergeByName(modifiers) {
  var merged = modifiers.reduce(function (merged, current) {
    var existing = merged[current.name];
    merged[current.name] = existing ? Object.assign({}, existing, current, {
      options: Object.assign({}, existing.options, current.options),
      data: Object.assign({}, existing.data, current.data)
    }) : current;
    return merged;
  }, {}); // IE11 does not support Object.values

  return Object.keys(merged).map(function (key) {
    return merged[key];
  });
}

var DEFAULT_OPTIONS = {
  placement: 'bottom',
  modifiers: [],
  strategy: 'absolute'
};

function areValidElements() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return !args.some(function (element) {
    return !(element && typeof element.getBoundingClientRect === 'function');
  });
}

function popperGenerator(generatorOptions) {
  if (generatorOptions === void 0) {
    generatorOptions = {};
  }

  var _generatorOptions = generatorOptions,
      _generatorOptions$def = _generatorOptions.defaultModifiers,
      defaultModifiers = _generatorOptions$def === void 0 ? [] : _generatorOptions$def,
      _generatorOptions$def2 = _generatorOptions.defaultOptions,
      defaultOptions = _generatorOptions$def2 === void 0 ? DEFAULT_OPTIONS : _generatorOptions$def2;
  return function createPopper(reference, popper, options) {
    if (options === void 0) {
      options = defaultOptions;
    }

    var state = {
      placement: 'bottom',
      orderedModifiers: [],
      options: Object.assign({}, DEFAULT_OPTIONS, defaultOptions),
      modifiersData: {},
      elements: {
        reference: reference,
        popper: popper
      },
      attributes: {},
      styles: {}
    };
    var effectCleanupFns = [];
    var isDestroyed = false;
    var instance = {
      state: state,
      setOptions: function setOptions(setOptionsAction) {
        var options = typeof setOptionsAction === 'function' ? setOptionsAction(state.options) : setOptionsAction;
        cleanupModifierEffects();
        state.options = Object.assign({}, defaultOptions, state.options, options);
        state.scrollParents = {
          reference: isElement(reference) ? listScrollParents(reference) : reference.contextElement ? listScrollParents(reference.contextElement) : [],
          popper: listScrollParents(popper)
        }; // Orders the modifiers based on their dependencies and `phase`
        // properties

        var orderedModifiers = orderModifiers(mergeByName([].concat(defaultModifiers, state.options.modifiers))); // Strip out disabled modifiers

        state.orderedModifiers = orderedModifiers.filter(function (m) {
          return m.enabled;
        });
        runModifierEffects();
        return instance.update();
      },
      // Sync update – it will always be executed, even if not necessary. This
      // is useful for low frequency updates where sync behavior simplifies the
      // logic.
      // For high frequency updates (e.g. `resize` and `scroll` events), always
      // prefer the async Popper#update method
      forceUpdate: function forceUpdate() {
        if (isDestroyed) {
          return;
        }

        var _state$elements = state.elements,
            reference = _state$elements.reference,
            popper = _state$elements.popper; // Don't proceed if `reference` or `popper` are not valid elements
        // anymore

        if (!areValidElements(reference, popper)) {
          return;
        } // Store the reference and popper rects to be read by modifiers


        state.rects = {
          reference: getCompositeRect(reference, getOffsetParent(popper), state.options.strategy === 'fixed'),
          popper: getLayoutRect(popper)
        }; // Modifiers have the ability to reset the current update cycle. The
        // most common use case for this is the `flip` modifier changing the
        // placement, which then needs to re-run all the modifiers, because the
        // logic was previously ran for the previous placement and is therefore
        // stale/incorrect

        state.reset = false;
        state.placement = state.options.placement; // On each update cycle, the `modifiersData` property for each modifier
        // is filled with the initial data specified by the modifier. This means
        // it doesn't persist and is fresh on each update.
        // To ensure persistent data, use `${name}#persistent`

        state.orderedModifiers.forEach(function (modifier) {
          return state.modifiersData[modifier.name] = Object.assign({}, modifier.data);
        });

        for (var index = 0; index < state.orderedModifiers.length; index++) {
          if (state.reset === true) {
            state.reset = false;
            index = -1;
            continue;
          }

          var _state$orderedModifie = state.orderedModifiers[index],
              fn = _state$orderedModifie.fn,
              _state$orderedModifie2 = _state$orderedModifie.options,
              _options = _state$orderedModifie2 === void 0 ? {} : _state$orderedModifie2,
              name = _state$orderedModifie.name;

          if (typeof fn === 'function') {
            state = fn({
              state: state,
              options: _options,
              name: name,
              instance: instance
            }) || state;
          }
        }
      },
      // Async and optimistically optimized update – it will not be executed if
      // not necessary (debounced to run at most once-per-tick)
      update: debounce(function () {
        return new Promise(function (resolve) {
          instance.forceUpdate();
          resolve(state);
        });
      }),
      destroy: function destroy() {
        cleanupModifierEffects();
        isDestroyed = true;
      }
    };

    if (!areValidElements(reference, popper)) {
      return instance;
    }

    instance.setOptions(options).then(function (state) {
      if (!isDestroyed && options.onFirstUpdate) {
        options.onFirstUpdate(state);
      }
    }); // Modifiers have the ability to execute arbitrary code before the first
    // update cycle runs. They will be executed in the same order as the update
    // cycle. This is useful when a modifier adds some persistent data that
    // other modifiers need to use, but the modifier is run after the dependent
    // one.

    function runModifierEffects() {
      state.orderedModifiers.forEach(function (_ref) {
        var name = _ref.name,
            _ref$options = _ref.options,
            options = _ref$options === void 0 ? {} : _ref$options,
            effect = _ref.effect;

        if (typeof effect === 'function') {
          var cleanupFn = effect({
            state: state,
            name: name,
            instance: instance,
            options: options
          });

          var noopFn = function noopFn() {};

          effectCleanupFns.push(cleanupFn || noopFn);
        }
      });
    }

    function cleanupModifierEffects() {
      effectCleanupFns.forEach(function (fn) {
        return fn();
      });
      effectCleanupFns = [];
    }

    return instance;
  };
}

var defaultModifiers = [eventListeners, popperOffsets$1, computeStyles$1, applyStyles$1, offset$1, flip$1, preventOverflow$1, arrow$1, hide$1];
var createPopper = /*#__PURE__*/popperGenerator({
  defaultModifiers: defaultModifiers
}); // eslint-disable-next-line import/no-unused-modules

var ARROW_SIZE_CSS_VAR = '--popper-arrow-size';
var ARROW_OFFSET_CSS_VAR = '--popper-arrow-offset';
var ARROW_SHADOW_CSS_VAR = '--popper-arrow-shadow-color';
var ARROW_BG_CSS_VAR = '--popper-arrow-bg';
var DATA_POPPER_ARROW_ATTR = 'data-popper-arrow';
var DATA_POPPER_HIDDEN_ATTR = 'data-popper-reference-hidden';
var popperEventListeners = {
    scroll: true,
    resize: true,
};
var getEventListenersModifier = function (value) {
    if (typeof value === 'object') {
        return {
            enabled: true,
            options: __assign(__assign({}, popperEventListeners), value),
        };
    }
    return {
        enabled: value,
        options: popperEventListeners,
    };
};
var getArrowStyle$1 = function (props) {
    var _a = props.size, size = _a === void 0 ? '12px' : _a, shadowColor = props.shadowColor, bg = props.bg, style = props.style;
    var computedStyle = __assign(__assign({}, style), { position: 'absolute' });
    if (size) {
        computedStyle[ARROW_SIZE_CSS_VAR] = size;
    }
    if (shadowColor) {
        computedStyle[ARROW_SHADOW_CSS_VAR] = shadowColor;
    }
    if (bg) {
        computedStyle[ARROW_BG_CSS_VAR] = bg;
    }
    return computedStyle;
};
var toVar = function (cssVar, defaultValue) {
    return !defaultValue ? "var(".concat(cssVar, ")") : "var(".concat(cssVar, ", ").concat(defaultValue, ")");
};

/* -------------------------------------------------------------------------------------------------
 The match width modifier sets the popper width to match the reference.
 It is useful for custom selects, autocomplete, etc.
* -----------------------------------------------------------------------------------------------*/
var matchToTriggerWidth = {
    name: 'matchToTriggerWidth',
    enabled: true,
    phase: 'beforeWrite',
    requires: ['computeStyles'],
    fn: function (_a) {
        var state = _a.state;
        // eslint-disable-next-line no-param-reassign
        state.styles.popper.width = "".concat(state.rects.reference.width, "px");
    },
    effect: function (_a) {
        var state = _a.state;
        return function () {
            var reference = state.elements.reference;
            // eslint-disable-next-line no-param-reassign
            state.elements.popper.style.width = "".concat(reference.offsetWidth, "px");
        };
    },
};
/* -------------------------------------------------------------------------------------------------
  The position arrow modifier adds width, height and overrides the `top/left/right/bottom`
  styles generated by popper.js to properly position the arrow
* -----------------------------------------------------------------------------------------------*/
var arrowSize = toVar(ARROW_SIZE_CSS_VAR);
var arrowOffset = toVar(ARROW_OFFSET_CSS_VAR);
// eslint-disable-next-line consistent-return
var getArrowStyle = function (placement) {
    if (placement.startsWith('top')) {
        return { property: 'bottom', value: arrowOffset };
    }
    if (placement.startsWith('bottom')) {
        return { property: 'top', value: arrowOffset };
    }
    if (placement.startsWith('left')) {
        return { property: 'right', value: arrowOffset };
    }
    if (placement.startsWith('right')) {
        return { property: 'left', value: arrowOffset };
    }
};
var setArrowStyles = function (state) {
    var _a;
    var _b;
    if (!state.placement)
        return;
    var overrides = getArrowStyle(state.placement);
    if (((_b = state.elements) === null || _b === void 0 ? void 0 : _b.arrow) && overrides) {
        Object.assign(state.elements.arrow.style, (_a = {},
            _a[overrides.property] = overrides.value,
            _a.width = arrowSize,
            _a.height = arrowSize,
            _a.zIndex = -100,
            _a));
        state.elements.arrow.style.setProperty(ARROW_OFFSET_CSS_VAR, "calc(calc(".concat(arrowSize, " / 2 - 1px) * -1)"));
    }
};
var positionArrow = {
    name: 'positionArrow',
    enabled: true,
    phase: 'afterWrite',
    fn: function (_a) {
        var state = _a.state;
        setArrowStyles(state);
    },
};
/* -------------------------------------------------------------------------------------------------
  This modifier lets you hide the popper if it appears to be detached from its reference element,
  or attached to nothing at all. This can occur when the reference element is inside a scrolling
  container and the popper is in a different context.
* -----------------------------------------------------------------------------------------------*/
var hidePopper = {
    name: 'hidePopper',
    enabled: true,
    phase: 'main',
    requires: ['hide'],
    fn: function (_a) {
        var state = _a.state;
        var hide = state.elements.popper.hasAttribute(DATA_POPPER_HIDDEN_ATTR);
        // eslint-disable-next-line no-param-reassign
        state.styles.popper.visibility = hide ? "hidden" : 'initial';
    },
};

var usePopover = function (props) {
    if (props === void 0) { props = {}; }
    var _a = props.enabled, enabled = _a === void 0 ? true : _a, modifiers = props.modifiers, _b = props.placement, placement = _b === void 0 ? 'top' : _b, _c = props.strategy, strategy = _c === void 0 ? 'absolute' : _c, _d = props.arrow, arrow = _d === void 0 ? true : _d, _e = props.arrowPadding, arrowPadding = _e === void 0 ? 8 : _e, _f = props.eventListeners, eventListeners = _f === void 0 ? true : _f, offset = props.offset, _g = props.gutter, gutter = _g === void 0 ? 8 : _g, _h = props.flip, flip = _h === void 0 ? true : _h, _j = props.boundary, boundary = _j === void 0 ? 'clippingParents' : _j, _k = props.preventOverflow, preventOverflow = _k === void 0 ? true : _k, _l = props.clipOverflow, clipOverflow = _l === void 0 ? true : _l, matchWidth = props.matchWidth;
    var targetRef = React__default["default"].useRef(null);
    var popperRef = React__default["default"].useRef(null);
    var popperInstance = React__default["default"].useRef(null);
    var cleanup = React__default["default"].useRef(function () { });
    var initializePopper = React__default["default"].useCallback(function () {
        var _a;
        if (!enabled || !targetRef.current || !popperRef.current)
            return;
        (_a = cleanup.current) === null || _a === void 0 ? void 0 : _a.call(cleanup); // clear previous popper instance
        popperInstance.current = createPopper(targetRef.current, popperRef.current, {
            placement: placement,
            strategy: strategy,
            modifiers: __spreadArray([
                positionArrow,
                hidePopper,
                __assign(__assign({}, matchToTriggerWidth), { enabled: Boolean(matchWidth) }),
                __assign({ name: 'eventListeners' }, getEventListenersModifier(eventListeners)),
                {
                    name: 'arrow',
                    enabled: Boolean(arrow),
                    options: { padding: arrowPadding },
                },
                {
                    name: 'offset',
                    options: {
                        offset: offset !== null && offset !== void 0 ? offset : [0, gutter],
                    },
                },
                {
                    name: 'flip',
                    enabled: Boolean(flip),
                    options: { padding: 8 },
                },
                {
                    name: 'preventOverflow',
                    enabled: Boolean(preventOverflow),
                    options: {
                        boundary: boundary,
                        altBoundary: true,
                        mainAxis: true,
                        altAxis: true,
                    },
                },
                {
                    name: 'hide',
                    enabled: Boolean(clipOverflow),
                }
            ], (modifiers !== null && modifiers !== void 0 ? modifiers : []), true),
        });
        // force update one-time to fix any positioning issues
        popperInstance.current.forceUpdate();
        cleanup.current = popperInstance.current.destroy;
    }, [
        placement,
        enabled,
        modifiers,
        matchWidth,
        eventListeners,
        arrow,
        arrowPadding,
        offset,
        gutter,
        flip,
        preventOverflow,
        boundary,
        strategy,
        clipOverflow,
    ]);
    // onUnmount
    React__default["default"].useEffect(function () { return function () {
        var _a;
        if (!targetRef.current && !popperRef.current) {
            (_a = popperInstance.current) === null || _a === void 0 ? void 0 : _a.destroy();
            popperInstance.current = null;
        }
    }; }, []);
    var targetRefSetter = React__default["default"].useCallback(function (node) {
        targetRef.current = node;
        initializePopper();
    }, [initializePopper]);
    var popperRefSetter = React__default["default"].useCallback(function (node) {
        popperRef.current = node;
        initializePopper();
    }, [initializePopper]);
    var getReferenceProps = React__default["default"].useCallback(function (refProps, ref) {
        if (refProps === void 0) { refProps = {}; }
        if (ref === void 0) { ref = null; }
        return (__assign(__assign({}, refProps), { ref: useAssignRef(targetRef, ref) }));
    }, [targetRef]);
    var getPopperProps = React__default["default"].useCallback(function (popperProps, ref) {
        if (popperProps === void 0) { popperProps = {}; }
        if (ref === void 0) { ref = null; }
        return (__assign(__assign({}, popperProps), { ref: useAssignRef(popperRef, ref) }));
    }, [popperRef]);
    var getArrowProps = React__default["default"].useCallback(function (arrowProps, ref) {
        var _a;
        if (arrowProps === void 0) { arrowProps = {}; }
        if (ref === void 0) { ref = null; }
        arrowProps.size; arrowProps.shadowColor; arrowProps.bg; arrowProps.style; var rest = __rest(arrowProps, ["size", "shadowColor", "bg", "style"]);
        return __assign(__assign({}, rest), (_a = { ref: ref }, _a[DATA_POPPER_ARROW_ATTR] = '', _a.style = getArrowStyle$1(arrowProps), _a));
    }, []);
    return {
        update: function () {
            var _a;
            (_a = popperInstance.current) === null || _a === void 0 ? void 0 : _a.update();
        },
        forceUpdate: function () {
            var _a;
            (_a = popperInstance.current) === null || _a === void 0 ? void 0 : _a.forceUpdate();
        },
        instance: popperInstance.current,
        targetRef: targetRefSetter,
        popperRef: popperRefSetter,
        getReferenceProps: getReferenceProps,
        getPopperProps: getPopperProps,
        getArrowProps: getArrowProps,
    };
};

var TooltipTest = {
    prefix: 'Tooltip',
};
/**
 * Tooltip
 *
 * @example
 *```tsx
 * import { Tooltip } from 'styleguide-react';
 *
 * <Tooltip title="Title" label="label" placement="top">
 *      <span>Open Tooltip</span>
 * </Tooltip>
 *```
 */
var Tooltip = React__namespace.forwardRef(function (_a, forwardedRef) {
    var id = _a.id, className = _a.className, triggerClassName = _a.triggerClassName, testId = _a.testId, children = _a.children, label = _a.label, title = _a.title, _b = _a.hasArrow, hasArrow = _b === void 0 ? true : _b, _c = _a.isOpen, isAlwaysOpen = _c === void 0 ? false : _c, _d = _a.disableForHoverTouch, disableForHoverTouch = _d === void 0 ? false : _d, _e = _a.disableForFocusBlur, disableForFocusBlur = _e === void 0 ? false : _e, _f = _a.smoothTransition, smoothTransition = _f === void 0 ? false : _f, _g = _a.isDisabled, isDisabled = _g === void 0 ? false : _g, _h = _a.isMobile, isMobile = _h === void 0 ? false : _h, coords = _a.coords, _j = _a.keepMounted, keepMounted = _j === void 0 ? false : _j, _k = _a["aria-live"], ariaLive = _k === void 0 ? 'off' : _k, popperParams = __rest(_a, ["id", "className", "triggerClassName", "testId", "children", "label", "title", "hasArrow", "isOpen", "disableForHoverTouch", "disableForFocusBlur", "smoothTransition", "isDisabled", "isMobile", "coords", "keepMounted", 'aria-live']);
    var virtualElement = React__namespace.useRef({
        getBoundingClientRect: generateGetBoundingClientRect(),
    });
    var triggerWrapperRef = React__namespace.useRef(null);
    var _l = useToggle(false), isOpen = _l[0], _m = _l[1], open = _m.open, close = _m.close;
    var _o = React__namespace.useState(false), shadowOpenValue = _o[0], setShadowOpenValue = _o[1];
    var interactionHandler = function (action) { return function () {
        if (action === 'in') {
            open();
            setShadowOpenValue(true);
        }
        if (action === 'out') {
            if (!keepMounted)
                close();
            setShadowOpenValue(false);
        }
    }; };
    // Obtain the event handlers props
    var _p = getTouchPointHandlers(interactionHandler, isMobile), touchEvents = _p.touchEvents, focusEvents = _p.focusEvents;
    // Create a unique id for each new tooltip (unless an `id` is provided)
    var tooltipId = getTestId(TooltipTest.prefix, id !== null && id !== void 0 ? id : (+Date.now()).toString());
    var _q = usePopover(popperParams), popperRef = _q.popperRef, getReferenceProps = _q.getReferenceProps, getArrowProps = _q.getArrowProps, getPopperProps = _q.getPopperProps, update = _q.update, targetRef = _q.targetRef;
    var ownPopperRefHandler = useAssignRef(popperRef, forwardedRef);
    // Tooltip should have only one child node is it exists
    var child = (children
        ? React__namespace.Children.only(children)
        : null);
    var touchEventsProps = !isAlwaysOpen && !disableForHoverTouch ? touchEvents : {};
    var focusEventsProps = !isAlwaysOpen && !disableForFocusBlur ? focusEvents : {};
    var handlersProps = __assign(__assign({}, touchEventsProps), focusEventsProps);
    var ariaProps = __assign({}, ((isAlwaysOpen || shadowOpenValue) && {
        // Silence the tooltip for screen readers when aria-live is "off".
        // This prevents duplicate announcements when the child trigger element
        // already has an aria-label.
        'aria-describedby': ariaLive === 'off' ? undefined : tooltipId,
    }));
    var triggerWrapperProps = getReferenceProps(__assign(__assign({}, handlersProps), ariaProps), triggerWrapperRef);
    React__namespace.useEffect(function () {
        if (coords)
            targetRef(virtualElement.current);
    }, []);
    // Close tooltip on ESC key press
    React__namespace.useEffect(function () {
        var handleKeyDown = function (e) {
            if (e.key === 'Escape') {
                interactionHandler('out')(e);
            }
        };
        document.addEventListener('keydown', handleKeyDown);
        return function () { return document.removeEventListener('keydown', handleKeyDown); };
    }, []);
    React__namespace.useEffect(function () {
        if ((coords === null || coords === void 0 ? void 0 : coords.x) && coords.y) {
            virtualElement.current.getBoundingClientRect = generateGetBoundingClientRect(coords.x, coords.y);
            update();
        }
    }, [coords === null || coords === void 0 ? void 0 : coords.x, coords === null || coords === void 0 ? void 0 : coords.y]);
    var isMountedButHidden = !isAlwaysOpen && keepMounted && !shadowOpenValue;
    var popperProps = getPopperProps({
        className: classNames('rck-tooltip', isMountedButHidden ? 'rck-tooltip--hidden' : '', className || '', getTooltipTransitionClass(smoothTransition)),
    }, ownPopperRefHandler);
    var arrowProps = getArrowProps();
    if ((!label && !title) || isDisabled) {
        return React__namespace.createElement(React__namespace.Fragment, null, children);
    }
    var shouldShowTooltip = isAlwaysOpen || isOpen;
    return (React__namespace.createElement(React__namespace.Fragment, null,
        React__namespace.createElement("div", __assign({ tabIndex: -1, className: classNames('rck-tooltip__trigger', triggerClassName) }, triggerWrapperProps), child),
        shouldShowTooltip && (React__namespace.createElement(AutoPortal, { id: id || 'tooltip-portal', className: "" },
            React__namespace.createElement("div", __assign({ role: "tooltip", id: tooltipId, "data-testid": getTestId(TooltipTest.prefix, testId), tabIndex: -1, "aria-live": ariaLive }, popperProps),
                getTooltipContent(label, title),
                hasArrow && (React__namespace.createElement("div", __assign({ className: "rck-tooltip__arrow" }, arrowProps))))))));
});
Tooltip.displayName = 'Tooltip';

var ToggleContainerTest = {
    prefix: 'ToggleContainer',
};
var ToggleContainer = function (_a) {
    var buttonOpenText = _a.buttonOpenText, buttonCloseText = _a.buttonCloseText, _b = _a.initiallyOpen, initiallyOpen = _b === void 0 ? false : _b, children = _a.children, className = _a.className, testId = _a.testId;
    var _c = React.useState(initiallyOpen), isOpen = _c[0], setIsOpen = _c[1];
    return (React__default["default"].createElement("div", { className: classNames('rck-toggle-container__wrapper', className || ''), "data-testid": getTestId(ToggleContainerTest.prefix, testId) },
        React__default["default"].createElement("div", { className: classNames('rck-toggle-container__container', isOpen
                ? 'rck-toggle-container__container--open'
                : 'rck-toggle-container__container--close') }, children),
        React__default["default"].createElement("div", null,
            React__default["default"].createElement(Button$1, { buttonType: exports.ButtonType.plain, onClick: function () { return setIsOpen(function (currentValue) { return !currentValue; }); }, size: exports.ButtonSize.medium, icon: React__default["default"].createElement(Icon$1, { type: isOpen ? IconType.angleUp : IconType.angleDown, color: exports.IconColor.secondary }), iconPosition: exports.ButtonIconPosition.right }, isOpen ? buttonCloseText : buttonOpenText))));
};
ToggleContainer.displayName = 'ToggleContainer';

exports.ShadowedCardTest = void 0;
(function (ShadowedCardTest) {
    ShadowedCardTest["prefix"] = "ShadowedCard";
})(exports.ShadowedCardTest || (exports.ShadowedCardTest = {}));
var ShadowedCard = function (_a) {
    var testId = _a.testId, _b = _a.shadow, shadow = _b === void 0 ? 100 : _b, _c = _a.children, children = _c === void 0 ? [] : _c;
    return (React__default["default"].createElement("div", { className: classNames('rck-shadowed-card', "rck-shadowed-card--shadow-".concat(shadow)), "data-testid": getTestId(exports.ShadowedCardTest.prefix, testId) }, children));
};
ShadowedCard.displayName = 'ShadowedCard';

exports.BoxSelectorItemTest = void 0;
(function (BoxSelectorItemTest) {
    BoxSelectorItemTest["prefix"] = "BoxSelectorItem";
})(exports.BoxSelectorItemTest || (exports.BoxSelectorItemTest = {}));
var BoxSelectorItem = function (_a) {
    var label = _a.label, icon = _a.icon, fieldGroupName = _a.fieldGroupName, value = _a.value, _b = _a.isSelected, isSelected = _b === void 0 ? false : _b, _c = _a.isDisabled, isDisabled = _c === void 0 ? false : _c, handleChange = _a.handleChange, testId = _a.testId;
    var _d = React.useState(isSelected && !isDisabled), selected = _d[0], setSelected = _d[1];
    var handleClick = function (event) {
        if (!isDisabled) {
            if (selected) {
                // If option is selected and we click on it again, force blur so that it
                // doesn't display the focus/hover styles.
                event.currentTarget.blur();
            }
            setSelected(!selected);
        }
    };
    return (React__default["default"].createElement("li", { className: "rck-box-selector__item" },
        React__default["default"].createElement("label", { className: classNames('rck-box-selector-item', selected && !isDisabled ? 'rck-box-selector-item--selected' : '', isDisabled ? 'rck-box-selector-item--disabled' : '') },
            React__default["default"].createElement(Icon$1, { type: icon, family: selected && !isDisabled ? exports.IconFamily.solid : exports.IconFamily.regular, className: "rck-box-selector-item__icon" }),
            React__default["default"].createElement("p", { className: "rck-box-selector-item__label" }, label),
            React__default["default"].createElement("input", { className: "rck-box-selector-item__checkbox", type: "checkbox", checked: selected && !isDisabled, disabled: isDisabled, name: fieldGroupName, value: value, onClick: function (event) { return handleClick(event); }, onChange: function (e) { return handleChange(e.target.value, e.target.checked); }, "data-testid": getTestId(exports.BoxSelectorItemTest.prefix, testId) }))));
};
BoxSelectorItem.displayName = 'BoxSelectorItem';

exports.BoxSelectorTest = void 0;
(function (BoxSelectorTest) {
    BoxSelectorTest["prefix"] = "BoxSelector";
})(exports.BoxSelectorTest || (exports.BoxSelectorTest = {}));
var BoxSelector = function (_a) {
    var testId = _a.testId, handleInputCheckedChange = _a.handleInputCheckedChange, fieldGroupName = _a.fieldGroupName, _b = _a.items, items = _b === void 0 ? [] : _b;
    return (React__default["default"].createElement("ul", { className: classNames('rck-box-selector'), "data-testid": getTestId(exports.BoxSelectorTest.prefix, testId) }, items.map(function (itemProps) { return (React__default["default"].createElement(BoxSelectorItem, __assign({}, itemProps, { fieldGroupName: fieldGroupName, handleChange: handleInputCheckedChange }))); })));
};
BoxSelector.displayName = 'BoxSelector';

exports.OpenerTest = void 0;
(function (OpenerTest) {
    OpenerTest["prefix"] = "Opener";
})(exports.OpenerTest || (exports.OpenerTest = {}));
var Opener = function (_a) {
    var type = _a.type, open = _a.open, testId = _a.testId, _b = _a.color, color = _b === void 0 ? exports.IconColor.secondary : _b;
    var iconMap = {
        chevron: {
            open: IconType.angleUp,
            close: IconType.angleUp,
            animation: 'rotate',
        },
        more: {
            open: IconType.minus,
            close: IconType.plus,
            animation: null,
        },
    };
    var iconInfo = iconMap[type];
    return (React__namespace.createElement(Icon$1, { type: open
            ? iconInfo.open
            : iconInfo.close, color: color, className: classNames('rck-opener', 'rck-opener--animation', "rck-opener--animation-".concat(iconInfo.animation).concat(open ? '-opened' : '')), testId: getTestId(exports.OpenerTest.prefix, testId) }));
};

exports.ExpandableTest = void 0;
(function (ExpandableTest) {
    ExpandableTest["prefix"] = "Expandable";
    ExpandableTest["opener"] = "Opener";
    ExpandableTest["content"] = "Content";
})(exports.ExpandableTest || (exports.ExpandableTest = {}));
var ExpandableBase = function (_a) {
    var children = _a.children, className = _a.className, leftContent = _a.leftContent, rightContent = _a.rightContent, _b = _a.initialOpenState, initialOpenState = _b === void 0 ? false : _b, _c = _a.isDisabled, isDisabled = _c === void 0 ? false : _c, testId = _a.testId, _d = _a.openerType, openerType = _d === void 0 ? 'chevron' : _d, openerColor = _a.openerColor, onClick = _a.onClick;
    var _e = React__namespace.useState(initialOpenState), open = _e[0], setOpen = _e[1];
    var expand = function () {
        if (isDisabled)
            return;
        setOpen(function (isOpen) { return !isOpen; });
        if (onClick) {
            onClick();
        }
    };
    return (React__namespace.createElement("div", { className: classNames('rck-expandable', isDisabled ? 'rck-expandable--disabled' : '', className), "data-testId": getTestId(exports.ExpandableTest.prefix, testId) },
        React__namespace.createElement("div", { className: classNames('rck-expandable__header', isDisabled ? 'rck-expandable__header--disabled' : ''), onClick: expand, role: "button", onKeyDown: expand, tabIndex: 0, "aria-expanded": open, "aria-disabled": isDisabled },
            React__namespace.createElement("span", { className: "rck-expandable__header--left-content" }, leftContent),
            React__namespace.createElement("span", { className: "rck-expandable__header--right-content" }, rightContent),
            React__namespace.createElement("span", { className: "rck-expandable__header--open-icon", "data-testId": getTestId(exports.ExpandableTest.prefix, testId, exports.ExpandableTest.opener) },
                React__namespace.createElement(Opener, { type: openerType, open: open, color: openerColor }))),
        React__namespace.createElement("div", { className: classNames('rck-expandable__container', open && 'rck-expandable__container--open'), "data-testId": getTestId(exports.ExpandableTest.prefix, testId, exports.ExpandableTest.content) }, children)));
};

var ExpandCollapse = function (_a) {
    var leftIcon = _a.leftIcon, leftImgSrc = _a.leftImgSrc, title = _a.title, rightText = _a.rightText, initialOpenState = _a.initialOpenState, isDisabled = _a.isDisabled, testId = _a.testId, openerType = _a.openerType, children = _a.children, className = _a.className, rightElement = _a.rightElement, onClick = _a.onClick;
    var leftContent = function () { return (React__namespace.createElement("div", { className: "rck-expand-collapse__header-right" },
        React__namespace.createElement("span", { className: "rck-expand-collapse__header-right-icon" },
            leftIcon && (React__namespace.createElement(Icon$1, { type: leftIcon, className: "rck-expand-collapse__header-icon", circle: true, color: isDisabled ? exports.IconColor.grayLight : exports.IconColor.secondary })),
            leftImgSrc && (React__namespace.createElement("img", { src: leftImgSrc, alt: title, className: classNames('rck-expand-collapse__header-image', isDisabled ? 'rck-expand-collapse__header-image--disabled' : '') }))),
        React__namespace.createElement(Text, { variant: "body-2-regular" }, title))); };
    var rightContent = function () {
        if (rightElement)
            return rightElement;
        if (rightText)
            return React__namespace.createElement(Text, { variant: "title-2-semibold-responsive" }, rightText);
        return undefined;
    };
    return (React__namespace.createElement(ExpandableBase, { leftContent: leftContent(), rightContent: rightContent(), className: className, initialOpenState: initialOpenState, isDisabled: isDisabled, openerType: openerType, openerColor: isDisabled ? exports.IconColor.grayLight : exports.IconColor.secondary, testId: testId, onClick: onClick }, children));
};

exports.ExpandableRowItemTest = void 0;
(function (ExpandableRowItemTest) {
    ExpandableRowItemTest["prefix"] = "Expandable-row-item";
    ExpandableRowItemTest["title"] = "Title";
    ExpandableRowItemTest["rightElement"] = "Right-element";
})(exports.ExpandableRowItemTest || (exports.ExpandableRowItemTest = {}));
var ExpandCollapseRowItem = function (_a) {
    var iconSrc = _a.iconSrc, title = _a.title, subtitle = _a.subtitle, rightElement = _a.rightElement, className = _a.className, testId = _a.testId, isDisabled = _a.isDisabled, onInfoActionClick = _a.onInfoActionClick;
    var leftSide = (React__namespace.createElement("div", { className: "rck-expand-collapse-row-item__left-element" },
        iconSrc && (React__namespace.createElement("img", { src: iconSrc, alt: title, className: classNames('rck-expand-collapse-row-item__icon', isDisabled ? 'rck-expand-collapse-row-item__icon--disabled' : '') })),
        React__namespace.createElement("div", { className: "rck-expand-collapse-row-item__title", "data-testId": getTestId(exports.ExpandableRowItemTest.prefix, testId, exports.ExpandableRowItemTest.title) },
            React__namespace.createElement(FlexLayout, { mainaxis: "row" },
                React__namespace.createElement(Text, { variant: "body-1-semibold" }, title),
                onInfoActionClick ? (React__namespace.createElement(Icon$1, { type: IconType.infoCircle, color: isDisabled ? exports.IconColor.disabled : exports.IconColor.secondary, className: classNames('rck-expand-collapse-row-item__action-icon', isDisabled
                        ? 'rck-expand-collapse-row-item__action-icon--disabled'
                        : '') })) : null),
            subtitle && (React__namespace.createElement("span", { className: "rck-expand-collapse-row-item__subtitle" }, subtitle)))));
    var onClickInfo = function (e) {
        e.stopPropagation();
        if (isDisabled || !onInfoActionClick)
            return;
        onInfoActionClick();
    };
    return (React__namespace.createElement("div", { className: classNames('rck-expand-collapse-row-item', className), "data-testId": getTestId(exports.ExpandableRowItemTest.prefix, testId) },
        onInfoActionClick && !isDisabled ? (React__namespace.createElement(TransparentButton$1, { onClick: onClickInfo, className: "rck-expand-collapse-row-item__action-button" }, leftSide)) : (leftSide),
        React__namespace.createElement("div", { className: "rck-expand-collapse-row-item__right-element", "data-testId": getTestId(exports.ExpandableRowItemTest.prefix, testId, exports.ExpandableRowItemTest.rightElement) }, rightElement)));
};

exports.ExpandableTextTest = void 0;
(function (ExpandableTextTest) {
    ExpandableTextTest["prefix"] = "Expandable-text";
    ExpandableTextTest["content"] = "Content";
})(exports.ExpandableTextTest || (exports.ExpandableTextTest = {}));
var ExpandableText = function (_a) {
    var children = _a.children, _b = _a.maxLines, maxLines = _b === void 0 ? 3 : _b, expandText = _a.expandText, collapseText = _a.collapseText, className = _a.className, testId = _a.testId, isTextExpanded = _a.isTextExpanded, onToggle = _a.onToggle;
    var _c = React.useState(isTextExpanded !== null && isTextExpanded !== void 0 ? isTextExpanded : false), isExpanded = _c[0], setIsExpanded = _c[1];
    var _d = React.useState(false), showButton = _d[0], setShowButton = _d[1];
    var textRef = React.useRef(null);
    /**
     * Check if the text overflows the container
     */
    var checkOverflow = function () {
        if (textRef.current) {
            // Get the lineHeight from the text element
            var lineHeight = Number(window.getComputedStyle(textRef.current).lineHeight.replace('px', ''));
            var maxHeight = lineHeight * maxLines;
            // check if the text overflows the container
            if (textRef.current.scrollHeight > maxHeight) {
                setShowButton(true);
            }
            else {
                setShowButton(false);
            }
        }
    };
    React.useEffect(function () {
        checkOverflow();
        var resizeObserver = new ResizeObserver(function () {
            checkOverflow();
        });
        if (textRef.current) {
            resizeObserver.observe(textRef.current);
        }
        return function () {
            if (textRef.current) {
                resizeObserver.unobserve(textRef.current);
            }
        };
    }, [maxLines, children]);
    var getIsExpanded = function () {
        if (isTextExpanded !== undefined) {
            return isTextExpanded;
        }
        return isExpanded;
    };
    var toggleExpand = function () {
        setIsExpanded(!isExpanded);
    };
    return (React__default["default"].createElement("div", { className: classNames('expandable-text', className), "data-testid": getTestId(exports.ExpandableTextTest.prefix, testId) },
        React__default["default"].createElement("div", { ref: textRef, className: classNames('expandable-text__body', "expandable-text--".concat(getIsExpanded() ? 'expanded' : 'collapsed')), style: {
                '--max-lines': maxLines,
            }, "data-testid": getTestId(exports.ExpandableTextTest.prefix, testId, exports.ExpandableTextTest.content), "aria-expanded": getIsExpanded() }, children),
        showButton && (React__default["default"].createElement(Button$1, { onClick: onToggle !== null && onToggle !== void 0 ? onToggle : toggleExpand, buttonType: exports.ButtonType.plain, block: true, size: exports.ButtonSize.medium, centered: true, className: "expandable-text__expand-button" }, getIsExpanded() ? collapseText : expandText))));
};

var prepareConversation = function (conversation, contacts) {
    var contactsMap = contacts.reduce(function (acc, curr) {
        var _a;
        return (__assign(__assign({}, acc), (_a = {}, _a[curr.id] = curr, _a)));
    }, {});
    return conversation.map(function (_a) {
        var date = _a.date, messages = _a.messages;
        return ({
            date: date,
            threads: messages.reduce(function (acc, curr) {
                var lastGroup = acc[acc.length - 1];
                if (lastGroup && lastGroup.contact.id === curr.contactId) {
                    lastGroup.messages.push(curr);
                }
                else {
                    acc.push({
                        id: "".concat(curr.contactId, "-").concat(acc.length),
                        contact: contactsMap[curr.contactId],
                        messages: [curr],
                        outgoing: curr.outgoing,
                    });
                }
                return acc;
            }, []),
        });
    });
};

var TextBubble = function (_a) {
    var message = _a.message;
    if (typeof message.content !== 'string')
        return null;
    return React__default["default"].createElement("div", { className: "rck-conversation__bubble" }, message.content);
};
var ImageBubble = function (_a) {
    var message = _a.message;
    if (typeof message.content !== 'string')
        return null;
    return (React__default["default"].createElement("div", { className: "rck-conversation__bubble rck-conversation__bubble--image" },
        React__default["default"].createElement("img", { src: message.content, alt: message.content })));
};
var MixedContent = function (_a) {
    var message = _a.message;
    if (typeof message.content === 'string')
        return null;
    return React__default["default"].createElement(React__default["default"].Fragment, null, message.content(message));
};
var MessageEntry = function (_a) {
    var message = _a.message;
    if (message.contentType === 'image')
        return React__default["default"].createElement(ImageBubble, { message: message });
    if (message.contentType === 'mixed')
        return React__default["default"].createElement(MixedContent, { message: message });
    return React__default["default"].createElement(TextBubble, { message: message });
};
var HighlightIcon = function (_a) {
    var highlight = _a.highlight;
    if (highlight === 'warning') {
        return (React__default["default"].createElement(Icon$1, { type: IconType.exclamationCircle, color: exports.IconColor.warning, family: exports.IconFamily.solid }));
    }
    if (highlight === 'error') {
        return React__default["default"].createElement(Icon$1, { type: IconType.exclamationCircle, color: exports.IconColor.error });
    }
    return null;
};
var ThreadEntry = function (_a) {
    var highlight = _a.highlight, children = _a.children;
    return (React__default["default"].createElement("div", { className: classNames('rck-conversation__entry', highlight ? "rck-conversation__entry--highlight-".concat(highlight) : '') }, children));
};
var ContactEntry = function (_a) {
    var contact = _a.contact;
    return (React__default["default"].createElement("div", { className: "rck-conversation__contact" },
        contact.pictureUrl ? (React__default["default"].createElement(Avatar, { className: "rck-conversation__contact-avatar", size: exports.AvatarSize.extraExtraSmall },
            React__default["default"].createElement("img", { src: contact.pictureUrl, alt: contact.name }))) : null,
        React__default["default"].createElement("div", { className: "rck-conversation__contact-name" }, contact.name)));
};
var DateIndicator = function (_a) {
    var date = _a.date;
    return (React__default["default"].createElement("div", { className: "rck-conversation__date-indicator" },
        React__default["default"].createElement("div", null,
            React__default["default"].createElement(Tag, { text: date, type: exports.GlobalType.gray, invertColor: true }))));
};

var ConversationTest = {
    prefix: 'Conversation',
};
var MessagesThread = function (_a) {
    var thread = _a.thread;
    return (React__default["default"].createElement("div", { key: thread.id, className: classNames('rck-conversation__thread', thread.outgoing
            ? 'rck-conversation__outgoing'
            : 'rck-conversation__incoming') },
        React__default["default"].createElement(ContactEntry, { contact: thread.contact }),
        React__default["default"].createElement("div", { className: "rck-conversation__entries" }, thread.messages.map(function (message) { return (React__default["default"].createElement(ThreadEntry, { key: message.id, highlight: message.highlight },
            React__default["default"].createElement(MessageEntry, { message: message }),
            React__default["default"].createElement(HighlightIcon, { highlight: message.highlight }))); }))));
};
var Conversation = function (_a) {
    var conversation = _a.conversation, contacts = _a.contacts, _b = _a.className, className = _b === void 0 ? '' : _b, testId = _a.testId;
    var datedThreads = prepareConversation(conversation, contacts);
    return (React__default["default"].createElement("div", { className: classNames('rck-conversation', className), "data-testid": getTestId(ConversationTest.prefix, testId), 
        // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
        tabIndex: 0 }, datedThreads.map(function (datedThread) { return (React__default["default"].createElement(React__default["default"].Fragment, null,
        datedThread.date && React__default["default"].createElement(DateIndicator, { date: datedThread.date }),
        datedThread.threads.map(function (thread) { return (React__default["default"].createElement(MessagesThread, { key: thread.id, thread: thread })); }))); })));
};

exports.IconWithBadgeTest = void 0;
(function (IconWithBadgeTest) {
    IconWithBadgeTest["prefix"] = "IconWithBadge";
})(exports.IconWithBadgeTest || (exports.IconWithBadgeTest = {}));
var IconWithBadge = function (_a) {
    var icon = _a.icon, showBadge = _a.showBadge, _b = _a.badgeColor, badgeColor = _b === void 0 ? exports.IconColor.secondary : _b, _c = _a.badgePosition, badgePosition = _c === void 0 ? 'left' : _c, containerClassName = _a.containerClassName, badgeClassname = _a.badgeClassname, iconClassname = _a.iconClassname, testId = _a.testId;
    return (React__namespace.createElement("div", { className: classNames('rck-icon-with-badge__container', containerClassName), "data-testid": getTestId(exports.IconWithBadgeTest.prefix, testId) },
        React__namespace.createElement(IconAdapter$1, { icon: icon, className: classNames('rck-icon-with-badge__icon', iconClassname) }),
        React__namespace.createElement("div", { className: classNames('rck-icon-with-badge__badge', badgePosition === 'right'
                ? 'rck-icon-with-badge__badge--right'
                : 'rck-icon-with-badge__badge--left', badgeClassname) }, showBadge && (React__namespace.createElement(Icon$1, { type: IconType.circle, family: exports.IconFamily.solid, color: badgeColor, className: classNames('rck-icon-with-badge__badge-icon'), testId: "new-item-list", circle: true })))));
};
IconWithBadge.displayName = 'IconWithBadge';

exports.Accordion = Accordion;
exports.AccordionItem = AccordionItem;
exports.AccordionMenu = AccordionMenu;
exports.AccountInfoHeader = AccountInfoHeader$1;
exports.ActionInput = ActionInput$1;
exports.ActionableContent = ActionableContent;
exports.AffiliateBadge = AffiliateBadge$1;
exports.AlertBox = AlertBox$1;
exports.AutoPortal = AutoPortal;
exports.Avatar = Avatar;
exports.Badge = Badge$1;
exports.Banner = Banner$1;
exports.BoxSelector = BoxSelector;
exports.BoxSelectorItem = BoxSelectorItem;
exports.BrandsHeader = BrandsHeader;
exports.Button = Button$1;
exports.Card = Card;
exports.CardActionTitle = CardActionTitle$1;
exports.Checkbox = Checkbox;
exports.ClickableListItem = ClickableListItem;
exports.CloudList = CloudList;
exports.ColorPicker = ColorPicker;
exports.ColorPickerTest = ColorPickerTest$1;
exports.CompactStyleListItem = CompactStyleListItem;
exports.ComparisonTable = ComparisonTable;
exports.ContentSeparator = ContentSeparator$1;
exports.Conversation = Conversation;
exports.CopyBox = CopyBox;
exports.CustomStyleListItem = CustomStyleListItem;
exports.DeactivableContent = DeactivableContent;
exports.DomainTrustSeal = DomainTrustSeal$1;
exports.Drawer = Drawer;
exports.Dropdown = Dropdown;
exports.DropdownFooter = DropdownFooter;
exports.DropdownHeader = DropdownHeader;
exports.DropdownOption = DropdownOption;
exports.DurationInput = DurationInput;
exports.EmptyPlaceholder = EmptyPlaceholder$1;
exports.ExpandCollapse = ExpandCollapse;
exports.ExpandCollapseRowItem = ExpandCollapseRowItem;
exports.ExpandableBase = ExpandableBase;
exports.ExpandableCard = ExpandableCard;
exports.ExpandableText = ExpandableText;
exports.Filter = Filter;
exports.FilterGroup = FilterGroup;
exports.FilterGroupTest = FilterGroupTest$1;
exports.FilterTest = FilterTest$1;
exports.FixedCtaCard = FixedCtaCard;
exports.FlexLayout = FlexLayout;
exports.FreeStyleListItem = FreeStyleListItem;
exports.FreeText = FreeText;
exports.GroupHeader = GroupHeader$1;
exports.Icon = Icon$1;
exports.IconAdapter = IconAdapter$1;
exports.IconSize = IconSize;
exports.IconType = IconType;
exports.IconWithBadge = IconWithBadge;
exports.InstructionBlock = InstructionBlock$1;
exports.InstructionCard = InstructionCard;
exports.Label = Label$1;
exports.Layout = Layout;
exports.LicenseInfoContainer = LicenseInfoContainer;
exports.Link = Link$1;
exports.List = List;
exports.ListBckImageIcon = ListBckImageIcon$1;
exports.ListColoredText = ListColoredText$1;
exports.ListCompactRightSubtitle = ListCompactRightSubtitle$1;
exports.ListCompactRightTitle = ListCompactRightTitle$1;
exports.ListCompactSubtitle = ListCompactSubtitle$1;
exports.ListCompactTitle = ListCompactTitle$1;
exports.ListLink = ListLink$1;
exports.ListSecondaryText = ListSecondaryText$1;
exports.ListSubtitle = ListSubtitle$1;
exports.ListThumbnail = ListThumbnail$1;
exports.ListTitle = ListTitle$1;
exports.LocationStyleListItem = LocationStyleListItem;
exports.MarketingBanner = MarketingBanner;
exports.Menu = Menu;
exports.MenuItem = MenuItem$1;
exports.Modal = Modal;
exports.ModalInfo = ModalInfo;
exports.ModalStyledText = ModalStyledText;
exports.MoreActionsButton = MoreActionsButton$1;
exports.MultiStep = MultiStep;
exports.NoContextDrawer = NoContextDrawer;
exports.Opener = Opener;
exports.PageTitle = PageTitle;
exports.PlanBox = PlanBox;
exports.PlanBoxExtended = PlanBoxExtended;
exports.Portal = Portal;
exports.PortalModal = PortalModal;
exports.ProfileInfoHeader = ProfileInfoHeader$1;
exports.Quote = Quote$1;
exports.RadioButton = RadioButton;
exports.RegularStyleListItem = RegularStyleListItem;
exports.Ribbon = Ribbon;
exports.SafeArea = SafeArea;
exports.ScrollableContent = ScrollableContent;
exports.SelectableListItem = SelectableListItem;
exports.SelectionBar = SelectionBar;
exports.ShadowedCard = ShadowedCard;
exports.SideNavigationMenu = SideNavigationMenu;
exports.Slider = Slider;
exports.Spinner = Spinner;
exports.StepBar = StepBar;
exports.StyledHeader = Header;
exports.StyledInstruction = StyledInstruction$1;
exports.Svg = Svg$1;
exports.Switch = Switch;
exports.TabPanel = TabPanel;
exports.Table = Table;
exports.TableBody = TableBody;
exports.TableCell = TableCell;
exports.TableHead = TableHead;
exports.TableRow = TableRow;
exports.Tabs = Tabs;
exports.Tag = Tag;
exports.TagTest = TagTest$1;
exports.Text = Text;
exports.TextArea = TextArea;
exports.TextField = TextField;
exports.TextFieldAdapter = TextFieldAdapter;
exports.TextTest = TextTest$1;
exports.TimePickerInput = TimePickerInput$1;
exports.Toast = Toast;
exports.ToastManager = ToastManager;
exports.ToggleContainer = ToggleContainer;
exports.Tooltip = Tooltip;
exports.TransparentButton = TransparentButton$1;
exports.makeClickableListItem = makeClickableListItem;
exports.theme = theme;
exports.useToggle = useToggle;
//# sourceMappingURL=index.js.map
