import React from 'react';
import { FreeStyleListItemProps } from '../types';
export declare enum FreeStyleListItemTest {
    prefix = "ListItem"
}
declare const FreeStyleListItem: React.FunctionComponent<FreeStyleListItemProps>;
export default FreeStyleListItem;
