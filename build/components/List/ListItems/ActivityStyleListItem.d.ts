import React from 'react';
import { ActivityStyleListItemProps } from '../types';
export declare enum ActivityStyleListItemTest {
    prefix = "ListItem",
    actionElement = "Action-element"
}
declare const ActivityStyleListItem: React.FunctionComponent<ActivityStyleListItemProps>;
export default ActivityStyleListItem;
