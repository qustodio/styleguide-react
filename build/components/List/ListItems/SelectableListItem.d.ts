import * as React from 'react';
import { SelectableListItemProps } from '../types';
export declare enum SelectableListItemTest {
    prefix = "ListItem"
}
declare const SelectableListItem: React.FunctionComponent<SelectableListItemProps>;
export default SelectableListItem;
