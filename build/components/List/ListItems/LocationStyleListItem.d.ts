import React from 'react';
import { LocationListItemProps } from '../types';
export declare enum LocationStyleListItemTest {
    prefix = "ListItem"
}
declare const LocationStyleListItem: React.FunctionComponent<LocationListItemProps>;
export default LocationStyleListItem;
