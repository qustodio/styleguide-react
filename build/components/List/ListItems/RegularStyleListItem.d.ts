import React from 'react';
import { RegularStyleListItemProps } from '../types';
export declare enum RegularStyleListItemTest {
    prefix = "ListItem",
    actionElement = "Action-element"
}
declare const RegularStyleListItem: React.FunctionComponent<RegularStyleListItemProps>;
export default RegularStyleListItem;
