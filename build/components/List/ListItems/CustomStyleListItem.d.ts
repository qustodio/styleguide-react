import React from 'react';
import { CustomListItemProps } from '../types';
export declare enum CustomStyleListItemTest {
    prefix = "ListItem"
}
declare const CustomStyleListItem: React.FunctionComponent<CustomListItemProps>;
export default CustomStyleListItem;
