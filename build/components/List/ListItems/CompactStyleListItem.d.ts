import React from 'react';
import { CompactListItemProps } from '../types';
export declare enum CompactStyleListItemTest {
    prefix = "ListItem",
    title = "Title-content",
    rightTitle = "Title-right-content",
    subtitle = "Subtitle-content",
    rightSubtitle = "Subtitle-right-content"
}
declare const CompactStyleListItem: React.FunctionComponent<CompactListItemProps>;
export default CompactStyleListItem;
