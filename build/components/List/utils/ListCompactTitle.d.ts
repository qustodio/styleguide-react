import React from 'react';
import { BaseComponentProps } from '../../../common/types';
export declare enum ListCompactTitleTest {
    prefix = "ListCompactTitle"
}
declare const _default: React.NamedExoticComponent<BaseComponentProps>;
export default _default;
