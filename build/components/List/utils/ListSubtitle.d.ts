import React from 'react';
import { ListSubtitleProps } from '../types';
export declare enum ListSubtitleTest {
    prefix = "ListSubtitle"
}
declare const _default: React.NamedExoticComponent<ListSubtitleProps>;
export default _default;
