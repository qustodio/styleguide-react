import React from 'react';
import { DeactivableListItemProps } from '../types';
export declare enum DeactivableListItemTest {
    prefix = "DeactivableListItem"
}
declare const DeactivableListItem: React.FunctionComponent<DeactivableListItemProps>;
export default DeactivableListItem;
