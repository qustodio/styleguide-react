import React from 'react';
import { ListSecondaryTextProps } from '../types';
export declare enum ListSecondaryTextTest {
    prefix = "ListSecondaryText"
}
declare const _default: React.NamedExoticComponent<ListSecondaryTextProps>;
export default _default;
