import React from 'react';
import { ListBckImageIconProps } from '../types';
export declare enum ListBckImageIconTest {
    prefix = "ListItemBrandIcon"
}
declare const _default: React.NamedExoticComponent<ListBckImageIconProps>;
export default _default;
