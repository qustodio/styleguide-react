import React from 'react';
import { ListLinkProps } from '../types';
export declare enum ListLinkTest {
    prefix = "ListLink"
}
declare const _default: React.NamedExoticComponent<ListLinkProps>;
export default _default;
