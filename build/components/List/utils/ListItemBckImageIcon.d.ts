import React from 'react';
import { ListItemBckImageIconProps } from '../types';
export declare enum ListItemBckImageIconTest {
    prefix = "ListItemBrandIcon"
}
declare const ListItemBckImageIcon: React.FunctionComponent<ListItemBckImageIconProps>;
export default ListItemBckImageIcon;
