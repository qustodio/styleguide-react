import React from 'react';
import { ListDeactivableContentProps } from '../types';
export declare enum ListDeactivableContentTest {
    prefix = "DeactivableListItem"
}
declare const ListDeactivableContent: React.FunctionComponent<ListDeactivableContentProps>;
export default ListDeactivableContent;
