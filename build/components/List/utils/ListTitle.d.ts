import React from 'react';
import { ListTitleProps } from '../types';
export declare enum ListTitleTest {
    prefix = "ListTitle"
}
declare const _default: React.NamedExoticComponent<ListTitleProps>;
export default _default;
