import React from 'react';
import { ListColoredTextProps } from '../types';
export declare enum ListColoredTextTest {
    prefix = "ListColoredText"
}
declare const _default: React.NamedExoticComponent<ListColoredTextProps>;
export default _default;
