import React from 'react';
import { ListThumbnailProps } from '../types';
export declare enum ListThumbnailTest {
    prefix = "ListThumbnail"
}
declare const _default: React.NamedExoticComponent<ListThumbnailProps>;
export default _default;
