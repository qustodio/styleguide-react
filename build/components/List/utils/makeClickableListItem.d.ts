import React from 'react';
import { ClickableListItemProps, ListLinkProps, ListSubtitleProps, ListTitleProps } from '../types';
type ValidItemListProps = ListLinkProps | ListSubtitleProps | ListTitleProps;
type ClickableListItemPropsExtended = ClickableListItemProps & {
    clickableClassName: string;
};
declare const makeClickableListItem: <P extends ValidItemListProps>(Component: React.ComponentType<P>) => React.FunctionComponent<P & ClickableListItemProps & {
    clickableClassName: string;
}>;
export default makeClickableListItem;
