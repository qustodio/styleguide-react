import React from 'react';
import { ClickableListItemProps } from '../types';
export declare enum ClickableListItemTest {
    prefix = "ClickableListItemTest"
}
declare const ClickableListItem: React.FunctionComponent<ClickableListItemProps>;
export default ClickableListItem;
