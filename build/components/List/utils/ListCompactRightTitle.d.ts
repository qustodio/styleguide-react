import React from 'react';
import { BaseComponentProps } from '../../../common/types';
export declare enum ListCompactRightTitleTest {
    prefix = "ListCompactRightTitle"
}
declare const _default: React.NamedExoticComponent<BaseComponentProps>;
export default _default;
