import * as React from 'react';
import { ListProps } from './types';
export declare enum ListTest {
    prefix = "List"
}
declare const List: React.FunctionComponent<ListProps>;
export default List;
