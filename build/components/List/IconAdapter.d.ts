import * as React from 'react';
export interface IconAdapterProps {
    icon: string | JSX.Element;
    className?: string;
    altText?: string;
}
declare const _default: React.NamedExoticComponent<IconAdapterProps>;
export default _default;
