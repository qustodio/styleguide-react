import * as React from 'react';
import { CloudListProps } from './types';
export declare enum CloudListTest {
    prefix = "CloudList"
}
declare const CloudList: React.FunctionComponent<CloudListProps>;
export default CloudList;
