import { ReactElement } from 'react';
import { RckColor } from '../../common/color.types';
import { RckBaseGrandient } from '../../common/gradient.types';
import { TagProps } from '../Tag';
import { ButtonProps } from '../Button/Button';
export type MarketingBannerProps = {
    backgroundType?: 'plain' | 'gradient';
    backgroundColor: RckColor | RckBaseGrandient;
    backgroundShapes?: boolean;
    imageSrc?: string;
    tag?: ReactElement<TagProps>;
    title: string;
    description?: string;
    button: ReactElement<ButtonProps>;
    showCloseIcon?: boolean;
    closeIconColor?: RckColor;
    alignment?: 'center' | 'left';
    onClose?: () => void;
};
declare const MarketingBanner: {
    ({ backgroundType, backgroundColor, backgroundShapes, imageSrc, tag, title, description, button, showCloseIcon, closeIconColor, alignment, onClose, }: MarketingBannerProps): JSX.Element;
    displayName: string;
};
export default MarketingBanner;
