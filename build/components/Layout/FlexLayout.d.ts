import * as React from 'react';
import { FlexLayoutProps } from './types';
export declare enum FlexLayoutTest {
    prefix = "FlexLayout"
}
declare const FlexLayout: React.FunctionComponent<FlexLayoutProps>;
export default FlexLayout;
