import { LayoutWrappingProps } from '../types';
declare const layoutWrappingClassNames: ({ noWrap }: LayoutWrappingProps) => string;
export default layoutWrappingClassNames;
