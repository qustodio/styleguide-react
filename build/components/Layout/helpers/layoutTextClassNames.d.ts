import memoize from 'memoizee';
import { LayoutTextProps } from '../types';
declare const _default: (({ lineHeight, noWrap, }: LayoutTextProps) => string) & memoize.Memoized<({ lineHeight, noWrap, }: LayoutTextProps) => string>;
export default _default;
