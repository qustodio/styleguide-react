import memoize from 'memoizee';
import { LayoutSpacingProps } from '../types';
declare const _default: (({ marginLeft, marginBottom, marginTop, marginRight, paddingLeft, paddingRight, paddingTop, paddingBottom, padding, margin, minHeight, maxHeight, minWidth, maxWidth, height, width, }: LayoutSpacingProps) => string) & memoize.Memoized<({ marginLeft, marginBottom, marginTop, marginRight, paddingLeft, paddingRight, paddingTop, paddingBottom, padding, margin, minHeight, maxHeight, minWidth, maxWidth, height, width, }: LayoutSpacingProps) => string>;
export default _default;
