import memoize from 'memoizee';
import { FlexItemProps } from '../types';
declare const _default: (({ order, flexBasis, flexGrow, flexShrink, alignSelf, gap, }: FlexItemProps) => string) & memoize.Memoized<({ order, flexBasis, flexGrow, flexShrink, alignSelf, gap, }: FlexItemProps) => string>;
export default _default;
