import * as React from 'react';
import { FlexLayoutItemProps } from './types';
export declare enum FlexLayoutItemTest {
    prefix = "FlexLayoutItem"
}
declare const FlexLayoutItem: React.FunctionComponent<FlexLayoutItemProps>;
export default FlexLayoutItem;
