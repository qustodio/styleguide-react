import * as React from 'react';
import { LayoutProps } from './types';
export declare enum LayoutTest {
    prefix = "Layout"
}
declare const Layout: React.FunctionComponent<LayoutProps>;
export default Layout;
