import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { ProfileInfoHeaderProps } from '../AccountInfoHeader/ProfileInfoHeader';
import { IconColor } from '../Icons';
export type PageTitleDefaultActionNames = 'back' | 'edit' | 'delete' | 'search' | 'settings' | 'menu';
export type PageTitleSize = 'regular' | 'small' | 'medium';
export type PageTitleActionName = PageTitleDefaultActionNames | string | 'none';
export interface PageTitleActionElement {
    actionName: PageTitleActionName;
    icon?: JSX.Element;
    iconColor?: IconColor;
    location?: string;
    size?: PageTitleSize;
    onClick?: () => void;
}
export interface PageTitleProps extends BaseComponentProps {
    title?: string;
    subtitle?: string | JSX.Element;
    subtitleEllipsis?: boolean;
    centered?: boolean;
    profileInfoHeader?: React.ReactElement<ProfileInfoHeaderProps>;
    size: PageTitleSize;
    actions?: PageTitleActionElement[];
}
export declare enum PageTitleTest {
    prefix = "PageTitle"
}
declare const PageTitle: React.FunctionComponent<PageTitleProps>;
export default PageTitle;
