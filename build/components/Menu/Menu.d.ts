import * as React from 'react';
import { MenuItemTypeProps } from './MenuItem';
import { BaseComponentProps } from '../../common/types';
import { ProfileInfoHeaderProps } from '../AccountInfoHeader/ProfileInfoHeader';
export interface MenuProps extends BaseComponentProps {
    items: MenuItemTypeProps[];
    selectedRule: string | undefined;
    showBadge?: boolean;
    isDataReady?: boolean;
    scrollable?: boolean;
    onClick: (ruleKey: string) => void;
    onClickAccordionMenu?: (newAccordionMenuVisibility: boolean) => void;
    accordionMenuIsVisible?: boolean;
    expanded?: boolean;
    profileInfoHeader?: React.ReactElement<ProfileInfoHeaderProps>;
}
export declare enum MenuTest {
    prefix = "Menu"
}
declare const Menu: React.FunctionComponent<MenuProps>;
export default Menu;
