import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface MenuItemTypeProps {
    key: string;
    translation?: string;
    content?: JSX.Element;
    isPremium: boolean;
    needsData?: boolean;
    children?: MenuItemTypeProps[] | undefined;
    showContent?: boolean;
}
export interface MenuItemProps extends BaseComponentProps {
    children?: JSX.Element;
    menuItem: MenuItemTypeProps;
    isHeader?: boolean;
    showBadge?: boolean;
    isLoading?: boolean;
    isActive?: boolean;
    expanded?: boolean;
    onClick?: () => void;
}
export declare enum MenuItemTest {
    prefix = "MenuItem",
    actionElement = "Action-link"
}
export declare const MenuItemContent: React.FunctionComponent<MenuItemProps>;
declare const _default: React.NamedExoticComponent<MenuItemProps>;
export default _default;
