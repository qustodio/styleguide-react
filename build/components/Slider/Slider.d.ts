import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export interface SliderProps extends BaseComponentProps, DisabledProps {
    id: string;
    name?: string;
    block?: boolean;
    minValue: number;
    maxValue: number;
    value: number;
    formatLabel: string;
    onInput?: (ev: React.SyntheticEvent, value: string) => void;
    onChange?: (ev: React.SyntheticEvent, value: string) => void;
}
export declare enum SliderTest {
    prefix = "Slider"
}
declare const Slider: React.FunctionComponent<SliderProps>;
export default Slider;
