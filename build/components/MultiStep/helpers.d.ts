import { MultiStepState, Step, StepName } from './MultiStep.types';
export declare const unsafeInitialPosition: (steps: [Step, Step, ...Step[]], name?: StepName | null) => number;
export declare const next: (steps: [Step, Step, ...Step[]]) => ({ position, }: MultiStepState) => MultiStepState;
export declare const prev: ({ position }: MultiStepState) => MultiStepState;
export declare const go: (name: StepName, steps: [Step, Step, ...Step[]]) => ({ position, }: MultiStepState) => MultiStepState;
export declare const safeAsNone: (selectedStep: Step) => Step;
