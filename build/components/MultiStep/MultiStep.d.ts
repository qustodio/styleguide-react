import * as React from 'react';
import { MultiStepProps } from './MultiStep.types';
/**
 *
 * Renders one step at a time allowing transition to next, previous or a selected step.
 *
 * Behavior
 *
 * 1. Can define default step using activeStep with value as step name.
 * 1. When is in te last step and call next the multiStep does not change step.
 * 2. When is in the first step and call prev the multiStep does not change step.
 * 3. If go is used with the name of a step that does not exist the multiStep does not render anything (Render null).
 *
 * Important
 *
 * 1. To navigate between the different steps it is recommended to use go instead of next or prev so as not depends to order of the steps.
 * 2. MultiStep use step name prop to select active step.
 *
 *
 * @example
 *
 * ```tsx
 * <MultiStep
 *    activeStep="three"
 *    steps={[{
 *      name: "one",
 *      step: ({ next }) => <div>
 *        <h2>Step One</h2>
 *        <button onClick={next}>Go to step 2</button>
 *      </div>
 *    }, {
 *      name: "two",
 *      step: ({ next, prev, transition }) => <div>
 *        <h2>Step two</h2>
 *        <div>
 *          <button onClick={prev}>Go to step 1</button>
 *          <button onClick={next}>Go to step 3</button>
 *        </div>
 *      </div>
 *    }, {
 *     name: "three",
 *     step: ({ next, prev, go, transition }) => <div>
 *       <h2>Step three</h2>
 *       <div>
 *          <button onClick={prev}>Go to step 2</button>
 *          <button onClick={next}>Go to step 3</button>
 *          <button onClick={() => go("one")}>Go to step 1</button>
 *        </div>
 *       </div>
 *    }]}
 *  />
 * ```
 */
declare const MultiStep: React.FC<MultiStepProps>;
export default MultiStep;
