import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface CopyBoxProps extends BaseComponentProps {
    text: string;
    url: string;
    confirmationText: string;
    onClickCallback: () => void;
}
export declare enum CopyBoxTest {
    prefix = "CopyBox"
}
declare const CopyBox: React.FunctionComponent<CopyBoxProps>;
export default CopyBox;
