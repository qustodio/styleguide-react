import { BaseComponentProps } from '../../common/types';
import { OpenerIconType } from '../Opener/Opener';
import { IconType } from '../Icons';
interface ExpandCollapseProps extends BaseComponentProps {
    leftIcon?: IconType;
    leftImgSrc?: string;
    title: string;
    rightText?: string;
    rightElement?: JSX.Element;
    initialOpenState?: boolean;
    isDisabled?: boolean;
    testId: string;
    openerType?: OpenerIconType;
    onClick?: () => void;
}
declare const ExpandCollapse: ({ leftIcon, leftImgSrc, title, rightText, initialOpenState, isDisabled, testId, openerType, children, className, rightElement, onClick, }: ExpandCollapseProps) => JSX.Element;
export default ExpandCollapse;
