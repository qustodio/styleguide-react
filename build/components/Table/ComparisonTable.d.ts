import { IconType } from '../Icons';
export interface ComparisonTableData {
    head: Array<{
        icon?: typeof IconType[keyof typeof IconType];
        text: string;
    }>;
    rows: Array<string | boolean | null>[];
}
export interface ComparisonTableProps {
    data: ComparisonTableData;
    key: string;
    border?: 'default' | 'none';
}
declare const ComparisonTable: {
    ({ data, key, border, }: ComparisonTableProps): JSX.Element;
    displayName: string;
};
export default ComparisonTable;
