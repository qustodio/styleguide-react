import { BaseComponentProps } from '../../common/types';
declare const TableBody: ({ children }: BaseComponentProps) => JSX.Element;
export default TableBody;
