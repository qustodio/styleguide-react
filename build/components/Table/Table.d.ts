import { BaseComponentProps } from '../../common/types';
interface TableProps extends BaseComponentProps {
    border: 'default' | 'none';
}
declare const Table: ({ border, children, className, testId }: TableProps) => JSX.Element;
export default Table;
