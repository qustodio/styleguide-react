import { BaseComponentProps } from '../../common/types';
interface TableCellProps extends BaseComponentProps {
    head?: boolean;
    alignement?: 'left' | 'right' | 'center';
}
declare const TableCell: ({ head, alignement, children }: TableCellProps) => JSX.Element;
export default TableCell;
