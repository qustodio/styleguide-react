import { BaseComponentProps } from '../../common/types';
declare const TableRow: ({ children }: BaseComponentProps) => JSX.Element;
export default TableRow;
