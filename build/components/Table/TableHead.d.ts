import { BaseComponentProps } from '../../common/types';
declare const TableHead: ({ children }: BaseComponentProps) => JSX.Element;
export default TableHead;
