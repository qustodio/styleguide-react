import * as React from 'react';
import { AlertBoxProps } from './types/AlertBoxProps.types';
export declare enum AlertBoxTest {
    prefix = "AlertBox",
    actionElement = "Action__close"
}
declare const _default: React.NamedExoticComponent<AlertBoxProps>;
export default _default;
