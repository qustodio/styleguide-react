import React, { SyntheticEvent } from 'react';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
import { AriaLabelingProps } from '../../common/aria.types';
import { BaseComponentProps, DisabledProps, TestableComponentProps } from '../../common/types';
export type IconPosition = 'right' | 'left';
export interface LinkProps extends BaseComponentProps, TestableComponentProps, DisabledProps, AriaLabelingProps {
    href?: string;
    hrefLang?: string;
    rel?: string;
    target?: string;
    referrerPolicy?: string;
    useAsLink?: React.FunctionComponent<AnchorPropTypes>;
    ariaLabel?: string;
    iconPosition?: IconPosition;
    onClick?: (ev: SyntheticEvent) => void;
    icon?: JSX.Element;
}
declare const _default: React.NamedExoticComponent<LinkProps>;
export default _default;
