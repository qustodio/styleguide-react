import * as React from 'react';
export declare enum BoxType {
    danger = "danger",
    warning = "warning",
    success = "success",
    info = "info"
}
declare const Box: React.FunctionComponent<{
    type: BoxType;
    text: string;
    iconClass?: string;
}>;
export default Box;
