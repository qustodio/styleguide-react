import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum ContentSeparatorTest {
    prefix = "ContentSeparator"
}
declare const _default: React.NamedExoticComponent<BaseComponentProps>;
export default _default;
