import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export interface SelectionBarProps extends BaseComponentProps, DisabledProps {
    title: string;
    hasNext: boolean;
    hasPrev: boolean;
    onClickNext: (ev: React.SyntheticEvent) => void;
    onClickPrev: (ev: React.SyntheticEvent) => void;
}
export declare enum SelectionBarTest {
    prefix = "SelectionBar"
}
declare const SelectionBar: React.FunctionComponent<SelectionBarProps>;
export default SelectionBar;
