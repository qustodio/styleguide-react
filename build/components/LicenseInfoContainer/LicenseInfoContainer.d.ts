import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
export interface LicenseInfoContainerProps extends BaseComponentProps {
    affiliateBadgeIconSrc?: string;
    affiliateBadgeDescription?: string;
    licenseTitle?: string;
    licenseSubtitle?: string;
    licenseBadgeText?: string;
    licenseBadgeClick?: () => void;
    licenseLinkTo?: string;
    useAsLink?: React.FunctionComponent<AnchorPropTypes>;
}
export declare enum LicenseInfoContainerTest {
    prefix = "LicenseInfoContainer"
}
declare const LicenseInfoContainer: React.FunctionComponent<LicenseInfoContainerProps>;
export default LicenseInfoContainer;
