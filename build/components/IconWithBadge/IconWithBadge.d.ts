import { IconColor } from '../Icons';
export declare enum IconWithBadgeTest {
    prefix = "IconWithBadge"
}
export interface IconWithBadgeProps {
    icon: string | JSX.Element;
    showBadge: boolean;
    badgeColor?: IconColor;
    badgePosition?: 'right' | 'left';
    containerClassName?: string;
    badgeClassname?: string;
    iconClassname?: string;
    testId?: string;
}
declare const IconWithBadge: {
    ({ icon, showBadge, badgeColor, badgePosition, containerClassName, badgeClassname, iconClassname, testId, }: IconWithBadgeProps): JSX.Element;
    displayName: string;
};
export default IconWithBadge;
