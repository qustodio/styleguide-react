import { IconColor } from '../Icons';
export type OpenerIconType = 'chevron' | 'more';
export type OpenerAnimations = 'rotate' | null;
export interface OpenerProps {
    type: OpenerIconType;
    open: boolean;
    testId?: string;
    color?: IconColor;
}
export declare enum OpenerTest {
    prefix = "Opener"
}
declare const Opener: ({ type, open, testId, color, }: OpenerProps) => JSX.Element;
export default Opener;
