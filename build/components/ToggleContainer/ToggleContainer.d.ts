import { BaseComponentProps } from '../../common/types';
export interface ToggleContainerProps extends BaseComponentProps {
    buttonOpenText: string;
    buttonCloseText: string;
    initiallyOpen?: boolean;
}
declare const ToggleContainer: {
    ({ buttonOpenText, buttonCloseText, initiallyOpen, children, className, testId, }: ToggleContainerProps): JSX.Element;
    displayName: string;
};
export default ToggleContainer;
