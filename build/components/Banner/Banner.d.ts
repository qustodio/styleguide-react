import React, { SyntheticEvent } from 'react';
import { BaseComponentProps } from '../../common/types';
export type BannerType = 'warning' | 'primary' | 'error' | 'white';
export interface BannerProps extends BaseComponentProps {
    type: BannerType;
    icon?: JSX.Element;
    showCloseIcon?: JSX.Element;
    centered?: boolean;
    fullWidth?: boolean;
    rounded?: boolean;
    onClose?: (ev: SyntheticEvent) => void;
}
export declare enum BannerTest {
    prefix = "Banner",
    actionElement = "Action__close"
}
declare const _default: React.NamedExoticComponent<BannerProps>;
export default _default;
