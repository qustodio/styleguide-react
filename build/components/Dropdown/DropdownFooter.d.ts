import React, { SyntheticEvent } from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum DropdownFooterTest {
    prefix = "DropdownFooter"
}
export interface DropdownFooterProps extends BaseComponentProps {
    onClick: (ev: SyntheticEvent) => void;
}
declare const DropdownFooter: React.FunctionComponent<DropdownFooterProps>;
export default DropdownFooter;
