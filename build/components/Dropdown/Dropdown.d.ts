import React from 'react';
import { DropdownOptionValue } from './DropdownOption';
import { FloatingMenuPlacement } from '../../hooks/useFloatingPlacement';
import { DropdownAlignment, DropdownElementOrientation } from './types';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export interface DropdownProps extends BaseComponentProps, DisabledProps {
    actionElement: JSX.Element;
    portalId?: string;
    useSinglePortalHook?: boolean;
    menuPlacement?: FloatingMenuPlacement;
    fixedMenuPlacement?: boolean;
    attachedMenuOriention?: DropdownElementOrientation;
    showSelectedOption?: boolean;
    className?: string;
    classNameList?: string;
    asModal?: boolean;
    header?: JSX.Element;
    footer?: JSX.Element;
    maxHeight?: number;
    maxWidth: number;
    minWidth?: number;
    showScrollbars?: boolean;
    alignment?: DropdownAlignment;
    id?: string;
    active?: boolean;
    enableRemoveScroll?: boolean;
    onChange?: (value: DropdownOptionValue) => void;
    onActiveChange?: (isActive: boolean) => void;
    onClose?: () => void;
    onOpen?: () => void;
}
export declare enum DropdownTest {
    prefix = "Dropdown",
    container = "Dropdown__container"
}
declare const Dropdown: React.FunctionComponent<DropdownProps>;
export default Dropdown;
