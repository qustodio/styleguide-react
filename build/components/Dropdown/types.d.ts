import { DropdownOptionValue } from './DropdownOption';
import { FloatingMenuPlacement } from '../../hooks/useFloatingPlacement';
export declare enum DropdownElementOrientation {
    notSet = "not-set",
    left = "left",
    right = "right",
    center = "center"
}
export declare enum DropdownAlignment {
    top = "top",
    center = "center",
    bottom = "bottom"
}
export { FloatingMenuPlacement as DropdownMenuPlacement };
export { DropdownOptionValue };
export declare enum Keys {
    up = "ArrowUp",
    down = "ArrowDown",
    enter = "Enter"
}
