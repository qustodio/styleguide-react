import React, { SyntheticEvent } from 'react';
import { TextFieldProps } from '../TextField/TextField';
import { ActionInputProps } from '../ActionInput/ActionInput';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export interface DropdownActionElementProps extends BaseComponentProps, DisabledProps {
    text?: string;
    icon?: JSX.Element;
    iconRight?: JSX.Element;
    onClick?: (ev: SyntheticEvent) => void;
}
export declare enum DropdownActionElementTest {
    prefix = "DropdownActionElement"
}
declare const DropdownActionElement: React.FunctionComponent<DropdownActionElementProps>;
export declare const createDefaultActionElement: (props: DropdownActionElementProps) => JSX.Element;
export declare const createDefaultActionTextFieldElement: (props: TextFieldProps) => JSX.Element;
export declare const createDefaultActionInputElement: (props: ActionInputProps) => JSX.Element;
export default DropdownActionElement;
