import React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum DropdownHeaderTest {
    prefix = "DropdownHeader"
}
declare const DropdownHeader: React.FunctionComponent<BaseComponentProps>;
export default DropdownHeader;
