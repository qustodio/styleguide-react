import React, { SyntheticEvent } from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export type DropdownOptionValue = string | number | boolean;
export interface DropdownOptionProps extends BaseComponentProps, DisabledProps {
    value: DropdownOptionValue;
    text?: string;
    icon?: JSX.Element;
    selected?: boolean;
    active?: boolean;
    assignKey?: string;
    showAssignKey?: boolean;
    ref?: React.MutableRefObject<HTMLLIElement>;
    showAsSelected?: boolean;
    onClick?: (value: DropdownOptionValue, ev?: SyntheticEvent) => void;
    onMouseEnter?: (ev: SyntheticEvent, value: DropdownOptionValue) => void;
    onMouseLeave?: (ev: SyntheticEvent, value: DropdownOptionValue) => void;
}
export declare enum DropdownOptionTest {
    prefix = "DropdownOption"
}
declare const DropdownOption: React.ForwardRefExoticComponent<Pick<DropdownOptionProps, "value" | "text" | "icon" | "selected" | "active" | "assignKey" | "showAssignKey" | "showAsSelected" | "onClick" | "onMouseEnter" | "onMouseLeave" | "id" | "role" | "tabIndex" | "children" | "className" | "testId" | "disabled"> & React.RefAttributes<HTMLLIElement>>;
export default DropdownOption;
