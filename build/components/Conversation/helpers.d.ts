import { ConversationMessage, ConversationContact, ConversationThread } from './types/Conversation.types';
declare const prepareConversation: (conversation: {
    date?: string;
    messages: ConversationMessage[];
}[], contacts: ConversationContact[]) => {
    date?: string;
    threads: ConversationThread[];
}[];
export default prepareConversation;
