export type ConversationContactId = string;
export type ConversationHighlights = 'warning' | 'error';
export type ConversationContentType = 'text' | 'image' | 'mixed';
export type ConversationMessage = {
    id: string;
    contactId: ConversationContactId;
    createdAt: number;
    content: string | ((message?: ConversationMessage) => React.ReactNode);
    outgoing: boolean;
    /** @default "text" */
    contentType?: ConversationContentType;
    highlight?: ConversationHighlights;
};
export type ConversationContact = {
    id: string;
    name: string;
    pictureUrl?: string;
};
export type ConversationThread = {
    id: string;
    contact: ConversationContact;
    messages: ConversationMessage[];
    outgoing: boolean;
};
export type ConversationType = {
    date?: string;
    messages: ConversationMessage[];
}[];
