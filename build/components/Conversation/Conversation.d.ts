import { BaseComponentProps } from '../../common/types';
import { ConversationMessage, ConversationContact } from './types/Conversation.types';
export interface ConversationProps extends BaseComponentProps {
    conversation: {
        date?: string;
        messages: ConversationMessage[];
    }[];
    contacts: ConversationContact[];
}
declare const Conversation: ({ conversation, contacts, className, testId, }: ConversationProps) => JSX.Element;
export default Conversation;
