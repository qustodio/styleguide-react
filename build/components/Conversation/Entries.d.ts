import React from 'react';
import { ConversationMessage, ConversationContact, ConversationHighlights } from './types/Conversation.types';
export declare const MessageEntry: ({ message }: {
    message: ConversationMessage;
}) => JSX.Element;
export declare const HighlightIcon: ({ highlight, }: {
    highlight: ConversationHighlights | undefined;
}) => JSX.Element | null;
export declare const ThreadEntry: ({ highlight, children, }: {
    highlight: ConversationHighlights | undefined;
    children: React.ReactNode;
}) => JSX.Element;
export declare const ContactEntry: ({ contact }: {
    contact: ConversationContact;
}) => JSX.Element;
export declare const DateIndicator: ({ date }: {
    date: string;
}) => JSX.Element;
