import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export type InfoFieldLabelLines = '1' | '2' | '3' | '4' | '5';
export type InfoFieldLabelMode = 'info' | 'error';
export interface InfoFieldLabelProps extends BaseComponentProps {
    mode?: InfoFieldLabelMode;
    htmlFor?: string;
    allowedLines?: InfoFieldLabelLines;
    keepSpace?: InfoFieldLabelLines;
}
export declare enum InfoFieldLabelTest {
    prefix = "InfoFieldLabel"
}
declare const _default: React.NamedExoticComponent<InfoFieldLabelProps>;
export default _default;
