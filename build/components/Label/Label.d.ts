import React from 'react';
import { BaseComponentProps, TestableComponentProps } from '../../common/types';
export interface LabelProps extends BaseComponentProps, TestableComponentProps {
    htmlFor?: string;
    ellipsis?: boolean;
}
export declare enum LabelTest {
    prefix = "Label"
}
declare const _default: React.NamedExoticComponent<LabelProps>;
export default _default;
