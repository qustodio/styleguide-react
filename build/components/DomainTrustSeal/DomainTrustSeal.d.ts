import React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum DomainTrustSealTest {
    prefix = "DomainTrustSeal"
}
export type DomainTrustSealReputation = 'unknown' | 'very-poor' | 'poor' | 'unsatisfactory' | 'good' | 'excellent';
export type DomainTrustSealMode = 'trustworthiness' | 'child-safety';
export interface DomainTrustSealProps extends BaseComponentProps {
    mode: DomainTrustSealMode;
    level: DomainTrustSealReputation;
    title: string;
    description: string;
}
declare const _default: React.NamedExoticComponent<DomainTrustSealProps>;
export default _default;
