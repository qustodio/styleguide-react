import { BaseComponentProps } from '../../common/types';
import { OpenerIconType } from '../Opener/Opener';
import { IconColor } from '../Icons';
interface ExpandableBaseProps extends BaseComponentProps {
    leftContent: JSX.Element;
    rightContent?: JSX.Element;
    initialOpenState?: boolean;
    isDisabled?: boolean;
    testId: string;
    openerType?: OpenerIconType;
    openerColor?: IconColor;
    onClick?: () => void;
}
export declare enum ExpandableTest {
    prefix = "Expandable",
    opener = "Opener",
    content = "Content"
}
declare const ExpandableBase: ({ children, className, leftContent, rightContent, initialOpenState, isDisabled, testId, openerType, openerColor, onClick, }: ExpandableBaseProps) => JSX.Element;
export default ExpandableBase;
