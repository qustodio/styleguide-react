import { PlanBoxAddonCheckboxProps } from './types/PlanBoxAddonCheckboxProps';
declare const PlanBoxAddonCheckbox: {
    ({ label, showCheckbox, isChecked, onClick, description, list, }: PlanBoxAddonCheckboxProps): JSX.Element;
    displayName: string;
};
export default PlanBoxAddonCheckbox;
