import * as React from 'react';
import { RibbonType } from '../Ribbon/Ribbon';
import { PlanBoxBaseProps } from './types/PlanBoxBaseProps';
export declare enum PlanBoxTest {
    prefix = "PlanBox"
}
export interface PlanBoxProps extends PlanBoxBaseProps {
    deviceInformation: string;
    hasDiscount?: boolean;
    ribbon?: {
        type: RibbonType;
        text: string;
    };
    carePlusInfo?: {
        showCheckbox: boolean;
        carePlusIncluded: boolean;
        label: JSX.Element;
        description: string;
        list: JSX.Element;
        onClick: () => void;
        isChecked: boolean;
    };
}
declare const PlanBox: React.FunctionComponent<PlanBoxProps>;
export default PlanBox;
