export interface PlanBoxFeatureListItemProps {
    icon: JSX.Element;
    text: string;
}
