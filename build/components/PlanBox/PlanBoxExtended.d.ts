import { IconType } from '../Icons';
import { PlanBoxBaseProps } from './types/PlanBoxBaseProps';
import { PlanBoxAddonCheckboxProps } from './types/PlanBoxAddonCheckboxProps';
export interface PlanBoxExtendedProps extends PlanBoxBaseProps {
    icon: IconType;
    subtitle: string;
    priceAdditionalInformation: string;
    ribbon?: {
        text: string;
    };
    features?: {
        title: string;
        list: string[];
    };
    addonInfo?: PlanBoxAddonCheckboxProps;
}
declare const PlanBoxExtended: {
    ({ icon, title, subtitle, price, years, priceAdditionalInformation, highlighted, current, currentText, addonInfo, ribbon, button, features, testId, }: PlanBoxExtendedProps): JSX.Element;
    displayName: string;
};
export default PlanBoxExtended;
