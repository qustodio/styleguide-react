import * as React from 'react';
import { PlanBoxFeatureListItemProps } from './types/PlanBoxFeatureListItemProps';
declare const PlanBoxFeatureListItem: React.FC<PlanBoxFeatureListItemProps>;
export default PlanBoxFeatureListItem;
