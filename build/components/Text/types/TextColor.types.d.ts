import { textColors } from '../constants';
export type TextColor = typeof textColors[number];
