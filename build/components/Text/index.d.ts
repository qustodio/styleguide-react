export { default } from './Text';
export { TextColor } from './types/TextColor.types';
export { TextProps } from './types/TextProps.types';
export { TextType } from './types/TextType.types';
export { default as TextTest } from './types/TextTest.types';
