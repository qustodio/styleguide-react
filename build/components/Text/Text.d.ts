import * as React from 'react';
import { TextProps } from './types/TextProps.types';
declare const Text: {
    ({ variant, color, align, renderAs, noWrap, marginTop, marginBottom, parentTestId, testId, children, className, }: TextProps): React.ReactElement<unknown, string | React.JSXElementConstructor<any>>;
    displayName: string;
};
export default Text;
