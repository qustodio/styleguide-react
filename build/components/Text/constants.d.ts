/**
 * Fixed typography variants
 *
 * These are the base typography types that are used in the design system.
 */
export declare const textBaseVariants: readonly ["hero-1-semibold", "hero-1-regular", "hero-2-semibold", "hero-2-regular", "headline-1-semibold", "headline-1-regular", "headline-2-semibold", "headline-2-regular", "title-1-semibold", "title-1-regular", "title-2-semibold", "title-2-regular", "title-3-semibold", "title-3-regular", "body-1-semibold", "body-1-regular", "body-2-semibold", "body-2-regular", "caption-1-semibold", "caption-1-regular", "caption-1-italic", "caption-2-semibold", "caption-2-regular", "caption-2-italic"];
/**
 * These typographies change their size based on the viewport width.
 */
export declare const textResponsiveVariants: readonly ["headline-1-semibold-responsive", "headline-1-regular-responsive", "headline-2-semibold-responsive", "headline-2-regular-responsive", "title-1-semibold-responsive", "title-1-regular-responsive", "title-2-semibold-responsive", "title-2-regular-responsive", "title-3-semibold-responsive", "title-3-regular-responsive"];
export declare const textVariants: readonly ["hero-1-semibold", "hero-1-regular", "hero-2-semibold", "hero-2-regular", "headline-1-semibold", "headline-1-regular", "headline-2-semibold", "headline-2-regular", "title-1-semibold", "title-1-regular", "title-2-semibold", "title-2-regular", "title-3-semibold", "title-3-regular", "body-1-semibold", "body-1-regular", "body-2-semibold", "body-2-regular", "caption-1-semibold", "caption-1-regular", "caption-1-italic", "caption-2-semibold", "caption-2-regular", "caption-2-italic", "headline-1-semibold-responsive", "headline-1-regular-responsive", "headline-2-semibold-responsive", "headline-2-regular-responsive", "title-1-semibold-responsive", "title-1-regular-responsive", "title-2-semibold-responsive", "title-2-regular-responsive", "title-3-semibold-responsive", "title-3-regular-responsive"];
export declare const textColors: readonly ["grey-100", "grey-200", "grey-300", "grey-400", "grey-500", "error", "warning", "success", "white", "brand"];
/**
 * It's recommended to use the `renderAs` prop to avoid coupling styles with
 * specific HTML elements.
 * By default, typography types are mapped to corresponding HTML elements, but
 * using `renderAs` allows for more flexibility in defining the heading level
 * hierarchy.
 */
export declare const textVariantElementMappings: Record<typeof textVariants[number], keyof React.ReactHTML>;
