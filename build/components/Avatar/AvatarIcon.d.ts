import { AvatarProps } from './Avatar.types';
declare const AvatarIcon: {
    ({ icon, iconColor, iconBackground, }: Pick<AvatarProps, 'icon' | 'iconBackground' | 'iconColor'>): JSX.Element | null;
    displayName: string;
};
export default AvatarIcon;
