import { AriaLabelingProps } from '../../common/aria.types';
import { BaseComponentProps, ImageLikeComponentProps, ValueOf } from '../../common/types';
import { TransparentButtonProps } from '../Button/TransparentButton';
import { IconType } from '../Icons';
import { LinkProps } from '../Link/Link';
export declare enum AvatarSize {
    extraExtraSmall = "xx-small",
    extraSmall = "x-small",
    small = "small",
    medium = "medium",
    large = "large",
    extraLarge = "x-large"
}
export declare enum AvatarTest {
    prefix = "Avatar"
}
type AvatarIconColors = 'white' | 'grey' | 'brand' | 'success' | 'error' | 'warning';
export type DefaultAvatarIconsSet = 'pencil' | 'camera' | 'status_tampered' | 'status_paused' | 'status_online';
interface AvatarPropsBase extends BaseComponentProps, AriaLabelingProps, ImageLikeComponentProps {
    size?: AvatarSize;
    /**
     * For empty avatar that requires fallback background (light-blue)
     *
     * @deprecated [Legacy, Retro compatibility] The light-blue background color varaint will be removed.
     * */
    emptyBackground?: boolean;
    /**
     * Prop to remove hidden overflow.
     * Acomomdates for avatars that do not fit in a circular shape.
     *
     * @deprecated [Retro compatibility] This prop is used so the animal
     * avatars are not cut by the avatar hidden overflow.
     * */
    isFreeForm?: boolean;
    /**
     * Allows to choose icon from a default set, from the `IconType` enum or as a JSX element.
     * */
    icon?: DefaultAvatarIconsSet | ValueOf<typeof IconType> | JSX.Element;
    iconColor?: AvatarIconColors;
    iconBackground?: 'none' | AvatarIconColors;
    renderAs?: 'link' | 'button';
}
export type AvatarButtonBase = Omit<TransparentButtonProps, 'onClick'>;
export type AvatarLinkBase = Omit<LinkProps, 'useAsLink' | 'ariaLabel' | 'iconPosition' | 'onClick' | 'icon' | 'disabled'>;
type AvatarLinkProps = {
    renderAs: 'link';
} & AvatarLinkBase;
type AvatarButtonProps = {
    renderAs: 'button';
    onClick?: (e: React.SyntheticEvent) => void;
} & AvatarButtonBase;
export type AvatarProps = AvatarPropsBase & (AvatarButtonProps | AvatarLinkProps | {
    renderAs?: never;
});
export {};
