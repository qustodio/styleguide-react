import * as React from 'react';
import { AvatarProps } from './Avatar.types';
declare const Avatar: React.ForwardRefExoticComponent<AvatarProps & React.RefAttributes<unknown>>;
export default Avatar;
