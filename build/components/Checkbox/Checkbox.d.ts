import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export interface CheckboxProps extends BaseComponentProps, DisabledProps {
    name: string;
    label?: string | JSX.Element;
    checked?: boolean;
    block?: boolean;
    onClick?: (isChecked: boolean) => void;
    onBlur?: (ev: React.SyntheticEvent) => void;
    onFocus?: (ev: React.SyntheticEvent) => void;
    onMouseDown?: (ev: React.SyntheticEvent) => void;
    onMouseUp?: (ev: React.SyntheticEvent) => void;
}
export declare enum CheckboxTest {
    prefix = "Checkbox",
    box = "Box"
}
declare const Checkbox: React.FunctionComponent<CheckboxProps>;
export default Checkbox;
