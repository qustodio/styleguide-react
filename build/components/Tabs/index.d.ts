import Tabs, { TabsTest } from './Tabs';
import TabPanel, { TabPanelTest } from './TabPanel';
export { TabPanel, TabPanelTest, TabsTest };
export default Tabs;
