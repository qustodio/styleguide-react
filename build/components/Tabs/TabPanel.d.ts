import * as React from 'react';
import { BaseComponentProps, DisabledProps, GlobalType } from '../../common/types';
export interface TabPanelProps extends BaseComponentProps, DisabledProps {
    content: string | JSX.Element;
    active: boolean;
    name: string;
    animateWhenChange?: string;
    children: React.ReactNode;
    activeColor?: GlobalType;
}
export declare enum TabPanelTest {
    prefix = "TabPanel"
}
declare const TabPanel: React.FunctionComponent<TabPanelProps>;
export default TabPanel;
