import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface TabProps extends BaseComponentProps {
    onChange: (name: string) => void;
    animateOnChange?: boolean;
    showSeparator?: boolean;
    hugContent?: boolean;
}
export declare enum TabsTest {
    prefix = "Tabs",
    tab = "Tab"
}
declare const Tabs: React.FunctionComponent<TabProps>;
export default Tabs;
