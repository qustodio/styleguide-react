import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { IconColor, IconType } from '../Icons';
export declare enum InstructionCardTest {
    prefix = "InstructionCard"
}
interface InstructionCardProps extends BaseComponentProps {
    iconType: IconType;
    iconColor: IconColor;
    title: string;
    description: string;
    instructions: string;
    url: string;
    compact: boolean;
    iconAsLink?: boolean;
}
declare const InstructionCard: React.FunctionComponent<InstructionCardProps>;
export default InstructionCard;
