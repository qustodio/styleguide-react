import React from 'react';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
import { BaseComponentProps, TestableComponentProps, TestableNestedCompomentProps } from '../../common/types';
import { BadgeProps } from '../Badge/Badge';
import { AffiliateBadgeProps } from '../Badge/AffiliateBadge';
import { AccountInfoHeaderProps } from '../AccountInfoHeader/AccountInfoHeader';
import { LicenseInfoContainerProps } from '../LicenseInfoContainer/LicenseInfoContainer';
export interface SideNavigationMenuProps extends BaseComponentProps, TestableNestedCompomentProps {
    userName: string;
    expand: boolean;
    accountInfo: React.ReactElement<AccountInfoHeaderProps>;
    licenseInfo?: React.ReactElement<LicenseInfoContainerProps> | React.ReactElement<BadgeProps>;
    affiliateBadge?: React.ReactElement<AffiliateBadgeProps>;
    licenseBadge?: JSX.Element;
    items: SideNavigationMenuItemType[];
    showGift?: boolean;
    onClickGift: () => void;
}
export interface MenuItemExtensionProps extends TestableComponentProps, TestableNestedCompomentProps {
    title: string;
    showBadge: boolean | undefined;
    showAccent: boolean | undefined;
    badge?: JSX.Element;
}
export interface SideNavigationMenuAnchorProps extends TestableComponentProps, TestableNestedCompomentProps {
    item: SideNavigationMenuItemType;
}
export interface SideNavigationMenuItemProps extends TestableComponentProps, TestableNestedCompomentProps {
    item: SideNavigationMenuItemType;
}
export type SideNavigationMenuItemType = {
    key: string;
    title: string;
    icon: JSX.Element;
    useAsLink?: React.FunctionComponent<AnchorPropTypes>;
    isActive: boolean;
    onClick: () => void;
    linkTo?: string;
    shouldShow?: boolean;
    showBadge?: boolean;
    showAccent?: boolean;
    badge?: JSX.Element;
};
export declare enum SideNavigationMenuTest {
    prefix = "SideNavigationMenu",
    menuItem = "SideNavigationMenuItem",
    anchor = "SideNavigationMenuAnchor",
    extension = "SideNavigationMenuItemExtension",
    menuItemWrapper = "SideNavigationMenuItem-wrapper"
}
declare const SideNavigationMenu: React.FunctionComponent<SideNavigationMenuProps>;
export default SideNavigationMenu;
