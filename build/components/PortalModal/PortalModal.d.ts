import * as React from 'react';
import { TestableComponentProps } from '../../common/types';
export interface PortalModalProps extends TestableComponentProps {
    overlayId?: string;
    overlayClassName?: string;
    modalWrapperClassName?: string;
    modalWrapperWidth?: string | number;
    modalWrapperHeight?: string | number;
    modalClassName?: string;
    closeButtonClassName?: string;
    animationEnabled?: boolean;
    hideCloseButton?: boolean;
    showBackButton?: boolean;
    onClose: () => void;
}
export declare enum PortalModalTest {
    prefix = "PortalModal",
    closeButton = "Action-close",
    backButton = "Action-back"
}
declare class PortalModal extends React.Component<PortalModalProps> {
    static readonly portalId = "styleguide-react-portal-modal";
    handleClose: (evt: React.SyntheticEvent) => void;
    stopEventPropagation: (evt: React.SyntheticEvent) => void;
    render(): JSX.Element;
}
export default PortalModal;
