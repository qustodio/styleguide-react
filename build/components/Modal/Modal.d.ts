import * as React from 'react';
import { BaseComponentProps, GlobalType } from '../../common/types';
import ModalStyledText from './utils/ModalStyledText';
export interface ModalHeader {
    icon?: JSX.Element;
    illustration?: JSX.Element;
}
/** @deprecated The close icon should always use the grey color */
export declare enum ModalCloseIconType {
    black = "black",
    white = "white"
}
/** @deprecated Buttons should always be in column */
export declare enum ModalButtonsAlignment {
    row = "row",
    column = "column"
}
export { ModalStyledText };
export declare enum ModalTest {
    prefix = "Modal",
    title = "Title_content",
    body = "Body_content"
}
export type ModalSizes = 'small' | 'medium' | 'large';
export interface ModalProps extends BaseComponentProps {
    header?: ModalHeader;
    size: ModalSizes;
    /** @deprecated Use `size` prop instead */
    width?: number;
    /** @deprecated modals should not have a defined height */
    height?: number;
    type?: GlobalType;
    title?: string;
    buttons?: JSX.Element[];
    buttonsAlignment?: ModalButtonsAlignment;
    /** @deprecated - does nothing */
    actionLabel?: string;
    /** @deprecated - does nothing */
    actionLoading?: boolean;
    closeIconType?: ModalCloseIconType;
    animationEnabled?: boolean;
    hideCloseButton?: boolean;
    /** When clicking back button, `onClickClose` will be invoked. */
    showBackButton?: boolean;
    /** @deprecated use the `size` property with `large` value, which goes fullscreen on mobile. */
    isFullScreen?: boolean;
    fixedContent?: JSX.Element[];
    onClickClose: () => void;
    onClickAction?: () => void;
}
declare const Modal: React.FunctionComponent<ModalProps>;
export default Modal;
