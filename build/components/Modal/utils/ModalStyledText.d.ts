import React from 'react';
import { BaseComponentProps } from '../../../common/types';
export interface ModalStyledTextProps extends BaseComponentProps {
    textAlign?: 'center' | 'left' | 'right';
    marginBottom?: '8' | '16' | '24';
}
declare const ModalStyledText: React.FunctionComponent<ModalStyledTextProps>;
export default ModalStyledText;
