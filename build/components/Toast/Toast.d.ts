import React from 'react';
import { ToastProps } from './types/toast.types';
declare const Toast: React.FC<ToastProps>;
export default Toast;
