import { SyntheticEvent } from 'react';
import { ToastPosition, ToastType } from './types/toast.types';
declare class ToastManager {
    private static instance;
    private timeout?;
    defaultPosition: ToastPosition;
    defaultHook: string;
    defaultTimeout: number;
    private constructor();
    private createPortal;
    private removeCurrentToast;
    private scheduleTaskToRemoveToast;
    private removeScheduledTaskToRemoveToast;
    private onCloseToastHandler;
    static getInstance(): ToastManager;
    setDefaultPosition(position: ToastPosition): void;
    setDefaultHook(defaultHook: string): void;
    setDefaultTimeout(ms: number): void;
    sendWithoutTimeout(title: string, message: string | JSX.Element, type?: ToastType, icon?: JSX.Element, onClose?: (ev: SyntheticEvent) => void, position?: ToastPosition): void;
    send(title: string, message: string | JSX.Element, type?: ToastType, icon?: JSX.Element, position?: ToastPosition, timeout?: number, onClose?: (ev: SyntheticEvent) => void): void;
}
export default ToastManager;
