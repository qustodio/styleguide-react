import { BaseComponentProps } from '../../../../common/types';
import { SvgColor } from './SvgColor.types';
import { SvgSizes } from './SvgSize.types';
export interface SvgProps extends BaseComponentProps {
    height?: SvgSizes;
    width?: SvgSizes;
    color?: SvgColor;
}
