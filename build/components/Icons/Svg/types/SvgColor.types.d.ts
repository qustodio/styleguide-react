export declare enum SvgColor {
    notSet = "not-set",
    error = "error",
    warning = "warning",
    success = "success",
    secondary = "secondary",
    neutral = "neutral",
    black = "black",
    primary = "primary",
    regular = "regular",
    contrast = "contrast",
    grayLight = "gray-light"
}
