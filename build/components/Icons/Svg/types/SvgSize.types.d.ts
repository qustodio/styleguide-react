import { ValueOf } from '../../../../common/types';
export declare const svgSizes: readonly ["16", "24", "32", "40", "48", "56", "64", "72", "80", "88", "96", "104", "112", "120", "128", "136", "144", "152", "160", "168", "176", "184", "192", "200", "240", "280"];
export type SvgSizes = ValueOf<typeof svgSizes>;
