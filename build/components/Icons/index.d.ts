import Icon, { IconTest } from './Icon';
import { IconColor } from './types/IconColor.types';
import { IconFamily } from './types/IconFamily.types';
import { IconSize } from './types/IconSize.types';
import { IconType } from './types/IconType.types';
export { IconColor, IconSize, IconFamily, IconType, IconTest };
export default Icon;
