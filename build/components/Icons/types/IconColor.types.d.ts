export declare enum IconColor {
    notSet = "not-set",
    error = "error",
    warning = "warning",
    success = "success",
    secondary = "secondary",
    neutral = "neutral",
    black = "black",
    primary = "primary",
    regular = "regular",
    contrast = "contrast",
    contrastBlue = "contrast-secondary",
    greenDecorationContrast = "green-decoration-contrast",
    grayLight = "gray-light",
    disabled = "disabled"
}
