import { ValueOf } from '../../../common/types';
export declare const IconSize: {
    readonly xs: "xs";
    readonly sm: "sm";
    readonly lg: "lg";
    readonly x2: "x2";
    readonly x3: "x3";
    readonly x4: "x4";
    readonly x5: "x5";
    readonly x7: "x7";
    readonly x10: "x10";
    readonly regular: "regular";
};
export type IconSize = ValueOf<typeof IconSize>;
