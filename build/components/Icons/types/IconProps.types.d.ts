import { BaseComponentProps } from '../../../common/types';
import { RocketCustomIconProps } from '../CustomIcons/types/RocketCustomIconProps.types';
import { RocketCustomIconSource } from '../CustomIcons/types/RocketCustomIconSource.types';
import { FontAwesomeIconProps } from '../FontAwesome/types/FontAwesomeIconProps.types';
import { FontAwesomeIconSource } from '../FontAwesome/types/FontAwesomeIconSource.types';
import { BaseIconProps } from './BaseIconProps.types';
import { IconContainer } from './IconContainer.types';
import { IconEvents } from './IconEvents.types';
export type IconProps = BaseComponentProps & BaseIconProps & IconEvents & IconContainer & (({
    source?: FontAwesomeIconSource;
} & FontAwesomeIconProps) | ({
    source: RocketCustomIconSource;
} & RocketCustomIconProps));
