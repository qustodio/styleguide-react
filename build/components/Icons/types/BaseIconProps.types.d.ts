import { AriaRole, TabIndex } from '../../../common/aria.types';
import { IconColor } from './IconColor.types';
export interface BaseIconProps {
    ariaHidden?: boolean;
    ariaLabel?: string;
    title?: string;
    role?: AriaRole;
    tabIndex?: TabIndex;
    color?: IconColor;
}
