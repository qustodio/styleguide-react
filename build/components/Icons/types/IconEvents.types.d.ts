import { SyntheticEvent } from 'react';
export interface IconEvents {
    onClick?: (ev: SyntheticEvent) => void;
    onFocus?: (ev: SyntheticEvent) => void;
    onBlur?: (ev: SyntheticEvent) => void;
}
