import { ValueOf } from '../../../common/types';
export declare const BaseIconSize: {
    readonly regular: "regular";
};
export type BaseIconSize = ValueOf<typeof BaseIconSize>;
