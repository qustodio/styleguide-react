import { RocketCustomIconSource } from '../CustomIcons/types/RocketCustomIconSource.types';
import { FontAwesomeIconSource } from '../FontAwesome/types/FontAwesomeIconSource.types';
export type IconSource = FontAwesomeIconSource | RocketCustomIconSource;
