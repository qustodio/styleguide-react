import React from 'react';
import { SvgProps } from './Svg/types/SvgProps.types';
export declare enum SvgTest {
    prefix = "Svg"
}
declare const _default: React.NamedExoticComponent<SvgProps>;
export default _default;
