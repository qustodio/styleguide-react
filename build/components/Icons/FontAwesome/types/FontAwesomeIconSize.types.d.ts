import { ValueOf } from '../../../../common/types';
import { IconSize } from '../../types/IconSize.types';
export declare const FontAwesomeIconSize: {
    [size in IconSize]: string;
};
export type FontAwesomeIconSize = ValueOf<typeof FontAwesomeIconSize>;
