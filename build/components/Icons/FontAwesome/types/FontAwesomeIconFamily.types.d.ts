export declare enum FontAwesomeIconFamily {
    brands = "fa-brands",
    solid = "fa-solid",
    regular = "fa-regular"
}
