import { IconSize } from '../../types/IconSize.types';
import { FontAwesomeIconFamily } from './FontAwesomeIconFamily.types';
import { FontAwesomeIconType } from './FontAwesomeIconType.types';
export type FontAwesomeIconProps = {
    type: FontAwesomeIconType;
    size?: IconSize;
    family?: FontAwesomeIconFamily;
};
