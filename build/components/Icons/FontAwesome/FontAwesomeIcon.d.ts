import React from 'react';
import { FontAwesomeIconProps } from './types/FontAwesomeIconProps.types';
declare const FontAwesomeIcon: React.FC<FontAwesomeIconProps>;
export default FontAwesomeIcon;
