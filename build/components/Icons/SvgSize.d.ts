import React from 'react';
import { SvgSizeProps } from './types';
export declare enum SvgSizeTest {
    prefix = "SvgSize"
}
declare const SvgSize: React.FunctionComponent<SvgSizeProps>;
export default SvgSize;
