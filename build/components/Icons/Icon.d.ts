import * as React from 'react';
import { IconProps } from './types/IconProps.types';
export declare enum IconTest {
    prefix = "Icon"
}
declare const _default: React.NamedExoticComponent<IconProps>;
export default _default;
