import { BaseComponentProps } from '../../common/types';
export * from './types/IconSize.types';
export * from './types/IconFamily.types';
export * from './types/IconType.types';
export * from './types/IconSource.types';
export declare enum IconColor {
    notSet = "not-set",
    error = "error",
    warning = "warning",
    success = "success",
    secondary = "secondary",
    neutral = "neutral",
    black = "black",
    primary = "primary",
    regular = "regular",
    contrast = "contrast",
    contrastBlue = "contrast-secondary",
    greenDecorationContrast = "green-decoration-contrast",
    grayLight = "gray-light",
    disabled = "disabled"
}
export declare enum SvgColor {
    notSet = "not-set",
    error = "error",
    warning = "warning",
    success = "success",
    secondary = "secondary",
    neutral = "neutral",
    black = "black",
    primary = "primary",
    regular = "regular",
    contrast = "contrast",
    grayLight = "gray-light"
}
export declare type SvgSizes = '16' | '24' | '32' | '40' | '48' | '56' | '64' | '72' | '80' | '88' | '96' | '104' | '112' | '120' | '128' | '136' | '144' | '152' | '160' | '168' | '176' | '184' | '192' | '200' | '240' | '280';
export interface SvgProps extends BaseComponentProps {
    height?: SvgSizes;
    width?: SvgSizes;
    color?: SvgColor;
}
