import { IconSize } from '../../types';
import { RocketCustomIconType } from './RocketCustomIconType.types';
export declare type RocketCustomIconProps = {
    type: RocketCustomIconType;
    size?: IconSize;
};
