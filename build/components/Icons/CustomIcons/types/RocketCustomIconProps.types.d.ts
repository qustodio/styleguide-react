import { IconSize } from '../../types/IconSize.types';
import { RocketCustomIconType } from './RocketCustomIconType.types';
export type RocketCustomIconProps = {
    type: RocketCustomIconType;
    size?: IconSize;
};
