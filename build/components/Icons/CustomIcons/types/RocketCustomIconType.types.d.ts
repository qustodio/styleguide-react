export declare enum RocketCustomIconType {
    outgoingCall = "outgoing-call",
    incomingCall = "incoming-call"
}
