import { ValueOf } from '../../../../common/types';
import { IconSize } from '../../types/IconSize.types';
export declare const RocketCustomIconSize: {
    [size in IconSize]: string;
};
export type RocketCustomIconSize = ValueOf<typeof RocketCustomIconSize>;
