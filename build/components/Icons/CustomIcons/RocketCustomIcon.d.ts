import React from 'react';
import { RocketCustomIconProps } from './types/RocketCustomIconProps.types';
declare const RocketCustomIcon: React.FC<RocketCustomIconProps>;
export default RocketCustomIcon;
