import { Theme } from '@material-ui/core/styles';
declare const styles: (theme: Theme) => (props?: any) => import("@material-ui/styles").ClassNameMap<"rounded" | "contained" | "root" | "containedSizeSmall" | "outlinedSizeSmall" | "containedSizeLarge" | "outlinedSizeLarge" | "roundedSizeSmall" | "roundedSizeLarge" | "containedPrimary" | "containedSecondary" | "containedWarning" | "containedError" | "outlinedWarning" | "outlinedError" | "textWarning" | "textError">;
export default styles;
