import React from 'react';
import { DisabledProps, BaseComponentProps } from '../../../common/types';
export type MuiButtonSize = 'small' | 'medium' | 'large';
export interface MuiButtonProps extends BaseComponentProps, DisabledProps {
    rounded?: boolean;
    fullWidth?: boolean;
    type: 'primary' | 'inherit' | 'default' | 'secondary' | undefined;
    variant: 'text' | 'contained' | 'outlined';
    size: MuiButtonSize;
    onClick?: () => void;
}
declare const MuiButton: React.FunctionComponent<MuiButtonProps>;
export default MuiButton;
