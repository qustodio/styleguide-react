import React from 'react';
import { BaseComponentProps } from '../../common/types';
export type FreeTextFontSize = '10px' | '12px' | '14px' | '16px' | '18px' | '20px' | '24px' | '30px' | '36px' | '48px';
export type FreeTextColor = 'gray' | 'gray-dark' | 'gray-semi' | 'gray-light' | 'gray-lighter' | 'error-dark' | 'warning-dark' | 'success-dark' | 'white';
export type FreeTextFontWeight = 'normal' | 'semi-bold';
export interface FreeTextProps extends BaseComponentProps {
    fontSize: FreeTextFontSize;
    color: FreeTextColor;
    fontWeight: FreeTextFontWeight;
}
export declare enum FreeTextTest {
    prefix = "FreeText"
}
declare const FreeText: React.FC<FreeTextProps>;
export default FreeText;
