import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum DrawerTest {
    prefix = "Drawer"
}
type From = 'left' | 'right';
interface BaseDrawerProps extends BaseComponentProps {
    isOpen: boolean;
    children: React.ReactNode;
    from: From;
    id?: string;
}
export interface NoContextDrawerProps extends BaseDrawerProps {
    /** Used to propagate initial className to nested children */
    baseClassName?: string;
}
export interface DrawerProps extends BaseDrawerProps {
    overlay?: boolean;
}
/**
 *
 * Defines drawer behavior without specifying content sizes, is used as base abstraction without context.
 *
 * Behavior
 *
 * 1. It Adapts size of children components.
 * 2. It adapts position of parent component.
 *
 * Important
 *
 * 1. It's recommended to use NoContextDrawer component together with useToggle hook to manage its state.
 * 2. it's recommended to use NoContextDrawer component together with Portal component to define parent position.
 *
 * @example
 *
 *```tsx
 * import { useToggle, NoContextDrawer, AutoPortal } from 'styleguide-react';
 *
 * const LeftSideMenu = () => {
 *   const [isOpen toggle] = useToggle(false);
 *
 *   return <div>
 *            <button onClick={toggle}>Toggle menu</button>
 *
 *            <AutoPortal id="left-drawer" className="custom-portal">
 *              <NoContextDrawer from="left" isOpen={isOpen}>
 *                  <Menu />
 *              <NoContextDrawer />
 *            </AutoPortal>
 *          </div>
 *
 * }
 *
 *```
 */
export declare const NoContextDrawer: React.FC<NoContextDrawerProps>;
/**
 *
 * Behavior
 *
 * 1. Uses all available screen height.
 * 2. It defines the positions in which it can be used (fixed left or fixed right).
 * 3. If an id is not provided works as singleton. If its need multiple instances must provide id for each instance.
 *
 * Important
 *
 * 1. It's recommended to use Drawer component together with useToggle hook to manage its state.
 *
 * @example
 *
 *```tsx
 * import { useToggle, Drawer } from 'styleguide-react';
 *
 * const LeftSideMenu = () => {
 *   const [isOpen toggle] = useToggle(false);
 *
 *   return <div>
 *            <button onClick={toggle}>Toggle menu</button>
 *
 *            <Drawer from="left" isOpen={isOpen}>
 *              <Menu />
 *            <Drawer />
 *          </div>
 *
 * }
 *
 *```
 *
 * If it's necessary to define where the drawer will be displayed, the NoContextDrawer component can be used.
 *
 */
declare const Drawer: React.FC<DrawerProps>;
export default Drawer;
