import React from 'react';
import { NotificationProps } from './types';
export declare enum NotificationTest {
    prefix = "Notification"
}
declare const Notification: React.FC<NotificationProps>;
export default Notification;
