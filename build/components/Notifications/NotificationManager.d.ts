import { SyntheticEvent } from 'react';
import { NotificationPosition, NotificationType } from './types';
declare class NotificationManager {
    private static instance;
    private timeout?;
    defaultPosition: NotificationPosition;
    defaultHook: string;
    defaultTimeout: number;
    private constructor();
    private createPortal;
    private removeCurrentNotification;
    private scheduleTaskToRemoveNotification;
    private removeScheduledTaskToRemoveNotification;
    private onCloseNotificationHandler;
    static getInstance(): NotificationManager;
    setDefaultPosition(position: NotificationPosition): void;
    setDefaultHook(defaultHook: string): void;
    setDefaultTimeout(ms: number): void;
    sendWithoutTimeout(title: string, message: string | JSX.Element, type?: NotificationType, icon?: JSX.Element, onClose?: (ev: SyntheticEvent) => void, position?: NotificationPosition): void;
    send(title: string, message: string | JSX.Element, type?: NotificationType, icon?: JSX.Element, position?: NotificationPosition, timeout?: number, onClose?: (ev: SyntheticEvent) => void): void;
}
export default NotificationManager;
