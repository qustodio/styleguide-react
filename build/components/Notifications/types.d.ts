import { SyntheticEvent } from 'react';
import { BaseComponentProps } from '../../common/types';
export declare type NotificationPosition = 'top-left' | 'top-right' | 'top-center' | 'bottom-left' | 'bottom-right' | 'bottom-center';
export declare type NotificationType = 'error' | 'success' | 'warning' | 'info';
export interface NotificationProps extends BaseComponentProps {
    message: string | JSX.Element;
    title: string;
    position?: NotificationPosition;
    type?: NotificationType;
    icon?: JSX.Element;
    onClose?: (ev: SyntheticEvent) => void;
}
