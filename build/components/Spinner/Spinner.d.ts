import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export type SpinnerSize = 'small' | 'medium' | 'large';
export type SpinnerColor = 'contrast' | 'regular';
export interface SpinnerProps extends BaseComponentProps {
    size: SpinnerSize;
    color: SpinnerColor;
}
export declare enum SpinnerTest {
    prefix = "Spinner"
}
declare const Spinner: React.FunctionComponent<SpinnerProps>;
export default Spinner;
