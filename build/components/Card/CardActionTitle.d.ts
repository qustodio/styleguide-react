import * as React from 'react';
import { BaseComponentProps, GlobalType } from '../../common/types';
import { IconType } from '../Icons';
export type CardActionTitleColor = 'inherit' | GlobalType.default | GlobalType.white | GlobalType.gray | GlobalType.primary | GlobalType.secondary | GlobalType.error;
export interface CardActionTitleProps extends BaseComponentProps {
    icon?: IconType | React.ReactNode;
    color?: CardActionTitleColor;
}
export declare enum CardActionTitleTest {
    prefix = "CardActionTitle"
}
declare const _default: React.NamedExoticComponent<CardActionTitleProps>;
export default _default;
