import * as React from 'react';
import { CardFooterProps } from './types/CardFooterProps.types';
declare const CardFooter: React.FunctionComponent<CardFooterProps>;
export default CardFooter;
export { CardFooterProps } from './types/CardFooterProps.types';
export { CardHeaderProps } from './types/CardHeaderProps.types';
