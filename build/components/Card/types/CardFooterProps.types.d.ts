export interface CardFooterProps {
    actionElement?: string | JSX.Element;
    testId?: string;
    onClickAction?: () => void;
}
