import { GlobalType } from '../../../common/types';
import { AlertBoxProps } from '../../AlertBox';
import { BannerProps } from '../../Banner/Banner';
export interface CardHeaderProps {
    title?: string | JSX.Element;
    type?: GlobalType;
    actionElement?: string | JSX.Element;
    border?: boolean;
    testId?: string;
    banner?: React.ReactElement<BannerProps> | React.ReactElement<AlertBoxProps>;
    wrapper?: ({ children, className, }: {
        children: React.ReactNode;
        className: string;
    }) => JSX.Element;
    onClickAction?: () => void;
}
