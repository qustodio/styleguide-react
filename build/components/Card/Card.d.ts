import * as React from 'react';
import { CardProps } from './types/CardProps.types';
declare const Card: React.FunctionComponent<CardProps>;
export default Card;
