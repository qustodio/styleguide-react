import * as React from 'react';
import { CardHeaderProps } from './types/CardHeaderProps.types';
declare const CardHeader: React.FunctionComponent<CardHeaderProps>;
export default CardHeader;
