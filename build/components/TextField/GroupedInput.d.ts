import * as React from 'react';
import { SyntheticEvent } from 'react';
import { DisabledProps, BaseComponentProps } from '../../common/types';
export type InputType = 'text' | 'password' | 'email' | 'tel' | 'number' | 'search';
export type InputIconConfiguration = {
    icon: JSX.Element;
    showIconWhen: InputShowIconWhen;
};
export declare enum InputShowIconWhen {
    always = 0,
    onError = 1,
    edit = 2
}
export declare enum GroupedInputTest {
    prefix = "GroupedInput",
    actionElement = "Action-element",
    input = "Input"
}
declare const GroupedInput: React.ForwardRefExoticComponent<DisabledProps & BaseComponentProps & {
    type: InputType;
    value?: string | undefined;
    id?: string | undefined;
    defaultValue?: string | undefined;
    error?: boolean | undefined;
    placeholder?: string | undefined;
    iconConfiguration?: InputIconConfiguration | undefined;
    autoComplete?: string | undefined;
    autoCorrect?: string | undefined;
    autoCapitalize?: string | undefined;
    spellCheck?: boolean | undefined;
    maxlength?: number | undefined;
    minlength?: number | undefined;
    required?: boolean | undefined;
    min?: number | undefined;
    max?: number | undefined;
    pattern?: string | undefined;
    name: string;
    iconLeft?: JSX.Element | undefined;
    onChange?: ((ev: React.SyntheticEvent, value: string) => void) | undefined;
    onBlur?: ((ev: React.SyntheticEvent) => void) | undefined;
    onDragStart?: ((ev: React.SyntheticEvent) => void) | undefined;
    onFocus?: ((ev: React.SyntheticEvent) => void) | undefined;
    onDrop?: ((ev: React.SyntheticEvent) => void) | undefined;
    onIconClick?: ((ev: React.SyntheticEvent) => void) | undefined;
} & React.RefAttributes<HTMLInputElement>>;
export default GroupedInput;
