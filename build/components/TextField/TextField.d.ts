import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
import { InfoFieldLabelLines } from '../Label/InfoFieldLabel';
import { InputType, InputIconConfiguration, InputShowIconWhen } from './GroupedInput';
export { InputType, InputIconConfiguration, InputShowIconWhen };
export type SupportTextConfiguration = {
    text: string;
    allowedLines?: InfoFieldLabelLines;
};
export declare enum TextFieldTest {
    prefix = "TextField",
    supportText = "Support_content"
}
export type TextInputSize = 'small' | 'regular';
export interface TextFieldProps extends BaseComponentProps, DisabledProps {
    id?: string;
    label?: string;
    block?: boolean;
    type: InputType;
    error?: boolean;
    supportTextConfiguration?: SupportTextConfiguration;
    placeholder?: string;
    autoComplete?: string;
    autoCorrect?: string;
    autoCapitalize?: string;
    spellCheck?: boolean;
    maxlength?: number;
    minlength?: number;
    required?: boolean;
    min?: number;
    max?: number;
    pattern?: string;
    name: string;
    value?: string;
    defaultValue?: string;
    canBeCleared?: boolean;
    showAlertOnError?: boolean;
    iconLeft?: JSX.Element;
    keepErrorLabelArea?: boolean;
    size?: TextInputSize;
    textCentered?: boolean;
    onIconClick?: (ev: React.SyntheticEvent) => void;
    onChange?: (ev: React.SyntheticEvent, value: string) => void;
    onBlur?: (ev: React.SyntheticEvent) => void;
    onDragStart?: (ev: React.SyntheticEvent) => void;
    onFocus?: (ev: React.SyntheticEvent) => void;
    onDrop?: (ev: React.SyntheticEvent) => void;
}
declare const TextField: React.ForwardRefExoticComponent<TextFieldProps & React.RefAttributes<HTMLInputElement>>;
export default TextField;
