import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export interface SwitchProps extends BaseComponentProps, DisabledProps {
    id?: string;
    className?: string;
    name?: string;
    checked?: boolean;
    disabled?: boolean;
    onClick?: (ev: React.SyntheticEvent) => void;
    onBlur?: (ev: React.SyntheticEvent) => void;
    onFocus?: (ev: React.SyntheticEvent) => void;
    onMouseDown?: (ev: React.SyntheticEvent) => void;
    onMouseUp?: (ev: React.SyntheticEvent) => void;
    onChange?: (ev: React.SyntheticEvent) => void;
}
export declare enum SwitchTest {
    prefix = "Switch",
    actionElement = "Action-element"
}
declare const Switch: React.FunctionComponent<SwitchProps>;
export default Switch;
