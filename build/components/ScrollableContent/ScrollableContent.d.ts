import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum ScrollableContentTest {
    prefix = "ScrollableContent",
    scroll = "Scroll"
}
export interface ScrollableContentProps extends BaseComponentProps {
    height?: number;
    hideScrollbar?: boolean;
}
declare const ScrollableContent: React.FunctionComponent<ScrollableContentProps>;
export default ScrollableContent;
