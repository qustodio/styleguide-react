import * as React from 'react';
import { TagProps } from './types/TagProps.types';
declare const Tag: React.FC<TagProps>;
export default Tag;
