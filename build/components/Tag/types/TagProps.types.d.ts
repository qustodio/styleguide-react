import { BaseComponentProps } from '../../../common/types';
import { IconType } from '../../Icons';
import { IconProps } from '../../Icons/types/IconProps.types';
import { TagSize } from './TagSize.types';
import { TagType } from './TagType.types';
export interface TagProps extends BaseComponentProps {
    text?: string;
    variant?: 'squared' | 'rounded';
    type?: TagType;
    iconType?: typeof IconType[keyof typeof IconType];
    iconProps?: IconProps;
    size?: TagSize;
    invertColor?: boolean;
    onClick?: (ev: React.SyntheticEvent) => void;
}
