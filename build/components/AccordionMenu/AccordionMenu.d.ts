import React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface AccordionMenuProps extends BaseComponentProps {
    isVisible: boolean;
}
export declare enum AccordionMenuTest {
    prefix = "AccordionMenu"
}
declare const AccordionMenu: React.FunctionComponent<AccordionMenuProps>;
export default AccordionMenu;
