import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum TextAreaTest {
    prefix = "TextArea"
}
export interface TextAreaProps extends BaseComponentProps {
    rows?: number;
    maxRows?: number;
    maxLength?: number;
    error?: boolean;
    disabled?: boolean;
    placeholder?: string;
    defaultValue?: string;
    helperText?: string;
    onChange?: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
}
declare const TextArea: React.FC<TextAreaProps>;
export default TextArea;
