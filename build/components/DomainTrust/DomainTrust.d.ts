import React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum DomainTrustTest {
    prefix = "DomainTrust"
}
export declare type DomainTrustReputation = 'unknown' | 'very-poor' | 'poor' | 'unsatisfactory' | 'good' | 'excellent';
export declare type DomainTrustMode = 'trustworthiness' | 'child-safety';
export interface DomainTrustProps extends BaseComponentProps {
    mode: DomainTrustMode;
    level: DomainTrustReputation;
    title: string;
    description: string;
}
declare const DomainTrust: React.FunctionComponent<DomainTrustProps>;
export default DomainTrust;
