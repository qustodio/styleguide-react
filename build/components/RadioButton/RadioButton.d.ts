import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export interface RadioButtonProps extends BaseComponentProps, DisabledProps {
    id: string;
    name?: string;
    checked?: boolean;
    block?: boolean;
    defaultChecked?: boolean;
    value: string;
    onClick?: (id: string) => void;
    onBlur?: (ev: React.SyntheticEvent) => void;
    onFocus?: (ev: React.SyntheticEvent) => void;
    onMouseDown?: (ev: React.SyntheticEvent) => void;
    onMouseUp?: (ev: React.SyntheticEvent) => void;
    onChange?: (ev: React.SyntheticEvent) => void;
}
export declare enum RadioButtonTest {
    prefix = "RadioButton"
}
declare const RadioButton: React.FunctionComponent<RadioButtonProps>;
export default RadioButton;
