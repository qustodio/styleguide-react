import * as React from 'react';
import { ColorPickerProps } from './types/ColorPickerProps.types';
declare const ColorPicker: React.FC<ColorPickerProps>;
export default ColorPicker;
