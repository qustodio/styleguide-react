import React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface InstructionBlockProps extends BaseComponentProps {
    block: boolean;
}
export declare enum InstructionBlockTest {
    prefix = "InstructionBlock"
}
declare const _default: React.NamedExoticComponent<InstructionBlockProps>;
export default _default;
