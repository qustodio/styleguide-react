import React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface StyledInstructionProps extends BaseComponentProps {
    block: boolean;
    color?: 'primary' | 'secondary' | 'error' | 'warning' | 'success';
}
export declare enum StyledInstructionTest {
    prefix = "StyledInstruction"
}
declare const _default: React.NamedExoticComponent<StyledInstructionProps>;
export default _default;
