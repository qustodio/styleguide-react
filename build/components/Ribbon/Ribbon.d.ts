import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum RibbonType {
    cornerRight = 0,
    cornerLeft = 1
}
export interface RibbonProps extends BaseComponentProps {
    type: RibbonType;
}
export declare enum RibbonTest {
    prefix = "Ribbon"
}
declare const Ribbon: React.FunctionComponent<RibbonProps>;
export default Ribbon;
