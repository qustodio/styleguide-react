import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum ExpandableCardTest {
    prefix = "ExpandableCard"
}
interface ExpandableCardVerticalProps extends BaseComponentProps {
    opener: boolean;
    height: string;
}
interface ExpandableCardProps extends ExpandableCardVerticalProps {
    mode: 'horizontal' | 'vertical';
}
declare const ExpandableCard: React.FC<ExpandableCardProps>;
export default ExpandableCard;
