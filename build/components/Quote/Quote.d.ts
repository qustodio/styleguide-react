import React from 'react';
import { BaseComponentProps } from '../../common/types';
export type QuoteMaxLines = 'none' | 'one' | 'two' | 'three' | 'four' | 'five';
export interface QuoteProps extends BaseComponentProps {
    maxLines?: QuoteMaxLines;
}
export declare enum QuoteTest {
    prefix = "Quote"
}
declare const _default: React.NamedExoticComponent<QuoteProps>;
export default _default;
