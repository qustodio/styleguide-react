import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface StepBarProp extends BaseComponentProps {
    activeStep: number;
    steps: string[];
    hideLabels?: boolean;
}
export declare enum StepBarTest {
    prefix = "StepBar"
}
declare const StepBar: React.FunctionComponent<StepBarProp>;
export default StepBar;
