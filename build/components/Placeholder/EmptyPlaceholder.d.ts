import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { LayoutContentSpacing } from '../Layout/types';
export interface EmptyPlaceholderProps extends BaseComponentProps {
    text: string;
    icon: JSX.Element;
    coloredBackground: boolean;
    centered: boolean;
    fillAvailableSpace?: boolean;
    maxWidth: LayoutContentSpacing;
    smallText?: boolean;
}
export declare enum EmptyPlaceholderTest {
    prefix = "EmptyPlaceholder"
}
declare const _default: React.NamedExoticComponent<EmptyPlaceholderProps>;
export default _default;
