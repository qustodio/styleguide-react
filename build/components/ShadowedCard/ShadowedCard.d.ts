import React from 'react';
import { RckShadow } from '../../common/shadow.types';
import { BaseComponentProps } from '../../common/types';
export interface ShadowedCardProps extends BaseComponentProps {
    shadow?: RckShadow;
    children?: React.ReactNode;
}
export declare enum ShadowedCardTest {
    prefix = "ShadowedCard"
}
declare const ShadowedCard: {
    ({ testId, shadow, children, }: ShadowedCardProps): JSX.Element;
    displayName: string;
};
export default ShadowedCard;
