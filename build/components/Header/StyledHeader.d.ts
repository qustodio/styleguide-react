import React from 'react';
import { BaseComponentProps } from '../../common/types';
export type StyledHeaderType = 'h1' | 'h2' | 'h3' | 'h4' | 'h5';
export interface StyledHeaderProps extends BaseComponentProps {
    type?: StyledHeaderType;
}
export declare enum StyledHeaderTest {
    prefix = "StyledHeader"
}
declare const _default: React.NamedExoticComponent<StyledHeaderProps>;
export default _default;
