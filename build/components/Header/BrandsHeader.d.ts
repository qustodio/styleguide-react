import React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum BrandsHeaderTest {
    prefix = "BrandsHeader"
}
declare const _default: React.NamedExoticComponent<BaseComponentProps>;
export default _default;
