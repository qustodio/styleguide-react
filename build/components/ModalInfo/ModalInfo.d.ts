import * as React from 'react';
import { ModalProps } from '../Modal/Modal';
import { AlertBoxType } from '../AlertBox';
export interface ModalInfoProps extends Omit<ModalProps, 'fixedContent'> {
    addCancelButton?: boolean;
    cancelButtonText?: string;
    alertBoxText?: string;
    alertBoxType?: AlertBoxType;
}
declare const ModalInfo: React.FC<ModalInfoProps>;
export default ModalInfo;
