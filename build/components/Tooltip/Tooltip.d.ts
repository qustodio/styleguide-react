import * as React from 'react';
import { TooltipProps } from './types/Tooltip.types';
/**
 * Tooltip
 *
 * @example
 *```tsx
 * import { Tooltip } from 'styleguide-react';
 *
 * <Tooltip title="Title" label="label" placement="top">
 *      <span>Open Tooltip</span>
 * </Tooltip>
 *```
 */
declare const Tooltip: React.ForwardRefExoticComponent<TooltipProps & React.RefAttributes<Element>>;
export default Tooltip;
