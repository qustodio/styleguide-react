import * as React from 'react';
import { TooltipProps } from './types/Tooltip.types';
export declare const getTooltipContent: (label: React.ReactNode, title: string | undefined) => JSX.Element;
export declare const getTooltipTransitionClass: (transition: TooltipProps['smoothTransition']) => string;
export declare const generateGetBoundingClientRect: (x?: number, y?: number) => () => ClientRect | DOMRect;
