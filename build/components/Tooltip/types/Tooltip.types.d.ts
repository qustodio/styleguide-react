import { BaseComponentProps } from '../../../common/types';
import { PopoverProps } from '../../../hooks/usePopover';
export interface TooltipProps extends Pick<PopoverProps, 'offset' | 'gutter' | 'preventOverflow' | 'clipOverflow' | 'flip' | 'matchWidth' | 'boundary' | 'arrowPadding' | 'strategy' | 'placement' | 'modifiers'>, BaseComponentProps {
    /**
     * If not specified, a new random ID is generated each time the tooltip becomes visible.
     *
     * @default `<TooltipTest.prefix>__<unix_timestamp>`
     */
    id?: string;
    triggerClassName?: string;
    children?: React.ReactNode;
    /**
     * Tooltip label
     */
    label: React.ReactNode;
    /**
     * Tooltip title
     */
    title?: string;
    /**
     * Whether the popper.js should show the arrow.
     * @default true
     */
    hasArrow?: boolean;
    /**
     * Whether the Tooltip is enabled or not.
     * @default false
     */
    isDisabled?: boolean;
    /**
     * Whether the Tooltip is always visible.
     * @default false
     */
    isOpen?: boolean;
    /**
     * If `true`, will use pointer events for enter and leaving the trigger.
     * @default false
     */
    isMobile?: boolean;
    /**
     * @default false
     */
    smoothTransition?: boolean | (100 | 200 | 300 | 400 | 500);
    /**
     * If this prop is set, a VirtualElement is created and the coordinates
     * are obtained from coords prop.
     */
    coords?: {
        x: number;
        y: number;
    };
    /**
     * If `true`, will keep the portal mounted through-out the lifecycle of the toolip.
     *
     * ---
     * ### Important!
     * You _MUST_ provide an `id`, otherwise the trigger and the tooltip will not be
     * associated for screen readers.
     *
     * @default false
     */
    keepMounted?: boolean;
    /**
     * If `true`, Tooltip will NOT be shown on hover (onMouseEnter/onMouseLeave) or touch (onTouchStart/onTouchEnd).
     * @default false
     */
    disableForHoverTouch?: boolean;
    /**
     * If `true`, Tooltip will NOT be shown on focus and will not hide on blur.
     * @default false
     */
    disableForFocusBlur?: boolean;
    /**
     * Indicates that an element will be updated, and describes the types of updates the user agents, assistive
     * technologies, and user can expect from the live region.
     *
     * @default 'off'
     * */
    'aria-live'?: 'off' | 'assertive' | 'polite';
}
