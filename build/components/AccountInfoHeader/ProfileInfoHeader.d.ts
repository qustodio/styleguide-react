import React from 'react';
import { BaseComponentProps } from '../../common/types';
import { AvatarProps } from '../Avatar/Avatar.types';
import { DropdownProps } from '../Dropdown/Dropdown';
export type ProfileStatus = 'active' | 'no-device' | 'offline' | 'standby' | 'unknown' | 'tampered-device';
export type ProfileInfoHeaderSize = 'regular' | 'small';
export interface ProfileInfoHeaderProps extends BaseComponentProps {
    name: string;
    avatar: React.ReactElement<AvatarProps>;
    status?: ProfileStatus;
    statusMessage?: string;
    size?: ProfileInfoHeaderSize;
    actionElement?: React.ReactElement<DropdownProps>;
    onEdit?: () => void;
}
export declare enum ProfileInfoHeaderTest {
    prefix = "ProfileInfoHeader"
}
declare const _default: React.NamedExoticComponent<ProfileInfoHeaderProps>;
export default _default;
