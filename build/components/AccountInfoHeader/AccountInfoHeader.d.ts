import React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface AccountInfoHeaderProps extends BaseComponentProps {
    name: string;
    logoImage: JSX.Element;
    showGift?: boolean;
    expand: boolean;
    onClickGift: () => void;
}
export declare enum AccountInfoHeaderTest {
    prefix = "AccountInfoHeader"
}
declare const _default: React.NamedExoticComponent<AccountInfoHeaderProps>;
export default _default;
