import React, { SyntheticEvent } from 'react';
import { BaseComponentProps, GlobalType } from '../../common/types';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
export declare enum ActionInputIconPosition {
    left = "left",
    right = "right"
}
export declare enum ActionInputButtonType {
    button = "button",
    submit = "submit"
}
export interface ActionInputProps extends BaseComponentProps {
    /** @deprecated use iconLeft or iconRight instead */
    icon?: JSX.Element;
    /** @deprecated use iconLeft or iconRight instead */
    iconPosition?: ActionInputIconPosition;
    iconLeft?: JSX.Element;
    iconRight?: JSX.Element;
    iconWithColor?: boolean;
    placeholder?: string;
    text?: string;
    block?: boolean;
    href?: string;
    buttonType: ActionInputButtonType;
    useAsLink?: React.FunctionComponent<AnchorPropTypes>;
    ariaLabel?: string;
    error?: boolean;
    disabled?: boolean;
    onClick?: (ev: SyntheticEvent) => void;
    onKeyDown?: (ev: SyntheticEvent) => void;
    /** @deprecated use backgroundColor and textColor instead */
    color?: 'primary';
    backgroundColor?: GlobalType;
    textColor?: GlobalType;
    compact?: boolean;
}
export declare enum ActionInputTest {
    prefix = "ActionInput",
    label = "ActionInput__label"
}
declare const _default: React.NamedExoticComponent<ActionInputProps>;
export default _default;
