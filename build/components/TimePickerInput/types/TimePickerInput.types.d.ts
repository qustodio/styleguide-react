import { BaseComponentProps, DisabledProps } from '../../../common/types';
export interface TimePickerInputProps extends DisabledProps, BaseComponentProps {
    name?: string;
    value?: string;
    readOnly?: boolean;
    hasError?: boolean;
    disabled?: boolean;
    /** @default "HH:mm" */
    placeholder?: string;
    onChange?: (value: string, event: React.SyntheticEvent<Element, Event>) => void;
    onClick?: (event: React.SyntheticEvent<Element, Event>) => void;
    onBlur?: (event: React.SyntheticEvent<Element, Event>) => void;
    onFocus?: (event: React.SyntheticEvent<Element, Event>) => void;
    /** click handler for right content clock icon */
    onClickTrigger?: (event: React.SyntheticEvent<Element, Event>) => void;
}
