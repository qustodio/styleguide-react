import * as React from 'react';
import { TimePickerInputProps } from './types/TimePickerInput.types';
declare const _default: React.NamedExoticComponent<TimePickerInputProps>;
export default _default;
