import * as React from 'react';
import { DurationInputProps } from './types/DurationInput.types';
declare const DurationInput: React.FunctionComponent<DurationInputProps>;
export default DurationInput;
