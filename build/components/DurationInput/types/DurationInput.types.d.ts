import React from 'react';
import { BaseComponentProps } from '../../../common/types';
export type DurationInputType = 'hours' | 'minutes';
export interface DurationInputProps extends BaseComponentProps {
    /** Initial value in minutes */
    value: number;
    /** @default 1440 */
    maxMinutes?: number;
    labels?: {
        /** @default "h" */
        hours: React.ReactNode;
        /** @default "m" */
        minutes: React.ReactNode;
    };
    hasError?: boolean;
    disabled?: boolean;
    /** Hours or minutes input change event handler */
    onChange: (value: number, event: React.SyntheticEvent<Element, Event>, inputType: DurationInputType) => void;
}
