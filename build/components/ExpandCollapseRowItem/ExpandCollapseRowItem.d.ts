interface ExpandCollapseRowItemProps {
    iconSrc?: string;
    title: string;
    subtitle: JSX.Element;
    rightElement: JSX.Element;
    className?: string;
    testId?: string;
    isDisabled?: boolean;
    onInfoActionClick?: () => void;
}
export declare enum ExpandableRowItemTest {
    prefix = "Expandable-row-item",
    title = "Title",
    rightElement = "Right-element"
}
declare const ExpandCollapseRowItem: ({ iconSrc, title, subtitle, rightElement, className, testId, isDisabled, onInfoActionClick, }: ExpandCollapseRowItemProps) => JSX.Element;
export default ExpandCollapseRowItem;
