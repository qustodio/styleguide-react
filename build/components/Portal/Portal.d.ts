import * as React from 'react';
export interface PortalProps {
    name: string;
}
declare class Portal extends React.Component<PortalProps> {
    portalContainer: HTMLElement | null;
    portal: HTMLDivElement;
    componentDidMount(): void;
    componentWillUnmount(): void;
    createPortalContainer: () => HTMLElement | undefined;
    render(): JSX.Element;
}
export default Portal;
