import { BaseComponentProps } from '../../common/types';
export interface AccordionItemProps extends BaseComponentProps {
    key: string;
    title: string;
    content: string | JSX.Element;
    isOpen?: boolean;
    icon: string;
    onClick?: () => void;
}
export declare enum AccordionItemTest {
    prefix = "AccordionItem"
}
declare const AccordionItem: {
    ({ title, content, isOpen, icon, onClick, testId, }: AccordionItemProps): JSX.Element;
    displayName: string;
};
export default AccordionItem;
