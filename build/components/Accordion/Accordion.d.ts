import { RckBaseColor } from '../../common/color.types';
import { AccordionItemProps } from './AccordionItem';
import { BaseComponentProps } from '../../common/types';
export interface AccordionProps extends BaseComponentProps {
    color?: RckBaseColor;
    highlightOnOpen?: boolean;
    maxColumns?: 1 | 2 | 3;
    items: Array<AccordionItemProps>;
}
export declare enum AccordionTest {
    prefix = "Accordion"
}
declare const Accordion: {
    ({ testId, color, highlightOnOpen, maxColumns, items, }: AccordionProps): JSX.Element;
    displayName: string;
};
export default Accordion;
