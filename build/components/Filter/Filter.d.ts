import * as React from 'react';
import { FilterProps } from './types/FilterProps.types';
declare const Filter: React.FC<FilterProps>;
export default Filter;
