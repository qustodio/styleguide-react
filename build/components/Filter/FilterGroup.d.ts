import * as React from 'react';
import { FilterGroupProps } from './types/FilterGroupProps.types';
/**
 *
 * Component to wrap filters into a group a gives them spacing and the option to pass `active` prop and `onClick` prop for all Filter children.
 *
 * Behavior
 *
 * - It only allows `Filter` component as child `(1..n)`.
 * - If `active` prop is provided, you have to use `id` prop in `Filter` component.
 * - If `onClick` prop is provided, you cannot use `onClick` prop in `Filter` component
 *
 * @example
 *```tsx
 * import { FilterGroup, Filter } from 'styleguide-react';
 *
 * const Component = () => {
 *   const [active, setActive] = React.useState('1');
 *   const onClick = (id, _e) => {
 *     setActive(id);
 *   };
 *
 *   return <FilterGroup active={active} onClick={onClick} size="regular">
 *      <Filter id="1" text="Filter 1" />
 *      <Filter id="2" text="Primary" type="primary" />
 *      <Filter id="3" text="Error" type="error" />
 *      <Filter id="4" text="Secondary" type="secondary" />
 *      <Filter id="5" text="Filter 5" />
 *    </FilterGroup>
 * }
 *```
 */
declare const FilterGroup: React.FC<FilterGroupProps>;
export default FilterGroup;
