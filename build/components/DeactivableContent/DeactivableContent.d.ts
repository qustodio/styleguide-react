import React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export declare enum DeactivableContentTest {
    prefix = "DeactivableListItem"
}
export interface DeactivableContentProps extends DisabledProps, BaseComponentProps {
}
declare const DeactivableContent: React.FunctionComponent<DeactivableContentProps>;
export default DeactivableContent;
