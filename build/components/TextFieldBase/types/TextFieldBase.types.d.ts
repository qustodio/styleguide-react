type InputType = 'text' | 'password' | 'email' | 'tel' | 'number' | 'search';
type TextInputSize = 'small' | 'regular';
type TextInputMaxWidth = 'small' | 'regular' | 'block' | 'hug-content';
export interface TextFieldBaseProps extends Omit<React.AllHTMLAttributes<HTMLInputElement>, 'disabled' | 'size' | 'autoFocus' | 'children'> {
    name?: string;
    className?: string;
    testId?: string;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    contentLeft?: JSX.Element;
    contentRight?: JSX.Element;
    /** @default "text" */
    type?: InputType;
    /** @default "small" */
    size?: TextInputSize;
    /** @default "block" */
    width?: TextInputMaxWidth;
    hasError?: boolean;
    /** Allows to show error independently for the input text and the border */
    errorConfig?: {
        /** @default true */
        border?: boolean;
        /** @default true */
        text?: boolean;
    };
    /**
     * Autofocuses the input when input component is mounted.
     * @default false
     * */
    autoFocus?: boolean;
    /**
     * Highlights the contents of the input on focus
     * @default false
     * */
    selectAllOnFocus?: boolean;
    /**
     * - Prop to use a custom JSX element to replace the input element.
     * - If you pass a function, you will recive the current `value` as param but not the `defaultValue`.
     * */
    children?: React.ReactNode;
}
export {};
