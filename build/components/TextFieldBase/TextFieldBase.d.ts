import * as React from 'react';
import { TextFieldBaseProps } from './types/TextFieldBase.types';
/**
 * __TextFieldBase__
 *
 * A text field is an input that allows a user to write or edit text.
 *
 * - Use this component to create others that interit from TextField styles.
 * - Do not export this component to the end user, it is meant for internal use.
 *
 * You can use the following data selector for customizations:
 * - `[data-rck-input-base--container]`
 * - `[data-rck-input-base--input]`
 * -  __Areas:__
 *      - `[data-area--left]`
 *      - `[data-area--body-wrapper]`
 *      - `[data-area--right]`
 * -  __States:__
 *      - `[data-disabled]`
 *      - `[data-error]`
 *
 * @example
 * ```scss
 * .my-component {
 *      & > [data-rck-input-base--container] {
 *           &:focus-within: { ... }
 *      }
 * }
 * ```
 */
declare const _default: React.NamedExoticComponent<TextFieldBaseProps & React.RefAttributes<HTMLInputElement>>;
export default _default;
