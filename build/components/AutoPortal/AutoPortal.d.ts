import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum AutoPortalTest {
    prefix = "AutoPortal"
}
interface PortalProps extends BaseComponentProps {
    id: string;
    className?: string;
}
/**
 *
 * Portal that uses `document.body` as container
 *
 * Behavior
 *
 * 1. Portals are added as div chid of body.
 * 2. Must define id.
 *
 *
 * @example
 *
 *```tsx
 * import { AutoPortal } from 'styleguide-react';
 * import { Modal } from '../components/Modal'
 *
 * const Modal: React.FC<ModalProps> = ({ children }) => {
 *   return <AutoPortal id="global-modal" className="custom-modal">
 *           <Modal>
 *            {children}
 *           </Modal>
 *          </AutoPortal>
 *
 * }
 *
 *```
 *
 */
declare const AutoPortal: React.FC<PortalProps>;
export default AutoPortal;
