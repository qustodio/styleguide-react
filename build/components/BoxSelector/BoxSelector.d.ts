import { BoxSelectorItemProps } from './BoxSelectorItem';
import { BaseComponentProps } from '../../common/types';
export interface BoxSelectorProps extends BaseComponentProps {
    fieldGroupName: string;
    handleInputCheckedChange: (value: string, checked: boolean) => void;
    items: Array<Omit<BoxSelectorItemProps, 'fieldGroupName' | 'handleChange'>>;
}
export declare enum BoxSelectorTest {
    prefix = "BoxSelector"
}
declare const BoxSelector: {
    ({ testId, handleInputCheckedChange, fieldGroupName, items, }: BoxSelectorProps): JSX.Element;
    displayName: string;
};
export default BoxSelector;
