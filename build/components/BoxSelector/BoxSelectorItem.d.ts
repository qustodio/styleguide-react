import { BaseComponentProps } from '../../common/types';
export interface BoxSelectorItemProps extends BaseComponentProps {
    fieldGroupName: string;
    value: string | number | readonly string[] | undefined;
    label: string;
    isSelected: boolean;
    isDisabled: boolean;
    icon: string;
    handleChange: (value: string, checked: boolean) => void;
}
export declare enum BoxSelectorItemTest {
    prefix = "BoxSelectorItem"
}
declare const BoxSelectorItem: {
    ({ label, icon, fieldGroupName, value, isSelected, isDisabled, handleChange, testId, }: BoxSelectorItemProps): JSX.Element;
    displayName: string;
};
export default BoxSelectorItem;
