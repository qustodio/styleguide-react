import React, { SyntheticEvent } from 'react';
export interface MoreButtonProps {
    onClick: (ev: SyntheticEvent) => void;
    className?: string;
}
declare const MoreActionsButton: React.FunctionComponent<MoreButtonProps>;
export default MoreActionsButton;
