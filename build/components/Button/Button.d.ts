import * as React from 'react';
import { BaseComponentProps, DisabledProps } from '../../common/types';
export declare enum ButtonType {
    primary = "primary",
    secondary = "secondary",
    plain = "plain"
}
export declare enum ButtonSize {
    small = "small",
    medium = "medium",
    big = "big"
}
export declare enum ButtonColor {
    secondary = "secondary",
    error = "error",
    white = "white",
    wellbeing = "wellbeing"
}
export declare enum ButtonIconPosition {
    left = "left",
    right = "right"
}
export interface ButtonProps extends BaseComponentProps, DisabledProps {
    label?: string;
    htmlType?: 'button' | 'reset' | 'submit';
    buttonType?: ButtonType;
    color?: ButtonColor;
    loading?: boolean;
    block?: boolean;
    form?: string;
    centered?: boolean;
    iconPosition?: ButtonIconPosition;
    icon?: JSX.Element;
    size?: ButtonSize;
    onClick?: () => void;
}
export declare enum ButtonTest {
    prefix = "Button"
}
declare const _default: React.NamedExoticComponent<ButtonProps>;
export default _default;
