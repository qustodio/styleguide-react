import React, { SyntheticEvent } from 'react';
import { DisabledProps, BaseComponentProps } from '../../common/types';
import { IconSize } from '../Icons';
export interface MoreButtonProps extends BaseComponentProps, DisabledProps {
    onClick: (ev: SyntheticEvent) => void;
    size?: IconSize;
}
export declare enum MoreActionsButtonTest {
    prefix = "MoreActionsButton"
}
declare const _default: React.NamedExoticComponent<MoreButtonProps>;
export default _default;
