import React, { SyntheticEvent } from 'react';
import { DisabledProps, BaseComponentProps } from '../../common/types';
export interface ActionableContentProps extends BaseComponentProps, DisabledProps {
    onClick: (ev: SyntheticEvent) => void;
    fillAvailableSpace?: boolean;
}
export declare enum ActionableContentTest {
    prefix = "ActionableContent"
}
declare const ActionableContent: React.FunctionComponent<ActionableContentProps>;
export default ActionableContent;
