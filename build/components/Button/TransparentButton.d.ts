import React, { SyntheticEvent } from 'react';
import { ButtonAriaProps } from '../../common/aria.types';
import { BaseComponentProps } from '../../common/types';
export interface TransparentButtonProps extends BaseComponentProps, ButtonAriaProps {
    onClick?: (ev: SyntheticEvent) => void;
    fillAvailableSpace?: boolean;
    htmlType?: 'button' | 'reset' | 'submit';
    /** Displays the element as an inline element */
    inline?: boolean;
    /**
     * A boolean attribute specifies that the button should have input focus when the page loads, or when
     * a <dialog> is opened.
     *
     * __Only one element in a document can have this attribute.__
     * */
    autoFocus?: boolean;
    /**
     * This props does not use the native <button> `disabled` prop but instead uses `aria-disabled.`
     * When true, it does not register any keyboard interaction but still allows keyboard focus for a11y.
     * @see https://css-tricks.com/making-disabled-buttons-more-inclusive/
     * */
    disabled?: boolean;
}
export declare enum TransparentButtonTest {
    prefix = "TransparentButton"
}
declare const _default: React.MemoExoticComponent<React.ForwardRefExoticComponent<TransparentButtonProps & React.RefAttributes<HTMLButtonElement>>>;
export default _default;
