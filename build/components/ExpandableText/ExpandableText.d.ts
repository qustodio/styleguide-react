import { BaseComponentProps, TestableComponentProps } from '../../common/types';
interface ExpandableTextProps extends BaseComponentProps, TestableComponentProps {
    maxLines?: number;
    expandText: string;
    collapseText: string;
    isTextExpanded?: boolean;
    onToggle?: () => void;
}
export declare enum ExpandableTextTest {
    prefix = "Expandable-text",
    content = "Content"
}
declare const ExpandableText: ({ children, maxLines, expandText, collapseText, className, testId, isTextExpanded, onToggle, }: ExpandableTextProps) => JSX.Element;
export default ExpandableText;
