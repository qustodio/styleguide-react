import { ButtonProps } from '../Button/Button';
export type FixedCtaCardBackgroundColor = 'brand-primary-300' | 'brand-primary-600' | 'gradient-brand';
export type FixedCtaCardType = {
    title: string;
    description?: string;
    backgroundColor: FixedCtaCardBackgroundColor;
    button: ButtonProps;
};
declare const FixedCtaCard: {
    ({ title, description, button, backgroundColor, }: FixedCtaCardType): JSX.Element;
    displayName: string;
};
export default FixedCtaCard;
