import { BaseComponentProps } from '../../../common/types';
import { LayoutSpacing } from '../../Layout/types';
import { TypographyColor } from './TypographyColor.types';
import { TypographyType } from './TypographyType.types';
import { TypographyWeight } from './TypographyWeight.types';
export interface TypographyProps extends BaseComponentProps {
    type: TypographyType;
    weight?: TypographyWeight;
    color?: TypographyColor;
    align?: 'center' | 'left' | 'right';
    italic?: boolean;
    /** @deprecated use `renderAs` prop instead */
    component?: keyof React.ReactHTML;
    renderAs?: keyof React.ReactHTML | React.ReactNode;
    marginTop?: LayoutSpacing;
    marginBottom?: LayoutSpacing;
    /**
     * If `true`, the text will not wrap, but instead will truncate with a text overflow ellipsis.
     * Note that text overflow can only happen with block or inline-block level elements (the element needs to have a width in order to overflow).
     */
    noWrap?: boolean;
}
