export type TypographyColor = 'gray' | 'gray-dark' | 'gray-semi' | 'gray-light' | 'gray-lighter' | 'error' | 'warning' | 'success' | 'white' | 'brand';
