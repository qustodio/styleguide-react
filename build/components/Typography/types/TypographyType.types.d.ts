/** @deprecated retro compatibility */
export type DeprecatedTypographyType = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'caption2-quote';
export type TypographyType = 'headline1' | 'headline2' | 'title1' | 'title2' | 'title3' | 'body1' | 'body2' | 'caption1' | 'caption2' | DeprecatedTypographyType;
