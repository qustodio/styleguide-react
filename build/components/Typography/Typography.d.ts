import * as React from 'react';
import { TypographyProps } from './types/TypographyProps.types';
declare const Typography: React.FC<TypographyProps>;
export default Typography;
