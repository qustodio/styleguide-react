export { default } from './Typography';
export { TypographyColor } from './types/TypographyColor.types';
export { TypographyProps } from './types/TypographyProps.types';
export { TypographyType } from './types/TypographyType.types';
export { TypographyWeight } from './types/TypographyWeight.types';
export { default as TypographyTest } from './types/TypographyTest.types';
