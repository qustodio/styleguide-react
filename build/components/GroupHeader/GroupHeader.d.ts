import React from 'react';
import { AnchorPropTypes } from '../../adapters/router/Anchor';
import { BaseComponentProps } from '../../common/types';
export interface GroupHeaderProps extends BaseComponentProps {
    onClickHelp: () => void;
    helpUrl?: string;
    useAsLink?: React.FunctionComponent<AnchorPropTypes>;
}
export declare enum GroupHeaderTest {
    prefix = "GroupHeader"
}
declare const _default: React.NamedExoticComponent<GroupHeaderProps>;
export default _default;
