import * as React from 'react';
import { BaseComponentProps } from '../../common/types';
export interface AffiliateBadgeProps extends BaseComponentProps {
    iconSrc: string;
    description?: string;
}
export declare enum AffiliateBadgeTest {
    prefix = "AffiliateBadge"
}
declare const _default: React.NamedExoticComponent<AffiliateBadgeProps>;
export default _default;
