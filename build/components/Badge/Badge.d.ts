import React from 'react';
import { BaseComponentProps } from '../../common/types';
export declare enum BadgeType {
    error = "error",
    warning = "warning",
    neutral = "neutral",
    brand = "brand",
    secondary = "secondary",
    success = "success",
    dark = "dark",
    contrast = "contrast",
    school = "school"
}
export type BadgeSize = 'regular' | 'small';
export interface BadgeProps extends BaseComponentProps {
    text: string;
    type: BadgeType;
    size?: BadgeSize;
}
export declare enum BadgeTest {
    prefix = "Badge"
}
declare const _default: React.NamedExoticComponent<BadgeProps>;
export default _default;
