/// <reference types="react" />
declare const useBubbleScrollEventToWindow: <T extends HTMLElement>(el: import("react").RefObject<T>, enabled: boolean) => void;
export default useBubbleScrollEventToWindow;
