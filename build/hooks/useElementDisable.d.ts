import { MutableRefObject } from 'react';
declare const useElementDisable: <T extends HTMLElement>(el: MutableRefObject<T>, disable: boolean, disableClass: string, overlapClass: string) => void;
export default useElementDisable;
