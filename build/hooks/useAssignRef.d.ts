import { MutableRefObject, Ref } from 'react';
type RefType<T> = MutableRefObject<T | null> | ((instance: T | null) => void) | null | undefined;
export declare const assignRefs: <T>(...refs: RefType<T>[]) => (node: T | null) => void;
declare const useAssignRef: <T>(...refs: (Ref<T> | undefined)[]) => ((instance: T | null) => void) | null;
export default useAssignRef;
