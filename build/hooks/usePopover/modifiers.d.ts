import { Modifier } from '@popperjs/core';
export declare const matchToTriggerWidth: Modifier<'matchToTriggerWidth', Record<string, unknown>>;
export declare const positionArrow: Modifier<'positionArrow', Record<string, unknown>>;
export declare const hidePopper: Modifier<'hidePopper', Record<string, unknown>>;
