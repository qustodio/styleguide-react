import { EventListenersModifier } from '@popperjs/core/lib/modifiers/eventListeners';
export declare const ARROW_SIZE_CSS_VAR = "--popper-arrow-size";
export declare const ARROW_OFFSET_CSS_VAR = "--popper-arrow-offset";
export declare const ARROW_SHADOW_CSS_VAR = "--popper-arrow-shadow-color";
export declare const ARROW_BG_CSS_VAR = "--popper-arrow-bg";
export declare const DATA_POPPER_ARROW_ATTR = "data-popper-arrow";
export declare const DATA_POPPER_HIDDEN_ATTR = "data-popper-reference-hidden";
export declare const getEventListenersModifier: (value?: boolean | EventListenersModifier['options']) => Partial<EventListenersModifier>;
export declare const getArrowStyle: (props: {
    size: number;
    shadowColor: string;
    bg: string;
    style: Record<string, unknown>;
}) => Record<string, unknown>;
export declare const toVar: (cssVar: string, defaultValue?: string) => string;
