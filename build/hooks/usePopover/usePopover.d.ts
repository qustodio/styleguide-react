import { Instance, VirtualElement } from '@popperjs/core';
import { PopoverProps } from './types/PopoverProps.types';
declare const usePopover: (props?: PopoverProps) => {
    update(): void;
    forceUpdate(): void;
    instance: Instance | null;
    targetRef: <T extends Element | VirtualElement>(node: T | null) => void;
    popperRef: <T_1 extends HTMLElement>(node: T_1 | null) => void;
    getReferenceProps: (refProps?: any, ref?: any) => any;
    getPopperProps: (popperProps?: any, ref?: any) => any;
    getArrowProps: (arrowProps?: any, ref?: any) => any;
};
export default usePopover;
