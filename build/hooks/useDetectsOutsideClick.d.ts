import { MutableRefObject } from 'react';
declare const useDetectOutsideClick: <T extends HTMLElement>(elements: MutableRefObject<T>[], initialState: boolean) => readonly [boolean, (active: boolean) => void, boolean];
export default useDetectOutsideClick;
