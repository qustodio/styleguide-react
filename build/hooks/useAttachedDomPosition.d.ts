import { MutableRefObject } from 'react';
import { DropdownElementOrientation } from '../components/Dropdown/types';
import { FloatingMenuPlacement } from './useFloatingPlacement';
declare const useAttachedDomPosition: <T extends HTMLElement>(el: MutableRefObject<T>, elToAttach: MutableRefObject<HTMLElement>, placement: FloatingMenuPlacement, actionElementPosition: DropdownElementOrientation, marginFromAttached?: number, initialTop?: number, initialLeft?: number) => readonly [React.CSSProperties, () => void];
export default useAttachedDomPosition;
