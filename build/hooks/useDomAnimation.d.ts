import { MutableRefObject } from 'react';
declare const useDomAnimation: <T extends HTMLElement>(el: MutableRefObject<T>, animationClass: string, milliseconds?: number, trigger?: unknown[] | undefined, disabled?: boolean) => void;
export default useDomAnimation;
