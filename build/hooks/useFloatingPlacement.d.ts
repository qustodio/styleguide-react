import { MutableRefObject } from 'react';
export declare enum FloatingMenuPlacement {
    top = "top",
    bottom = "bottom"
}
declare const useFloatingPlacement: <T extends HTMLElement>(el: MutableRefObject<T>, initialPlacement: FloatingMenuPlacement, isActive: boolean, fixed?: boolean) => readonly [FloatingMenuPlacement, () => void];
export default useFloatingPlacement;
