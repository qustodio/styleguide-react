declare const useToggle: (initial: boolean) => [
    boolean,
    {
        toggle: () => void;
        open: () => void;
        close: () => void;
    }
];
export default useToggle;
