/**
 * Extracts ARIA attributes from a props object.
 *
 * @example
 * const ariaProps = useAriaProps({
 *   'aria-label': 'test',
 *   'aria-describedby': 'description',
 *   className: 'text-red' // Will be filtered out
 * });
 * // Returns { 'aria-label': 'test', 'aria-describedby': 'description' }
 */
declare const useAriaProps: <T extends Record<string, unknown>>(props: T) => Record<keyof T, any>;
export default useAriaProps;
