declare const usePortal: (id: string, useSingleHook?: boolean) => HTMLElement;
export default usePortal;
