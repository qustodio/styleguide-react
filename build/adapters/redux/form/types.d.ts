import * as React from 'react';
export type Input = {
    checked?: boolean;
    name: string;
    value?: string;
    onBlur: (ev: React.SyntheticEvent) => void;
    onChange: (ev: React.SyntheticEvent) => void;
    onDragStart: (ev: React.SyntheticEvent) => void;
    onDrop: (ev: React.SyntheticEvent) => void;
    onFocus: (ev: React.SyntheticEvent) => void;
};
export type Meta = {
    touched: boolean;
    error: string;
};
export type DOMPosition = {
    top: number;
    left: number;
};
