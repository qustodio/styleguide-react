type FactoryFunction = (key: string, line: string) => JSX.Element;
declare const textWrapper: (keyPrefix: string, wrappingElement?: keyof HTMLElementTagNameMap | FactoryFunction) => {
    wrap: (text: string) => JSX.Element;
    reset: () => void;
};
export default textWrapper;
