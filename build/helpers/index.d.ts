export declare const classNames: (...classes: unknown[]) => string;
export declare const isTestEnv: () => boolean;
export declare const getTestId: (prefix: string, id?: string, suffix?: string, suffix2?: string) => string | undefined;
export declare const getTestIdWithParentTestId: (parentTestId?: string, testId?: string) => string | undefined;
export declare const handleKeyboardSelection: <T>(handlerFn: (e: import("react").KeyboardEvent<T>) => void) => (e: import("react").KeyboardEvent<T>) => void;
/** Checks if a given value is a member of the specified enum. */
export declare const isEnumMember: <T>(value: unknown, enumArg: Record<string | number | symbol, T>) => value is T;
