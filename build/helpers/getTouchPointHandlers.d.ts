type EventHandler<U> = (data: U, ...rest: unknown[]) => void;
export type TouchInteractionHandler<U = never> = (state?: 'in' | 'out') => EventHandler<U>;
/**
 * Returns an object with events handlers to manage enter an leave an Element in desktop and mobile.
 *
 * @param handler - The handler that will manage the fired events.
 * @returns - An object containing the onMouseEnter and onMouseLeave if the platform is Desktop.
 *              If the platform is Mobile, the object will have onTouchStart and onTouchEnd.
 */
declare const getTouchPointHandlers: <U>(handler: TouchInteractionHandler<U>, isMobile?: boolean) => {
    touchEvents: Record<string, {
        <V extends U>(e: V): void;
        <R, V_1 extends U>(a: V_1, ...b: R[]): void;
    } | {
        <V_2 extends U>(e: V_2): void;
        <R_1, V_3 extends U>(a: V_3, ...b: R_1[]): void;
    }>;
    focusEvents: Record<string, {
        <V extends U>(e: V): void;
        <R, V_1 extends U>(a: V_1, ...b: R[]): void;
    } | {
        <V_2 extends U>(e: V_2): void;
        <R_1, V_3 extends U>(a: V_3, ...b: R_1[]): void;
    }>;
    isMobile: boolean;
};
export default getTouchPointHandlers;
