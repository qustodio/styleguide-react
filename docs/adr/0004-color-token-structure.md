# [ADR-0004] Color token structure

## Status

Accepted

## Context

In order to keep our SASS variable names as close as possible to Design Token conventions, we have been using the following nomenclature for our color-related SASS variables: `$color-(palette)-(color)-(shade)`.

```scss
/* Examples of color SASS variables. */
$color-base-cherry-500: #c84851;
$color-brand-primary-500: base.$color-base-violet-500;
```

This way of naming color variables has an important limitation: [SASS doesn't support interpolation in variable names](https://sass-lang.com/documentation/variables/#advanced-variable-functions).

This means that we cannot take advantage of our highly consistent nomenclature to obtain colors dynamically.

```scss
/* SASS doesn't support this. */
@function get-color($palette, $color, $shade) {
  @return $color-#{$palette}-#{$color}-#{$shade};
}
```

When working in components that can take several colors as props, it’s inconvenient to not be able to dynamically obtain color variables or generate CSS classes that use colors in the name.

For example, to style a component that accepts any color in the base palette as prop, we would need to do something like this:
  
```scss
@use 'rck';

$color-scheme: (
  'white',
  'black',
  'grey',
  'violet',
  ...
);

@function get-accent-color($color-name) {
  @if($color-name == 'white') {
    @return rck.$color-base-white;
  } @else if($color-name == 'black') {
    @return rck.$color-base-black;
  } @else if($color-name == 'grey') {
    @return rck.$color-base-grey-500;
  } @else if($color-name == 'violet') {
    @return rck.$color-base-violet-500;
  } @else if(...) {
    ...
  }
}
```

* It makes the code unnecessarily verbose.
* It requires manual maintenance as new colors are added or removed.
* It is prone to human error due to misspelling or forgetfulness.

### Suggested alternative

The most common workaround to this limitation is to use [SASS maps](https://sass-lang.com/documentation/modules/map/).

```scss
@use 'sass:map';

$colors: (
  "base": (
    "cherry": (
      "500": #c84851
    ),
    ...
  ),
  ...
);

@function get-color($palette, $color, $shade) {
  @return map.get($colors, $palette, $color, $shade);
}
``` 

### Cons of the suggested alternative

* Completely replacing the current convention with the maps approach would require a significant refactor effort, not only inside of Rocket itself but from projects that use it.
* As Rocket is a library, it is important that it is flexible enough to be used in projects with slightly different needs. SASS supports overwriting variables from external projects through the use of the [`!default` and `@use 'library' with` directives](https://sass-lang.com/documentation/at-rules/use/#configuration). It would be significantly more complex to overwrite color values with maps.

## Decision

We will keep the current color variables, but we will also provide a SASS map for every color palette with all the color values. This way, developers can choose to use either approach depending on their needs, benefiting from:

* The consistency and legibility of the current naming convention.
* The possibility having an easily configurable public API thanks to SASS overwrite directives.
* The possibility of using the SASS maps for dynamic color access.

Only variables in the `tokens/colors/alias` folder will be overwritable. The reason is that base color token names are heavily coupled with their actual values (`$color-base-violet-500`, `$color-base-red-300`, etc), so it makes no sense for them to be overriden with a different color.

```scss
// src/tokens/colors/alias/brand.scss
@use 'sass:map';
@use '../base';

$color-brand-primary-500: base.$color-base-violet-500 !default;

$color-brand: (
  "primary": (
    "500": $color-brand-primary-500
  )
);
```

```scss
// A helper function that helps retrieve a color dynamically.
@use '../../tokens/rck';

@function get-color($palette, $color, $shade) {
  @if $palette == "base" {
    @return map.get(rck.$color-base, $color, $shade);
  } @else if $palette == "brand" {
    @return map.get(rck.$color-brand, $color, $shade);
  }
}
```

```scss
// An external project that uses Rocket but wants to overwrite a color value.
@use 'rck' with (
  $color-brand-primary-500: #ff0000
);
```

In addition, the following helper functions have been created in order to ease access to design color token values:

* `rck.get-color($palette, $color, $shade)`: Returns the color value for the given palette, color and shade.
* `rck.get-colors-from($palette)`: Returns the list of color names for the given palette. A use case for this is to dynamically generate color-related CSS class names for each color of a palette.

Examples of usage of both functions can be found in the [Accordion component](../../src/components/Accordion/Accordion.scss).

## Consequences

* No need to refactor current usages of color variables.
* Both approaches have pros and cons. By making both available, developers can choose the one that best fits their needs.
* It adds extra maintenance, as we need to keep the variables and the maps in sync.
* To make maintenance easier, the following tasks have been created:
  * [ROCK-251: Install and configure stylelint to enforce SCSS rules](https://qustodio.atlassian.net/browse/ROCK-251)
  * [ROCK-252: Create stylelint rule "Make design token variables overwritable by using the !default flag"](https://qustodio.atlassian.net/browse/ROCK-252)
  * [ROCK-253: Create stylelint rule "Make design token variables dynamically accesible by adding them to a SASS map"](https://qustodio.atlassian.net/browse/ROCK-253)
