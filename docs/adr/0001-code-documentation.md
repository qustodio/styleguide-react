# [ADR-0001] Code documentation

## Status

Accepted

## Context

In the context of developing software, it is essential to ensure that the code is properly documented for future maintenance and enhancement. The problem statement is to determine the standard for code documentation in TypeScript.

## Decision

The following are the main drivers for the decision:

- Code readability and maintainability
- Consistency and uniformity in documentation style
- Ease of use and integration with the development workflow
- Ease of discovery and accessibility of the documentation
- Avoid adding additional noise in the code

And we proposed to follow the Clean Code book's comments rules:

- Always try to explain yourself in code.
- Don't be redundant.
- Don't add obvious noise.
- Don't use closing brace comments.
- Don't comment out code. Just remove.
- Use as explanation of intent.
- Use as clarification of code.
- Use as warning of consequences.

The following options were considered for code documentation in TypeScript:

### Using concise and short inline comments

- Good, because it provides a clear and concise description of the code in specific difficult-to-understand code areas
- Good because it does not add too much noise in the code
- Good, because it is easy to add and does not require any additional tools
- Good, because it can add TODOs and FIXMEs to the code to have in consideration for future enhancements
- Bad, because it may not be easily discoverable and can get cluttered, making it difficult to read

### Using TSDoc

Reference: [https://tsdoc.org/](https://tsdoc.org/)

- Good, because it provides a structured and organized way of documenting the code
- Good, because we have de possibility to generate HTML documentation from the source code, making it easily discoverable
- Bad, because it requires additional configuration and setup
- Bad, because it may add additional noise in the code

As a result of the analysis, we decided to use **both options**. Additionally, both options will follow the **Clean Code book's comment rules**, ensuring the code documentation is clear and concise.

It is important to **use the correct comment type for each case**. For example, if we want to add a TODO or FIXME in the middle of a function, we should use an inline comment. If we want to add a description of the function, we should use a TSDoc header.

Also, we should add **very contextual and concise inline comments**, only when it is necessary to clarify difficult-to-understand code. We should not add general information and the moment we are adding a comment, we should ask ourselves if the code is not self-explanatory enough and if it is not, we should refactor the code to make it self-explanatory.

In addition, **it is mandatory to add an inline comment when we are adding an ESLint disable comment**, to explain the reason why we are disabling the rule.

### Examples of good and bad code documentation (following our decision)

#### Bad: redundant TSDoc header

```ts
/**
 * sum two numbers
 *
 * @param a - a number
 * @param b
 * @returns number with he sum of the two numbers
 */
function sum(a: number, b: number): number {
  return a + b;
}
```

Explanation: The TSDoc header is redundant because the function name and parameters are self-explanatory:

- The function name is `sum`, which is a verb that indicates the function's purpose
- There are two parameters, `a` and `b`, therefore the function applies to two numbers
- We do not need to add all parameters in the TSDoc header, only the ones that are not self-explanatory
- The parameters have type `number`, which indicates that they are numbers and it is not necessary to document it
- The result of the function has a `number` type, which indicates already that the result is a number
- The returns TSDoc is redundant because the function name is a verb that indicates the function's purpose and the description has this information repeated

#### Good: TSDoc header with a clear and concise information

```ts
/**
 * @param query - query string to be parsed using this format:
 * `key1=value1&key2=value2`
 * @returns an object with the parsed query string from the url
 * and the query string parameters. If the same key is present in both,
 * the query string parameters will override the url parameters
 */
const getAllUrlParamsParsed = (
  url: string,
  query: string
): Record<string, string> => {
  const urlParams = new URLSearchParams(url);
  const queryParams = new URLSearchParams(query);

  return Object.fromEntries([...urlParams, ...queryParams]);
};
```

Explanation:

- The function name is `getAllUrlParamsParsed`, which is a verb that indicates the function's purpose very clearly and it is not necessary to add a description in the TSDoc header
- The `url` parameter is self-explanatory and does not require any additional documentation
- The `query` parameter is a string that is parsed using a specific format, therefore it is necessary to document the format
- The `@returns` TSDoc is necessary because the function return type is a `Record<string, string>`, which is not self-explanatory and requires additional documentation about how the object is generated

#### Good: inline comments to clarify difficult-to-understand code

```ts
const fetchFamilyPortalLogout = (): BaseThunk<Promise<any>> => dispatch =>
  dispatch(() =>
    fetch(qinit.tenant.common.dashboard.logout_link).catch(() => {
      // This catch block avoids reporting the CORS error to Sentry
    })
  );
```

Explanation: When we see an empty catch block, it is not clear why it is there. Therefore, it is necessary to add an inline comment to explain the reason and to make other developers aware of it.

#### Bad: use inline comments to document function description

```ts
// Retrieves the user name from the user object changing it to uppercase
const getUserName = (user: User): string => user.name.toUpperCase();
```

#### Good: TSDoc comments to document object fields

```ts
interface SetupParams {
  /** Available devices */
  devices?: CreateListFactoryWithDataIn<Device>;
  /** Index of the profile devices [start, end] */
  profileDevices?: [number, number];
  profileId?: number;
  /** data to set into the profile */
  profile?: PartialDeep<Profile>;
}
```

Explanation: The TSDoc comments make the code editor to show the description of the field when the mouse is over the field.

## Consequences

The adoption of this standard will result in the following consequences:

- Improved code readability and maintainability through clear and concise code documentation
- Consistent and uniform documentation style across the codebase
- Additional configuration and setup required for TSDoc, it's necessary to create ESLint rules and plugins for TSDoc
- Use PRs to review the code documentation and ensure it follows the standard or to ask for documentation if it is missing in some difficult understanding code sections