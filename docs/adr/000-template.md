# [ADR-0000] Architecture Decision Record (ADR) Template

## Status

[Proposed | Accepted | Rejected | Deprecated | Superseded by (link to other ADR)]

## Context

[Describe the context and problem statement, e.g., in free form using two to three sentences. You may want to articulate the problem in form of a question.]

## Decision

[What are the changes that we're proposing and/or doing?]

[Optional: list of all important drivers for the decision, e.g., [performance, reliability, usability, portability, â€¦]]

- [driver 1, e.g., a force, facing concern, â€¦]
- [driver 2, e.g., a force, facing concern, â€¦]

[list of the possible solutions with pros and cons]

### [option 1]

- [[Good | Bad], because [argument]]
- [ ... ]

### [option 2]

- [[Good | Bad], because [argument]]
- [ ... ]

### [ ... ]

[list of all options chosen and a justification for these decisions and stablish all the decisions made]

## Consequences

[What are the benefits of this changes and what becomes easier or more difficult to do because of this change?]