# [ADR-0002] Use design tokens

## Status

Proposed

## Context

The current design process lacks consistency and efficiency, with disparate styles and elements scattered across our applications. This leads to challenges in maintaining a cohesive user experience and makes updates and changes time-consuming. To address these issues, we are considering the adoption of [design tokens](https://www.uxpin.com/studio/blog/what-are-design-tokens/) to streamline and centralize our [design system](https://www.uxpin.com/create-design-system-guide/).

you can find a more in-depth context in the [confluence page](https://qustodio.atlassian.net/wiki/spaces/DSY/pages/3875209393/New+Rocket#Overview)

## Decision

We propose the following changes:

- **Design Tokens**: Introduce design tokens to encapsulate our design decisions, such as colors, typography, spacing, and other visual elements, in a central repository.

### Drivers

- **Consistency**: Establishing a single source of truth for design elements ensures consistency across applications and reduces the risk of visual discrepancies.

- **Efficiency**: Design tokens facilitate quicker updates and changes, as modifications can be made at a centralized level, affecting all instances where the token is utilized, as well as give us a common language with UX to facilitate communication.

### Options

#### Adopting design tokens

- **Good**: Promotes consistency and efficiency in design management.
- **Bad**: Initial implementation may require a significant upfront investment of time and resources.

#### Chosen option

We have chosen **Adopting design tokens**

Justification:

- **Consistency**: Design tokens provide a systematic approach to maintaining design consistency, reducing the likelihood of errors or discrepancies as we will have a common language with ux.
- **Efficiency**: Although there is an initial investment, the long-term benefits in terms of design efficiency and ease of maintenance outweigh the upfront costs.

### Token naming conventions

To ensure clarity and ease of use, we will establish a naming convention for our design tokens alongside with a series of layers.

#### Token layers

We have defined a series of layers to organize our tokens:

- **Base**: This layer will contain the base tokens, those are the most basic expression of the value.
- **Alias**: This layer will group base tokens into a more meaningful groups.
- **Component**: This layer will define which tokens are used in a component level.

When using a token you should use the closest to the component as possible, each layer will be defined using the tokens from the previous one.
More about this can be found on the [confluence page](https://qustodio.atlassian.net/wiki/spaces/DSY/pages/4002742408/Definitions)

#### Architecture

We will have a tokens folder containing a folder for each of the categories, in the root of each category folder we will find the base tokens in a file called `base.scss`, together with a folder called _alias_, containing the possible Layer variants.
All these files will be exported in a file at the root of the tokens folder called `rck.scss`.

#### Token naming

The naming convention will follow a structured format depending on the category of the token:

- `[Category]-[Layer]-[Element]-[Modifier]`
- `[Category]-[Element]-[Modifier]`
- `[Category]-[Modifier]`

  - **Category**: Represents the overarching category of the design element (eg., color, typography, spacing).
  - **Layer**: Represents the layer or group of the design element (e.g. base,brand, heading).
  - **Element**: Describes the design element (e.g. red, font-weight, primary)
  - **Modifier**: (Optional) Represents any modification of the base design element (e.g. a values between 100 and 900, or the value that the token express: radius-8 or typography-font-weight-bold )

##### Token naming rules

- Always use lowercase.
- We should choose meaningful names for design token groups in the alias layer.
- Each part of the token name structure will be separated using a **-**.

For example:

- **Bad**:
  - typography-font-size-xl
  - Color-base-pink-200
  - color_brand_primary_200
  - radius-4
- **Good**:
  - color-base-cherry-500
  - color-brand-primary-500
  - typography-family-poppins
  - radius-8

This naming convention will provide clarity and consistency in identifying and implementing design tokens across our applications.

## Consequences

With the adoption of design tokens, we anticipate the following consequences:

- **Benefits**:
  - Improved consistency in design elements.
  - Streamlined design updates and changes.
  - Easier collaboration among designers and developers.
- **Challenges**:
  - Initial learning curve for the team in implementing and utilizing design tokens.
  - Upfront investment in transitioning to the new system.
  - Adding types in an efficient way.

This decision, along with the naming convention, sets the foundation for a more scalable and maintainable design system, enhancing the overall user experience and easing the burden of design management.
