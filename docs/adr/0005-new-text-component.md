# [ADR-0005] New Text component

## Status

Proposed

## Context

As part of our new design system, we have defined typography rules to manage text styling across our applications. These rules support various typography variants and automatically adapt text size for desktop and mobile platforms when needed.

We have developed a React component called `Text` to manage text styling and responsiveness for each typography variant. Additionally, we provide Sass mixins for each typography variant with options to set font weight and style, allowing for custom text styling within components.

## Decision

We have decided to adopt the following rules for using the `Text` component and `typography` foundation Sass mixins in our style guide:

- The `Text` component uses the `typography` foundation Sass mixins to apply the correct typography styling. If a new typography variant is introduced, update the Sass mixin first and then modify the `Text` component to use it.
- Use the `Text` component for specific text elements like headings and body text.
- By default, apply responsive text sizes for titles unless UX requirements specify otherwise.
- For components that require custom text sizes but do not use the `Text` component (e.g., buttons with simple text), use the `typography` foundation Sass mixins. This approach simplifies the generated HTML code. Similar to how we avoid using text tags like `p`, `h1`, or `h2` inside buttons in standard HTML, we should treat the `Text` component as a specialized tag for specific text elements.

## Consequences

- By adhering to these guidelines, we ensure consistent typography across our applications while providing flexibility for customizing text sizes based on specific requirements.
- Utilizing the `Text` component simplifies text management and improves maintainability, while the Sass mixins offer granular control over typography styling when needed.
