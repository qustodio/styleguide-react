# [ADR-0003] Typography Usage

## Status

Superseded by [[ADR-0005] New Text component](0005-new-text-component.md)

## Context

As part of our new design system, we have defined new typography rules to manage text styling across our applications. The proposed rules support various typography variants and automatically adapts text size for desktop and mobile platforms.

We have developed a React component called `Typography` to manage text styling and text responsiveness of each typography variant. Additionally, we provide Sass mixins for each typography variant with the option to set font weight and disable responsive behavior if needed.

## Decision

We have decided to adopt the following rules for using `Typography` component and sass mixins in our style-guide:

- Whenever there is a need to use text elements like headings, body text, etc., within another component that requires responsiveness, we must utilize the `Typography` React component.

- When a component requires custom text sizes for desktop and mobile platforms, we should use the provided Sass mixins. This allows us to customize font sizes while disabling the responsive behavior.

- It's encouraged to use the `Typography` component whenever possible for displaying text, ensuring consistency and easier maintenance across applications. However, if an implementation requires specific font sizes for each resolution, we can opt to use the Sass mixins.

## Consequences

- By adhering to these guidelines, we ensure consistent typography across our applications while providing flexibility for customizing text sizes based on specific requirements.

- Utilizing the `Typography` component simplifies text management and improves maintainability, while the Sass mixins offer granular control over typography styling when needed.
