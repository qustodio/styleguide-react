import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { ExpandableCard } from '../src';

export default {
  title: 'ExpandableCard',
  component: ExpandableCard,
  args: {
    height: '102px',
    mode: 'vertical',
    opener: true,
    style: { border: 'radius' },
  },
} as Meta<typeof ExpandableCard>;

type Story = StoryObj<typeof ExpandableCard>;

const Container = ({ children }: { children: React.ReactNode }) => (
  <div
    style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      marginTop: '32px',
    }}
  >
    <div
      style={{
        width: '900px',
        border: '1px solid #ccc',
        borderRadius: '4px',
        padding: '20px',
      }}
    >
      {children}
    </div>
  </div>
);

const Item = ({ number, minWidth }: { number: number; minWidth: string }) => (
  <div
    style={{
      height: '97px',
      display: 'flex',
      minWidth: minWidth,
      justifyContent: 'center',
      alignItems: 'center',
      margin: '2px',
      border: '1px solid #ccc',
      borderRadius: '4px',
    }}
  >
    {number}
  </div>
);

const items = (length: number) =>
  Array.from({ length }).map((_, number) => number + 1);

export const ExpandableCardVertical: Story = {
  render: props => (
    <Container>
      <span>Click on down arrow</span>
      <ExpandableCard {...props}>
        {items(15).map(number => (
          <Item number={number} minWidth="32%" key={number} />
        ))}
      </ExpandableCard>
    </Container>
  ),
};

export const ExpandableCardHorizontal: Story = {
  render: props => (
    <Container>
      <span>Scroll to right side</span>
      <ExpandableCard {...props} mode="horizontal">
        {items(21).map(number => (
          <Item number={number} minWidth="200px" key={number} />
        ))}
      </ExpandableCard>
    </Container>
  ),
};
