import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Filter from '../src/components/Filter/Filter';
import FilterGroup from '../src/components/Filter/FilterGroup';
import { fn } from '@storybook/test';

export default {
  title: 'Filter',
  component: Filter,
  parameters: {
    layout: 'centered',
  },
  args: {
    text: 'Filter text',
    active: false,
    size: 'regular',
    type: undefined,
    onClick: fn(),
  },
} as Meta<typeof Filter>;

type Story = StoryObj<typeof Filter>;

export const Default: Story = {
  render: props => (
    <div style={{ width: '400px' }}>
      <Filter {...props} />
    </div>
  ),
};

export const ActiveFilterTypes: Story = {
  render: () => (
    <div style={{ width: '400px' }}>
      <Filter text="Inactive" active={false} />
      <Filter text="Primary" active={true} type="primary" />
      <Filter text="Secondary" active={true} type="secondary" />
      <Filter text="Error" active={true} type="error" />
    </div>
  ),
};

export const FilterGroupComponent: Story = {
  render: () => {
    const [active, setActive] = React.useState('1');
    const onClick = (id: string, _e: React.MouseEvent<HTMLButtonElement>) =>
      setActive(id);

    return (
      <div style={{ width: '300px' }}>
        <FilterGroup
          active={active}
          onClick={onClick as () => unknown}
          size="regular"
        >
          <Filter id="1" text="Filter 1" />
          <Filter id="2" text="Primary" type="primary" />
          <Filter id="3" text="Error" type="error" />
          <Filter id="4" text="Secondary" type="secondary" />
          <Filter id="5" text="Filter 5" />
        </FilterGroup>
      </div>
    );
  },
};
