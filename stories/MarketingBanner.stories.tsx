import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import MarketingBanner from '../src/components/MarketingBanner/MarketingBanner';
import Button, { ButtonSize } from '../src/components/Button/Button';
import Tag from '../src/components/Tag';
import MarketingBannerImg from '../src/assets/images/examples/marketing-banner.png';
import { fn } from '@storybook/test';
import { GlobalType } from '../src';

export default {
  title: 'MarketingBanner',
  component: MarketingBanner,
  argTypes: {
    backgroundType: { control: 'select', options: ['plain', 'gradient'] },
    backgroundColor: { control: 'select', options: ['yolk', 'marine'] },
    backgroundShapes: { control: 'boolean' },
    showCloseIcon: { control: 'boolean' },
    closeIconColor: { control: 'select', options: ['yolk', 'white'] },
    onClose: { action: 'close click' },
    button: { control: 'text' },
    alignment: { control: 'select', options: ['center', 'left'] },
    imageSrc: { control: 'text' },
  },
} as Meta<typeof MarketingBanner>;

type Story = StoryObj<typeof MarketingBanner>;

export const DefaultMarketingBanner: Story = {
  args: {
    title: 'title of Banner',
    description: 'Description of the banner',
  },
  render: ({
    backgroundType,
    backgroundColor,
    backgroundShapes,
    showCloseIcon,
    closeIconColor,
    title,
    description,
    alignment,
  }) => (
    <MarketingBanner
      backgroundType={backgroundType}
      backgroundColor={backgroundColor}
      backgroundShapes={backgroundShapes}
      showCloseIcon={showCloseIcon}
      closeIconColor={closeIconColor}
      title={title}
      description={description}
      onClose={fn()}
      button={
        <Button label="Button Text" size={ButtonSize.small} onClick={fn()} />
      }
      tag={
        <Tag
          type={GlobalType.booster}
          text="New!"
          variant="rounded"
          invertColor
        />
      }
      alignment={alignment}
      imageSrc={MarketingBannerImg}
    />
  ),
};
