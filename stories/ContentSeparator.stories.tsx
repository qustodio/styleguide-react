import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import ContentSeparator from '../src/components/ContentSeparator/ContentSeparator';

export default {
  title: 'ContentSeparator',
  parameters: {
    layout: 'centered',
  },
  component: ContentSeparator,
} as Meta<typeof ContentSeparator>;

type Story = StoryObj<typeof ContentSeparator>;

export const ContentSeparatorStory: Story = {
  render: () => (
    <div>
      <p style={{ margin: '0', padding: '0' }}>content 1</p>
      <ContentSeparator />
      <p style={{ margin: '0', padding: '0' }}>content 2</p>
    </div>
  ),
};
