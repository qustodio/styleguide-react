import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import ProfileInfoHeader from '../src/components/AccountInfoHeader/ProfileInfoHeader';
import Avatar from '../src/components/Avatar/Avatar';
import PigAvatarSvg from './assets/images/Avatars/Avatars_Pig.svg';

const meta: Meta<typeof ProfileInfoHeader> = {
  title: 'ProfileInfoHeader',
  component: ProfileInfoHeader,
  parameters: {
    layout: 'centered',
  },
  args: {},
};

export default meta;

type Story = StoryObj<typeof ProfileInfoHeader>;

export const Default: Story = {
  render: ({ status, statusMessage, size }) => (
    <div style={{ width: '200px', height: '300px' }}>
      <ProfileInfoHeader
        status={status}
        statusMessage={statusMessage}
        name="Alisa"
        avatar={
          <Avatar>
            <img src={PigAvatarSvg} />
          </Avatar>
        }
        size={size}
        onEdit={() => {}}
      />
    </div>
  ),
};
