import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import {
  IconWithBadge,
  Avatar,
  AvatarSize,
  Icon,
  IconType,
  IconColor,
  IconSize,
} from '../src';
import ElephantAvatarSvg from './assets/images/Avatars/Avatars_Elephant.svg';

export default {
  title: 'IconWithBadge',
  component: IconWithBadge,
  parameters: {
    layout: 'centered',
  },
  args: {
    showBadge: true,
    badgePosition: 'right',
  },
} as Meta<typeof IconWithBadge>;

type Story = StoryObj<typeof IconWithBadge>;

export const AvatarWithBadge: Story = {
  render: props => (
    <div>
      <IconWithBadge
        icon={
          <Avatar size={AvatarSize.extraSmall} isFreeForm>
            <img src={ElephantAvatarSvg} />
          </Avatar>
        }
        showBadge={props.showBadge}
        badgePosition={props.badgePosition}
      />
    </div>
  ),
};

export const IconRckWithBadge: Story = {
  render: props => (
    <div>
      <IconWithBadge
        icon={<Icon type={IconType.bell} size={IconSize.x2} />}
        showBadge={props.showBadge}
        badgeColor={IconColor.error}
        badgePosition={props.badgePosition}
      />
    </div>
  ),
};
