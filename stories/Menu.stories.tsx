import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Menu from '../src/components/Menu/Menu';
import PigAvatarSvg from './assets/images/Avatars/Avatars_Pig.svg';
import ProfileInfoHeader from '../src/components/AccountInfoHeader/ProfileInfoHeader';
import Avatar from '../src/components/Avatar/Avatar';
import ActionInput, {
  ActionInputIconPosition,
} from '../src/components/ActionInput/ActionInput';
import Icon, { IconType } from '../src/components/Icons';
import DropdownOption from '../src/components/Dropdown/DropdownOption';
import Dropdown from '../src/components/Dropdown/Dropdown';

const menuData = [
  {
    key: 'key1',
    translation: 'Activity',
    isPremium: false,
    needsData: true,
    showContent: true,
    children: [
      {
        key: 'ke2',
        translation: 'Summary',
        isPremium: false,
        needsData: false,
        showContent: true,
      },
      {
        key: 'ke3',
        translation: 'Timeline',
        isPremium: false,
        needsData: false,
        showContent: true,
      },
    ],
  },
  {
    key: 'key4',
    translation: 'Rules',
    isPremium: false,
    needsData: false,
    showContent: true,
    children: [
      {
        key: 'key5',
        translation: 'Daily time limits',
        isPremium: false,
        needsData: false,
        showContent: true,
      },
      {
        key: 'key6',
        translation: 'Restricted times',
        isPremium: false,
        needsData: false,
        showContent: true,
      },
      {
        key: 'key7',
        translation: 'Web filtering',
        isPremium: false,
        needsData: false,
        showContent: true,
      },
      {
        key: 'key8',
        translation: 'Games & apps',
        isPremium: true,
        needsData: false,
        showContent: true,
      },
      {
        key: 'key9',
        translation: 'Youtube',
        isPremium: true,
        needsData: false,
        showContent: true,
      },
      {
        key: 'key10',
        translation: 'Social monitoring',
        isPremium: true,
        needsData: false,
        showContent: true,
      },
      {
        key: 'key11',
        translation: 'Calls & SMS',
        isPremium: false,
        needsData: false,
        showContent: true,
      },
      {
        key: 'key12',
        translation: 'Location',
        isPremium: false,
        needsData: false,
        showContent: true,
      },
      {
        key: 'key13',
        translation: 'Panic button',
        isPremium: false,
        needsData: false,
        showContent: true,
      },
    ],
  },
];

const MenuWithAccordionMenuContainer = ({
  isStatic,
}: {
  isStatic: boolean;
}) => {
  const [selected, setSelected] = useState(undefined);
  const [accordionMenuIsVisible, setAccordionMenuIsVisible] = useState(false);

  const handleClick = id => {
    setSelected(id);
  };
  const handleClickAccordionMenu = () => {
    setAccordionMenuIsVisible(!accordionMenuIsVisible);
  };

  return (
    <div
      style={{
        height: '270px',
        width: '216px',
      }}
    >
      <Menu
        isStatic={isStatic}
        items={menuData}
        selectedRule={selected}
        onClick={handleClick}
        onClickAccordionMenu={handleClickAccordionMenu}
        accordionMenuIsVisible={accordionMenuIsVisible}
        showBadge={true}
        expanded={true}
        scrollable={false}
        profileInfoHeader={
          <ProfileInfoHeader
            status="active"
            name="This is my name"
            statusMessage="Active"
            size="regular"
            avatar={
              <Avatar>
                <img src={PigAvatarSvg} />
              </Avatar>
            }
          />
        }
      />
    </div>
  );
};

const dropDownOptions = [
  {
    value: '1',
    text: 'School',
    selected: true,
  },
  {
    value: '2',
    text: 'Home',
    selected: false,
  },
];

const meta: Meta<typeof Menu> = {
  title: 'Menu',
  component: Menu,
  parameters: {
    layout: 'centered',
  },
};

export default meta;

type Story = StoryObj<typeof Menu>;

export const MenuWithAccordionMenu: Story = {
  render: () => <MenuWithAccordionMenuContainer isStatic={false} />,
};

export const MenuWithDropdown: Story = {
  render: () => (
    <div style={{ height: '270px' }}>
      <div id="test" />
      <Menu
        items={[]}
        profileInfoHeader={
          <ProfileInfoHeader
            status="active"
            name="This is my name"
            statusMessage="Active"
            size="regular"
            avatar={
              <Avatar>
                <img src={PigAvatarSvg} />
              </Avatar>
            }
            actionElement={
              <Dropdown
                actionElement={
                  <ActionInput
                    icon={<Icon type={IconType.angleDown} />}
                    iconPosition={ActionInputIconPosition.right}
                    text="School"
                    block
                    color="primary"
                  />
                }
                portalId="test"
              >
                {dropDownOptions.map(option => (
                  <DropdownOption
                    key={option.value}
                    value={option.value}
                    text={option.text}
                    selected={option.selected}
                  />
                ))}
              </Dropdown>
            }
          />
        }
      />
    </div>
  ),
};

export const MenuWithDropdownAndAccordion: Story = {
  render: () => (
    <div style={{ height: '270px' }}>
      <div id="test" />
      <Menu
        items={menuData}
        profileInfoHeader={
          <ProfileInfoHeader
            status="active"
            name="This is my name"
            statusMessage="Active"
            size="regular"
            avatar={
              <Avatar>
                <img src={PigAvatarSvg} />
              </Avatar>
            }
            actionElement={
              <Dropdown
                actionElement={
                  <ActionInput
                    icon={<Icon type={IconType.angleDown} />}
                    iconPosition={ActionInputIconPosition.right}
                    text="School"
                    placeholder="School"
                    block
                    color="primary"
                  />
                }
                portalId="test"
              >
                {dropDownOptions.map(option => (
                  <DropdownOption
                    key={option.value}
                    value={option.value}
                    text={option.text}
                    selected={option.selected}
                  />
                ))}
              </Dropdown>
            }
          />
        }
      />
    </div>
  ),
};
