import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import ColorPicker from '../src/components/ColorPicker/ColorPicker';

export default {
  title: 'ColorPicker',
  parameters: {
    layout: 'centered',
  },
  component: ColorPicker,
} as Meta<typeof ColorPicker>;

type Story = StoryObj<typeof ColorPicker>;

const palette = {
  primaryDark: '#2f7768',
  primaryLight: '#9dcfc4',
  secondary: '#6161ff',
  secondaryDark: '#4949b9',
  secondaryDarker: '#1f6178',
  success: '#9dcfc4',
  successDark: '#2f7768',
  error: '#f3656f',
  errorDarker: '#a03941',
  errorDark: '#c84851',
  warning: '#ffc054',
  warningDark: '#ffa710',
  decorationOrange: '#ff965c',
  decorationOrangeDark: '#c85618',
  decorationBlue: '#60d0ec',
  decorationBlueDark: '#117892',
  decorationGreen: '#49c592',
  decorationGreenDark: '#236c4e',
  booster: '#d70469',
  boosterDark: '#890343',
  school: '#0a95c2',
  schoolDark: '#054b61',
  marketingRed: '#e54141',
  marketingOrange: '#fd752c',
};

export const ColorPickerStory: Story = {
  render: () => {
    const [active, setActive] = useState<string | undefined>();
    const onClick = (color: string) => {
      setActive(color);
    };
    return (
      <div style={{ width: '400px' }}>
        <ColorPicker palette={palette} activeColor={active} onClick={onClick} />
      </div>
    );
  },
};
