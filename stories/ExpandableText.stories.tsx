import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { ExpandableText, Text } from '../src';

export default {
  title: 'ExpandableText',
  component: ExpandableText,
  args: {
    maxLines: 3,
    expandText: 'Show more',
    collapseText: 'Show less',
  },
} as Meta<typeof ExpandableText>;

type Story = StoryObj<typeof ExpandableText>;

export const expandableText: Story = {
  render: props => (
    <div style={{ maxWidth: '200px' }}>
      <ExpandableText {...props}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac
        fermentum sapien. Nullam nec metus nec felis tincidunt fermentum.
        Phasellus nec nisl at purus ultrices ultricies. Nulla facilisi.
        Vestibulum ac sapien eget nunc tincidunt aliquam. Nulla facilisi.
        Vestibulum ac sapien eget nunc tincidunt aliquam.
      </ExpandableText>
    </div>
  ),
};

export const expandableTextWithText: Story = {
  render: props => (
    <div style={{ maxWidth: '200px' }}>
      <ExpandableText {...props}>
        <Text variant="body-1-regular">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac
          fermentum sapien. Nullam nec metus nec felis tincidunt fermentum.
          Phasellus nec nisl at purus ultrices ultricies. Nulla facilisi.
          Vestibulum ac sapien eget nunc tincidunt aliquam. Nulla facilisi.
          Vestibulum ac sapien eget nunc tincidunt aliquam.
        </Text>
      </ExpandableText>
    </div>
  ),
};

export const shortText: Story = {
  render: props => (
    <div style={{ maxWidth: '200px' }}>
      <ExpandableText {...props}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      </ExpandableText>
    </div>
  ),
};

export const percentWidthContainer: Story = {
  render: props => (
    <div style={{ maxWidth: '25%' }}>
      <ExpandableText {...props}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac
        fermentum sapien. Nullam nec metus nec felis tincidunt fermentum.
        Phasellus nec nisl at purus ultrices ultricies. Nulla facilisi.
        Vestibulum ac sapien eget nunc tincidunt aliquam. Nulla facilisi.
        Vestibulum ac sapien eget nunc tincidunt aliquam. Lorem ipsum dolor sit
        amet, consectetur adipiscing elit. Sed ac fermentum sapien. Nullam nec
        metus nec felis tincidunt fermentum. Phasellus nec nisl at purus
        ultrices ultricies. Nulla facilisi. Vestibulum ac sapien eget nunc
        tincidunt aliquam. Nulla facilisi. Vestibulum ac sapien eget nunc
        tincidunt aliquam.
      </ExpandableText>
    </div>
  ),
};

export const percentFullWidth: Story = {
  render: props => (
    <div style={{ maxWidth: '100%' }}>
      <ExpandableText {...props}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac
        fermentum sapien. Nullam nec metus nec felis tincidunt fermentum.
        Phasellus nec nisl at purus ultrices ultricies. Nulla facilisi.
        Vestibulum ac sapien eget nunc tincidunt aliquam. Nulla facilisi.
      </ExpandableText>
    </div>
  ),
};

export const controlledExpandText: Story = {
  render: props => {
    const [isExpanded, setIsExpanded] = React.useState(false);
    const onToggle = () => setIsExpanded(!isExpanded);
    return (
      <div style={{ maxWidth: '200px' }}>
        <button onClick={onToggle}>expand</button>
        <ExpandableText
          {...props}
          isTextExpanded={isExpanded}
          onToggle={onToggle}
        >
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac
          fermentum sapien. Nullam nec metus nec felis tincidunt fermentum.
          Phasellus nec nisl at purus ultrices ultricies. Nulla facilisi.
          Vestibulum ac sapien eget nunc tincidunt aliquam. Nulla facilisi.
        </ExpandableText>
      </div>
    );
  },
};
