import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { TextArea } from '../src/index';

export default {
  title: 'TextArea',
  component: TextArea,
  argTypes: {
    defaultValue: { control: 'text' },
    maxRows: { control: 'number' },
    rows: { control: 'number' },
    maxLength: { control: 'number' },
    helperText: { control: 'text' },
    disabled: { control: 'boolean' },
    error: { control: 'boolean' },
    placeholder: { control: 'text' },
    testId: { control: 'text' },
  },
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof TextArea>;

type Story = StoryObj<typeof TextArea>;

const StoryLayout = ({ children }: { children: React.ReactNode }) => (
  <div
    style={{
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: '30px',
    }}
  >
    <div
      style={{
        width: '800px',
      }}
    >
      {children}
    </div>
  </div>
);

export const TextAreaGrowsInfinitely: Story = {
  render: () => (
    <StoryLayout>
      <TextArea defaultValue="Text..." rows={10} />
    </StoryLayout>
  ),
};

export const TextAreaMaxRowsGrowsWithScroll: Story = {
  args: {
    defaultValue: [
      'Lorem',
      'ipsum',
      'dolor',
      'sit',
      'amet,',
      'consectetur',
      'adipiscing',
      'elit,',
      'sed',
      'do',
      'eiusmod',
      'tempor',
      'incididunt',
      'ut',
      'labore',
      'et',
      'dolore',
      'magna',
      'aliqua.',
    ].join('\n'),
  },
  render: ({ defaultValue }) => (
    <StoryLayout>
      <TextArea defaultValue={defaultValue} maxRows={15} rows={10} />
    </StoryLayout>
  ),
};

export const TextAreaBasicWithHelperText: Story = {
  render: () => (
    <StoryLayout>
      <TextArea
        defaultValue="Text..."
        maxRows={15}
        rows={10}
        helperText="Helper text."
      />
    </StoryLayout>
  ),
};

export const TextAreaTextLimited: Story = {
  render: () => (
    <StoryLayout>
      <TextArea defaultValue="Text..." maxRows={15} rows={10} maxLength={50} />
    </StoryLayout>
  ),
};

export const TextAreaDisabled: Story = {
  render: () => (
    <StoryLayout>
      <TextArea defaultValue="Text..." maxRows={15} rows={10} disabled={true} />
    </StoryLayout>
  ),
};

export const TextAreaError: Story = {
  render: () => (
    <StoryLayout>
      <TextArea defaultValue="Text..." maxRows={15} rows={10} error={true} />
    </StoryLayout>
  ),
};

export const TextAreaErrorWithHelperText: Story = {
  render: () => (
    <StoryLayout>
      <TextArea
        defaultValue="Text..."
        maxRows={15}
        rows={10}
        error={true}
        helperText="Invalid text."
      />
    </StoryLayout>
  ),
};

export const TextAreaPlaceholder: Story = {
  render: () => (
    <StoryLayout>
      <TextArea
        placeholder="Comments..."
        maxRows={15}
        rows={10}
        testId="comments"
      />
    </StoryLayout>
  ),
};
