import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Avatar from '../src/components/Avatar/Avatar';
import {
  AvatarProps,
  AvatarSize,
  DefaultAvatarIconsSet,
} from '../src/components/Avatar/Avatar.types';
import ChickenAvatarSvg from './assets/images/Avatars/Avatars_Chicken.svg';
import ElephantAvatarSvg from './assets/images/Avatars/Avatars_Elephant.svg';
import PigAvatarSvg from './assets/images/Avatars/Avatars_Pig.svg';
import Icon, {
  IconColor,
  IconFamily,
  IconSize,
  IconType,
} from '../src/components/Icons';
import FlexLayout from '../src/components/Layout/FlexLayout';
import {
  AlertBox,
  AlertBoxType,
  ClickableListItem,
  List,
  ListItemVerticalAlign,
  ListTitle,
  RegularStyleListItem,
  Text,
} from '../src';

const AvatarItem = ({
  children,
  label,
}: {
  children: JSX.Element;
  label: string | string[];
}) => (
  <FlexLayout mainaxis="column" crossaxisAlignment="center">
    <FlexLayout
      mainaxis="column"
      crossaxisAlignment="center"
      mainaxisAlignment="center"
      height="120"
    >
      {children}
    </FlexLayout>
    <Text variant="body-2-regular" align="center" noWrap>
      {label}
    </Text>
  </FlexLayout>
);

export default {
  title: 'Avatars',
  component: Avatar,
  parameters: {
    layout: 'centered',
  },
  argTypes: {
    size: {
      control: { type: 'select' },
      options: Object.values(AvatarSize),
    },
    icon: {
      control: { type: 'select' },
      options: [
        'pencil',
        'camera',
        'status_tampered',
        'status_paused',
        'status_online',
        'custom',
        undefined,
      ] as (DefaultAvatarIconsSet | 'custom' | undefined)[],
      mapping: {
        custom: <Icon type={IconType.bell} family={IconFamily.solid} />,
      },
    },
    renderAs: {
      control: { type: 'select' },
      options: ['default', 'button', 'link'],
    },
    iconBackground: {
      control: { type: 'select' },
      options: [
        'none',
        'brand',
        'error',
        'grey',
        'success',
        'success',
        'warning',
        'white',
      ] as AvatarProps['iconBackground'][],
    },
    iconColor: {
      control: { type: 'select' },
      options: [
        'brand',
        'error',
        'grey',
        'success',
        'success',
        'warning',
        'white',
      ] as AvatarProps['iconColor'][],
    },
    children: {
      control: { type: 'select' },
      options: [
        'Initial',
        'Chicken',
        'Elephant',
        'Pig',
        'Profile Image',
        'Plus icon',
        'empty',
      ],
      mapping: {
        empty: null,
        Initial: 'g',
        Chicken: <img alt="avatar-image" src={ChickenAvatarSvg} />,
        Elephant: <img alt="avatar-image" src={ElephantAvatarSvg} />,
        Pig: <img alt="avatar-image" src={PigAvatarSvg} />,
        'Profile Image': (
          <img
            alt="avatar-image"
            src="https://s3.amazonaws.com/static-pre.qustodio.com/img/email/comm_educational_email_2/Img2.jpg"
          />
        ),
        'Plus icon': <Icon type={IconType.plus} size={IconSize.x2} />,
      },
    },
  },
} as Meta<typeof Avatar>;

type Story = StoryObj<typeof Avatar>;

export const Default: Story = {
  args: {
    children: 'G',
    size: AvatarSize.extraLarge,
    icon: 'camera',
    emptyBackground: false,
    isFreeForm: false,
    onClick: () => console.log('clicked'),
    href: 'https://www.qustodio.com',
    target: '_blank',
  },
};

export const Types: Story = {
  args: {
    size: AvatarSize.extraLarge,
  },
  render: args => (
    <FlexLayout
      mainaxis="row"
      gap="32"
      crossaxisAlignment="center"
      mainaxisAlignment="center"
    >
      <AvatarItem label="Text">
        <Avatar
          aria-label="Letter R avatar"
          renderAs={args.renderAs}
          size={args.size}
        >
          G
        </Avatar>
      </AvatarItem>
      <AvatarItem label="Image">
        <Avatar
          renderAs={args.renderAs}
          size={args.size}
          src="https://s3.amazonaws.com/static-pre.qustodio.com/img/email/comm_educational_email_2/Img2.jpg"
          alt="Profile image avatar"
        />
      </AvatarItem>
    </FlexLayout>
  ),
};

export const DeprecatedTypes: Story = {
  args: {
    size: AvatarSize.extraLarge,
  },
  render: args => (
    <>
      <FlexLayout
        mainaxis="row"
        gap="32"
        crossaxisAlignment="center"
        mainaxisAlignment="center"
      >
        <AvatarItem label="Empty background">
          <Avatar
            aria-label="Empty background"
            renderAs={args.renderAs}
            emptyBackground
            size={args.size}
          />
        </AvatarItem>
        <AvatarItem label="Empty background + icon *">
          <Avatar
            aria-label="Empty avatar with plus icon"
            renderAs={args.renderAs}
            emptyBackground
            size={args.size}
          >
            <Icon type={IconType.plus} size={IconSize.x2} />
          </Avatar>
        </AvatarItem>
        <AvatarItem label="Freeform Image **">
          <Avatar
            renderAs={args.renderAs}
            isFreeForm
            size={args.size}
            src={ElephantAvatarSvg}
            alt="Elephant profile avatar"
          />
        </AvatarItem>
      </FlexLayout>
      <FlexLayout mainaxis="column" gap="16" marginTop="104" maxWidth="520">
        <AlertBox type={AlertBoxType.info} icon={<>(*)</>}>
          The provided icon does not scale with the avatar.
        </AlertBox>
        <AlertBox type={AlertBoxType.info} icon={<>(**)</>}>
          Freeform means that we do not hide the overflow. It is used for image
          avatars that do not fit properly in a circular shape.
        </AlertBox>
        <AlertBox
          type={AlertBoxType.warning}
          icon={IconType.exclamationTriangle}
        >
          These variants will be removed in the future. They are here for
          retro-compatibility reasons.
        </AlertBox>
      </FlexLayout>
    </>
  ),
};

export const Sizes: Story = {
  args: {
    emptyBackground: false,
    isFreeForm: false,
    children: 'G',
    icon: undefined,
  },
  render: args => (
    <FlexLayout mainaxis="row" gap="32" crossaxisAlignment="center">
      {Object.values(AvatarSize).map(size => (
        <AvatarItem key={size} label={size}>
          <Avatar
            renderAs="button"
            size={size}
            emptyBackground={args.emptyBackground}
            isFreeForm={args.isFreeForm}
            icon={args.icon}
            aria-label={`Avatar with size ${size}`}
          >
            {args.children}
          </Avatar>
        </AvatarItem>
      ))}
    </FlexLayout>
  ),
};

export const DefaultIcons: Story = {
  render: () => (
    <FlexLayout mainaxis="column" gap="32" crossaxisAlignment="center">
      <FlexLayout mainaxis="row" gap="32">
        {([
          'camera',
          'pencil',
          'status_online',
          'status_paused',
          'status_tampered',
        ] as DefaultAvatarIconsSet[]).map(icon => (
          <AvatarItem key={icon} label={icon}>
            <Avatar
              renderAs="button"
              aria-label={`Avatar with ${icon} icon`}
              size={AvatarSize.extraLarge}
              icon={icon}
            >
              G
            </Avatar>
          </AvatarItem>
        ))}
      </FlexLayout>
      <FlexLayout mainaxis="row" gap="32">
        {([
          'camera',
          'pencil',
          'status_online',
          'status_paused',
          'status_tampered',
        ] as DefaultAvatarIconsSet[]).map(icon => (
          <AvatarItem key={icon} label="">
            <Avatar
              renderAs="button"
              size={AvatarSize.small}
              icon={icon}
              src="https://s3.amazonaws.com/static-pre.qustodio.com/img/email/comm_educational_email_2/Img2.jpg"
              alt={`Avatar with ${icon} icon`}
            />
          </AvatarItem>
        ))}
      </FlexLayout>
    </FlexLayout>
  ),
};

export const CustomIcon: Story = {
  args: {
    size: AvatarSize.extraLarge,
    children: undefined,
    iconColor: 'white',
    iconBackground: 'error',
  },
  render: args => (
    <FlexLayout mainaxis="row" gap="32">
      <Avatar
        renderAs="button"
        size={args.size}
        icon={<Icon type={IconType.ban} family={IconFamily.solid} />}
        iconColor={args.iconColor}
        iconBackground={args.iconBackground}
        src="https://s3.amazonaws.com/static-pre.qustodio.com/img/email/comm_educational_email_2/Img2.jpg"
        alt={`Avatar with custom icon`}
      ></Avatar>
    </FlexLayout>
  ),
};

export const Clickable: Story = {
  args: {
    size: AvatarSize.extraLarge,
  },
  render: args => (
    <FlexLayout mainaxis="row" gap="32">
      <AvatarItem label="Default">
        <Avatar
          isFreeForm
          size={args.size}
          src={ElephantAvatarSvg}
          alt="Elephant avatar"
        />
      </AvatarItem>
      <AvatarItem label="Button">
        <Avatar
          renderAs="button"
          size={args.size}
          src="https://s3.amazonaws.com/static-pre.qustodio.com/img/email/comm_educational_email_2/Img2.jpg"
          alt="Image photo Avatar"
          onClick={() => alert('Button clicked')}
        />
      </AvatarItem>
      <AvatarItem label="Link">
        <Avatar
          renderAs="link"
          emptyBackground
          icon="pencil"
          size={args.size}
          href="https://www.qustodio.com/"
          target="_blank"
          aria-label="Letter F avatar"
        >
          F
        </Avatar>
      </AvatarItem>
    </FlexLayout>
  ),
};

export const AvatarsInList: Story = {
  args: {
    size: AvatarSize.large,
  },
  render: args => {
    const listItems = [
      <Avatar
        size={args.size}
        src={ElephantAvatarSvg}
        isFreeForm
        alt="Elephant avatar"
      />,
      <Avatar aria-label="Avatar with a letter" size={args.size}>
        Jo
      </Avatar>,
      <Avatar size={args.size}>
        <img
          alt="Image photo Avatar"
          src="https://s3.amazonaws.com/static-pre.qustodio.com/img/email/comm_educational_email_2/Img2.jpg"
        />
      </Avatar>,
    ];

    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          width: '500px',
          height: 'auto',
          padding: '20px',
          flexDirection: 'row',
        }}
      >
        <List listItemMarginTop="16" listItemPaddingBottom="8">
          {listItems.map((avatarItem, index) => (
            <RegularStyleListItem
              key={index}
              centerMiddleContent
              iconVerticalAlign={ListItemVerticalAlign.center}
              actionElementVerticalAlign={ListItemVerticalAlign.center}
              actionElement={
                <ClickableListItem>
                  <Icon
                    type={IconType.angleRight}
                    size={IconSize.lg}
                    color={IconColor.grayLight}
                  />
                </ClickableListItem>
              }
              title={
                <ClickableListItem>
                  <ListTitle>List item {index + 1}</ListTitle>
                </ClickableListItem>
              }
              icon={avatarItem}
              upperSubtitle=""
              lowerSubtitle=""
            />
          ))}
        </List>
      </div>
    );
  },
};
