import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import TabPanel from '../src/components/Tabs/TabPanel';
import Tabs from '../src/components/Tabs/Tabs';
import { GlobalType } from '../src/common/types';
import { fn } from '@storybook/test';

export default {
  title: 'Tabs',
  component: Tabs,
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof Tabs>;

type Story = StoryObj<typeof Tabs>;

const TabsWrapper = ({
  children,
}: {
  children: (
    tabName: string,
    handleChange: (name: string) => void
  ) => React.ReactElement;
}) => {
  const [tabName, setTabName] = useState('cars');
  const handleChange = (name: string) => {
    setTabName(name);
  };
  return <React.Fragment>{children(tabName, handleChange)}</React.Fragment>;
};

export const BasicTab: Story = {
  render: ({ hugContent }) => {
    const activeColor = GlobalType.default;
    const separator = true;

    return (
      <div
        style={{
          position: 'absolute',
          top: '100px',
          left: '10px',
          width: '90%',
        }}
      >
        <TabsWrapper>
          {(tabName, handleChange) => (
            <Tabs
              onChange={handleChange}
              animateOnChange
              showSeparator={separator}
              hugContent={hugContent}
            >
              <TabPanel
                name="cars"
                content="cars"
                active={tabName === 'cars'}
                activeColor={activeColor}
              >
                <ul>
                  <li>mercedes</li>
                  <li>citroen</li>
                  <li>renault</li>
                  <li>tesla</li>
                </ul>
              </TabPanel>
              <TabPanel
                name="computerCompanies"
                content="computer companies"
                active={tabName === 'computerCompanies'}
                activeColor={activeColor}
                disabled
              >
                <ul>
                  <li>IBM</li>
                  <li>Atari</li>
                  <li>MSX</li>
                  <li>Amstrad</li>
                  <li>MSI</li>
                </ul>
              </TabPanel>
              <TabPanel
                name="films"
                content="films"
                active={tabName === 'films'}
                activeColor={activeColor}
              >
                <ul>
                  <li>2001 Space Odissey</li>
                  <li>The Chase</li>
                  <li>The Lost Weekend</li>
                  <li>Paths of Glory</li>
                  <li>Léolo</li>
                </ul>
              </TabPanel>
            </Tabs>
          )}
        </TabsWrapper>
      </div>
    );
  },
};

export const TabsColoredActiveLabel: Story = {
  render: ({ hugContent }) => {
    const separator = false;

    return (
      <React.Fragment>
        {new Array(4).fill(0).map((_, index) => (
          <div
            key={index}
            style={{
              position: 'relative',
              top: '10px',
              left: '10px',
              width: '500px',
            }}
          >
            <Tabs
              showSeparator={separator}
              hugContent={hugContent}
              onChange={fn()}
            >
              <TabPanel
                name="All"
                content="All"
                active={index === 0}
                children={undefined}
              />
              <TabPanel
                name="Allowed"
                content="Allowed"
                active={index === 1}
                activeColor={GlobalType.primary}
                children={undefined}
              />
              <TabPanel
                name="Blocked"
                content="Blocked"
                active={index === 2}
                activeColor={GlobalType.error}
                children={undefined}
              />
              <TabPanel
                name="Limit set"
                content="Limit set"
                active={index === 3}
                children={undefined}
              />
            </Tabs>
          </div>
        ))}
      </React.Fragment>
    );
  },
};
