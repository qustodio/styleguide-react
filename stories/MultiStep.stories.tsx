import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { MultiStep } from '../src';

const meta: Meta<typeof MultiStep> = {
  title: 'MultiStep',
  component: MultiStep,
  parameters: {
    layout: 'centered',
  },
};

export default meta;
type Story = StoryObj<typeof MultiStep>;

const Container = ({ children }: { children: React.ReactNode }) => (
  <div
    style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      marginTop: '32px',
    }}
  >
    <div
      style={{
        width: '900px',
        border: '1px solid #ccc',
        borderRadius: '4px',
        padding: '20px',
      }}
    >
      {children}
    </div>
  </div>
);

const stepStyle = (color: string): React.CSSProperties => ({
  border: '1px solid #ccc',
  borderRadius: '4px',
  padding: '20px',
  backgroundColor: color,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
});

export const MultiStepAllSteps: Story = {
  render: () => (
    <Container>
      <MultiStep
        testId="all"
        steps={[
          {
            name: 'one',
            step: ({ next, transition }) => (
              <div style={stepStyle('#ff876f')}>
                <h2>Step One</h2>
                <p>Transition: {transition}</p>
                <button onClick={next}>Go to step 2</button>
              </div>
            ),
          },
          {
            name: 'two',
            step: ({ next, prev, transition }) => (
              <div style={stepStyle('#fad694')}>
                <h2>Step two</h2>
                <div>
                  <p>Transition: {transition}</p>
                  <button onClick={prev}>Go to step 1</button>
                  <button onClick={next}>Go to step 3</button>
                </div>
              </div>
            ),
          },
          {
            name: 'three',
            step: ({ next, prev, go, transition }) => (
              <div style={stepStyle('#afac34')}>
                <h2>Step three</h2>
                <div>
                  <p>Transition: {transition}</p>
                  <button onClick={prev}>Go to step 2</button>
                  <button onClick={next}>Go to step 3</button>
                  <button onClick={() => go('one')}>Go to step 1</button>
                </div>
              </div>
            ),
          },
        ]}
      />
    </Container>
  ),
};

export const MultiStepSelectedActiveStep: Story = {
  render: () => (
    <Container>
      <MultiStep
        testId="selected"
        activeStep="three"
        steps={[
          {
            name: 'one',
            step: ({ next, transition }) => (
              <div style={stepStyle('#ff876f')}>
                <h2>Step One</h2>
                <p>Transition: {transition}</p>
                <button onClick={next}>Go to step 2</button>
              </div>
            ),
          },
          {
            name: 'two',
            step: ({ next, prev, transition }) => (
              <div style={stepStyle('#fad694')}>
                <h2>Step two</h2>
                <div>
                  <p>Transition: {transition}</p>
                  <button onClick={prev}>Go to step 1</button>
                  <button onClick={next}>Go to step 3</button>
                </div>
              </div>
            ),
          },
          {
            name: 'three',
            step: ({ next, prev, go, transition }) => (
              <div style={stepStyle('#afac34')}>
                <h2>Step three: selected default</h2>
                <div>
                  <p>Transition: {transition}</p>
                  <button onClick={prev}>Go to step 2</button>
                  <button onClick={next}>Go to step 3</button>
                  <button onClick={() => go('one')}>Go to step 1</button>
                </div>
              </div>
            ),
          },
        ]}
      />
    </Container>
  ),
};

export const MultiStepSelectedNonExistentStep: Story = {
  render: () => (
    <Container>
      <MultiStep
        testId="non-existent"
        activeStep="non-existent-step"
        steps={[
          {
            name: 'one',
            step: ({ next, transition }) => (
              <div style={stepStyle('#ff876f')}>
                <h2>Step One</h2>
                <p>Transition: {transition}</p>
                <button onClick={next}>Go to step 2</button>
              </div>
            ),
          },
          {
            name: 'two',
            step: ({ next, prev, transition }) => (
              <div style={stepStyle('#fad694')}>
                <h2>Step two</h2>
                <div>
                  <p>Transition: {transition}</p>
                  <button onClick={prev}>Go to step 1</button>
                  <button onClick={next}>Go to step 3</button>
                </div>
              </div>
            ),
          },
          {
            name: 'three',
            step: ({ next, prev, go, transition }) => (
              <div style={stepStyle('#afac34')}>
                <h2>Step three</h2>
                <div>
                  <p>Selected step default</p>
                  <p>Transition: {transition}</p>
                  <button onClick={prev}>Go to step 2</button>
                  <button onClick={next}>Go to step 3</button>
                  <button onClick={() => go('one')}>Go to step 1</button>
                </div>
              </div>
            ),
          },
        ]}
      />
    </Container>
  ),
};

const MultiStepWithStateDelegation = () => {
  const [step, setStep] = React.useState('one');

  return (
    <Container>
      <MultiStep
        testId="state-delegation"
        activeStep={step}
        steps={[
          {
            name: 'one',
            step: () => (
              <div style={stepStyle('#ff876f')}>
                <h2>Step One</h2>
              </div>
            ),
          },
          {
            name: 'two',
            step: () => (
              <div style={stepStyle('#fad694')}>
                <h2>Step two</h2>
              </div>
            ),
          },
          {
            name: 'three',
            step: () => (
              <div style={stepStyle('#afac34')}>
                <h2>Step three</h2>
              </div>
            ),
          },
        ]}
      />

      <button onClick={() => setStep('one')}>One</button>
      <button onClick={() => setStep('two')}>Two</button>
      <button onClick={() => setStep('three')}>Three</button>
    </Container>
  );
};

export const MultiStepWithStateDelegationStory: Story = {
  render: () => <MultiStepWithStateDelegation />,
};
