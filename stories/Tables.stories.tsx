import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import ComparisonTable from '../src/components/Table/ComparisonTable';
import { IconType } from '../src/components/Icons';

const tableData = {
  head: [
    { icon: IconType.user, text: 'Comparative Features' },
    { text: 'Item one' },
    { text: 'Item two' },
  ],
  rows: [
    ['Feature one', true, true],
    ['Feature two', false, true],
    ['Feature three', null, true],
    ['Feature four', '*included', true],
    ['Feature five', true, false],
  ],
};

const StoryWrapper = (Story: () => React.ReactElement) => (
  <div style={{ margin: 16 }}>
    <Story />
  </div>
);

const meta: Meta<typeof ComparisonTable> = {
  component: ComparisonTable,
  decorators: [StoryWrapper],
  parameters: {
    layout: 'centered',
  },
  args: {
    data: tableData,
  },
};

export default meta;

type Story = StoryObj<typeof ComparisonTable>;

export const Default: Story = {
  render: props => <ComparisonTable {...props} />,
};
