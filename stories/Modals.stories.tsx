import React from 'react';
import { GlobalType } from '../src/common/types';
import Modal, {
  ModalButtonsAlignment,
  ModalStyledText,
} from '../src/components/Modal/Modal';
import Button, { ButtonSize } from '../src/components/Button/Button';
import StepBar from '../src/components/StepBar/StepBar';
import Icon, { IconType } from '../src/components/Icons';
import TabPanel from '../src/components/Tabs/TabPanel';
import Tabs from '../src/components/Tabs/Tabs';
import Avatar from '../src/components/Avatar/Avatar';
import BoyAvatarSvg from './assets/images/Avatars/boy.svg';
import { Meta, StoryObj } from '@storybook/react';
import { fn } from '@storybook/test';

const createHeaderOptions = () => {
  type Keys = keyof typeof IconType | 'none';
  type HeaderOptions = Record<
    Keys,
    { icon: React.ReactNode | undefined } | undefined
  >;

  return Object.entries(IconType).reduce(
    (options: HeaderOptions, [key, value]) => ({
      ...options,
      [key]: { icon: <Icon type={value as IconType} /> },
    }),
    { none: undefined } as HeaderOptions
  );
};

const meta: Meta = {
  title: 'Modal',
  component: Modal,
  args: {
    width: 320,
    height: 200,
    type: GlobalType.primary,
    title: 'Modal title',
    animationEnabled: false,
    hideCloseButton: false,
    onClickClose: fn(),
    showBackButton: false,
    isFullScreen: false,
    size: 'small',
    buttonsAlignment: ModalButtonsAlignment.column,
  },
  argTypes: {
    type: {
      control: 'select',
      options: Object.keys(GlobalType),
      mapping: GlobalType,
    },
    buttons: {
      control: 'select',
      options: ['none', 'one', 'two', 'three', ' block'],
      mapping: {
        none: undefined,
        one: [
          <Button key="1" size={ButtonSize.medium} onClick={fn()}>
            Button 1
          </Button>,
        ],
        two: [
          <Button key="1" size={ButtonSize.medium} onClick={fn()}>
            Button 1
          </Button>,
          <Button key="2" size={ButtonSize.medium} onClick={fn()}>
            Button 2
          </Button>,
        ],
        three: [
          <Button key="1" size={ButtonSize.medium} onClick={fn()}>
            Button 1
          </Button>,
          <Button key="2" size={ButtonSize.medium} onClick={fn()}>
            Button 2
          </Button>,
          <Button key="3" size={ButtonSize.medium} onClick={fn()}>
            Button 3
          </Button>,
        ],
        block: [
          <Button block size={ButtonSize.medium}>
            Skip
          </Button>,
          <Button block size={ButtonSize.medium}>
            Accept
          </Button>,
        ],
      },
    },
    header: {
      control: 'select',
      options: ['none', ...Object.keys(IconType)],
      mapping: createHeaderOptions(),
    },
    children: {
      control: 'select',
      options: ['text', 'long text', 'custom body', 'custom body 2'],
      mapping: {
        text: 'Modal Example',
        'custom body': (
          <>
            <StepBar activeStep={1} steps={['Step 1', 'Step 2', 'Step 3']} />
            <p style={{ marginTop: '16px' }}>
              This is a custom content for this modal whitout the usual
              structure with title and text.
            </p>
          </>
        ),
        'custom body 2': (
          <Tabs onChange={fn}>
            <TabPanel active name="boy" content="Boy">
              {[...Array(6)].map((_, i) => (
                <div
                  style={{
                    overflow: 'hidden',
                    float: 'left',
                  }}
                  key={i}
                >
                  <Avatar>
                    <img
                      src={BoyAvatarSvg}
                      style={{ overflow: 'hidden', float: 'left' }}
                    />
                  </Avatar>
                </div>
              ))}
            </TabPanel>
            <TabPanel
              name="girl"
              content="Girl"
              active={false}
              children={undefined}
            />
          </Tabs>
        ),
        'long text':
          'Are your ready to join up and be part of our awesome, super-value Yearly Membership Subscription? ',
      },
    },
    buttonsAlignment: {
      control: 'select',
      options: Object.keys(ModalButtonsAlignment),
      mapping: ModalButtonsAlignment,
    },
    isFullScreen: {
      control: 'boolean',
    },
    showBackButton: {
      control: 'boolean',
    },
    hideCloseButton: {
      control: 'boolean',
    },
  },
};

export default meta;

type Story = StoryObj<typeof Modal>;

const withRocketPortal = (Story: React.FunctionComponent) => (
  <div id="styleguide-react-portal-modal">
    <Story />
  </div>
);

const ModalTypes = {
  Default: undefined,
  primary: GlobalType.primary,
  secondary: GlobalType.secondary,
  success: GlobalType.success,
  warning: GlobalType.warning,
  error: GlobalType.error,
};

export const ModalSimple: Story = {
  render: props => {
    return (
      <div id="styleguide-react-portal-modal">
        <Modal {...props}>
          <ModalStyledText marginBottom="16">
            {'Modal Simple without animation'} <a href="#">This is a link</a>
          </ModalStyledText>
        </Modal>
      </div>
    );
  },
};

export const ModalWithAnimation: Story = {
  args: {
    width: 320,
    height: 200,
    type: GlobalType.primary,
    title: 'Modal title',
    animationEnabled: true,
    hideCloseButton: false,
    onClickClose: fn(),
    showBackButton: false,
    isFullScreen: false,
    size: 'small',
    children: 'Modal Simple with animation',
  },
  decorators: [withRocketPortal],
};

export const ModalSandbox: Story = {
  args: {
    height: 472,
    title: 'Modify modal title',
    children: 'Modify modal text',
  },
  decorators: [withRocketPortal],
};
