import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import ToggleContainer from '../src/components/ToggleContainer/ToggleContainer';
import FlexLayout from '../src/components/Layout/FlexLayout';
import Icon, { IconFamily, IconType } from '../src/components/Icons';

export default {
  title: 'ToggleContainer',
  component: ToggleContainer,
  parameters: {
    layout: 'centered',
  },
  argTypes: {
    buttonCloseText: {
      control: 'text',
    },
    buttonOpenText: {
      control: 'text',
    },
    initiallyOpen: {
      control: 'boolean',
    },
  },
} as Meta<typeof ToggleContainer>;

type Story = StoryObj<typeof ToggleContainer>;

const Element = () => {
  return (
    <FlexLayout
      mainaxis="row"
      alignSelf="center"
      mainaxisAlignment="center"
      crossaxisAlignment="center"
    >
      <Icon type={IconType.android} family={IconFamily.brands} />
      <FlexLayout mainaxis="column">
        <b>device name</b>
        <p>device status</p>
      </FlexLayout>
    </FlexLayout>
  );
};

export const ToggleContainerStory: Story = {
  args: {
    buttonCloseText: 'close',
    buttonOpenText: 'open',
    initiallyOpen: false,
  },
  render: ({ buttonCloseText, buttonOpenText, initiallyOpen }) => (
    <ToggleContainer
      buttonCloseText={buttonCloseText}
      buttonOpenText={buttonOpenText}
      initiallyOpen={initiallyOpen}
    >
      <FlexLayout
        mainaxis="row"
        mainaxisAlignment="space-evenly"
        crossaxisAlignment="center"
      >
        <Element />
        <Element />
        <Element />
      </FlexLayout>
      <FlexLayout
        mainaxis="row"
        mainaxisAlignment="space-evenly"
        crossaxisAlignment="center"
      >
        <Element />
        <Element />
        <Element />
      </FlexLayout>
    </ToggleContainer>
  ),
};

export const ToggleContainerInitiallyOpenedStory: Story = {
  args: {
    buttonCloseText: 'close',
    buttonOpenText: 'open',
    initiallyOpen: true,
  },
  render: ({ buttonCloseText, buttonOpenText, initiallyOpen }) => (
    <ToggleContainer
      buttonCloseText={buttonCloseText}
      buttonOpenText={buttonOpenText}
      initiallyOpen={initiallyOpen}
    >
      <FlexLayout
        mainaxis="row"
        mainaxisAlignment="space-evenly"
        crossaxisAlignment="center"
      >
        <Element />
        <Element />
        <Element />
      </FlexLayout>
    </ToggleContainer>
  ),
};

export const ToggleContainerWithScrollStory: Story = {
  args: {
    buttonCloseText: 'close',
    buttonOpenText: 'open',
    initiallyOpen: false,
  },
  render: ({ buttonCloseText, buttonOpenText, initiallyOpen }) => (
    <ToggleContainer
      buttonCloseText={buttonCloseText}
      buttonOpenText={buttonOpenText}
      initiallyOpen={initiallyOpen}
    >
      {[...Array(15)].map((_, index) => (
        <FlexLayout
          key={index}
          mainaxis="row"
          mainaxisAlignment="space-evenly"
          crossaxisAlignment="center"
        >
          <Element />
          <Element />
          <Element />
        </FlexLayout>
      ))}
    </ToggleContainer>
  ),
};
