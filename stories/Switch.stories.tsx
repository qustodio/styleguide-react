import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { Switch } from '../src';

const listElementStyle = {
  height: '50px',
  display: 'flex',
  alignItems: 'center',
  margin: '10px',
};

const SwitchContainer = () => {
  const [isChecked, setIsChecked] = useState(false);
  const handleClick = () => {
    setIsChecked(!isChecked);
  };
  return (
    <div style={listElementStyle}>
      <Switch
        id="switch"
        checked={isChecked}
        onClick={handleClick}
        onChange={() => {}}
        disabled={false}
      />
    </div>
  );
};

const SwitchDisabledContainer = () => {
  return (
    <ul style={{ listStyleType: 'none', padding: 0, margin: 0 }}>
      <li style={listElementStyle}>
        <Switch
          id="switch"
          checked={false}
          disabled={true}
          onChange={() => {}}
        />
      </li>
      <li style={listElementStyle}>
        <Switch
          id="switch"
          checked={true}
          disabled={true}
          onChange={() => {}}
        />
      </li>
    </ul>
  );
};

const SwitchListContainer = () => {
  const [isChecked, setIsChecked] = useState(false);
  const [isChecked2, setIsChecked2] = useState(true);
  const handleClick = (
    currentValue: boolean,
    setValue: (val: boolean) => void
  ) => {
    setValue(!currentValue);
  };
  return (
    <ul style={{ listStyleType: 'none', padding: 0, margin: 0 }}>
      <li style={listElementStyle}>
        <Switch
          id="switch"
          checked={isChecked}
          onClick={() => handleClick(isChecked, setIsChecked)}
          disabled={false}
          onChange={() => {}}
        />
      </li>
      <li style={listElementStyle}>
        <Switch
          id="switch2"
          checked={isChecked2}
          onClick={() => handleClick(isChecked2, setIsChecked2)}
          disabled={false}
          onChange={() => {}}
        />
      </li>
    </ul>
  );
};

const meta: Meta = {
  title: 'Switch',
  component: Switch,
  decorators: [
    Story => (
      <div style={{ margin: '16px' }}>
        <Story />
      </div>
    ),
  ],
};

export default meta;

type Story = StoryObj<typeof Switch>;

export const Default: Story = {
  render: () => <SwitchContainer />,
};

export const SwitchDisabled: Story = {
  name: 'Switch Disabled',
  render: () => <SwitchDisabledContainer />,
};

export const SwitchList: Story = {
  name: 'List of Switches',
  render: () => <SwitchListContainer />,
};
