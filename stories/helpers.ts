type Func<I, O> = (x: I) => O;

const of = <I>(x: I) => [x];

const ap = <I, O>(fns: Func<I, O>[], values: I[]) =>
  fns.flatMap(f => values.map(value => f(value)));

export const lift = <T>(f: Func<any, any>) => (...params: any[]) =>
  params.reduce((fc, fx) => ap(fc, fx), of(f)) as T;
