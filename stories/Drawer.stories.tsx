import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { Drawer, useToggle } from '../src';

export default {
  title: 'Drawer',
  component: Drawer,
  args: {
    from: 'right',
    isOpen: false,
    overlay: true,
  },
} as Meta<typeof Drawer>;

type Story = StoryObj<typeof Drawer>;

const useDelay = (time: number, action: () => void) => {
  return React.useEffect(() => {
    const id = setTimeout(action, time);
    return () => clearTimeout(id);
  }, []);
};

const Container: React.FC = ({ children }) => (
  <div
    style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      minHeight: '100vh',
    }}
  >
    {children}
  </div>
);

const buttonStyle = {
  background: 'none',
  border: 'solid #0000002e 1px',
  padding: '10px',
  cursor: 'pointer',
};

const Button = ({
  onClick,
  isOpen,
}: {
  onClick: () => void;
  isOpen: boolean;
}) => (
  <button style={buttonStyle} onClick={onClick}>
    {isOpen ? 'Close Drawer' : 'Open Drawer'}
  </button>
);

const Content = ({ toggle }: { toggle: () => void }) => (
  <div
    style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      border: 'solid #0000002e 1px',
      padding: '10px',
      width: '200px',
      minHeight: '80vh',
      backgroundColor: 'white',
    }}
  >
    <div>
      <button style={buttonStyle} onClick={toggle}>
        Close
      </button>
    </div>
  </div>
);

export const DrawerFromRight: Story = {
  render: props => {
    const [isOpen, { toggle }] = useToggle(false);
    useDelay(300, toggle);

    return (
      <Container>
        <Button onClick={toggle} isOpen={isOpen} />

        <Drawer {...props} isOpen={isOpen} from="right">
          <Content toggle={toggle} />
        </Drawer>
      </Container>
    );
  },
};

export const DrawerFromLeft: Story = {
  render: props => {
    const [isOpen, { toggle }] = useToggle(false);
    useDelay(300, toggle);

    return (
      <Container>
        <Button onClick={toggle} isOpen={isOpen} />

        <Drawer {...props} isOpen={isOpen} from="left">
          <Content toggle={toggle} />
        </Drawer>
      </Container>
    );
  },
};

export const MultiDrawer: Story = {
  render: props => {
    const [isOpen, { toggle }] = useToggle(false);
    useDelay(300, toggle);

    return (
      <Container>
        <Button onClick={toggle} isOpen={isOpen} />

        <Drawer {...props} isOpen={isOpen} from="left" id="rck-drawer-left">
          <Content toggle={toggle} />
        </Drawer>

        <Drawer
          isOpen={isOpen}
          from="right"
          id="rck-drawer-right"
          overlay={false}
        >
          <Content toggle={toggle} />
        </Drawer>
      </Container>
    );
  },
};
