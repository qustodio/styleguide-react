import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import TimePickerInput from '../src/components/TimePickerInput/TimePickerInput';
import DropdownOption from '../src/components/Dropdown/DropdownOption';
import Dropdown from '../src/components/Dropdown/Dropdown';

export default {
  title: 'TimePickerInput',
  component: TimePickerInput,
  argTypes: {
    hasError: {
      control: 'boolean',
    },
    disabled: {
      control: 'boolean',
    },
    readOnly: {
      control: 'boolean',
    },
    placeholder: {
      control: 'text',
    },
  },
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof TimePickerInput>;

type Story = StoryObj<typeof TimePickerInput>;

export const TimePickerInputStory: Story = {
  render: ({ hasError, disabled, readOnly }) => (
    <div style={{ width: '300px', padding: 32 }}>
      <TimePickerInput
        hasError={hasError}
        disabled={disabled}
        readOnly={readOnly}
        onChange={action('onChange')}
        onClickTrigger={action('onClickTrigger')}
        testId="custom-test-id"
      />
    </div>
  ),
};

const TimePickerDropDownStory = () => {
  const getActiveValue = () => `Option ${selectedOption}`;
  const [selectedOption, setSelectedOption] = React.useState(10);
  const [inputValue, setInputValue] = React.useState(getActiveValue());
  React.useEffect(() => {
    setInputValue(getActiveValue());
  }, [selectedOption]);

  return (
    <Dropdown
      maxWidth={400}
      actionElement={
        <TimePickerInput
          value={inputValue}
          onChange={value => setInputValue(value)}
          hasError={false}
          disabled={false}
          readOnly={false}
        />
      }
      maxHeight={240}
      onChange={value => setSelectedOption(value as number)}
      portalId="test"
    >
      {Array.from({ length: 40 }).map((_, num) => (
        <DropdownOption
          key={num}
          text={`Option ${num + 1}`}
          value={num + 1}
          selected={num + 1 === selectedOption}
        />
      ))}
    </Dropdown>
  );
};

export const WithDropdown: Story = {
  render: () => (
    <div style={{ width: '300px', padding: 32 }}>
      <div id="test" />
      <TimePickerDropDownStory />
    </div>
  ),
};
