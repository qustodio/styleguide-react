import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import DeactivableContent from '../src/components/DeactivableContent/DeactivableContent';

export default {
  title: 'DeactivableContent',
  parameters: {
    layout: 'centered',
  },
  component: DeactivableContent,
  args: {
    disabled: false,
  },
} as Meta<typeof DeactivableContent>;

type Story = StoryObj<typeof DeactivableContent>;

export const Default: Story = {
  render: ({ disabled }) => (
    <DeactivableContent disabled={disabled}>
      <p style={{ margin: '0', padding: '0' }}>content 1</p>
      <button>click me</button>
      <p style={{ margin: '0', padding: '0' }}>content 2</p>
    </DeactivableContent>
  ),
};
