import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Spinner from '../src/components/Spinner/Spinner';

const meta: Meta<typeof Spinner> = {
  title: 'Spinner',
  component: Spinner,
};

export default meta;

type Story = StoryObj<typeof Spinner>;

export const Default: Story = {
  args: {
    size: 'small',
    color: 'regular',
  },
  decorators: [
    Story => (
      <div
        style={{
          backgroundColor: 'black',
          width: '100%',
          height: '400px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Story />
      </div>
    ),
  ],
};
