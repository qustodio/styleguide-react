import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Checkbox from '../src/components/Checkbox/Checkbox';
import { fn } from '@storybook/test';

export default {
  title: 'Checkbox',
  parameters: {
    layout: 'centered',
  },
  component: Checkbox,
} as Meta<typeof Checkbox>;

type Story = StoryObj<typeof Checkbox>;

export const CheckboxStory: Story = {
  render: () => {
    const [isChecked, setIsChecked] = useState(true);

    const handleClick = () => {
      setIsChecked(!isChecked);
    };

    return (
      <div style={{ backgroundColor: '#e8fff7' }}>
        <Checkbox
          name="test"
          label="Checkbox text"
          block={false}
          checked={isChecked}
          onClick={handleClick}
        ></Checkbox>
      </div>
    );
  },
};

export const CheckboxDisabled: Story = {
  render: () => (
    <div style={{ backgroundColor: '#e8fff7' }}>
      <Checkbox
        name="test"
        label="Checkbox text"
        disabled
        checked
        onClick={fn}
      ></Checkbox>
    </div>
  ),
};
