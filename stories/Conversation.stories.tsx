import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Conversation, {
  ConversationProps,
} from '../src/components/Conversation/Conversation';
import { AlertBox, AlertBoxType, IconType } from '../src';

export default {
  title: 'Conversation',
  parameters: {
    layout: 'centered',
  },
  component: Conversation,
} as Meta<typeof Conversation>;

type Story = StoryObj<typeof Conversation>;

const simpleConversation: ConversationProps['conversation'] = [
  {
    date: '2024-09-12',
    messages: [
      {
        id: '1',
        contactId: 'uid-1',
        createdAt: 1694517900000,
        content: 'I’ve heard some things, Barzini.',
        outgoing: true,
      },
      {
        id: '1-2',
        contactId: 'uid-1',
        createdAt: 1694517900000,
        content: 'It concerns me.',
        outgoing: true,
      },
      {
        id: '1-3',
        contactId: 'uid-1',
        createdAt: 1694517900000,
        content:
          'We should settle this peacefully, with respect. What do you have to say about these rumors?',
        outgoing: true,
      },
      {
        id: '2',
        contactId: 'uid-2',
        createdAt: 1694518020000,
        content: 'Rumors? Don Corleone',
        outgoing: false,
      },
      {
        id: '2-1',
        contactId: 'uid-2',
        createdAt: 1694518020000,
        content:
          'I don’t deal in rumors. I deal in facts. And the fact is, your time is slipping.',
        outgoing: false,
      },
      {
        id: '2-1',
        contactId: 'uid-2',
        createdAt: 1694518020000,
        content: 'The families are starting to see the cracks in your power.',
        outgoing: false,
        highlight: 'warning',
      },
      {
        id: '3',
        contactId: 'uid-3',
        createdAt: 1694518140000,
        content:
          'Mr. Barzini, Don Corleone is still the most respected man in the business. You would be wise to show him the proper respect.',
        outgoing: false,
      },
      {
        id: '4',
        contactId: 'uid-1',
        createdAt: 1694518260000,
        content:
          'Luca, there’s no need for that. Barzini, you and I both know where we stand. Let’s not pretend there’s nothing behind these whispers. The drug trade, the territory grabs...',
        outgoing: true,
      },
      {
        id: '4-1',
        contactId: 'uid-1',
        createdAt: 1694518260000,
        content: 'You’re reaching too far, too quickly.',
        outgoing: true,
      },
      {
        id: '5',
        contactId: 'uid-2',
        createdAt: 1694518320000,
        content:
          "Times are changing, Don. The old ways are… outdated. We can't ignore new opportunities forever. Maybe it's time we both adapt.",
        outgoing: false,
      },
      {
        id: '6',
        contactId: 'uid-3',
        createdAt: 1694518440000,
        content:
          'Adapting doesn’t mean betraying alliances. You step out of line, Barzini, you’ll find yourself in a position you can’t escape.',
        outgoing: false,
      },
      {
        id: '7',
        contactId: 'uid-1',
        createdAt: 1694518560000,
        content:
          "Luca, please. Barzini knows the consequences. But I am a reasonable man. Barzini, let's end this conversation in a way that benefits us both. Perhaps we can meet and discuss a way forward… without hostility.",
        outgoing: true,
      },
      {
        id: '8',
        contactId: 'uid-2',
        createdAt: 1694518680000,
        content: 'Always the diplomat, Don.',
        outgoing: false,
      },
      {
        id: '8-1',
        contactId: 'uid-2',
        createdAt: 1694518680000,
        content:
          'Very well, a meeting it is. But make no mistake, the old ways will have to give way to the new, whether you’re ready or not.',
        outgoing: false,
      },
      {
        id: '9',
        contactId: 'uid-1',
        createdAt: 1694518800000,
        content: 'We’ll see about that, Barzini. Until we meet.',
        outgoing: true,
      },
    ],
  },
];

const complexConversation: ConversationProps['conversation'] = [
  {
    date: 'April 10 at 14:30',
    messages: [
      {
        id: '1',
        contactId: 'uid-3',
        createdAt: 1,
        content: 'buona sera',
        outgoing: false,
      },
      {
        id: '2',
        contactId: 'uid-3',
        createdAt: 1,
        content: 'Cosa dovremmo fare con questo tizio Barzini?',
        outgoing: false,
      },
    ],
  },
  {
    date: 'April 11 at 11:00',
    messages: [
      {
        id: '3',
        contactId: 'uid-1',
        createdAt: 2,
        content: "I'm going to make him an offer he can't refuse.",
        outgoing: true,
        highlight: 'warning',
      },
      {
        id: '4',
        contactId: 'uid-2',
        createdAt: 3,
        content: '🐀 > 🐟',
        outgoing: false,
      },
      {
        id: '5',
        contactId: 'uid-2',
        createdAt: 3,
        content: 'Luca Brasi sleeps with the fishes',
        outgoing: false,
      },
      {
        id: '6',
        contactId: 'uid-3',
        createdAt: 5,
        content: 'Luca brasi left the chat',
        outgoing: false,
        highlight: 'error',
      },
      {
        id: '12',
        contactId: 'uid-1',
        createdAt: 61,
        contentType: 'mixed',
        content: () => (
          <AlertBox
            showCloseIcon
            icon={IconType.sadTear}
            type={AlertBoxType.blue}
          >
            Updated status: "Feeling down"
          </AlertBox>
        ),
        outgoing: true,
      },
      {
        id: '13',
        contactId: 'uid-1',
        createdAt: 5,
        contentType: 'image',
        content:
          'https://s3.amazonaws.com/static-pre.qustodio.com/img/email/comm_educational_email_2/Img2.jpg',
        outgoing: true,
      },
    ],
  },
];

const contacts: ConversationProps['contacts'] = [
  {
    id: 'uid-1',
    name: 'Don Corleone',
    pictureUrl:
      'https://s3.amazonaws.com/static-pre.qustodio.com/img/email/comm_educational_email_2/Img2.jpg',
  },
  {
    id: 'uid-2',
    name: 'Barzini',
  },
  {
    id: 'uid-3',
    name: 'Luca Brasi',
  },
];

export const Simple: Story = {
  render: () => (
    <div style={{ width: 390, background: '#fafafa', height: 550 }}>
      <Conversation conversation={simpleConversation} contacts={contacts} />
    </div>
  ),
};

export const Mixed: Story = {
  render: () => (
    <div style={{ width: 390, background: '#fafafa', height: 550 }}>
      <Conversation conversation={complexConversation} contacts={contacts} />
    </div>
  ),
};

export const Responsive: Story = {
  render: () => (
    <div style={{ width: 800, background: '#fafafa', height: 700 }}>
      <Conversation conversation={complexConversation} contacts={contacts} />
    </div>
  ),
};
