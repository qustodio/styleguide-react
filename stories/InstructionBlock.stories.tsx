import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import InstructionBlock from '../src/components/InstructionBlock/InstructionBlock';
import { StyledInstruction } from '../src';

export default {
  title: 'InstructionBlock',
  component: InstructionBlock,
  parameters: {
    layout: 'centered',
  },
  argTypes: {
    block: {
      control: { type: 'boolean' },
    },
  },
} as Meta<typeof InstructionBlock>;

type Story = StoryObj<typeof InstructionBlock>;

export const InstructionWithStrongAndLink: Story = {
  render: ({ block }) => (
    <InstructionBlock block={block}>
      This is a instruction <strong>with some strong text</strong> and this{' '}
      <a href="#">link to anywhere</a>
    </InstructionBlock>
  ),
};

export const InstructionWithStyledInstruction: Story = {
  render: ({ block }) => (
    <InstructionBlock block={false}>
      This is a instruction.
      <br />
      <StyledInstruction color="error" block={block}>
        This is a styled instruction.
      </StyledInstruction>
    </InstructionBlock>
  ),
};
