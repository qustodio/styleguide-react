import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import TextField from '../src/components/TextField/TextField';
import Icon, { IconType } from '../src/components/Icons';

export default {
  title: 'Text Fields',
  component: TextField,
  argTypes: {
    label: { control: 'text' },
    disabled: { control: 'boolean' },
    block: { control: 'boolean' },
    canBeCleared: { control: 'boolean' },
    type: {
      control: { type: 'select' },
      options: ['text', 'email', 'password'],
    },
    error: { control: 'boolean' },
    placeholder: { control: 'text' },
    iconLeft: { control: false },
    supportTextConfiguration: { control: 'object' },
    onIconClick: { action: 'onIconClick' },
    onChange: { action: 'onChange' },
    onBlur: { action: 'onBlur' },
    onDragStart: { action: 'onDragStart' },
    onFocus: { action: 'onFocus' },
    onDrop: { action: 'onDrop' },
    size: { control: { type: 'select' }, options: ['small', 'regular'] },
  },
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof TextField>;

type Story = StoryObj<typeof TextField>;

type IconName = keyof typeof IconType;

const getIcon = (iconName: IconName | 'none') => {
  if (iconName === 'none') {
    return undefined;
  }
  return <Icon type={IconType[iconName] as IconType} />;
};

export const TextFieldStory: Story = {
  render: () => {
    const iconLeftName = 'none';
    const size = 'regular';
    return (
      <div style={{ width: '300px' }}>
        <TextField
          iconLeft={getIcon(iconLeftName)}
          name={''}
          size={size}
          label="Label"
          disabled={false}
          block={false}
          canBeCleared={true}
          type="text"
          error={false}
          placeholder=""
          supportTextConfiguration={undefined}
          onIconClick={action('on icon click')}
          onChange={action('on change')}
          onBlur={action('on blur')}
          onDragStart={action('on drag start')}
          onFocus={action('on focus')}
          onDrop={action('on drop')}
        />
      </div>
    );
  },
};

export const TextFieldsSizes: Story = {
  render: () => (
    <div style={{ margin: '10px' }}>
      <h2>Small</h2>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          gap: '10px',
        }}
      >
        <TextField
          name={''}
          size="small"
          placeholder="dummy text"
          type="text"
          iconLeft={getIcon('search')}
        />
        <TextField
          name={''}
          size="small"
          placeholder="dummy text"
          type="text"
          canBeCleared={true}
        />
        <TextField
          size="small"
          placeholder="dummy text"
          type="text"
          name={''}
        />
        <TextField
          name={''}
          size="small"
          placeholder="dummy text"
          type="text"
          iconLeft={getIcon('search')}
          canBeCleared={true}
        />
      </div>
      <br />
      <h2>Regular</h2>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          gap: '10px',
        }}
      >
        <TextField
          size="regular"
          placeholder="dummy text"
          type="text"
          name={''}
        />
        <TextField
          name={''}
          size="regular"
          placeholder="dummy text"
          type="text"
          iconLeft={getIcon('search')}
        />
        <TextField
          name={''}
          size="regular"
          placeholder="dummy text"
          type="text"
          canBeCleared={true}
        />
        <TextField
          name={''}
          size="regular"
          placeholder="dummy text"
          type="text"
          iconLeft={getIcon('search')}
          canBeCleared={true}
        />
      </div>
    </div>
  ),
};

export const TextFieldsStates: Story = {
  render: () => (
    <div style={{ margin: '10px' }}>
      <h2>States</h2>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          gap: '10px',
        }}
      >
        <TextField
          name={''}
          size="regular"
          placeholder="Disabled"
          type="text"
          iconLeft={getIcon('search')}
          canBeCleared={true}
          disabled={true}
        />
        <TextField
          name={''}
          size="small"
          placeholder="Disabled"
          type="text"
          iconLeft={getIcon('search')}
          canBeCleared={true}
          disabled={true}
        />
      </div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          gap: '10px',
        }}
      >
        <TextField
          name={''}
          size="regular"
          placeholder="Error"
          type="text"
          iconLeft={getIcon('search')}
          canBeCleared={true}
          error={true}
        />
        <TextField
          name={''}
          size="small"
          placeholder="Error"
          type="text"
          iconLeft={getIcon('search')}
          canBeCleared={true}
          error={true}
        />
      </div>
    </div>
  ),
};

export const TextFieldsTextCentered: Story = {
  render: () => (
    <div style={{ margin: '10px' }}>
      <h2>States</h2>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          gap: '10px',
        }}
      >
        <TextField
          name={''}
          size="regular"
          placeholder="dummy text"
          type="text"
          textCentered={true}
        />
        <TextField
          name={''}
          size="small"
          placeholder="dummy text"
          type="text"
          canBeCleared={true}
          textCentered={true}
        />
      </div>
    </div>
  ),
};

const AutoFocusInputComponent = () => {
  const ref = React.useRef<HTMLInputElement>(null);
  React.useEffect(() => {
    if (ref?.current) ref.current.focus();
  }, []);

  return (
    <div style={{ width: '300px' }}>
      <TextField
        name={''}
        ref={ref}
        type="text"
        label="Autofocus"
        placeholder="Your text..."
      />
    </div>
  );
};
export const TextFieldAutofocus: Story = {
  render: () => <AutoFocusInputComponent />,
};
