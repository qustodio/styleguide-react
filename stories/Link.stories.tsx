import React from 'react';
import { Meta, StoryObj } from '@storybook/react';

import Link from '../src/components/Link/Link';
import Icon, { IconType } from '../src/components/Icons/index';
import { fn } from '@storybook/test';
import { Layout } from '../src';

export default {
  title: 'Link',
  component: Link,
  parameters: {
    layout: 'centered',
  },
  args: {
    icon: <Icon type={IconType.plus} />,
    iconPosition: 'left',
    disabled: false,
    href: 'https://google.com',
    children: 'This is the link content',
    onClick: fn(),
  }
} as Meta<typeof Link>;

type Story = StoryObj<typeof Link>;

export const Default: Story = {};
