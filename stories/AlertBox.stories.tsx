import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import AlertBox, { AlertBoxType } from '../src/components/AlertBox';
import { fn } from '@storybook/test';
import { Icon, IconFamily, IconSize, IconType } from '../src';

const alertBoxTypes = {
  error: AlertBoxType.error,
  warning: AlertBoxType.warning,
  success: AlertBoxType.success,
  info: AlertBoxType.info,
  booster: AlertBoxType.booster,
  blue: AlertBoxType.blue,
};

const StoryWrapper = (Story: () => React.ReactElement) => (
  <div style={{ margin: 24, marginTop: 48 }}>
    <Story />
  </div>
);

const meta: Meta<typeof AlertBox> = {
  title: 'AlertBox',
  component: AlertBox,
  parameters: {
    layout: 'centered',
  },
  args: {
    children: 'I am a very cool alert!',
    centered: false,
    solid: false,
    rounded: true,
    fullWidth: true,
    showInfoIcon: false,
    showCloseIcon: false,
    onClose: fn(),
  },
  argTypes: {
    type: {
      control: { type: 'select' },
      options: Object.keys(AlertBoxType),
      mapping: Object.values(AlertBoxType),
    },
  },
};

export default meta;

type Story = StoryObj<typeof AlertBox>;

export const Default: Story = {
  decorators: [StoryWrapper],
};

export const AlertBoxTypes: Story = {
  render: props => (
    <>
      {Object.keys(AlertBoxType).map(type => (
        <div key={type} style={{ marginBottom: 24 }}>
          <AlertBox {...props} type={type as AlertBoxType}>
            I am {type} alert!
          </AlertBox>
        </div>
      ))}

      <br />
      <p>
        <small>Solid colors:</small>
      </p>
      {Object.keys(AlertBoxType).map(type => (
        <div key={type} style={{ marginBottom: 24 }}>
          <AlertBox {...props} type={type as AlertBoxType}>
            I am a solid {type} alert!
          </AlertBox>
        </div>
      ))}
    </>
  ),
};

export const AlertBoxLeftIconIconType: Story = {
  render: props => (
    <>
      {Object.keys(AlertBoxType).map(type => (
        <div key={type} style={{ marginBottom: 24 }}>
          <AlertBox
            {...props}
            type={type as AlertBoxType}
            showInfoIcon
            fullWidth
            icon={IconType.infoCircle}
          >
            {'I am an alert with a custom left icon'}
          </AlertBox>
        </div>
      ))}
    </>
  ),
};

export const AlertBoxLeftIconCustomIcon: Story = {
  name: 'AlertBox Left Icon / Custom icon',
  render: props => (
    <>
      {Object.keys(AlertBoxType).map(type => (
        <div key={type} style={{ marginBottom: 24 }}>
          <AlertBox
            {...props}
            type={type as AlertBoxType}
            fullWidth
            icon={
              <Icon
                type={IconType.plane}
                family={IconFamily.solid}
                size={IconSize.x10}
              />
            }
          >
            {'I am an alert with a custom left icon'}
          </AlertBox>
        </div>
      ))}
    </>
  ),
};

export const AlertBoxLeftIconInfoIcon: Story = {
  name: 'AlertBox Left Icon / Info icon',
  render: props => (
    <>
      <AlertBox {...props} showInfoIcon>
        {'I am an alert with an info icon'}
      </AlertBox>
      <br />
      <br />
      <p>
        <small>
          <strong>showCloseIcon</strong> prop will be ignored if{' '}
          <strong>icon</strong> prop is provided
        </small>
      </p>
    </>
  ),
};

export const AlertBoxCloseIcon: Story = {
  name: 'AlertBox Close Icon',
  decorators: [StoryWrapper],
  args: {
    showCloseIcon: true,
    children: 'I am an alert with a close icon',
  },
};

export const AlertBoxCentered: Story = {
  name: 'AlertBox Centered',
  args: {
    showInfoIcon: false,
    showCloseIcon: false,
    centered: true,
    fullWidth: true,
    children: 'I am a centered alertbox',
  },
};

export const AlertBoxLongText: Story = {
  name: 'AlertBox Long Text',
  decorators: [StoryWrapper],
  args: {
    centered: false,
    fullWidth: true,
    showInfoIcon: false,
    showCloseIcon: false,
    children:
      'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, quia dolor sit amet, consectetur, adipisci velit Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, quia dolor sit amet, consectetur, adipisci velit Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, quia dolor sit amet, consectetur, adipisci velit',
  },
};
