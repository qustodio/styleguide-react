import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import {
  ExpandCollapse,
  FlexLayout,
  Icon,
  IconFamily,
  IconType,
  RadioButton,
  Text,
} from '../src';
import headerImage from './assets/images/Icons/icon-youtube.svg';
import ExpandCollapseRowItem from '../src/components/ExpandCollapseRowItem/ExpandCollapseRowItem';

export default {
  title: 'ExpandCollapse',
  component: ExpandCollapse,
  args: {},
} as Meta<typeof ExpandCollapse>;

type Story = StoryObj<typeof ExpandCollapse>;

const getSubtitle = () => (
  <>
    <Icon type={IconType.apple} family={IconFamily.brands} />
    <Text variant="caption-1-regular">IOS | Ages 99 +</Text>
  </>
);

const getRightElement = () => (
  <>
    <RadioButton id="1" value="1" onClick={() => console.log('clicked')} />
  </>
);

const infoActionTest = () => {
  console.log('active info icon');
};

export const expandCollapse: Story = {
  render: props => {
    return (
      <ExpandCollapse
        leftIcon={IconType.bell}
        title="I am a title"
        rightText="9999"
        testId="expand-collapse"
      >
        <ExpandCollapseRowItem
          iconSrc={headerImage}
          title="APP 1"
          subtitle={getSubtitle()}
          rightElement={getRightElement()}
        />
        <ExpandCollapseRowItem
          iconSrc={headerImage}
          title="APP 2"
          subtitle={getSubtitle()}
          rightElement={getRightElement()}
        />
        <ExpandCollapseRowItem
          iconSrc={headerImage}
          title="APP 3"
          subtitle={getSubtitle()}
          rightElement={getRightElement()}
        />
      </ExpandCollapse>
    );
  },
};

export const expandCollapseWithImage: Story = {
  render: props => {
    return (
      <ExpandCollapse
        leftImgSrc={headerImage}
        title="I am a title"
        rightText="9999"
        testId="expand-collapse"
      >
        <ExpandCollapseRowItem
          iconSrc={headerImage}
          title="APP 1"
          subtitle={getSubtitle()}
          rightElement={getRightElement()}
        />
        <ExpandCollapseRowItem
          iconSrc={headerImage}
          title="APP 2"
          subtitle={getSubtitle()}
          rightElement={getRightElement()}
        />
        <ExpandCollapseRowItem
          iconSrc={headerImage}
          title="APP 3"
          subtitle={getSubtitle()}
          rightElement={getRightElement()}
        />
      </ExpandCollapse>
    );
  },
};

export const expandCollapseDisabled: Story = {
  render: props => {
    return (
      <>
        <ExpandCollapse
          leftImgSrc={headerImage}
          title="I am a title"
          rightText="9999"
          testId="expand-collapse"
          isDisabled
          initialOpenState
        >
          <ExpandCollapseRowItem
            iconSrc={headerImage}
            title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac bibendum metus."
            subtitle={getSubtitle()}
            rightElement={getRightElement()}
            isDisabled
          />
        </ExpandCollapse>
        <ExpandCollapse
          leftIcon={IconType.bell}
          title="I am a title"
          rightText="9999"
          testId="expand-collapse"
          isDisabled
        ></ExpandCollapse>
      </>
    );
  },
};

export const expandCollapseWithRightElement: Story = {
  render: props => {
    return (
      <>
        <ExpandCollapse
          leftImgSrc={headerImage}
          title="I am a title"
          rightElement={<Text variant="title-2-semibold-responsive">999</Text>}
          testId="expand-collapse"
        ></ExpandCollapse>
        <ExpandCollapse
          leftIcon={IconType.bell}
          title="I am a title"
          rightElement={
            <Text variant="title-2-semibold-responsive">
              9
              <Text color="grey-400" variant="body-2-regular" renderAs="span">
                /999
              </Text>
            </Text>
          }
          testId="expand-collapse"
        ></ExpandCollapse>
      </>
    );
  },
};

export const expandCollapseWithInfoAction: Story = {
  render: props => {
    return (
      <>
        <ExpandCollapse
          leftIcon={IconType.bell}
          title="I am a title"
          rightElement={
            <Text variant="title-2-semibold-responsive">
              9
              <Text color="grey-400" variant="body-2-regular" renderAs="span">
                /999
              </Text>
            </Text>
          }
          testId="expand-collapse"
        >
          <ExpandCollapseRowItem
            iconSrc={headerImage}
            title="APP 1"
            subtitle={getSubtitle()}
            rightElement={getRightElement()}
            onInfoActionClick={infoActionTest}
          />
          <ExpandCollapseRowItem
            iconSrc={headerImage}
            title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac bibendum metus."
            subtitle={getSubtitle()}
            rightElement={getRightElement()}
            onInfoActionClick={infoActionTest}
          />
          <ExpandCollapseRowItem
            iconSrc={headerImage}
            title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac bibendum metus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac bibendum metus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac bibendum metus."
            subtitle={getSubtitle()}
            rightElement={getRightElement()}
            onInfoActionClick={infoActionTest}
          />
        </ExpandCollapse>
        <ExpandCollapse
          leftIcon={IconType.bell}
          title="disabled expand"
          initialOpenState
          isDisabled
          rightElement={
            <Text variant="title-2-semibold-responsive">
              9
              <Text color="grey-400" variant="body-2-regular" renderAs="span">
                999
              </Text>
            </Text>
          }
          testId="expand-collapse"
        >
          <ExpandCollapseRowItem
            iconSrc={headerImage}
            title="APP 1"
            subtitle={getSubtitle()}
            rightElement={getRightElement()}
            onInfoActionClick={infoActionTest}
            isDisabled
          />
          <ExpandCollapseRowItem
            iconSrc={headerImage}
            title="APP 2"
            subtitle={getSubtitle()}
            rightElement={getRightElement()}
            onInfoActionClick={infoActionTest}
            isDisabled
          />
          <ExpandCollapseRowItem
            iconSrc={headerImage}
            title="No action"
            subtitle={getSubtitle()}
            rightElement={getRightElement()}
            isDisabled
          />
        </ExpandCollapse>
        <ExpandCollapse
          leftIcon={IconType.bell}
          title="without info action"
          rightElement={<Text variant="title-2-regular-responsive">9</Text>}
          testId="expand-collapse"
        >
          <ExpandCollapseRowItem
            iconSrc={headerImage}
            title="APP 1"
            subtitle={getSubtitle()}
            rightElement={getRightElement()}
          />
        </ExpandCollapse>
      </>
    );
  },
};
