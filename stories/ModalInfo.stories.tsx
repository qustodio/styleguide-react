import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import ModalInfo from '../src/components/ModalInfo/ModalInfo';
import Button, { ButtonSize } from '../src/components/Button/Button';
import headerImage1 from './assets/images/Modals/modal_illustration.png';
import Icon, { IconType } from '../src/components/Icons';
import { AlertBoxType } from '../src';

const meta: Meta = {
  title: 'ModalInfo',
  component: ModalInfo,
};

export default meta;

type Story = StoryObj<typeof ModalInfo>;

const ICON_CHOICES = {
  none: false,
  question: {
    type: Icon,
    props: {
      type: IconType.questionCircle,
    },
  },
};

const ILLUSTRATION_CHOICES = {
  none: false,
  illustration1: headerImage1,
};

export const ModalWithAllOptions: Story = {
  render: ({ size, showBackButton, hideCloseButton }) => {
    const withHeader = false;
    const iconName = 'question';
    const illustrationName = 'none';

    const buttons = [
      <Button block size={ButtonSize.medium}>
        This is a button
      </Button>,
    ];

    const bodyContent = (
      <React.Fragment>{`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac neque
    est. Lorem ipsum dolor sit amet, consectetur adipiscing elit.`}</React.Fragment>
    );

    return (
      <div id="styleguide-react-portal-modal">
        <ModalInfo
          size={'small'}
          title={'Modal info title'}
          buttons={buttons}
          onClickClose={action('Close Modal')}
          animationEnabled={false}
          showBackButton={showBackButton}
          hideCloseButton={hideCloseButton}
          header={
            withHeader
              ? {
                  icon:
                    iconName === 'none'
                      ? undefined
                      : React.createElement(
                          ICON_CHOICES[iconName].type,
                          ICON_CHOICES[iconName].props
                        ),
                  illustration:
                    illustrationName === 'none'
                      ? undefined
                      : React.createElement('img', {
                          src: ILLUSTRATION_CHOICES[illustrationName],
                        }),
                }
              : null
          }
          addCancelButton={false}
          cancelButtonText="Cancel"
          alertBoxText={
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
          }
          alertBoxType={AlertBoxType.info}
        >
          {bodyContent}
        </ModalInfo>
      </div>
    );
  },
};
