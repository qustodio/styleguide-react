import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { BadgeType, GlobalType, SideNavigationMenu, Tag } from '../src';
import Icon, { IconType, IconFamily, IconSize } from '../src/components/Icons';
import { Link } from '@reach/router';
import AccountInfoHeader from '../src/components/AccountInfoHeader/AccountInfoHeader';
import LogoImage from './assets/images/Logo.svg';
import LicenseInfoContainer from '../src/components/LicenseInfoContainer/LicenseInfoContainer';

const styles = { height: '600px' };

const affiliateIconExample =
  'https://s3.amazonaws.com/static.qustodio.com/img/affiliates/q_vivo/white_logo.svg';

const SideNavigationMenuWithHeaderContainer = ({
  isAffiliate,
  showGift,
  expand,
}: {
  isAffiliate: boolean;
  showGift: boolean;
  expand: boolean;
}) => {
  const [pathTitleSelected, setPathTitleSelected] = useState<
    string | undefined
  >(undefined);

  const handleClick = (pathTitle: string) => setPathTitleSelected(pathTitle);

  return (
    <div style={styles}>
      <SideNavigationMenu
        accountInfo={
          <AccountInfoHeader
            name="foo"
            logoImage={<img src={LogoImage} />}
            showGift={showGift}
            onClickGift={() => {}}
            expand={false}
          />
        }
        licenseInfo={
          <LicenseInfoContainer
            licenseTitle="PLAN PREMIUM LARGE"
            licenseSubtitle={
              <span>
                Expires on <strong>July 20th, 2022</strong>
              </span>
            }
            licenseBadgeText="Upgrade"
            licenseBadgeType={BadgeType.secondary}
            licenseBadgeClick={() => {}}
            licenseLinkTo="#/upgrade"
            affiliateBadgeIconSrc={isAffiliate ? affiliateIconExample : null}
            affiliateBadgeDescription={'Vivo'}
          />
        }
        expand={expand}
        items={[
          {
            key: 'key1',
            title: 'Home',
            icon: (
              <Icon
                type={IconType.home}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 1',
            onClick: () => handleClick('option 1'),
            useAsLink: Link,
            linkTo: '/devices',
            shouldShow: true,
          },
          {
            key: 'key2',
            title: 'Devices',
            icon: (
              <Icon
                type={IconType.camera}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 2',
            onClick: () => handleClick('option 2'),
            useAsLink: Link,
            linkTo: '/account',
            shouldShow: true,
          },
          {
            key: 'key3',
            title: 'Account',
            icon: (
              <Icon
                type={IconType.settings}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 3',
            onClick: () => handleClick('option 3'),
            useAsLink: Link,
            linkTo: '/account',
            shouldShow: true,
          },
          {
            key: 'key4',
            title: 'Help',
            icon: (
              <Icon
                type={IconType.questionCircle}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 4',
            onClick: () => handleClick('option 4'),
            shouldShow: true,
          },
          {
            key: 'key5',
            title: 'Logout',
            icon: (
              <Icon
                type={IconType.signOut}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 5',
            onClick: () => handleClick('option 5'),
            useAsLink: Link,
            linkTo: '/logout',
            shouldShow: true,
          },
          {
            key: 'key6',
            title: 'Family Locator',
            icon: (
              <Icon
                type={IconType.mapMarkerAlt}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 6',
            onClick: () => handleClick('option 6'),
            shouldShow: true,
            showBadge: true,
            showAccent: true,
          },
        ]}
      />
    </div>
  );
};

const SideNavigationMenuContainer = ({
  showGift,
  expand,
}: {
  showGift: boolean;
  expand: boolean;
  isAffiliate: boolean;
}) => {
  const [pathTitleSelected, setPathTitleSelected] = useState<
    string | undefined
  >(undefined);
  const handleClick = (pathTitle: string) => setPathTitleSelected(pathTitle);
  return (
    <div style={styles}>
      <SideNavigationMenu
        accountInfo={
          <AccountInfoHeader
            name="foo"
            logoImage={<img src={LogoImage} />}
            showGift={showGift}
            onClickGift={() => {}}
            expand={false}
          />
        }
        expand={expand}
        items={[
          {
            key: 'key1',
            title: 'Your family',
            icon: (
              <Icon
                type={IconType.home}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 1',
            onClick: () => handleClick('option 1'),
            useAsLink: Link,
            linkTo: '/devices',
            shouldShow: true,
            badge: (
              <Tag
                text="New!"
                variant="rounded"
                type={GlobalType.marketing}
                invertColor
                size="small"
              />
            ),
          },
          {
            key: 'key2',
            title: 'Your devices',
            icon: (
              <Icon
                type={IconType.camera}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 2',
            onClick: () => handleClick('option 2'),
            useAsLink: Link,
            linkTo: '/account',
            shouldShow: true,
          },
          {
            key: 'key3',
            title: 'Your account',
            icon: (
              <Icon
                type={IconType.settings}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 3',
            onClick: () => handleClick('option 3'),
            useAsLink: Link,
            linkTo: '/account',
            shouldShow: true,
          },
          {
            key: 'key4',
            title: 'Help center',
            icon: (
              <Icon
                type={IconType.questionCircle}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 4',
            onClick: () => handleClick('option 4'),
            shouldShow: true,
            showAccent: true,
          },
          {
            key: 'key5',
            title: 'Logout',
            icon: (
              <Icon
                type={IconType.signOut}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 5',
            onClick: () => handleClick('option 5'),
            useAsLink: Link,
            linkTo: '/logout',
            shouldShow: true,
          },
          {
            key: 'key6',
            title: 'Family Locator',
            icon: (
              <Icon
                type={IconType.mapMarkerAlt}
                family={IconFamily.solid}
                size={IconSize.x2}
              />
            ),
            isActive: pathTitleSelected === 'option 6',
            onClick: () => handleClick('option 6'),
            shouldShow: true,
          },
        ]}
        userName={''}
        onClickGift={function(): void {
          throw new Error('Function not implemented.');
        }}
      />
    </div>
  );
};

const meta: Meta<typeof SideNavigationMenu> = {
  title: 'Side Navigation Menu',
  component: SideNavigationMenu,
  decorators: [
    Story => (
      <div style={{ margin: '16px' }}>
        <Story />
      </div>
    ),
  ],
};

export default meta;

type Story = StoryObj<typeof SideNavigationMenu>;

export const Default: Story = {
  render: ({ expand, showGift }) => (
    <SideNavigationMenuContainer
      expand={expand}
      showGift={!!showGift}
      isAffiliate={false}
    />
  ),
};

export const WithOptionals: Story = {
  args: {
    expand: true,
    showGift: false,
  },
  render: ({ expand, showGift }) => (
    <SideNavigationMenuWithHeaderContainer
      expand={expand}
      showGift={!!showGift}
      isAffiliate={true}
    />
  ),
};
