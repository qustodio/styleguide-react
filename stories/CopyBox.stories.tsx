import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { fn } from '@storybook/test';
import CopyBox from '../src/components/CopyBox/CopyBox';

export default {
  title: 'CopyBox',
  parameters: {
    layout: 'centered',
  },
  component: CopyBox,
} as Meta<typeof CopyBox>;

type Story = StoryObj<typeof CopyBox>;

export const Default: Story = {
  render: () => (
    <div
      style={{
        margin: '24px',
      }}
    >
      <CopyBox
        text="qustodio.com/downloads"
        url="https://qustodio.com/downloads"
        confirmationText="Copied to clipboard"
        onClickCallback={fn}
      />
    </div>
  ),
};
