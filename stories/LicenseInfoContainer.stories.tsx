import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { Link } from '@reach/router';
import LicenseInfoContainer from '../src/components/LicenseInfoContainer/LicenseInfoContainer';

export default {
  title: 'License info container',
  component: LicenseInfoContainer,
  argTypes: {
    licenseTitle: { control: 'text' },
    licenseSubtitle: { control: 'text' },
    licenseBadgeText: { control: 'text' },
    licenseBadgeClick: { action: 'clicked' },
    licenseLinkTo: { control: 'text' },
  },
} as Meta<typeof LicenseInfoContainer>;

type Story = StoryObj<typeof LicenseInfoContainer>;

export const BasicLicenseInfoContainer: Story = {
  render: () => (
    <div style={{ width: '250px', backgroundColor: '#3b9e8a' }}>
      <LicenseInfoContainer
        licenseTitle={'PLAN PREMIUM LARGE'}
        licenseSubtitle={
          ((
            <span>
              Expires on <strong>July 20th, 2022</strong>
            </span>
          ) as unknown) as string
        }
        licenseBadgeText="Upgrade"
        licenseBadgeClick={() => {}}
        licenseLinkTo="#/upgrade"
        useAsLink={Link}
      />
    </div>
  ),
};
