import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import AccordionMenu from '../src/components/AccordionMenu/AccordionMenu';

const meta: Meta<typeof AccordionMenu> = {
  title: 'AccordionMenu',
  component: AccordionMenu,
  parameters: {
    layout: 'centered',
  },
  args: {
    isVisible: true,
  },
};

export default meta;
type Story = StoryObj<typeof AccordionMenu>;

const styles = {
  container: {
    width: '255px',
    margin: '24px',
    border: '1px solid lightgrey',
    borderRadius: '6px',
  },
  list: {
    margin: 0,
    padding: 0,
  },
  button: {
    listStyleType: 'none',
    margin: 0,
    padding: '6px',
  },
  item: {
    borderTop: '1px solid lightgrey',
    listStyleType: 'none',
    margin: 0,
    padding: '6px',
  },
};

export const Default: Story = {
  render: ({ isVisible }) => (
    <div style={styles.container}>
      <p style={styles.button}>Use controls to open accordion menu</p>
      <AccordionMenu isVisible={isVisible}>
        <ul style={styles.list}>
          <li key="key1" style={styles.item}>
            Menu option 1
          </li>
          <li key="key2" style={styles.item}>
            Menu option 2
          </li>
          <li key="key3" style={styles.item}>
            Menu option 3
          </li>
          <li key="key4" style={styles.item}>
            Menu option 4
          </li>
        </ul>
      </AccordionMenu>
    </div>
  ),
  args: {},
};
