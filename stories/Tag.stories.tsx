import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Tag from '../src/components/Tag/Tag';
import { IconType } from '../src/components/Icons';
import { GlobalType } from '../src/common/types';
import { fn } from '@storybook/test';

export default {
  title: 'Tag',
  component: Tag,
  args: {
    onClick: fn(),
  },
  argTypes: {
    variant: { control: { type: 'select' }, options: ['squared', 'rounded'] },
    text: { control: 'text', defaultValue: 'Tag text' },
    size: {
      control: { type: 'select' },
      defaultValue: 'regular',
      options: ['small', 'regular', 'large'],
    },
    type: {
      control: { type: 'select' },
      options: Object.values(GlobalType),
      defaultValue: GlobalType.primary,
    },
    iconType: {
      control: { type: 'select' },
      options: Object.values(IconType),
      defaultValue: IconType.smile,
    },
    invertColor: { control: 'boolean', defaultValue: false },
  },
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof Tag>;

type Story = StoryObj<typeof Tag>;

export const BasicTag: Story = {
  args: {
    variant: 'squared',
    text: 'Tag text',
    size: 'regular',
    type: GlobalType.primary,
    iconType: IconType.smile,
    invertColor: false,
  },
  render: ({ variant, text, size, type, iconType, invertColor }) => (
    <div>
      <Tag
        variant={variant}
        text={text}
        size={size}
        type={type}
        iconType={iconType}
        invertColor={invertColor}
      />
    </div>
  ),
};

export const TagTypes: Story = {
  args: {
    variant: 'rounded',
  },
  render: ({ variant }) => {
    return (
      <div style={{ width: '600px' }}>
        {Object.values(GlobalType).map(color => (
          <Tag key={color} variant={variant} text={color} type={color} />
        ))}
        <br />
        <br />
        <p>
          <small>Inverted colors:</small>
        </p>
        {Object.values(GlobalType).map(color => (
          <Tag
            key={color}
            variant={variant}
            text={color}
            type={color}
            invertColor
          />
        ))}
      </div>
    );
  },
};

export const TagWithe: Story = {
  args: {
    variant: 'rounded',
    text: 'Tag text',
    size: 'regular',
    invertColor: false,
  },
  render: ({ variant, text, size, invertColor }) => {
    return (
      <div style={{ background: '#000', padding: 50 }}>
        <Tag
          variant={variant}
          text={text}
          size={size}
          type={GlobalType.white}
          invertColor={invertColor}
        />
      </div>
    );
  },
};

export const TagWithIcon: Story = {
  args: {
    variant: 'rounded',
  },
  render: ({ variant }) => {
    const type = GlobalType.secondary;
    return (
      <div>
        <Tag variant={variant} text="Text" type={type} invertColor />
        <Tag
          variant={variant}
          text="With icon"
          type={type}
          iconType={IconType.calendarAlt}
          invertColor
        />
        <Tag
          variant={variant}
          type={type}
          iconType={IconType.calendarAlt}
          invertColor
        />
      </div>
    );
  },
};

export const TagSize: Story = {
  args: {
    variant: 'rounded',
  },
  render: ({ variant }) => {
    const type = GlobalType.secondary;

    return (
      <div>
        <Tag
          variant={variant}
          text="Small"
          type={type}
          size="small"
          invertColor
        />
        <Tag
          variant={variant}
          text="Regular"
          type={type}
          size="regular"
          invertColor
        />
        <Tag
          variant={variant}
          text="Large"
          type={type}
          size="large"
          invertColor
        />
        <br />
        <Tag
          variant={variant}
          type={type}
          size="small"
          iconType={IconType.calendarAlt}
          invertColor
        />
        <Tag
          variant={variant}
          type={type}
          size="regular"
          iconType={IconType.calendarAlt}
          invertColor
        />
        <Tag
          variant={variant}
          type={type}
          size="large"
          iconType={IconType.calendarAlt}
          invertColor
        />
        <br />
        <Tag
          variant={variant}
          text="Small"
          type={type}
          size="small"
          iconType={IconType.calendarAlt}
          invertColor
        />
        <Tag
          variant={variant}
          text="Regular"
          type={type}
          size="regular"
          iconType={IconType.calendarAlt}
          invertColor
        />
        <Tag
          variant={variant}
          text="Large"
          type={type}
          size="large"
          iconType={IconType.calendarAlt}
          invertColor
        />
      </div>
    );
  },
};

export const TagClickable: Story = {
  args: {
    variant: 'rounded',
    type: GlobalType.secondary,
    invertColor: false,
    onClick: fn(),
  },
  render: ({ variant, type, invertColor, onClick }) => (
    <div>
      <Tag
        text="With onClick prop"
        variant={variant}
        type={type}
        iconType={IconType.calendarAlt}
        onClick={onClick}
        invertColor={invertColor}
      />
    </div>
  ),
};
