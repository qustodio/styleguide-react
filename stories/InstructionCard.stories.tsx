import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import InstructionCard from '../src/components/InstructionCard/InstructionCard';
import { IconColor, IconType } from '../src';

export default {
  title: 'InstructionCard',
  component: InstructionCard,
  parameters: {
    layout: 'centered',
  },
  argTypes: {
    compact: { control: 'boolean' },
    iconType: { control: 'select', options: Object.values(IconType) },
    iconColor: { control: 'select', options: Object.values(IconColor) },
    iconAsLink: { control: 'boolean' },
  },
} as Meta<typeof InstructionCard>;

type Story = StoryObj<typeof InstructionCard>;

export const DefaultInstructionCard: Story = {
  render: ({ compact, iconColor, iconAsLink, iconType }) => {
    return (
      <div
        style={{
          width: '375px',
          margin: '24px',
          padding: '16px',
          border: '1px solid lightgrey',
          borderRadius: '6px',
        }}
      >
        <InstructionCard
          title="Tracking for PEPA is OFF"
          description="Turn location services ON to be able to see where Pepa is"
          instructions="Turn location tracking ON"
          url="#"
          compact={compact}
          iconType={iconType || IconType.mapMarkerSlash}
          iconColor={iconColor || IconColor.secondary}
          iconAsLink={iconAsLink}
        />
      </div>
    );
  },
};
