import { Meta, StoryObj } from '@storybook/react';
import Accordion from '../src/components/Accordion/Accordion';
import { IconType } from '../src/components/Icons';

const meta: Meta<typeof Accordion> = {
  title: 'Accordion',
  component: Accordion,
  parameters: {
    layout: 'padded',
  },
  args: {
    items: [
      {
        icon: IconType.shieldAlt,
        title: 'Filter content & apps',
        key: '1',
        content:
          'Block inappropriate apps, games and websites. Allow your kids to visit child-friendly websites and automatically prevent them from viewing potentially harmful ones.',
      },
      {
        icon: IconType.chartBar,
        key: '2',
        title: 'Monitor activity',
        content:
          'Get an easy, real-time view of your child’s browsing history, YouTube views, social media use, screen time, location, and more.',
      },
      {
        icon: IconType.clock,
        key: '3',
        title: 'Set time limits',
        content:
          'Prevent screen addiction, preserve family time, and encourage healthy sleep routines with consistent time limits and scheduled downtime. Plus, pause the internet at the touch of a button.',
      },
      {
        icon: IconType.phone,
        key: '4',
        title: 'Track calls & SMS for Android and iOS',
        content:
          'See who your child is exchanging calls and messages with, and read what they’re writing. Also, block calls to and from specific contacts.\n** Calls and messages for iOS requires a Mac computer.',
      },
      {
        icon: IconType.mapMarkerAlt,
        key: '5',
        title: 'Locate family',
        content:
          'Check your family members’ locations on the map and see where they’ve been. Receive alerts when they arrive and leave your saved locations.',
      },
      {
        icon: IconType.bell,
        key: '6',
        title: 'Reports, alerts & SOS',
        content:
          'In addition to the real-time dashboard, receive detailed daily, weekly and monthly reports of your child’s online activity by email. Set alerts for certain activities and enable them to send an SOS if they need you.',
      },
    ],
  },
};

export default meta;
type Story = StoryObj<typeof Accordion>;

export const Default: Story = {};
