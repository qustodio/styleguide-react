import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { Link } from '@reach/router';
import Icon, {
  IconColor,
  IconFamily,
  IconSize,
  IconType,
} from '../src/components/Icons';
import { GlobalType } from '../src/common/types';
import ActionInput, {
  ActionInputButtonType,
  ActionInputIconPosition,
} from '../src/components/ActionInput/ActionInput';

const meta: Meta<typeof ActionInput> = {
  title: 'ActionInput',
  component: ActionInput,
  parameters: {
    layout: 'centered',
  },
  args: {
    text: 'Label',
    compact: true,
    buttonType: ActionInputButtonType.button,
  },
  argTypes: {
    iconPosition: {
      control: {
        type: 'select',
        options: [ActionInputIconPosition.left, ActionInputIconPosition.right],
      },
    },
    icon: {
      control: {
        type: 'select',
        options: Object.values(IconType),
      },
    },
  },
};

export default meta;
type Story = StoryObj<typeof ActionInput>;

const Margin = ({ children }: { children: React.ReactNode }) => (
  <div style={{ margin: '24px' }}>{children}</div>
);

const WithMargin = (Story: () => React.ReactElement) => (
  <Margin>
    <Story />
  </Margin>
);

export const Default: Story = {
  decorators: [
    Story => (
      <Margin>
        <Story />
        <br />
        <small>
          <code>color</code>, <code>icon</code> and <code>iconPosition</code>{' '}
          properties will be deprecated in the next major update
        </small>
      </Margin>
    ),
  ],
};

export const ActionInputSizes: Story = {
  render: ({ compact, buttonType }) => (
    <Margin>
      <small>Only text</small>
      <ActionInput
        buttonType={buttonType}
        compact={compact}
        text="Placeholder text"
      />
      <br />
      <small>Left icon + text</small>
      <ActionInput
        buttonType={buttonType}
        compact={compact}
        text="Placeholder text"
        iconLeft={
          <Icon
            square
            type={IconType.check}
            family={IconFamily.solid}
            color={IconColor.primary}
          />
        }
      />
      <br />
      <small>Text + right icon</small>
      <ActionInput
        buttonType={buttonType}
        compact={compact}
        text="Placeholder text"
        iconRight={
          <Icon square type={IconType.stopWatch} color={IconColor.secondary} />
        }
      />
      <br />
      <small>Only left and right icon</small>
      <ActionInput
        buttonType={buttonType}
        compact={compact}
        iconLeft={
          <Icon
            square
            type={IconType.stopWatch}
            color={IconColor.secondary}
            size={IconSize.lg}
          />
        }
        iconRight={<Icon type={IconType.angleDown} color={IconColor.neutral} />}
      />
      <br />
      <small>Icons + text</small>
      <ActionInput
        buttonType={buttonType}
        compact={compact}
        textColor={GlobalType.error}
        text="Placeholder text"
        iconLeft={
          <Icon
            square
            type={IconType.ban}
            color={IconColor.error}
            family={IconFamily.solid}
          />
        }
        iconRight={<Icon type={IconType.angleDown} color={IconColor.neutral} />}
      />
    </Margin>
  ),
};

export const ActionInputWithLongText: Story = {
  args: {
    text: 'Some random text to see how it looks with a long text',
  },
  decorators: [WithMargin],
};

export const ActionInputWithPlaceholder: Story = {
  args: {
    text: '',
    placeholder: 'This is a placeholder',
    block: false,
  },
  decorators: [WithMargin],
};

export const ActionInputAsAnchor: Story = {
  args: {
    text: 'label',
    placeholder: 'This is a placeholder',
    href: 'www.google.com',
  },
  decorators: [WithMargin],
};

export const ActionInputAsLink: Story = {
  args: {
    text: 'label',
    placeholder: 'This is a placeholder',
    href: 'www.google.com',
    useAsLink: Link,
  },
  decorators: [WithMargin],
};

export const ActionInputDisabled: Story = {
  args: {
    icon: <Icon color={IconColor.disabled} type={IconType.clock} />,
    disabled: true,
    iconPosition: ActionInputIconPosition.left,
    block: true,
  },
  decorators: [WithMargin],
};

export const ActionInputWithColor: Story = {
  args: {
    icon: <Icon type={IconType.flag} />,
    text: 'School',
    placeholder: 'This is a placeholder',
    block: true,
    iconPosition: ActionInputIconPosition.left,
    color: 'primary',
  },
  decorators: [WithMargin],
};

export const ActionInputWithTextColorAndBackgroundColor: Story = {
  args: {
    text: 'School',
    block: true,
    iconPosition: ActionInputIconPosition.left,
    color: 'primary',
    textColor: GlobalType.secondary,
    backgroundColor: GlobalType.secondary,
  },
  decorators: [WithMargin],
};
