import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Icon, {
  IconColor,
  IconFamily,
  IconSize,
  IconType,
} from '../src/components/Icons';
import { FontAwesomeIconType } from '../src/components/Icons/FontAwesome/types/FontAwesomeIconType.types';
import { RocketCustomIconType } from '../src/components/Icons/CustomIcons/types/RocketCustomIconType.types';
import { fn } from '@storybook/test';

export default {
  title: 'Icons',
  component: Icon,
  args: {
    type: IconType.exclamationCircle,
    family: IconFamily.regular,
    source: 'fontAwesome',
    color: IconColor.notSet,
    size: IconSize.regular,
    square: false,
    circle: false,
    onBlur: fn(),
    onClick: fn(),
    onFocus: fn(),
  },
  argTypes: {
    type: {
      control: 'select',
      options: Object.keys(IconType),
      mapping: IconType,
    },
    family: {
      control: 'select',
      options: Object.keys(IconFamily),
      mapping: IconFamily,
    },
    source: {
      control: 'select',
      options: ['fontAwesome', 'custom'],
    },
    color: {
      control: 'select',
      options: Object.keys(IconColor),
      mapping: IconColor,
    },
    size: {
      control: 'select',
      options: Object.keys(IconSize),
      mapping: IconSize,
    },
  },
} as Meta<typeof Icon>;

type Story = StoryObj<typeof Icon>;

export const Default: Story = {
  render: ({
    source,
    color,
    type,
    size,
    square,
    circle,
    family,
    onBlur,
    onClick,
    onFocus,
  }) => {
    return (
      <div style={{ margin: '10px' }}>
        <Icon
          type={type as IconType}
          family={family}
          source={source}
          size={size}
          color={color}
          square={square}
          circle={circle}
          onBlur={onBlur}
          onClick={onClick}
          onFocus={onFocus}
        />
      </div>
    );
  },
};

export const FontAwesomeIcons: Story = {
  render: ({ color, circle, size, family }) => (
    <ul style={{ margin: '5px', listStyle: 'none', columns: 5 }}>
      {Object.keys(FontAwesomeIconType).map((key: string) => (
        <li style={{ margin: '5px' }} key={key}>
          <span style={{ minWidth: '25px', display: 'inline-block' }}>
            <Icon
              type={
                FontAwesomeIconType[key as keyof typeof FontAwesomeIconType]
              }
              color={color}
              square={circle}
              circle={circle}
              size={size}
              family={family}
              source="fontAwesome"
            />
          </span>
          <span style={{ fontSize: 'small', margin: '5px' }}>{key}</span>
        </li>
      ))}
    </ul>
  ),
};

export const CustomIcons: Story = {
  render: ({ family, color, circle, size, square }) => (
    <ul style={{ margin: '5px', listStyle: 'none', columns: 5 }}>
      {Object.keys(RocketCustomIconType).map((key: string) => (
        <li style={{ margin: '5px' }} key={key}>
          <span style={{ minWidth: '25px', display: 'inline-block' }}>
            <Icon
              type={
                RocketCustomIconType[key as keyof typeof RocketCustomIconType]
              }
              color={color}
              square={square}
              circle={circle}
              size={size}
              source="custom"
              family={family}
            />
          </span>
          <span style={{ fontSize: 'small', margin: '5px' }}>{key}</span>
        </li>
      ))}
    </ul>
  ),
};
