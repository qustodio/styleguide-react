import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import AccountInfoHeader from '../src/components/AccountInfoHeader/AccountInfoHeader';
import LogoImage from './assets/images/Logo.svg';

const meta: Meta<typeof AccountInfoHeader> = {
  title: 'AccountInfoHeader',
  component: AccountInfoHeader,
  parameters: {
    layout: 'centered',
  },
  args: {
    logoImage: <img src={LogoImage} />,
    name: 'Foo Bar',
    showGift: false,
    expand: false,
    testId: '123',
  },
};

export default meta;

type Story = StoryObj<typeof AccountInfoHeader>;

export const Default: Story = {};

export const Expanded: Story = {
  args: {
    expand: true,
  },
};
