import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import DurationInput from '../src/components/DurationInput/DurationInput';

export default {
  title: 'DurationInput',
  component: DurationInput,
  args: {
    maxMinutes: 24 * 60,
    disabled: false,
    hasError: false,
    hoursLabel: 'h',
    minutesLabel: 'm',
  },
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof DurationInput>;

type Story = StoryObj<typeof DurationInput>;

export const Default: Story = {
  render: props => {
    const [time, setTime] = useState(300);

    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
        }}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: 300,
          }}
        >
          <DurationInput
            {...props}
            value={time}
            onChange={value => setTime(value)}
          />
        </div>
        <div>
          <small style={{ color: '#999' }}>
            When <b>maxMinutes</b> prop is not present it's default value is
            1440 (24h * 60)
          </small>
        </div>
      </div>
    );
  },
};
