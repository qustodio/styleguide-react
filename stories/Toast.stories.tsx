import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Toast from '../src/components/Toast/Toast';
import { fn } from '@storybook/test';

export default {
  title: 'Toasts',
  component: Toast,
  parameters: {
    layout: 'centered',
  },
  argTypes: {
    title: {
      control: 'text',
    },
    message: {
      control: 'text',
    },
    position: {
      control: {
        type: 'select',
      },
      options: [
        'top-left',
        'top-right',
        'top-center',
        'bottom-left',
        'bottom-right',
        'bottom-center',
      ],
    },
    type: {
      control: {
        type: 'select',
      },
      options: ['error', 'success', 'warning', 'info'],
    },
  },
} as Meta<typeof Toast>;

type Story = StoryObj<typeof Toast>;

export const Playground: Story = {
  args: {
    title: 'Toast message title',
    message: 'Toast message description ',
    position: 'bottom-center',
    type: 'info',
  },
  render: ({ title, message, position, type }) => {
    return (
      <Toast
        title={title}
        message={message}
        position={position}
        type={type}
        onClose={fn()}
      />
    );
  },
};

export const Header: Story = {
  args: {
    title: 'Toast message title',
    type: 'info',
  },
  render: ({ title, type }) => (
    <Toast title={title} position="bottom-center" type={type} onClose={fn()} />
  ),
};

export const Message: Story = {
  args: {
    message: 'Toast message description',
    type: 'info',
  },
  render: ({ message, type }) => (
    <Toast
      message={message}
      position="bottom-center"
      type={type}
      onClose={fn()}
    />
  ),
};

export const LongText: Story = {
  args: {
    title:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus purus magna',
    type: 'info',
    message:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus purus magna, non hendrerit lorem lobortis vitae. In velit lacus, hendrerit malesuada vehicula ac, mollis at dolor',
  },
  render: ({ title, message, type }) => (
    <Toast
      title={title}
      message={message}
      position="bottom-center"
      type={type}
      onClose={fn()}
    />
  ),
};
