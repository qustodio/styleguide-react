import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import EmptyPlaceholder from '../src/components/Placeholder/EmptyPlaceholder';
import Icon, {
  IconType,
  IconSize,
  IconColor,
  IconFamily,
} from '../src/components/Icons';

export default {
  title: 'Placeholders',
  component: EmptyPlaceholder,
  parameters: {
    layout: 'centered',
  },
  args: {
    text: 'placeholder text here',
    coloredBackground: false,
    centered: false,
    maxWidth: 'auto',
  },
} as Meta<typeof EmptyPlaceholder>;

type Story = StoryObj<typeof EmptyPlaceholder>;

export const EmptyPlaceholderDefault: Story = {
  render: props => (
    <div style={{ width: '90%' }}>
      <EmptyPlaceholder {...props} />
    </div>
  ),
};

export const EmptyPlaceholderWithIcons: Story = {
  render: props => (
    <div style={{ width: '90%', display: 'flex', height: '200px' }}>
      <EmptyPlaceholder
        {...props}
        icon={
          <Icon
            family={IconFamily.brands}
            type={IconType.apple}
            size={IconSize.x3}
            color={IconColor.neutral}
          />
        }
      />
    </div>
  ),
};
