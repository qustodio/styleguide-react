import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { ExpandableBase, Icon, IconType, Text } from '../src';

export default {
  title: 'ExpandableBase',
  component: ExpandableBase,
  args: {
    leftContent: <Text variant="body-2-regular"> expandable title </Text>,
    rightContent: <Text variant="caption-1-regular">999</Text>,
    initialOpenState: false,
    isDisabled: false,
    rightIcon: {
      open: <Icon type={IconType.angleUp} />,
      close: <Icon type={IconType.angleDown} />,
    },
    children: (
      <>
        <div>
          <Text variant="body-2-regular">content</Text>
        </div>
        <div>
          <Text variant="body-2-regular">content</Text>
        </div>
      </>
    ),
  },
} as Meta<typeof ExpandableBase>;

type Story = StoryObj<typeof ExpandableBase>;

export const Default: Story = {};
