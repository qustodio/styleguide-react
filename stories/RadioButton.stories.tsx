import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { RadioButton } from '../src';

const listElementStyle = {
  height: '30px',
  display: 'flex',
  alignItems: 'center',
  margin: 0,
};

const labelStyle = {
  cursor: 'pointer',
};

const RadioButtonContainer = ({ disabled }: { disabled: boolean }) => {
  const [selected, setSelected] = useState<string | undefined>(undefined);
  const handleClick = (value: string) => {
    setSelected(value);
  };
  return (
    <div style={listElementStyle}>
      <RadioButton
        id="radiobutton"
        disabled={disabled}
        checked={selected === 'radiobutton'}
        name="radioButton"
        onClick={() => handleClick('radiobutton')}
        value="radiobutton"
      />
      <label onClick={() => handleClick('radiobutton')} style={labelStyle}>
        {'Radio button text'}
      </label>
    </div>
  );
};

const ListOfRadioButtonsContainer = ({ disabled }: { disabled: boolean }) => {
  const [selected, setSelected] = useState<string | undefined>(undefined);
  const handleClick = (value: string) => {
    setSelected(value);
  };
  return (
    <ul style={{ listStyleType: 'none', padding: 0, margin: 0 }}>
      <li style={listElementStyle}>
        <RadioButton
          id="red"
          disabled={disabled}
          checked={selected === 'red'}
          name={'color'}
          onClick={() => handleClick('red')}
          value="red"
        />
        <label onClick={() => handleClick('red')} style={labelStyle}>
          {'Red Label'}
        </label>
      </li>
      <li style={listElementStyle}>
        <RadioButton
          id="blue"
          disabled={disabled}
          checked={selected === 'blue'}
          name={'color'}
          onClick={() => handleClick('blue')}
          value="blue"
        />
        <label onClick={() => handleClick('blue')} style={labelStyle}>
          {'Blue Label'}
        </label>
      </li>
      <li style={listElementStyle}>
        <RadioButton
          id="green"
          disabled={disabled}
          checked={selected === 'green'}
          name={'color'}
          onClick={() => handleClick('green')}
          value="green"
        />
        <label onClick={() => handleClick('green')} style={labelStyle}>
          {'Green Label'}
        </label>
      </li>
      <li style={listElementStyle}>
        <RadioButton
          id="purple"
          disabled={disabled}
          checked={selected === 'purple'}
          name={'color'}
          onClick={() => handleClick('purple')}
          value="purple"
        />
        <label onClick={() => handleClick('purple')} style={labelStyle}>
          {'Purple Label'}
        </label>
      </li>
    </ul>
  );
};

const meta: Meta = {
  title: 'Radio Button',
  component: RadioButtonContainer,
};

export default meta;

type Story = StoryObj;

export const Default: Story = {
  render: () => <RadioButtonContainer disabled={false} />,
};

export const ListOfRadioButtons: Story = {
  render: () => <ListOfRadioButtonsContainer disabled={false} />,
};
