import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Label from '../src/components/Label/Label';
import Switch from '../src/components/Switch/Switch';
import FlexLayout from '../src/components/Layout/FlexLayout';
import { fn } from '@storybook/test';

export default {
  title: 'Label',
  component: Label,
} as Meta<typeof Label>;

type Story = StoryObj<typeof Label>;

export const BasicLabel: Story = {
  render: ({ ellipsis }) => (
    <div style={{ width: '90%' }}>
      <FlexLayout
        mainaxis="row"
        padding="16"
        paddingBottom="16"
        hasBox
        mainaxisAlignment="space-between"
      >
        <Label htmlFor="test" ellipsis={ellipsis}>
          This is a Label for switch
        </Label>
        <Switch id="test" onChange={fn()} checked />
      </FlexLayout>
    </div>
  ),
};
