import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import DomainTrustSeal from '../src/components/DomainTrustSeal/DomainTrustSeal';

export default {
  title: 'DomainTrustSeal',
  component: DomainTrustSeal,
  args: {
    mode: 'child-safety',
    level: 'excellent',
    title: 'This is the title',
    description: 'This is the description',
  },
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof DomainTrustSeal>;

type Story = StoryObj<typeof DomainTrustSeal>;

export const Default: Story = {
  render: ({ mode, level, title, description }) => (
    <DomainTrustSeal
      mode={mode}
      level={level}
      title={title}
      description={description}
    />
  ),
};
