import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { Slider } from '../src';

const listElementStyle = {
  height: '100px',
  display: 'flex',
  alignItems: 'center',
  margin: 0,
  marginLeft: '20px',
};

const SliderContainer = ({ disabled }: { disabled: boolean }) => {
  const [value, setValue] = useState(100);

  return (
    <div style={listElementStyle}>
      <Slider
        id="slider"
        name="slider"
        minValue={100}
        maxValue={200}
        formatLabel="m"
        value={value}
        disabled={disabled}
        onChange={(_ev, value) => setValue(Number(value))}
      />
    </div>
  );
};

const meta: Meta<typeof Slider> = {
  title: 'Slider',
  component: Slider,
  decorators: [
    Story => (
      <div style={{ margin: '16px' }}>
        <Story />
      </div>
    ),
  ],
};

export default meta;

type Story = StoryObj<typeof Slider>;

export const Default: Story = {
  render: ({ disabled }) => <SliderContainer disabled={!!disabled} />,
};
