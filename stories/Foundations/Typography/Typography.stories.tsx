import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import styles from './typography.scss';
import { Layout, Text } from '../../../src';

const FIGMA_LINK_TYPOGRAPHY_BASE = `https://www.figma.com/design/SPOLPCjdBuZu3Iw6gaqgbq/New-Rocket?node-id=856-4564&t=bBRq7OUak7mHuoJP-4`;
const FIGMA_LINK_TYPOGRAPHY_RESPONSIVE = `PENDING`;
const FIGMA_LINK_TYPOGRAPHY_PUBLIC_SITE = `PENDING`;

interface TypographyMap {
  [styleName: string]: {
    [variant: string]: string; // classname;
  };
}

const TypographyComponent = ({
  type,
}: {
  type: 'base' | 'responsive' | 'public-site';
}) => {
  if (type === 'base') {
    return <TypographyBase />;
  }
  if (type === 'responsive') {
    return <TypographyResponsive />;
  }
  if (type === 'public-site') {
    return <TypographyPublicSite />;
  }
  return <></>;
};

const TypographyTable = ({
  typographyMap,
  type,
}: {
  type: 'base' | 'responsive' | 'public-site';
  typographyMap: TypographyMap;
}) => {
  const typographyTable = Object.entries(typographyMap).reduce(
    (acc, [styleName, variants]) => {
      return [
        ...acc,
        ...Object.entries(variants).map(([variant, className]) => ({
          styleName,
          variant,
          className,
        })),
      ];
    },
    [] as {
      styleName: string;
      variant: string;
      className: string;
    }[]
  );

  return (
    <div className="typography-table">
      <div className="header">
        <div className="cell">Style name</div>
      </div>
      {typographyTable.map(({ styleName, variant }, i) => (
        <>
          <div className={`row ${i % 2 === 0 ? 'odd' : ''}`}>
            <div className="cell">
              <span
                className={`foundation__typography--${styleName}-${variant}${
                  type === 'base' ? '' : '-' + type
                }`}
              >
                {styleName}/{variant}
                {type === 'base' ? '' : '-' + type}
              </span>
            </div>
          </div>
        </>
      ))}
    </div>
  );
};

const TypographyBase = () => {
  const typographyMapString = styles.typographyBase;
  const link = FIGMA_LINK_TYPOGRAPHY_BASE;

  const typographyMap = JSON.parse(
    typographyMapString.slice(1, -1)
  ) as TypographyMap;

  return (
    <div className="foundation__typography">
      <Layout marginBottom="32">
        <Text variant="headline-1-semibold" marginBottom="24">
          Typography base
        </Text>
        <Text
          variant="body-1-semibold"
          color="brand"
          renderAs="div"
          marginBottom="24"
        >
          <a target="_blank" href={link}>
            Figma
          </a>
        </Text>
        <TypographyTable typographyMap={typographyMap} type="base" />
      </Layout>
    </div>
  );
};

const TypographyResponsive = () => {
  const typographyMapString = styles.typographyResponsive;
  const link = FIGMA_LINK_TYPOGRAPHY_RESPONSIVE;

  const typographyMap = JSON.parse(typographyMapString.slice(1, -1)) as {
    [styleName: string]: {
      [variant: string]: string; // classname;
    };
  };

  return (
    <div className="foundation__typography">
      <Layout marginBottom="32">
        <Text variant="headline-1-semibold" marginBottom="24">
          Typography responsive
        </Text>
        <Text
          variant="body-1-semibold"
          color="brand"
          renderAs="div"
          marginBottom="24"
        >
          <a target="_blank" href={link}>
            Figma
          </a>
        </Text>
      </Layout>
      <Text marginBottom="16" variant="body-1-regular">
        Change the viewport size to see the typography change.
      </Text>
      <Text variant="body-1-regular">Current displayed version:</Text>
      <Text variant="body-1-semibold" color="brand">
        <span className="mobile-version">Mobile</span>
        <span className="desktop-version">Desktop</span>
      </Text>
      <TypographyTable typographyMap={typographyMap} type="responsive" />
    </div>
  );
};

const TypographyPublicSite = () => {
  const typographyMapString = styles.typographyPublicSite;
  const link = FIGMA_LINK_TYPOGRAPHY_PUBLIC_SITE;

  const typographyMap = JSON.parse(
    typographyMapString.slice(1, -1)
  ) as TypographyMap;

  return (
    <div className="foundation__typography">
      <Layout marginBottom="32">
        <Text variant="headline-1-semibold" marginBottom="24">
          Typography Public Site
        </Text>
        <Text
          variant="body-1-semibold"
          color="brand"
          renderAs="div"
          marginBottom="24"
        >
          <a target="_blank" href={link}>
            Figma
          </a>
        </Text>
        <TypographyTable typographyMap={typographyMap} type="public-site" />
      </Layout>
    </div>
  );
};

const meta: Meta<typeof TypographyBase> = {
  title: 'Foundations/Typography',
  component: TypographyComponent,
  parameters: {
    layout: 'padded',
  },
  argTypes: {
    type: {
      table: {
        disable: true,
      },
    },
  },
};

export default meta;
type Story = StoryObj<typeof TypographyBase>;

export const Base: Story = {
  args: {
    type: 'base',
  },
};

export const Responsive: Story = {
  args: {
    type: 'responsive',
  },
};

export const PublicSite: Story = {
  name: 'Public Site',
  args: {
    type: 'public-site',
  },
};
