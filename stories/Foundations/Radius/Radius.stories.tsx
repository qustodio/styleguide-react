import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import styles from './radius.scss';
import { Layout, Text } from '../../../src';

const FIGMA_LINK = `https://www.figma.com/design/SPOLPCjdBuZu3Iw6gaqgbq/New-Rocket?node-id=896-6742&t=gdm5BsPu1V32dhsS-1`;

const Radius = ({
  title,
  tokensMapString,
  link,
}: {
  title: string;
  tokensMapString: string;
  link: string;
}) => {
  const tokensMap = JSON.parse(tokensMapString.slice(1, -1));

  return (
    <div className="foundation-radius">
      <Layout marginBottom="32">
        <Text variant="headline-1-semibold" marginBottom="24">
          {title}
        </Text>
        <Text
          variant="body-1-semibold"
          color="brand"
          renderAs="div"
          marginBottom="24"
        >
          <a target="_blank" href={link}>
            Figma
          </a>
        </Text>
      </Layout>

      <div className="foundation-radius__grid">
        {Object.entries(tokensMap).map(([name, value]) => (
          <div key={name} className="foundation-radius__cell">
            <div className={`cell foundation__radius--${name}`}></div>
            <Text variant="body-2-regular">
              <strong>radius-{name}</strong>
              <br />
              {value}
            </Text>
          </div>
        ))}
      </div>
    </div>
  );
};

const meta: Meta<typeof Radius> = {
  title: 'Foundations/Radius',
  component: Radius,
  parameters: {
    layout: 'padded',
  },
};

export default meta;
type Story = StoryObj<typeof Radius>;

export const Base: Story = {
  args: {
    title: 'Radius',
    tokensMapString: styles.baseRadius,
    link: FIGMA_LINK,
  },
};
