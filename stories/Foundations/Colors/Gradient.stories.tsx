import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import styles from './gradients.scss';
import { Layout, Text } from '../../../src';

const FIGMA_LINK_BASE_GRADIENTS = `https://www.figma.com/design/SPOLPCjdBuZu3Iw6gaqgbq/New-Rocket?node-id=1014-34048&t=v0kGVhLUQS7fwFgX-1`;

const Palette = ({
  title,
  gradientsMapString,
  link,
}: {
  title: string;
  gradientsMapString: string;
  link: string;
}) => {
  const gradientMap = JSON.parse(gradientsMapString.slice(1, -1));

  return (
    <div className="colors-grid">
      <Layout marginBottom="32">
        <Text variant="headline-1-semibold" marginBottom="24">
          {title}
        </Text>

        <Text
          variant="body-1-semibold"
          color="brand"
          renderAs="div"
          marginBottom="24"
        >
          <a target="_blank" href={link}>
            Figma
          </a>
        </Text>
      </Layout>

      <div className="foundation-gradient__rows">
        {Object.entries(gradientMap).map(([name, value]) => (
          <div className="foundation-gradient__cell">
            <div className={`cell foundation__gradient--${name}`}></div>
            <Text variant="body-1-regular">
              <strong>Gradient {name}</strong>
              <br />
              <Text color="grey-300" variant="body-2-regular">
                {value}
              </Text>
            </Text>
          </div>
        ))}
      </div>
    </div>
  );
};

const meta: Meta<typeof Palette> = {
  title: 'Foundations/Colors',
  component: Palette,
  parameters: {
    layout: 'padded',
  },
};

export default meta;
type Story = StoryObj<typeof Palette>;

export const Gradients: Story = {
  args: {
    title: 'Base Gradients',
    gradientsMapString: styles.gradients,
    link: FIGMA_LINK_BASE_GRADIENTS,
  },
};
