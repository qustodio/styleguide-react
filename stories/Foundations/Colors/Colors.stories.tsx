import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import styles from './colors.scss';
import { Layout, Text } from '../../../src';

const FIGMA_LINK_BASE_COLORS = `https://www.figma.com/design/SPOLPCjdBuZu3Iw6gaqgbq/New-Rocket?node-id=1507-13875&t=v8iOGjbjQ46ekEez-4`;
const FIGMA_LINK_BRAND_COLORS = `https://www.figma.com/design/SPOLPCjdBuZu3Iw6gaqgbq/New-Rocket?node-id=2254-12253&t=v0kGVhLUQS7fwFgX-1`;
const FIGMA_LINK_SEMANTIC_COLORS = `https://www.figma.com/design/SPOLPCjdBuZu3Iw6gaqgbq/New-Rocket?node-id=1507-14341&t=kCayF6qlDu8eO8We-4`;

const ColorCell = ({ name, color }: { name: string; color: string }) => {
  return (
    <div className="foundation-colors__cell">
      <div className={`cell foundation__color--${name}`}></div>
      <Text variant="body-2-regular">
        <strong>{name}</strong>
        <br />
        {(color as string).toUpperCase()}
      </Text>
    </div>
  );
};

const Palette = ({
  title,
  colorsMapString,
  link,
}: {
  title: string;
  colorsMapString: string;
  link: string;
}) => {
  const colorsMap = JSON.parse(colorsMapString.slice(1, -1));

  return (
    <div className="colors-grid">
      <Layout marginBottom="32">
        <Text variant="headline-1-semibold" marginBottom="24">
          {title}
        </Text>

        <Text
          variant="body-1-semibold"
          color="brand"
          renderAs="div"
          marginBottom="24"
        >
          <a target="_blank" href={link}>
            Figma
          </a>
        </Text>
      </Layout>

      <div className="foundation-colors__grid">
        {Object.entries(colorsMap).map(([key, value]) => {
          if (typeof value === 'object') {
            return (
              <div key={key} className="foundation-colors__row">
                {Object.entries(value as object).map(([shade, color]) => (
                  <ColorCell
                    key={`${key}-${shade}-${color}`}
                    name={`${key}-${shade}`}
                    color={color as string}
                  />
                ))}
              </div>
            );
          }

          return (
            <div key={key} className="foundation-colors__row">
              <ColorCell name={key} color={value as string} />
            </div>
          );
        })}
      </div>
    </div>
  );
};

const meta: Meta<typeof Palette> = {
  title: 'Foundations/Colors',
  component: Palette,
  parameters: {
    layout: 'padded',
  },
};

export default meta;
type Story = StoryObj<typeof Palette>;

export const Base: Story = {
  args: {
    title: 'Base Colors',
    colorsMapString: styles.colorBase,
    link: FIGMA_LINK_BASE_COLORS,
  },
};

export const Brand: Story = {
  args: {
    title: 'Brand Colors',
    colorsMapString: styles.colorBrand,
    link: FIGMA_LINK_BRAND_COLORS,
  },
};

export const Semantic: Story = {
  args: {
    title: 'Semantic Colors',
    colorsMapString: styles.colorSemantic,
    link: FIGMA_LINK_SEMANTIC_COLORS,
  },
};
