import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import styles from './shadow.scss';
import { Layout, Text } from '../../../src';

const FIGMA_LINK = `https://www.figma.com/design/SPOLPCjdBuZu3Iw6gaqgbq/New-Rocket?node-id=832-13206&t=gdm5BsPu1V32dhsS-1`;

const Shadow = ({
  title,
  tokensMapString,
  link,
}: {
  title: string;
  tokensMapString: string;
  link: string;
}) => {
  const tokensMap = JSON.parse(tokensMapString.slice(1, -1));

  return (
    <div className="foundation-shadow">
      <Layout marginBottom="32">
        <Text variant="headline-1-semibold" marginBottom="24">
          {title}
        </Text>

        <Text
          variant="body-1-semibold"
          color="brand"
          renderAs="div"
          marginBottom="24"
        >
          <a target="_blank" href={link}>
            Figma
          </a>
        </Text>
      </Layout>

      <div className="foundation-shadow__grid">
        {Object.entries(tokensMap).map(([name, value]) => (
          <div key={name} className="foundation-shadow__cell">
            <div className={`cell foundation__shadow--${name}`}></div>
            <Text variant="body-2-regular">
              <strong>shadow-{name}</strong>
              <br />
              {value}
            </Text>
          </div>
        ))}
      </div>
    </div>
  );
};

const meta: Meta<typeof Shadow> = {
  title: 'Foundations/Shadow',
  component: Shadow,
  parameters: {
    layout: 'padded',
  },
};

export default meta;
type Story = StoryObj<typeof Shadow>;

export const Base: Story = {
  args: {
    title: 'Shadows',
    tokensMapString: styles.baseShadows,
    link: FIGMA_LINK,
  },
};
