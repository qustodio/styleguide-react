import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { StyledHeader, Text } from '../src';
import { textColors, textVariants } from '../src/components/Text/constants';

export default {
  title: 'Text',
  component: Text,
  parameters: {
    layout: 'centered',
    controls: {
      expanded: true,
      exclude: ['testId', 'className'],
      sort: 'requiredFirst',
    },
  },
  args: {
    variant: 'body-1-regular',
    renderAs: '',
    children: 'Plain Text',
  },
  argTypes: {
    variant: {
      type: {
        required: true,
        name: 'string',
      },
      options: (textVariants as unknown) as string[],
      control: { type: 'select' },
      description: 'Text variant to define the font size and weight.',
    },
    color: {
      options: (textColors as unknown) as string[],
      control: { type: 'select' },
      description: 'Text color. Only accepts the colors defined.',
    },
    align: {
      options: ['center', 'left', 'right'],
      control: { type: 'select' },
    },
    renderAs: {
      options: ['', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'div', 'span'],
      control: { type: 'select' },
      description:
        'Render the Text as a different HTML element. You could check the HTML element used in the browser dev tools.',
    },
    noWrap: {
      control: { type: 'boolean' },
      description:
        'Prevent the text from wrapping to the next line. It will truncate the text instead with an ellipsis.',
    },
    marginTop: {
      options: [
        '0',
        '4',
        '8',
        '16',
        '24',
        '32',
        '40',
        '48',
        '56',
        '72',
        '80',
        '88',
        '96',
        '104',
        '112',
        '160',
        '200',
      ],
      control: { type: 'select' },
    },
    marginBottom: {
      options: [
        '0',
        '4',
        '8',
        '16',
        '24',
        '32',
        '40',
        '48',
        '56',
        '72',
        '80',
        '88',
        '96',
        '104',
        '112',
        '160',
        '200',
      ],
      control: { type: 'select' },
    },
    children: {
      name: 'Set content',
      options: [
        'Plain Text',
        'Strong children',
        'Italic children',
        'Strong and Italic children',
      ],
      control: { type: 'select', required: true },
      description:
        'Test different content types to see how the typography renders.',
    },
  },
} as Meta<typeof Text>;

type Story = StoryObj<typeof Text>;

export const Default: Story = {
  render: args => {
    const { children, ...rest } = args;

    return (
      <div
        style={{
          margin: '0 auto',
          width: '80vw',
          border: '1px solid black',
          boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
          paddingLeft: '16px',
          paddingRight: '16px',
        }}
      >
        {children === 'Plain Text' && (
          <Text {...rest}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta
            sem non ligula commodo hendrerit. Praesent vitae orci ut mi semper
            rutrum quis sollicitudin ligula. Vestibulum non euismod metus. Nunc
            purus dolor, pulvinar id erat id, suscipit egestas lorem. Aenean
            congue molestie tortor, et pharetra mi eleifend id. Pellentesque
            vulputate justo tellus, ullamcorper tempor lorem viverra ut. Sed eu
            sem eros.
          </Text>
        )}
        {children === 'Strong children' && (
          <Text {...rest}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta
            sem non ligula commodo hendrerit. Praesent vitae orci ut mi semper
            rutrum quis sollicitudin ligula. Vestibulum non euismod metus.{' '}
            <strong>Nunc purus dolor, pulvinar id erat id,</strong> suscipit
            egestas lorem. Aenean congue molestie tortor, et pharetra mi
            eleifend id. Pellentesque vulputate justo tellus, ullamcorper tempor
            lorem viverra ut. Sed eu sem eros.
          </Text>
        )}
        {children === 'Italic children' && (
          <Text {...rest}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta
            sem non ligula commodo hendrerit. Praesent vitae orci ut mi semper
            rutrum quis sollicitudin ligula. Vestibulum non euismod metus. Nunc
            purus dolor, <em>pulvinar id erat id, suscipit egestas lorem.</em>{' '}
            Aenean congue molestie tortor, et pharetra mi eleifend id.
            Pellentesque vulputate justo tellus, ullamcorper tempor lorem
            viverra ut. Sed eu sem eros.
          </Text>
        )}
        {children === 'Strong and Italic children' && (
          <Text {...rest}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta
            sem non ligula commodo hendrerit. Praesent vitae orci ut mi semper
            rutrum quis sollicitudin ligula. Vestibulum non euismod metus.{' '}
            <strong>
              Nunc purus dolor, <em>pulvinar id erat id,</em>
            </strong>{' '}
            suscipit egestas lorem. Aenean congue molestie tortor, et pharetra
            mi eleifend id. Pellentesque vulputate justo tellus, ullamcorper
            tempor lorem viverra ut. Sed eu sem eros.
          </Text>
        )}
      </div>
    );
  },
};
