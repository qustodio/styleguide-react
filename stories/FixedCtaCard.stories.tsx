import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import FixedCtaCard from '../src/components/FixedCtaCard/FixedCtaCard';

export default {
  title: 'Fixed Cta Card',
  component: FixedCtaCard,
  args: {
    title: 'Title',
    description: 'Description (optional)',
    button: {
      onClick: () => {},
      block: true,
      children: 'Button',
    },
    backgroundColor: 'brand-primary-300',
  },
  decorators: [
    Story => (
      <div style={{ backgroundColor: '#fafafa', height: '700px' }}>
        Scrollable content
        <Story />
      </div>
    ),
  ],
} as Meta<typeof FixedCtaCard>;

type Story = StoryObj<typeof FixedCtaCard>;

export const FixedCtaCardDefault: Story = {
  args: {
    title: 'Title',
    description: 'Description (optional)',
    button: {
      onClick: () => {},
      block: true,
      children: 'Button',
    },
    backgroundColor: 'brand-primary-300',
  },
};

export const FixedCtaCardBrand: Story = {
  args: {
    title: 'Title',
    description: 'Description (optional)',
    button: {
      onClick: () => {},
      block: true,
      children: 'Button',
    },
    backgroundColor: 'brand-primary-600',
  },
};

export const FixedCtaCardGradient: Story = {
  args: {
    title: 'Title',
    description: 'Description (optional)',
    button: {
      onClick: () => {},
      block: true,
      color: 'wellbeing',
      children: 'Button',
    },
    backgroundColor: 'gradient-brand',
  },
};
