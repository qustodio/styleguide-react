import React, { useState } from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import PlanBox from '../src/components/PlanBox/PlanBox';
import PlanBoxExtended, {
  PlanBoxExtendedProps,
} from '../src/components/PlanBox/PlanBoxExtended';
import { RibbonType } from '../src/components/Ribbon/Ribbon';
import {
  FlexLayout,
  FreeStyleListItem,
  Icon,
  IconSize,
  IconType,
  List,
} from '../src';

const meta: Meta = {
  title: 'PlanBoxes',
  component: PlanBox,
};

export default meta;

type Story = StoryObj<typeof PlanBox>;

const StoryLayout = ({
  children,
  style,
}: {
  children: React.ReactNode;
  style?: React.CSSProperties;
}) => {
  return (
    <div
      className="story-layout"
      style={{
        padding: '40px',
        backgroundColor: '#eff3f6',
        ...style,
      }}
    >
      {children}
    </div>
  );
};

const boolean = (_label: string, defaultValue: boolean) => defaultValue;
const text = (_label: string, defaultValue: string) => defaultValue;

const PlanBoxDefault = () => {
  const [isChecked, setIsChecked] = useState(true);
  const handleClick = () => {
    setIsChecked(!isChecked);
  };
  const showCheckbox = boolean('Has checkbox', true);
  const carePlusIncluded = boolean('Has care plus hired', false);

  return (
    <StoryLayout>
      <PlanBox
        title={text('Title', 'Small')}
        price={text('Price', '$36.00')}
        years={text('Years', 'year')}
        priceAdditionalInformation={text(
          'Additional information',
          'Same as $3.00/month'
        )}
        deviceInformation={text('Device Information', '5 devices')}
        highlighted={boolean('Highlighted', true)}
        hasDiscount={boolean('Has Discount', false)}
        current={boolean('Current', false)}
        currentText={text('Current Text', 'Your current plan')}
        ribbon={{
          text: text('Ribbon Text', '10% OFF'),
          type: (undefined as unknown) as RibbonType,
        }}
        button={{
          text: text('Button Text', 'Buy now'),
          isLoading: boolean('Button is loading', false),
          onClick: action('On Click'),
        }}
        carePlusInfo={{
          showCheckbox: showCheckbox,
          carePlusIncluded: carePlusIncluded,
          label: (
            <React.Fragment>
              <strong>
                <span>Care Plus / </span>
                <span style={{ color: '#6161ff' }}>Free Forever</span>
              </strong>
            </React.Fragment>
          ),
          description:
            'Enhance your Premium experience by adding Care Plus, which includes these exclusive features:',
          list: (
            <List listItemMarginTop="8">
              <FreeStyleListItem>
                <FlexLayout mainaxis="row" marginTop="8">
                  <FlexLayout marginRight="8" mainaxis={'column'}>
                    <Icon type={IconType.phone} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Priority phone support
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row">
                  <FlexLayout marginRight="8" mainaxis={'column'}>
                    <Icon type={IconType.userFriends} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Ongoing check-ins
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row">
                  <FlexLayout marginRight="8" mainaxis={'column'}>
                    <Icon type={IconType.handSparkles} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Personalised help
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row" marginBottom="8">
                  <FlexLayout marginRight="8" mainaxis={'column'}>
                    <Icon type={IconType.dots} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Available in English & Spanish
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
            </List>
          ),
          onClick: handleClick,
          isChecked: isChecked,
        }}
      />
    </StoryLayout>
  );
};

const defaultPlanProps = {
  title: 'small',
  deviceInformation: '5 devices',
  priceAdditionalInformation: 'Same as $4.93/month',
  currentText: 'Your current plan',
  price: '$59.16',
  years: 'year',
  button: {
    isLoading: false,
    text: 'Buy now',
    onClick: () => {},
  },
};

const PlanBoxUpgradeWithCheckbox = ({
  highlighted,
  price,
}: {
  highlighted: boolean;
  price: string;
}) => {
  const [isChecked, setIsChecked] = useState(true);
  const handleClick = () => {
    setIsChecked(!isChecked);
  };
  return (
    <StoryLayout>
      <PlanBox
        {...defaultPlanProps}
        highlighted={highlighted}
        carePlusInfo={{
          showCheckbox: true,
          carePlusIncluded: false,
          label: (
            <React.Fragment>
              <strong>
                <span>Care Plus / </span>
                <span style={{ color: '#6161ff' }}>{price}</span>
              </strong>
            </React.Fragment>
          ),
          description:
            'Enhance your Premium experience by adding Care Plus, which includes these exclusive features:',
          list: (
            <List listItemMarginTop="8">
              <FreeStyleListItem>
                <FlexLayout mainaxis="row" marginTop="8">
                  <FlexLayout marginRight="8" minWidth="24" mainaxis={'column'}>
                    <Icon type={IconType.phone} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Priority phone support
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row">
                  <FlexLayout marginRight="8" minWidth="24" mainaxis="column">
                    <Icon type={IconType.userFriends} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Ongoing check-ins
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row">
                  <FlexLayout marginRight="8" minWidth="24" mainaxis="column">
                    <Icon type={IconType.handSparkles} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Personalised help
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row" marginBottom="8">
                  <FlexLayout marginRight="8" minWidth="24" mainaxis="column">
                    <Icon type={IconType.dots} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Available in English & Spanish
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
            </List>
          ),
          onClick: handleClick,
          isChecked: isChecked,
        }}
      />
    </StoryLayout>
  );
};

const PlanBoxUpgradeWithCarePlus = () => {
  return (
    <StoryLayout>
      <PlanBox
        {...defaultPlanProps}
        highlighted={false}
        current={true}
        carePlusInfo={{
          showCheckbox: false,
          carePlusIncluded: true,
          label: (
            <FlexLayout marginRight="8" mainaxis="column">
              <strong>
                <span>Care Plus included</span>
              </strong>
            </FlexLayout>
          ),
          description:
            'Enhance your Premium experience by adding Care Plus, which includes these exclusive features:',
          list: (
            <List listItemMarginTop="8">
              <FreeStyleListItem>
                <FlexLayout mainaxis="row" marginTop="8">
                  <FlexLayout marginRight="8" mainaxis="column">
                    <Icon type={IconType.phone} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Priority phone support
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row">
                  <FlexLayout marginRight="8" mainaxis="column">
                    <Icon type={IconType.userFriends} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Ongoing check-ins
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row">
                  <FlexLayout marginRight="8" mainaxis="column">
                    <Icon type={IconType.handSparkles} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Personalised help
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
              <FreeStyleListItem>
                <FlexLayout mainaxis="row" marginBottom="8">
                  <FlexLayout marginRight="8" mainaxis="column">
                    <Icon type={IconType.dots} size={IconSize.sm} />
                  </FlexLayout>
                  <p className="rck-plan-box__checkbox-description">
                    Available in English & Spanish
                  </p>
                </FlexLayout>
              </FreeStyleListItem>
            </List>
          ),
          onClick: () => {},
          isChecked: false,
        }}
      />
    </StoryLayout>
  );
};

export const PlanBoxKnobsPlayground: Story = {
  render: () => <PlanBoxDefault />,
};

export const PlanBoxDefaultPlan: Story = {
  render: () => (
    <StoryLayout>
      <PlanBox {...defaultPlanProps} />
    </StoryLayout>
  ),
};

export const PlanBoxCurrentPlan: Story = {
  render: () => (
    <StoryLayout>
      <PlanBox {...defaultPlanProps} current />
    </StoryLayout>
  ),
};

export const PlanBoxPlanWithDiscount: Story = {
  render: () => (
    <StoryLayout>
      <PlanBox
        {...defaultPlanProps}
        ribbon={{
          type: RibbonType.cornerLeft,
          text: 'SAVE 20%',
        }}
        priceAdditionalInformation="Was $73.95 - Save $14.79"
      />
    </StoryLayout>
  ),
};

export const PlanBoxHighlightedPlanWithDiscount: Story = {
  render: () => (
    <StoryLayout>
      <PlanBox
        {...defaultPlanProps}
        highlighted
        ribbon={{
          type: RibbonType.cornerLeft,
          text: 'SAVE 20%',
        }}
        priceAdditionalInformation="Was $73.95 - Save $14.79"
        hasDiscount
      />
    </StoryLayout>
  ),
};

export const PlanBoxHighlightedPlanWithoutDiscount: Story = {
  render: () => (
    <StoryLayout>
      <PlanBox
        {...defaultPlanProps}
        highlighted
        ribbon={{
          type: RibbonType.cornerRight,
          text: 'BEST VALUE',
        }}
      />
    </StoryLayout>
  ),
};

export const PlanBoxUpgradeHighlightedWithCheckbox: Story = {
  render: () => (
    <PlanBoxUpgradeWithCheckbox highlighted={false} price="+9.99$" />
  ),
};

export const PlanBoxUpgradeHighlightedWithCarePlus: Story = {
  render: () => <PlanBoxUpgradeWithCarePlus />,
};

export const ThreePlanBoxes: Story = {
  render: () => (
    <div style={{ display: 'flex', flexDirection: 'row' }}>
      <PlanBoxUpgradeWithCheckbox highlighted={false} price="+12.99$" />
      <PlanBoxUpgradeWithCheckbox highlighted={true} price="Free forever" />
      <PlanBoxUpgradeWithCheckbox highlighted={false} price="Free forever" />
    </div>
  ),
};

const addonCheckboxProps = {
  label: (
    <React.Fragment>
      <strong>Add Care Plus</strong> for $12.99
    </React.Fragment>
  ),
  showCheckbox: true,
  isChecked: false,
  onClick: () => {},
  description:
    'Let us take it from here. Care Plus helps busy parents like you save time through:',
  list: [
    {
      text: 'Priority phone support',
      icon: <Icon type={IconType.phone} />,
    },
    {
      text: 'Ongoing check-ins',
      icon: <Icon type={IconType.userFriends} />,
    },
    {
      text: 'Personalised help',
      icon: <Icon type={IconType.handSparkles} />,
    },
    {
      text: 'Available in English & Spanish',
      icon: <Icon type={IconType.dots} />,
    },
  ],
};

const basicProps = {
  icon: IconType.lockAlt,
  title: 'Basic',
  subtitle: 'The tools you need for core protection',
  price: '$54.95',
  years: 'year',
  priceAdditionalInformation: 'same as $4.58/month',
  highlighted: false,
  button: {
    text: 'Buy now',
    isLoading: false,
    onClick: () => {},
  },
  features: {
    title: 'Basic includes:',
    list: [
      'Daily time limits',
      'Web filtering',
      'Location monitoring',
      'Pause the internet',
    ],
  },
  addonInfo: addonCheckboxProps,
} as PlanBoxExtendedProps;

export const PlanBoxExtendedBasic: Story = {
  render: () => (
    <StoryLayout>
      <PlanBoxExtended {...basicProps} />
    </StoryLayout>
  ),
};

const completeProps = {
  icon: IconType.shieldAlt,
  title: 'Complete',
  subtitle: 'Advanced safety features with full customization',
  price: '$99.95',
  years: 'year',
  priceAdditionalInformation: 'same as $8.33 / month',
  highlighted: true,
  ribbon: {
    text: 'Most Popular',
  },
  button: {
    text: 'Buy now',
    isLoading: false,
    onClick: () => {},
  },
  features: {
    title: 'Complete includes:',
    list: [
      'Daily time limits',
      'Web filtering',
      'Location monitoring',
      'Pause the internet',
      'Custom alerts',
      'Games & apps time limits',
      'Calls & messages monitoring',
      'Unlimited devices',
    ],
  },
  addonInfo: {
    ...addonCheckboxProps,
    label: (
      <React.Fragment>
        <strong>Care Plus</strong> included free
      </React.Fragment>
    ),
    isChecked: true,
  },
} as PlanBoxExtendedProps;

export const PlanBoxExtendedComplete: Story = {
  render: () => (
    <StoryLayout>
      <PlanBoxExtended {...completeProps} />
    </StoryLayout>
  ),
};
