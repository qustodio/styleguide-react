import React from 'react';
import { useState } from 'react';
import DropdownOption from '../src/components/Dropdown/DropdownOption';
import Dropdown from '../src/components/Dropdown/Dropdown';
import {
  DropdownElementOrientation,
  DropdownMenuPlacement,
  DropdownAlignment,
} from '../src/components/Dropdown/types';
import {
  createDefaultActionTextFieldElement,
  createDefaultActionInputElement,
  createDefaultActionElement,
} from '../src/components/Dropdown/ActionElement';
import Icon, {
  IconColor,
  IconFamily,
  IconSize,
  IconType,
} from '../src/components/Icons';
import Avatar from '../src/components/Avatar/Avatar';
import { AvatarSize } from '../src/components/Avatar/Avatar.types';
import BoyAvatarSvg from './assets/images/Avatars/boy.svg';
import RegularStyleListItem from '../src/components/List/ListItems/RegularStyleListItem';
import List, { ListTitle } from '../src/components/List';
import { GlobalType } from '../src/common/types';
import { Meta, StoryObj } from '@storybook/react';
import { fn } from '@storybook/test';
import { render } from 'react-dom';

export default {
  title: 'Dropdown',
  component: Dropdown,
  args: {},
  argTypes: {
    showSelectedOption: { control: 'boolean' },
    menuPlacement: {
      control: 'select',
      options: Object.keys(DropdownMenuPlacement),
      mapping: DropdownMenuPlacement,
    },
    fixedMenuPlacement: {
      control: 'boolean',
    },
    onActiveChange: fn(),
    onChange: fn(),
    attachedMenuOriention: {
      control: 'select',
      options: Object.keys(DropdownElementOrientation),
      mapping: DropdownElementOrientation,
    },
  },
} as Meta<typeof Dropdown>;

type Story = StoryObj<typeof Dropdown>;

const options = [
  {
    value: '1',
    text: 'ramon',
    icon: (
      <Avatar size={AvatarSize.extraExtraSmall}>
        <img src={BoyAvatarSvg} />
      </Avatar>
    ),
    disabled: false,
    assignKey: 'r',
  },
  {
    value: '2',
    text: 'pepe (disabled)',
    icon: (
      <Avatar size={AvatarSize.extraExtraSmall}>
        <img src={BoyAvatarSvg} />
      </Avatar>
    ),
    disabled: true,
    assignKey: 'p',
  },
  {
    value: '3',
    text: 'manolo',
    icon: (
      <Avatar size={AvatarSize.extraExtraSmall}>
        <img src={BoyAvatarSvg} />
      </Avatar>
    ),
    disabled: false,
    assignKey: 'm',
  },
];

const FullSampleFilteringContainer = () => {
  const [selected, setSelected] = useState(undefined);
  const [filter, setFilter] = useState('');
  const [isActive, setActive] = useState(false);

  const handleChange = value => {
    setFilter('');
    setSelected(value);
  };

  const handleTextChange = ev => {
    setFilter(ev.target.value);
  };

  const handleIsActive = isActive => {
    if (!isActive) {
      setFilter('');
    }
    setActive(isActive);
  };

  const getValue = () => {
    if (!isActive) {
      if (selected) {
        return options.find(opt => opt.value === selected).text;
      }
      return '';
    }
    return filter;
  };

  return (
    <div style={{ width: '80%' }}>
      <div id="test" />
      <Dropdown
        actionElement={createDefaultActionTextFieldElement({
          name: 'text',
          onChange: handleTextChange,
          value: getValue(),
          placeholder: 'Select a spanish folkloric name',
        })}
        onChange={handleChange}
        onActiveChange={handleIsActive}
        portalId="test"
      >
        {options
          .filter(
            option => !filter || (filter && option.text.indexOf(filter) !== -1)
          )
          .map(option => (
            <DropdownOption
              {...option}
              selected={option.value === selected}
              key={option.value}
            />
          ))}
      </Dropdown>
    </div>
  );
};

const FullSampleActionInputContainer = ({ actionElementProps = {} }) => {
  const [selected, setSelected] = useState(undefined);

  const handleChange = value => {
    setSelected(value);
  };

  return (
    <div style={{ width: '80%' }}>
      <div id="test" />
      <Dropdown
        actionElement={createDefaultActionInputElement({
          name: 'text',
          text: options.find(opt => opt.value === selected)?.text || '',
          placeholder: 'Select a spanish folkloric name',
          ...actionElementProps,
        })}
        onChange={handleChange}
        portalId="test"
      >
        {options.map(option => (
          <DropdownOption
            {...option}
            selected={option.value === selected}
            key={option.value}
          />
        ))}
      </Dropdown>
    </div>
  );
};

const FullSampleActionElementContainer = () => {
  const [selected, setSelected] = useState('1');

  const handleChange = value => {
    setSelected(value);
  };

  return (
    <div style={{ width: '80%' }}>
      <div id="test" />
      <Dropdown
        actionElement={createDefaultActionElement({
          icon: options.find(opt => opt.value === selected)?.icon,
          text: options.find(opt => opt.value === selected)?.text || '',
        })}
        onChange={handleChange}
        portalId="test"
      >
        {options.map(option => (
          <DropdownOption
            {...option}
            selected={option.value === selected}
            key={option.value}
          />
        ))}
      </Dropdown>
    </div>
  );
};

export const StandardDropdownAttachedModeAndPositionRecalc: Story = {
  render: props => {
    return (
      <div>
        <div id="test">
          Go down to find the dropdown. Moving up and down you can test
          automatic dropdown menu placement
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            height: '1000px',
            justifyContent: 'center',
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Dropdown
              {...props}
              actionElement={createDefaultActionTextFieldElement()}
              portalId="test"
            >
              <DropdownOption text="Option 1" value="1" />
              <DropdownOption text="Option 2" value="2" />
              <DropdownOption text="Option 3" value="3" selected />
              <DropdownOption text="Option 4" value="4" />
              <DropdownOption text="Option 5" value="5" />
              <DropdownOption text="Option 6" value="6" />
              <DropdownOption text="Option 7" value="7" />
            </Dropdown>
          </div>
        </div>
      </div>
    );
  },
};

export const GenericDropdownWithIcons: Story = {
  render: () => (
    <div style={{ width: '80%' }}>
      <div id="test" />
      <Dropdown onActiveChange={fn()} onChange={fn()} portalId="test">
        <DropdownOption
          icon={<Icon type={IconType.ban} color={IconColor.error} />}
          text="Option 1"
          value="1"
          selected
        />
        <DropdownOption
          icon={<Icon type={IconType.checkCircle} color={IconColor.success} />}
          text="Option 2"
          value="2"
        />
        <DropdownOption
          icon={
            <Icon type={IconType.exclamationCircle} color={IconColor.warning} />
          }
          text="Option 3"
          value="3"
        />
      </Dropdown>
    </div>
  ),
};

export const MobileStyleDropdown: Story = {
  render: () => (
    <div style={{ width: '80%' }}>
      <div id="test" />
      <Dropdown
        actionElement={<button>...</button>}
        actionElementPosition={DropdownElementOrientation.left}
        onActiveChange={fn()}
        onChange={fn()}
        footer={<span>Cancel</span>}
        header={<span>This is the title</span>}
        asModal
        portalId="test"
      >
        <DropdownOption text="Option 1" value="1" selected />
        <DropdownOption text="Option 2" value="2" />
        <DropdownOption text="Option 3" value="3" />
      </Dropdown>
    </div>
  ),
};

export const RegularStyleListItemWithDropdown: Story = {
  render: () => (
    <div style={{ width: '80%' }}>
      <List>
        <RegularStyleListItem
          actionElement={
            <Dropdown
              actionElement={<button>...</button>}
              actionElementPosition={DropdownElementOrientation.left}
              onActiveChange={fn()}
              onChange={fn()}
              footer={<span>Cancel</span>}
              header={<span>This is the title</span>}
              asModal
              portalId="test"
              alignment={DropdownAlignment.center}
            >
              <DropdownOption text="Option 1" value="1" selected />
              <DropdownOption text="Option 2" value="2" />
              <DropdownOption text="Option 3" value="3" />
            </Dropdown>
          }
          title={<ListTitle>{'This is a title'}</ListTitle>}
          upperSubtitle="This is a subtitle"
        ></RegularStyleListItem>
      </List>
    </div>
  ),
};

export const FullSampleWithDefaultActionElement: Story = {
  render: () => <FullSampleActionElementContainer />,
};

export const FullSampleWithFiltering: Story = {
  render: () => <FullSampleFilteringContainer />,
};

export const DropdownWithColoredActionInput: Story = {
  render: () => {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          height: '500px',
          justifyContent: 'center',
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <FullSampleActionInputContainer
            actionElementProps={{
              textColor: GlobalType.primary,
              backgroundColor: GlobalType.primary,
              icon: undefined,
              iconLeft: <Icon type={IconType.flag} color={IconColor.primary} />,
              iconRight: (
                <Icon
                  type={IconType.angleDown}
                  size={IconSize.regular}
                  color={IconColor.primary}
                />
              ),
            }}
          />
        </div>
      </div>
    );
  },
};

export const ShouldShowTheSelectedOptionInTheDropdown = {
  render: () => {
    const maxOptions = 40;
    const selectedOption = 20;
    return (
      <div style={{ width: '80%' }}>
        <div id="test" />
        <Dropdown
          actionElement={createDefaultActionInputElement({
            name: 'text',
            onChange: fn(),
            text: `Option ${selectedOption}`,
            placeholder: 'Select an option',
          })}
          maxHeight={240}
          onChange={fn()}
          onActiveChange={fn()}
          portalId="test"
        >
          {Array.from({ length: maxOptions }).map((_, num) => (
            <DropdownOption
              key={num}
              text={`Option ${num + 1}`}
              value={`${num + 1}`}
              selected={num + 1 === selectedOption}
            />
          ))}
        </Dropdown>
      </div>
    );
  },
};
