import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import BoxSelector from '../src/components/BoxSelector/BoxSelector';
import { IconType } from '../src/components/Icons';
import { fn } from '@storybook/test';

export default {
  title: 'BoxSelector',
  component: BoxSelector,
  args: {
    fieldGroupName: 'example-group',
    handleInputCheckedChange: fn(),
    items: [
      {
        icon: IconType.comment,
        label: 'Communication skills',
        value: 'communication_skills',
      },
      {
        icon: IconType.heartPulse,
        label: 'Healthy lifestyle behaviors',
        value: 'healthy_lifestyle_behaviors',
      },
      {
        icon: IconType.faceSmileRelaxed,
        label: 'Emotional support',
        value: 'emotional_support',
      },
      {
        icon: IconType.lightbulbOn,
        label: 'Conflict resolution',
        value: 'conflict_resolution',
      },
      {
        icon: IconType.star,
        label: 'Quality time',
        value: 'quality_time',
      },
      {
        icon: IconType.graduationCap,
        label: 'Education & learning',
        value: 'education_and_learning',
      },
      {
        icon: IconType.headSideHeart,
        label: 'Mental health',
        value: 'mental_health',
      },
      {
        icon: IconType.utensils,
        label: 'Eating habits',
        value: 'eating_habits',
      },
      {
        icon: IconType.handHoldingHeard,
        label: 'Parenting & Co-parenting',
        value: 'parenting_and_co-parenting',
      },
      {
        icon: IconType.familyPants,
        label: 'Overall family wellbeing',
        value: 'overall_family_wellbeing',
      },
    ].map((content, index) => ({
      key: `key${index + 1}`,
      icon: content.icon,
      label: content.label,
      isDisabled: false,
      isSelected: false,
      value: content.value,
    })),
  },
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof BoxSelector>;

type Story = StoryObj<typeof BoxSelector>;

export const BoxSelectorStory: Story = {
  render: props => (
    <div style={{ padding: '30px', height: '473px', width: '350px' }}>
      <BoxSelector {...props} />
    </div>
  ),
};
