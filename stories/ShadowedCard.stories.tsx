import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import textWrapper from '../src/helpers/textWrapper';
import ShadowedCard from '../src/components/ShadowedCard/ShadowedCard';
import Text from '../src/components/Text/Text';

const meta: Meta<typeof ShadowedCard> = {
  title: 'ShadowedCard',
  component: ShadowedCard,
  decorators: [
    Story => (
      <div style={{ padding: '30px', maxWidth: '800px' }}>
        <Story />
      </div>
    ),
  ],
};

export default meta;

type Story = StoryObj<typeof ShadowedCard>;

export const Default: Story = {
  args: {
    shadow: 100,
  },
  render: ({ shadow }) => {
    const paragraphWrapper = textWrapper('paragraph', (key, line) => (
      <Text type="body1" marginBottom="16" key={key}>
        {line}
      </Text>
    ));

    return (
      <ShadowedCard shadow={shadow}>
        <Text
          type="headline1"
          weight="semibold"
          align="center"
          marginBottom={'16'}
        >
          {shadow === 100 ? 'Card options' : 'Lorem ipsum dolor sit amet'}
        </Text>
        {paragraphWrapper.wrap(
          shadow === 100
            ? 'Sample content'
            : 'Nulla sodales ligula risus, et consectetur nibh imperdiet eget. Integer leo tortor, porta non tincidunt id, euismod eget elit.\nSed finibus id odio lobortis vulputate. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla facilisi. Aenean vel interdum magna. Phasellus consectetur eros sed facilisis egestas. Proin vulputate quam vitae pretium fermentum.'
        )}
      </ShadowedCard>
    );
  },
};
