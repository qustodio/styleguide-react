import { Meta, StoryObj } from '@storybook/react';
import { Opener } from '../src';
import React from 'react';

export default {
  title: 'Opener',
  component: Opener,
  args: {
    type: 'chevron',
    open: false,
    testId: '',
  },
} as Meta<typeof Opener>;

type Story = StoryObj<typeof Opener>;

export const Default: Story = {};
