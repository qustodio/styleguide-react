import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import List, {
  ListColoredText,
  ListCompactSubtitle,
  ListItemVerticalAlign,
} from '../src/components/List';
import Icon, {
  IconColor,
  IconFamily,
  IconSize,
  IconType,
} from '../src/components/Icons';
import CompactStyleListItem from '../src/components/List/ListItems/CompactStyleListItem';
import YoutubeSvg from './assets/images/Icons/icon-youtube.svg';
import Badge, { BadgeType } from '../src/components/Badge/Badge';
import MoreActionsButton from '../src/components/Button/MoreActionsButton';
import RegularStyleListItem from '../src/components/List/ListItems/RegularStyleListItem';
import FreeStyleListItem from '../src/components/List/ListItems/FreeStyleListItem';
import LocationStyleListItem from '../src/components/List/ListItems/LocationStyleListItem';
import CustomStyleListItem from '../src/components/List/ListItems/CustomStyleListItem';
import SelectableListItem from '../src/components/List/ListItems/SelectableListItem';
import Avatar from '../src/components/Avatar/Avatar';
import { AvatarSize } from '../src/components/Avatar/Avatar.types';
import BoyAvatarSvg from './assets/images/Avatars/boy.svg';
import ListSubtitle from '../src/components/List/utils/ListSubtitle';
import ListLink from '../src/components/List/utils/ListLink';
import ListTitle from '../src/components/List/utils/ListTitle';
import ListThumbnail from '../src/components/List/utils/ListThumbnail';
import Switch from '../src/components/Switch/Switch';
import ClickableListItem from '../src/components/List/utils/ClickableListItem';
import Quote from '../src/components/Quote/Quote';
import CloudList from '../src/components/List/CloudList';
import ActionInput from '../src/components/ActionInput/ActionInput';
import DeactivableContent from '../src/components/DeactivableContent/DeactivableContent';
import { GlobalType } from '../src/common/types';
import { fn } from '@storybook/test';
import { Text } from '../src';

export default {
  title: 'Lists/CardList',
  component: List,
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof List>;

const containerStyle = {
  justifyElements: 'center',
  width: '500px',
  height: 'auto',
  padding: '20px',
};

const iconContainerStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: '24px',
  height: '24px',
  borderRadius: '20%',
  backgroundColor: 'white',
  boxShadow: '0 0 0 1.3px #efefef',
  marginLeft: '1px',
};

type Story = StoryObj<typeof List>;

export const CardList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List bottomPadding="small" listItemMarginBottom="16">
        <CompactStyleListItem
          title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"
          subtitle={
            <ListCompactSubtitle>
              <ListColoredText color="warning">{'subtitle 1'}</ListColoredText>
            </ListCompactSubtitle>
          }
          rightTitle="Time spent"
          icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
        />
        <CompactStyleListItem
          title="style 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"
          subtitle={
            <ListCompactSubtitle>
              <ListColoredText color="warning">{'subtitle 1'}</ListColoredText>
            </ListCompactSubtitle>
          }
          rightTitle="Time spent"
          icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
          onInfoActionClick={() => {
            console.log('info clicked');
          }}
        />
        <CompactStyleListItem
          title="title 2"
          rightTitle="Time spent"
          rightSubtitle="2h"
          icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/8ccf72b39839d7da0436288e69e15942e071b634.png"
        />
        <CompactStyleListItem
          title="title 3"
          subtitle="subtitle 3"
          rightTitle="Time spent"
          rightSubtitle="40m"
          icon={
            <div style={iconContainerStyle}>
              <Icon type={IconType.star} square color={IconColor.error} />
            </div>
          }
        />
        <CompactStyleListItem
          title="title 4"
          subtitle="subtitle 4"
          rightSubtitle="1h 30m"
          icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
        />
        <CompactStyleListItem
          title="title 5"
          subtitle="subtitle 5"
          rightTitle={
            <Text align="right" color="brand" variant="body-1-semibold">
              {'Time spent'}
            </Text>
          }
          rightSubtitle="2h"
        />
        <CompactStyleListItem
          title="title 6"
          rightSubtitle="340m"
          icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/a414891b42d884fff0e5ce9bafcf331141a75a4b.png"
        />
        <CompactStyleListItem title="only title" />
        <CompactStyleListItem
          title="only title and icon"
          icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
        />
        <CompactStyleListItem
          title="only title and icon"
          icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
          onInfoActionClick={() => {
            console.log('info clicked');
          }}
        />
      </List>
    </div>
  ),
};

export const DevicesLikeList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List>
        <RegularStyleListItem
          icon={
            <Avatar size={AvatarSize.small}>
              <Icon type={IconType.mobile} family={IconFamily.solid} />
            </Avatar>
          }
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Device name"
          upperSubtitle="Assigned to: Ramon"
          lowerSubtitle="Protection enabled"
          actionElement={
            <ClickableListItem onClick={() => alert('Icon clicked!')}>
              <Icon
                type={IconType.angleRight}
                size={IconSize.lg}
                color={IconColor.regular}
              />
            </ClickableListItem>
          }
          actionElementVerticalAlign={ListItemVerticalAlign.center}
        />
        <RegularStyleListItem
          icon={
            <Avatar size={AvatarSize.small}>
              <Icon type={IconType.mobile} family={IconFamily.solid} />
            </Avatar>
          }
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Device name"
          upperSubtitle="Assigned to: Ramon"
          lowerSubtitle="Protection enabled"
          actionElement={
            <Icon
              type={IconType.angleRight}
              size={IconSize.lg}
              color={IconColor.regular}
            />
          }
          actionElementVerticalAlign={ListItemVerticalAlign.center}
        />
        <RegularStyleListItem
          icon={
            <Avatar size={AvatarSize.small}>
              <Icon type={IconType.mobile} family={IconFamily.solid} />
            </Avatar>
          }
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Device name"
          upperSubtitle="Assigned to: Ramon"
          lowerSubtitle="Protection enabled"
          actionElement={
            <Icon
              type={IconType.angleRight}
              size={IconSize.lg}
              color={IconColor.regular}
            />
          }
          actionElementVerticalAlign={ListItemVerticalAlign.center}
        />
      </List>
    </div>
  ),
};

export const RegularListLikeWebfilters: Story = {
  render: () => (
    <div style={containerStyle}>
      <List listItemMarginBottom="16">
        <RegularStyleListItem
          icon={<Icon type={IconType.ban} color={IconColor.error} square />}
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Category 1"
          actionElement={
            <ClickableListItem onClick={() => alert('Icon clicked!')}>
              <Icon
                type={IconType.angleRight}
                size={IconSize.lg}
                color={IconColor.regular}
              />
            </ClickableListItem>
          }
          actionElementVerticalAlign={ListItemVerticalAlign.center}
        />
        <RegularStyleListItem
          icon={<Icon type={IconType.ban} color={IconColor.error} square />}
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Category 2"
          actionElement={
            <ClickableListItem onClick={() => alert('Icon clicked!')}>
              <Icon
                type={IconType.angleRight}
                size={IconSize.lg}
                color={IconColor.regular}
              />
            </ClickableListItem>
          }
          actionElementVerticalAlign={ListItemVerticalAlign.center}
        />
        <RegularStyleListItem
          icon={
            <Icon
              type={IconType.exclamation}
              family={IconFamily.solid}
              color={IconColor.warning}
              square
            />
          }
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Category 3"
          actionElement={
            <ClickableListItem onClick={() => alert('Icon clicked!')}>
              <Icon
                type={IconType.angleRight}
                size={IconSize.lg}
                color={IconColor.regular}
              />
            </ClickableListItem>
          }
          actionElementVerticalAlign={ListItemVerticalAlign.center}
        />
      </List>
    </div>
  ),
};

export const FamilyLocatorLikeList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List>
        <RegularStyleListItem
          icon={
            <Avatar size={AvatarSize.small}>
              <img src={BoyAvatarSvg} />
            </Avatar>
          }
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Kid name"
          upperSubtitle="Avinguda Paralel"
          lowerSubtitle="08001 Barcelona"
          actionElement={<MoreActionsButton onClick={() => {}} />}
        />
        <RegularStyleListItem
          icon={
            <Avatar size={AvatarSize.small}>
              <img src={BoyAvatarSvg} />
            </Avatar>
          }
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Kid name"
          upperSubtitle="Avinguda Paralel"
          lowerSubtitle="08001 Barcelona"
          actionElement={<MoreActionsButton onClick={() => {}} />}
        />
        <RegularStyleListItem
          icon={
            <Avatar size={AvatarSize.small}>
              <img src={BoyAvatarSvg} />
            </Avatar>
          }
          iconVerticalAlign={ListItemVerticalAlign.center}
          title="Kid name"
          upperSubtitle="Avinguda Paralel"
          lowerSubtitle="08001 Barcelona"
          actionElement={<MoreActionsButton onClick={() => {}} />}
        />
      </List>
    </div>
  ),
};

export const CallSMSLikeList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List>
        <RegularStyleListItem
          icon={<Icon type={IconType.ban} color={IconColor.regular} />}
          title="This is a contact"
          upperSubtitle="When enabled, you’ll recieve an email alert when a blocked website is visited."
        />
        <RegularStyleListItem
          icon={<Icon type={IconType.ban} color={IconColor.regular} />}
          title="This is a contact"
          upperSubtitle="When enabled, you’ll recieve an email alert when a blocked website is visited."
        />
        <RegularStyleListItem
          icon={<Icon type={IconType.ban} color={IconColor.regular} />}
          title="This is a contact"
          upperSubtitle="When enabled, you’ll recieve an email alert when a blocked website is visited."
        />
      </List>
    </div>
  ),
};

export const CalSMSNoLowerSubtitleLikeList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List>
        <RegularStyleListItem
          icon={<Icon type={IconType.checkCircle} color={IconColor.success} />}
          title="Contact name"
          upperSubtitle="This is a subtitle"
        />
        <RegularStyleListItem
          icon={<Icon type={IconType.ban} color={IconColor.error} />}
          title="Contact name"
          upperSubtitle="This is a subtitle"
        />
        <RegularStyleListItem
          icon={<Icon type={IconType.checkCircle} color={IconColor.success} />}
          title="Contact name"
          upperSubtitle="This is a subtitle"
        />
      </List>
    </div>
  ),
};

export const WebFilteringCategoriesLikeList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List bottomPadding="small">
        <RegularStyleListItem
          icon={<Icon type={IconType.ban} color={IconColor.error} />}
          title="This is a Web category blocked This is a Web category blocked long text"
        />
        <RegularStyleListItem
          icon={<Icon type={IconType.checkCircle} color={IconColor.success} />}
          title="This is a Web category allowed"
        />
        <RegularStyleListItem
          icon={
            <Icon type={IconType.exclamationCircle} color={IconColor.warning} />
          }
          title="This is a Web category with warning"
        />
        <RegularStyleListItem
          icon={<Icon type={IconType.timesCircle} color={IconColor.neutral} />}
          title="This is a Web category ignored"
        />
      </List>
    </div>
  ),
};

export const AvatarLikeList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List listItemMarginTop="16" listItemPaddingBottom="8">
        <RegularStyleListItem
          centerMiddleContent
          iconVerticalAlign={ListItemVerticalAlign.center}
          icon={
            <Avatar size={AvatarSize.extraExtraSmall}>
              <img src={BoyAvatarSvg} alt="Boy Avatar" />
            </Avatar>
          }
          title="Contact name"
        />
        <RegularStyleListItem
          centerMiddleContent
          iconVerticalAlign={ListItemVerticalAlign.center}
          icon={
            <Avatar size={AvatarSize.extraExtraSmall}>
              <img src={BoyAvatarSvg} alt="Boy Avatar" />
            </Avatar>
          }
          title="Contact name"
        />
        <RegularStyleListItem
          centerMiddleContent
          iconVerticalAlign={ListItemVerticalAlign.center}
          icon={
            <Avatar size={AvatarSize.extraExtraSmall}>
              <img src={BoyAvatarSvg} alt="Boy Avatar" />
            </Avatar>
          }
          title="Contact name"
        />
      </List>
    </div>
  ),
};

export const LocationLikeList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List>
        <LocationStyleListItem
          leftSubtitle=" 9:30"
          upperTitle="Passeig de Gracia, 19,"
          lowerTitle="08004 Barcelona"
        />
        <LocationStyleListItem
          leftSubtitle="12:11"
          upperTitle="Passeig de Gracia, 19,"
          lowerTitle="08004 Barcelona"
        />
        <LocationStyleListItem
          leftSubtitle="23:00"
          upperTitle="Passeig de Gracia, 19,"
          lowerTitle="08004 Barcelona"
        />
      </List>
    </div>
  ),
};

const styleFooterContainer = {
  flex: '1 1 auto',
  display: 'flex',
  flexDirection: 'column',
};

const styleFooterText = {
  color: '#b0b4bd',
  fontSize: '14px',
  marginLeft: '8px',
};

const styleFooterSeparator = {
  borderTop: '1px solid #e1e3e6',
  flex: '1 1 auto',
  margin: '5px 0 5px 0',
};

const styleFooterIcon = { marginLeft: '46px' };

const styleIconWrapper = { display: 'inline-flex' };

const styleBodyContainer = { display: 'flex', alignItems: 'center' };

const styleBodyText = { color: '#b0b4bd', fontSize: '14px' };

const styleBodyIconDown = {
  color: '#2aabcb',
  marginLeft: '8px',
  height: '22px',
  display: 'flex',
  alignItems: 'center',
};

export const CustomizableLikeAppList: Story = {
  render: () => {
    const disabled = false;

    return (
      <div style={containerStyle}>
        <List listItemMarginTop="16">
          <CustomStyleListItem
            disabled={disabled}
            icon={<img src={YoutubeSvg} />}
            title="Youtube AppYoutube"
            footerContent={
              <div style={styleFooterContainer}>
                <div style={styleFooterSeparator} />
                <div style={styleFooterIcon}>
                  <span style={styleIconWrapper}>
                    <Icon
                      type={IconType.apple}
                      family={IconFamily.brands}
                      color={IconColor.neutral}
                    />
                  </span>
                  <span style={styleFooterText}>iOS</span>
                </div>
              </div>
            }
            rightContent={<Switch onClick={() => {}} onChange={() => {}} />}
          >
            <p style={styleBodyContainer}>
              <span style={styleBodyText}>This is a customizable content</span>
              <span style={styleBodyIconDown}>
                {' '}
                <Icon type={IconType.angleDown} color={IconColor.neutral} />
              </span>
            </p>
          </CustomStyleListItem>
          <CustomStyleListItem
            disabled={disabled}
            icon={<img src={YoutubeSvg} />}
            title="Youtube App"
            footerContent={
              <div style={styleFooterContainer}>
                <div style={styleFooterSeparator} />
                <div style={styleFooterIcon}>
                  <span style={styleIconWrapper}>
                    <Icon
                      type={IconType.apple}
                      family={IconFamily.brands}
                      color={IconColor.neutral}
                    />
                  </span>
                  <span style={styleFooterText}>iOS</span>
                </div>
              </div>
            }
            rightContent={<Switch onClick={() => {}} onChange={() => {}} />}
          >
            <p style={styleBodyContainer}>
              <span style={styleBodyText}>This is a customizable content</span>
              <span style={styleBodyIconDown}>
                {' '}
                <Icon type={IconType.angleDown} color={IconColor.neutral} />
              </span>
            </p>
          </CustomStyleListItem>
          <CustomStyleListItem
            disabled={disabled}
            icon={<img src={YoutubeSvg} />}
            title="Youtube App"
            footerContent={
              <div style={styleFooterContainer}>
                <div style={styleFooterSeparator} />
                <div style={styleFooterIcon}>
                  <span style={styleIconWrapper}>
                    <Icon
                      type={IconType.apple}
                      family={IconFamily.brands}
                      color={IconColor.neutral}
                    />
                  </span>
                  <span style={styleFooterText}>iOS</span>
                </div>
              </div>
            }
            rightContent={<Switch onClick={() => {}} onChange={() => {}} />}
            onInfoActionClick={() => alert('Info action clicked')}
          >
            <p style={styleBodyContainer}>
              <span style={styleBodyText}>This is a customizable content</span>
              <span style={styleBodyIconDown}>
                {' '}
                <Icon type={IconType.angleDown} color={IconColor.neutral} />
              </span>
            </p>
          </CustomStyleListItem>
          <CustomStyleListItem
            disabled={disabled}
            icon={<img src={YoutubeSvg} />}
            title="Youtube kids App"
            rightContent={
              <ActionInput
                textColor={GlobalType.secondary}
                iconLeft={
                  <Icon
                    square
                    type={IconType.stopWatch}
                    color={IconColor.secondary}
                  />
                }
                text="Option"
                iconRight={
                  <Icon type={IconType.angleDown} color={IconColor.neutral} />
                }
              />
            }
          >
            <span style={styleIconWrapper}>
              <Icon
                type={IconType.apple}
                family={IconFamily.brands}
                color={IconColor.neutral}
              />
            </span>
            <span style={styleFooterText}>iOS</span>
          </CustomStyleListItem>
          <CustomStyleListItem
            disabled={disabled}
            icon={<img src={YoutubeSvg} />}
            title="Youtube kids App with action"
            onInfoActionClick={() => alert('Info action clicked')}
            rightContent={
              <ActionInput
                textColor={GlobalType.secondary}
                iconLeft={
                  <Icon
                    square
                    type={IconType.stopWatch}
                    color={IconColor.secondary}
                  />
                }
                text="Option"
                iconRight={
                  <Icon type={IconType.angleDown} color={IconColor.neutral} />
                }
              />
            }
          >
            <span style={styleIconWrapper}>
              <Icon
                type={IconType.apple}
                family={IconFamily.brands}
                color={IconColor.neutral}
              />
            </span>
            <span style={styleFooterText}>iOS</span>
          </CustomStyleListItem>
          <CustomStyleListItem
            disabled={disabled}
            icon={<img src={YoutubeSvg} />}
            title="Some other App"
            rightContent={
              <ActionInput
                compact={true}
                iconLeft={
                  <Icon
                    square
                    type={IconType.stopWatch}
                    color={IconColor.secondary}
                    size={IconSize.lg}
                  />
                }
                iconRight={
                  <Icon type={IconType.angleDown} color={IconColor.neutral} />
                }
              />
            }
          >
            <span style={styleIconWrapper}>
              <Icon
                type={IconType.apple}
                family={IconFamily.brands}
                color={IconColor.neutral}
              />
            </span>
            <span style={styleFooterText}>iOS</span>
          </CustomStyleListItem>
          <CustomStyleListItem
            disabled={disabled}
            icon={<img src={YoutubeSvg} />}
            title="Some other App with an action"
            onInfoActionClick={() => alert('Info action clicked')}
            rightContent={
              <ActionInput
                compact={true}
                iconLeft={
                  <Icon
                    square
                    type={IconType.stopWatch}
                    color={IconColor.secondary}
                    size={IconSize.lg}
                  />
                }
                iconRight={
                  <Icon type={IconType.angleDown} color={IconColor.neutral} />
                }
              />
            }
          >
            <span style={styleIconWrapper}>
              <Icon
                type={IconType.apple}
                family={IconFamily.brands}
                color={IconColor.neutral}
              />
            </span>
            <span style={styleFooterText}>iOS</span>
          </CustomStyleListItem>
          <CustomStyleListItem
            title={
              <DeactivableContent disabled={disabled}>
                <ListTitle>Deactivable</ListTitle>
              </DeactivableContent>
            }
            icon={
              <DeactivableContent disabled={false}>
                <img src={YoutubeSvg} />
              </DeactivableContent>
            }
            onInfoActionClick={() => alert('Info action clicked')}
          >
            <span style={styleIconWrapper}>
              <Icon
                type={IconType.apple}
                family={IconFamily.brands}
                color={IconColor.neutral}
              />
            </span>
            <span style={styleFooterText}>iOS</span>
          </CustomStyleListItem>
        </List>
      </div>
    );
  },
};

export const FreeStyleList: Story = {
  render: () => (
    <List listItemPaddingTop="24">
      <FreeStyleListItem>
        <ListSubtitle>Table cell</ListSubtitle>
        <ListTitle>Caption text</ListTitle>
        <ListSubtitle>Table cell</ListSubtitle>
        <div style={{ marginTop: '8px', width: '200px', height: '200px' }}>
          <div style={{ height: '100%' }}>
            <ListThumbnail
              url="https://www.youtube.com/watch?v=8plKfz_kUws"
              thumbnailUrl="https://img.youtube.com/vi/8plKfz_kUws/mqdefault.jpg"
            />
          </div>
        </div>
      </FreeStyleListItem>
      <FreeStyleListItem>
        <ListSubtitle size="medium">11:49 PM - Home page</ListSubtitle>
        <ListLink isBold color="danger">
          http://example.com
        </ListLink>
        <div style={{ marginTop: '8px' }}>
          <Badge text="Tag with text" type={BadgeType.error} />
        </div>
      </FreeStyleListItem>
    </List>
  ),
};

export const ClickableList: Story = {
  render: () => (
    <List listItemMarginTop="16" listItemPaddingLeft="16">
      <RegularStyleListItem
        title={
          <ClickableListItem onClick={() => alert('data')}>
            <ListTitle>This is a title</ListTitle>
          </ClickableListItem>
        }
        icon={<Icon type={IconType.camera} color={IconColor.error} />}
      />
      <RegularStyleListItem
        title={
          <ClickableListItem onClick={() => alert('data')}>
            <ListTitle>This is a title</ListTitle>
          </ClickableListItem>
        }
        icon={<Icon type={IconType.calendarEdit} color={IconColor.error} />}
      />
      <RegularStyleListItem
        title={
          <ClickableListItem onClick={() => alert('data')}>
            <ListTitle>This is a title</ListTitle>
          </ClickableListItem>
        }
        icon={<Icon type={IconType.checkCircle} color={IconColor.error} />}
      />
    </List>
  ),
};

export const CheckboxList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List
        onClickItem={() => {
          alert('clicked parent element');
        }}
        ariaRole="group"
        listItemMarginBottom="8"
      >
        <SelectableListItem
          id="1"
          type="checkbox"
          selected
          icon={<img src={YoutubeSvg} />}
          title="Youtube"
        />
        <SelectableListItem
          id="2"
          type="checkbox"
          selected
          icon={<img src={YoutubeSvg} />}
          title="Youtube for kids"
          subtitle={
            <p style={styleBodyContainer}>
              <span style={styleBodyText}>Android</span>
            </p>
          }
        />
        <SelectableListItem
          id="16"
          type="checkbox"
          selected
          icon={<img src={YoutubeSvg} />}
          title="Youtube for kids"
          subtitle={
            <p style={styleBodyContainer}>
              <span style={styleBodyText}>Android</span>
            </p>
          }
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem
          id="3"
          type="checkbox"
          icon={<img src={YoutubeSvg} />}
          title="Whatsapp"
          subtitle={
            <p style={styleBodyContainer}>
              <span style={styleBodyText}>iOS</span>
            </p>
          }
        />
        <SelectableListItem
          id="4"
          type="checkbox"
          selected
          icon={<Icon type={IconType.smoking} />}
          title="Tobacco"
        />
        <SelectableListItem
          id="5"
          type="checkbox"
          selected
          icon={<Icon type={IconType.music} />}
          title="Music"
        />
        <SelectableListItem
          id="5"
          type="checkbox"
          selected
          icon={<Icon type={IconType.music} />}
          title="Music"
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem
          id="6"
          type="checkbox"
          icon={<Icon type={IconType.gamepad} />}
          title="Games"
        />
        <SelectableListItem
          id="7"
          type="checkbox"
          icon={<Icon type={IconType.sportsball} />}
          title="Sports"
        />
        <SelectableListItem
          id="14"
          type="checkbox"
          icon={<Icon type={IconType.sportsball} />}
          title="Sports"
          onClick={() => alert('Click parent element')}
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem
          id="8"
          type="checkbox"
          isDisabled
          icon={<Icon type={IconType.ban} />}
          title="Disabled"
        />
        <SelectableListItem
          id="15"
          type="checkbox"
          isDisabled
          icon={<Icon type={IconType.ban} />}
          title="Disabled"
          onClick={() => alert('Click parent element')}
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem
          id="9"
          type="checkbox"
          selected
          isDisabled
          icon={<Icon type={IconType.minusCircle} />}
          title="Disabled and checked"
        />
        <SelectableListItem
          id="10"
          type="checkbox"
          icon={<Icon type={IconType.faceSwear} />}
          title="With title and description"
          description="This is the space to add more info about this option"
        />
        <SelectableListItem
          id="17"
          type="checkbox"
          icon={<Icon type={IconType.faceSwear} />}
          title="With title and description"
          description="This is the space to add more info about this option"
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem
          id="11"
          type="checkbox"
          selected
          icon={<Icon type={IconType.rocketLaunch} />}
          title="With title and description"
          description="This is the space to add more info about this option"
        />
        <SelectableListItem
          id="12"
          type="checkbox"
          icon={<Icon type={IconType.thumbsUp} />}
          title="With title, subtitle and description"
          subtitle={
            <p style={styleBodyContainer}>
              <span style={styleBodyText}>iOS</span>
            </p>
          }
          description="This is the space to add more info about this option"
        />
        <SelectableListItem id="13" type="checkbox" title="With no icon" />
        <SelectableListItem
          id="18"
          type="checkbox"
          icon={<Icon type={IconType.thumbsUp} />}
          title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          subtitle={
            <p style={styleBodyContainer}>
              <span style={styleBodyText}>iOS</span>
            </p>
          }
          description="This is the space to add more info about this option"
        />
      </List>
    </div>
  ),
};

export const RadioList: Story = {
  render: () => (
    <div style={containerStyle}>
      <List
        listItemMarginBottom="16"
        onClickItem={action('onClickItem')}
        ariaRole="radiogroup"
        tabindex="0"
      >
        <SelectableListItem
          id="4"
          type="radio"
          selected
          icon={<Icon type={IconType.smoking} />}
          title="Tobacco"
        />
        <SelectableListItem
          id="5"
          type="radio"
          selected
          icon={<Icon type={IconType.music} />}
          title="Music"
          description="Selected option"
        />
        <SelectableListItem
          id="11"
          type="radio"
          selected
          icon={<Icon type={IconType.music} />}
          title="Music"
          description="Selected option"
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem
          id="6"
          type="radio"
          icon={<Icon type={IconType.gamepad} />}
          title="Games"
        />
        <SelectableListItem
          id="7"
          type="radio"
          icon={<Icon type={IconType.sportsball} />}
          title="Sports"
        />
        <SelectableListItem
          id="7"
          type="radio"
          icon={<Icon type={IconType.camera} />}
          title="With subtitle"
          description="This is the space to add more info about this option"
        />
        <SelectableListItem
          id="13"
          type="radio"
          icon={<Icon type={IconType.camera} />}
          title="With subtitle"
          description="This is the space to add more info about this option"
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem
          id="8"
          type="radio"
          isDisabled
          icon={<Icon type={IconType.ban} />}
          title="Disabled"
        />
        <SelectableListItem
          id="12"
          type="radio"
          isDisabled
          icon={<Icon type={IconType.ban} />}
          title="Disabled"
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem
          id="9"
          type="radio"
          boldTitleOnSelected
          icon={<Icon type={IconType.buildingColumns} />}
          title="Will not be bold when not selected"
        />
        <SelectableListItem
          id="10"
          type="radio"
          selected
          boldTitleOnSelected
          icon={<Icon type={IconType.exclamationTriangle} />}
          title="It's bold when selected"
        />
        <SelectableListItem
          id="14"
          type="radio"
          selected
          boldTitleOnSelected
          icon={<Icon type={IconType.exclamationTriangle} />}
          title="It's bold when selected"
          onInfoActionClick={() => alert('Click info action')}
        />
        <SelectableListItem type="radio" id="11" title="With no icon" />
      </List>
    </div>
  ),
};

export const HorizontalList: Story = {
  render: () => (
    <List horizontalOrientation listItemPaddingLeft="16">
      <FreeStyleListItem>This is item 1</FreeStyleListItem>
      <FreeStyleListItem>This is item 2</FreeStyleListItem>
      <FreeStyleListItem>This is item 3</FreeStyleListItem>
    </List>
  ),
};

export const ListWith3Columns: Story = {
  render: () => (
    <List columns={3}>
      <FreeStyleListItem>This is item 1</FreeStyleListItem>
      <FreeStyleListItem>This is item 2</FreeStyleListItem>
      <FreeStyleListItem>This is item 3</FreeStyleListItem>
      <FreeStyleListItem>This is item 4</FreeStyleListItem>
      <FreeStyleListItem>This is item 5</FreeStyleListItem>
    </List>
  ),
};

export const ListWithThreeColumnsNoNative: Story = {
  render: () => (
    <List columns={3} useNativeColumns={false}>
      <FreeStyleListItem>This is item 1</FreeStyleListItem>
      <FreeStyleListItem>This is item 2</FreeStyleListItem>
      <FreeStyleListItem>This is item 3</FreeStyleListItem>
      <FreeStyleListItem>This is item 4</FreeStyleListItem>
      {/* <FreeStyleListItem>This is item 5</FreeStyleListItem> */}
    </List>
  ),
};

const availableMaxWidth = {
  '20%': '20%',
  '25%': '25%',
  '30%': '30%',
  '35%': '35%',
  '40%': '40%',
  '45%': '45%',
  '50%': '50%',
  '55%': '55%',
  '60%': '60%',
  '65%': '65%',
  '70%': '70%',
  '75%': '75%',
  '80%': '80%',
  '100%': '100%',
};

export const CloudListDefault: Story = {
  render: () => (
    <div style={{ width: '200px', border: 'solid 1px black' }}>
      <CloudList allowedListItemType={Quote} listItemMaxWidth={'100%'}>
        <Quote>es un test</Quote>
        <Quote>es un test es un test es un test es un test</Quote>
        <Quote>es un test es un test</Quote>
        <Quote>test</Quote>
        <Quote>test</Quote>
        <Quote>test</Quote>
        <Quote>es un test es un test</Quote>
      </CloudList>
    </div>
  ),
};
