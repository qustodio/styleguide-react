import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import { GlobalType } from '../src/common/types';
import Card from '../src/components/Card/Card';
import Icon, { IconColor, IconSize, IconType } from '../src/components/Icons';
import List, {
  CompactStyleListItem,
  ListColoredText,
  ListCompactTitle,
} from '../src/components/List';
import {
  AlertBox,
  AlertBoxType,
  CardActionTitle,
  FlexLayout,
  Switch,
} from '../src';
import { fn } from '@storybook/test';

const cardTypes = {
  Default: GlobalType.default,
  primary: GlobalType.primary,
  secondary: GlobalType.secondary,
  success: GlobalType.success,
  warning: GlobalType.warning,
  error: GlobalType.error,
  gray: GlobalType.gray,
  None: undefined,
};

export default {
  title: 'Card',
  component: Card,
  args: {
    width: 420,
    height: 100,
    type: GlobalType.default,
  },
  argTypes: {
    type: {
      control: 'select',
      options: Object.keys(cardTypes),
      mapping: cardTypes,
    },
  },
} as Meta<typeof Card>;

type Story = StoryObj<typeof Card>;

const list1 = () => (
  <List showSeparator={false}>
    <CompactStyleListItem
      title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"
      subtitle="subtitle 1"
      rightTitle="Time spent"
      icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
      rightSubtitle="2h"
      bottomPadding={'compact'}
    />
    <CompactStyleListItem
      title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"
      subtitle="subtitle 1"
      rightTitle="Time spent"
      icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
      rightSubtitle="2h"
      bottomPadding={'compact'}
    />
    <CompactStyleListItem
      title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"
      subtitle="subtitle 1"
      rightTitle="Time spent"
      icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
      rightSubtitle="2h"
      bottomPadding={'compact'}
    />
  </List>
);
const list2 = () => (
  <List showSeparator={false}>
    <CompactStyleListItem
      title={
        <ListCompactTitle>
          <ListColoredText color="error">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod
          </ListColoredText>
        </ListCompactTitle>
      }
      subtitle="subtitle 1"
      rightTitle="Time spent"
      icon="https://s3.amazonaws.com/static-pre.qustodio.com/app/f9df002f82d24d6d38851429f2f1f1fb2bec5672.png"
      rightSubtitle="2h"
      bottomPadding={'compact'}
    />
  </List>
);

const list3 = () => (
  <List showSeparator={false}>
    <CompactStyleListItem
      title={<ListCompactTitle>656.54.56</ListCompactTitle>}
      rightTitle={
        <FlexLayout mainaxis="row" mainaxisAlignment="space-around">
          <span>
            <Icon
              type={IconType.phone}
              color={IconColor.regular}
              size={IconSize.sm}
            />{' '}
            2
          </span>
          <span>
            <Icon
              type={IconType.comments}
              color={IconColor.regular}
              size={IconSize.sm}
            />{' '}
            4
          </span>
        </FlexLayout>
      }
      extraRightSpace
      rightSubtitle={''}
      bottomPadding={'compact'}
    />
  </List>
);

export const CardWithText: Story = {
  args: {
    width: 420,
    backgroundGradient: false,
    backgroundShapes: false,
  },
  render: ({ width, type, backgroundGradient, backgroundShapes }) => {
    return (
      <div
        style={{
          margin: '24px',
          backgroundColor: '#f8fafd',
          height: '400px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Card
          width={width}
          type={type}
          header={{
            title: 'This is a title',
            actionElement: (
              <div style={{ fontSize: '16px' }}>
                <i className="far fa-info-circle"></i>
              </div>
            ),
            border: false,
            onClickAction: fn(),
          }}
          footer={{
            actionElement: 'link 2',
            onClickAction: fn(),
          }}
          backgroundGradient={backgroundGradient}
          backgroundShapes={backgroundShapes}
        >
          {list1()}
        </Card>
      </div>
    );
  },
};

export const CardWithErrorText: Story = {
  render: ({ width, type }) => {
    return (
      <div
        style={{
          margin: '24px',
          backgroundColor: '#f8fafd',
          height: '400px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Card
          width={width}
          type={type}
          header={{
            title: 'This is a title',
            actionElement: (
              <div style={{ fontSize: '16px' }}>
                <i className="far fa-info-circle"></i>
              </div>
            ),
            border: false,
            onClickAction: fn(),
          }}
          footer={{
            actionElement: 'link 2',
            onClickAction: fn(),
          }}
        >
          {list2()}
        </Card>
      </div>
    );
  },
};

export const CardWithIconCallsAtRight: Story = {
  render: ({ width, type }) => {
    return (
      <div
        style={{
          margin: '24px',
          backgroundColor: '#f8fafd',
          height: '400px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Card
          width={width}
          type={type}
          header={{
            title: 'This is a title',
            actionElement: (
              <div style={{ fontSize: '16px' }}>
                <i className="far fa-info-circle"></i>
              </div>
            ),
            border: false,
            onClickAction: fn(),
          }}
          footer={{
            actionElement: 'link 2',
            onClickAction: fn(),
          }}
        >
          {list3()}
        </Card>
      </div>
    );
  },
};

export const CardWithActionableIcon: Story = {
  args: {
    width: 320,
  },
  render: ({ width, type }) => {
    return (
      <div style={{ margin: '24px' }}>
        <Card
          width={width}
          type={type}
          header={{
            title: 'This is a title',
            actionElement: 'link 1',
            onClickAction: fn(),
            border: false,
          }}
        >
          Card with text
        </Card>
      </div>
    );
  },
};

export const CardWithGreenOrYellowBackground: Story = {
  args: {
    width: 320,
  },
  render: ({ width, type }) => {
    return (
      <div style={{ margin: '24px' }}>
        <Card width={width} type={type}>
          Card with text
        </Card>
      </div>
    );
  },
};

export const CardWithBanner: Story = {
  args: {
    width: 320,
  },
  render: ({ width }) => (
    <div style={{ margin: '24px' }}>
      <Card
        minHeight="520"
        width={width}
        header={{
          banner: (
            <AlertBox type={AlertBoxType.error} fullWidth>
              This is a banner <a>with a link</a>
            </AlertBox>
          ),
        }}
      >
        This is the body
      </Card>
    </div>
  ),
};

export const CardWithGradientAndShapes: Story = {
  args: {
    type: GlobalType.secondary,
  },
  render: ({ width, height, type }) => (
    <div style={{ margin: '24px' }}>
      <Card
        width={width}
        height={height}
        type={type}
        backgroundGradient={true}
        backgroundShapes={true}
      >
        This is the body
      </Card>
    </div>
  ),
};

export const CardWithHeaderType: Story = {
  render: ({ width, type }) => (
    <div
      style={{
        margin: '24px',
        backgroundColor: '#f8fafd',
        height: '400px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Card
        width={width}
        type={type}
        header={{
          type: cardTypes.None,
          title: 'This is a title',
        }}
      >
        {list1()}
      </Card>
    </div>
  ),
};

export const ClickableHeader: Story = {
  render: ({ type }) => {
    return (
      <div style={{ margin: '24px', maxWidth: 500 }}>
        <Card
          type={type}
          header={{
            title: (
              <CardActionTitle color="inherit">
                Click me! I'm wrapper in {`<a>`}
              </CardActionTitle>
            ),
            actionElement: (
              <Icon type={IconType.chevronRight} size={IconSize.lg} />
            ),
            wrapper: ({ ...props }) => (
              <a {...props} onClick={() => alert('I was clicked')} />
            ),
          }}
        />
        <br />
        <br />
        <Card
          type={cardTypes.gray}
          header={{
            title: (
              <CardActionTitle
                icon={IconType.palette}
                color={GlobalType.secondary}
              >
                Card Header with CardActionTitle
              </CardActionTitle>
            ),
            actionElement: (
              <Switch checked={false} onChange={() => undefined} />
            ),
          }}
        >
          Card header with a complex actionElement
        </Card>
      </div>
    );
  },
};
