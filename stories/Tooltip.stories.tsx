import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Tooltip from '../src/components/Tooltip/Tooltip';
import { TransparentButton } from '../src';

export default {
  title: 'Tooltip',
  component: Tooltip,
  parameters: {
    layout: 'centered',
  },
  argTypes: {
    title: {
      control: 'text',
    },
    label: {
      control: 'text',
    },
    placement: {
      control: {
        type: 'select',
        options: ['auto', 'top', 'right', 'bottom', 'left'],
      },
    },
    boundary: {
      control: 'object',
    },
    clipOverflow: {
      control: 'boolean',
    },
    preventOverflow: {
      control: 'boolean',
    },
    coords: {
      control: 'object',
    },
    isOpen: {
      control: 'boolean',
    },
    arrow: {
      control: 'boolean',
    },
  },
} as Meta<typeof Tooltip>;

type Story = StoryObj<typeof Tooltip>;

const TooltipStory = ({
  containerRef,
}: {
  containerRef: React.RefObject<HTMLElement>;
}) => {
  const [coords, setCoords] = React.useState({ x: 0, y: 0 });

  React.useEffect(() => {
    const handler = ({
      clientX: x,
      clientY: y,
    }: {
      clientX: number;
      clientY: number;
    }) => setCoords({ x, y });

    document.addEventListener('mousemove', handler);
    return () => {
      document.removeEventListener('mousemove', handler);
    };
  }, []);

  return (
    <div
      style={{
        height: 1000,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        gap: 40,
      }}
    >
      <Tooltip
        title="Title"
        label="label"
        placement="auto"
        boundary={containerRef.current as HTMLElement}
        clipOverflow
        preventOverflow
        coords={coords}
        isOpen
      >
        <span>I'm a free spirit, I like to roam freely ✨</span>
      </Tooltip>
    </div>
  );
};

export const TooltipStoryComponent: Story = {
  render: () => {
    return (
      <div>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            margin: 200,
            width: 400,
            gap: 100,
          }}
        >
          <Tooltip
            keepMounted
            id="1"
            title="Tooltip"
            placement="top"
            label="Label"
          >
            <button>Hover over me!</button>
          </Tooltip>

          <Tooltip
            className="looo"
            testId="testtttt"
            id="2"
            title="Title"
            label="Label"
            placement="top"
          >
            <button>Hover over me too!</button>
          </Tooltip>
        </div>
      </div>
    );
  },
};

export const TooltipPlacements: Story = {
  render: () => {
    const Component = () => {
      const [isOpen, setisOpen] = React.useState(false);
      React.useEffect(() => {
        setisOpen(true);
      }, []);
      return (
        <div>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'column',
              margin: 60,
              width: 400,
              gap: 100,
            }}
          >
            <Tooltip
              label="Label"
              id="1"
              title="Top"
              placement="top"
              isOpen={isOpen}
              coords={{ x: 100, y: 100 }}
            />

            <Tooltip
              label="Label"
              id="2"
              title="Bottom"
              placement="bottom"
              isOpen={isOpen}
              coords={{ x: 100, y: 100 }}
            />

            <Tooltip
              label="Label"
              id="3"
              title="Right"
              placement="right"
              isOpen={isOpen}
              coords={{ x: 100, y: 200 }}
            />

            <Tooltip
              id="4"
              label="Label"
              title="Left"
              placement="left"
              isOpen={isOpen}
              coords={{ x: 100, y: 200 }}
            />
          </div>
        </div>
      );
    };
    return <Component />;
  },
};

export const FreeTooltip: Story = {
  render: () => {
    const Component = () => {
      const containerRef = React.useRef<HTMLDivElement>(null);

      return (
        <div ref={containerRef}>
          <TooltipStory containerRef={containerRef} />
        </div>
      );
    };
    return <Component />;
  },
};

export const NoArrowTooltip: Story = {
  render: () => {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          margin: 100,
          width: 400,
          gap: 100,
        }}
      >
        <Tooltip
          id="1"
          title="Top"
          placement="top"
          hasArrow={false}
          isOpen
          label="Label"
        >
          <span>Tooltip with no arrow</span>
        </Tooltip>
      </div>
    );
  },
};

export const FocusTooltip: Story = {
  render: () => {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          margin: 100,
          width: 500,
          gap: 100,
        }}
      >
        <Tooltip
          matchWidth
          placement="top"
          label="This is a content of this tooltip"
          aria-live="polite"
        >
          <TransparentButton>Focus or hover over me</TransparentButton>
        </Tooltip>
        <Tooltip
          disableForHoverTouch
          matchWidth
          placement="top"
          label="I only appear when focued"
        >
          <TransparentButton>Focus me</TransparentButton>
        </Tooltip>
        <ol>
          <li>Tooltip will appear on both focus and focus-visible state.</li>
          <li>Press ESC to close the Tooltip.</li>
          <li>Tooltip is accessible (use screen reader to test).</li>
        </ol>
      </div>
    );
  },
};
