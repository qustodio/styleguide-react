import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Button, {
  ButtonType,
  ButtonSize,
  ButtonColor,
  ButtonIconPosition as IconPosition,
  ButtonProps,
} from '../src/components/Button/Button';
import Icon, { IconFamily, IconType } from '../src/components/Icons';
import { fn } from '@storybook/test';
import { groupByProp, lift } from './helpers';

export default {
  title: 'Button',
  component: Button,
  parameters: {
    layout: 'centered',
  },
  args: {
    onClick: fn(),
  },
  argTypes: {
    buttonType: {
      control: { type: 'select' },
      options: Object.keys(ButtonType),
      mapping: ButtonType,
    },
    color: {
      control: { type: 'select' },
      options: Object.keys(ButtonColor),
      mapping: ButtonColor,
    },
    loading: { control: 'boolean', defaultValue: false },
    iconPosition: {
      control: { type: 'select' },
      options: Object.keys(IconPosition),
      mapping: IconPosition,
    },
    size: {
      control: { type: 'select' },
      options: Object.keys(ButtonSize),
      mapping: ButtonSize,
    },
    icon: {
      control: { type: 'select' },
      options: ['none', 'left-arrow', 'right-arrow'],
      mapping: {
        none: undefined,
        'left-arrow': (
          <Icon type={IconType.arrowLeft} family={IconFamily.solid} />
        ),
        'right-arrow': (
          <Icon type={IconType.arrowRight} family={IconFamily.solid} />
        ),
      },
    },
  },
} as Meta<typeof Button>;

type Story = StoryObj<typeof Button>;

export const Default: Story = {
  args: {
    children: 'Button text',
  },
};

const combine = lift(
  (type: ButtonType) => (color: ButtonColor) => (loading: boolean) => (
    iconPosition: IconPosition
  ) => (size: ButtonSize) => (icon: React.ReactNode) => (
    disabled: boolean
  ) => ({ type, color, loading, iconPosition, size, icon, disabled })
);

export const allButtons: Story = {
  render: () => {
    const allButtons = combine(
      Object.values(ButtonType),
      Object.values(ButtonColor),
      [false, true],
      [...Object.values(IconPosition), undefined],
      Object.values(ButtonSize),
      [
        undefined,
        <Icon type={IconType.arrowLeft} family={IconFamily.solid} />,
        <Icon type={IconType.arrowRight} family={IconFamily.solid} />,
      ],
      [false, true]
    ) as ButtonProps[];

    return (
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: 'repeat(4, 1fr)',
          gap: '20px', // Espacio entre las celdas del grid
          padding: '20px', // Espacio interior del contenedor
        }}
      >
        {allButtons.map(props => (
          <Button {...props} onClick={fn()}>
            Button
          </Button>
        ))}
      </div>
    );
  },
};
