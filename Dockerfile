FROM node:20.10.0-slim

WORKDIR /opt/qustodio/rocket


# Install basic tooling
# ---------------------------------------------------------------------------------------
RUN apt-get update && apt-get install -y --no-install-recommends \
      wget \
      build-essential \
      git \
      unzip \
      curl \
      ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


# Install aws-cli
# ---------------------------------------------------------------------------------------
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install


# Set node environment
# ---------------------------------------------------------------------------------------
## More info: https://classic.yarnpkg.com/en/docs/cli/policies/
ENV YARN_VERSION=1.22.18
RUN yarn policies set-version $YARN_VERSION


# Install node dependencies
# ---------------------------------------------------------------------------------------
COPY ./package.json /opt/qustodio/rocket/
COPY ./yarn.lock /opt/qustodio/rocket/
RUN yarn
RUN yarn playwright install-deps      
RUN yarn playwright install

# parents-app
# ---------------------------------------------------------------------------------------
## Copy code
COPY . /opt/qustodio/rocket

# Entrypoint
# ---------------------------------------------------------------------------------------
ENTRYPOINT ["yarn"]
