import { create } from '@storybook/theming/create';
import logo from './public/logo.svg';

export default create({
  base: 'light',
  colorPrimary: '#49C592',
  colorSecondary: '#6161FF',
  appBg: '#F8FAFD',
  textColor: '#36383B',
  fontBase: '"Poppins", sans-serif',
  fontCode: 'monospace',
  inputTextColor: '#36383B',
  brandTitle: 'Qustodio',
  brandUrl: 'https://www.qustodio.com',
  brandImage: logo,
});
